const errorOverlayMiddleware = require("react-dev-utils/errorOverlayMiddleware");
const noopServiceWorkerMiddleware = require("react-dev-utils/noopServiceWorkerMiddleware");
const config = require("./webpack.config.dev");
const cors = require("cors");
const paths = require("./paths");
const bodyParser = require("body-parser");
const _ = require("lodash");

const Parser = require("rss-parser");

const protocol = process.env.HTTPS === "true" ? "https" : "http";
const host = process.env.HOST || "0.0.0.0";

const { isExist, mp3 } = require("../src/tts/make.js");

let parser = new Parser({
  timeout: 20000,
});

module.exports = function (proxy, allowedHost) {
  return {
    disableHostCheck: !proxy || process.env.DANGEROUSLY_DISABLE_HOST_CHECK === "true",
    compress: true,
    clientLogLevel: "none",
    contentBase: paths.appPublic,
    watchContentBase: true,
    hot: true,
    publicPath: config.output.publicPath,
    quiet: true,
    watchOptions: {
      ignored: /node_modules/,
    },
    https: protocol === "https",
    host: host,
    overlay: false,
    historyApiFallback: {
      disableDotRule: true,
    },
    public: allowedHost,
    proxy,
    setup(app) {
      // CORS 설정
      app.use(cors());

      app.use(errorOverlayMiddleware());
      app.use(noopServiceWorkerMiddleware());

      app.use(bodyParser.urlencoded({ extended: false }));
      app.use(bodyParser.json());

      app.get("/tts", function (req, res) {
        const {
          // headers,
          query: { name, text },
        } = req;

        let path = "build/voice";

        console.log("- tts ", path, name, text);

        if (!name) {
          console.error("Audio file name nothing...");
          res.status(500).json({
            common: {
              success: false,
              message: "",
              error: { message: "Audio file name nothing.." },
            },
          });
          return false;
        }

        const url = path + "/" + name + ".mp3";

        isExist(url, file => {
          if (!file) {
            // 음성 파일 생성.
            mp3(url, text);

            res.status(200).json({
              common: {
                success: true,
                message: `voice file ${name} make! `,
                error: null,
              },
              body: { name, url },
            });
          } else {
            res.status(200).json({
              common: {
                success: true,
                message: `voice file ${name} is exist `,
                error: null,
              },
              body: { name, url },
            });
          }
        });
      });

      app.post("/tts", function (req, res) {
        const {
          // headers,
          body: { voices },
        } = req;

        console.log("- tts voices ", voices);

        let path = "build/voice";

        _.map(voices, (voice, inx) => {
          const { name, text } = voice;

          const url = path + "/" + name + ".mp3";

          voice.url = url;

          console.log("- voice ", name, text, url);

          isExist(url, file => {
            if (!file) {
              mp3(url, text);

              if (voices.length - 1 === inx) {
                res.status(200).json({
                  common: {
                    success: true,
                    message: `voice file ${name} make! `,
                    error: null,
                  },
                  body: { voices },
                });
              }
            } else {
              if (voices.length - 1 === inx) {
                res.status(200).json({
                  common: {
                    success: true,
                    message: `voice file ${name} is exist `,
                    error: null,
                  },
                  body: { voices },
                });
              }
            }
          });
        });
      });

      // Cross Domain 때문에 서버에서 요청 한다.
      app.get("/rss", function (req, res) {
        let { url } = req.query;

        // console.log("- rss url", url);

        (async () => {
          try {
            let feed = await parser.parseURL(url);

            // console.log("- feed", feed);

            // feed.items.forEach(item => {
            //   console.log(item.title + ":" + item.link);
            // });

            res.status(200).json({
              common: {
                success: true,
                message: `rss feed ok`,
                error: null,
              },
              body: { feed },
            });
          } catch (error) {
            console.error(error);

            res.status(500).json({
              common: {
                success: false,
                message: "",
                error,
              },
            });
          }
        })();
      });
    },
  };
};
