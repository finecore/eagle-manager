/* This config file is only for transpiling the Express server file.
 * You need webpack-node-externals to transpile an express file
 * but if you use it on your regular React bundle, then all the
 * node modules your app needs to function get stripped out.
 *
 * Note: that prod and dev mode are set in npm scripts.
 */
const nodeExternals = require("webpack-node-externals");
const path = require("path");
const paths = require("./paths");

process.env.NODE_PATH = "src";

module.exports = (env, argv) => {
  process.env.BABEL_ENV = argv.define;
  process.env.NODE_ENV = argv.define;

  process.setMaxListeners(20);

  const SERVER_PATH = argv.define === "production" ? "./src/server/server-prod.js" : "./src/server/server-test.js";

  console.log("- SERVER_PATH", SERVER_PATH, argv.define);

  return {
    entry: {
      server: [require.resolve("./polyfills"), SERVER_PATH],
    },
    output: {
      path: paths.appBuild,
      publicPath: "/",
      filename: "[name].js",
    },
    //mode: argv.define,
    target: "node",
    node: {
      // Need this when working with express, otherwise the build fails
      __dirname: false, // if you don't put this is, __dirname
      __filename: false, // and __filename return blank or /
    },
    externals: [nodeExternals()], // Need this to avoid error when working with Express
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.resolve(__dirname, "./src/"),
          loader: "babel-loader",
          exclude: /node_modules/,
          query: {
            presets: ["react", "es2015"],
          },
        },
      ],
    },
    resolve: {
      modules: [path.resolve("./src"), "node_modules"],
    },
  };
};
