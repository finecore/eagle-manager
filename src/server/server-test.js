const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const chalk = require("chalk");
const cors = require("cors");
const _ = require("lodash");
const fs = require("fs");
const rimraf = require("rimraf");
const Parser = require("rss-parser");

let parser = new Parser({
  timeout: 10000,
});

const { isExist, voice, mp3 } = require("../tts/make.js");

process.env.BABEL_ENV = "development";
process.env.NODE_ENV = "development";

const logger = require("logger/logger");

const app = express();

const PORT = 80;

app.listen(PORT, () => {
  logger.info("========== node express server started =============");
  console.info("Service Mode", process.env.NODE_ENV);
  logger.info("Listening on " + PORT);
  logger.info("==================================================== \n");
});

app.use(express.static(path.resolve("build")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS 설정
app.use(cors());

// error handler
app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  logger.error("app error:" + err.stack + " " + req.baseUrl + " " + req.originalUrl);

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.get("/tts", function (req, res) {
  const {
    query: { text, voice_name, voice_rate },
  } = req;

  console.log("");
  console.log("------------------ tts ------------------");
  console.log("- get tts ", text);

  if (!text) {
    console.error("Audio text nothing...");
    res.status(500).json({
      common: {
        success: false,
        message: "",
        error: { message: "Audio text nothing.." },
      },
    });
    return false;
  }

  // 음성 데이터 반환.
  voice(text, voice_name, voice_rate).then((response) => {
    const { audioContent } = response;

    if (audioContent.length) {
      res.status(200).json({
        common: {
          success: true,
          message: `voice contents ${text} make! `,
          error: null,
        },
        body: { contents: audioContent },
      });
    } else {
      res.status(500).json({
        common: {
          success: false,
          message: "",
          error: { message: `voice contents ${text} falil! ` },
        },
      });
    }
  });
});

app.post("/tts", function (req, res) {
  const {
    // headers,
    body: {
      place_id,
      voice: { name, text },
      voice_name,
      voice_rate,
      reset,
    },
  } = req;

  console.log("");
  console.log("------------------ tts ------------------");
  console.log("- tts ", place_id, name, text, voice_name, voice_rate);

  if (!place_id) {
    console.error("Audio place_id nothing...");
    res.status(500).json({
      common: {
        success: false,
        message: "",
        error: { message: "Audio place_id nothing.." },
      },
    });
    return false;
  }

  if (!name) {
    console.error("Audio file name nothing...");
    res.status(500).json({
      common: {
        success: false,
        message: "",
        error: { message: "Audio file name nothing.." },
      },
    });
    return false;
  }

  let path = "build/voice/" + place_id;

  if (!fs.existsSync(path)) fs.mkdirSync(path);

  const url = path + "/" + name + ".mp3";

  console.log("- tts url ", url);

  isExist(url, (file) => {
    // 파일이 없거나 리셋 명령이면 새로 만든다.
    if (!file || reset) {
      // 음성 파일 생성.
      mp3(url, text, voice_name, voice_rate).then((file) => {
        res.status(200).json({
          common: {
            success: true,
            message: `voice file ${name} make! `,
            error: null,
          },
          body: { name, url },
        });
      });
    } else {
      res.status(200).json({
        common: {
          success: true,
          message: `voice file ${name} is exist `,
          error: null,
        },
        body: { name, url },
      });
    }
  });
});

app.post("/tts/list", function (req, res) {
  const {
    // headers,
    body: { place_id, voices, voice_name, voice_rate, reset },
  } = req;

  if (!place_id) {
    console.error("Audio place_id nothing...");
    res.status(500).json({
      common: {
        success: false,
        message: "",
        error: { message: "Audio place_id nothing.." },
      },
    });
    return false;
  }

  console.log("");
  console.log("------------------ tts/list ------------------");
  console.log("- tts list ", { place_id, voices, voice_name, voice_rate, reset });

  let path = "build/voice/" + place_id;

  // 리셋 명령 시 디렉토리 삭제.
  if (reset && fs.existsSync(path)) rimraf.sync(path);

  // 디렉토리 없으면 생성
  if (!fs.existsSync(path)) fs.mkdirSync(path);

  let cnt = 0;

  _.map(voices, (voice, inx) => {
    const { name, text } = voice;

    if (name && text) {
      const url = path + "/" + name + ".mp3";

      voice.url = url;

      isExist(url, (file) => {
        // 파일이 없거나 리셋 명령이면 새로 만든다.
        if (!file) {
          // 한번에 요청 시 API 작동 안함.
          setTimeout(() => {
            console.log("- new voice file start ", text, url);

            // 음성 파일 생성.
            mp3(url, text, voice_name, voice_rate).then((done) => {
              if (done) console.log("- new voice file done ", done, cnt, text, url);

              cnt++;

              if (voices.length === cnt) console.log("- new voice file success ");
            });
          }, 100 * cnt);
        } else {
          console.log("- voice file exist ", text, url);
          cnt++;
        }
      });
    } else {
      console.error("Audio file name nothing...");
    }
  });

  res.status(200).json({
    common: {
      success: true,
      message: `voice file make! `,
      error: null,
    },
    body: { voices },
  });
});

// Cross Domain 때문에 서버에서 요청 한다.
app.get("/rss", function (req, res) {
  let { url } = req.query;

  console.log("- rss url", url);

  (async () => {
    try {
      let feed = await parser.parseURL(url);

      // console.log("- feed", feed);

      // feed.items.forEach(item => {
      //   console.log(item.title + ":" + item.link);
      // });

      res.status(200).json({
        common: {
          success: true,
          message: `rss feed ok`,
          error: null,
        },
        body: { feed },
      });
    } catch (error) {
      res.status(500).json({
        common: {
          success: false,
          message: "",
          error,
        },
      });
    }
  })();
});

/* support client-side routing */
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "./../build/index.html"));
});

/**
 * Make node.js not exit on error
 */
process.on("uncaughtException", function (err) {
  console.error(chalk.red("==> Caught exception: " + JSON.stringify(err)));
});
