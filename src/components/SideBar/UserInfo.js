import React, { Component } from "react";
import { Collapse } from "react-bootstrap";
import { connect } from "react-redux";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

class UserInfo extends Component {
  state = {
    isShowingUserMenu: false,
    loading: true,
  };

  componentDidMount() {
    // 로딩 상태 종료
    this.setState({
      loading: false,
    });
  }

  render() {
    let { user } = this.props;
    let { isShowingUserMenu } = this.state;

    if (this.state.loading) {
      // 로딩중일때 로더 보여주기
      return <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>;
    }

    return (
      <div className="user-wrapper">
        <div className="user">
          <img src={user.image} alt={user.name} className="photo" />
          <div className="userinfo">
            <div className="username">{user.name}</div>
            <div className="title">Admin</div>
          </div>
          <span
            onClick={() =>
              this.setState({
                isShowingUserMenu: !this.state.isShowingUserMenu,
              })
            }
            className={cx("pe-7s-angle-down collapse-arrow", {
              active: isShowingUserMenu,
            })}
          />
        </div>
        <Collapse in={isShowingUserMenu}>
          <ul className="nav user-nav">
            <li>
              <a href="#">My Profile</a>
            </li>
            <li>
              <a href="#">Edit Profile</a>
            </li>
            <li>
              <a href="#">Settings</a>
            </li>
          </ul>
        </Collapse>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.Auth.user,
});

export default connect(mapStateToProps)(UserInfo);
