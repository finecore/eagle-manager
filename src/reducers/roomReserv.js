/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomReserv from "actions/roomReserv";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  item: {},
  list: [],
  search: [],
  enables: [],
  websocket: false,
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomReserv.SET_ROOM_RESERV:
      return applySetRoomReserv(state, action);
    case RoomReserv.SET_ALL_ROOM_RESERV_LIST:
      return applySetAllRoomReservList(state, action);
    case RoomReserv.SET_SEARCH_ROOM_RESERV_LIST:
      return applySetSearchRoomReservList(state, action);
    case RoomReserv.SET_ROOM_RESERV_LIST:
      return applySetRoomReservList(state, action);
    case RoomReserv.SET_PLACE_ENABLE_RESERV_LIST:
      return applySetEnableRoomReservList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomReservList(state, action) {
  const { all_room_reservs: list } = action;
  return {
    ...state,
    list,
  };
}

function applySetSearchRoomReservList(state, action) {
  const { all_room_reservs: search } = action;
  return {
    ...state,
    search,
  };
}

function applySetRoomReservList(state, action) {
  const { room_reservs: list } = action;

  return {
    ...state,
    list,
  };
}

function applySetEnableRoomReservList(state, action) {
  const { room_reservs: enables } = action;

  return {
    ...state,
    enables,
  };
}

function applySetRoomReserv(state, action) {
  const { room_reserv: item, websocket } = action;

  let list = state.list;

  if (item.id) {
    list = state.list.filter((val) => val.id !== item.id);
    list = list.concat({ ...item });
  }

  return {
    ...state,
    list,
    item,
    websocket,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
