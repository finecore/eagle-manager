/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as Ws from "actions/ws";
import { JOIN_REQUESTED, MESSAGE_ADDED, USER_JOINED, USER_LEFT, USER_REQUESTED, USER_STARTED_TYPING, USER_STOPPED_TYPING } from "constants/message-types";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  user: {},
  users: [],
  messages: [],
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return applyHandler(state, action);
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applyHandler(state = {}, action) {
  const { type, payload, metadata } = action;
  const handler = handlers[type];
  if (handler) {
    return handler({ state, payload, metadata });
  }
  return {
    ...state,
  };
}

const handlers = {
  [JOIN_REQUESTED]: ({ state, payload: currentUser }) => ({
    ...state,
    currentUser,
  }),
  // Note: currentUser is already in users
  [USER_JOINED]: ({ state, payload: user }) => ({
    ...state,
    users: state.users.concat([user]),
  }),
  [Ws.TYPING_STARTED]: ({ state }) => ({
    ...state,
    currentUserIsTyping: true,
  }),
  [Ws.TYPING_STOPPED]: ({ state }) => ({
    ...state,
    currentUserIsTyping: false,
  }),
  [MESSAGE_ADDED]: ({ state, payload: message, metadata: { createdAt } }) => ({
    ...state,
    messages: state.messages.concat([{ ...message, createdAt }]),
  }),
  [USER_REQUESTED]: ({ state, payload: users }) => ({
    ...state,
    users,
  }),
  [USER_LEFT]: ({ state, payload }) => ({
    ...state,
    users: state.users ? state.users.filter(({ id }) => payload.user && id !== payload.user.id) : [],
  }),
  [USER_STARTED_TYPING]: ({ state, payload: { user } }) => ({
    ...state,
    userIdsTyping: {
      ...state.userIdsTyping,
      [user.id]: true,
    },
  }),
  [USER_STOPPED_TYPING]: ({ state, payload: { user } }) => ({
    ...state,
    userIdsTyping: Object.keys(state.userIdsTyping)
      .filter((key) => key !== user.id.toString())
      .reduce((accum, key) => ({ ...accum, [key]: true }), {}),
  }),
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
