/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomReserv from "actions/preferences";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  modify: {}, // 환경설정에서 부모 자식 간에 전달할 수정 데이터.
  item: {},
  stopLoad: false, // 자동 로딩 중지 여부(화면 조작 시 초기화 되는 문제로 중단.)
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomReserv.SET_PREFERENCE:
      return applySetPreferences(state, action);
    case RoomReserv.SET_PREFERENCE_MODIFY:
      return applySetModify(state, action);
    case RoomReserv.SET_STOP_RELOAD:
      return applySetStopReload(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

let stay_type_color = {
  0: "#c8c8c8",
  1: "#ffab3e",
  2: "#52d3ee",
  3: "#ff727a",
  4: "#a1a1a1",
};

let key_type_color = {
  1: "#5c56af",
  2: "#548b8d",
  3: "#bb3e3eda",
};

let room_name_color = {
  1: "#2f2f2fd4",
  2: "#e02c14fa",
};

let time_bg_color = {
  1: "#f3f3f394",
  2: "#ff8484a8",
};

let room_log_show_type = {
  power: 0,
  signal: 0,
  air_temp: 0,
  car_call: 0,
};

function toMap(item) {
  if (!item.voice_opt) item.voice_opt = {};
  else if (typeof item.voice_opt !== "object") item.voice_opt = JSON.parse(item.voice_opt);

  if (!item.stay_type_color) item.stay_type_color = stay_type_color;
  else if (typeof item.stay_type_color !== "object") item.stay_type_color = JSON.parse(item.stay_type_color);

  if (!item.key_type_color) item.key_type_color = key_type_color;
  else if (typeof item.key_type_color !== "object") item.key_type_color = JSON.parse(item.key_type_color);

  if (!item.room_name_color) item.room_name_color = room_name_color;
  else if (typeof item.room_name_color !== "object") item.room_name_color = JSON.parse(item.room_name_color);

  if (!item.time_bg_color) item.time_bg_color = time_bg_color;
  else if (typeof item.time_bg_color !== "object") item.time_bg_color = JSON.parse(item.time_bg_color);

  if (!item.room_log_show_type) item.room_log_show_type = room_log_show_type;
  else if (typeof item.room_log_show_type !== "object") item.room_log_show_type = JSON.parse(item.room_log_show_type);

  return item;
}

function applySetPreferences(state, action) {
  const { preferences } = action;

  console.log("- applySetPreferences", preferences);

  let item = toMap(preferences);

  let modify = _.cloneDeep(item);

  return {
    ...state,
    item,
    modify,
  };
}

function applySetModify(state, action) {
  let { modify } = action;

  console.log("- applySetModify", state.item, modify);

  modify = toMap(modify);

  return {
    ...state,
    modify,
  };
}

function applySetStopReload(state, action) {
  const { stop } = action;

  console.log("- applySetStopReload", stop);

  return {
    ...state,
    stopLoad: stop,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
