/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomStateLog from "actions/roomStateLog";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  item: {},
  list: [],
  days: [],
  isDayLog: false, // 일일 로그 전체 조회 여부.(음성에서 전체 조회 시 무시 하기 위해)
  search: [], // 조회 매출 목록
  lastItem: {}, // 객실 상태 항목별 마지막 상태 저장.
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomStateLog.SET_ROOM_STATE_LOG:
      return applySetRoomStateLog(state, action);
    case RoomStateLog.SET_ALL_ROOM_STATE_LOG_LIST:
      return applySetAllRoomStateLogList(state, action);
    case RoomStateLog.SET_ROOM_STATE_LOG_LIST:
      return applySetRoomStateLogList(state, action);
    case RoomStateLog.SET_ROOM_STATE_DAY_LOG_LIST:
      return applySetRoomStateDayLogList(state, action);
    case RoomStateLog.ADD_ROOM_STATE_LOG_LIST:
      return applyAddRoomStateLog(state, action);
    case RoomStateLog.SET_ROOM_STATE_LOG_SEARCH_LIST:
      return applySetRoomStateLogSearchList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomStateLogList(state, action) {
  const { all_room_state_logs: list } = action;

  let lastItem = {};

  _.each(list, (item) => {
    lastItem[item.room_id] = item;
  });

  return {
    ...state,
    list,
    isDayLog: false,
    lastItem,
  };
}

function applySetRoomStateLogList(state, action) {
  const { room_state_logs } = action;

  let list = state.list.filter((val) => Number(val.room_id) !== Number(room_state_logs.room_id));

  let lastItem = state.lastItem;

  _.each(list, (item) => {
    lastItem[item.room_id] = item;
  });

  return {
    ...state,
    list: list.concat([...room_state_logs]),
    isDayLog: false,
    lastItem,
  };
}

function applySetRoomStateLog(state, action) {
  const { room_state_log: item, room } = action;

  console.log("- applySetRoomStateLog", item, room);

  let list = state.list;
  let days = state.days;
  let lastItem = state.lastItem;

  // 해당 객실 마지막 이력 데이터 조회.
  let roomItem = _.find(lastItem, { room_id: item.room_id });

  if (!roomItem) roomItem = item;
  else roomItem.data = _.merge({}, roomItem.data, item.data);

  if (list) list.splice(0, 0, item); // 처음에 추가 (로그는 수정이 없다)

  if (item.room_id && item.data) {
    item.data = typeof item.data === "object" ? item.data : JSON.parse(item.data);
    if (item.data.sale !== undefined || item.data.key !== undefined || item.data.door !== undefined) {
      const roomItem = _.find(room.list, { id: Number(item.room_id) });
      item.data.name = roomItem.name; // 객실명.
      days.splice(0, 0, item.data); // 메인 객실 이력 메뉴에 추가.
    }

    //console.log("- data, days", data, days);
  }

  return {
    ...state,
    list,
    item,
    days,
    isDayLog: false,
  };
}

function applySetRoomStateDayLogList(state, action) {
  const { room_state_logs: days } = action;

  let lastItem = state.lastItem;

  _.each(days, (item) => {
    lastItem[item.room_id] = item;
  });

  return {
    ...state,
    days,
    item: {},
    isDayLog: true,
    lastItem,
  };
}

// device 에서 room_state 전송 시 websocket client 수신값.
function applyAddRoomStateLog(state, action) {
  const { room_state_log: item, room, websocket } = action;

  console.log("- applyAddRoomStateLog", item);

  // 동일 값 수신 시 패스.
  if (JSON.stringify(item) === JSON.stringify(state.item)) return state;

  let list = state.list;
  let days = state.days;
  let lastItem = state.lastItem;

  // 해당 객실 마지막 이력 데이터 조회.
  let roomItem = _.find(lastItem, { room_id: item.room_id });

  if (!roomItem) roomItem = item;
  else roomItem.data = _.merge({}, roomItem.data, item.data);

  if (item.room_id && item.data) {
    item.websocket = websocket; // websocket 에서 수신된 로그만 음성 재생함.

    try {
      item.data = typeof item.data === "object" ? item.data : JSON.parse(item.data);

      const roomItem = _.find(room.list, { id: Number(item.room_id) });

      if (roomItem) {
        let change_data = {};

        // 객실 이력의 마지막 상태와 동일 하다면 이력 추가 안함.(음성 재생 안함)
        if (roomItem && roomItem.data) {
          _.map(item.data, (v, k) => {
            console.log("- data", roomItem.data[v], v);
            if (!roomItem.data[v] || roomItem.data[v] !== v) change_data[k] = v;
          });
        } else {
          change_data = item.data;
        }

        console.log("- change_data", change_data);

        if (!_.isEmpty(change_data)) {
          change_data.name = roomItem.name; // 객실명.
          days.splice(0, 0, change_data); // 메인 객실 이력 메뉴에 추가.
        }
      }

      list.splice(0, 0, item); // 처음에 추가 (로그는 수정이 없다)
    } catch (e) {}
  }

  lastItem[item.room_id] = roomItem;

  // console.log("- lastItem", lastItem);

  return {
    ...state,
    list,
    item,
    days,
    isDayLog: false,
    lastItem,
  };
}

function applySetRoomStateLogSearchList(state, action) {
  const { room_state_logs: search } = action;

  //console.log("- applySetRoomStateLogSearchList", search);

  let lastItem = state.lastItem;

  _.each(search, (item) => {
    lastItem = _.merge({}, lastItem, item.data);
  });

  return {
    ...state,
    search,
    isDayLog: false,
    lastItem,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
