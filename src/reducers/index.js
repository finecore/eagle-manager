// redux-form reducer.
import { reducer as formReducer } from "redux-form";
//import undoable from "redux-undo";

// user reducers.
import error from "./error";
import common from "./common";
import network from "./network";
import layout from "./layout";
import auth from "./auth";
import company from "./company";
import place from "./place";
import placeLock from "./placeLock";
import device from "./device";
import preferences from "./preferences";
import room from "./room";
import roomTheme from "./roomTheme";
import roomType from "./roomType";
import roomFee from "./roomFee";
import roomSale from "./roomSale";
import roomSalePay from "./roomSalePay";
import roomState from "./roomState";
import roomReserv from "./roomReserv";
import roomStateLog from "./roomStateLog";
import roomView from "./roomView";
import roomViewItem from "./roomViewItem";
import roomInterrupt from "./roomInterrupt";
import roomDoorLock from "./roomDoorLock";
import seasonPremium from "./seasonPremium";
import voice from "./voice";
import iscSet from "./iscSet";
import iscKeyBox from "./iscKeyBox";
import iscState from "./iscState";
import subscribe from "./subscribe";
import placeSubscribe from "./placeSubscribe";
import timeOption from "./timeOption";
import mileage from "./mileage";
import mileageLog from "./mileageLog";
import member from "./member";
import sms from "./sms";
import notice from "./notice";
import noticePlace from "./noticePlace";
import as from "./as";
import file from "./file";

import ws from "./ws";
import user from "./user";
import rss from "./rss";

import tasks from "./tasks";

// websocket sore.subscribe 에서 마지막 action type 가져오려고 사용.
function lastAction(state = null, action) {
  return action;
}

export default {
  form: formReducer,
  tasks,
  layout,
  error,
  common,
  network,
  auth,
  company,
  place,
  placeLock,
  device,
  preferences,
  room,
  roomTheme,
  roomType,
  roomFee,
  roomSale,
  roomSalePay,
  roomState,
  roomReserv,
  roomStateLog,
  roomView,
  roomViewItem,
  roomInterrupt,
  roomDoorLock,
  seasonPremium,
  voice,
  iscSet,
  iscKeyBox,
  iscState,
  user,
  subscribe,
  placeSubscribe,
  timeOption,
  mileage,
  member,
  mileageLog,
  notice,
  noticePlace,
  file,
  as,
  ws,
  rss,
  sms,

  lastAction,
};
