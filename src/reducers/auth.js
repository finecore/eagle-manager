/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as Auth from "actions/auth";
import Swal from "sweetalert2";
import { actionCreators as wsAction } from "actions/ws";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  // 로그아웃이 안되게 sessionStorage 에 저장 한다. 유효 기간 체크는 거래시 마다 수행 한다.
  user: sessionStorage.getItem("user") && sessionStorage.getItem("user").indexOf("{") > -1 ? JSON.parse(sessionStorage.getItem("user")) : null,
  token: sessionStorage.getItem("token") || null,
  isLogined: sessionStorage.getItem("login") && sessionStorage.getItem("login") === "Y",
  isTempLogined: sessionStorage.getItem("temp") && sessionStorage.getItem("temp") === "Y",
  access_count: 1,
  isViewerLogin: sessionStorage.getItem("login") && sessionStorage.getItem("isViewerLogin") ? Boolean(sessionStorage.getItem("isViewerLogin")) : false,
  isIcrewViewer: sessionStorage.getItem("login") && sessionStorage.getItem("isIcrewViewer") ? Boolean(sessionStorage.getItem("isIcrewViewer")) : false,
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case Auth.SUCCESS_LOGIN:
      return applySuccessLogin(state, action);
    case Auth.SUCCESS_TEMP_LOGIN:
      return applySuccessTempLogin(state, action);
    case Auth.SUCCESS_LOGOUT:
      return applySuccessLogout(state, action);
    case Auth.DUP_ACCESS_FAILUE:
      return applyDupAccessFailue(state, action);
    case Auth.DUP_ACCESS_LOGOUT:
      return applyDupAccessLogout(state, action);
    case Auth.SET_UUID:
      return applySetUuid(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

function applySuccessLogin(state, action) {
  const { user, token, isViewerLogin } = action;

  // 아이크루 뷰어는 모든 업소 조회 후 로그인 가능.
  let isIcrewViewer = !isViewerLogin && user.type === 1 && (user.level < 2 || user.level === 9);

  console.log("- applySuccessLogin ", { isViewerLogin, isIcrewViewer });

  sessionStorage.setItem("token", token); // 브라우저 종료시 삭제.
  sessionStorage.setItem("user", JSON.stringify(user));
  sessionStorage.setItem("isIcrewViewer", isIcrewViewer);
  sessionStorage.setItem("isViewerLogin", isViewerLogin);
  sessionStorage.setItem("login", "Y");
  sessionStorage.setItem("temp", "N");

  if (!state.isIcrewViewer) localStorage.setItem("loginId", user.id); // 최종 로그인 id 정보 로컬 저장.

  return {
    ...state,
    user,
    token,
    isViewerLogin,
    isIcrewViewer,
    isLogined: !isIcrewViewer,
    isTempLogined: false,
  };
}

function applySuccessTempLogin(state, action) {
  const { user, token } = action;

  console.log("- applySuccessTempLogin ", { user, token });

  sessionStorage.setItem("token", token); // 브라우저 종료시 삭제.
  sessionStorage.setItem("user", JSON.stringify(user));
  sessionStorage.setItem("login", "N");
  sessionStorage.setItem("temp", "Y");

  return {
    ...state,
    user,
    token,
    isViewerLogin: false,
    isIcrewViewer: false,
    isLogined: false,
    isTempLogined: true,
  };
}

function applySuccessLogout(state, action) {
  // router 이동만 하면 현재 세션이 살이 있어서 리프레쉬 해준다.
  setTimeout(() => {
    // login token remove.
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("isIcrewViewer");
    sessionStorage.removeItem("isViewerLogin");
    sessionStorage.removeItem("login");
    sessionStorage.removeItem("temp");

    window.location.href = "/";
  }, 1);

  return {
    ...state,
    isLogined: false,
  };
}

// 중복로그인 시 이전 접속 계정 로그아웃 시킨다.
function applyDupAccessFailue(state, action) {
  // router 이동만 하면 현재 세션이 살이 있어서 리프레쉬 해준다.
  setTimeout(() => {
    Swal.fire({
      icon: "info",
      title: "중복접속",
      html: `동일 계정으로 다른 곳에서 접속 중 입니다.<br/><br/>고정 라이선스는 중복 접속이 불가능 합니다.`,
      showCloseButton: false,
      confirmButtonText: "확인",
    }).then((result) => {
      // login token remove.
      sessionStorage.removeItem("token");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("isIcrewViewer");
      sessionStorage.removeItem("isViewerLogin");
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("temp");

      window.location.href = "/";
    });

    // 웹소켓 종료(재접속됨)
    wsAction.close();
  }, 1);

  return {
    ...state,
    isLogined: false,
  };
}

// 중복로그인 시 이전 접속 계정 로그아웃 시킨다.
function applyDupAccessLogout(state, action) {
  // router 이동만 하면 현재 세션이 살이 있어서 리프레쉬 해준다.
  setTimeout(() => {
    Swal.fire({
      icon: "info",
      title: "중복접속 로그아웃",
      html: `동일 계정으로 다른 곳에서 접속 하였습니다. <br/><br/>보안을 위해 자동 로그아웃 되었습니다.`,
      showCloseButton: false,
      confirmButtonText: "확인",
    }).then((result) => {
      // login token remove.
      sessionStorage.removeItem("token");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("isIcrewViewer");
      sessionStorage.removeItem("isViewerLogin");
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("temp");

      window.location.href = "/";
    });

    // 웹소켓 종료(재접속됨)
    wsAction.close();
  }, 1);

  return {
    ...state,
    isLogined: false,
  };
}

function applySetUuid(state, action) {
  const { uuid, access_count } = action;
  const { user } = state;

  user.uuid = uuid;

  console.log("- applySetUuid", { uuid, access_count });

  sessionStorage.setItem("user", JSON.stringify(user)); // 브라우저 종료 후에도 지워지지 않음.

  return {
    ...state,
    user,
    access_count,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
