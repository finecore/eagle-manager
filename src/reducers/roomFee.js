/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomFee from "actions/roomFee";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialFee /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialFee = {
  list: [],
  item: [],
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialFee, action) {
  switch (action.type) {
    case RoomFee.SET_ROOM_FEE:
      return applySetRoomFee(state, action);
    case RoomFee.SET_ALL_ROOM_FEE_LIST:
      return applySetAllRoomFeeList(state, action);
    case RoomFee.SET_ROOM_FEE_TYPE_LIST:
      return applySetRoomFeeTypeList(state, action);
    case RoomFee.SET_ROOM_FEE_LIST:
      return applySetRoomFeeList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomFeeList(state, action) {
  const { all_room_fees, channel } = action;

  let list = state.list.filter((val) => val.channel !== channel); // 적용 채널 (0: 카운터, 1:자판기)

  list = list.concat([...all_room_fees]);
  list = _.sortBy(list, ["channel", "room_type_id", "day", "stay_type", "begin"]);

  return {
    ...state,
    list,
  };
}

function applySetRoomFeeTypeList(state, action) {
  const { room_fees, room_type_id, channel } = action;

  // 같은 채널의 타입 삭제. 적용 채널 (0: 카운터, 1:자판기)
  let list = state.list.filter((val) => val.room_type_id !== room_type_id && val.channel === channel);

  // 삭제가 아니라면.
  if (room_fees) {
    list = list.concat([...room_fees]);
    list = _.sortBy(list, ["channel", "room_type_id", "day", "stay_type", "begin"]);
  }

  return {
    ...state,
    list,
    item: room_fees,
  };
}

function applySetRoomFeeList(state, action) {
  const { room_fees, del } = action;

  // 기존 요금 제거.
  let list = state.list.filter((val) => !_.find(room_fees, { id: val.id }));

  // 삭제가 아니라면.
  if (!del) {
    list = list.concat([...room_fees]);
    list = _.sortBy(list, ["room_type_id", "day", "stay_type", "begin"]);
  }

  console.log("- applySetRoomFeeList", room_fees);

  return {
    ...state,
    list,
    item: room_fees[0],
  };
}

function applySetRoomFee(state, action) {
  const { room_fee: item } = action;

  let list = state.list.filter((val) => val.id !== item.id);

  // 삭제가 아니라면.
  if (item.day) {
    list = list.concat({ ...item });
    list = _.sortBy(list, ["room_type_id", "day", "stay_type", "begin"]);
  }

  return {
    ...state,
    list,
    item,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
