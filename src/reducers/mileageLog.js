/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as MileageLog from "actions/mileageLog";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  item: {},
  list: [],
  days: [],
  isDayLog: false, // 일일 로그 전체 조회 여부.(음성에서 전체 조회 시 무시 하기 위해)
  search: [], // 조회 매출 목록
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case MileageLog.SET_MILEAGE_LOG:
      return applySetMileageLog(state, action);
    case MileageLog.SET_ALL_MILEAGE_LOG_LIST:
      return applySetAllMileageLogList(state, action);
    case MileageLog.SET_MILEAGE_LOG_LIST:
      return applySetMileageLogList(state, action);
    case MileageLog.SET_MILEAGE_DAY_LOG_LIST:
      return applySetMileageDayLogList(state, action);
    case MileageLog.SET_MILEAGE_LOG_SEARCH_LIST:
      return applySetMileageLogSearchList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllMileageLogList(state, action) {
  const { all_mileage_logs: list } = action;
  return {
    ...state,
    list,
    isDayLog: false,
  };
}

function applySetMileageLogList(state, action) {
  const { mileage_logs } = action;

  let list = state.list.filter((val) => Number(val.room_id) !== Number(mileage_logs.room_id));

  return {
    ...state,
    list: list.concat([...mileage_logs]),
    isDayLog: false,
  };
}

function applySetMileageLog(state, action) {
  const { mileage_log: item, room } = action;

  let list = state.list;

  console.log("- applySetMileageLog", item, room);

  if (list) list.splice(0, 0, item); // 처음에 추가 (로그는 수정이 없다)

  return {
    ...state,
    list,
    item,
    isDayLog: false,
  };
}

function applySetMileageDayLogList(state, action) {
  const { mileage_logs: days } = action;

  return {
    ...state,
    days,
    item: {},
    isDayLog: true,
  };
}

function applySetMileageLogSearchList(state, action) {
  const { mileage_logs: search } = action;

  //console.log("- applySetMileageLogSearchList", search);

  return {
    ...state,
    search,
    isDayLog: false,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
