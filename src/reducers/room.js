/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as Room from "actions/room";
import _ from "lodash";
/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  list: [],
  item: {}, // 선택 객실.
  enable_list: [],
  selected: 0, // 룸선택 id.
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case Room.SET_ROOM_LIST:
      return applySetRoomList(state, action);
    case Room.SET_ROOM:
      return applySetRoom(state, action);
    case Room.SET_ROOM_SELECTED:
      return applySetRoomSelected(state, action);
    case Room.SET_ROOM_ENABLE_RESERV:
      return applySetRoomEnableReservList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetRoomList(state, action) {
  const { rooms } = action;

  let list = _.orderBy(rooms || [], (v) => Number(v.name.replace(/[^\d]/g, "")));

  return {
    ...state,
    list,
    item: list ? list[0] : {},
  };
}

function applySetRoom(state, action) {
  const { room: item } = action;

  let list = state.list;

  if (item) {
    list = state.list.filter((val) => val.id !== item.id);

    // 삭제가 아니라면.
    if (item.name && item.id) list = list.concat({ ...item });

    list = _.sortBy(list, ["name", "room_type_id"]);
  }

  // console.log("- applySetRoomType", list, item);

  return {
    ...state,
    list,
    item,
  };
}

function applySetRoomEnableReservList(state, action) {
  const { rooms: enable_list } = action;

  return {
    ...state,
    enable_list,
  };
}

function applySetRoomSelected(state, action) {
  const { selected } = action;

  const item = state.list.filter((val) => val.id === selected)[0];

  return {
    ...state,
    item,
    selected,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
