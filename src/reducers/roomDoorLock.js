/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomDoorLock from "actions/roomDoorLock";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  login: { token: null },
  item: {},
  list: [],
  search: [],
  enables: [],
  websocket: false,
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomDoorLock.SET_ROOM_DOORLOCK_LOGIN:
      return applySetRoomDoorLockLogin(state, action);
    case RoomDoorLock.SET_ROOM_DOORLOCK:
      return applySetRoomDoorLock(state, action);
    case RoomDoorLock.SET_ALL_ROOM_DOORLOCK_LIST:
      return applySetAllRoomDoorLockList(state, action);
    case RoomDoorLock.SET_SEARCH_ROOM_DOORLOCK_LIST:
      return applySetSearchRoomDoorLockList(state, action);
    case RoomDoorLock.SET_ROOM_DOORLOCK_LIST:
      return applySetRoomDoorLockList(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetRoomDoorLockLogin(state, action) {
  let { login } = action;

  return {
    ...state,
    login,
  };
}

function applySetAllRoomDoorLockList(state, action) {
  const { all_room_doorlocks: list } = action;
  return {
    ...state,
    list,
  };
}

function applySetSearchRoomDoorLockList(state, action) {
  const { all_room_doorlocks: search } = action;
  return {
    ...state,
    search,
  };
}

function applySetRoomDoorLockList(state, action) {
  const { room_doorlocks: list } = action;

  return {
    ...state,
    list,
  };
}

function applySetRoomDoorLock(state, action) {
  const { room_doorlock: item, websocket } = action;

  let list = state.list;

  if (item.id) {
    list = state.list.filter((val) => val.id !== item.id);
    list = list.concat({ ...item });
  }

  return {
    ...state,
    list,
    item,
    websocket,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
