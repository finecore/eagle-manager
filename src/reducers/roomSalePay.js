/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomSalePay from "actions/roomSalePay";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialSalePay /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialSalePay = {
  item: {},
  list: [],
  search: [], // 조회 매출 목록
  place: [], // 업소 전체 매출 합계.
  sum: [], // 기간별 판매 합계(객실별 타입별 ROLL UP)
  today: [], // 오늘 판매 합계(객실별 타입별 ROLL UP)
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialSalePay, action) {
  switch (action.type) {
    case RoomSalePay.SET_ROOM_SALE_PAY:
      return applySetRoomSalePay(state, action);
    case RoomSalePay.SET_ALL_ROOM_SALE_PAY_LIST:
      return applySetAllRoomSalePayList(state, action);
    case RoomSalePay.SET_ROOM_SALE_PAY_LIST:
      return applySetRoomSalePayList(state, action);
    case RoomSalePay.SET_ROOM_SALE_PAY_SEARCH_LIST:
      return applySetRoomSalePaySearchList(state, action);
    case RoomSalePay.SET_PLACE_ROOM_SALE_PAY_SUM:
      return applySetPlaceRoomSalePaySum(state, action);
    case RoomSalePay.SET_ROOM_SALE_PAY_SUM_ALL:
      return applySetAllRoomSalePaySum(state, action);
    case RoomSalePay.SET_ROOM_SALE_PAY_SUM:
      return applySetRoomSalePaySum(state, action);
    case RoomSalePay.SET_ROOM_SALE_PAY_TODAY:
      return applySetRoomSalePayToday(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomSalePayList(state, action) {
  const { all_room_sale_pays: list } = action;

  return {
    ...state,
    list,
  };
}

function applySetRoomSalePayList(state, action) {
  const { room_sale_pays, room_id } = action;

  let list = state.list.filter((val) => val.room_id !== room_id);
  let item = _.find(state.list, { id: room_id });

  return {
    ...state,
    list: room_sale_pays ? list.concat([...room_sale_pays]) : list,
    item,
  };
}

function applySetRoomSalePaySearchList(state, action) {
  const { room_sale_pays: search } = action;

  return {
    ...state,
    search,
  };
}

function applySetRoomSalePay(state, action) {
  let { room_sale_pay } = action;

  // 매출 기본 정보(기본적으로있어야 화면에 오류 안남.)
  const default_room_sale_pay = {
    sale_id: 0,
    user_id: undefined,
    default_fee: 0,
    add_fee: 0,
    pay_card_amt: 0,
    pay_cash_amt: 0,
  };

  if (!room_sale_pay) {
    return state;
  }

  let item = _.find(state.list, { id: room_sale_pay.id });
  let list = state.list.filter((val) => val.id !== room_sale_pay.id);

  // 기존이 있으면 머지.
  if (item) item = _.merge({}, item, room_sale_pay);
  else item = _.merge({}, default_room_sale_pay, room_sale_pay);

  return {
    ...state,
    list: list.concat({ ...item }),
    item,
  };
}

function applySetPlaceRoomSalePaySum(state, action) {
  const { room_sale_pays: place } = action;

  return {
    ...state,
    place,
  };
}

function applySetAllRoomSalePaySum(state, action) {
  const { room_sale_pays: sum } = action;

  return {
    ...state,
    sum,
  };
}

function applySetRoomSalePaySum(state, action) {
  const { room_id } = action;

  let sum = state.sum.filter((val) => val.room_id !== room_id);

  return {
    ...state,
    sum: state.sum.concat({ ...sum }),
  };
}

function applySetRoomSalePayToday(state, action) {
  const { room_sale_pays: today } = action;

  return {
    ...state,
    today,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
