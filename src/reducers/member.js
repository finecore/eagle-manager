/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as Member from "actions/member";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  list: [],
  item: {},
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case Member.SET_MEMBER_LIST:
      return applySetMemberList(state, action);
    case Member.SET_MEMBER:
      return applySetMember(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetMemberList(state, action) {
  let { members: list } = action;

  return {
    ...state,
    list,
  };
}

function applySetMember(state, action) {
  const { member } = action;

  let item = null;
  let list = state.list;

  if (member && member.id) {
    item = _.find(list, { id: member.id }) || {};
    list = list.filter((val) => val.id !== member.id);

    if (!member.deleted) {
      item = _.merge({}, item, member);
      list = list.concat({ ...item });
    }
  }

  list = _.orderBy(list, ["id"], ["asc"]);

  return {
    ...state,
    list,
    item,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
