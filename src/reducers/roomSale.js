/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomSale from "actions/roomSale";
import moment from "moment";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialSale /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialSale = {
  item: {},
  list: [],
  search: [], // 조회 매출 목록
  place: [], // 업소 전체 매출 합계.
  rooms: [], // 업소 객실별 매출 집계.
  sum: [], // 기간별 판매 합계(객실별 타입별 ROLL UP)
  today: [], // 오늘 판매 합계(객실별 타입별 ROLL UP)
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialSale, action) {
  switch (action.type) {
    case RoomSale.SET_ROOM_SALE:
      return applySetRoomSale(state, action);
    case RoomSale.SET_ALL_ROOM_SALE_LIST:
      return applySetAllRoomSaleList(state, action);
    case RoomSale.SET_ROOM_SALE_LIST:
      return applySetRoomSaleList(state, action);
    case RoomSale.SET_ROOM_SALE_SEARCH_LIST:
      return applySetRoomSaleSearchList(state, action);
    case RoomSale.SET_PLACE_ROOM_SALE_SUM:
      return applySetPlaceRoomSaleSum(state, action);
    case RoomSale.SET_ROOM_SALE_SUM_ROOMS:
      return applySetAllRoomSaleSum(state, action);
    case RoomSale.SET_ROOM_SALE_SUM:
      return applySetRoomSaleSum(state, action);
    case RoomSale.SET_ROOM_SALE_TODAY:
      return applySetRoomSaleToday(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomSaleList(state, action) {
  const { all_room_sales: list } = action;

  console.log("- all_room_sales", list);

  return {
    ...state,
    list,
  };
}

function applySetRoomSaleList(state, action) {
  const { room_sales, room_id } = action;

  let list = state.list.filter((val) => val.room_id !== room_id);
  let item = _.find(state.list, { id: room_id });

  return {
    ...state,
    list: room_sales ? list.concat([...room_sales]) : list,
    item,
  };
}

function applySetRoomSaleSearchList(state, action) {
  const { room_sales: search } = action;

  return {
    ...state,
    search,
  };
}

function applySetRoomSale(state, action) {
  let { room_sale } = action;

  // console.log("- applySetRoomSale", room_sale);

  // 매출 기본 정보(기본적으로있어야 화면에 오류 안남.)
  const default_room_sale = {
    state: undefined, // 미입실 상태.
    stay_type: undefined, // 미입실 상태.
    person: 0,
    check_in: undefined,
    check_out: undefined,
    check_in_exp: moment().format("YYYY-MM-DD HH:mm"),
    check_out_exp: moment().add(1, "day").format("YYYY-MM-DD HH:mm"),
    room_reserv_id: 0,
    default_fee: 0,
    add_fee: 0,
    reserv_fee: 0,
    pay_card_amt: 0,
    pay_cash_amt: 0,
    pay_point_amt: 0,
    prepay_ota_amt: 0,
    prepay_cash_amt: 0,
    prepay_card_amt: 0,
    prepay_point_amt: 0,
    alarm: "0",
    alarm_hour: 9,
    alarm_min: 0,
  };

  if (!room_sale) {
    return state;
  }

  let item = _.find(state.list, { id: room_sale.id });
  let list = state.list.filter((val) => val.id !== room_sale.id);

  // 기존이 있으면 머지.
  if (item) item = _.merge({}, item, room_sale);
  else item = _.merge({}, default_room_sale, room_sale);

  return {
    ...state,
    list: list.concat({ ...item }),
    item,
  };
}

function applySetPlaceRoomSaleSum(state, action) {
  const { room_sales: place } = action;

  return {
    ...state,
    place,
  };
}

function applySetAllRoomSaleSum(state, action) {
  const { room_sales: rooms } = action;

  return {
    ...state,
    rooms,
  };
}

function applySetRoomSaleSum(state, action) {
  const { room_id } = action;

  let sum = state.sum.filter((val) => val.room_id !== room_id);

  return {
    ...state,
    sum: state.sum.concat({ ...sum }),
  };
}

function applySetRoomSaleToday(state, action) {
  const { room_sales: today } = action;

  return {
    ...state,
    today,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
