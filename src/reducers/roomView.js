/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomView from "actions/roomView";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  list: [],
  item: {},
  mode: 0, // 0: 객실상태보기, 1:객실매출보기
  selected: 0, // 뷰 탭 선택.
  displayLayer: false, // popup display room id.
  filter: {
    key: { 0: true, 1: true, 2: true, 3: true, 4: true, 5: true, 6: true }, // 키 상태 (0:없음, 1:고객키,2:마스터키 3:청소키, 4:고객키 제거, 5:마스터키 제거, 6:청소키 제거)
    sale: { 0: true, 1: true, 2: true, 3: true }, // 판매 상태
    outing: { 0: true }, // 외출 상태
  }, // 뷰 필터.
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomView.SET_ALL_ROOM_VIEW_LIST:
      return applySetAllRoomViewList(state, action);
    case RoomView.SET_ROOM_VIEW:
      return applySetRoomView(state, action);
    case RoomView.SET_ROOM_VIEW_SELECTED:
      return applySetRoomViewSelected(state, action);
    case RoomView.SET_ROOM_VIEW_MODE:
      return applySetViewMode(state, action);
    case RoomView.SET_ROOM_VIEW_FILTER:
      return applySetViewFilter(state, action);
    case RoomView.SET_ROOM_VIEW_DISPLAY_LAYER:
      return applySetDisplayLayer(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomViewList(state, action) {
  const { all_room_views: list } = action;
  return {
    ...state,
    list,
  };
}

function applySetRoomView(state, action) {
  const { room_view } = action;

  let list = state.list;
  let item = state.item;

  if (room_view) {
    item = _.find(state.list, { id: room_view.id }) || {};
    item = _.merge({}, item, room_view); // 변경 사항 업데이트.

    list = state.list.filter((val) => val.id !== room_view.id);
    list = list.concat({ ...item });
  }

  list = _.sortBy(list, ["order"]);

  return {
    ...state,
    list,
    item,
  };
}

function applySetViewMode(state, action) {
  const { mode } = action;

  return {
    ...state,
    mode,
  };
}

function applySetViewFilter(state, action) {
  const { filter } = action;

  return {
    ...state,
    filter,
  };
}

function applySetRoomViewSelected(state, action) {
  const { selected } = action;

  const item = _.find(state.list, { id: selected }) || {};

  return {
    ...state,
    item,
    selected,
  };
}

function applySetDisplayLayer(state, action) {
  const { displayLayer } = action;

  // console.log("- applySetDisplayLayer", displayLayer);
  return {
    ...state,
    displayLayer,
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
