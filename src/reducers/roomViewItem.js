/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import * as RoomViewItem from "actions/roomViewItem";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// initialState /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const initialState = {
  list: [],
  item: {},
  viewElement: {},
};

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function reducer(state = initialState, action) {
  switch (action.type) {
    case RoomViewItem.SET_ALL_ROOM_VIEW_ITEM_LIST:
      return applySetAllRoomViewItemList(state, action);
    case RoomViewItem.SET_ROOM_VIEW_ITEM_LIST:
      return applySetRoomViewItemList(state, action);
    case RoomViewItem.SET_ROOM_VIEW_ITEM:
      return applySetRoomViewItem(state, action);
    case RoomViewItem.SET_ROOM_VIEW_ITEM_ELEMENT:
      return applySetRoomViewItemElement(state, action);
    case RoomViewItem.REMOVE_ROOM_VIEW_ITEM:
      return applyRemoveRoomViewItem(state, action);
    default:
      return state;
  }
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// reducer functions ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function applySetAllRoomViewItemList(state, action) {
  const { all_room_view_items: list } = action;

  return {
    ...state,
    list,
  };
}

function applySetRoomViewItemList(state, action) {
  const { room_view_items: items, view_id } = action;

  // console.log("- applySetRoomViewItemList", view_id, items);

  let list = state.list.filter((val) => val.view_id !== view_id);

  return {
    ...state,
    list: list.concat([...items]),
  };
}

function applySetRoomViewItem(state, action) {
  const { room_view_item } = action;

  let list = state.list;
  let item = state.item;

  if (room_view_item) {
    item = _.find(state.list, { id: room_view_item.id }) || {};
    item = _.assign({}, item, room_view_item); // 변경 사항 업데이트.

    list = state.list.filter((val) => val.id !== room_view_item.id);
    list = list.concat({ ...item });
  }
  return {
    ...state,
    list,
    item,
  };
}

function applySetRoomViewItemElement(state, action) {
  const { elements, view_id } = action;

  // console.log("- applySetRoomViewItemElement", view_id, elements.length);

  let viewElement = state.viewElement;
  viewElement[view_id] = elements;

  return {
    ...state,
    viewElement,
  };
}

function applyRemoveRoomViewItem(state, action) {
  const { id } = action;

  let list = state.list.filter((val) => val.id !== id);

  return {
    ...state,
    list,
    item: {},
  };
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// export reducer ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export default reducer;
