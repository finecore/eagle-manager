/**
 * Socket Parser Class.
 */
const SocketParser = {
  isJson(json) {
    return /^[\],:{}\s]*$/.test(json.toString());
  },

  /**
   * buffer to json.
   *
   * @param {*} buff
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  decode(buff, encoding = "utf8") {
    var json = buff.toString(encoding);

    try {
      json = json.replace(/"\\/gi, "").trim();
      json = JSON.parse(json);
    } catch (e) {
      // console.error(e, json);
      json = {
        common: {
          success: false,
          error: {
            code: 500,
            message: e.message,
            detail: "전문 형식이 맞지 않습니다.",
          },
        },
        body: json,
      };
    }
    return json;
  },

  /**
   * json to buffer.
   *
   * @param {*} json
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  encode(json, encoding = "utf8") {
    if (typeof json === "object") json = JSON.stringify(json);
    json = json.replace(/"\\/gi, "");
    return new Buffer.from(json, encoding);
  },
};

module.exports = SocketParser;
