// imports.
import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";

import { FormControl, ButtonToolbar, Button, Checkbox } from "react-bootstrap";

import { CubeGrid } from "better-react-spinkit";
import { envProps } from "config/configureStore";

import format from "utils/format-util";

import moment from "moment";
import cx from "classnames";

let QR = (props) => {
  let { preferences, item, timer, isValidPhone } = props;

  let isValid = item.qr_key_data && moment().isSameOrAfter(item.start_date) && moment().isSameOrBefore(item.end_date);

  let add = preferences.doolock_qr_add || "/upload/add/creweagle_banner00.jpg";

  add = envProps.API_SERVER_URL + add;

  // console.log("- add", add);
  // console.log("- item", { item, isValidPhone, isValid, timer });

  return (
    <div className="qr-key-wrapper">
      {props.loading && (
        // 로딩중일때 로더 보여주기
        <div className={cx("loader")}>{<CubeGrid color="#eee" size={60} />}</div>
      )}
      <div className="header">
        <h4>도어락 QR 코드</h4>
      </div>
      <div className="content">
        <div className="form-group form-radius">
          <div className="qrcode-img">
            {isValid ? (
              !props.isValidPhone ? (
                <div className="checkPhone">
                  <p className="place-text">{item.place_name}</p>
                  <p className="room-text">{"[" + item.room_type_name + "] " + item.room_name}</p>
                  <p className="comment">QR 코드키가 발급된 전화번호를 입력해 주세요</p>

                  <span className="phone">
                    <FormControl
                      type="tel"
                      placeholder="휴대폰 번호 입력"
                      value={props.phoneQRNumber || "010"}
                      onChange={(evt) => {
                        let toPhone = format.toPhone(evt.target.value);
                        props.inputQRPhone(toPhone);
                      }}
                      maxLength={13}
                    />
                    <Button onClick={(evt) => props.checkPhone()} disabled={!format.isPhone(props.phoneQRNumber)}>
                      {format.isPhone(props.phoneQRNumber) ? "휴대폰 인증" : "전화번호 입력 중"}
                    </Button>
                  </span>
                  <span className="checkno">
                    <Checkbox inline defaultChecked={props.isSavePhone} onChange={(evt) => props.handlePhoneSave(evt.target.checked)}>
                      <span />
                      번호 기억
                    </Checkbox>
                  </span>
                </div>
              ) : (
                <div>
                  <p className="place-text">{item.place_name}</p>
                  <p className="room-text">{"[" + item.room_type_name + "] " + item.room_name}</p>

                  {props.timer > 0 ? (
                    <div className="qr_arear">
                      <img id="image" src={`data:image/png;base64,${item.qr_key_data}`}></img>
                    </div>
                  ) : (
                    <p className="error-text">QR 코드를 새로고침 하세요</p>
                  )}

                  {props.timer > 0 ? <p className="notice">도어락에 QR코드를 {props.timer}초 내에 인식 시켜 주세요</p> : <p className="notice">QR 코드 인식 허용시간이 지났습니다</p>}

                  <p className="time-text">
                    QR 코드 키 사용기간 <br /> {moment(item.start_date).format("MM/DD HH:mm")} ~ {moment(item.end_date).format("MM/DD HH:mm")}
                  </p>
                </div>
              )
            ) : moment().isBefore(item.start_date) ? (
              <div>
                <p className="error-text">입실 가능시간 이전 입니다</p>
                <p className="time-text">
                  QR 코드 키 사용기간 <br /> {moment(item.start_date).format("MM/DD HH:mm")} ~ {moment(item.end_date).format("MM/DD HH:mm")}
                </p>
                <p className="alert-text">
                  입실 가능시간 이후 새로고침 하거나 <br />
                  카운터에 문의 하시기 바랍니다
                </p>
              </div>
            ) : moment().isAfter(item.end_date) ? (
              <div>
                <p className="error-text">QR 코드가 만료 되었습니다</p>
                <p className="time-text">
                  QR 코드 키 사용기간 <br /> {moment(item.start_date).format("MM/DD HH:mm")} ~ {moment(item.end_date).format("MM/DD HH:mm")}
                </p>
                <p className="alert-text">카운터에 문의 하시기 바랍니다</p>
              </div>
            ) : item.cancel ? (
              <div>
                <p className="error-text">QR 코드 발급이 취소 되었슴니다</p>
                <p className="alert-text">카운터에 문의 하시기 바랍니다</p>
              </div>
            ) : (
              <div>
                <p className="error-text">QR 코드가 올바르지 않습니다</p>
                <p className="alert-text">
                  잠시후 새로고침 하거나 <br />
                  카운터에 문의 하시기 바랍니다
                </p>
              </div>
            )}
          </div>

          {props.timer <= 0 || !isValid ? (
            <div className="btn-refresh">
              <Button onClick={(evt) => props.checkQrCode(true)} disabled={props.delay}>
                QR 코드 새로고침
              </Button>
            </div>
          ) : null}

          <div className="add-area">
            {/* OTP 번호 표시 추가 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
            <img src={add} alt="아이크루 스토어" />
          </div>
        </div>
        {item.place_tel && (
          <div className="callcenter">
            <a href={"tel:" + item.place_tel}>Tel. {item.place_tel}</a>
          </div>
        )}
        <div className="copyright">Copyrightⓒ 2021 All rights keyed by iCrew company</div>
      </div>
    </div>
  );
};

export default QR;
