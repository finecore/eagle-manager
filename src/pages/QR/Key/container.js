// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import _ from "lodash";
import moment from "moment";
import { encrypt, decrypt } from "utils/aes256-util";
import format from "utils/format-util";
import Swal from "sweetalert2";

// Component.
import QR from "./presenter";

// Styles.
import "./styles.scss";

import logoIcrew from "assets/images/logo_icrew_b.png";

class Container extends Component {
  constructor() {
    super();

    this.timeSec = 15; // QR 코드 화면 유효 시간.

    this.state = {
      item: {},
      loading: true,
      delay: false, // 조회 딜레이(무한 조회 방지)
      timer: this.timeSec,
      phoneQRNumber: "", // QR 코드 발급 전화번호
      isValidPhone: false, // QR 코드 발급 휴대전화 번호가 맞는지 체크
      isSavePhone: false,
    };

    this.checkTimer = null;
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    roomDoorLock: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  checkQrCode = (reset) => {
    const {
      params: { room_id },
    } = this.props.match;

    console.log("- room_id", room_id);

    if (room_id) {
      this.setState({
        loading: true,
        delay: true,
      });

      let id = decodeURIComponent(room_id);
      id = decrypt(id);

      // console.log("- decipher", id);

      if (id) {
        this.props.getRoomDoorLock(Number(id));

        if (reset) this.expireTimer();
      }

      setTimeout(() => {
        this.setState({
          loading: false,
        });
      }, 300);

      setTimeout(() => {
        this.setState({
          delay: false,
        });
      }, 5000);
    }
  };

  expireTimer = () => {
    this.setState({
      timer: this.timeSec,
    });

    if (this.checkTimer) clearInterval(this.checkTimer);

    // QR 코드 화면 갱신 시간 타이머. 일정 시간 사용 안하면 재조회 후 사용 해야함.(발급 취소 시 QR 데이터 변경 되므로)
    this.checkTimer = setInterval(() => {
      const { timer } = this.state;

      if (timer > 0) {
        this.setState({
          timer: timer - 1,
        });
      } else {
        clearInterval(this.checkTimer);
      }
    }, 1000);
  };

  inputQRPhone = (phoneQRNumber) => {
    this.setState({
      phoneQRNumber,
    });
  };

  checkPhone = () => {
    const {
      item: { qr_key_phone },
      phoneQRNumber,
      isSavePhone,
    } = this.state;

    let isValidPhone = format.toPhone(qr_key_phone) === format.toPhone(phoneQRNumber);

    console.log("- isValidPhone", { isValidPhone, qr_key_phone, phoneQRNumber });

    if (isValidPhone) {
      this.expireTimer();

      if (isSavePhone) {
        localStorage.setItem("phoneQRNumber", phoneQRNumber); // 로컬 쿠키 저장.
      }
    } else {
      Swal.fire({
        icon: "error",
        title: "전봐번호 인증 오류",
        html: `QR 코드키 발급 전화번호가 올바르지 않습니다. <br/><br/><font color="#aaa">인증 오류가 계속 발생하면 카운터에 문의 바랍니다</font>`,
        confirmButtonText: "확인",
      });
    }

    localStorage.setItem("isSavePhone", isSavePhone ? "Y" : "N");

    this.setState({
      isValidPhone,
    });
  };

  handlePhoneSave = (checked) => {
    console.log("- handlePhoneSave", { checked });

    if (!checked) localStorage.removeItem("phoneQRNumber"); // 로컬 쿠키 삭제.

    this.setState({
      isSavePhone: checked,
    });
  };

  componentDidMount = () => {
    this.checkQrCode();

    // 로컬 쿠키 조회
    let phoneQRNumber = localStorage.getItem("phoneQRNumber");
    let isSavePhone = localStorage.getItem("isSavePhone");

    // 최초는 true
    if (!isSavePhone) isSavePhone = true;
    else isSavePhone = isSavePhone === "Y";

    this.setState({
      phoneQRNumber,
      isSavePhone,
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.roomDoorLock && nextProps.roomDoorLock.item) {
      let { item } = nextProps.roomDoorLock;

      let { preferences } = this.props;

      // 업소 환경 설정 조회.
      if (!preferences.id && item.place_id) {
        this.props.initPreferences(item.place_id);
      }

      // 로딩 상태 종료
      this.setState({
        item,
      });
    }
  }

  render() {
    return <QR {...this.props} {...this.state} checkQrCode={this.checkQrCode} inputQRPhone={this.inputQRPhone} checkPhone={this.checkPhone} handlePhoneSave={this.handlePhoneSave} />;
  }
}

export default Container;
