// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as authAction } from "actions/auth";
import { actionCreators as roomDoorLockAction } from "actions/roomDoorLock";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, roomDoorLock, preferences } = state;

  return {
    place,
    auth,
    roomDoorLock,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    doLogout: () => dispatch(authAction.doLogout()),
    getRoomDoorLock: (id) => dispatch(roomDoorLockAction.getRoomDoorLock(id)),
    initPreferences: (palceId) => dispatch(preferencesAction.getPreferenceList(palceId)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
