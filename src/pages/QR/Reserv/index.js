// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as authAction } from "actions/auth";
import { actionCreators as roomReservAction } from "actions/roomReserv";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, roomReserv, preferences } = state;

  return {
    place,
    auth,
    roomReserv,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    doLogout: () => dispatch(authAction.doLogout()),
    getRoomReservQR: (id) => dispatch(roomReservAction.getRoomReservQR(id)),
    initPreferences: (palceId) => dispatch(preferencesAction.getPreferenceList(palceId)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
