// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import _ from "lodash";
import { encrypt, decrypt } from "utils/aes256-util";

// Component.
import QR from "./presenter";

// Styles.
import "./styles.scss";

import logoIcrew from "assets/images/logo_icrew_b.png";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      item: {},
      loading: true,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    roomReserv: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  componentDidMount = () => {
    const { params } = this.props.match;

    let { reserv_id } = params;

    console.log("- reserv_id", reserv_id);

    if (reserv_id) {
      // let hash = encrypt(1111234123222);
      // console.log("- encrypt hash", hash);

      // hash = encodeURIComponent(hash);
      // console.log("- encode hash", hash);

      // hash = decodeURIComponent(hash);
      // console.log("- decode hash", hash);

      // let text = decrypt(hash);
      // console.log("- hash", text);

      reserv_id = decodeURIComponent(reserv_id);

      let id = decrypt(reserv_id);

      console.log("- decipher", id);

      if (id) {
        this.props.getRoomReservQR(Number(id));
      } else {
        // 로딩 상태 종료
        this.setState({
          loading: false,
        });
      }
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.roomReserv && nextProps.roomReserv.item) {
      let { item } = nextProps.roomReserv;

      let { preferences } = this.props;

      // 업소 환경 설정 조회.
      if (!preferences.id && item.place_id) {
        this.props.initPreferences(item.place_id);
      }

      // 로딩 상태 종료
      this.setState({
        item,
        loading: false,
      });
    }
  }

  render() {
    return <QR {...this.props} {...this.state} />;
  }
}

export default Container;
