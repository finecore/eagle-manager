// imports.
import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";

import { FormControl } from "react-bootstrap";
import { envProps } from "config/configureStore";

import QRCode from "qrcode";

let QR = (props) => {
  let { preferences, item } = props;

  let add = preferences.doolock_qr_add || "/upload/add/creweagle_banner00.jpg";

  add = envProps.API_SERVER_URL + add;

  // console.log("- add", add);

  if (item.id) {
    // QRCode.toDataURL(item.reserv_num, function (err, url) {
    //   console.log(url);
    // });

    setTimeout(() => {
      const canvas = document.getElementById("canvas");

      if (canvas) {
        QRCode.toCanvas(canvas, item.reserv_num, function (error) {
          if (error) console.error(error);
          // console.log("success!");
        });
      }
    }, 100);
  }

  return (
    <div className="qr-reserv-wrapper">
      <div className="header">
        <h4>예약번호 QR 코드</h4>
      </div>
      <div className="content">
        <div className="form-group form-radius">
          <div className="qrcode-img">
            {item.id ? (
              <div>
                <p className="place-text">{item.place_name}</p>
                <p className="room-text">{"[" + item.room_type_name + "] " + item.name}</p>

                <canvas id="canvas"></canvas>

                <p className="notice">신분증 투입구에 QR코드를 인식 시켜 주세요</p>
                <p className="content-text">예약번호</p>
                <p className="content-sub-text">{item.reserv_num}</p>
              </div>
            ) : (
              <div>
                <p className="error-text">QR 코드가 올바르지 않습니다</p>
                <p className="alert-text">예약번호를 직접 입력해주세요</p>
              </div>
            )}
          </div>

          <div className="add-area">
            <img src={add} alt="아이크루 스토어" />
          </div>
        </div>
        {item.place_tel && (
          <div className="callcenter">
            <a href={"tel:" + item.place_tel}>Tel. {item.place_tel}</a>
          </div>
        )}
        <div className="copyright">Copyrightⓒ 2021 All rights keyed by iCrew company</div>
      </div>
    </div>
  );
};

export default QR;
