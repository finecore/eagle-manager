import React, { Component } from "react";
import { connect } from "react-redux";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";
import moment from "moment";

class Clock extends Component {
  state = {
    date: null,
    ampm: null,
    hour: null,
    min: null,
    sec: true,
    hhmm: false, // 시간 표기 타입.
    tick: false,
    loading: true,
  };

  componentDidMount() {
    // 로딩 상태 종료
    this.setState({
      loading: false,
    });

    setInterval(() => {
      //console.log("- sec ", this.state.sec);
      this.setState({
        date: moment().format("YYYY년 MM월 DD일 dddd"),
        ampm: moment().format("HH") < 12 ? "오전" : "오후",
        hour: moment().format(this.state.hhmm ? "HH" : "h"),
        min: moment().format("mm"),
        sec: moment().format("ss"),
        tick: !this.state.tick,
      });
    }, 1000);
  }

  render() {
    //let { user } = this.props;

    if (this.state.loading) {
      // 로딩중일때 로더 보여주기
      return <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>;
    }

    return (
      <div onClick={() => this.setState({ hhmm: !this.state.hhmm })}>
        <span className="ampm">{!this.state.hhmm && this.state.ampm}</span>
        <span>
          <span className="hour">{this.state.hour}</span>

          <span className="min">
            <span className={this.state.tick ? "on" : "off"}>:</span>
            {this.state.min}
          </span>

          <span className="sec">
            {/* <span className={this.state.tick ? "on" : "off"}>:</span> */}
            {this.state.sec}
          </span>
        </span>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(Clock);
