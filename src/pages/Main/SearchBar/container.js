// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SearchBar from "./presenter";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
      alert: {
        title: "확인",
        text: "",
        type: "warning", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
      showSimulator: false,
      stayTypeColor: [],
      keyTypeColor: [],
      iscStates: [], // 자판기 상태 목록.
      isc: [], // 자판기
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    modify: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
  };

  timer = null;

  handleMode = (evt, mode) => {
    console.log("- handleMode", mode);

    const {
      preferences: { place_id, day_sale_end_time },
    } = this.props;

    if (mode === 1) {
      let start = moment({ hour: day_sale_end_time, minute: 0 });

      if (start > moment()) {
        // 현재 시간 이전이라면 어제 일자로.
        start = start.add(-1, "day");
      }

      // 오늘 매출 합계 조회.
      this.props.initPlaceRoomSale(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

      // 일일 객실별 매출
      this.props.initPlaceRoomSaleRoom(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));
    }

    this.props.handleViewMode(mode);
  };

  handleFilter = (evt, key, value) => {
    console.log("- handleFilter", key, value);
    let { filter } = this.props.roomView;

    filter[key][value] = !filter[key][value];

    // 키없음 일때 키 제거 값도 셋팅.
    if (key === "key" && value === 0) {
      filter[key][4] = filter[key][5] = filter[key][6] = filter[key][value];
    }

    console.log("- filter", filter);

    this.props.handleViewFilter(filter);
  };

  toggleIscSale = (item) => {
    const { confirm } = this.state;
    const {
      preferences: { speed_checkin },
    } = this.props;

    let { id, sale_stop, name } = item;

    console.log("-- toggleIscSale", name, sale_stop);

    if (!confirm && !speed_checkin) {
      this.setState({
        alert: {
          type: "warning",
          title: "크루무인 설정 확인",
          text: "크루무인 " + name + " 판매를 <font color='red'>[" + (sale_stop === 1 ? "허용" : "중지") + "]</font> 하시겠습니까?",
          callback: () => this.toggleIscSale(item),
          showCancelBtn: true,
          close: false,
        },
        dialog: true,
      });
    } else {
      sale_stop = sale_stop === 0 ? 1 : 0;

      this.props.handleSaveDevice(id, { device: { sale_stop } });

      this.setState({
        dialog: false,
      });
    }
  };

  toggleIscMinorMode = (item, name) => {
    const { confirm } = this.state;
    const {
      preferences: { speed_checkin },
    } = this.props;

    let { serialno, minor_mode } = item;

    console.log("-- toggleIscMinorMode", name, minor_mode);

    if (!confirm && !speed_checkin) {
      this.setState({
        alert: {
          type: "warning",
          title: "성인인증 모드 설정 확인",
          text: "크루무인 " + name + " 성인인증 모드를 <font color='red'>[" + (minor_mode === 1 ? "해제" : "사용") + "]</font> 하시겠습니까?",
          callback: () => this.toggleIscMinorMode(item),
          showCancelBtn: true,
          close: false,
        },
        dialog: true,
      });
    } else {
      let isc_state = { minor_mode: minor_mode === 0 ? 1 : 0 };

      this.props.handleSaveIscState(serialno, { isc_state });

      this.setState({
        dialog: false,
      });
    }
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value, save) => {
    const {
      preferences: { id, place_id },
    } = this.props;

    let modify = {};

    // 아이템 정보 변경.
    modify[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleInputChange name", name, "value", modify[name]);

    this.props.handleSavePreferences(id, { preferences: { place_id, ...modify } });
  };

  onCancel = () => {
    this.closeModal();
  };

  onConfirm = (confirm, value) => {
    console.log("- onConfirm", confirm);

    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(value);

          // 확인 상태 초기화.
          setTimeout(() => {
            this.setState({
              confirm: false,
            });
          }, 100);
        }
      }
    );
  };

  toggleSimulator = () => {
    console.log("- toggleSimulator", !this.state.showSimulator);
    this.setState({
      showSimulator: !this.state.showSimulator,
    });
  };

  clickLayoutToggleButton = (direction) => {
    console.log("- clickLayoutButton::direction", direction);

    let { left, right } = this.props.contents;
    const contents = {
      left: direction === "left" ? !left : left,
      right: direction === "right" ? !right : right,
    };
    this.props.handleLayoutContents(contents);

    console.log("- clickLayoutButton::contents", contents);

    if (direction === "right" && contents.right) {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.props.contents.right = false;
        this.props.handleLayoutContents(this.props.contents);
      }, 6000);
    }
  };

  componentDidMount = () => {
    const { place_id, stay_type_color, key_type_color } = this.props.preferences;

    this.setState({
      stayTypeColor: stay_type_color || [],
      keyTypeColor: key_type_color || [],
      loading: false,
    });

    this.timer = setTimeout(() => {
      this.props.contents.left = true;
      this.props.handleLayoutContents(this.props.contents);
    }, 1000);

    // 무인 자판기 상태 목록 조회.
    if (place_id) this.props.initIscStates(place_id);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      const { place_id, stay_type_color, key_type_color } = nextProps.preferences;

      console.log("- componentWillReceiveProps stay_type_color", stay_type_color);

      let { alert, dialog } = this.state;

      if (dialog) {
        alert.type = "success";
        alert.title = "설정 완료";
        alert.text = "설정이 정상적으로 적용 되었습니다.";
        alert.confirmButtonText = "확인";

        alert.close = true;
        alert.showCancelBtn = false;
        alert.callback = undefined;

        setTimeout(() => {
          let { alert } = this.state;

          if (alert.type === "success") {
            alert.type = "info";

            this.setState({
              alert,
              dialog: false,
              confirm: false,
            });
          }
        }, 2000);

        this.setState({
          alert,
          confirm: false,
        });
      }

      // 무인 자판기 상태 목록 조회.
      this.props.initIscStates(place_id);

      this.setState({
        stayTypeColor: stay_type_color,
        keyTypeColor: key_type_color,
      });
    }

    if (nextProps.device && nextProps.device !== this.props.device) {
      const { isc } = nextProps.device;

      console.log("- isc", isc);

      this.setState({
        isc,
      });
    }

    if (nextProps.iscState && nextProps.iscState !== this.props.iscState) {
      const { list: iscStates } = nextProps.iscState;

      console.log("- iscStates", iscStates);

      this.setState({
        iscStates,
      });
    }
  }

  render() {
    return (
      <SearchBar
        {...this.state}
        {...this.props}
        clickLayoutToggleButton={this.clickLayoutToggleButton}
        handleMode={this.handleMode}
        handleFilter={this.handleFilter}
        toggleSimulator={this.toggleSimulator}
        handleInputChange={this.handleInputChange}
        toggleIscSale={this.toggleIscSale}
        toggleIscMinorMode={this.toggleIscMinorMode}
        onConfirm={this.onConfirm}
        onCancel={this.onCancel}
      />
    );
  }
}

export default withSnackbar(Container);
