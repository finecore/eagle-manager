// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as iscStateAction } from "actions/iscState";
import { actionCreators as deviceAction } from "actions/device";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, layout, preferences, room, roomView, device, error, iscState } = state;
  return {
    auth,
    layout,
    header: layout.header,
    contents: layout.contents,
    preferences: preferences.item,
    modify: preferences.modify,
    room,
    roomView,
    device,
    iscState,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleLayoutContents: (contents) => dispatch(layoutAction.setLayoutContents(contents)),
    handleViewMode: (mode) => dispatch(roomViewAction.setViewMode(mode)),

    initPlaceRoomSale: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSum(placeId, begin, end)),

    initPlaceRoomSaleRoom: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSumRooms(placeId, begin, end)),

    handleViewFilter: (filter) => dispatch(roomViewAction.setViewFilter(filter)),
    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),

    handleSaveDevice: (id, device) => dispatch(deviceAction.putDevice(id, device)),

    initIscStates: (place_id) => dispatch(iscStateAction.getIscStateList(place_id)),
    handleSaveIscState: (serialno, isc_state) => dispatch(iscStateAction.putIscState(serialno, isc_state)),

    handleModifyPreferences: (modify) => dispatch(preferencesAction.setModify(modify)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
