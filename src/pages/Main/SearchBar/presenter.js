import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import cx from "classnames";

import { Form, ButtonToolbar, Button } from "react-bootstrap";

import Simulator from "./Simulator";

import swal from "sweetalert";
import Swal from "sweetalert2";

const SearchBar = (props) => {
  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div className="sub-header">
      <Form inline>
        <ButtonToolbar className="btn-key">
          <Button className={cx(props.roomView.filter.key[0] ? "down" : "up")} onClick={(evt) => props.handleFilter(evt, "key", 0)}>
            <span className={cx("mdi mdi-key-remove")} />
            키없음
          </Button>
          <Button className={cx(props.roomView.filter.key[1] ? "down" : "up")} style={{ background: props.keyTypeColor[1] }} onClick={(evt) => props.handleFilter(evt, "key", 1)}>
            <span className={cx("mdi mdi-key")} />
            손님키
          </Button>
          <Button className={cx(props.roomView.filter.key[2] ? "down" : "up")} style={{ background: props.keyTypeColor[2] }} onClick={(evt) => props.handleFilter(evt, "key", 2)}>
            <span className={cx("mdi mdi mdi-eye")} />
            마스터키
          </Button>
          <Button className={cx(props.roomView.filter.key[3] ? "down" : "up")} style={{ background: props.keyTypeColor[3] }} onClick={(evt) => props.handleFilter(evt, "key", 3)}>
            <span className={cx("mdi mdi-broom")} />
            청소키
          </Button>
        </ButtonToolbar>
        <ButtonToolbar className="btn-room">
          <Button className={cx(props.roomView.filter.sale[0] ? "down" : "up")} style={{ background: props.stayTypeColor[0] }} onClick={(evt) => props.handleFilter(evt, "sale", 0)}>
            공실
          </Button>
          <Button className={cx(props.roomView.filter.sale[2] ? "down" : "up")} style={{ background: props.stayTypeColor[2] }} onClick={(evt) => props.handleFilter(evt, "sale", 2)}>
            대실
          </Button>
          <Button className={cx(props.roomView.filter.sale[1] ? "down" : "up")} style={{ background: props.stayTypeColor[1] }} onClick={(evt) => props.handleFilter(evt, "sale", 1)}>
            숙박
          </Button>
          <Button className={cx(props.roomView.filter.sale[3] ? "down" : "up")} style={{ background: props.stayTypeColor[3] }} onClick={(evt) => props.handleFilter(evt, "sale", 3)}>
            장기
          </Button>
          <Button className={cx(props.roomView.filter.outing[0] ? "down" : "up")} style={{ background: props.stayTypeColor[4] }} onClick={(evt) => props.handleFilter(evt, "outing", 0)}>
            외출
          </Button>
        </ButtonToolbar>
        <ButtonToolbar className="btn-mode">
          <Button className={cx(props.roomView.mode === 0 ? "down" : "up")} onClick={(evt) => props.handleMode(evt, 0)}>
            <span className={cx("mdi mdi-account-check")} />
            객실
          </Button>
          <Button className={cx(props.roomView.mode === 1 ? "down" : "up")} onClick={(evt) => props.handleMode(evt, 1)}>
            <span className={cx("mdi mdi-clipboard-text")} />
            매출
          </Button>
        </ButtonToolbar>

        {props.isc.length ? (
          <ButtonToolbar className="btn-mode">
            {props.preferences.isc_sale === 0 ? (
              props.isc.map((item, key) => (
                <Button key={key} className={cx(item.sale_stop === 0 ? "down isc" : "up isc")} onClick={(evt) => props.toggleIscSale(item)}>
                  <span className={cx("mdi mdi-dice-6")} />
                  {`${item.name} ${item.sale_stop === 0 ? "판매 정상" : "판매 중지"}`}
                </Button>
              ))
            ) : (
              <Button className={cx("up isc")} disabled={true}>
                <span className={cx("mdi mdi-close-circle-outline")} />
                크루무인 사용안함
              </Button>
            )}
          </ButtonToolbar>
        ) : null}

        {props.preferences.isc_sale === 0 && props.isc.length && props.iscStates.length ? (
          <ButtonToolbar className="btn-mode">
            {props.iscStates.map((item, key) => (
              <Button key={key} className={cx(item.minor_mode === 1 ? "down minor_mode" : "up minor_mode")} onClick={(evt) => props.toggleIscMinorMode(item, props.isc[key].name)}>
                <span className={cx("mdi mdi-dice-6")} />
                {`${props.isc[key] ? props.isc[key].name : null} ${item.minor_mode === 1 ? "성인인증 사용" : "성인인증 해제"}`}
              </Button>
            ))}
          </ButtonToolbar>
        ) : null}

        <div className="btn-layout">
          <Simulator showSimulator={props.showSimulator} />
          {/* 개발 시 시뮬레이터 활성화 */}
          {props.auth.user.place_id === 1 && (
            <Button bsSize="xsmall" className="btn-simulator" onClick={(evt) => props.toggleSimulator()}>
              Simulator
            </Button>
          )}
          <ButtonToolbar>
            <Button bsSize="xsmall" className={`left ${props.contents.left ? "on" : "down"}`} onClick={(evt) => props.clickLayoutToggleButton("left")}>
              button
            </Button>
            <Button bsSize="xsmall" className={`right ${props.contents.right ? "on" : "down"}`} onClick={(evt) => props.clickLayoutToggleButton("right")}>
              button
            </Button>
          </ButtonToolbar>
        </div>
      </Form>
      ;
    </div>
  );
};

const mapStateToProps = (state) => ({});

export default connect(withRouter(mapStateToProps))(SearchBar);
