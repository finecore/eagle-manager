// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import { sign } from "utils/jwt-util";

import _ from "lodash";

// Components.
import Gnb0102 from "./presenter";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      room_id: 0,
      checked: { key: true, door: false, emlock: false },
      room_state: { key: 0, door: 0, emlock: 0 },
    };
  }

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  onSelectRoom = (room_id) => {
    const {
      roomState: { list },
    } = this.props;

    let room_state = _.find(list, { room_id: Number(room_id) });

    if (room_state) {
      console.log("- onSelectRoom", room_id, room_state);

      localStorage.setItem(`simulator_room_id`, room_id);

      this.setState({
        room_state,
        room_id,
      });
    }
  };

  // 동작 원리??
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  handleCheckChange = (name) => {
    console.log("- handleCheckChange", name);

    let { checked } = this.state;

    checked[name] = !checked[name];

    this.setState({
      checked,
    });
  };

  handleDataChange = (name, value) => {
    console.log("- handleDataChange", name, value);

    let { room_state } = this.state;

    room_state[name] = value;

    this.setState({
      room_state,
    });
  };

  handleRun = () => {
    const { room_id, token, checked } = this.state;

    let room_state = {};

    _.map(checked, (v, k) => {
      if (v !== null || v !== undefined) room_state[k] = this.state.room_state[k];
    });

    if (room_state["key"]) {
      room_state["main_relay"] = Number(room_state["key"]) < 4 ? 1 : 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
    }

    room_state.headers = {
      channel: "device",
      token,
      serialno: "00000001",
    };

    console.log("- handleRun", room_id, room_state);

    if (room_id) {
      // 객실 상태.
      this.props.handleSaveRoomState(room_id, {
        room_state,
      });
    } else {
      alert("테스트 할 객실을 선택해 주세요.");
    }
  };

  componentDidMount() {
    // JWT(Json Web Token) 생성.
    const payload = {
      channel: "device",
      id: 1,
      place_id: 1,
      type: "01",
      serialno: "00000001",
    };

    sign(payload, (err, token) => {
      this.setState(
        {
          token,
          loading: false,
        },
        () => {
          // 이전 설정 객실 셋팅.
          setTimeout(() => {
            let room_id = localStorage.getItem(`simulator_room_id`);
            if (room_id) this.onSelectRoom(Number(room_id));
          }, 1000);
        }
      );
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { room } = this.props;

    // 객실 상태 변경.
    if (nextProps.roomState && nextProps.roomState !== this.props.roomState && nextProps.roomState.list !== this.props.roomState.list) {
      const { list } = nextProps.roomState;

      const room_state = _.find(list, { room_id: room.id });

      // console.log("- Simulator roomState componentWillReceiveProps", room_state);

      this.setState(
        {
          room_state: _.merge({}, this.state.room_state, room_state),
        },
        () => {
          console.log("- state", this.state.room_state);
        }
      );
    }
  }

  render() {
    return (
      <Gnb0102
        {...this.props}
        {...this.state}
        onSelectRoom={this.onSelectRoom}
        closeModal={this.closeModal}
        handleCheckChange={this.handleCheckChange}
        handleDataChange={this.handleDataChange}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        handleRun={this.handleRun}
        handleTerm={this.handleTerm}
      />
    );
  }
}

export default withSnackbar(Container);
