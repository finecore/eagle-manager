// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as layoutAction } from "actions/layout";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, room, roomState, preferences } = state;

  return {
    user: auth.user,
    room,
    roomState,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomState: (roomId) => dispatch(roomStateAction.getRoomState(roomId)),

    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
