// imports.
import React from "react";

//room-popup
import { Table, FormControl, Radio, Checkbox, Button } from "react-bootstrap";

import _ from "lodash";

// styles.
import "./styles.scss";

// functional component
const Control = (props) => {
  return (
    <article className={"simulator-popup " + (props.showSimulator ? "open" : "")}>
      <div className="popup-inner">
        <h1>CCU 시뮬레이터</h1>
        <ul className="list-bul">
          <li>
            <div>
              <FormControl componentClass="select" placeholder="객실선택" value={props.room_id} onChange={(evt) => props.onSelectRoom(Number(evt.target.value))}>
                <option value="">객실선택</option>
                {props.room.list.map((room, k) => (
                  <option key={k} value={room.id}>
                    {room.name}
                  </option>
                ))}
              </FormControl>
            </div>
            <div className="form-content">
              <div className="table-simulatro">
                <Table>
                  <thead>
                    <tr>
                      <th>이름</th>
                      <th>상태</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>key</td>
                      <td>
                        <Checkbox inline checked={props.checked.key} onChange={(evt) => props.handleCheckChange("key")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="keyRadio"
                          id="keyRadio01"
                          value={0}
                          checked={props.room_state.key === 0 || props.room_state.key > 3}
                          onChange={(evt) => props.handleDataChange("key", props.room_state.key + 3)}
                        >
                          <span />
                          OUT
                        </Radio>
                        <Radio
                          inline
                          name="keyRadio"
                          id="keyRadio02"
                          value={1}
                          checked={props.room_state.key === 1}
                          disabled={props.room_state.key > 0 && props.room_state.key < 4}
                          onChange={(evt) => props.handleDataChange("key", Number(evt.target.value))}
                        >
                          <span />
                          고객키 IN
                        </Radio>
                        <Radio
                          inline
                          name="keyRadio"
                          id="keyRadio03"
                          value={3}
                          checked={props.room_state.key === 3}
                          disabled={props.room_state.key > 0 && props.room_state.key < 4}
                          onChange={(evt) => props.handleDataChange("key", Number(evt.target.value))}
                        >
                          <span />
                          청소키 IN
                        </Radio>
                        <Radio
                          inline
                          name="keyRadio"
                          id="keyRadio04"
                          value={2}
                          checked={props.room_state.key === 2}
                          disabled={props.room_state.key > 0 && props.room_state.key < 4}
                          onChange={(evt) => props.handleDataChange("key", Number(evt.target.value))}
                        >
                          <span />
                          마스터키 IN
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>power</td>
                      <td>
                        <Checkbox inline checked={props.checked.main_relay} onChange={(evt) => props.handleCheckChange("main_relay")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="mainRadio"
                          id="mainRadio01"
                          value={0}
                          checked={Number(props.room_state.main_relay) === 0}
                          onChange={(evt) => props.handleDataChange("main_relay", Number(evt.target.value))}
                        >
                          <span />
                          전원 ON
                        </Radio>
                        <Radio
                          inline
                          name="mainRadio"
                          id="mainRadio02"
                          value={1}
                          checked={Number(props.room_state.main_relay) === 1}
                          onChange={(evt) => props.handleDataChange("main_relay", Number(evt.target.value))}
                        >
                          <span />
                          전원 OFF
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>에어컨</td>
                      <td>
                        <Checkbox inline checked={props.checked.air_power} onChange={(evt) => props.handleCheckChange("air_power")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="airRadio"
                          id="airRadio01"
                          value={0}
                          checked={Number(props.room_state.air_power) === 0}
                          onChange={(evt) => props.handleDataChange("air_power", Number(evt.target.value))}
                        >
                          <span />
                          전원 ON
                        </Radio>
                        <Radio
                          inline
                          name="airRadio"
                          id="airRadio02"
                          value={1}
                          checked={Number(props.room_state.air_power) === 1}
                          onChange={(evt) => props.handleDataChange("air_power", Number(evt.target.value))}
                        >
                          <span />
                          전원 OFF
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>door</td>
                      <td>
                        <Checkbox inline checked={props.checked.door} onChange={(evt) => props.handleCheckChange("door")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="doorRadio"
                          id="doorRadio01"
                          value={0}
                          checked={Number(props.room_state.door) === 0}
                          onChange={(evt) => props.handleDataChange("door", Number(evt.target.value))}
                        >
                          <span />
                          닫힘
                        </Radio>
                        <Radio
                          inline
                          name="doorRadio"
                          id="doorRadio02"
                          value={1}
                          checked={Number(props.room_state.door) === 1}
                          onChange={(evt) => props.handleDataChange("door", Number(evt.target.value))}
                        >
                          <span />
                          열림
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>emlock</td>
                      <td>
                        <Checkbox inline checked={props.checked.emlock} onChange={(evt) => props.handleCheckChange("emlock")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="emlockRadio"
                          id="emlockRadio01"
                          value={0}
                          checked={props.room_state.emlock === 0}
                          onChange={(evt) => props.handleDataChange("emlock", Number(evt.target.value))}
                        >
                          <span />
                          잠김
                        </Radio>
                        <Radio
                          inline
                          name="emlockRadio"
                          id="emlockRadio02"
                          value={1}
                          checked={props.room_state.emlock === 1}
                          onChange={(evt) => props.handleDataChange("emlock", Number(evt.target.value))}
                        >
                          <span />
                          열림
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>차량호출</td>
                      <td>
                        <Checkbox inline checked={props.checked.car_call} onChange={(evt) => props.handleCheckChange("car_call")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="car_callRadio"
                          id="car_callRadio01"
                          value={0}
                          checked={props.room_state.car_call === 0}
                          onChange={(evt) => props.handleDataChange("car_call", Number(evt.target.value))}
                        >
                          <span />
                          {props.room_state.car_call === 0 ? "없음" : "취소"}
                        </Radio>
                        <Radio
                          inline
                          name="car_callRadio"
                          id="car_callRadio02"
                          value={1}
                          checked={props.room_state.car_call !== 0}
                          onChange={(evt) => props.handleDataChange("car_call", Number(evt.target.value))}
                        >
                          <span />
                          호출
                        </Radio>
                      </td>
                    </tr>
                    <tr>
                      <td>비상호출</td>
                      <td>
                        <Checkbox inline checked={props.checked.emerg} onChange={(evt) => props.handleCheckChange("emerg")}>
                          <span />
                          전송
                        </Checkbox>
                        <div className="separator" />
                        <Radio
                          inline
                          name="emergRadio"
                          id="emergRadio01"
                          value={0}
                          checked={props.room_state.emerg === 0}
                          onChange={(evt) => props.handleDataChange("emerg", Number(evt.target.value))}
                        >
                          <span />
                          {props.room_state.emerg === 0 ? "없음" : "취소"}
                        </Radio>
                        <Radio
                          inline
                          name="emergRadio"
                          id="emergRadio02"
                          value={1}
                          checked={props.room_state.emerg !== 0}
                          onChange={(evt) => props.handleDataChange("emerg", Number(evt.target.value))}
                        >
                          <span />
                          호출
                        </Radio>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td />
                      <td>
                        <Button className="btn-run" onClick={(evt) => props.handleRun(evt)}>
                          실행
                        </Button>
                      </td>
                    </tr>
                  </tfoot>
                </Table>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </article>
  );
};

export default Control;
