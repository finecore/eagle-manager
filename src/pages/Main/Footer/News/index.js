import React, { Component } from "react";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
// import PropTypes from "prop-types";

import { ButtonToolbar, Button } from "react-bootstrap";

class News extends Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.setState({
      loading: false,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <article className={"allnews-popup" + this.props.className} onMouseLeave={(evt) => this.props.closePopup()}>
        <div className="popup-inner">
          <h1>숙박뉴스</h1>
          <ul>
            {this.props.curr && (
              <li>
                <b>
                  <a href={this.props.curr.link} target="_blank" rel="noopener noreferrer">
                    {this.props.curr.title}
                  </a>
                </b>
                <span>{this.props.curr.pubDate}</span>
                <br />
                {this.props.curr.content}
              </li>
            )}
          </ul>
          <ButtonToolbar className="btn-control">
            <Button className={"btn-prev" + this.props.hidePrevClassName} onClick={(evt) => this.props.prevNews()}>
              &lt;
            </Button>
            <Button className={"btn-next" + this.props.hideNextClassName} onClick={(evt) => this.props.nextNews()}>
              &gt;
            </Button>
          </ButtonToolbar>
          <Button className="btn-close" onClick={(evt) => this.props.closePopup()}>
            닫기
          </Button>
        </div>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(News));
