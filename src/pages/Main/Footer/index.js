import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

import { actionCreators as authAction } from "actions/auth";
import { actionCreators as rssAction } from "actions/rss";
import { actionCreators as noticeAction } from "actions/notice";
import { actionCreators as asAction } from "actions/as";
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as deviceAction } from "actions/device";

import { withSnackbar } from "notistack";
import CustomizedStackSnackbar from "components/Notification/CustomizedStackSnackbar";

import { Form, ButtonToolbar, Button } from "react-bootstrap";
import Notice from "./Notice";
import News from "./News";
import Slider from "react-slick";
import VolumeUp from "@material-ui/icons/VolumeUp";
import Language from "@material-ui/icons/Language";
import Phone from "@material-ui/icons/SettingsPhone";

import { keyToValue } from "constants/key-map";

import cx from "classnames";

import Gnb0501 from "pages/Setting/Gnb05/Gnb0501";
import Gnb0503 from "pages/Setting/Gnb05/Gnb0503";
import Gnb0401 from "../../Setting/Gnb04/Gnb0401";

import "./styles.scss";

class Footer extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    network: PropTypes.object.isRequired,
    device: PropTypes.object.isRequired,
    rss: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      notice1: [],
      notice2: [],
      news: [],
      show_notice: false,
      show_news: false,
      selPrev: {},
      selCurr: {},
      selNext: {},
      status: { SOCKET: true, API: true },
      devices: [],
      controlNum: null,
      hidePrevButton: false,
      hideNextButton: false,
      width: 6500,
      isAlertId: null,
    };

    this.timer = null;
    this.timer2 = null;
    this._isMounted = false;

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);

    this.shows = [];
    this.reservSnackBar = [];
  }

  play() {
    this.slider.slickPlay();
  }
  pause() {
    this.slider.slickPause();
  }

  clickDetail(name, item) {
    if (name === "notice") {
      const { id, mod_date, reg_date } = item;

      console.log("- clickDetail", { name, id, mod_date, reg_date });

      let lastDate = moment(mod_date || reg_date).format("YYYYMMDDHHmmss");
      localStorage.setItem(`${name}_display_${id}`, lastDate);
      localStorage.setItem(`${name}_display_${id}_today`, moment().format("YYYYMMDD"));

      this.props.handleDisplayLayer(Gnb0501, { item });
    } else {
      const { id, action_date } = item;

      console.log("- clickDetail", { name, id, action_date });

      let lastDate = moment(action_date).format("YYYYMMDDHHmmss");
      localStorage.setItem(`${name}_display_${id}`, lastDate);

      this.props.handleDisplayLayer(Gnb0503, { item });
    }
  }

  togglePopup(type, inx, n) {
    let { news, notice } = this.state;
    let list = type === "notice" ? notice : news;

    console.log("- togglePopup", type, inx, n);

    if (inx === "next") {
      this._isMounted &&
        this.setState({
          show_notice: type === "notice",
          show_news: type === "news",
          selCurr: list[n],
          controlNum: n,
          hidePrevButton: n === 0 ? true : null,
          hideNextButton: n === list.length - 1 ? true : null,
        });
    } else if (inx === "prev") {
      this._isMounted &&
        this.setState({
          show_notice: type === "notice",
          show_news: type === "news",
          selCurr: list[n],
          controlNum: n,
          hidePrevButton: n === 0 ? true : null,
          hideNextButton: n === list.length - 1 ? true : null,
        });
    } else {
      this._isMounted &&
        this.setState({
          show_notice: type === "notice",
          show_news: type === "news",
          selPrev: list[inx - 1] || null,
          selCurr: list[inx],
          selNext: list[inx + 1] || null,
          controlNum: inx,
          hidePrevButton: inx <= 1 ? true : null,
          hideNextButton: inx >= list.length - 1 ? true : null,
        });
    }
  }

  handleClose() {
    this._isMounted &&
      this.setState({
        show_notice: false,
        show_news: false,
      });
  }

  handleShow = (error, info) => {};

  rssFeed() {
    // 전체기사
    // http://www.sukbakmagazine.com/rss/allArticle.xml
    // 인기기사
    // http://www.sukbakmagazine.com/rss/clickTop.xml
    // 중앙회뉴스
    // http://www.sukbakmagazine.com/rss/S1N1.xml
    // 숙박뉴스
    // http://www.sukbakmagazine.com/rss/S1N2.xml
    // 기획·특집
    // http://www.sukbakmagazine.com/rss/S1N3.xml
    // 이달의숙박인
    // http://www.sukbakmagazine.com/rss/S1N4.xml
    // 트렌드
    // http://www.sukbakmagazine.com/rss/S1N5.xml
    // BIZ
    // http://www.sukbakmagazine.com/rss/S1N6.xml

    this._isMounted && this.props.handleGetRss("http://www.sukbakmagazine.com/rss/clickTop.xml");
  }

  componentDidMount() {
    this._isMounted = true;

    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    // 3달전 부터 등록 공지중 현재 공개 일자에 포함된것만 조회.
    let between = "a.reg_date";
    let begin = moment().add("month", -3).format("YYYY-MM-DD HH:mm");
    let end = moment().add("day", 1).format("YYYY-MM-DD HH:mm");
    let filter = `(a.place_id=0 or a.place_id=${place_id}) and open=1`;

    // 1일 이내 응답 업데이트가 있는 AS건의만 조회.
    let between2 = "a.action_date";
    let begin2 = moment().add("day", -1).format("YYYY-MM-DD HH:mm");
    let end2 = moment().add("day", 1).format("YYYY-MM-DD HH:mm");
    let filter2 = `a.place_id=${place_id} and a.action_date is not null and open=1 `;

    setTimeout(() => {
      // 공지 사항.(알림 목록에 저장)
      this.props.initNotices(filter, between, begin, end, true);

      // AS/건의 응답(알림 목록에 저장)
      this.props.initAs(filter2, between2, begin2, end2, true);

      // 숙박뉴스
      this.rssFeed();
    }, 3000);

    if (this.timer) clearInterval(this.timer);
    if (this.timer2) clearInterval(this.timer2);

    // 1 분만다 재조회.
    this.timer = setInterval(() => {
      console.log("- timer", moment().format("HH:mm:ss"));
      this.props.initNotices(filter, between, begin, end, true);
      this.props.initAs(filter2, between2, begin2, end2, true);
      // 장비 조회.
      this.props.initDevices(place_id);
    }, 60 * 1000);

    // 10 분만다 재조회.
    this.timer2 = setInterval(() => {
      this.rssFeed();
    }, 10 * 60 * 1000);
  }

  componentWillUnmount = () => {
    if (this.timer) clearInterval(this.timer);
    if (this.timer2) clearInterval(this.timer2);

    this._isMounted = false;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.network && nextProps.network !== this.props.network && nextProps.network.status !== this.props.network.status) {
      const { status } = nextProps.network;

      this._isMounted &&
        this.setState({
          status,
        });
    }

    if (nextProps.device && nextProps.device !== this.props.device && nextProps.device.list !== this.props.device.list) {
      const { list } = nextProps.device;

      this._isMounted &&
        this.setState({
          devices: list,
        });
    }

    if (nextProps.notice && nextProps.notice !== this.props.notice && nextProps.notice.notifi !== this.props.notice.notifi) {
      const { notifi } = nextProps.notice;

      console.log("- notice notifi ", notifi);

      // 알림 목록
      let displays = _.filter(notifi, { display: 1 });

      _.each(displays, (item, key) => {
        const { id, type, title, display_time, display_repeat, reg_date, mod_date } = item;

        let hhmm = moment(display_time, "HH:mm");

        // console.log("- display_time", hhmm.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

        if (hhmm.isSameOrBefore(moment())) {
          const { enqueueSnackbar } = this.props;
          const name = "notice";

          // 현재 팝업 여부.
          let isShow = _.find(this.shows, { name, id });

          // 최종 수정 후 공지 여부.
          let lastDate = moment(mod_date || reg_date).format("YYYYMMDDHHmmss");
          let saveDate = localStorage.getItem(`${name}_display_${id}`);
          let showToday = localStorage.getItem(`${name}_display_${id}_today`); // 오늘 반복 알림 여부.

          let displayed = saveDate ? lastDate === saveDate : false; // 반복 시 오늘 알림 없으면 다시 알림.

          let today = moment().format("YYYYMMDD");
          let isRepeat = false;

          // 일일 반복 시 오늘 팝업 여부
          if (display_repeat && displayed && today !== showToday) {
            displayed = false;
            isRepeat = true;
          }

          console.log("- display", { displayed, isRepeat, title, isShow, lastDate, saveDate });

          if (!isShow && !displayed) {
            this.shows.push({ name, id });

            CustomizedStackSnackbar({
              variant: type === 1 ? "info" : "warning", // 타입(1: 공지사항, 2: 업데이트 사항, 3: 기타
              message: title,
              detail: "",
              handleDismiss: (data, show) => {
                console.log("-display handleDismiss", { data, show });

                const { name, id, date, isRepeat } = data;

                if (show) {
                  // 일일 반복 아닐때만 저장.
                  if (!isRepeat) {
                    // 알림 확인.
                    let lastDate = moment(date).format("YYYYMMDDHHmmss");
                    localStorage.setItem(`${name}_display_${id}`, lastDate);
                  }

                  localStorage.setItem(`${name}_display_${id}_today`, moment().format("YYYYMMDD"));

                  // 공지 게시판 열기.
                  this.props.handleDisplayLayer(Gnb0501, { item });
                } else {
                  this.shows = _.filter(this.shows, (v) => v.name !== name || v.id !== id);
                  console.log("-display shows", this.shows);
                }
              },
              enqueueSnackbar,
              anchorOrigin: {
                vertical: "bottom",
                horizontal: "right",
              },
              linkButton: true,
              autoHideDuration: 30000,
              callbackData: { name, id, date: mod_date || reg_date, isRepeat },
            });
          }
        }
      });

      this._isMounted &&
        this.setState({
          notice1: notifi,
        });
    }

    if (nextProps.as && nextProps.as !== this.props.as && nextProps.as.notifi !== this.props.as.notifi) {
      const { notifi } = nextProps.as;

      console.log("- as notifi ", notifi);

      _.each(notifi, (item, key) => {
        const { id, title, action_date } = item;
        const { enqueueSnackbar } = this.props;

        const name = "as";

        // 현재 팝업 여부.
        let isShow = _.find(this.shows, { name, id });

        // 조치 일시 체크.
        let lastDate = moment(action_date).format("YYYYMMDDHHmmss");
        let date = localStorage.getItem(`${name}_display_${id}`);
        const displayed = date ? lastDate === date : false; // 반복 시 오늘 알림 없으면 다시 알림.

        console.log("- display", { displayed, title, isShow, lastDate, date });

        if (!isShow && !displayed) {
          this.shows.push({ name, id });

          CustomizedStackSnackbar({
            variant: "success",
            message: title,
            detail: "",
            handleDismiss: (data, show) => {
              console.log("-display handleDismiss", { data, show });

              const { name, id, action_date } = data;

              if (show) {
                // 알림 확인.
                let lastDate = moment(action_date).format("YYYYMMDDHHmmss");
                localStorage.setItem(`${name}_display_${id}`, lastDate);

                // 공지 게시판 열기.
                this.props.handleDisplayLayer(Gnb0503, { item });
              } else {
                this.shows = _.filter(this.shows, (v) => v.name !== name || v.id !== id);
              }
            },
            enqueueSnackbar,
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "right",
            },
            linkButton: true,
            autoHideDuration: 30000,
            callbackData: { name, id, action_date },
          });
        }
      });

      this._isMounted &&
        this.setState({
          notice2: notifi,
        });
    }

    // 객실 예약 목록.
    if (nextProps.roomReserv && nextProps.roomReserv !== this.props.roomReserv && nextProps.roomReserv.item !== this.props.roomReserv.item) {
      const { item, websocket } = nextProps.roomReserv;
      const { enqueueSnackbar, closeSnackbar } = this.props;
      let { isAlertId } = this.state;

      console.log("- room reserv", item, websocket);

      let {
        id,
        type, // 0: 등록, 1: 변경, 2: 취소
        place_id,
        room_type_id,
        room_type_name,
        room_id,
        room_name,
        ota_code,
        stay_type,
        state,
        check_in_exp,
        check_out_exp,
        reserv_date,
        name,
        hp,
        reserv_num,
        room_fee,
        reserv_fee,
        prepay_ota_amt,
        prepay_cash_amt,
        prepay_card_amt,
        memo,
      } = item;

      if (websocket && id !== isAlertId) {
        this.setState(
          {
            isAlertId: id,
          },
          () => {
            let message = `[${keyToValue("room_reserv", "ota_code", ota_code)}] ${keyToValue("room_sale", "stay_type", stay_type)} ${keyToValue("room_reserv", "type", type)} | ${
              room_type_name ? room_type_name : "객실타입 미배정"
            } | ${room_name ? room_name : "객실 미배정"}`;

            let detail = `\n${name} 님 | 예약일: ${moment(check_in_exp).format("MM/DD HH:mm")} ~ ${moment(check_out_exp).format("MM/DD HH:mm")}`;

            const key = CustomizedStackSnackbar({
              variant: "info",
              message,
              detail: JSON.stringify(detail),
              handleDismiss: (data, show) => {
                if (show) {
                  // 예약 게시판 열기.
                  this.props.handleDisplayLayer(Gnb0401, { item });
                  _.each(this.reservSnackBar, (key) => {
                    console.log("- reservSnackBar key", key);
                    closeSnackbar(key);
                  });
                  this.reservSnackBar = [];
                } else {
                  closeSnackbar();
                }

                this.setState({
                  isAlertId: null,
                });
              },
              enqueueSnackbar,
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
              linkButton: true,
              autoHideDuration: 60 * 60 * 1000, // 1 시간표시.
              callbackData: { name, id },
            });

            this.reservSnackBar.push(key);
          }
        );
      }
    }

    if (nextProps.rss && nextProps.rss !== this.props.rss) {
      const { feed } = nextProps.rss;

      this._isMounted &&
        this.setState({
          news: feed.items,
        });
    }
  }

  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      vertical: true,
      verticalSwiping: true,
    };

    return (
      <div className="footer-wrap">
        <Form inline>
          <ButtonToolbar className="btn-option">
            <Button className={cx(this.state.status.SOCKET ? "on" : "off")} onClick={(evt) => this.handleShow(1)}>
              SOCKET
            </Button>
            <Button className={cx(this.state.status.API ? "on" : "off")} onClick={(evt) => this.handleShow(2)}>
              API
            </Button>
            {_.map(this.state.devices, (device, i) => (
              <Button key={i} className={cx(device.connect === 0 ? "on" : "off")} onClick={(evt) => this.handleShow(3)}>
                {device.name}
              </Button>
            ))}
          </ButtonToolbar>
        </Form>

        <div className="info-box notice-box">
          <div className="notice-title">
            <VolumeUp />
          </div>
          <div className="ul">
            <Slider ref={(slider) => (this.slider = slider)} {...settings}>
              {_.map(this.state.notice1, (item, k) => (
                <span className={cx("li")} key={k} onClick={(evt) => this.clickDetail("notice", item)} onMouseEnter={this.pause} onMouseLeave={this.play}>
                  [{item.type === 1 ? "공지사항" : "업데이트"}] <span className={cx(item.important === 2 ? "important" : "")}>{item.important === 2 ? "중요" : ""} </span> {item.title}
                </span>
              ))}
              {_.map(this.state.notice2, (item, k) => (
                <span className={cx("li")} key={k} onClick={(evt) => this.clickDetail("as", item)} onMouseEnter={this.pause} onMouseLeave={this.play}>
                  [AS조치] {item.title}
                </span>
              ))}
            </Slider>
          </div>
        </div>

        <div className="info-box news-box">
          <div className="news-title">
            <Language />
          </div>
          <div className="ul">
            <Slider ref={(slider) => (this.slider = slider)} {...settings}>
              {_.map(this.state.news, (item, k) => (
                <span className="li" key={k} onClick={(evt) => this.togglePopup("news", k)} onMouseEnter={this.pause} onMouseLeave={this.play}>
                  {item.title}
                </span>
              ))}
            </Slider>
          </div>
          <div className="call-center">
            <Phone /> <span>1600-5356</span>
          </div>
        </div>

        <Notice
          className={this.state.show_notice ? " open" : ""}
          prev={this.state.selPrev}
          curr={this.state.selCurr}
          next={this.state.selNext}
          closePopup={(evt) => this.handleClose()}
          hidePrevClassName={this.state.hidePrevButton ? " disable" : ""}
          hideNextClassName={this.state.hideNextButton ? " disable" : ""}
          prevNews={(evt) => this.togglePopup("notice", "prev", this.state.controlNum - 1)}
          nextNews={(evt) => this.togglePopup("notice", "next", this.state.controlNum + 1)}
        />

        <News
          className={this.state.show_news ? " open" : ""}
          prev={this.state.selPrev}
          curr={this.state.selCurr}
          next={this.state.selNext}
          closePopup={(evt) => this.handleClose()}
          hidePrevClassName={this.state.hidePrevButton ? " disable" : ""}
          hideNextClassName={this.state.hideNextButton ? " disable" : ""}
          prevNews={(evt) => this.togglePopup("news", "prev", this.state.controlNum - 1)}
          nextNews={(evt) => this.togglePopup("news", "next", this.state.controlNum + 1)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { preferences, layout, auth, placeLock, roomView, roomReserv, network, device, notice, as, rss } = state;
  return {
    auth,
    network,
    device,
    preferences: preferences.item,
    stopLoad: preferences.stopLoad,
    layout,
    contents: layout.contents,
    user: auth.user,
    roomView,
    roomReserv,
    placeLock,
    notice,
    as,
    rss,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleLogout: () => dispatch(authAction.doLogout()),
    initNotices: (place_id, between, begin, end, isNotifi) => dispatch(noticeAction.getNoticeList(place_id, between, begin, end, isNotifi)),
    initAs: (place_id, between, begin, end, isNotifi) => dispatch(asAction.getAsList(place_id, between, begin, end, isNotifi)),
    initDevices: (place_id) => dispatch(deviceAction.getDeviceList(place_id)),
    handleGetRss: (url) => dispatch(rssAction.getRss(url)),
    handleDisplayLayer: (displayLayer, props) => dispatch(layoutAction.setDisplayLayer(displayLayer, props)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Footer));
