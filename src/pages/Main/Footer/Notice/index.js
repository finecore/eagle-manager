import React, { Component } from "react";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
// import PropTypes from "prop-types";

import { ButtonToolbar, Button } from "react-bootstrap";

import { actionCreators as layoutAction } from "actions/layout";
import Gnb0501 from "pages/Setting/Gnb05/Gnb0501";

class Notice extends Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      loading: true,
      timer: null,
    };
  }

  onTimeoutClose() {
    this.timer = setTimeout(() => {
      this.props.closePopup();
    }, 2000);
  }

  onTimeoutCancel() {
    if (this.timer) clearTimeout(this.timer);
  }

  handleClick(item) {
    console.log("- handleClick", item);

    // 공지 게시판 열기.
    this.props.handleDisplayLayer(Gnb0501, { item });
  }

  componentDidMount() {
    this.setState({
      loading: false,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <article className={"allnews-popup" + this.props.className} onMouseEnter={(evt) => this.onTimeoutCancel()} onMouseLeave={(evt) => this.onTimeoutClose()}>
        <div className="popup-inner">
          <h1>공지 알림</h1>
          <ul>
            {this.props.curr && (
              <li>
                <p>
                  <a href="#none" onClick={(evt) => this.handleClick(this.props.curr)}>
                    <b>{this.props.curr.title}</b>
                  </a>
                </p>
                <span>{this.props.curr.pubDate}</span>
                <br />
                <div draggable={false} className="content full" dangerouslySetInnerHTML={{ __html: this.props.curr.content }} />
              </li>
            )}
          </ul>
          <ButtonToolbar className="btn-control">
            <Button className={"btn-prev" + this.props.hidePrevClassName} onClick={(evt) => this.props.prevNews()}>
              &lt;
            </Button>
            <Button className={"btn-next" + this.props.hideNextClassName} onClick={(evt) => this.props.nextNews()}>
              &gt;
            </Button>
          </ButtonToolbar>
          <Button className="btn-close" onClick={(evt) => this.props.closePopup()}>
            닫기
          </Button>
        </div>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return { handleDisplayLayer: (displayLayer, props) => dispatch(layoutAction.setDisplayLayer(displayLayer, props)) };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Notice));
