// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomTypeAction } from "actions/roomType";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";
import { actionCreators as seasonPremiumAction } from "actions/seasonPremium";
import { actionCreators as roomReservAction } from "actions/roomReserv";
import { actionCreators as roomTheme } from "actions/roomTheme";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as timeOptionAction } from "actions/timeOption";
import { actionCreators as placeSubscribeAction } from "actions/placeSubscribe";
import { actionCreators as subscribeAction } from "actions/subscribe";
import { actionCreators as roomDoorLockAction } from "actions/roomDoorLock";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, layout, preferences, roomTheme, roomView, roomState, roomSale, roomDoorLock, timeOption, placeSubscribe, subscribe, room } = state;

  return {
    auth,
    layout,
    preferences: preferences.item,
    stopLoad: preferences.stopLoad,
    roomTheme,
    roomView,
    roomState,
    roomSale,
    roomDoorLock,
    timeOption,
    placeSubscribe,
    subscribe,
    room,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRooms: (palceId, callback) => dispatch(roomAction.getRoomList(palceId, callback)),
    initAllRoomTypes: (palceId) => dispatch(roomTypeAction.getRoomTypeList(palceId)),
    initAllRoomSales: (palceId) => dispatch(roomSaleAction.getNowRoomSales(palceId)),
    initAllRoomStates: (palceId) => dispatch(roomStateAction.getRoomStateList(palceId)),
    initAllRoomReservs: (palceId) => dispatch(roomReservAction.getNowRoomReservs(palceId)),
    initAllRoomDoorLock: (palceId) => dispatch(roomDoorLockAction.getRoomDoorLockList(palceId)),
    initAllSeasonPremium: (palceId) => dispatch(seasonPremiumAction.getSeasonPremiumList(palceId)),
    initAllRoomViews: (palceId) => dispatch(roomViewAction.getRoomViewList(palceId)),
    initAllRoomViewItems: (palceId) => dispatch(roomViewItemAction.getPlaceRoomViewItemList(palceId)),
    initAllSubscribes: () => dispatch(subscribeAction.getSubscribeList()),
    initAllPlaceSubscribes: (palceId) => dispatch(placeSubscribeAction.getPlaceSubscribeList(palceId)),
    // initPlaceEnableReservs: placeId => dispatch(roomReservAction.getPlaceEnableReservs(placeId)),
    initRoomReservs: (filter, begin, end) => dispatch(roomReservAction.getRoomReservs(filter, begin, end)),
    initTimeOptions: (palceId) => dispatch(timeOptionAction.getTimeOptions(palceId)),
    initRoomThemes: () => dispatch(roomTheme.getRoomThemeList()),
    updateRoomViewSelected: (selected) => dispatch(roomViewAction.setRoomViewSelected(selected)),
    addRoomView: (palceId, room_view) => dispatch(roomViewAction.newRoomView(palceId, room_view)),
    setStopReload: (stop) => dispatch(preferencesAction.setStopReload(stop)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
