// imports.
import React from "react";
import { Tabs, Tab } from "react-bootstrap";

import { FormControl, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";
import Alert from "sweetalert-react";
import InputNumber from "rc-input-number";

// Components.
import RoomView from "./RoomView";

// Styles.
import "./styles.scss";

const Contents = (props) => (
  <Tabs defaultActiveKey={0} id="plan-text-tabs" transition="false" onSelect={(key) => props.onSelect(key)} activeKey={props.roomView.selected} alt="">
    {props.roomView.list &&
      props.roomView.list.map((view, key) => (
        <Tab transition="false" eventKey={key} key={key} title={view.title} alt="">
          {props.roomView.selected === key && <RoomView view={view} tabIdx={key} alt="" />}
        </Tab>
      ))}

    {/* 뷰 추가 */}
    <Tab transition="false" eventKey={99} title={"+"} readOnly={true} alt="" delay={10000}>
      <Alert
        title={"입력 체크"}
        type="warning"
        show={props.alert.show}
        html={true}
        confirmButtonColor="#212c42"
        confirmButtonText={"확인"}
        showCancelButton={false}
        text={props.alert.text}
        onConfirm={(evt) => props.onConfirm(true)}
        onEscapeKey={(evt) => props.onConfirm(false)}
      />

      <div className="add-view">
        <div>객실 화면 추가</div>
        <table>
          <tbody>
            <tr>
              <th>타이틀</th>
              <td>
                <FormControl type="text" placeholder="타이틀 입력" value={props.room_view.title || ""} onChange={(evt) => props.handleInputChange("title", evt.target.value)} maxLength={10} />
              </td>
              <th>설명</th>
              <td colSpan="3">
                <FormControl
                  type="text"
                  value={props.room_view.comment || ""}
                  placeholder="설명 입력"
                  className="content"
                  onChange={(evt) => props.handleInputChange("comment", evt.target.value)}
                  maxLength={100}
                />
              </td>
            </tr>
            <tr>
              <th>테마</th>
              <td>
                <FormControl componentClass="select" placeholder="테마" name="theme_id" value={props.room_view.theme_id} onChange={(evt) => props.handleInputChange("theme_id", evt.target.value)}>
                  {props.roomTheme &&
                    props.roomTheme.list.map((v, k) => (
                      <option key={k} value={v.id}>
                        {v.name}
                      </option>
                    ))}
                </FormControl>
              </td>
              <th>층별 객실수</th>
              <td>
                <InputNumber
                  className="form-control small"
                  placeholder="층별객실수"
                  value={props.room_view.cols || ""}
                  step={1}
                  min={5}
                  max={50}
                  onChange={(value) => props.handleInputChange("cols", value)}
                />
              </td>
              <th>뷰화면 순서</th>
              <td>
                <InputNumber
                  className="form-control small"
                  placeholder="순서"
                  name="order"
                  value={props.room_view.order || ""}
                  step={1}
                  min={1}
                  max={props.roomView.list.length + 1}
                  onChange={(value) => props.handleInputChange("order", value)}
                />
              </td>
            </tr>
            <tr>
              <th>객실추가타입</th>
              <td>
                <ButtonGroup aria-label="객실추가타입" onChange={(evt) => props.handleInputChange("all_room", evt.target.value)}>
                  <Radio inline name="all_room" value="1">
                    <span />
                    모든객실
                  </Radio>
                  <Radio inline name="all_room" value="0" defaultChecked={true}>
                    <span />
                    남은객실
                  </Radio>
                </ButtonGroup>
              </td>
              <th>객실추가순서</th>
              <td>
                <ButtonGroup aria-label="객실추가순서" onChange={(evt) => props.handleInputChange("room_order", evt.target.value)}>
                  <Radio inline name="room_order" value="1" defaultChecked={true}>
                    <span />
                    객실명순
                  </Radio>
                  <Radio inline name="room_order" value="2">
                    <span />
                    객실명역순
                  </Radio>
                  <Radio inline name="room_order" value="3">
                    <span />
                    타입명순
                  </Radio>
                  <Radio inline name="room_order" value="4">
                    <span />
                    타입명역순
                  </Radio>
                </ButtonGroup>
              </td>
            </tr>
          </tbody>
        </table>

        <div className="form-content">
          <ButtonToolbar className="btn-room">
            <Button onClick={(evt) => props.onSave(evt)}>저장</Button>
            <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
              닫기
            </Button>
          </ButtonToolbar>
        </div>
      </div>
    </Tab>
  </Tabs>
);

export default Contents;
