// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

// import { CubeGrid } from "better-react-spinkit";
// import cx from "classnames";

// Components.
import Contents from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    view: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    roomViewItem: PropTypes.object.isRequired,
    roomTheme: PropTypes.object.isRequired,
    handleSaveRoomViewItems: PropTypes.func.isRequired,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      items: undefined,
      room_view: this.props.view,
      mounted: false,
      edit: false,
      change: false,
      viewChange: false,
      selectedItem: null,
      selectedIdx: -1,
      mouseDown: false,
      mousePosX: 0,
      mousePosY: 0,
      targetIdx: -1,
      alert: {
        title: "확인",
        text: "",
        callback: undefined,
      },
      dialog: false,
      confirm: false,
      isUpdate: false, // 업데이트 무한 루프 방지.
    };
  }

  updateViewItems = (viewItems) => {
    const {
      room_view: { cols, rows },
    } = this.state;

    const { room } = this.props;

    console.log("--- updateViewItems cols", cols, " rows", rows);

    // 화면에 그려줄 객실 칸.
    const items = _.map(new Array(rows * cols), (v, i) => {
      const x = i % cols;
      const y = Math.floor(i / cols);

      // 이미 존재하는 객실 여부.
      const item = _.find(viewItems, { col: x, row: y });

      if (item) {
        // 객실 정보 추가.
        const roomItem = _.find(room.list, { id: item.room_id });
        if (roomItem) {
          item.name = roomItem.name;
          item.room_type_id = roomItem.room_type_id;
          item.mode = "org";
        }

        // console.log("--- item", item.name, item);
      }

      const data = {
        x,
        y,
        w: 1,
        h: 1,
        i,
        static: !item ? true : false, // 존재 하는 객실만 이동 가능.
        item,
        orgItem: JSON.parse(JSON.stringify(item || {})), // deep copy.
        pos: {},
        element: undefined,
        move: {},
        drag: false,
      };

      // if (data.item) console.log("--- updateViewItems data", data.item.name, data);

      return data;
    });

    this.setState(
      {
        items,
        rows,
        change: false,
        mounted: true,
        isUpdate: true,
      },
      () => {
        // 화면에 객실이 모두 그려진 다음에 위치를 셋팅 한다.
        setTimeout(() => {
          let { items } = this.state;

          // room grid position update.
          items = _.map(items, (item, i) => {
            const element = document.getElementById("roomListDiv_" + this.props.tabIdx + "_" + i);
            item.element = element;
            item.pos = this.getElePos(element);
            return item;
          });

          // 뷰 화면 업데이트.
          this.props.handleSaveRoomViewElement(this.props.tabIdx, items);

          // SET_ROOM_VIEW_ITEM_LIST 업데이트 무한 루프 방지.
          setTimeout(() => {
            this.setState({
              isUpdate: false,
            });
          }, 100);

          console.info("-- Main > Contents > RoomView 객실 뷰 탭 updateViewItems ", this.props.tabIdx);
        }, 100);
      }
    );
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { room_view, isUpdate } = this.state;

    if (!isUpdate) {
      // 뷰 아이템 정보 변경.
      room_view[name] = !value || isNaN(value) ? value : Number(value);

      console.log("- handleInputChange name", name, "value", value);

      if (name === "theme_id") {
        // 테마 바로 적용.
        let item = _.find(this.props.roomTheme.list, { id: Number(value) });
        console.log("- theme", item);
        if (item) room_view["theme"] = item.theme;
      }

      this.props.handleUpdateRoomView(room_view);

      this.setState({
        viewChange: true,
      });
    }
  };

  moveItems = (addItem, targetItem) => {
    let { items } = this.state;

    let pItem = Object.assign({}, addItem.item);

    items = items.map((v, k) => {
      // 이동 하는 곳에 item 있으면 이동 시키다가 빈공간 나오면 stop!!
      if (pItem && targetItem.i <= v.i) {
        if (v.item) {
          let vItem = Object.assign({}, v.item);
          v.item = pItem;
          pItem = vItem;
        } else {
          v.item = pItem;
          pItem = null;
        }

        v.item.row = v.y;
        v.item.col = v.x;

        //console.log("-> move item", v);
      }

      // 업데이트 체크.
      if (v.item) {
        if (v.item.mode !== "new" && v.item.mode !== "del") {
          if (v.item.id !== v.orgItem.id) {
            v.item.mode = "update";
          } else {
            v.item.mode = "org";
          }
        }

        if (v.item.mode !== "org") {
          console.log("-> item mode ", v.item.mode, v.item);
        }
      }

      return v;
    });

    this.setState({
      items,
    });
  };

  onAddItem = () => {
    console.log("onAddItem ");

    const { items, selectedIdx } = this.state;

    let addItemIdx = selectedIdx;

    if (addItemIdx === -1) addItemIdx = 0;

    console.log("addItemIdx", addItemIdx, items.length);

    let { roomType, view } = this.props;

    // 신규 객실.
    const addItem = Object.assign({}, items[addItemIdx]);

    addItem.item = {
      view_id: view.id,
      place_id: view.place_id,
      room_type_id: roomType.list[0].id,
      room_id: "",
      name: "",
      col: addItem.x,
      row: addItem.y,
      mode: "new",
    };

    // romm right shift.
    this.moveItems(addItem, addItem);

    this.setState({
      change: true,
      selectedIdx: ++addItemIdx,
    });
  };

  onRemoveItem = (i) => {
    console.log("onRemoveItem", i);

    let { change, items } = this.state;

    items = _.map(items, (v, k) => {
      if (Number(i) === Number(v.i)) {
        if (v.item.mode === "new") {
          v.item = undefined;
        } else {
          v.item.mode = "del"; // 객실 삭제.
          change = true;
        }
      }
      return v;
    });

    this.setState({
      items,
      change,
    });
  };

  onReloadItem = (close) => {
    console.log("onReloadItem", this.state.confirm);

    if (this.state.confirm || !this.state.change) {
      console.log("this.props.view.id", this.props.view.id);

      this.props.initRoomViewItems(this.props.view.id);

      this.setState({
        change: false,
        viewChange: false,
      });
    } else {
      this.setState({
        alert: {
          title: "갱신 알림",
          text: "새로고침 시 저장 하지 않은 내용은 취소 됩니다. 새로고침 하시겠습니까?",
          showCancelBtn: true,
          callback: (confirm) => {
            if (confirm) this.onReloadItem();
          },
        },
        dialog: true,
      });
    }
  };

  onConfirm = (confirm) => {
    // console.log("- onConfirm", confirm);

    const { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false,
      },
      () => {
        setTimeout(() => {
          if (alert.callback) alert.callback(_.cloneDeep(confirm));
          this.setState({
            confirm: false,
          });
        }, 100);
      }
    );
  };

  onSaveItem = (close, callback) => {
    if (this.state.confirm) {
      const {
        auth: {
          user: { place_id },
        },
        view,
      } = this.props;

      const { viewChange, change } = this.state;

      // 변경 아이템.
      const items = this.state.items.filter((list) => list.item && list.item.mode && list.item.mode !== "org").map((list) => list.item);

      console.log("onSaveItem", items);

      // 입력값 검사.
      const isValid = _.filter(items, (item) => !item.name);

      console.log("----> isValid", isValid);

      if (isValid.length) {
        this.setState({
          alert: {
            title: "입력 오류",
            text: (isValid[0].room_id ? "수정" : "추가") + " 할 객실명을 입력해 주세요!",
            type: 0,
          },
          dialog: true,
        });
        return;
      }

      const newRooms = _.filter(items, (item) => {
        return !item.room_id && item.mode === "new"; // 추가 아이템이 이미 존재 하는지 검사.
      });

      if (newRooms.length > 0) {
        console.log("----> newRooms", newRooms);

        newRooms.map((item, k) => {
          const { room_type_id, name } = item;

          // 신규 객실 등록.
          this.props.handleNewRoom({ room: { place_id, room_type_id, name } }, (info) => {
            const { insertId: room_id } = info;
            console.log("----> onSaveItem new room room_id", room_id);
            item.room_id = room_id;

            // 모두 등록 후 뷰 화면 업데이트.
            if (k === newRooms.length - 1) {
              console.log("-----> new room add OK ");

              this.props.handleSaveRoomViewItems(view.id, items, (info) => {
                if (callback) callback(info);

                this.props.initAllRoomStates(place_id); //  화면 에 객실 표시 하기위해 상태 정보도 없데이트
              });
            }
          });

          return item;
        });
      } else {
        if (change) {
          // 뷰 화면 업데이트.
          this.props.handleSaveRoomViewItems(view.id, items, (info) => {
            if (callback) callback(info);

            this.props.initAllRoomStates(place_id);
          });
        }

        if (viewChange) {
          let { room_view } = this.state;

          const { id, cols, rows, col_gap, row_gap, width, height, theme_id } = room_view;

          // 뷰 설정 업데이트.
          this.props.handleSaveRoomView(view.id, {
            room_view: { id, cols, rows, col_gap, row_gap, width, height, theme_id },
          });

          this.setState(
            {
              edit: false,
              change: false,
              viewChange: false,
            },
            () => {
              // 편집 시 리로드 중지/시작.
              this.props.setStopReload(this.state.edit);
              this.props.initRoomViewItems(this.props.view.id);
            }
          );
        }
      }

      this.setState({
        dialog: false,
        change: false,
        mounted: false,
      });
    } else {
      this.setState({
        alert: {
          title: "저장 알림",
          text: "객실 화면을 수정 하시겠습니까?",
          showCancelBtn: true,
          callback: (confirm) => {
            if (confirm) this.onSaveItem();
          },
        },
        dialog: true,
      });
    }
  };

  onDeleteView = () => {
    if (!this.state.confirm) {
      this.setState({
        alert: {
          title: "삭제 알림",
          text: "객실 화면을 삭제 하시겠습니까?",
          showCancelBtn: true,
          callback: (confirm) => {
            if (confirm) this.onDeleteView();
          },
        },
        dialog: true,
      });
    } else {
      const {
        auth: {
          user: { place_id },
        },
        view,
      } = this.props;

      console.log("- poace_id, view.id", place_id, view.id);

      this.props.handleDeleteRoomView(place_id, view.id);

      // 화면 갱신을 위해 선택 초기화.
      this.props.updateRoomViewSelected(-1);

      // 리로드 시작.
      this.props.setStopReload(false);

      this.setState({
        edit: false,
        change: false,
      });

      setTimeout(() => {
        this.props.initRoomViewItems(this.props.view.id);
        setTimeout(() => {
          this.props.updateRoomViewSelected(0);
        }, 100);
      }, 100);
    }
  };

  onEditView = (noSave) => {
    let { viewChange, change, edit, confirm } = this.state;

    console.log("onEditView", { viewChange, change, edit, confirm });

    if (!noSave && edit && (change || viewChange)) {
      this.setState({
        alert: {
          title: "저장 확인",
          text: "저장 하지 않은 내용은 취소 됩니다. 저장 하시겠습니까?",
          showCancelBtn: true,
          callback: (isOk) => {
            if (isOk) this.onSaveItem();
            else this.onEditView(true);
          },
        },
        dialog: true,
      });
    } else {
      this.setState(
        {
          edit: !edit,
          change: false,
          viewChange: false,
        },
        () => {
          // 편집 시 리로드 중지/시작.
          this.props.setStopReload(edit);
          this.props.initRoomViewItems(this.props.view.id);
        }
      );
    }
  };

  // Room 에서 변경시.
  onChaneItem = (update, callback) => {
    // 변경 사항이 있으면 먼저 업데이트 한다.
    if (update && this.state.change) {
      this.onSaveItem(true, callback);
    } else {
      // 변경 여부 표시.
      this.setState({
        change: true,
      });
      if (callback) callback();
    }
  };

  getElePos = (element) => {
    if (!element) return {};

    const { left, top, width, height } = element.getBoundingClientRect();
    return {
      position: "absolute",
      zIndex: 999,
      left: left,
      top: top - 100,
      width,
      height,
    };
  };

  onMouseDown = (evt, selected) => {
    console.log("onMouseDown selected", selected);

    let { items, selectedItem } = this.state;

    selectedItem = _.find(items, {
      i: selected,
    });

    selectedItem.drag = true;
    selectedItem.move = Object.assign({}, selectedItem.pos);

    console.log("- click item", selectedItem);

    this.setState({
      selectedItem,
      mousePosX: evt.clientX,
      mousePosY: evt.clientY,
    });
  };

  onMouseDrag = (evt) => {
    let { items, selectedItem, mousePosX, mousePosY } = this.state;

    if (selectedItem) {
      //console.log("onMouseDrag selected", selectedItem.item);

      let { position, zIndex, left, top, width, height } = selectedItem.move;

      selectedItem.move = {
        position,
        zIndex,
        left: left + evt.clientX - mousePosX,
        top: top + evt.clientY - mousePosY,
        width,
        height,
      };

      let targetItem = null;

      items.map((item, i) => {
        if (!targetItem && item.i !== selectedItem.i) {
          if (left + width / 2 > item.pos.left && left + width / 2 < item.pos.left + width) {
            if (top + height / 2 > item.pos.top && top + height / 2 < item.pos.top + height) {
              targetItem = item;
              //console.log("---> targetItem", targetItem.item);
            }
          }
        }
        return item;
      });

      // console.log("- move", selectedItem.move);

      this.setState({
        mousePosX: evt.clientX,
        mousePosY: evt.clientY,
        targetIdx: targetItem ? targetItem.i : -1,
      });
    }
  };

  onMouseUp = (evt) => {
    console.log("onMouseUp");

    let { items, selectedItem } = this.state;

    if (selectedItem) {
      const { left, top, width, height } = selectedItem.move;

      let targetItem = null;

      items.map((item, i) => {
        if (!targetItem && item.i !== selectedItem.i) {
          if (left + width / 2 > item.pos.left && left + width / 2 < item.pos.left + width) {
            if (top + height / 2 > item.pos.top && top + height / 2 < item.pos.top + height) {
              targetItem = item;
              console.log("---> targetItem", targetItem.item);
            }
          }
        }
        return item;
      });

      selectedItem.drag = false;
      selectedItem.move = {};

      if (targetItem) {
        const addItem = Object.assign({}, selectedItem);

        selectedItem.item = null;

        // romm right shift.
        this.moveItems(addItem, targetItem);

        this.setState({
          change: true,
        });
      }
    }

    this.setState({
      selectedItem: null,
      targetIdx: -1,
    });
  };

  onItemClick = (e, item) => {
    // console.log("onItemClick item", item);

    this.setState({
      selectedIdx: item.i,
    });
  };

  // right button clicked.
  handleContextMenu = (e, room) => {
    console.log("handleContextMenu item", room);
    if (room && room.item) {
      this.props.handleRoomSelected(room.item.room_id);
      this.props.handleDisplayRoomLayer(true); // 바로 레이어 오픈.
    }
  };

  // context menu clicked.
  clickContextMenu = (e, data) => {
    console.log("clickContextMenu", data.idx);

    const { idx } = data;
    if (idx === 1) {
      this.props.handleDisplayRoomLayer(true);
    }
  };

  componentDidMount = () => {
    console.info("----------------------------------------------------------");
    console.info("-- Main > Contents > RoomView 객실 뷰 탭 componentDidMount");
    console.info("----------------------------------------------------------");

    const { roomViewItem } = this.props;
    const {
      auth: {
        user: { place_id },
      },
      view,
    } = this.props;

    let viewItems = null;

    if (roomViewItem) viewItems = roomViewItem.list.filter((item) => item.view_id === view.id);

    console.info("-- viewItems", viewItems);

    if (viewItems && viewItems.length) {
      this.updateViewItems(viewItems);
    } else {
      setTimeout(() => {
        if (roomViewItem) viewItems = roomViewItem.list.filter((item) => item.view_id === view.id);

        if (viewItems && viewItems.length) this.updateViewItems(viewItems);
        else this.props.initRoomViewItems(view.id); // 아이템이 없다면 리로드.
      }, 1000);
    }

    this.props.initAllRoomStates(place_id);
    this.props.initAllRoomSales(place_id);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.roomViewItem && nextProps.roomViewItem.list !== this.props.roomViewItem.list && !this.state.isUpdate) {
      const viewItems = nextProps.roomViewItem.list.filter((item) => item.view_id === this.props.view.id);

      // console.info("-- Main > Contents > RoomView 객실 뷰 아이템 업데이트 ", viewItems);

      // 객실이 없으면 편집 화면으로 전환.
      if (!viewItems.length) {
        this.setState({
          edit: true,
        });
      }

      this.updateViewItems(viewItems);
    }

    if (nextProps.view && nextProps.view !== this.props.view) {
      // console.info("-- Main > Contents > View 객실 뷰  ", nextProps.view);

      this.setState(
        {
          room_view: nextProps.view,
        },
        () => {
          const viewItems = (nextProps.roomViewItem || this.props.roomViewItem).list.filter((item) => item.view_id === this.props.view.id);
          this.updateViewItems(viewItems);
        }
      );
    }
  }

  render() {
    // 등록된 객실이 있어야 화면 보여준다.
    if (this.state.items && this.props.room.list.length > 0) {
      return (
        <Contents
          {...this.props}
          {...this.state}
          onEditView={this.onEditView}
          onSaveItem={this.onSaveItem}
          onReloadItem={this.onReloadItem}
          onAddItem={this.onAddItem}
          onConfirm={this.onConfirm}
          onRemoveItem={this.onRemoveItem}
          onDeleteView={this.onDeleteView}
          onBreakpointChange={this.onBreakpointChange}
          onLayoutChange={this.onLayoutChange}
          onMouseDown={this.onMouseDown}
          onMouseDrag={this.onMouseDrag}
          onMouseUp={this.onMouseUp}
          handleContextMenu={this.handleContextMenu}
          clickContextMenu={this.clickContextMenu}
          onItemClick={this.onItemClick}
          onChaneItem={this.onChaneItem}
          handleInputChange={this.handleInputChange}
        />
      );
    }
    return null;
  }
}

export default Container;
