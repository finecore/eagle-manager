import React, { Component } from "react";
import { connect } from "react-redux";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import CheckInIcon from "@material-ui/icons/CheckCircleOutline";

import Room from "../Room";

class RoomList extends Component {
  state = {};

  componentDidMount() {
    // console.log("--> RoomList componentDidMount", this.props.el);
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    if (nextProps.roomView.displayLayer) {
      return false;
    }
    return true;
  };

  render() {
    const { el } = this.props;

    const i = el.i;

    // console.log("--> generateDOM", i);

    return (
      <div
        key={i}
        id={"roomListDiv_" + this.props.tabIdx + "_" + i}
        className={cx(
          "noselect",
          "room-list",
          this.props.edit ? "edit" : "",
          el && el.drag ? "org" : "",
          this.props.targetIdx === i ? "drag" : "",
          this.props.edit && this.props.selectedIdx === i ? "selected" : ""
        )}
        onClick={(evt) => this.props.onItemClick(evt, el)}
        onDoubleClick={(evt) => this.props.handleContextMenu(evt, el)}
      >
        <span>
          {!el.item || el.item.mode === "del" ? (
            <div id={"roomList" + i} className={cx("roomDiv")}>
              {this.props.edit && (
                <span>
                  <span>
                    <dl className="room-box item-on color_0">
                      <dt className="room-title room-txtcolor0">
                        <div className="key-wrap">
                          <div className="name">
                            <div className="num">{i + 1}</div>
                          </div>
                        </div>
                      </dt>
                      <dd>
                        <div className="room-info off">
                          <div className="room-status">
                            <img src={this.props.roomTheme1} alt="" />
                          </div>
                          <div className="room-data" />
                        </div>
                      </dd>
                    </dl>
                  </span>
                </span>
              )}
            </div>
          ) : (
            <div id={"roomList" + i} className={cx("roomDiv", el.drag ? "drag" : "")} style={el.drag && el.move ? el.move : null}>
              <ContextMenuTrigger id={"context_unique_identifier_" + this.props.tabIdx + "_" + i} holdToDisplay={3000}>
                <div>
                  <Room
                    view={this.props.view}
                    viewItems={this.props.items}
                    viewItem={el}
                    vItem={el.item}
                    roomViewItem={this.props.roomViewItem}
                    vIdx={el.i}
                    edit={this.props.edit}
                    onRemoveItem={this.props.onRemoveItem}
                    onMouseDown={this.props.onMouseDown}
                    onMouseDrag={this.props.onMouseDrag}
                    onMouseUp={this.props.onMouseUp}
                    onChaneItem={this.props.onChaneItem}
                  />
                </div>
              </ContextMenuTrigger>
              <ContextMenu
                id={"context_unique_identifier_" + this.props.tabIdx + "_" + (!this.props.edit ? i : "-1")}
                onShow={(evt) => this.props.handleContextMenu(evt, el)}
                //onHide={evt => props.handleContextMenu(evt, null)}
              >
                <MenuItem data={{ idx: 1 }} onClick={this.props.clickContextMenu} selected={true}>
                  <div style={{ width: "80px" }}>
                    <CheckInIcon />
                    <span>체크인</span>
                  </div>
                </MenuItem>
                {/* <MenuItem data={{ idx: 2 }} onClick={props.clickContextMenu}>
                  ContextMenu Item 2
                </MenuItem>
                <MenuItem divider />
                <MenuItem data={{ idx: 3 }} onClick={props.clickContextMenu}>
                  ContextMenu Item 3
                </MenuItem> */}
              </ContextMenu>
            </div>
          )}
        </span>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(RoomList);
