// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import RoomLayer from "./presenter";

// Components.
import RoomTab01 from "./RoomTab01";
import RoomTab02 from "./RoomTab02";
// import RoomTab03 from "./RoomTab03";
// import RoomTab04 from "./RoomTab04";
// import RoomTab05 from "./RoomTab05";

import Gnb0102 from "pages/Setting/Gnb01/Gnb0102";
import Gnb0101 from "pages/Setting/Gnb01/Gnb0101";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      openDate: new Date(),
      useMileage: false,
      showMileage: false,
      selected: 1,
      alert: {
        title: "확인",
        text: "",
        type: 0, // 타입(0:알림,1:선택)
        yes: undefined, // 예/아니오 확인여부.
        callback: undefined,
      },
      dialog: false, // 알림창 보이기.
      confirm: false, // 알림창 확인 여부.
      interrupt: undefined,
      lockCountDownAlert: 5, // 잠금 해제 알람창 팝업 coutdown min
      lockTimeout: 10, // 잠금 해제 알람창대기시간(시간 오버시 창 닫힌다)
      isInterrupted: false, // 현재 인터럽트 여부.
    };
  }

  tabs = [
    { name: "입실처리", RoomTab: RoomTab01 },
    { name: "객실예약", RoomTab: RoomTab02 },
    { name: "객실이력", RoomTab: null, Link: Gnb0102 },
    { name: "매출이력", RoomTab: null, Link: Gnb0101 },
    // { name: "객실정보", RoomTab: RoomTab05 }
  ];

  timer = null;
  timer2 = null;

  _isMounted = false;

  static propTypes = {
    room: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
  };

  clickTab = (selected) => {
    const { room } = this.props;

    let tab = this.tabs[selected - 1];

    console.log("- clickTab", room.item.id);

    if (tab.Link) {
      this.props.handleDisplayLayer(tab.Link, { room_id: room.item.id });
      this.closeModal();
    } else {
      this.setState({
        selected,
      });
    }
  };

  openModal = (evt) => {
    this.props.handleDisplayRoomLayer(true);
  };

  closeModal = (evt) => {
    this.props.handleDisplayRoomLayer(false);
  };

  // 인터럽트 유지.(환경설정의 유지 시간으로 타이머 적용)
  handleIntterrupt = (yes) => {
    const { auth, preferences } = this.props;
    let { openDate, interrupt, lockCountDownAlert, lockTimeout, confirm } = this.state;

    console.info("-- handleIntterrupt ", yes, confirm);

    let isExpire = false;

    if (interrupt && yes === undefined) {
      const diff = Math.abs(new Date() - openDate); // ms
      const min = Math.floor(diff / 1000 / 60);

      // 인터럽트 자동 취소 시간 지났다면 초기화.
      if (min > preferences.interrupt_release - lockCountDownAlert) isExpire = true;

      console.info("-- isExpire ", isExpire, diff, min, preferences.interrupt_release, lockCountDownAlert);
    }

    clearTimeout(this.timer);

    if (!isExpire) {
      this.timer = setTimeout(() => {
        this.handleIntterrupt(undefined);
      }, 1000 * 10);
      return;
    }

    if (!confirm) {
      let lsatTime = lockTimeout;

      this.setState({
        alert: {
          title: "체크인 처리 시간 연장",
          text:
            "<span>체크인 처리 시간(" +
            preferences.interrupt_release +
            " 초)이 초과 되었습니다. </span>" +
            "<br /> <span>체크인 처리 시간 연장을 " +
            preferences.interrupt_release +
            " 초 더 하시겠습니까?</span>",
          showCancelBtn: true, // 선택
          callback: (yes) => {
            if (yes === true) {
              clearTimeout(this.timer);
              clearInterval(this.timer2);

              // 화면 잠금 연장 .
              this.props.handleSetRoomInterrupt(this.props.room.selected, {
                room_interrupt: {
                  room_id: this.props.room.selected,
                  channel: 1, // 작업 채널 (1: WEB, 2: 자판기)
                  token: auth.token, // 토큰.
                  sale: 1,
                  state: 1,
                  mod_date: new Date(),
                },
              });
            }
          },
        },
        dialog: true,
      });

      this.timer2 = setInterval(() => {
        lsatTime--;

        let { alert } = this.state;

        this.setState({
          alert: {
            ...alert,
            text:
              "<span>체크인 처리 시간(" +
              preferences.interrupt_release +
              " 초)이 초과 되었습니다. </span>" +
              "<br /> <span>체크인 처리 시간 연장을 " +
              preferences.interrupt_release +
              " 초 더 하시겠습니까?</span>" +
              "<br /><span><font color='red'>체크인창이 " +
              lsatTime +
              "초 후 닫힙니다.</font></span>",
          },
        });
      }, 1000);

      // 일정시간 응답 없으면 강제 잠금 해제.
      this.timer = setTimeout(() => {
        console.log("- isExpire", isExpire);

        if (isExpire) {
          this.setState({
            dialog: false,
          });
          this.closeModal();
        }
      }, lockTimeout * 1000);
    }
  };

  onConfirm = (confirm) => {
    console.log("- onConfirm", confirm);

    const { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false,
      },
      () => {
        setTimeout(() => {
          if (alert.callback) alert.callback(_.cloneDeep(confirm));
          this.setState({
            confirm: false,
          });
        }, 100);
      }
    );
  };

  componentDidMount = () => {
    this._isMounted = true;

    const { preferences, roomInterrupt, auth } = this.props;

    console.log("--> componentDidMount RoomLayer roomInterrupt ", this.props.roomInterrupt, preferences.interrupt_use);

    // 객실 인터럽트
    if (preferences.interrupt_use && this.props.room.selected) {
      let interrupt = _.find(roomInterrupt.list, {
        room_id: this.props.room.selected,
      });

      console.info("-- interrupt ", interrupt);

      if (interrupt) {
        const diff = Math.abs(new Date() - new Date(moment(interrupt.mod_date))); // ms
        const min = Math.floor(diff / 1000 / 60);

        console.info("-- diff min ", min, preferences.interrupt_release);

        // 인터럽트 자동 취소 시간 지났다면 초기화.
        if (min > preferences.interrupt_release) {
          this.props.handleClearInterrupt(this.props.room.selected); // 인터럽트 삭제.
          interrupt = undefined;
        }
      }

      if (!interrupt) {
        // 화면 잠금
        this.props.handleNewRoomInterrupt({
          room_interrupt: {
            room_id: this.props.room.selected,
            channel: 1, // 작업 채널 (1: WEB, 2: 자판기)
            token: auth.token, // 토큰.
            sale: 1,
            state: 1,
            mod_date: new Date(),
          },
        });
      }

      this.setState({
        interrupt,
      });
    }

    this.setState({
      openDate: new Date(),
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;

    clearTimeout(this.timer);
    clearInterval(this.timer2);

    if (!this.state.interrupt) this.props.handleClearInterrupt(this.props.room.selected); // 인터럽트 삭제.
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { preferences } = this.props;

    // 객실 인터럽트
    if (nextProps.roomInterrupt && preferences.interrupt_use) {
      let interrupt = _.find(nextProps.roomInterrupt.list, {
        room_id: this.props.room.selected,
      });

      console.info("-- interrupt ", interrupt);

      this.setState({
        interrupt,
      });
    }
  }

  render() {
    return <RoomLayer {...this.props} {...this.state} tabs={this.tabs} openModal={this.openModal} closeModal={this.closeModal} clickTab={this.clickTab} onConfirm={this.onConfirm} />;
  }
}

export default withSnackbar(Container);
