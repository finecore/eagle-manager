// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

import moment from "moment";
import _ from "lodash";
import { keyToValue } from "constants/key-map";

// Components.
import Gnb0102 from "./presenter";

import Gnb01022 from "pages/Setting/Gnb01/Gnb0102";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomStateLog: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      focused1: null,
      focused2: null,
      list: [],
      begin: { date: moment(moment(new Date(), "YYYY-MM-DD 00:00")), hour: 0, min: 0 },
      end: { date: moment(), hour: 24, min: 0 },
      option: {
        all: true,
        key_1: true,
        key_3: true,
        door_0: true,
        rent: true,
        stay: true,
        long: true,
        clean_1: true,
        clean_0: true,
        outing_1: true,
        outing_0: true,
        check_out: true,
        change_room_id: true,
        room_id: undefined,
      }, // 필터 적용,
    };
  }

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  handleSerach = () => {
    const {
      user: { place_id },
    } = this.props;

    const {
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let begin = moment(bd).hour(bh).minute(bm).format("YYYY-MM-DD HH:mm");

    let end = moment(ed).hour(eh).minute(em).format("YYYY-MM-DD HH:mm");

    console.log("- handleSerach", place_id, begin, end);

    this.props.initRoomStateSearchLogs(place_id, begin, end, "");
  };

  handleDateTime = (date) => {
    console.log("- handleDateTime ", JSON.stringify(date));

    const state = _.merge({}, this.state, date);

    this.setState(state);
  };

  handleTerm = (value, mode) => {
    const { begin, end } = this.state;
    const { preferences } = this.props;

    // 시간 초기화.
    begin.hour = preferences.day_sale_end_time;
    begin.min = 0;

    end.hour = preferences.day_sale_end_time;
    end.min = 0;

    let start = moment({ hour: preferences.day_sale_end_time, minute: 0 });

    // 오늘 정산 시간 지났는지 여부.
    let isTodaySaleEnd = false;

    if (start > moment()) {
      // 매출 마감 시간이 남았다면.
      isTodaySaleEnd = true;
    }

    if (mode === "day" && value === -1) {
      // 어제
      begin.date = moment().add(-2, mode);
      end.date = moment().add(-1, mode);
    } else if (mode === "week" && value === -1) {
      begin.date = moment().startOf("week");
      end.date = moment().endOf("week");
    } else if (mode === "month" && value === -1) {
      begin.date = moment().startOf("month");
      end.date = moment().endOf("month");
    } else if (mode === "year" && value === -1) {
      begin.date = moment().startOf("year");
      end.date = moment().endOf("year");
    } else {
      // 오늘 (정산 시간 지났으면 오늘 ~ 내일 정산 일자로 설정)
      begin.date = moment().add(isTodaySaleEnd ? -1 : 0, mode);
      end.date = moment().add(isTodaySaleEnd ? 0 : 1, mode);
    }

    console.log("- handleTerm", value, mode, begin);

    this.setState({
      begin,
      end,
    });
  };

  handleOption = ({ option }) => {
    const {
      roomStateLog: { search },
    } = this.props;

    console.log("- handleOption option ", JSON.stringify(option));

    let filter = _.merge({}, this.state.option, option);

    // 전체 선택/해제.
    if (option.all !== undefined) {
      _.map(this.state.option, (v, k) => {
        if (k !== "room_id") filter[k] = option.all;
      });
    } else {
      // 전체 옵션 선택 시 전체 체크.
      if (!option.all) {
        let isAll = true;
        _.map(filter, (v, k) => {
          console.log("- k,v", k, v);
          if (k !== "room_id" && k !== "all" && !v) {
            isAll = false;
            return false;
          }
        });

        filter.all = isAll;
      }
    }

    // console.log("- filter", filter);

    this.setState({ option: filter }, () => {
      this.filter(search);
    });
  };

  // 목록 필터링.
  filter = (search) => {
    const {
      option: { key_1, key_3, door_0, rent, stay, long, clean_1, clean_0, outing_1, outing_0, check_out: _check_out, change_room_id: _change_room_id, room_id },
    } = this.state;

    let list = [];

    _.map(_.cloneDeep(search), (state) => {
      const { room_id: id, reg_date, name, data } = state;

      const { key, door, sale, clean, outing, channel, check_in, check_out, change_room_id } = data;

      // console.log("- room_id", room_id, id);

      // 필터 옵션.
      if (room_id && Number(id) !== Number(room_id)) return;

      if ((key_1 && (Number(key) === 1 || Number(key) === 4)) || (key_3 && (Number(key) === 3 || Number(key) === 6))) state.key = key;
      if (door_0) state.door = door;

      state.check_in = check_in;

      if (_check_out && check_out) state.check_out = check_out;
      if (_change_room_id && change_room_id) state.change_room_id = change_room_id;

      if ((rent && Number(sale) === 2) || (stay && Number(sale) === 1) || (long && Number(sale) === 3)) state.sale = sale;
      if ((clean_1 && Number(clean) === 1) || (clean_0 && Number(clean) === 0)) state.clean = clean;
      if ((outing_1 && Number(outing) === 1) || (outing_0 && Number(outing) === 0)) state.outing = outing;

      // console.log("- state", state);

      if (Object.keys(state).length > 0) {
        let item = { reg_date, name, data: "" };

        if (!state.check_in && state.sale) {
          if (channel !== "web") item.data = keyToValue("room_sale", "channel", channel) + " ";
          item.data += keyToValue("room_state", "sale", Number(state.sale)) + " 입실";
          list.push(_.clone(item));
        }
        if (state.key !== undefined) {
          item.data =
            Number(state.key) === 1
              ? "고객키 ▼"
              : Number(state.key) === 2
              ? "마스터키 ▼"
              : Number(state.key) === 3
              ? "청소키 ▼"
              : Number(state.key) === 4
              ? "고객키 △"
              : Number(state.key) === 5
              ? "마스터키 △"
              : Number(state.key) === 6
              ? "청소키 △"
              : "NO KEY";

          list.push(_.clone(item));
        }
        if (state.door !== undefined) {
          item.data = Number(state.door) === 1 ? "문 【열림】" : "문 닫】【힘";
          list.push(_.clone(item));
        }
        if (state.clean !== undefined) {
          item.data = Number(state.clean) === 1 ? "청소요청" : "청소완료";
          list.push(_.clone(item));
        }
        if (state.outing !== undefined) {
          item.data = Number(state.outing) === 0 ? "외출복귀" : "외출";
          list.push(_.clone(item));
        }
        // if (state.check_in !== undefined) {
        //   item.data = keyToValue("room_sale", "stay_type", state.stay_type) + " 입실";
        //   list.push(_.clone(item));
        // }
        if (state.check_out !== undefined) {
          item.data = "퇴실";
          list.push(_.clone(item));
        }
        if (!state.check_in && !state.check_out && state.change_room_id !== undefined) {
          item.data = "객실 이동";
          list.push(_.clone(item));
        }
      }
    });

    console.log("- list", list);

    this.setState({ list });
  };

  componentDidMount() {
    const {
      user: { place_id },
    } = this.props;

    const {
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let begin = moment(bd).hour(bh).minute(bm).format("YYYY-MM-DD HH:mm");

    let end = moment(ed).hour(eh).minute(em).format("YYYY-MM-DD HH:mm");

    this.props.initRoomStateSearchLogs(place_id, begin, end, ""); // 객실 정보 조회.

    this.setState({
      loading: false,
    });

    this.props.handleDisplayLayer(Gnb01022);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // this.setState() 를 해도 추가적으로 렌더링하지 않습니다.

    // 객실 로그.
    if (nextProps.roomStateLog && nextProps.roomStateLog !== this.props.roomStateLog && nextProps.roomStateLog.search !== this.props.roomStateLog.search) {
      const { search } = nextProps.roomStateLog;

      this.filter(search);
    }
  }

  render() {
    return (
      <Gnb0102
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleDateTime={this.handleDateTime}
        handleOption={this.handleOption}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        handleSerach={this.handleSerach}
        handleTerm={this.handleTerm}
      />
    );
  }
}

export default withSnackbar(Container);
