// imports.
import React from "react";
import Modal from "react-awesome-modal";

import moment from "moment";
import _ from "lodash";

//room-popup
import { Form, ControlLabel, FormControl, Checkbox, Table, ButtonToolbar, Button, Radio } from "react-bootstrap";

import cx from "classnames";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

// functional component
const Gnb0102 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper">
          <h1>객실이력조회</h1>
          <div className="setting-form-wrapper" id="gnb0102">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <h2>조회일자</h2>
                    <div className="form-content">
                      <Radio inline name="search_term" value="1" onChange={(evt) => props.handleTerm(-1, "day")}>
                        <span />
                        어제
                      </Radio>
                      <Radio inline name="search_term" value="2" defaultChecked={true} onChange={(evt) => props.handleTerm(0, "day")}>
                        <span />
                        오늘
                      </Radio>
                      <Radio inline name="search_term" value="3" onChange={(evt) => props.handleTerm(-1, "week")}>
                        <span />
                        이번주
                      </Radio>
                      {/* <Radio
                        inline
                        name="search_term"
                        value="4"
                        onChange={evt => props.handleTerm(-1, "month")}
                      >
                        <span />
                        이번달
                      </Radio> */}
                      <div className="date-set">
                        <SingleDatePicker
                          id="startsOnDateInput-1"
                          placeholder="검색 시작일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.begin.date}
                          isOutsideRange={(day) => moment(day) > moment().add(1, "day")}
                          onDateChange={(date) => props.handleDateTime({ begin: { date } })}
                          focused={props.focused1}
                          onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.hour}
                            step={1}
                            min={0}
                            max={24}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.handleDateTime({ begin: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.min}
                            step={1}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.handleDateTime({ begin: { min } })}
                          />
                        </span>
                        <span> ~ </span>
                        <SingleDatePicker
                          id="endsOnDateInput-2"
                          placeholder="검색 종료일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.end.date}
                          isOutsideRange={(day) => moment(day).diff(props.begin.date) < 0 || moment(day) > moment().add(1, "day")}
                          onDateChange={(date) => props.handleDateTime({ end: { date } })}
                          focused={props.focused2}
                          onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.hour}
                            step={1}
                            min={0}
                            max={24}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.handleDateTime({ end: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.min}
                            step={1}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.handleDateTime({ end: { min } })}
                          />
                        </span>

                        <span>
                          <label className="checkbox-inline" title="">
                            <Button onClick={(evt) => props.handleSerach(evt)}>검색조회</Button>
                          </label>
                        </span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <h2>필터옵션</h2>
                    <div className="form-content search-select">
                      <ControlLabel htmlFor="gnb0102-01">객실명</ControlLabel>
                      <FormControl componentClass="select" placeholder="전체" id="gnb0102-01" onChange={(evt) => props.handleOption({ option: { room_id: evt.target.value } })}>
                        <option value="">전체객실</option>
                        {props.room.list.map((room, k) => (
                          <option key={k} value={room.id}>
                            {room.name}
                          </option>
                        ))}
                      </FormControl>
                    </div>
                    <Checkbox inline checked={props.option.all} onChange={(evt) => props.handleOption({ option: { all: evt.target.checked } })}>
                      <span />
                      전체
                    </Checkbox>

                    <div className="separator" />
                    <div className="form-content search-select">
                      <Checkbox inline checked={props.option.key_1} onChange={(evt) => props.handleOption({ option: { key_1: evt.target.checked } })}>
                        <span />
                        손님키
                      </Checkbox>
                      <Checkbox inline checked={props.option.key_3} onChange={(evt) => props.handleOption({ option: { key_3: evt.target.checked } })}>
                        <span />
                        청소키
                      </Checkbox>
                      <Checkbox inline checked={props.option.door_0} onChange={(evt) => props.handleOption({ option: { door_0: evt.target.checked } })}>
                        <span />
                        출입문
                      </Checkbox>
                      <Checkbox inline checked={props.option.rent} onChange={(evt) => props.handleOption({ option: { rent: evt.target.checked } })}>
                        <span />
                        대실
                      </Checkbox>
                      <Checkbox inline checked={props.option.stay} onChange={(evt) => props.handleOption({ option: { stay: evt.target.checked } })}>
                        <span />
                        숙박
                      </Checkbox>
                      <Checkbox inline checked={props.option.long} onChange={(evt) => props.handleOption({ option: { long: evt.target.checked } })}>
                        <span />
                        장기
                      </Checkbox>
                      <Checkbox inline checked={props.option.check_out} onChange={(evt) => props.handleOption({ option: { check_out: evt.target.checked } })}>
                        <span />
                        퇴실
                      </Checkbox>
                      <Checkbox inline checked={props.option.change_room_id} onChange={(evt) => props.handleOption({ option: { change_room_id: evt.target.checked } })}>
                        <span />
                        객실이동
                      </Checkbox>
                      <Checkbox inline checked={props.option.clean_1} onChange={(evt) => props.handleOption({ option: { clean_1: evt.target.checked } })}>
                        <span />
                        청소요청
                      </Checkbox>
                      <Checkbox inline checked={props.option.clean_0} onChange={(evt) => props.handleOption({ option: { clean_0: evt.target.checked } })}>
                        <span />
                        청소완료
                      </Checkbox>
                      <Checkbox inline checked={props.option.outing_1} onChange={(evt) => props.handleOption({ option: { outing_1: evt.target.checked } })}>
                        <span />
                        외출
                      </Checkbox>
                      <Checkbox inline checked={props.option.outing_0} onChange={(evt) => props.handleOption({ option: { outing_0: evt.target.checked } })}>
                        <span />
                        외출복귀
                      </Checkbox>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="search-result">
                {/* <ButtonToolbar className="btn-room">
                  <Button>프린트하기</Button>
                  <Button>엑셀저장</Button>
                </ButtonToolbar> */}
                <div className="table-wrapper">
                  <Table>
                    <thead>
                      <tr>
                        <th>일시</th>
                        <th>객실명</th>
                        <th>상태</th>
                      </tr>
                    </thead>
                    <tbody>
                      {props.list.map((log, k) => (
                        <tr key={k} className={cx("slide")}>
                          <td>{moment(log.reg_date).format("YYYY-MM-DD HH:mm:ss")}</td>
                          <td>{log.name}</td>
                          <td>{log.data}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0102;
