// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomInterruptAction } from "actions/roomInterrupt";
import { actionCreators as layoutAction } from "actions/layout";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room, roomView, roomState, roomInterrupt } = state;

  return {
    auth,
    room,
    roomView,
    roomState,
    preferences: preferences.item,
    roomInterrupt,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleGetRoomInterrupt: (roomId) => dispatch(roomInterruptAction.getRoomInterrupt(roomId)),

    handleNewRoomInterrupt: (roomInterrupt) => {
      dispatch(roomInterruptAction.newRoomInterrupt(roomInterrupt));
    },

    handleSetRoomInterrupt: (roomId, roomInterrupt) => {
      dispatch(roomInterruptAction.putRoomInterrupt(roomId, roomInterrupt));
    },

    handleDisplayLayer: (displayLayer, props) => dispatch(layoutAction.setDisplayLayer(displayLayer, props)),

    handleClearInterrupt: (roomId) => dispatch(roomInterruptAction.delRoomInterrupt(roomId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
