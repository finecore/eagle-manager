// imports.
import { connect } from "react-redux";

// Actions.

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { room } = state;
  return {
    room,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
