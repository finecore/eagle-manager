// imports.
import React from "react";
import { FormGroup, FormControl, Checkbox, Table, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

const RoomTab04 = (props) => {
  return (
    <div>
      <ul className="list-bul">
        <li>
          <h2>검색조건</h2>
          <div className="form-content">
            <div>
              <SingleDatePicker
                id="startsOnDateInput-3"
                placeholder="검색 시작일 선택"
                displayFormat="YYYY년 MM월 DD일"
                date={props.date}
                onDateChange={(date) => props.onDateChange({ date })}
                focused={props.focused}
                onFocusChange={({ focused }) => props.onFocusChange({ focused })}
              />
              <span className="numeric-wrap">
                <InputNumber className="form-control" defaultValue={0} step={1} min={0} max={24} formatter={(value) => `${value}시`} />
              </span>
              <span> ~ </span>
              <SingleDatePicker
                id="endsOnDateInput-3"
                placeholder="검색 마침일 선택"
                displayFormat="YYYY년 MM월 DD일"
                date={props.date2}
                onDateChange={(date2) => props.onDateChange2({ date2 })}
                focused={props.focused2}
                onFocusChange={({ focused: focused2 }) => props.onFocusChange2({ focused2 })}
              />
              <span className="numeric-wrap">
                <InputNumber className="form-control" defaultValue={0} step={1} min={0} max={24} formatter={(value) => `${value}시`} />
              </span>
            </div>
            <FormGroup className="form-check">
              <Checkbox inline>
                <span />
                전체
              </Checkbox>
              <Checkbox inline>
                <span />
                대실
              </Checkbox>
              <Checkbox inline>
                <span />
                숙박
              </Checkbox>
              <Checkbox inline>
                <span />
                장기
              </Checkbox>
              <Checkbox inline>
                <span />
                입실취소
              </Checkbox>
              <div className="separator" />
              <Checkbox inline>
                <span />
                내림차순
              </Checkbox>
              <Checkbox inline>
                <span />
                오름차순
              </Checkbox>
              <Button>조회</Button>
            </FormGroup>
            <div className="table-x-scroll">
              <div className="table-wrapper">
                <Table>
                  <thead>
                    <tr>
                      <th>입실일시</th>
                      <th>퇴실일시</th>
                      <th>사용시간</th>
                      <th>객실명</th>
                      <th>투숙형태</th>
                      <th>기본요금</th>
                      <th>추가요금</th>
                      <th>할인요금</th>
                      <th>매출</th>
                      <th>현금</th>
                      <th>카드</th>
                      <th>미수금</th>
                      <th>근무자</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <td colSpan="13">정산 내용</td>
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                      <td>2019-01-01, 00:00:00</td>
                      <td>사용중</td>
                      <td>120분</td>
                      <td>101호</td>
                      <td>대실</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </div>
            <ButtonToolbar className="btn-room">
              <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                닫기
              </Button>
            </ButtonToolbar>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default RoomTab04;
