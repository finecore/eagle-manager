// imports.
import React from "react";

// Components.
import Memo from "./Memo";

import { Table, ButtonToolbar, Button } from "react-bootstrap";

import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

const RoomTab05 = (props) => {
  return (
    <div>
      {/* 메모등록 팝업 */}
      {props.room.displayCheckIn && <Memo />}

      <ul className="list-bul">
        <li>
          <h2>검색조건</h2>
          <div className="form-content">
            <div className="search-date">
              <SingleDatePicker
                id="startsOnDateInput-4"
                placeholder="검색 시작일 선택"
                displayFormat="YYYY년 MM월 DD일"
                date={props.date}
                onDateChange={(date) => props.onDateChange({ date })}
                focused={props.focused}
                onFocusChange={({ focused }) => props.onFocusChange({ focused })}
              />
              <span> ~ </span>
              <SingleDatePicker
                id="endsOnDateInput-3"
                placeholder="검색 마침일 선택"
                displayFormat="YYYY년 MM월 DD일"
                date={props.date2}
                onDateChange={(date2) => props.onDateChange2({ date2 })}
                focused={props.focused2}
                onFocusChange={({ focused: focused2 }) => props.onFocusChange2({ focused2 })}
              />
              <Button>조회</Button>
            </div>
            <ButtonToolbar className="btn-room">
              <Button onClick={(evt) => props.openModal(evt)}>추가</Button>
              <Button onClick={(evt) => props.openModal(evt)}>수정</Button>
              <Button onClick={(evt) => props.openModal(evt)}>삭제</Button>
            </ButtonToolbar>
            <div className="table-wrapper">
              <Table>
                <thead>
                  <tr>
                    <th>등록일</th>
                    <th>내용</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <ButtonToolbar className="btn-room">
              <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                닫기
              </Button>
            </ButtonToolbar>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default RoomTab05;
