// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Memo from "./presenter";

class Container extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
  };

  closeModal = (evt) => {
    this.props.handleDisplayRoomLayer(false);
  };

  render() {
    return <Memo {...this.props} closeModal={this.closeModal} />;
  }
}

export default withSnackbar(Container);
