// imports.
import React from "react";
import Modal from "react-awesome-modal";

//room-popup
import { FormControl, ButtonToolbar, Button } from "react-bootstrap";

// styles.
import "./styles.scss";

// functional component
const Memo = (props) => {
  return (
    <Modal visible={false} effect="fadeInUp">
      <article className="memo-write">
        <h1>메모등록</h1>
        <div className="write-form-wrapper">
          <FormControl componentClass="textarea" placeholder="메모를 입력해 주세요." />
          <ButtonToolbar className="btn-room">
            <Button>저장</Button>
            <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
              닫기
            </Button>
          </ButtonToolbar>
        </div>
        <a onClick={(evt) => props.closeModal(evt)} className="move-close">
          Close
        </a>
      </article>
    </Modal>
  );
};

export default Memo;
