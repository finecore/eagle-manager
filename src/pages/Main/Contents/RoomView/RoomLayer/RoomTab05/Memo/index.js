// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomAction } from "actions/room";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { room } = state;
  return {
    room,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayRoomLayer: (display) => dispatch(roomAction.displayCheckInPopUp(display)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
