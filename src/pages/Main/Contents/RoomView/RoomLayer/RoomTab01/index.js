// imports.
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";

import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomReservAction } from "actions/roomReserv";
import { actionCreators as roomFeeAction } from "actions/roomFee";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as timeOptionAction } from "actions/timeOption";
import { actionCreators as mileageAction } from "actions/mileage";
import { actionCreators as smsAction } from "actions/sms";
import { actionCreators as placeSubscribeAction } from "actions/placeSubscribe";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomDoorLockAction } from "actions/roomDoorLock";
import { actionCreators as seasonPremiumAction } from "actions/seasonPremium";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { preferences, place, device, auth, room, roomSale, roomState, roomReserv, roomDoorLock, roomType, roomFee, seasonPremium, subscribe, placeSubscribe, timeOption, mileage, sms } = state;

  const day = moment().isoWeekday();

  // 해당 룸타입의 요일 요금 목록
  let roomFeeTypeList = _.filter(roomFee.list, (v) => v.room_type_id === room.item.room_type_id && v.day === day);

  // 도어락 객실 정보 ITEM 설정(조회 이전 현재 상태값 가져온다)
  let roomDoorLockCopy = _.cloneDeep(roomDoorLock);

  roomDoorLockCopy.item = _.find(roomDoorLock, { room_id: room.item.id }) || {};

  // console.log("--- roomDoorLockCopy ", roomDoorLockCopy);

  return {
    selected: room.selected,
    user: auth.user,
    place: place.item,
    room: room.item,
    roomType,
    roomFee: roomFeeTypeList,
    roomSale,
    roomState,
    roomReserv,
    roomDoorLock: roomDoorLockCopy,
    roomStateOrg: roomState,
    subscribe,
    placeSubscribe,
    seasonPremiums: seasonPremium.list,
    preferences: preferences.item,
    device,
    timeOption,
    mileage,
    sms,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomSale: (id) => dispatch(roomSaleAction.getRoomSale(id)),
    initPlaceRoomSale: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSum(placeId, begin, end)),
    initPlaceRoomSaleRoom: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSumRooms(placeId, begin, end)),
    initRoomState: (roomId) => dispatch(roomStateAction.getRoomState(roomId)),
    initRoomReservs: (filter, begin, end) => dispatch(roomReservAction.getRoomReservs(filter, begin, end)),
    initRoomTypeFees: (roomTypeId) => dispatch(roomFeeAction.getRoomTypeFees(roomTypeId)),
    initTimeOptions: (palceId) => dispatch(timeOptionAction.getTimeOptions(palceId)),
    initMileage: (placeId, phone) => dispatch(mileageAction.getMileage(placeId, phone)),
    initAllPlaceSubscribes: (palceId) => dispatch(placeSubscribeAction.getPlaceSubscribeList(palceId)),
    initRoomDoorLock: (room_id) => dispatch(roomDoorLockAction.getRoomDoorLockByRoomId(room_id)),

    handleNewMileage: (mileage) => dispatch(mileageAction.newMileage(mileage)),
    handleSmsSendAuthNo: (phone, callback) => dispatch(smsAction.sendAuthNo(phone, callback)),
    handleSmsSendMsg: (sms) => dispatch(smsAction.newSms(sms)),
    handleNewRoomSale: (roomSale, callback) => dispatch(roomSaleAction.newRoomSale(roomSale, callback)),
    handleSaveRoomSale: (id, roomSale, callback) => dispatch(roomSaleAction.putRoomSale(id, roomSale, callback)),
    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),
    handleSaveRoomReserv: (id, roomReserv) => dispatch(roomReservAction.putRoomReserv(id, roomReserv)),
    handleSaveMileage: (placeId, phone, mileage) => dispatch(mileageAction.putMileage(placeId, phone, mileage)),
    handleIncreaseMileage: (place_id, phone, user_id, point, rollback) => dispatch(mileageAction.increaseMileage(place_id, phone, user_id, point, rollback)),
    handleDecreaseMileage: (place_id, phone, user_id, point, rollback) => dispatch(mileageAction.decreaseMileage(place_id, phone, user_id, point, rollback)),
    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),
    handleDisplayRoomLayer: (display) => dispatch(roomViewAction.setDisplayRoomLayer(display)),
    setStopReload: (stop) => dispatch(preferencesAction.setStopReload(stop)),
    handleSaveRoom: (id, room, callback) => dispatch(roomAction.putRoom(id, room, callback)),
    handleSaveRoomDoorLock: (id, room_doorlock) => dispatch(roomDoorLockAction.putRoomDoorLock(id, room_doorlock)),
    handleClearRoomDoorLock: (id) => dispatch(roomDoorLockAction.clearRoomDoorLock(id)),
    handleLoginQrCode: (query) => dispatch(roomDoorLockAction.loginQrCode(query)),
    handleSendQrCode: (id, query) => dispatch(roomDoorLockAction.sendQrCode(id, query)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
