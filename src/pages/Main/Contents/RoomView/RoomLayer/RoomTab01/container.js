// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import moment from "moment";
import _ from "lodash";
import format from "utils/format-util";

import swal from "sweetalert";
import Swal from "sweetalert2";

import { Wave } from "better-react-spinkit";
import cx from "classnames";

import { keyToValue } from "constants/key-map";
import { CheckSubscribe } from "utils/subscribe-util";
import { encrypt, decrypt } from "utils/aes256-util";
import { YYDoorLock } from "utils/doorlock-util";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import RoomTab01 from "./presenter";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    roomSale: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
    roomReserv: PropTypes.object.isRequired,
    roomDoorLock: PropTypes.object.isRequired,
    roomStateOrg: PropTypes.object.isRequired,
    seasonPremiums: PropTypes.array.isRequired,
    preferences: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
    subscribe: PropTypes.object.isRequired,
    placeSubscribe: PropTypes.object.isRequired,
    device: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      roomSaleItem: {},
      roomStateItem: {},
      roomStateItemOrg: {},
      stayTypeColor: [],
      isSubscribePoint: false,
      idm: {}, // IDM 데몬
      isc: {}, // 자판기
      data: {
        room: {},
        sale: {
          room_reserv_id: 0,
          default_fee: 0,
          add_fee: 0,
          option_fee: 0,
          pay_card_amt: 0,
          pay_cash_amt: 0,
          pay_point_amt: 0,
          prepay_ota_amt: 0,
          prepay_cash_amt: 0,
          prepay_card_amt: 0,
          prepay_point_amt: 0,
          reserv_fee: 0,
          car_no: "",
          time_option_fee: 0,
          phone: "",
          save_point: 0,
        },
        state: {
          notice: "",
          air_temp: 0,
          air_set_temp: 0,
          temp_key_1: 0, // 사용중
          temp_key_2: 0, // 외출중
          temp_key_3: 0, // 공실
          temp_key_4: 0, // 청소중
          temp_key_5: 0, // 청소대기
        },
      },
      org: { room: {}, sale: {}, state: {} }, // 변경 항목 검사를 위한 원본 데이터
      save: { sale: {}, state: {} }, // 변경 항목 저장.
      prevSale: {},
      fee: {}, // 시간별 요금.
      reserv: {}, // 예약 정보
      mileage: {
        id: 0,
        point: 0, // 적립 포인트.
        phone: "", // 전화번호
      },
      remainPoint: 0, // 잔여 포인트
      savePoint: 0, // 적립 포인트
      phoneQRNumber: "", // QR 전송 전화번호
      phoneNumber: "", // 조회 전화번호
      isPointSearch: false, // 포인트 조회 여부.
      reservs: [], // 현재 객실 예약 정보.
      season: undefined, // 시즌 정보(요금)
      stayTime: 0, // 기본 숙박 시간.
      rentTime: 0, // 기본 대실 시간.
      changeCnt: 0, // 변경 카운트.
      changeStay: false, // 숙박 변경.
      displayRoomMove: false, // 객실 이동 팝업.
      timeOptions: [], // 입실 시간 옵션.
      alert: {
        type: "info",
        title: "확인",
        text: "",
        callback: undefined,
      },
      dialog: false, // 알림창 보이기.
      confirm: false, // 알림창 확인 여부.
      checkInCancel: false, // 입실 취소 가능 시간(환경 설정 정보)
      checkOutCancel: false, // 퇴실 취소 가능 시간(환경 설정 정보)
      useCheckout: false, // 사용연장
    };

    this.isUpdate = false; // 중복 체크인 방지.

    // un mounted check
    this._isMounted = false;

    // YY 도어락 API 유틸.
    console.log("----> global.yYDoorLock", global.yYDoorLock);
  }

  // 객실 정보 변경 이벤트.
  handleInputChange = (table, name, value) => {
    console.log("----> handleInputChange", { table, name, value });

    const {
      preferences: { stay_time_type, car_call_display },
    } = this.props;

    let { stayTime, rentTime, data, org, save, changeCnt, changeStay, mileage } = this.state;

    // console.log("- org", org.sale, " data", data.sale);

    // 현재 판매 정보 저장.
    let prevSale = _.cloneDeep(data.sale);

    // 체크아웃 날자 체크
    if (name.indexOf("check_out_exp") === 0) {
      if (data.sale.stay_type === 1) {
        if (moment(value).isBefore(moment(data.sale.check_in || data.sale.check_in_exp))) {
          return;
        }
      }
    }

    // 체크인/아웃 날자 포멧 맞춘다.
    if (name.indexOf("check_") === 0) {
      // 비교를 위해 포멧.
      if (data[table][name]) data[table][name] = moment(data[table][name]).format("YYYY-MM-DD HH:mm:ss");

      // 원본 수정.
      if (org[table][name]) org[table][name] = moment(org[table][name]).format("YYYY-MM-DD HH:mm:ss");

      value = moment(value).format("YYYY-MM-DD HH:mm:ss");

      //console.log("- date format ", name, value);
    }

    // 기본 요금 변경 시.
    if (name === "default_fee") {
      value = value < 0 ? 0 : value;
      data.sale.change_default_fee = value !== data.sale.default_fee;
      console.log("- change_default_fee ", data.sale.change_default_fee);
    }

    // 추가 요금 변경 시.
    if (name === "add_fee") {
      // 기본 요금보다 작을 수 없다.
      value = value < -data.sale.default_fee ? -data.sale.default_fee : value;

      // 체크인 전, 재입실 시
      if (org.state.sale === 0) {
        let payfee = data.sale.default_fee + value;

        // 지불요금 보다 결제 요금이 크다면...
        if (data.sale.pay_cash_amt > payfee) data.sale.pay_cash_amt = payfee; // 현금 지불 요금 조정.
        payfee -= data.sale.pay_cash_amt;

        if (data.sale.pay_card_amt > payfee) data.sale.pay_card_amt = payfee; // 카드 지불 요금 조정.
        payfee -= data.sale.pay_card_amt;

        if (data.sale.pay_point_amt > payfee) data.sale.pay_point_amt = payfee <= mileage.point ? payfee : mileage.point; // 포인트 지불 요금 조정.
      }
    }

    if (name !== "sale" && String(data[table][name]) === String(value)) {
      // 동일 값이면 패스.
      console.log("--->  동일 값이면 패스:", name);
      this._isMounted &&
        this.setState({
          confirm: false,
        });
      return;
    }

    const before = String(data[table][name]) === String(org[table][name]);
    const beforeValue = data[table][name];

    console.log("----> ", { table, name, value });

    // 데이터 변경.
    data[table][name] = value;

    // 숙박 형태 변경 시.
    if (name === "sale") {
      let hour = Number(moment().format("HH"));

      // console.log("- stay_time_type", data.state.sale, stay_time_type, stayTime, rentTime);

      // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
      if (data.state.sale === 1 && stay_time_type === 1) {
        data.sale.check_out_exp = moment(data.sale.check_in)
          .hour(stayTime)
          .minute(0)
          .add(stayTime > hour ? 0 : 1, "day") // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
          .format("YYYY-MM-DD HH:mm:ss"); // 체크아웃 예정일.

        // 숙박 시 입/퇴실 일자 동일 하다면 1일 추가.
        if (moment(data.sale.check_in).format("YYYYMMDD") === moment(data.sale.check_out_exp).format("YYYYMMDD")) {
          data.sale.check_out_exp = moment(data.sale.check_out_exp).add(1, "day");
        }
      } else {
        data.sale.check_out_exp = moment(data.sale.check_in || new Date())
          .add(data.state.sale === 2 ? rentTime : stayTime, "hour")
          .format("YYYY-MM-DD HH:mm:ss"); // 체크아웃 예정일.
      }

      // console.log("- stay_time_type", data.sale.check_out_exp);

      if (changeStay) {
        // 입실 옵션 초기화.
        data.sale.time_option_id = 0;
        data.sale.time_option_fee = 0;
      }

      // 대실/숙박 변경 시 원래 상태는 그때 매출로 적용 한다.
      if (changeStay && org.state.sale === value) {
        data.sale = _.cloneDeep(org.sale);
        save.sale = {};
        console.log("- changeStay data.sale", data.sale);
      } else {
        save.sale.stay_type = data.state.sale;
      }
    }

    // 차량번호 등록/변경 시.
    if (name.indexOf("car_no") === 0 && car_call_display) {
      const regex = new RegExp(beforeValue, "g");
      data.state.notice = String(data.state.notice || "").replace(regex, "");
      data.state.notice = value + " " + data.state.notice;
    }

    // 난방 온도 변경 시.
    if (name.indexOf("temp_key_") === 0) {
      if (data.state.outing === 1 || (data.state.clean === 1 && data.state.key !== 3)) {
        // 외출 온도.
        if (name === "temp_key_2") save.state["air_set_temp"] = value;
        // 청소대기 온도.
        else if (name === "temp_key_5") save.state["air_set_temp"] = value;
      } else {
        if (org.state.sale > 0 || data.state.key === 3) {
          // 사용중 온도.
          if (name === "temp_key_1") save.state["air_set_temp"] = value;
          // 청소중 온도.
          else if (name === "temp_key_4") save.state["air_set_temp"] = value;
        } else if (org.state.sale === 0) {
          if (name === "temp_key_3") save.state["air_set_temp"] = value; // 공실 시 온도.
        }
      }
    }

    const after = String(data[table][name]) === String(org[table][name]);

    if (!before && after) {
      delete save[table][name]; // 변경 항목 제거.
    } else if (before && !after) {
      save[table][name] = value; // 변경 항목 추가.
    } else if (String(save[table][name]) !== String(value)) {
      save[table][name] = value; // 변경 항목 업데이트.
    }

    ++changeCnt;

    console.log("- changeCnt", changeCnt);

    let remainPoint = mileage.id ? mileage.point + org.sale.pay_point_amt - data.sale.pay_point_amt : 0; // 잔여 포인트 계산
    if (remainPoint > mileage.point) remainPoint = mileage.point;

    this._isMounted &&
      this.setState(
        {
          data,
          org,
          prevSale,
          changeCnt,
          changeStay: name === "check_out_exp" ? changeStay : false,
          remainPoint, // 잔여 포인트 계산
          confirm: false,
        },
        () => {
          console.log("- setState", table, name, data[table][name]);

          // 숙박 형태 변경 시.
          if (name === "sale") {
            this.setFee(this.props, true, true, (sale) => {
              this.onChangeSale(null, 1);
            });
          }

          // 인원 변경 시.
          if (name === "person") this.setFee(this.props, org.state.sale === 0, false);

          // 입/퇴실 일자 변경 시.
          if (name.indexOf("check_") === 0) this.setFee(this.props, org.state.sale === 0, false);

          // 결제 요금 변경 시.
          if (name === "default_fee" || name === "add_fee" || name === "pay_cash_amt" || name === "pay_card_amt" || name === "pay_point_amt" || name === "room_reserv_id") {
            this.setFee(this.props, false, false, (sale) => {
              // 포인트 계산.
              this.calcPoint(sale);
              console.log("- change sale", sale);
            });
          }
        }
      );
  };

  // 표시 문구 삭제.
  noticeDel = (checked) => {
    console.log("- noticeDel", checked);

    let {
      data: {
        state: { notice },
      },
    } = this.state;

    if (checked) {
      if (notice) this.handleInputChange("state", "notice", "");
    }
  };

  // 차량번호 표시
  noticeAddCarNo = (checked) => {
    console.log("- noticeAddCarNo", checked);

    let {
      data: {
        state: { notice = "" },
        sale: { car_no = "" },
      },
    } = this.state;

    const regex = new RegExp(car_no, "g");
    let text = notice.replace(regex, "").replace(/^\s*/, "");

    if (checked) text = car_no + " " + text;

    this.handleInputChange("state", "notice", text);
  };

  // 난방 온도 리셋.
  resetTemp = (save) => {
    const {
      preferences: { temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5 },
    } = this.props;

    let { roomStateItem } = this.state;
    let { room_id, sale, outing, clean, key, check_out, inspect } = roomStateItem;

    let room_state = {
      temp_key_1,
      temp_key_2,
      temp_key_3,
      temp_key_4,
      temp_key_5,
    };

    if (clean === 1 && key !== 3) {
      room_state["air_set_temp"] = temp_key_5; // 청소대기 온도.
    } else if (outing === 1 && key !== 3) {
      room_state["air_set_temp"] = temp_key_2; // 외출 시 온도.
    } else {
      if (key === 3) {
        room_state["air_set_temp"] = temp_key_4; // 청소중 온도.
      } else if (sale > 0 && !check_out) {
        room_state["air_set_temp"] = temp_key_1; // 사용중 온도.
      } else if (sale === 0) {
        room_state["air_set_temp"] = temp_key_3; // 공실 온도.
      } else {
        room_state["air_set_temp"] = temp_key_5; // 청소대기 온도.
      }
    }

    if (inspect === 1) {
      room_state["air_set_temp"] = temp_key_5; // 청소/점검대기 온도
    }

    if (inspect === 2) {
      room_state["air_set_temp"] = temp_key_4; // 청소/점검 중 온도
    }

    console.log("- resetTemp", room_id, room_state);

    if (save) {
      this.props.handleSaveRoomState(room_id, { room_state });
    }
  };

  // 현금/카드/포인트 결제금액 설정.
  handlePayCheck = (type, isAll, isChecked) => {
    console.log("- handlePayCheck", { type, isAll, isChecked });

    let {
      data: { sale },
      remainPoint,
      mileage,
    } = this.state;

    const { preferences } = this.props;

    let change = _.cloneDeep(sale);

    console.log("- handlePayCheck", { sale, change });

    let fee_total = 0;

    // 예약은 예약 할인 가격이 add_fee 에 포함되어 있으므로 할인가를 제외 한 add_fee 를 계산 한다.
    let reservAddFee = change.default_fee - change.reserv_fee + change.add_fee;

    let prePay = change.prepay_ota_amt - change.prepay_cash_amt - change.prepay_card_amt;

    if (change.reserv_fee) fee_total = change.reserv_fee + reservAddFee - prePay;
    else fee_total = change.default_fee + change.add_fee - prePay;

    console.log("- fee_total", fee_total, prePay);

    if (type === "pay_cash_amt") {
      if (isAll) {
        change.pay_cash_amt = fee_total;
        change.pay_card_amt = 0;
        change.pay_point_amt = 0;
      } else {
        change.pay_cash_amt = fee_total - change.pay_card_amt - change.pay_point_amt;
      }
    }

    if (type === "pay_card_amt") {
      if (isAll) {
        change.pay_cash_amt = 0;
        change.pay_card_amt = fee_total;
        change.pay_point_amt = 0;
      } else {
        change.pay_card_amt = fee_total - change.pay_cash_amt - change.pay_point_amt;
      }
    }

    if (type === "pay_point_amt") {
      if (isAll) {
        // 현금/카드/포인트 혼합 사용 시 포인트 사용 가능.
        if (fee_total > remainPoint) {
          change.pay_point_amt = remainPoint;

          if (isChecked) {
            // 남은 결제 잔액
            let remainPay = fee_total - change.pay_point_amt;

            if (change.pay_cash_amt) {
              let pay = remainPay - change.pay_card_amt;
              if (pay > 0) change.pay_cash_amt = pay;
              else change.pay_card_amt = pay;
            } else if (change.pay_card_amt) {
              let pay = remainPay - change.pay_cash_amt;
              if (pay > 0) change.pay_card_amt = pay;
              else change.pay_cash_amt = pay;
            } else {
              // 체크인 기본 요금 타입 (1: 현금, 2:카드)
              if (preferences.default_pay_type === 1) change.pay_cash_amt = remainPay;
              else change.pay_card_amt = remainPay;
            }
          }
        } else {
          change.pay_cash_amt = 0;
          change.pay_card_amt = 0;
          change.pay_point_amt = fee_total;
        }
      } else {
        let point = fee_total - change.pay_cash_amt - change.pay_card_amt;
        change.pay_point_amt = point > mileage.point ? mileage.point : point;
      }
    }

    console.log("- change", { sale, change });

    if (sale.pay_cash_amt !== change.pay_cash_amt) this.handleInputChange("sale", "pay_cash_amt", change.pay_cash_amt);
    if (sale.pay_card_amt !== change.pay_card_amt) this.handleInputChange("sale", "pay_card_amt", change.pay_card_amt);
    if (sale.pay_point_amt !== change.pay_point_amt) this.handleInputChange("sale", "pay_point_amt", change.pay_point_amt);
  };

  // 현금/카드/포인트 결제금액 설정.
  handlePreferencesSave = (name, value) => {
    console.log("- handlePreferencesSave", name, value);

    const { preferences } = this.props;

    let modifyClone = _.cloneDeep(preferences);
    modifyClone[name] = value;

    if (name === "car_call_display") this.noticeAddCarNo(value === 1);

    if (modifyClone.voice_opt)
      // Object To Json.
      modifyClone.voice_opt = JSON.stringify(modifyClone.voice_opt);

    if (modifyClone.stay_type_color) modifyClone.stay_type_color = JSON.stringify(modifyClone.stay_type_color);
    if (modifyClone.key_type_color) modifyClone.key_type_color = JSON.stringify(modifyClone.key_type_color);
    if (modifyClone.room_name_color) modifyClone.room_name_color = JSON.stringify(modifyClone.room_name_colors);
    if (modifyClone.time_bg_color) modifyClone.time_bg_color = JSON.stringify(modifyClone.time_bg_color);

    this.props.handleSavePreferences(preferences.id, {
      preferences: modifyClone,
    });
  };

  // 달력
  onDateChange = ({ date }) => {
    this._isMounted && this.setState({ date });
  };

  // 달력
  onFocusChange = ({ focused }) => {
    this._isMounted && this.setState({ focused });
  };

  // 예약 적용.
  onSetReserv = (reservId, confirm) => {
    console.log("- onSetReserv", reservId);

    reservId = Number(reservId);

    const {
      preferences: { speed_checkin, stay_time_type },
    } = this.props;

    let { stayTime, rentTime, data, org, reservs } = this.state;

    if (reservId > 0 && org.state.sale > 0) {
      Swal.fire({
        icon: "info",
        title: "사용 알림",
        text: "현재 객실이 사용중 입니다. 퇴실 처리 후 예약을 선택해 주세요.",
        confirmButtonText: "확인",
      }).then((result) => {
        if (result.value) {
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
      return;
    }

    const reserv = reservId > 0 ? _.find(reservs, { id: reservId }) : undefined;

    console.log("- reserv", reserv);

    if (!confirm && !speed_checkin) {
      if (reservId > 0) {
        Swal.fire({
          icon: "info",
          title: "예약 선택 확인",
          html: "객실을 [" + (reserv.name ? reserv.name : reserv.reserv_num) + "] 고객 님의 예약으로 설정 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onSetReserv(reservId, true); // 파라메터를 전송 하기 위해 함수로 갑싼다.
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });
      } else {
        Swal.fire({
          icon: "info",
          title: "예약 선택 취소",
          html: "객실에 설정된 예약 정보를 취소 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onSetReserv(reservId, true);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });
      }
    } else {
      this.handleInputChange("sale", "room_reserv_id", reservId);

      let hour = Number(moment().format("HH"));

      // 예약 정보 업데이트.
      const {
        stay_type = 0,
        person = 2,
        check_out_exp,
        prepay_ota_amt = 0,
        prepay_cash_amt = 0,
        prepay_card_amt = 0,
        prepay_point_amt = 0,
        pay_cash_amt = 0,
        pay_card_amt = 0,
        pay_point_amt = 0,
        room_fee = 0,
        reserv_fee = 0,
        add_fee = 0,
        option_fee = 0,
        memo,
      } = reserv || _.cloneDeep(org.sale); // 예약 정보 취소 시 초기화 정보 설정.

      const sale = _.merge({}, data.sale, {
        room_reserv_id: reservId,
        stay_type,
        person,
        check_out_exp:
          stay_type === 2
            ? moment().add(rentTime, "hour").format("YYYY-MM-DD HH:mm:ss")
            : stay_time_type === 0 // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
            ? moment().add(stayTime, "hour").format("YYYY-MM-DD HH:mm:ss")
            : moment()
                .hour(stayTime)
                .minute(0)
                .add(stayTime > hour ? 0 : 1, "day") // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
                .format("YYYY-MM-DD HH:mm:ss"),
        reserv_fee,
        default_fee: room_fee,
        add_fee: reserv_fee - room_fee + add_fee, // 할인 가.
        option_fee,
        prepay_ota_amt,
        prepay_cash_amt,
        prepay_card_amt,
        prepay_point_amt,
        pay_cash_amt,
        pay_card_amt,
        pay_point_amt,
        memo,
      });

      const state = _.assign({}, data.state, { sale: stay_type || 0 }); // 객실 판매 상태 업데이트.

      console.log("--> room_reserv ", { state, reserv, sale });

      this._isMounted &&
        this.setState(
          {
            data: { ...this.state.data, state, sale },
            reserv,
            savePoint: 0, // 적립금 초기화.
          },
          () => {
            this.setFee(this.props, false, false);
            // console.log("--> room_reserv ", this.state.data.sale);
          }
        );
    }
  };

  // 입실 시간 옵션 적용.
  onSetTimeOption = (timeOptionId, confirm) => {
    console.log("- onSetTimeOption", timeOptionId);

    timeOptionId = Number(timeOptionId);

    const {
      preferences: { speed_checkin },
    } = this.props;

    let { data, org, timeOptions } = this.state;

    const timeOption = timeOptionId > 0 ? _.find(timeOptions, { id: timeOptionId }) : undefined;

    if (!speed_checkin && !confirm) {
      if (timeOptionId > 0) {
        Swal.fire({
          icon: "info",
          title: "입실 시간 옵션 확인",
          html: "객실을 [" + timeOption.name + "] 입실 시간 옵션으로 설정 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onSetTimeOption(timeOptionId, true);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });
      } else {
        Swal.fire({
          icon: "info",
          title: "입실 시간 옵션 취소",
          html: "객실에 설정된 입실 시간 옵션을 취소 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onSetTimeOption(timeOptionId, true);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });
      }
    } else {
      console.log("- timeOption", timeOption);

      let sale = timeOption ? data.sale : _.cloneDeep(org.sale); // 취소 시 초기화 정보 설정.

      if (timeOption) {
        let { stay_type, use_time, use_time_type, add_fee } = timeOption;

        sale.stay_type = stay_type;
        sale.time_option_id = timeOptionId;
        sale.time_option_fee = add_fee;

        let hour = Number(moment().format("HH"));

        // 이용/퇴실 시간 선택 (1: 이용시간, 2: 퇴실시간)
        if (use_time_type === 1) {
          sale.check_out_exp = moment(sale.check_in).add(use_time, "hour");
        } else {
          sale.check_out_exp = moment(sale.check_in)
            .hour(use_time)
            .minute(0)
            .add(use_time > hour ? 0 : 1, "day"); // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
        }
      } else {
        // 취소 시 초기화 정보 설정.
        sale.stay_type = data.sale.stay_type;
        sale.time_option_id = timeOptionId;
        sale.time_option_fee = 0;
        sale.check_out_exp = org.sale.check_out_exp;
      }

      const { stay_type, check_out_exp, time_option_fee, time_option_id } = sale;

      sale = _.assign({}, data.sale, {
        time_option_id,
        check_out_exp: moment(check_out_exp).format("YYYY-MM-DD HH:mm:ss"),
        time_option_fee,
      });

      const state = _.assign({}, data.state, { sale: stay_type || 0 }); // 객실 판매 상태 업데이트.

      console.log("--> time_option ", { sale, state });

      this._isMounted &&
        this.setState(
          {
            data: { ...this.state.data, sale, state },
          },
          () => {
            this.setFee(this.props, true, false);
          }
        );
    }
  };

  // 숙박 형태 변경 시.
  onChangeStay = (evt) => {
    let {
      data: { state },
      changeStay,
    } = this.state;

    console.log("- onChangeStay", changeStay);

    const sale = state.sale === 1 ? 2 : 1;

    this.handleInputChange("state", "prev_sale", state.sale);

    this._isMounted &&
      this.setState(
        {
          changeStay: true,
        },
        () => {
          this.handleInputChange("state", "sale", sale);
        }
      );
  };

  // 숙박/대실 기간 변경.
  onChangeTerm = (value) => {
    let {
      data: { sale, state },
    } = this.state;

    console.log("- onChangeTerm", value);

    let check_out_exp = moment(sale.check_out_exp);

    check_out_exp =
      state.sale === 2
        ? moment(sale.check_in).add(value, "hour").format("YYYY-MM-DD HH:mm:ss")
        : moment(sale.check_in)
            .add(value, "day")
            .hour(check_out_exp.hour()) // 시간/분은 체크아웃 시간 유지.
            .minute(check_out_exp.minute())
            .format("YYYY-MM-DD HH:mm:ss");

    this.handleInputChange("sale", "check_out_exp", check_out_exp);
  };

  // 객실 이동 시.
  openRoomMove = (evt) => {
    this._isMounted &&
      this.setState({
        displayRoomMove: true,
      });
  };

  closeRoomMove = (evt) => {
    const { room } = this.props;

    this._isMounted &&
      this.setState({
        displayRoomMove: false,
      });

    setTimeout(() => {
      this.props.initRoomSale(room.id); // 판매 정보 재조회.
      this.props.initRoomTypeFees(room.room_type_id, 0); // 객실 타입 요일 요금 정보 목록 (적용 채널 (0: 카운터, 1:자판기))
    }, 100);
  };

  onChangeSale = (evt, changeState, type, confirm, reservAlert) => {
    console.log("- onChangeSale", changeState, type, this.state.confirm);

    let { data, org, save, phoneQRNumber } = this.state;

    const {
      room,
      preferences: { speed_checkin, pre_check_in_time_clean, use_sale_cancel_comment },
      roomReserv,
    } = this.props;

    const {
      state: { temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5 },
      sale: { room_reserv_id, check_out_exp },
    } = data;

    const { roomStateItem, roomSaleItem, roomStateItemOrg } = this.state;

    if (changeState === 1) {
      type = type || data.state.sale;

      if (!type) {
        Swal.fire({
          icon: "info",
          title: "입실 확인",
          html: "숙박/대실을 선택해 주세요.",
          showCloseButton: true,
          confirmButtonText: "확인",
        });
        return;
      }

      let isReserv = null;

      _.each(roomReserv.list, (reserv) => {
        // 현재 체크아웃 시간이 예약 체크인 시간과 겹치면 예약 알림!
        if (
          reserv.state === "A" &&
          reserv.room_id === room.id &&
          moment(reserv.check_out_exp).isAfter(moment()) &&
          moment(check_out_exp).add(pre_check_in_time_clean, "minute").isSameOrAfter(moment(reserv.check_in_exp))
        ) {
          isReserv = reserv;
          console.log("- isReserv", reserv);
          return false;
        }
      });

      if (!confirm && !reservAlert && !room_reserv_id && isReserv) {
        let { name, reserv_num, check_in_exp, check_out_exp } = isReserv;

        Swal.fire({
          icon: "info",
          title: "예약 확인",
          html: `<b>[${room.name}]</b> 객실에 <font color='red'>[${name || reserv_num}]</font> 예약이 있습니다.<br/><br/>
          예약 시간 : <b>${moment(check_in_exp).format("YYYY-MM-DD HH:mm:ss")} ~ ${moment(check_out_exp).format("YYYY-MM-DD HH:mm:ss")}</b><br/>
          청소 시간 : ${pre_check_in_time_clean}분<br/><br/>정말 체크인 하시겠습니까?`,
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onChangeSale(evt, changeState, type, true, true);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            this.props.handleDisplayRoomLayer(false);
          }
        });

        return;
      }
    }

    if (!confirm && !speed_checkin) {
      let msg = "";

      msg = org.state.sale > 0 && data.state.key === 1 ? "<font color='red'>객실이 사용중 입니다.</font><br/><br/>" : ""; // 판매중이고 손님 키가 있다면.

      const value = keyToValue("room", "state", changeState);

      if (changeState === 1) {
        msg += " [" + keyToValue("room_state", "sale", type) + "] 체크인을 하시겠습니까?";
      } else if (changeState === 2) {
        msg += " [" + (type === 1 ? "외출" : "외출복귀") + "] 처리를 하시겠습니까?";
      } else if (changeState === 3) {
        if (roomStateItem.key === 3) msg += "현재 <font color='red'>소중</font> 입니다.";
      } else if (changeState === 5) {
        msg += " [" + value + "] 를 하시겠습니까?";
      } else if (changeState === 7) {
        msg += "입실취소 시 <font color='red'>매출도 취소</font> 됩니다. <br/><br/>입실 취소를 하시겠습니까?";
      } else if (changeState === 9) {
      } else {
        msg += " [" + value + "] 처리를 하시겠습니까?";
      }

      if (msg) {
        Swal.fire({
          icon: "info",
          title: "처리 확인",
          html: msg,
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onChangeSale(evt, changeState, type, true, reservAlert);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });

        return;
      }
    }

    console.log("- save changeState", save.state, save.sale, changeState);

    // 중복 일실 필터링.
    if (this.isUpdate) return;

    this.isUpdate = true;

    setTimeout(() => {
      this.isUpdate = false;
    }, 1000);

    if (changeState === 0) {
      // 공실 처리 => state.sale = 0 , state.room_sale_id = 0
      if (!roomSaleItem.check_out) save.sale["check_out"] = moment().format("YYYY-MM-DD HH:mm:ss"); // 체크아웃
      if (roomSaleItem.state === "A") save.sale["state"] = "C"; // 매출 마감.

      // 무조건 공실 처리
      save.state["sale"] = 0; // 판매 상태 (0:없음 1:숙박 2:대실 3:장기)
      save.state["inspect"] = 0; // 인스펙트 초기화.

      if (roomStateItem.room_sale_id) save.state["room_sale_id"] = null; // 매출 키.
      if (roomStateItem.outing) save.state["outing"] = 0; // 외출 없음.
      if (roomStateItem.key !== 3 && roomStateItem.clean) save.state["clean"] = 0; // 청소 완료.
      if (roomStateItem.key > 3 || data.state.key > 3) save.state["key"] = 0; // 키 초기화
      if (roomStateItem.car_call) save.state["car_call"] = 0; // 차량 호출 초기화.
      if (roomStateItem.emerg) save.state["emerg"] = 0; // 비상 호출 초기화.

      // 차량 번호 표시 제거.
      if (roomStateItem.notice && roomSaleItem.car_no && roomStateItem.notice.indexOf(roomSaleItem.car_no) > -1) {
        const regex = new RegExp(roomSaleItem.car_no, "g");
        save.state["notice"] = roomStateItem.notice ? roomStateItem.notice.replace(regex, "").replace(/^\s*/, "") : "";
      }

      // 공실시 온도
      save.state["air_set_temp"] = temp_key_3;
      if (roomStateItem.key > 3) save.state["main_relay"] = 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
      if (!roomStateItem.use_auto_power_off) save.state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      save.state["air_power_type"] = 0; // 예냉 초기화..
    }
    // 입실 처리 => state.sale > 0 , state.room_sale_id > 0
    else if (changeState === 1) {
      save.state["sale"] = type; // 숙박형태
      save.state["inspect"] = 0; // 인스펙트 초기화.

      if (roomStateItem.clean) save.state["clean"] = 0; // 청소 요청 없음.
      if (roomStateItem.key > 3 || data.state.key > 3) save.state["key"] = 0; // 키 초기화
      if (!roomStateItem.use_auto_power_off) save.state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      // 사용중 온도
      save.state["air_set_temp"] = temp_key_1;
    }
    // 외출 처리
    else if (changeState === 2) {
      if (roomStateItem.outing !== type) save.state["outing"] = type; // 외출/외출복귀

      // 외출시 / 외출복귀
      save.state["air_set_temp"] = type === 1 ? temp_key_2 : temp_key_1;
      save.state["air_power_type"] = 0; // 예냉 초기화..
    }
    // 청소요청 처리
    else if (changeState === 3) {
      if (!roomStateItem.clean) save.state["clean"] = 1; // 청소 지시.
      if (roomStateItem.key > 3 || data.state.key > 3) save.state["key"] = 0; // 키 초기화

      // 청소대기 온도
      save.state["air_set_temp"] = temp_key_5;
    }
    // 청소중 처리
    else if (changeState === 4) {
      // 청소중 온도
      save.state["air_set_temp"] = temp_key_4;
    }
    // 청소완료 처리
    else if (changeState === 5) {
      if (roomStateItem.clean) save.state["clean"] = 0; // 청소 요청 없음.

      // 사용중 / 공실시 온도
      save.state["air_set_temp"] = roomStateItem.sale > 0 ? temp_key_1 : temp_key_3;
    }
    // 퇴실 처리
    else if (changeState === 6) {
      if (!roomSaleItem.check_out) save.sale["check_out"] = moment().format("YYYY-MM-DD HH:mm:ss"); // 체크아웃
      if (roomSaleItem.state === "A") save.sale["state"] = "C"; // 매출 마감.

      if (roomStateItem.outing) save.state["outing"] = 0; // 외출 없음.
      if (!roomStateItem.clean) save.state["clean"] = 1; // 청소 지시.

      if (roomStateItem.key > 3) save.state["main_relay"] = 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
      if (!roomStateItem.use_auto_power_off) save.state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      // 청소대기 온도
      save.state["air_set_temp"] = temp_key_5;
      save.state["air_power_type"] = 0; // 예냉 초기화..
    }
    // 입실취소 => state.sale = 0 , state.room_sale_id = 0
    else if (changeState === 7) {
      if (roomSaleItem.state === "A") save.sale["state"] = "B"; // 매출 취소.
      if (roomSaleItem.room_reserv_id) save.sale["room_reserv_id"] = 0; // 예약 키.

      if (roomStateItem.sale) save.state["sale"] = 0; // 공실
      if (roomStateItem.room_sale_id) save.state["room_sale_id"] = null; // 매출 키.
      if (roomStateItem.outing) save.state["outing"] = 0; // 외출 없음.
      if (!roomStateItem.clean) save.state["clean"] = 1; // 청소 지시.
      if (roomStateItem.car_call) save.state["car_call"] = 0; // 차량 호출 초기화.
      if (roomStateItem.emerg) save.state["emerg"] = 0; // 비상 호출 초기화.

      // 차량 번호 표시 제거.
      if (roomStateItem.notice && roomSaleItem.car_no && roomStateItem.notice.indexOf(roomSaleItem.car_no) > -1) {
        const regex = new RegExp(roomSaleItem.car_no, "g");
        save.state["notice"] = roomStateItem.notice ? roomStateItem.notice.replace(regex, "").replace(/^\s*/, "") : "";
      }

      // 청소대기 온도
      save.state["air_set_temp"] = temp_key_5;
      save.sale["rollback"] = 1; // 입실취소.
      save.state["air_power_type"] = 0; // 예냉 초기화..
    }
    // 퇴실 취소
    else if (changeState === 8) {
      // 퇴실 취소 시 원래 정보 업데이트.
      org.sale = data.sale = roomSaleItem;
      save.sale = {};

      if (roomSaleItem.check_out) save.sale["check_out"] = null; // 체크아웃 취소.
      if (roomSaleItem.state !== "A") save.sale["state"] = "A"; // 매출 상태.

      if (roomStateItem.clean) save.state["clean"] = 0; // 청소 없음.
      if (roomStateItem.key < 4) save.state["main_relay"] = 1; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
      if (!roomStateItem.use_auto_power_off) save.state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      save.state["air_set_temp"] = temp_key_1; // 사용중 온도
      save.sale["rollback"] = 2; // 퇴실취소.

      // 퇴실 취소 시 원래 정보 업데이트.
      if (roomStateItem.resale) {
        let { sale, room_sale_id } = roomStateItemOrg;

        save.state["sale"] = sale;
        save.state["room_sale_id"] = room_sale_id;
      }
    }
    // 사용중 체크아웃
    else if (changeState === 9) {
      if (!roomSaleItem.check_out) save.sale["check_out"] = moment().format("YYYY-MM-DD HH:mm:ss"); // 체크아웃
      if (roomSaleItem.state === "A") save.sale["state"] = "C"; // 매출 마감.
      if (roomStateItem.use_auto_power_off) save.state["use_auto_power_off"] = 0; // 자동 전원 차단 사용 안함.
    }

    console.log("- save room_state", save.state);
    console.log("- save room_sale", save.sale);

    if (changeState === 0 && roomStateItem.sale > 0 && roomSaleItem.state === "A") {
      Swal.fire({
        icon: "warning",
        title: "공실 처리 확인",
        html: `<font color=red><b>객실이 사용중</b></font> 입니다. <br/><br/>공실 처리 하시겠습니다.`,
        showCancelButton: true,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
      }).then((result) => {
        console.log("- result", result);
        if (result.value) {
          this.setState(
            {
              roomStateItem: roomStateItemOrg,
              data,
              org,
            },
            () => {
              this.handleSave(save.state, save.sale);
            }
          );
        }
      });
    } else if (changeState === 7 && use_sale_cancel_comment) {
      Swal.fire({
        input: "text",
        title: "취소 사유 입력",
        html: `<font color=red><b>입실 취소 사유</b></font>를 입력해 주세요`,
        inputPlaceholder: "내용 입력",
        inputAttributes: {
          maxlength: 100,
        },
        inputValue: "",
        inputValidator: (value) => {
          if (!value) {
            return "내용을 입력해 주세요!";
          }
        },
        showCancelButton: true,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
      }).then((result) => {
        console.log("- result", result);
        if (!result.dismiss) {
          if (result.value) {
            // 취소 사유.
            save.sale.comment = result.value;

            this.setState(
              {
                roomStateItem: roomStateItemOrg,
                data,
                org,
              },
              () => {
                this.handleSave(save.state, save.sale);
              }
            );
          }
        }
      });
    } else {
      // 바로 실행.
      this.setState(
        {
          roomStateItem: roomStateItemOrg,
          data,
          org,
        },
        () => {
          this.handleSave(save.state, save.sale);
        }
      );
    }
  };

  onExtendStay = (evt) => {
    this._isMounted &&
      Swal.fire({
        title: "사용연장(재입실)을 하시겠습니까?",
        html: `사용연장은 <font color=red>퇴실</font> 처리가 되며, 재입실 처리가 필요 합니다.
          <br/>퇴실 후에도 객실전원이 유지됩니다.
          <br/><br/><font color="#928bf8">단순 이용시간 연장은 퇴실시간을 변경 해주세요.</font>
          <br/><br/><font color="#999">"퇴실" 처리 후 바로 재입실 처리를 해주세요.</font>`,
        icon: "warning",
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: "퇴실",
        cancelButtonText: "취소",
      }).then((result) => {
        if (result.value) {
          this._isMounted &&
            this.setState(
              {
                useCheckout: true,
              },
              () => {
                this.onChangeSale(evt, 9);
              }
            );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          // ignore
        }
      });
  };

  onChangePoint = (savePoint) => {
    this._isMounted &&
      this.setState({
        savePoint,
      });
  };

  onSave = (evt) => {
    const {
      save: { state, sale },
    } = this.state;

    console.log("- onSave ", { state, sale });

    // 요금 재 계산 시 state 변경 딜레이
    setTimeout(() => {
      this.handleSave(state, sale, false, false, false, false, true);
    }, 100);
  };

  handleSave = (room_state, room_sale, pass_check_phone, pass_phone_auth, send_auth_no, confirm, no_change_sale) => {
    let { data, org, mileage, useCheckout, savePoint, phoneNumber } = this.state;

    const {
      preferences: { speed_checkin, day_sale_end_time, place_id, point_use_type, point_check_phone },
    } = this.props;

    // save 이외에 변경 사항 체크.
    _.map(Object.keys(data.state), (key) => {
      if (key !== "sale" && String(data.state[key]) !== String(org.state[key])) {
        room_state[key] = data.state[key];
      }
    });

    // 입실/퇴실 취소시 매출 변경은 저장 하지 않는다.
    if (!room_sale.rollback) {
      _.map(Object.keys(data.sale), (key, value) => {
        if (String(data.sale[key]) !== String(org.sale[key])) {
          // console.log("- key ", key, data.sale[key]);
          room_sale[key] = data.sale[key];
        }
      });
    }

    console.log("- handleSave ", { room_state, room_sale });

    const isNew = room_state && room_state.sale > 0 && !room_sale.rollback && (!org.sale.check_in || org.sale.check_out); // 매출 신규 (매출 변경 시 체크)

    // 요금 변경 시 미수금/반환금 체크.
    let pay_amt = data.sale.pay_card_amt + data.sale.pay_cash_amt + data.sale.pay_point_amt + data.sale.prepay_card_amt + data.sale.prepay_cash_amt + data.sale.prepay_ota_amt;

    // 예약은 예약 할인 가격이 add_fee 에 포함되어 있으므로 할인가를 제외 한 add_fee 를 계산 한다.
    let reservAddFee = data.sale.default_fee - data.sale.reserv_fee + data.sale.add_fee;

    // 결제 미수 금액.
    let receptAmt = data.sale.default_fee + data.sale.add_fee - pay_amt;
    if (data.sale.reserv_fee) receptAmt = data.sale.reserv_fee + reservAddFee - pay_amt;

    // 공실 처리 아닐때 체크.
    if (room_state.sale > 0) {
      let text = "";

      if (receptAmt > 0) {
        text = `결제 금액이 <font color='red'>${String(Math.abs(receptAmt)).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</font>원 부족 합니다.<br/><br/>${
          org.state.sale === 0 ? "체크인을 하시겠습니까?" : "변경 하시겠습니까?"
        }`;
      } else if (receptAmt < 0) {
        text = `결제 금액이 <font color='red'>${String(Math.abs(receptAmt)).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</font>원 초과 되었습니다.<br/><br/>${
          org.state.sale === 0 ? "체크인을 하시겠습니까?" : "변경 하시겠습니까?"
        }`;
      } else {
        text = !speed_checkin && no_change_sale ? "변경 사항을 저장 하시겠습니까?" : "";
      }

      if (text && !confirm) {
        var elem = document.createElement("div");
        elem.innerHTML = text;

        swal({
          title: "처리 확인",
          icon: "warning",
          content: elem,
          buttons: {
            cash: receptAmt !== 0 && {
              text: receptAmt > 0 ? "현금결제" : "카드차감",
              value: "cash",
              className: "green-btn",
            },
            card: receptAmt !== 0 && {
              text: receptAmt > 0 ? "카드결제" : "현금차감",
              value: "card",
              className: "blue-btn",
            },
            confirm: {
              text: room_state.sale ? "체크인" : "저장",
              value: "confirm",
              className: "btn-set-confirm",
            },
            cancle: {
              text: "아니오",
              value: "cancle",
              className: "btn-set-cancel",
            },
          },
          closeOnClickOutside: true,
        }).then((value) => {
          switch (value) {
            case "confirm":
              this.handleSave(room_state, room_sale, pass_check_phone, pass_phone_auth, send_auth_no, true);
              break;
            case "cancle":
              break;
            case "cash":
              this.handlePayCheck(receptAmt > 0 ? "pay_cash_amt" : "pay_card_amt", false);
              this.handleSave(this.state.save.state, this.state.save.sale, pass_check_phone, pass_phone_auth, send_auth_no, true);
              break;
            case "card":
              this.handlePayCheck(receptAmt > 0 ? "pay_card_amt" : "pay_cash_amt", false);
              this.handleSave(this.state.save.state, this.state.save.sale, pass_check_phone, pass_phone_auth, send_auth_no, true);
              break;
            default:
          }
        });

        return;
      }
    }

    let isChangeAmt = room_sale.pay_cash_amt || room_sale.pay_card_amt || room_sale.pay_point_amt || room_sale.add_fee;
    let isCheckPhone = isNew ? isChangeAmt : org.state.sale && isChangeAmt;

    // 포인트 사용 시
    if (point_use_type > 0) {
      // 포인트 적립 시 전화번호 입력 체크.
      if (!pass_check_phone && !format.isPhone(phoneNumber) && point_check_phone && isCheckPhone && savePoint) {
        Swal.fire({
          title: "휴대전화 번호 확인",
          html: "포인트 적립을 위해 휴대전화 번호를 입력 하시겠습니까?.",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            this.handleSave(room_state, room_sale, true);
          }
        });

        return;
      }

      // 등록된 휴대전화가 아니라면 등록 시킨다.
      if (format.isPhone(phoneNumber) && !mileage.id) {
        this.pointSearch(true);
      }

      // 포인트 사용시 인증(포인트 사용을 하려면 이미 휴대전화 번호 조회를 한 상태임)
      let isPhoneAuth = room_sale.pay_point_amt && !org.sale.phone;

      console.log("- isPhoneAuth", isPhoneAuth, phoneNumber);

      // 포인트로 결제 시 휴대전화 번호 인증 무조건 체크.
      if (!pass_phone_auth && isPhoneAuth) {
        if (format.isPhone(phoneNumber)) {
          // 개발 시 인증번호 전송 안함!
          if (process.env.REACT_APP_MODE !== "PROD") {
            send_auth_no = "00000";

            Swal.fire("인증 완료", "휴대전화 인증이 완료 되었습니다", "success").then((result) => {
              this.handleSave(room_state, room_sale, true, true, send_auth_no);
            });
          } else {
            if (!send_auth_no) {
              // 인증 번호 발송.
              this.props.handleSmsSendAuthNo(phoneNumber, (res) => {
                send_auth_no = res;
              });
            }

            Swal.fire({
              title: "휴대전화 번호 인증",
              html: `포인트 결제 시 본인 확인을 해야 합니다.<br/>고객님의 휴대전화로 인증번호를 발송 했습니다.`,
              input: "text",
              inputPlaceholder: "인증번호 5자리 숫자를 입력해 주세요",
              inputAttributes: {
                maxlength: 5,
              },
              showCancelButton: true,
              cancelButtonText: "인증번호 재발송",
              confirmButtonText: "확인",
            }).then((result) => {
              if (result.value) {
                console.log("- send_auth_no", send_auth_no, result.value, process.env.REACT_APP_MODE);
                if (send_auth_no === result.value) {
                  Swal.fire("인증 완료", "휴대전화 인증이 완료 되었습니다", "success").then((result) => {
                    this.handleSave(room_state, room_sale, true, true, send_auth_no);
                  });
                } else {
                  Swal.fire("인증 오류!", "인증번호가 올바르지 않습니다.", "error").then((result) => {
                    this.handleSave(room_state, room_sale, true, false, send_auth_no);
                  });
                }
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                // 인증번호 재발송
                this.handleSave(room_state, room_sale, true, false, null);
              }
            });
          }
        } else {
          Swal.fire({
            title: "휴대전화 번호 확인",
            text: "올바른 휴대전화 번호를 입력 해주세요.",
            confirmButtonText: "확인",
          });
        }

        return;
      }
    }

    console.log("- handleSave ", { isNew });

    // 매출 신규.
    if (isNew) {
      // 매출 정보 추가.
      room_sale = _.merge({}, room_sale, data.sale);

      this.newSale(room_state, room_sale, (err) => {
        if (!err) this.onClose(true);
      });
    } else {
      console.log("- data len ", Object.keys(room_state).length, Object.keys(room_sale).length);

      // 상태 수정.
      if (Object.keys(room_state).length) {
        this.updateState(room_state, (err) => {
          if (!err) this.onClose(true);
        });
      }

      // 매출 수정.(퇴실 시 정보 수정 금지)
      if (Object.keys(room_sale).length) {
        this.updateSale(room_sale, (err) => {
          if (!err) this.onClose(true);
        });
      }
    }

    if (Object.keys(room_sale).length) {
      setTimeout(() => {
        let start = moment({
          hour: day_sale_end_time,
          minute: 0,
        });

        if (start > moment()) {
          // 현재 시간 이전이라면 어제 일자로.
          start = start.add(-1, "day");
        }

        // 일일 업소 매출.
        this.props.initPlaceRoomSale(place_id, start.format("YYYY-MM-DD HH:mm:ss"), moment().format("YYYY-MM-DD HH:mm:ss"));

        // 일일 객실별 매출
        this.props.initPlaceRoomSaleRoom(place_id, start.format("YYYY-MM-DD HH:mm:ss"), moment().format("YYYY-MM-DD HH:mm:ss"));
      }, 1000);
    }
  };

  newSale = (room_state, room_sale, callback) => {
    console.log("- newSale", { room_state, room_sale });

    const {
      data,
      org,
      mileage: { phone },
      savePoint,
      phoneNumber,
    } = this.state;

    const {
      state: { temp_key_1 },
    } = data;

    const {
      preferences: { place_id },
      user,
    } = this.props;

    room_sale.id = null;
    room_sale.channel = "web"; // 매출 채널 (web, isc, api)
    room_sale.room_id = org.state.room_id;
    room_sale.state = "A"; // 정상
    room_sale.stay_type = room_state.sale; // 숙박 형태.
    room_sale.user_id = user.id; // 현재 근무자 id

    // console.log("- save_point", room_sale.phone, room_sale.save_point, { savePoint });

    if (format.isPhone(phoneNumber)) {
      if (phoneNumber) room_sale.phone = phone; // 고객 전화번호
      if (phoneNumber && savePoint) room_sale.save_point = savePoint; // 적립 포인트
    } else {
      room_sale.save_point = 0;
      room_sale.phone = "";
    }

    room_sale.check_in = moment().format("YYYY-MM-DD HH:mm:ss"); // 입실 시 체크인.
    room_sale.check_in_exp = moment(room_sale.check_in_exp || new Date()).format("YYYY-MM-DD HH:mm:ss");
    room_sale.check_out = null;
    room_sale.check_out_exp = moment(room_sale.check_out_exp).format("YYYY-MM-DD HH:mm:ss");

    console.log("- newRoomSale", room_sale);

    // 새로운 매출 발생.(신규 매출 id -> room_state room_sale_id 에 매핑)
    this.props.handleNewRoomSale({ room_sale }, ({ info, error }) => {
      if (!error) {
        // 매출 id
        room_state.room_sale_id = info.insertId;
        room_state.air_set_temp = temp_key_1; // 사용중/청소중 온도

        console.log("- handleSaveRoomState", org.state.room_id, room_state);

        // 매출 정보를 객실에 연결.
        this.props.handleSaveRoomState(org.state.room_id, {
          room_state,
        });

        const { room_reserv_id, stay_type, room_id } = room_sale;

        // 예약 객실 배정.
        if (room_reserv_id) {
          const room_reserv = {
            state: "C",
            stay_type,
            room_id,
          }; // 예약 상태 (A: 정상, B: 취소, C: 입실)
          this.props.handleSaveRoomReserv(room_reserv_id, {
            room_reserv,
          });
        }

        if (phone) {
          // 포인트 사용 시 차감.
          if (room_sale.pay_point_amt) this.props.handleDecreaseMileage(place_id, phone, user.id, room_sale.pay_point_amt);

          setTimeout(() => {
            // 포인트 적립 시 증가.
            if (savePoint) this.props.handleIncreaseMileage(place_id, phone, user.id, savePoint);
          }, 100);
        }
      }

      callback(error);
    });
  };

  updateSale = (room_sale, callback) => {
    const {
      data,
      org,
      roomSaleItem,
      mileage: { phone },
      savePoint,
      phoneNumber,
      reserv,
    } = this.state;

    const {
      preferences: { place_id },
      user,
    } = this.props;

    if (!data.sale.id) {
      callback(false);
    } else {
      room_sale.channel = "web"; // 매출 채널 (web, isc, api)
      room_sale.id = data.sale.id;
      room_sale.room_id = data.sale.room_id;
      room_sale.user_id = user.id; // 현재 직원 id

      if (format.isPhone(phoneNumber)) {
        if (phoneNumber) room_sale.phone = phone; // 고객 전화번호
        if (phoneNumber && savePoint) room_sale.save_point = savePoint; // 적립 포인트
      } else {
        room_sale.save_point = 0;
        room_sale.phone = "";
      }

      // 숙박형태 변경 시 이전 숙박 형태 저장.
      if (room_sale.stay_type && room_sale.stay_type !== org.sale.stay_type) room_sale.prev_stay_type = org.sale.stay_type;

      console.log("- updateSale", room_sale);

      // 매출 수정
      this.props.handleSaveRoomSale(room_sale.id, { room_sale }, ({ info, error }) => {
        if (!error) {
          const { rollback } = room_sale;

          // 입실 취소 시 예약 상태 롤백.
          if (roomSaleItem.room_reserv_id && rollback === 1) {
            let room_reserv = { state: "A", room_id: null }; // 예약 상태 (A: 정상, B: 취소, C: 입실)

            this.props.handleSaveRoomReserv(roomSaleItem.room_reserv_id, {
              room_reserv,
            });
          }

          if (phone) {
            // 입실 취소 시
            if (rollback === 1) {
              if (org.sale.pay_point_amt) this.props.handleIncreaseMileage(place_id, phone, user.id, org.sale.pay_point_amt, rollback);
              setTimeout(() => {
                if (org.sale.save_point) this.props.handleDecreaseMileage(place_id, phone, user.id, org.sale.save_point, rollback);
              }, 100);
            } else {
              // 포인트 사용 시
              let pay_point = org.sale.pay_point_amt - room_sale.pay_point_amt;

              // 사용 포인트 줄었을때 증가
              if (pay_point > 0) this.props.handleIncreaseMileage(place_id, phone, user.id, pay_point);
              else if (pay_point < 0) this.props.handleDecreaseMileage(place_id, phone, user.id, Math.abs(pay_point));

              setTimeout(() => {
                // 포인트 적립 시.
                let save_point = room_sale.save_point - org.sale.save_point;

                // 적립 포인트 적립 시
                if (save_point > 0) this.props.handleIncreaseMileage(place_id, phone, user.id, save_point);
                else if (save_point < 0) this.props.handleDecreaseMileage(place_id, phone, user.id, Math.abs(save_point));
              }, 100);
            }
          }

          // console.log("- updateSale", rollback, room_sale.check_out);

          // 체크인 상태가 아닐때..
          if (rollback === 1 || room_sale.check_out) {
            // QR 발급 취소(만료)
            this.invalidQrKey(true, false);
          }
        }

        callback(error);
      });
    }
  };

  updateState = (room_state, callback) => {
    const {
      user,
      roomDoorLock: {
        item: { id, room_id, qr_key_data, start_date, end_date, cancel, send_sms },
      },
    } = this.props;

    const { data, save } = this.state;

    if (!data.state.id) return false;

    room_state.channel = "web"; // 매출 채널 (web, isc, api)
    room_state.id = data.state.id;
    room_state.room_id = data.state.room_id;
    room_state.user_id = user.id; // 현재 직원 id

    if (!data.state.qr_key_yn) {
      // QR 코드키 유효 여부
      let isInValid = send_sms && !cancel && moment().isBefore(end_date);

      if (isInValid) room_state.qr_key_yn = 1;
    }

    console.log("- updateState", room_state);

    this.props.handleSaveRoomState(data.state.room_id, { room_state }, ({ info, error }) => {
      if (!error) {
        delete data.state.prev_sale;
        delete save.state.prev_sale;

        this.props.initRoomState(data.state.room_id);
      }

      callback(error);
    });
  };

  onClose = (force, confirm) => {
    const { changeCnt, useCheckout } = this.state;

    console.log("- onClose", { changeCnt, confirm, useCheckout });

    if (this._isMounted) {
      if (!force && changeCnt > 0 && !confirm) {
        Swal.fire({
          icon: "warning",
          title: "취소 확인",
          html: "저장 하지 않은 내용은 취소 됩니다. 취소 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "예",
          cancelButtonText: "아니오",
        }).then((result) => {
          if (result.value) {
            this.onClose(force, true);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
          }
        });
      } else {
        this.setState(
          {
            changeCnt: 0,
            confirm: false,
            useCheckout: false,
          },
          () => {
            clearInterval(this.timer);
            setTimeout(() => {
              this.props.handleDisplayRoomLayer(false);
            }, 100);

            if (useCheckout) {
              setTimeout(() => {
                this.props.handleDisplayRoomLayer(true);
              }, 2000);
            }
          }
        );
      }
    }
  };

  // 요금 계산.
  setFee = (props, first, changSale, callback) => {
    let {
      data: { sale },
      org,
      save,
      mileage,
      changeCnt,
    } = this.state;

    let { default_fee, add_fee, option_fee, pay_cash_amt, pay_card_amt, pay_point_amt, fee, season, usePeriod, stayTime, rentTime } = this.getFee(props, first, changSale);

    if (Number(sale.default_fee) !== Number(default_fee)) save.sale.default_fee = default_fee;
    if (Number(sale.add_fee) !== Number(add_fee)) save.sale.add_fee = add_fee;
    if (Number(sale.option_fee) !== Number(option_fee)) save.sale.option_fee = option_fee;

    // 기본요금/추가요금 매출에 반영.
    sale.default_fee = default_fee;
    sale.add_fee = add_fee;
    sale.option_fee = option_fee;

    if (org.state.sale === 0) {
      sale.pay_cash_amt = pay_cash_amt;
      sale.pay_card_amt = pay_card_amt;
      sale.pay_point_amt = pay_point_amt;
    }

    console.log("--> setFee ", { first, sale });

    let remainPoint = mileage.id ? mileage.point + org.sale.pay_point_amt - sale.pay_point_amt : 0; // 잔여 포인트 계산
    if (remainPoint > mileage.point) remainPoint = mileage.point;

    this._isMounted &&
      this.setState(
        {
          data: { ...this.state.data, sale },
          fee,
          season,
          usePeriod,
          stayTime,
          rentTime,
          remainPoint, // 잔여 포인트 계산
          changeCnt: changeCnt++,
        },
        () => {
          // 포이트 재계산
          this.calcPoint(sale);

          if (callback) callback(sale);
        }
      );
  };

  // 요금 정책.
  getFee = (props, first, changSale) => {
    const { roomFee = [] } = props; // nextProps or this.props
    const { preferences } = this.props;

    let {
      data: { state, sale },
      org,
      roomTypeItem,
    } = this.state;

    // console.log("--> getFee ", sale, { first, changSale });

    // 객실 기본 요금
    let default_fee = state.sale === 2 ? roomTypeItem.default_fee_rent : roomTypeItem.default_fee_stay;
    let reserv_fee = sale.reserv_fee || 0;

    console.log("--> getFee ", { first, default_fee, reserv_fee });

    const card = sale.pay_card_amt > 0 || sale.prepay_card_amt > 0; // 카드 결제 시.
    if (card) default_fee += roomTypeItem.card_add_fee;

    const day = moment().isoWeekday();
    const hour = moment().hour();

    // 기본 숙박 시간
    let stayTime = day > 5 ? preferences.stay_time_weekend : preferences.stay_time;

    // 기본 대실 시간
    let rentTime =
      hour < 12 // 오전
        ? day > 5 // 주말
          ? preferences.rent_time_am_weekend
          : preferences.rent_time_am
        : day > 5
        ? preferences.rent_time_pm_weekend
        : preferences.rent_time_pm;

    rentTime = rentTime < 1 ? 1 : rentTime;

    // 요일 / 시간 요금.
    let fee = undefined;

    let hhmm = moment().format("HHmm");

    // 해당 요일의 해당 시간대의 추가 요금 조회.
    roomFee.forEach(function (v) {
      // 적용 채널 (0: 카운터, 1:자판기)
      if (v.channel === 0 && v.stay_type === state.sale) {
        let st = Number(v.begin);
        let et = Number(v.end);
        let nt = Number(hhmm);

        // console.log("- fee ", v, st, "<=", nt, "~", nt, "<", et);

        // 이전일 ~ 다음일 시간 이라면.
        if (st > et) {
          if (st <= nt || et > nt) {
            fee = v;
          }
        } else {
          if (st <= nt && et > nt) {
            fee = v;
          }
        }
      }
    });

    // console.log("- fee ", fee);

    // 이용 기간(숙박/장기만 해당)
    let usePeriod = moment(moment(sale.check_out_exp).format("YYYYMMDD")).diff(moment(moment(sale.check_in).format("YYYYMMDD")), "day");

    // 0박은 1박으로 한다
    usePeriod = usePeriod || 1;

    // 추가요금에서 옵션 요금 제외 한다.
    let add_fee = sale.add_fee - sale.option_fee;

    // 옵션 요금.
    let option_fee = 0;

    // 현재 요일 시간 추가 요금 적용.
    if (fee) option_fee += fee.add_fee;

    // 입실 시간 옵션 추가 요금 적용.
    if (sale.time_option_fee) option_fee += sale.time_option_fee;

    // 인원 추가 요금.
    if (sale.person > roomTypeItem.person) {
      option_fee += roomTypeItem.person_add_fee * (sale.person - roomTypeItem.person);
    }

    // 시즌 프리미엄 요금 적용 여부.
    let season = this.getSeason();

    // 시즌
    if (!sale.room_reserv_id && season && season.premium) {
      // 입실 시간 기준 일때만 성수기/비성수기 이용 시간 추가.
      if (preferences.stay_time_type === 0 && season.add_stay_time !== 0) stayTime += season.add_stay_time;
      if (season.add_rent_time !== 0) rentTime += season.add_rent_time;
      // 시즌 추가 요금
      option_fee += season.premium;
    }

    // 숙박 시 사용 일자에 곱한다.
    if (state.sale !== 2 && usePeriod > 1) {
      default_fee *= usePeriod;
    }

    default_fee = sale.change_default_fee ? sale.default_fee : default_fee; // 기본요금 직접 변경 여부(변경 시 현재 값 유지!)
    add_fee += option_fee;

    // 예약 선결제 금액(예약시 포인트 결제는 금지)
    let prepay_amt = sale.prepay_card_amt + sale.prepay_cash_amt + sale.prepay_ota_amt;

    // 예약은 예약 할인 가격이 add_fee 에 포함되어 있으므로 할인가를 제외 한 add_fee 를 계산 한다.
    let reservAddFee = sale.default_fee - sale.reserv_fee + sale.add_fee;

    // 결제 할 금액.
    let pay_amt = default_fee + add_fee - prepay_amt;
    if (reserv_fee) pay_amt = reserv_fee + reservAddFee - prepay_amt;

    let pay_cash_amt = sale.pay_cash_amt;
    let pay_card_amt = sale.pay_card_amt;
    let pay_point_amt = sale.pay_point_amt;
    // let pay_sum = pay_cash_amt + pay_card_amt + pay_point_amt;

    // console.log("- getFee pay_amt", { pay_amt, pay_cash_amt, pay_card_amt, pay_point_amt });

    // 판매 상태가 아니라면 결제 요금을 자동 셋팅 해준다.
    if (org.state.sale === 0) {
      if (changSale || (!pay_cash_amt && !pay_card_amt && !pay_point_amt)) {
        if (first) {
          // 체크인 기본 요금 타입 (1: 현금, 2:카드)
          if (preferences.default_pay_type === 1) pay_cash_amt = pay_amt;
          else pay_card_amt = pay_amt;
        }
      }
    }

    console.log("- getFee ok", {
      default_fee,
      add_fee,
      option_fee,
      pay_cash_amt,
      pay_card_amt,
      pay_point_amt,
      fee,
      season,
      usePeriod,
      stayTime,
      rentTime,
    });

    return {
      default_fee,
      add_fee,
      option_fee,
      pay_cash_amt,
      pay_card_amt,
      pay_point_amt,
      fee,
      season,
      usePeriod,
      stayTime,
      rentTime,
    };
  };

  getSeason = () => {
    const { seasonPremiums } = this.props;
    let {
      data: { sale },
    } = this.state;

    // 이용 기간(숙박/장기만 해당)
    let usePeriod = moment(moment(sale.check_out_exp).format("YYYYMMDD")).diff(moment(moment(sale.check_in).format("YYYYMMDD")), "day");

    // 0박은 1박으로 한다
    usePeriod = usePeriod || 1;

    // 시즌 프리미엄 요금 적용 여부.
    let season = _.filter(seasonPremiums, (v, k) => {
      // console.log("- season", v);
      let isSeason = false;
      for (let i = 0; i <= usePeriod; i++) {
        const mmdd = moment(sale.check_in).add(i, "day").format("MMDD");

        // console.log("- day", mmdd, v.begin, v.end);

        // 기간이 다음 년도 포함 시.
        if (Number(v.begin) > Number(v.end)) {
          isSeason = (Number(mmdd) <= Number("1231") && Number(mmdd) >= Number(v.begin)) || (Number(mmdd) >= Number("0101") && Number(mmdd) <= Number(v.end)); // 시즌에 속하는지 여부.
        } else {
          isSeason = Number(v.begin) <= Number(mmdd) && Number(v.end) >= Number(mmdd); // 시즌에 속하는지 여부.
        }

        if (isSeason) break;
      }
      //  console.log("- isSeason", isSeason);
      return isSeason;
    })[0];

    console.log("- getSeason", season);

    return season;
  };

  inputQRPhone = (phoneQRNumber) => {
    // console.log("- inputQRPhone", phoneQRNumber);

    this._isMounted &&
      this.setState({
        phoneQRNumber,
      });
  };

  sendQrCodeLink = () => {
    console.log("- sendQrCodeLink");

    let {
      data,
      org: {
        state: { qr_key_yn },
      },
      phoneQRNumber,
    } = this.state;

    const {
      place,
      room: { id: room_id, name, doorlock_id },
      roomDoorLock: {
        item: { id, qr_key_data, start_date, end_date, cancel, send_sms },
      },
    } = this.props;

    // 도어락 QR코드 발송 서비스 구독여부.
    if (!CheckSubscribe(this.props, "32")) {
      return;
    }

    if (process.env.REACT_APP_MODE === "LOCAL") {
      phoneQRNumber = "010-9844-0098";
    }

    if (!doorlock_id) {
      Swal.fire({
        icon: "error",
        title: "도어락 오류",
        html: `${name} 객실에는 QR 도어락 정보가 없습니다.`,
        confirmButtonText: "확인",
      });

      return;
    }

    if (!format.isPhone(phoneQRNumber)) {
      Swal.fire({
        icon: "error",
        title: "고객 전봐번호 오류",
        html: `고객 전화번호가 올바르지 않습니다.`,
        confirmButtonText: "확인",
      });

      return;
    }

    phoneQRNumber = format.toPhone(phoneQRNumber);

    Swal.fire({
      icon: "question",
      title: "QR 코드키 전송 확인",
      html: `<font color="green">${phoneQRNumber}</font> 번이 <font color="red"><b>${name}</b></font> 객실 이용고객 번호가 맞습니까?<br/><br/>QR 코드키를 고객번호로 전송 하시겠습니까?`,
      showCloseButton: false,
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        let startDate = moment(data.sale.check_in || data.sale.check_in_exp);
        let endDate = moment(data.sale.check_out_exp);
        let deviceId = doorlock_id; // 도어락 id

        global.yYDoorLock.makeQRCodeQuerys(startDate, endDate, deviceId, true).then((query) => {
          // console.log("- makeQRCodeQuerys", query);

          let room_doorlock = { qr_key_phone: phoneQRNumber, qr_key_data: "", start_date: startDate, end_date: endDate, cancel: 0 };

          this.props.handleSendQrCode(id, { data: { query, room_doorlock, place_name: place.name, pnone: phoneQRNumber } }).then(({ success, error, qr_link }) => {
            // console.log("- handleSaveRoom", success, error, qr_link);

            if (success) {
              this.props.initRoomDoorLock(room_id);

              // QR 코드키 유효 여부
              if (!qr_key_yn) this.handleInputChange("state", "qr_key_yn", 1);

              let html = `${phoneQRNumber} 로 QR 코드 키를 발송 했습니다.`;

              // 개발/테스트 시 SMS 문자 발송 표시
              if (process.env.REACT_APP_MODE !== "PROD") {
                html += `<br/><br/>- 테스트 SMS -<br/><font color="blue">${qr_link}</font>`;
              }

              Swal.fire({
                icon: "success",
                title: "QR 코드 키 알림",
                html,
                confirmButtonText: "확인",
              });
            } else {
              let { detail = "" } = error || {};

              Swal.fire({
                icon: "error",
                title: "QR 코드 생성 실패",
                html: detail,
                confirmButtonText: "확인",
              });
            }
          });
        });
      }
    });
  };

  cancelQrCode = () => {
    console.log("- cancelQrCode");

    let { phoneQRNumber } = this.state;

    const {
      room: { name, doorlock_id },
      roomDoorLock: {
        item: { id, qr_key_phone },
      },
    } = this.props;

    if (!doorlock_id) {
      Swal.fire({
        icon: "error",
        title: "도어락 오류",
        html: `${name} 객실에는 QR 도어락 정보가 없습니다.`,
        confirmButtonText: "확인",
      });

      return;
    }

    if (!qr_key_phone) {
      Swal.fire({
        icon: "error",
        title: "도어락 오류",
        html: `${name} 객실로 발급된 QR 코드가 없습니다.`,
        confirmButtonText: "확인",
      });

      return;
    }

    Swal.fire({
      icon: "question",
      title: "QR 취소 확인",
      html: `[${name}] 발급된 QR 코드를 취소 하시겠습니까?`,
      showCloseButton: false,
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        // QR 발급 취소(만료)
        this.invalidQrKey(true, true);
      }
    });
  };

  invalidQrKey = (isCancel, isAlert, count) => {
    console.log("- invalidQrKey", { isCancel, isAlert, count });

    const {
      room: { id: room_id, name, doorlock_id },
      roomDoorLock: {
        item: { id, qr_key_phone, qr_key_data, start_date, end_date, cancel },
      },
    } = this.props;

    let {
      org: {
        state: { qr_key_yn },
      },
    } = this.state;

    // 유효 시간을 만료 시킴
    if (doorlock_id && qr_key_phone && qr_key_data) {
      let isValid = isCancel ? cancel === 0 : moment().isSameOrBefore(end_date);

      console.log("- isValid", isValid);

      if (isValid) {
        // 취소는 STARTDATE 를 -시간으로 설정함
        let startDate = moment().add("minute", -10);
        let endDate = moment().add("minute", -1);
        let deviceId = doorlock_id; // 도어락 id

        global.yYDoorLock.makeQRCodeQuerys(startDate, endDate, deviceId, true).then((query) => {
          console.log("- makeQRCodeQuerys", query);

          if (query) {
            //  도어락 만료 시킴(삭제하면 qr 코드 안보이므로 재발행함).
            let room_doorlock = { start_date: startDate, end_date: endDate };
            if (isCancel) room_doorlock.cancel = 1;

            this.props.handleSendQrCode(id, { data: { query, room_doorlock } }).then(({ success, error, qr_link }) => {
              console.log("- handleSaveRoom", success, qr_link);

              if (success) {
                this.props.initRoomDoorLock(room_id);

                // QR 코드키 유효 여부
                if (qr_key_yn) this.handleInputChange("state", "qr_key_yn", 0);

                if (isAlert) {
                  Swal.fire({
                    icon: "success",
                    title: "QR 코드 취소 알림",
                    html: `QR 코드 키를 취소 했습니다.`,
                    confirmButtonText: "확인",
                  });
                }
              } else {
                let { detail = "" } = error || {};

                Swal.fire({
                  icon: "error",
                  title: "QR 코드 취소 실패",
                  html: detail,
                  confirmButtonText: "확인",
                });
              }
            });
          }
        });
      }
    }
  };

  inputPhone = (phoneNumber) => {
    // console.log("- inputPhone", phoneNumber);
    this._isMounted &&
      this.setState({
        phoneNumber,
      });
  };

  pointSearch = (isPointSearch) => {
    const {
      preferences: { place_id },
    } = this.props;

    let { phoneNumber } = this.state;

    console.log("- pointSearch", phoneNumber);

    if (!format.isPhone(phoneNumber)) {
      Swal.fire({
        icon: "info",
        title: "휴대전화 번호 확인",
        html: "휴대전화 번호를 확인 하세요",
        showCloseButton: true,
        confirmButtonText: "확인",
      });

      return false;
    } else {
      this._isMounted &&
        this.setState(
          {
            isPointSearch,
          },
          () => {
            // 포인트 조회.
            this.props.initMileage(place_id, format.toNumber(phoneNumber));
          }
        );
    }

    return true;
  };

  // 포인트 계산
  calcPoint = (sale) => {
    let { changeCnt, phoneNumber } = this.state;
    const {
      preferences: { cash_point_rate, card_point_rate, reserv_point_rate, pay_add_point },
    } = this.props;

    // 적립 포인트 계산.
    let savePoint = 0;

    let cash = sale.prepay_cash_amt + sale.pay_cash_amt;
    let card = sale.prepay_card_amt + sale.pay_card_amt;

    if (cash || card) {
      savePoint += cash * (cash_point_rate / 100);
      savePoint += card * (card_point_rate / 100);

      if (sale.prepay_cash_amt || sale.prepay_card_amt) savePoint += (cash + card) * (reserv_point_rate / 100); // 예약 시 추가 포인트 적립
      if (pay_add_point !== 0 && savePoint) savePoint += pay_add_point; // 추가 포인트 적립

      savePoint = parseInt(String(savePoint), 0);
    }

    console.log("- savePoint", savePoint, { cash, card, pay_add_point });

    //  적립 포인트 저장
    if (format.isPhone(phoneNumber)) {
      this.handleInputChange("sale", "save_point", savePoint);
    }

    this._isMounted &&
      this.setState({
        savePoint,
        changeCnt: changeCnt + 1,
      });
  };

  // 매출 기본 정보 조회.
  getDefaultSale = () => {
    const { preferences } = this.props;

    let { roomStateItem } = this.state;

    const day = moment().isoWeekday();
    let hour = moment().hour();

    // 기본 숙박 시간
    let stayTime =
      day > 5 // 주말
        ? preferences.stay_time_weekend
        : preferences.stay_time;

    // 기본 대실 시간
    let rentTime =
      day > 5 // 주말
        ? hour < 12
          ? preferences.rent_time_am_weekend
          : preferences.rent_time_pm_weekend
        : hour < 12
        ? preferences.rent_time_am
        : preferences.rent_time_pm;

    let cloneRoomState = _.cloneDeep(roomStateItem);

    // 공실 || 재입실
    if (!cloneRoomState.sale && !cloneRoomState.room_sale_id) {
      // 숙박/대실 시간대 (대실 가능 시간 이외는 숙박으로 처리)
      const check_in_type = hour >= Number(preferences.use_rent_begin) && hour < Number(preferences.use_rent_end) ? 2 : 1;
      cloneRoomState.sale = check_in_type;

      if (check_in_type === 1 && preferences.stay_time_type === 0 && moment().add(stayTime, "hour").format("DD") === moment().format("DD")) {
        // 입실 시간 기준 퇴실 일이 오늘 날자면 대실로 설정.
        cloneRoomState.sale = 2;
      }
    }

    // 매출 기본 정보.
    const defaultSale = {
      state: undefined, // 미판매 상태.
      stay_type: undefined, // 미입실 상태.
      person: 2,
      check_in: undefined,
      check_out: undefined,
      check_in_exp: moment().format("YYYY-MM-DD HH:mm:ss"),
      check_out_exp:
        cloneRoomState.sale === 2
          ? moment().add(rentTime, "hour").format("YYYY-MM-DD HH:mm:ss")
          : preferences.stay_time_type === 0 // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
          ? moment().add(stayTime, "hour").format("YYYY-MM-DD HH:mm:ss")
          : moment()
              .hour(stayTime)
              .minute(0)
              .add(stayTime > hour ? 0 : 1, "day") // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
              .format("YYYY-MM-DD HH:mm:ss"),
      room_reserv_id: 0,
      default_fee: 0,
      add_fee: 0,
      option_fee: 0,
      pay_card_amt: 0,
      pay_cash_amt: 0,
      pay_point_amt: 0,
      prepay_ota_amt: 0,
      prepay_cash_amt: 0,
      prepay_card_amt: 0,
      prepay_point_amt: 0,
      alarm: "0",
      alarm_hour: 9,
      alarm_min: 0,
      time_option_fee: 0, // 시간 옵션 add_fee (sale field 아님, add_fee 계산용)
      phone: "",
      save_point: 0, // 적립 포인트
    };

    console.log("- getDefaultSale", { defaultSale, cloneRoomState });

    this.setState({
      stayTime,
      rentTime,
    });

    return { defaultSale, cloneRoomState };
  };

  // 입실 정보 초기화.
  onSetInit = (callback) => {
    const { room, roomReserv, preferences } = this.props;
    let { roomStateItem, roomSaleItem } = this.state;

    console.log("- onSetInit", { roomStateItem, roomSaleItem });

    // 매출 기본 정보.
    let { defaultSale, cloneRoomState } = this.getDefaultSale();

    // 초기 객실 상태 설정.
    this._isMounted &&
      this.setState(
        {
          stayTypeColor: preferences.stay_type_color,
          data: {
            room,
            state: cloneRoomState,
            sale:
              roomSaleItem && roomSaleItem.id && !roomStateItem.resale
                ? _.cloneDeep(roomSaleItem) // roomSaleItem.list 에서 참조한 데이터 있으면 가져다 쓴다.
                : _.merge({}, defaultSale, {
                    check_in: moment().format("YYYY-MM-DD HH:mm:ss"),
                  }),
          },
          org: {
            state: _.cloneDeep(roomStateItem),
            sale: roomSaleItem && roomSaleItem.id ? _.cloneDeep(roomSaleItem) : _.merge({}, defaultSale),
          },
          reservs: roomReserv,
          changeCnt: 0,
          changeStay: false,
          displayRoomMove: false, // 객실 이동 팝업.
        },
        () => {
          // 시즌 프리미엄 요금 적용 여부.
          let season = this.getSeason();

          this.setState({
            season,
          });

          if (callback) callback();
        }
      );
  };

  componentDidMount = () => {
    // console.log("--- componentDidMount roomTab01 ");

    this._isMounted = true;

    let { phoneNumber, savePoint } = this.state;

    const {
      preferences: { place_id, doorlock_send_qr_code },
      room,
      roomState,
      roomSale,
      roomFee,
      roomType,
      roomReserv,
      roomDoorLock,
      device,
    } = this.props;

    let roomStateItem =
      _.find(roomState.list, {
        room_id: room.id,
      }) || {}; // 현재 목록에서 조회.

    let roomSaleItem = _.find(roomSale.list, { id: roomStateItem.room_sale_id }) || {}; // 현재 목록에서 조회.

    // 현재 화면만 적용을 위해 복제 해서 사용 한다.
    roomStateItem = _.cloneDeep(roomStateItem);
    roomSaleItem = _.cloneDeep(roomSaleItem);

    // 객실 타입 정보.
    let roomTypeItem =
      _.find(roomType.list, {
        id: room.room_type_id,
      }) || {}; // 현재 목록에서 조회.

    console.log("--- componentDidMount RoomTab01 ", {
      roomStateItem,
      roomSaleItem,
      roomTypeItem,
      roomFee,
    });

    // 원본 저장(퇴실 취소 시 사용)
    let roomStateItemOrg = _.cloneDeep(roomStateItem);

    roomStateItem.resale = 0;

    if (roomSaleItem.phone) phoneNumber = roomSaleItem.phone;
    if (roomSaleItem.save_point) savePoint = roomSaleItem.save_point;

    // 퇴실 상태 일때는 결제 요금 정보를 초기화 한다.
    if (roomSaleItem && roomSaleItem.state === "C") {
      roomStateItem.sale = 0;
      roomStateItem.room_sale_id = 0;
      roomStateItem.resale = 1;
    }

    // 객실 예약 정보.
    const reserv = roomSaleItem && roomSaleItem.room_reserv_id ? _.find(roomReserv.list, { id: roomSaleItem.room_reserv_id }) : undefined;
    console.log("--> reserv", reserv);

    if (reserv) {
      // roomSaleItem.default_fee = reserv.stay_type === 2 ? reserv.default_fee_rent : reserv.default_fee_stay; //  기본 요금(객실 타입에서 가져온다)
      roomSaleItem.default_fee = reserv.room_fee; //  판매 요금을 기본요금 으로 한다.
      roomSaleItem.reserv_fee = reserv.reserv_fee;
      roomSaleItem.room_fee = reserv.room_fee;
    }

    // 객실 예약 정보
    let filter = `a.place_id=${room.place_id} `;
    let begin = moment().add(-1, "day").format("YYYY-MM-DD 00:ss");
    let end = moment().add(1, "month").format("YYYY-MM-DD 00:ss");

    // 예약 정보.
    this.props.initRoomReservs(filter, begin, end);

    // 입실 옵션 정보
    this.props.initTimeOptions(place_id);

    // 마일리지 포인트 조회.
    if (phoneNumber) this.props.initMileage(place_id, format.toNumber(phoneNumber));

    // 장비 정보
    const { idm, isc } = device;

    // 구독 서비스 정보.
    this.props.initAllPlaceSubscribes(place_id);

    // 마일리지 포인트 서비스 구독여부.
    let isSubscribePoint = CheckSubscribe(this.props, "02", false);

    // 도어락 정보 조회.(QR 도어락 사용 시)
    if (doorlock_send_qr_code && room.doorlock_id) {
      this.props.initRoomDoorLock(room.id);
    }

    // 입실 정보 초기화.
    this._isMounted &&
      this.setState(
        {
          roomStateItemOrg,
          roomStateItem,
          roomSaleItem,
          roomTypeItem,
          isSubscribePoint,
          phoneNumber,
          savePoint,
          reserv,
          idm,
          isc,
        },
        () => {
          setTimeout(() => {
            this.onSetInit(() => {
              // 체크인/재입실 시 요금 설정.
              if (!roomStateItem.room_sale_id) {
                this.setFee(this.props, true, true, (sale) => {
                  // 적립 포인트 계산.
                  this.calcPoint(sale);
                });
              }
            }, 100);
          });
        }
      );

    // 초 단위 타이머.
    this.timer = setInterval(() => {
      let {
        org: { sale },
      } = this.state;

      //  console.log("--> ", sale.check_in, sale.check_out, state.sale);

      if (sale && sale.id) {
        // 경과 시간.
        const checkInMin = moment().diff(sale.check_in || new Date(), "minute");
        const checkOutMin = moment().diff(sale.check_out || new Date(), "minute");

        let checkInCancel = false;
        let checkOutCancel = false;

        // 입실 취소 가능 시간 체크.
        if (sale.check_in && !sale.check_out) {
          if (!checkInCancel && checkInMin < Number(this.props.preferences.cancel_time)) {
            checkInCancel = true;
          }
        }

        if (this.state.checkInCancel !== checkInCancel) {
          this._isMounted &&
            this.setState({
              checkInCancel,
            });
        }

        // 퇴실 취소 가능 시간 체크.
        if (sale.check_out) {
          if (!checkOutCancel && checkOutMin < Number(this.props.preferences.check_out_cancle_time)) {
            checkOutCancel = true;
          }
        }

        if (this.state.checkOutCancel !== checkOutCancel) {
          this._isMounted &&
            this.setState({
              checkOutCancel,
            });
        }
      }
    }, 1000);
  };

  componentWillUnmount() {
    let {
      roomStateItem: { room_id, sale, qr_key_yn },
    } = this.state;

    const {
      roomDoorLock: {
        item: { id, qr_key_data, start_date, end_date, cancel, send_sms },
      },
    } = this.props;

    // 매출 없으면
    if (!sale && id) {
      // SMS 발송 안했거나 취소거나 시간 만료 여부.
      let isInValid = !send_sms || cancel || moment().isAfter(end_date);

      console.log("- componentWillUnmount QR Code isInValid ", isInValid);

      if (isInValid) this.props.handleClearRoomDoorLock(id);
    }

    this._isMounted = false;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 객실 정보 초기화.
    if (nextProps.room && nextProps.room !== this.props.room) {
      //console.log("--- componentWillReceiveProps RoomTab01 room ");

      let { data, org } = this.state;

      this._isMounted &&
        this.setState({
          data: {
            ...data,
            room: _.merge({}, data.room, nextProps.room),
          },
          org: JSON.parse(
            JSON.stringify({
              ...org,
              room: _.merge({}, data.room, nextProps.room),
            })
          ),
        });
    }

    // 객실 상테.
    if (nextProps.roomState && nextProps.roomState !== this.props.roomState && nextProps.roomState.item !== this.props.roomState.item) {
      // console.log("--- componentWillReceiveProps RoomTab01 roomState ", nextProps.roomState);

      const { item } = nextProps.roomState;

      const roomStateItemNext = item.room_id === this.props.room.id ? item : null;

      console.log("--- roomStateItemNext ", roomStateItemNext);

      if (roomStateItemNext) {
        if (roomStateItemNext.air_set_temp === 0) {
          // 환경설정 온도 초기화.
          roomStateItemNext.air_set_temp = this.props.preferences.air_set_temp;
          roomStateItemNext.temp_key_1 = this.props.preferences.temp_key_1;
          roomStateItemNext.temp_key_2 = this.props.preferences.temp_key_2;
          roomStateItemNext.temp_key_3 = this.props.preferences.temp_key_3;
          roomStateItemNext.temp_key_4 = this.props.preferences.temp_key_4;
          roomStateItemNext.temp_key_5 = this.props.preferences.temp_key_5;
        }

        let { roomStateItem } = this.state;
        let { sale, outing, clean, key } = roomStateItem;

        let { data, save, org } = this.state;

        delete data.state.prev_sale;
        delete save.state.prev_sale;

        // 조작중 표시문구 유지.
        if (data.state) roomStateItemNext.notice = data.state.notice;

        // 소켓으로 객실 이동 시 판매 상태 초기화.
        if (roomStateItemNext.sale > 0 && roomStateItemNext.room_sale_id !== roomStateItem.room_sale_id && roomStateItem.room_sale_id > 0) {
          this.props.handleDisplayRoomLayer(false); // 객실 레이어 닫는다.

          // 체크인 리프레쉬.
          setTimeout(() => {
            this.props.handleDisplayRoomLayer(true); // 객실 레이어 오픈.
          }, 500);
        }

        org.state = _.merge({}, org.state, roomStateItemNext);
        data.state = _.merge({}, data.state, roomStateItemNext);

        this._isMounted &&
          this.setState({
            roomStateItem: roomStateItemNext,
            data,
            save,
            org,
          });
      }
    }

    // 판매 정보.
    if (nextProps.roomSale && nextProps.roomSale !== this.props.roomSale && nextProps.roomSale.item !== this.props.roomSale.item) {
      // console.log("--- componentWillReceiveProps RoomTab01 roomSale ", nextProps.roomSale);

      let { roomStateItem } = this.state;
      const { item } = nextProps.roomSale;
      const { roomState, room } = this.props;

      roomStateItem = roomStateItem || _.find(roomState.list, { room_id: room.id }); // 현재 목록에서 조회.

      const roomSaleItemNext = item.id === roomStateItem.room_sale_id ? item : null;

      console.log("--- roomSaleItemNext ", roomSaleItemNext);

      if (roomSaleItemNext) {
        let { data, org } = this.state;

        if (data.sale) {
          roomSaleItemNext.memo = data.sale.memo; // 조작중 메모 정보 유지.
          roomSaleItemNext.car_no = data.sale.car_no; // 차량 번호 유지.
        }

        org.sale = _.merge({}, org.sale, roomSaleItemNext);
        data.sale = _.merge({}, data.sale, roomSaleItemNext);

        this._isMounted &&
          this.setState({
            roomSaleItem: roomSaleItemNext,
            data,
            org,
          });
      }
    }

    // 객실 도어락 정보.
    if (nextProps.roomDoorLock && nextProps.roomDoorLock !== this.props.roomDoorLock) {
      const {
        item: { id, qr_key_phone },
      } = nextProps.roomDoorLock;

      this.setState({
        phoneQRNumber: qr_key_phone,
      });
    }

    // 객실 유효 예약 목록.
    if (nextProps.roomReserv && nextProps.roomReserv !== this.props.roomReserv && nextProps.roomReserv.list !== this.props.roomReserv.list) {
      let { roomSaleItem } = this.state;

      const {
        room,
        preferences: { pre_check_in_time_stay, pre_check_in_time_rent, pre_check_in_time_clean },
      } = this.props;

      let { list } = nextProps.roomReserv;

      console.log("- reservs list", list.length);

      let reservs = _.filter(list, (item) => {
        let { id, room_type_id, room_type_name, room_id, room_name, name, state, stay_type, check_in_exp, check_out_exp } = item;

        // console.log("- reservs item", room_type_name, room_name, room_id);

        let isEnable = false;

        // 배정 객실은 동일 객실 또는 미배정 객실
        if (state !== "B" && room_type_id === room.room_type_id && (room_id === room.id || !room_id)) {
          // let ie = Number(moment(check_in_exp).format("YYYYMMDD"));
          // let oe = Number(moment(check_out_exp).format("YYYYMMDDHHmm"));

          // 예약 입실 가능 시간.
          let pre_check_in_time = stay_type !== 2 ? pre_check_in_time_stay.split(":") : pre_check_in_time_rent.split(":");

          // 예약 입실 가능 시간 (예약 입실 시간 - 미리 입실 가능 시간 + 청소 시간)
          let reserv_check_in_exp = moment(check_in_exp).add(-Number(pre_check_in_time[0]), "hour").add(-Number(pre_check_in_time[1]), "minute").add(pre_check_in_time_clean, "minute");

          // 예약 입실 시간이 지났고 퇴실 예정 시간 전이라면 예약 입실 가능.
          let isInTime = moment().isAfter(reserv_check_in_exp) && moment().isSameOrBefore(check_out_exp);

          // console.log("- reservs isInTime", isInTime, {
          //   name,
          //   room_name,
          //   stay_type,
          //   check_in_exp: moment(check_in_exp).format("YYYY-MM-DD HH:mm:ss"),
          //   check_out_exp: moment(check_out_exp).format("YYYY-MM-DD HH:mm:ss"),
          //   pre_check_in_time,
          //   reserv_check_in_exp: moment(reserv_check_in_exp).format("YYYY-MM-DD HH:mm:ss"),
          //   isInTime,
          // });

          // 정상 예약 여부.
          isEnable = isInTime && state === "A";

          // 현제 체크인이 예약 매출 이면 추가.
          if (roomSaleItem && roomSaleItem.room_reserv_id === id && roomSaleItem.state === "A" && state === "C") {
            isEnable = true;
          }

          // console.log("- reservs isEnable", isEnable);
        }

        if (isEnable) console.log("- reservs isEnable", room_type_name, room_name, room_id);

        return isEnable;
      });

      reservs = _.uniqBy(reservs, "id");
      reservs = _.orderBy(reservs, ["room_id", "check_in_exp"], ["asc", "asc"]);

      console.log("- reservs", reservs.length);

      this._isMounted &&
        this.setState({
          reservs,
        });
    }

    // 입실 가능 옵션 목록
    if (nextProps.timeOption && nextProps.timeOption !== this.props.timeOption && nextProps.timeOption.list !== this.props.timeOption.list) {
      let { list } = nextProps.timeOption;

      let { data, org, roomTypeItem } = this.state;

      let timeOptions = [];

      const hour = moment().hour();

      // 객실 타입 설정.
      _.map(list, (item) => {
        let types = item.room_types.split(",");
        let isSelected = _.includes(types, String(roomTypeItem.id));
        if (isSelected) {
          let isAbleTime = false;
          if (item.begin < item.end) isAbleTime = item.begin <= hour && item.end >= hour;
          else isAbleTime = item.begin <= hour || item.end >= hour; // 오늘 ~ 다음날

          if (data.sale && item.id === data.sale.time_option_id) isAbleTime = true; // 현재 설정된 옵션
          // console.log("- isAbleTime  ", isAbleTime, item.begin, item.end, hour);

          if (isAbleTime) timeOptions.push(item);
        }
      });

      // console.log("- timeOptions  ", timeOptions, data.sale.time_option_id);

      // 입실 시간 옵션 초기 설정.
      if (data.sale.time_option_id) {
        const timeOption = _.find(timeOptions, {
          id: data.sale.time_option_id,
        });

        let { id, add_fee } = timeOption;

        // console.log("- timeOption  ", { id, add_fee });

        data.sale.time_option_id = id;
        data.sale.time_option_fee = add_fee;

        org.sale.time_option_id = id;
        org.sale.time_option_fee = add_fee;

        this._isMounted &&
          this.setState({
            data,
            org,
          });
      }

      this._isMounted &&
        this.setState({
          timeOptions,
        });
    }

    // 고객 포인트
    if (nextProps.mileage && nextProps.mileage !== this.props.mileage) {
      let { item } = nextProps.mileage;
      const { data, isPointSearch, phoneNumber } = this.state;
      const { user } = this.props;

      console.log("- mileage  ", item);

      let mileage = item || { id: 0, point: 0, phone: phoneNumber };

      if (isPointSearch) {
        // 등록된 전화번호가 없다면 등록.
        if (!mileage.id) {
          this.props.handleNewMileage({
            mileage: {
              place_id: user.place_id,
              phone: format.toPhone(mileage.phone),
              user_id: user.id,
              type: 1, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
            },
          });
        }

        if (!mileage.point) {
          Swal.fire({
            title: "포인트 조회",
            html: phoneNumber + " 고객님은 적립된 포인트가 없습니다.",
            icon: "warning",
            showCancelButton: false,
            showCloseButton: true,
            confirmButtonText: "확인",
          });
        }

        // 조회된 전화번호 추가.
        data.sale.phone = phoneNumber;

        this.setState({
          data,
        });

        // 적립 포인트 재 계산.
        this.calcPoint(this.state.data.sale);
      }

      this._isMounted &&
        this.setState({
          mileage,
          remainPoint: mileage.point,
          isPointSearch: false,
        });
    }

    if (nextProps.placeSubscribe && nextProps.placeSubscribe !== this.props.placeSubscribe) {
      // 마일리지 포인트 서비스 구독여부.
      let isSubscribePoint = CheckSubscribe(this.props, "02", false);

      this._isMounted &&
        this.setState({
          isSubscribePoint,
        });
    }
  }

  render() {
    return this._isMounted && this.state.data.sale.check_out_exp ? (
      <RoomTab01
        {...this.props}
        {...this.state}
        isUpdate={this.isUpdate}
        onClose={this.onClose}
        onSetInit={this.onSetInit}
        onSetReserv={this.onSetReserv}
        onSetTimeOption={this.onSetTimeOption}
        onChangeTerm={this.onChangeTerm}
        onExtendStay={this.onExtendStay}
        onSave={this.onSave}
        onDateChange={this.onDateChange}
        onFocusChange={this.onFocusChange}
        onChangeState={this.onChangeState}
        onChangeStay={this.onChangeStay}
        onChangeSale={this.onChangeSale}
        handleInputChange={this.handleInputChange}
        handlePayCheck={this.handlePayCheck}
        handlePreferencesSave={this.handlePreferencesSave}
        noticeDel={this.noticeDel}
        noticeAddCarNo={this.noticeAddCarNo}
        openRoomMove={this.openRoomMove}
        onChangePoint={this.onChangePoint}
        closeRoomMove={this.closeRoomMove}
        resetTemp={this.resetTemp}
        inputQRPhone={this.inputQRPhone}
        sendQrCodeLink={this.sendQrCodeLink}
        cancelQrCode={this.cancelQrCode}
        inputPhone={this.inputPhone}
        pointSearch={this.pointSearch}
      />
    ) : null;
  }
}

export default withSnackbar(Container);
