// imports.
import React from "react";

import moment from "moment";
import cx from "classnames";

import ReactTooltip from "react-tooltip";

import { keyToValue } from "constants/key-map";
import format from "utils/format-util";

// Components.
import RoomMove from "./RoomMove";

import { ControlLabel, FormControl, Checkbox, Table, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

const RoomTab01 = (props) => {
  const isCheckIn = props.org.sale && props.org.sale.state !== "B" && props.org.sale.check_in ? true : false;
  const isCheckOut = props.org.state.resale === 1 || (props.org.sale && props.org.sale.state === "C" && props.org.sale.check_out) ? true : false;

  let inspectState = props.data.state.inspect; // 점검 상태.

  // console.log("- RoomTab01 props", { isCheckIn, isCheckOut }, props.org.state.resale, props.org.sale, props.data.state.key);

  // const reservDis = props.data.sale.default_fee - props.data.sale.reserv_fee;
  let totalPay = props.data.sale.default_fee + props.data.sale.add_fee; // 총 결제 금액.
  if (props.data.sale.reserv_fee) totalPay = props.data.sale.reserv_fee + props.data.sale.add_fee + props.data.sale.default_fee - props.data.sale.reserv_fee; // 예약 결제 금액.

  const totalPrePay = props.data.sale.prepay_ota_amt + props.data.sale.prepay_cash_amt + props.data.sale.prepay_card_amt + props.data.sale.prepay_point_amt; // 선불 금액.

  // 미수금.
  let totalReceptPay = totalPay - (totalPrePay + props.data.sale.pay_cash_amt + props.data.sale.pay_card_amt + props.data.sale.pay_point_amt); // 미수금.

  // console.log("- RoomTab01 props", props.data.sale, {
  //   totalPay,
  //   totalPrePay,
  //   totalReceptPay
  // });

  let hour = Number(moment().format("HH"));

  return (
    <div>
      <ReactTooltip
        id={"roomCheckIn"}
        place="bottom"
        type="info"
        effect="solid"
        delayShow={1000}
        delayHide={0}
        html={true}
        getContent={(dataTip) => `${dataTip}`}
        className={cx("tooltop")}
        disable={props.preferences.tooltip_show !== 1 || props.edit} // 기타 정보
      />

      {/* 객실이동 팝업 */}
      {props.displayRoomMove ? <RoomMove {...props} /> : null}

      <ul className="list-bul noselect">
        <li>
          <h2>
            입실정보
            <b className="state">
              {isCheckOut
                ? inspectState === 1
                  ? "점검대기"
                  : inspectState === 2
                  ? "점검 중.."
                  : inspectState === 3
                  ? "점검 완료"
                  : "퇴실"
                : isCheckIn
                ? keyToValue("room_state", "sale", props.data.state.sale) + " 상태"
                : "공실 상태"}
            </b>
            {/*
            preferences.isc_sale : 자판기 판매 여부 (0:판매, 1:중지)
            preferences.isc_sale_1 ~ 3 : 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
            state.isc_sale_1 ~ 3 : 객실별 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
            */}
            {props.isc.length ? (
              props.preferences.isc_sale === 1 ? (
                <span className={cx("isc-sale", "disabled")}>무인판매 사용 안함</span>
              ) : (
                <span className={cx("isc-sale")}>
                  <Checkbox
                    inline
                    className={cx(props.data.state.isc_sale_1 === 0 && props.preferences.isc_sale_1 === 0 ? "on" : "off")}
                    checked={props.data.state.isc_sale_1 === 0 && props.preferences.isc_sale_1 === 0}
                    onChange={(evt) => props.handleInputChange("state", "isc_sale_1", evt.target.checked ? 0 : 1)}
                    disabled={props.preferences.isc_sale_1 === 1}
                  >
                    <span />
                    무인 숙박판매 {props.data.state.isc_sale_1 === 1 || props.preferences.isc_sale_1 === 1 ? "중지" : ""}
                  </Checkbox>
                  <Checkbox
                    inline
                    className={cx(props.data.state.isc_sale_2 === 0 && props.preferences.isc_sale_2 === 0 ? "on" : "off")}
                    checked={props.data.state.isc_sale_2 === 0 && props.preferences.isc_sale_2 === 0}
                    onChange={(evt) => props.handleInputChange("state", "isc_sale_2", evt.target.checked ? 0 : 1)}
                    disabled={props.preferences.isc_sale_2 === 1}
                  >
                    <span />
                    무인 대실판매 {props.data.state.isc_sale_2 === 1 || props.preferences.isc_sale_2 === 1 ? "중지" : ""}
                  </Checkbox>
                  <Checkbox
                    inline
                    className={cx(props.data.state.isc_sale_3 === 0 && props.preferences.isc_sale_3 === 0 ? "on" : "off")}
                    checked={props.data.state.isc_sale_3 === 0 && props.preferences.isc_sale_3 === 0}
                    onChange={(evt) => props.handleInputChange("state", "isc_sale_3", evt.target.checked ? 0 : 1)}
                    disabled={props.preferences.isc_sale_3 === 1}
                  >
                    <span />
                    무인 예약판매 {props.data.state.isc_sale_3 === 1 || props.preferences.isc_sale_3 === 1 ? "중지" : ""}
                  </Checkbox>
                </span>
              )
            ) : null}
          </h2>
          <div className="form-content">
            <Table>
              <tbody>
                <tr>
                  <th>
                    <ControlLabel>객실명</ControlLabel>
                  </th>
                  <td>
                    <b>
                      <span>{props.data.room.name}</span> <span>[{props.data.room.type_name}]</span>
                    </b>
                  </td>
                  <th>
                    <ControlLabel>예약</ControlLabel>
                  </th>
                  <td colSpan={4}>
                    <FormControl
                      componentClass="select"
                      placeholder="예약 목록"
                      value={props.data.sale.room_reserv_id || 0}
                      onChange={(evt) => props.onSetReserv(evt.target.value)}
                      disabled={!props.reservs.length || isCheckIn}
                      className={cx(props.data.sale.room_reserv_id ? "assign" : props.reservs.length ? "on" : "off")}
                    >
                      <option value="0">{!props.reservs.length > 0 ? "현재 입실가능 예약 없음" : "현재 입실가능 예약 " + props.reservs.length + " 건"}</option>
                      {props.reservs.length > 0 &&
                        props.reservs.map((v, k) => (
                          <option key={k} value={v.id}>
                            [{keyToValue("room_reserv", "ota_code", v.ota_code)}] {moment(v.check_in_exp).format("MM/DD HH:mm") + " " + keyToValue("room_sale", "stay_type", v.stay_type)}
                            {v.room_name ? " 【" + v.room_name + "】 " : " (미배정) "} {v.name || "미등록고객"} ({String(v.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")})
                          </option>
                        ))}
                    </FormControl>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel>입실시간</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      value={moment(props.data.sale.check_in).format("YYYY-MM-DD HH:mm")}
                      className="check_in_time"
                      onChange={(evt) => props.handleInputChange("sale", props.data.sale.check_in ? "check_in" : "check_in_exp", evt.target.value)}
                      disabled={props.data.sale.check_in}
                    />
                  </td>
                  <th>
                    <ControlLabel>퇴실예정</ControlLabel>
                  </th>
                  <td className="check-out-cal">
                    <div>
                      <SingleDatePicker
                        id="startsOnDateInput-2"
                        placeholder="퇴실예정일 선택"
                        displayFormat="YYYY-MM-DD"
                        date={moment(props.data.sale.check_out_exp)}
                        onDateChange={(date) => {
                          let value = moment(moment(date).format("YYYYMMDD")).diff(moment(moment(props.data.sale.check_in).format("YYYYMMDD")), "day");

                          props.handleInputChange("sale", "check_out_exp", moment(props.org.sale.check_out_exp).add(value, "day").format("YYYY-MM-DD HH:mm"));
                        }}
                        focused={props.focused2}
                        onFocusChange={({ focused: focused2 }) => {
                          props.onFocusChange({ focused2 });
                        }}
                        isOutsideRange={(day) => day < moment()}
                        disabled={props.data.state.sale === 0 || props.data.state.sale === 2}
                      />
                      <div className="reserve-time-form">
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control sm75"
                            value={Number(moment(props.data.sale.check_out_exp).format("HH"))}
                            step={1}
                            min={0}
                            max={23}
                            formatter={(value) => `${value}시`}
                            onChange={(value) => {
                              props.handleInputChange("sale", "check_out_exp", moment(props.data.sale.check_out_exp).hour(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={props.data.state.sale === 0 || props.data.state.sale === 2}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control sm75"
                            value={Number(moment(props.data.sale.check_out_exp).format("mm"))}
                            step={10}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(value) => {
                              props.handleInputChange("sale", "check_out_exp", moment(props.data.sale.check_out_exp).minute(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={props.data.state.sale === 0 || props.data.state.sale === 2}
                          />
                        </span>
                      </div>
                    </div>
                  </td>
                  <th>
                    <ControlLabel>인원</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        value={props.data.sale.person || props.roomTypeItem.person}
                        step={1}
                        min={1}
                        max={100}
                        formatter={(value) => format.toMoney(value) + "명"}
                        onChange={(value) => props.handleInputChange("sale", "person", value)}
                      />
                    </span>
                  </td>
                </tr>
                <tr>
                  <th>투숙형태</th>
                  <td colSpan="3">
                    <ButtonToolbar className="btn-room">
                      <Button
                        className={cx(
                          props.org.state.sale && !props.org.state.resale
                            ? !props.changeStay
                              ? props.data.state.sale === 2
                                ? "sel"
                                : "room-bgcolor-dim"
                              : props.data.state.sale !== 2
                              ? ""
                              : "disabled sel"
                            : props.data.state.sale !== 2
                            ? "room-bgcolor-dim"
                            : "sel"
                        )}
                        style={{ background: props.stayTypeColor[2] }}
                        onClick={(evt) => props.handleInputChange("state", "sale", 2)}
                        disabled={isCheckIn && !isCheckOut}
                      >
                        대실
                      </Button>
                      <Button
                        className={cx(
                          props.org.state.sale && !props.org.state.resale
                            ? !props.changeStay
                              ? props.data.state.sale === 1
                                ? "sel"
                                : "room-bgcolor-dim"
                              : props.data.state.sale !== 1
                              ? ""
                              : "disabled sel"
                            : props.data.state.sale !== 1
                            ? "room-bgcolor-dim"
                            : "sel"
                        )}
                        style={{ background: props.stayTypeColor[1] }}
                        onClick={(evt) => props.handleInputChange("state", "sale", 1)}
                        disabled={isCheckIn && !isCheckOut}
                      >
                        숙박
                      </Button>
                      <Button
                        className={cx(
                          props.org.state.sale && !props.org.state.resale
                            ? !props.changeStay
                              ? props.data.state.sale === 3
                                ? "sel"
                                : "room-bgcolor-dim"
                              : props.data.state.sale !== 3
                              ? ""
                              : "disabled sel"
                            : props.data.state.sale !== 3
                            ? "room-bgcolor-dim"
                            : "sel"
                        )}
                        style={{ background: props.stayTypeColor[3] }}
                        onClick={(evt) => props.handleInputChange("state", "sale", 3)}
                        disabled={isCheckIn && !isCheckOut}
                      >
                        장기
                      </Button>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid"
                          value={
                            props.data.state.sale === 2
                              ? moment(props.data.sale.check_out_exp).diff(props.data.sale.check_in, "hour")
                              : moment(moment(props.data.sale.check_out_exp).format("YYYYMMDD")).diff(moment(moment(props.data.sale.check_in).format("YYYYMMDD")), "day")
                          }
                          step={1}
                          min={props.data.state.sale === 2 ? 1 : moment(props.data.sale.check_out_exp).hour() > hour ? 0 : 1}
                          max={props.data.state.sale === 2 ? 24 : 365}
                          formatter={(value) => value + (props.data.state.sale === 2 ? "시간" : "박" + (Number(value) + 1) + "일")}
                          onChange={(value) => {
                            props.onChangeTerm(value);
                          }}
                          disabled={!props.data.state.sale}
                        />
                      </span>
                    </ButtonToolbar>
                  </td>
                  <th>
                    <ControlLabel>차량번호</ControlLabel>
                  </th>
                  <td colSpan="2">
                    <FormControl
                      type="text"
                      placeholder="차량번호"
                      style={{ width: "100px" }}
                      defaultValue={props.data.sale.car_no || ""}
                      onChange={(evt) => props.handleInputChange("sale", "car_no", evt.target.value)}
                    />
                  </td>
                  <td>
                    <Checkbox inline checked={props.preferences.car_call_display === 1} onChange={(evt) => props.handlePreferencesSave("car_call_display", evt.target.checked ? 1 : 0)}>
                      <span />
                      <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="차량번호를 객실에 표시 합니다.">
                        객실표시
                      </span>
                    </Checkbox>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info01-0106">입실옵션</ControlLabel>
                  </th>
                  <td colSpan={3}>
                    <FormControl
                      componentClass="select"
                      placeholder="입실 옵션 목록"
                      value={props.data.sale.time_option_id}
                      onChange={(evt) => props.onSetTimeOption(evt.target.value)}
                      className={cx(props.timeOptions.length > 0 ? "assign" : "")}
                      disabled={props.data.sale.room_reserv_id ? true : false}
                    >
                      <option value={0}>{!props.timeOptions.length > 0 ? "없음" : "선택 가능 옵션 " + props.timeOptions.length + " 개"}</option>
                      {props.timeOptions.length > 0 &&
                        props.timeOptions.map((v, k) => (
                          <option key={k} value={v.id}>
                            {`${v.name} [${keyToValue("room_sale", "stay_type", v.stay_type)}] ${v.use_time} ${v.use_time_type === 1 ? "시간" : "시"} /
                           ${v.add_fee > 0 ? "추가요금" : "할인요금"}: ${format.toMoney(v.add_fee)}원
                            ${v.sale_isc === 0 ? " / 무인판매" : ""} / 적용시간: ${v.begin} ~ ${v.end} 시`}
                          </option>
                        ))}
                    </FormControl>
                  </td>
                  <td>
                    <ButtonToolbar>
                      <Button onClick={(evt) => props.onSetTimeOption(0)} disabled={!props.data.sale.time_option_id || isCheckIn}>
                        취소
                      </Button>
                    </ButtonToolbar>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info01-0106">고객메모</ControlLabel>
                  </th>
                  <td colSpan="4">
                    <FormControl placeholder="메모는 매출별 적용 됩니다." value={props.data.sale.memo || ""} onChange={(evt) => props.handleInputChange("sale", "memo", evt.target.value)} />
                  </td>
                  <td>
                    <ButtonToolbar>
                      <Button onClick={(evt) => props.handleInputChange("sale", "memo", "")} disabled={!props.data.sale.memo}>
                        지우기
                      </Button>
                    </ButtonToolbar>
                  </td>
                </tr>
                <tr>
                  <th>
                    <Checkbox
                      inline
                      checked={props.data.sale.alarm === 1}
                      onChange={(evt) => {
                        props.handleInputChange("sale", "alarm", evt.target.checked ? 1 : 0);
                        if (evt.target.checked) {
                          if (!props.data.sale.memo) {
                            props.handleInputChange("sale", "memo", props.data.room.name + " 알람 시간 입니다.");
                          }
                        } else {
                          if (props.data.sale.memo === props.data.room.name + " 알람 시간 입니다.") {
                            props.handleInputChange("sale", "memo", "");
                          }
                        }
                      }}
                    >
                      <span />
                      <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="객실 알람 설정 입니다.">
                        알람
                      </span>
                    </Checkbox>
                  </th>
                  <td colSpan="5">
                    {props.data.sale.alarm === 1 && (
                      <div className="alarm">
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.data.sale.alarm_hour !== null ? props.data.sale.alarm_hour : Number(moment(props.data.sale.check_out_exp).format("HH"))}
                            step={1}
                            min={0}
                            max={23}
                            onChange={(value) => props.handleInputChange("sale", "alarm_hour", value)}
                            formatter={(value) => value + " 시"}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.data.sale.alarm_min !== null ? props.data.sale.alarm_min : Number(moment(props.data.sale.check_out_exp).format("mm"))}
                            step={5}
                            min={0}
                            max={59}
                            onChange={(value) => props.handleInputChange("sale", "alarm_min", value)}
                            formatter={(value) => value + " 분"}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.data.sale.alarm_term || 1}
                            step={1}
                            min={1}
                            max={60}
                            onChange={(value) => props.handleInputChange("sale", "alarm_term", value)}
                            formatter={(value) => value + " 분간격"}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.data.sale.alarm_repeat || 1}
                            step={1}
                            min={1}
                            max={60}
                            onChange={(value) => props.handleInputChange("sale", "alarm_repeat", value)}
                            formatter={(value) => value + " 회반복"}
                          />
                        </span>
                        <span className="alarm_memo_play">
                          <Checkbox inline checked={props.data.sale.alarm_memo_play === 1} onChange={(evt) => props.handleInputChange("sale", "alarm_memo_play", evt.target.checked ? 1 : 0)}>
                            <span />
                            <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="객실 알람 시 메모를 음성 으로 재생 합니다.">
                              알람 시 메모 재생
                            </span>
                          </Checkbox>
                        </span>
                      </div>
                    )}
                  </td>
                </tr>
                {props.preferences.doorlock_send_qr_code === 1 ? (
                  <tr>
                    <th>
                      <ControlLabel htmlFor="info01-0106">도어락</ControlLabel>
                    </th>
                    <td>
                      {props.room.doorlock_id ? (
                        !props.roomDoorLock.item.qr_key_data ? (
                          <span className="ready">QR 코드 키 미발급</span>
                        ) : props.roomDoorLock.item.cancel ? (
                          <span className="cancel">QR 코드 키 발급 취소</span>
                        ) : moment().isAfter(props.roomDoorLock.item.end_date) ? (
                          <span className="expired">QR 코드 키 만료</span>
                        ) : (
                          <span className="success">QR 코드 키 발급 완료</span>
                        )
                      ) : (
                        <span className="cancel">객실 도어락 ID 없음</span>
                      )}
                    </td>
                    <td>
                      {props.room.doorlock_id && props.roomDoorLock.item.qr_key_data && !props.roomDoorLock.item.cancel ? (
                        <span className="validtime">
                          사용기간 {moment(props.roomDoorLock.item.start_date).format("MM/DD HH:mm")} ~ {moment(props.roomDoorLock.item.end_date).format("MM/DD HH:mm")}
                        </span>
                      ) : null}
                    </td>
                    <td colSpan="3">
                      {props.room.doorlock_id ? (
                        <span className="phone">
                          <FormControl
                            type="text"
                            placeholder="휴대폰 번호 입력"
                            value={props.phoneQRNumber || "010"}
                            onChange={(evt) => {
                              let toPhone = format.toPhone(evt.target.value);
                              props.inputQRPhone(toPhone);
                            }}
                            maxLength={13}
                          />
                          <ButtonToolbar>
                            {!props.roomDoorLock.item.send_sms || props.roomDoorLock.item.cancel ? (
                              <Button onClick={(evt) => props.sendQrCodeLink()} disabled={!format.isPhone(props.phoneQRNumber)}>
                                QR 코드 SMS 전송
                              </Button>
                            ) : (
                              <Button onClick={(evt) => props.sendQrCodeLink()} disabled={!format.isPhone(props.phoneQRNumber)} className="send">
                                QR 코드 SMS 재전송
                              </Button>
                            )}
                            <Button onClick={(evt) => props.cancelQrCode()} disabled={!props.roomDoorLock.item.qr_key_data || !!props.roomDoorLock.item.cancel}>
                              QR 코드 발급 취소
                            </Button>
                          </ButtonToolbar>
                        </span>
                      ) : (
                        <span className="nodoorid">도어락 ID 를 등록해 주세요</span>
                      )}
                    </td>
                  </tr>
                ) : null}
              </tbody>
            </Table>
          </div>
        </li>
        <li>
          <h2>
            객실표시
            <span className="car-no-add" data-for="roomCheckIn" data-type="info" data-place="top" data-effect="solid" data-tip="차량 번호를 객실 표시에 추가 합니다.">
              <Checkbox
                inline
                checked={(props.data.state.notice && props.data.state.notice.indexOf(props.data.sale.car_no) > -1) || false}
                onChange={(evt) => props.noticeAddCarNo(evt.target.checked)}
                disabled={!props.data.sale.car_no}
              >
                <span />
                차량번호 표시
              </Checkbox>
            </span>
          </h2>
          <div className="form-content">
            <Table>
              <tbody>
                <tr>
                  <td>
                    <FormControl
                      type="text"
                      placeholder="객실화면에 표시 할 메모 입니다."
                      value={props.data.state.notice || ""}
                      onChange={(evt) => props.handleInputChange("state", "notice", evt.target.value.replace(/\\n/gi, "<br/>"))}
                    />
                  </td>
                  <td>
                    <ButtonToolbar>
                      <Button value={props.data.state.notice_display} onClick={(evt) => props.noticeDel(true)}>
                        지우기
                      </Button>
                    </ButtonToolbar>
                  </td>
                  <th>표시 투명도</th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        value={props.data.state.notice_opacity}
                        step={1}
                        min={0}
                        max={10}
                        onChange={(value) => props.handleInputChange("state", "notice_opacity", value)}
                      />
                    </span>
                    <span> 0: 투명 ~ 10: 불투명</span>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </li>
        <li>
          <h2>
            요금
            <span className="add-fee">
              {props.data.sale.option_fee > 0 ? <ControlLabel>[추가 요금 합계] {String(props.data.sale.option_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} </ControlLabel> : null}
              {props.data.sale.option_fee > 0 && props.data.sale.person > props.roomTypeItem.person ? (
                <span className={cx("person")}>
                  (인원 추가:
                  <span className={cx("plus")}>{String(props.roomTypeItem.person_add_fee * (props.data.sale.person - props.roomTypeItem.person)).replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</span>{" "}
                </span>
              ) : null}

              {props.data.sale.option_fee > 0 && props.fee && props.fee.add_fee ? (
                <span className={cx("time")}>
                  ({props.fee.add_fee > 0 ? "시간 추가: " : "시간 할인: "}
                  <span className={cx(props.fee && props.fee.add_fee > 0 ? "plus" : "minus")}>{String(props.fee.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</span>
                </span>
              ) : null}

              {props.data.sale.option_fee > 0 && props.data.sale.time_option_fee ? (
                <span className={cx("option")}>
                  ({props.data.sale.time_option_fee > 0 ? "입실 옵션 추가: " : "입실 옵션 할인: "}
                  <span className={cx(props.data.sale.time_option_fee > 0 ? "plus" : "minus")}>{String(props.data.sale.time_option_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</span>
                </span>
              ) : null}

              {props.data.sale.option_fee > 0 && props.season && props.season.premium ? (
                <span className={cx("season")}>
                  ({props.season.premium > 0 ? "시즌 추가: " : "시즌 할인: "}
                  <span className={cx(props.season && props.season.premium > 0 ? "plus" : "minus")}>{String(props.season.premium).replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</span>
                </span>
              ) : null}

              {props.data.sale.option_fee > 0 && props.data.sale && props.roomTypeItem.card_add_fee && (props.data.sale.pay_card_amt > 0 || props.data.sale.prepay_card_amt > 0) ? (
                <span className={cx("card")}>
                  (카드 추가:
                  <span className={cx("plus")}>{String(props.roomTypeItem.card_add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</span>
                </span>
              ) : null}
            </span>
            {props.isSubscribePoint && (props.preferences.point_use_type > 0 || props.data.sale.pay_point_amt) && (
              <span className="point">
                {format.isPhone(props.phoneQRNumber) ? (
                  <ControlLabel>
                    <Checkbox
                      inline
                      checked={format.toPhone(props.phoneQRNumber) === format.toPhone(props.phoneNumber || props.data.sale.phone)}
                      onChange={(evt) => {
                        props.inputPhone(props.phoneQRNumber);

                        setTimeout(() => {
                          props.pointSearch(true);
                        }, 100);
                      }}
                    >
                      <span />
                      <span>QR 전송 번호</span>
                    </Checkbox>
                  </ControlLabel>
                ) : null}
                <FormControl
                  type="text"
                  placeholder="휴대폰 번호 입력"
                  value={format.toPhone(props.phoneNumber || props.data.sale.phone || "010")}
                  onChange={(evt) => {
                    let toPhone = format.toPhone(evt.target.value);

                    props.inputPhone(toPhone);

                    if (toPhone.length >= 13) {
                      setTimeout(() => {
                        props.pointSearch(true);
                      }, 100);
                    }
                  }}
                  maxLength={13}
                  disabled={props.org.sale.phone}
                />

                <Button className="btn-search" onClick={(evt) => props.pointSearch(true)} disabled={props.data.sale.phone ? false : true}>
                  포인트 조회
                </Button>
              </span>
            )}
          </h2>
          <div className="form-content">
            <Table>
              <tbody>
                {props.reserv && Number(props.reserv.ota_code) < 9 && (
                  <tr>
                    <th>
                      <ControlLabel>예약가</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" placeholder="예약가" value={String(props.data.sale.reserv_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </td>
                    <th>
                      <ControlLabel>OTA 결제</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" className="mid120" placeholder="OTA 결제" value={String(props.data.sale.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </td>
                    <th>
                      <ControlLabel>OTA 예약</ControlLabel>
                    </th>
                    <td className="ota-code">{keyToValue("room_reserv", "ota_code", props.reserv.ota_code)}</td>
                    {/* <th>
                      <ControlLabel>예약 번호</ControlLabel>
                    </th>
                    <td>
                      <span className="reserv-num">{String(props.reserv.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")}</span> [{props.reserv.name ? props.reserv.name : ""}]
                    </td> */}
                  </tr>
                )}
                {props.reserv && Number(props.reserv.ota_code) === 9 && (
                  <tr>
                    <th>
                      <ControlLabel>예약가</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" placeholder="예약가" value={String(props.data.sale.reserv_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </td>
                    {/* <th>
                      <ControlLabel>예약 번호</ControlLabel>
                    </th>
                    <td>
                      <span className="reserv-num">{String(props.reserv.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")}</span> [{props.reserv.name ? props.reserv.name : ""}]
                    </td> */}
                    <th>
                      <ControlLabel>예약 현금</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" className="mid120" placeholder="예약 현금" value={String(props.data.sale.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </td>
                    <th>
                      <ControlLabel>예약 카드</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" className="mid120" placeholder="예약 카드" value={String(props.data.sale.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </td>
                  </tr>
                )}
                <tr>
                  <th>
                    <ControlLabel>기본 요금</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      className="mid120"
                      value={String(props.data.sale.default_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      onChange={(evt) => props.handleInputChange("sale", "default_fee", Number(String(evt.target.value).replace(/[,]/g, "")))}
                      disabled={true}
                    />
                  </td>
                  <th>
                    <ControlLabel>
                      <Checkbox inline checked={props.data.sale.pay_cash_amt !== 0} onChange={(evt) => props.handlePayCheck("pay_cash_amt", true, evt.target.checked)}>
                        <span />
                        <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="클릭 시 결제 금액이 현금으로 전액 입력 됩니다.">
                          현금
                        </span>
                      </Checkbox>
                    </ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid120"
                        value={props.data.sale.pay_cash_amt}
                        step={props.data.state.sale === 2 ? props.preferences.rent_fee_unit : props.preferences.stay_fee_unit}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("sale", "pay_cash_amt", value)}
                      />
                    </span>
                    <Button
                      onClick={(evt) => props.handlePayCheck("pay_cash_amt", false, null)}
                      data-for="roomCheckIn"
                      data-type="info"
                      data-effect="solid"
                      data-tip="클릭 시 결제 액이 자동 입력 됩니다."
                    >
                      잔액 <span className="mdi mdi-plus-circle-outline" />
                    </Button>
                  </td>
                  <th>
                    <ControlLabel>판매가</ControlLabel>
                  </th>
                  <td>
                    <FormControl type="text" placeholder="판매가" value={String(totalPay).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel>{!props.data.sale.room_reserv_id ? "추가/할인" : "추가/할인"}</ControlLabel>
                  </th>
                  <td className="add_fee_td">
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid120"
                        value={props.data.sale.add_fee}
                        step={props.data.state.sale === 2 ? props.preferences.rent_fee_unit : props.preferences.stay_fee_unit}
                        min={-props.data.sale.default_fee}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("sale", "add_fee", value)}
                      />
                    </span>
                    {/* <div className="option-fee">
                      <ControlLabel>옵션요금</ControlLabel>
                      <FormControl type="text" placeholder="옵션요금" value={String(props.data.sale.option_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                    </div> */}
                  </td>
                  <th>
                    <ControlLabel>
                      <Checkbox inline checked={props.data.sale.pay_card_amt !== 0} onChange={(evt) => props.handlePayCheck("pay_card_amt", true, evt.target.checked)}>
                        <span />
                        <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="클릭 시 결제 금액이 카드로 전액 입력 됩니다.">
                          카드
                        </span>
                      </Checkbox>
                    </ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid120"
                        value={props.data.sale.pay_card_amt}
                        step={props.data.state.sale === 2 ? props.preferences.rent_fee_unit : props.preferences.stay_fee_unit}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("sale", "pay_card_amt", value)}
                      />
                    </span>
                    <Button
                      onClick={(evt) => props.handlePayCheck("pay_card_amt", false, null)}
                      data-for="roomCheckIn"
                      data-type="info"
                      data-effect="solid"
                      data-tip="클릭 시 결제 잔액이 자동 입력 됩니다."
                    >
                      잔액 <span className="mdi mdi-plus-circle-outline" />
                    </Button>
                  </td>
                  <th>
                    <ControlLabel htmlFor="receive_amt">{totalReceptPay >= 0 ? "미수금" : "반환금"}</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      placeholder={totalReceptPay >= 0 ? "미수금" : "반환금"}
                      className={cx("mid120", totalReceptPay === 0 ? "" : totalReceptPay > 0 ? "fontRed" : "fontBlue")}
                      value={String(Math.abs(totalReceptPay)).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      readOnly={true}
                    />
                  </td>
                </tr>
                {props.isSubscribePoint && (props.preferences.point_use_type > 0 || props.data.sale.pay_point_amt) && (
                  <tr>
                    <th>
                      <ControlLabel>보유 포인트</ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" className="mid120" value={String(props.remainPoint).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} disabled={true} />
                    </td>
                    <th>
                      <ControlLabel>
                        <Checkbox
                          inline
                          checked={props.data.sale.pay_point_amt > 0}
                          onChange={(evt) => props.handlePayCheck("pay_point_amt", true, evt.target.checked)}
                          disabled={props.preferences.point_pay_type === 1 && props.remainPoint < totalPay}
                        >
                          <span />
                          <span data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="클릭 시 결제 금액이 포인트로 전액 입력 됩니다.">
                            포인트
                          </span>
                        </Checkbox>
                      </ControlLabel>
                    </th>
                    <td>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid120"
                          value={props.data.sale.pay_point_amt}
                          step={props.data.state.sale === 2 ? props.preferences.rent_fee_unit : props.preferences.stay_fee_unit}
                          min={0}
                          max={props.mileage.point + props.org.sale.pay_point_amt}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("sale", "pay_point_amt", value)}
                          disabled={props.preferences.point_pay_type === 1}
                        />
                      </span>
                      <Button
                        className={cx(props.preferences.point_pay_type === 1 && "all-pay")}
                        onClick={(evt) => props.handlePayCheck("pay_point_amt", props.preferences.point_pay_type === 1, null)}
                        data-for="roomCheckIn"
                        data-type="info"
                        data-effect="solid"
                        data-tip={props.preferences.point_pay_type === 1 ? "클릭 시 결제 금액이 자동 입력 됩니다." : "클릭 시 결제 잔액이 자동 입력 됩니다."}
                        disabled={props.preferences.point_pay_type === 1 && props.remainPoint < totalPay}
                      >
                        {props.preferences.point_pay_type === 1 ? "전액 " : "잔액 "}
                        <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </td>
                    <th>
                      <ControlLabel
                        data-for="roomCheckIn"
                        data-type="info"
                        data-effect="solid"
                        data-tip={`
                      현금 결제: ${props.preferences.cash_point_rate}% <br/>
                      카드 결제: ${props.preferences.card_point_rate}% <br/>
                      예약 결제: ${props.preferences.reserv_point_rate}% (추가) <br/>
                      무인 결제: ${props.preferences.isg_point_rate}% (추가) <br/>
                      추가 포인트: ${props.preferences.pay_add_point}
                      `}
                      >
                        적립 포인트
                        <br />
                        {props.preferences.pay_add_point > 0 ? (
                          <span className="add_point">적립추가 {props.preferences.pay_add_point}</span>
                        ) : props.preferences.pay_add_point < 0 ? (
                          <span className="dis_point">적립차감 {props.preferences.pay_add_point}</span>
                        ) : null}
                      </ControlLabel>
                    </th>
                    <td>
                      <FormControl type="text" placeholder="적립 포인트" value={format.isPhone(props.phoneNumber) ? props.savePoint : 0} readOnly />
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </li>
        <li>
          <h2>난방 설정</h2>
          <div className="form-content">
            <Table>
              <tbody>
                <tr>
                  <th>
                    <ControlLabel>현재온도</ControlLabel>
                  </th>
                  <td colSpan="2" className="celsius">
                    <FormControl type="text" placeholder="현재온도" value={(props.data.state.air_temp === 63 ? 0 : props.data.state.air_temp) + " ℃"} readOnly={true} />
                  </td>
                  <th colSpan="2">
                    <ControlLabel>적용온도</ControlLabel>
                  </th>
                  <td colSpan="2">
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.air_set_temp}
                        onChange={(value) => props.handleInputChange("state", "air_set_temp", value)}
                      />
                    </span>
                  </td>
                  <td colSpan="2">
                    <Button onClick={(evt) => props.resetTemp()} data-for="roomCheckIn" data-type="info" data-effect="solid" data-tip="클릭 시 난방 온도 기본 설정값으로 초기화 됩니다.">
                      기본값
                      <span className="mdi mdi-sync" />
                    </Button>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel>사용중</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.temp_key_1}
                        onChange={(value) => props.handleInputChange("state", "temp_key_1", value)}
                      />
                    </span>
                  </td>
                  <th>
                    <ControlLabel>외출</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.temp_key_2}
                        onChange={(value) => props.handleInputChange("state", "temp_key_2", value)}
                      />
                    </span>
                  </td>
                  <th>
                    <ControlLabel>{props.preferences.inspect_use ? "청소/점검대기" : "청소대기"}</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.temp_key_5}
                        onChange={(value) => props.handleInputChange("state", "temp_key_5", value)}
                      />
                    </span>
                  </td>
                  <th>
                    <ControlLabel>{props.preferences.inspect_use ? "청소/점검 중" : "청소 중"}</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.temp_key_4}
                        onChange={(value) => props.handleInputChange("state", "temp_key_4", value)}
                      />
                    </span>
                  </td>
                  <th>
                    <ControlLabel>공실</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        step={1}
                        min={0}
                        max={63}
                        value={props.data.state.temp_key_3}
                        onChange={(value) => props.handleInputChange("state", "temp_key_3", value)}
                      />
                    </span>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </li>
        <li>
          <h2>예약 냉방</h2>
          <div className="form-content">
            기본 설정 : <span className="default_setting"> [{props.preferences.air_power_type === 0 ? "키에 연동" : props.preferences.air_power_type === 1 ? "전원 ON" : "전원 OFF"}] </span>
            <ControlLabel>
              <Checkbox
                inline
                checked={props.data.state.air_power_type === 1}
                onChange={(evt) => props.handleInputChange("state", "air_power_type", evt.target.checked ? 1 : 0)}
                className={cx(isCheckIn && !isCheckOut && props.changeCnt === 0 ? "active" : "off")}
                disabled={props.data.state.key === 1}
              >
                <span className="check" />
                <span className={cx(props.data.state.air_power_type === 1 ? "btn on" : "btn off")}>예냉 ON</span>
                <span className={cx(props.data.state.air_power_type === 1 ? "notice on" : props.data.state.key === 1 ? "notice off" : "notice")}>
                  (
                  {props.data.state.key === 1
                    ? "고객 입실 상태에서는 사용 할 수 없습니다."
                    : props.data.state.air_power_type === 1
                    ? "손님키 삽입 시 기본 설정으로 자동 변경됩니다."
                    : "손님 입실전 예약 냉방 기능 입니다."}
                  )
                </span>
              </Checkbox>
            </ControlLabel>
          </div>
        </li>
        <li>
          <h2>변경</h2>
          <div className="form-content">
            <ButtonToolbar className="btn-room">
              {/* state	0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료*/}
              <Button
                // 체크인 전 가능.
                className={cx(!isCheckIn || isCheckOut || props.checkInCancel ? "active" : "off", isCheckIn && !isCheckOut ? "cancel" : "in")}
                onClick={(evt) => {
                  props.onChangeSale(evt, !isCheckIn || isCheckOut ? 1 : 7);
                }}
              >
                {!isCheckIn || isCheckOut ? "입실" : "입실취소"}
              </Button>
              <Button className={cx("active")} onClick={(evt) => props.onChangeSale(evt, 0)}>
                {inspectState === 1 ? "점검대기" : inspectState === 2 ? "점검 중" : inspectState === 3 ? "점검 완료" : "공실처리"}
              </Button>
              <Button
                // 입실 후 퇴실 전 가능
                className={cx(isCheckIn && !isCheckOut && props.changeCnt === 0 ? "active" : "off")}
                onClick={(evt) => props.onChangeSale(evt, 2, props.data.state.outing === 0 ? 1 : 0)}
              >
                {props.data.state.outing === 0 ? "외출" : "외출복귀"}
              </Button>
              <Button
                // 입실 후 퇴실 전 가능.
                className={cx(isCheckIn && !isCheckOut && props.changeCnt === 0 ? "active" : "off")}
                onClick={(evt) => props.openRoomMove(evt)}
              >
                객실이동
              </Button>
              <Button
                // 퇴실 전 가능.
                className={cx(isCheckIn && !isCheckOut && props.data.state.sale > 0 ? "active" : "off", props.changeStay ? "on" : null)}
                onClick={(evt) => props.onChangeStay(evt)}
              >
                {props.data.state.sale === 2 ? "숙박변경" : "대실변경"}
              </Button>
            </ButtonToolbar>
            <ButtonToolbar className="btn-room">
              <Button
                className={cx(isCheckIn && (!isCheckOut || (isCheckOut && props.checkOutCancel)) ? "active" : "off", isCheckOut ? "cancel" : "out")}
                onClick={(evt) => props.onChangeSale(evt, !isCheckOut ? 6 : 8)}
              >
                {!isCheckOut ? "퇴실" : "퇴실취소"}
              </Button>
              <Button className={cx(isCheckIn && !isCheckOut ? "active" : "off", "use_out")} onClick={(evt) => props.onExtendStay(evt)}>
                사용연장(재입실)
              </Button>
              <Button
                // 현재 상태가 청소 중이 아니라면 가능.
                className={cx(props.data.state.key !== 3 ? "active" : "off")}
                onClick={(evt) => props.onChangeSale(evt, props.data.state.clean === 1 ? 5 : 3)}
              >
                {props.data.state.clean === 1 ? "청소완료" : "청소지시"}
              </Button>
              <Button className={props.changeState > -1 || props.changeCnt > 0 ? "active on" : "off"} onClick={(evt) => props.onSave(evt)}>
                저장
              </Button>
              <Button className="btn-close" onClick={(evt) => props.onClose(evt)}>
                {props.changeCnt > 0 ? "취소" : "닫기"}
              </Button>
            </ButtonToolbar>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default RoomTab01;
