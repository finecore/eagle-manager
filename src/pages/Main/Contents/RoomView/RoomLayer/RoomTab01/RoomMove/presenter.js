// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";

// style component
import { Table, ButtonToolbar, Button } from "react-bootstrap";

import swal from "sweetalert";
import Swal from "sweetalert2";
// import Alert from "sweetalert-react";

// styles.
import "./styles.scss";

// functional component
const RoomMove = (props) => {
  return (
    <Modal visible={true} effect="fadeInUp">
      <article className="room-move">
        <h1>객실이동</h1>
        <div className="move-form-wrapper">
          <h2>객실선택</h2>
          <div className="table-wrapper">
            <Table>
              <thead>
                <tr>
                  <th onClick={(evt) => props.handleSort("type_name")}>
                    타입 <b className={cx("caret", props.asc["type_name"] === "asc" ? "asc" : "desc")} />
                  </th>
                  <th onClick={(evt) => props.handleSort("name")}>
                    객실명 <b className={cx("caret", props.asc["name"] === "asc" ? "asc" : "desc")} />
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.moveRooms.map(
                  (item, i) =>
                    props.data.sale.room_id !== item.id && (
                      <tr key={i} onClick={(evt) => props.handleRoomClick(item)} className={props.selected && props.selected.id === item.id ? "on" : "off"}>
                        <td>{item.type_name}</td>
                        <td>{item.name}</td>
                      </tr>
                    )
                )}
              </tbody>
            </Table>
          </div>
          <ButtonToolbar className="btn-room">
            {props.selected && <Button onClick={(evt) => props.handelMoveRoom(evt)}>객실이동</Button>}
            <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
              닫기
            </Button>
          </ButtonToolbar>
        </div>
        <a onClick={(evt) => props.closeModal(evt)} className="move-close">
          Close
        </a>
      </article>
    </Modal>
  );
};

export default RoomMove;
