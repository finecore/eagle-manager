// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import RoomMove from "./presenter";

class Container extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
    changeRoomSale: PropTypes.func.isRequired,
    closeRoomMove: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired, // 상위 컴포넌트 선택 객실 정보.
  };

  constructor(props) {
    super(props);
    this.state = {
      asc: { type_name: "asc", name: "asc", floor: "asc" },
      moveRooms: [],
      selected: undefined,
    };
  }

  handleSort = (type) => {
    let { moveRooms, asc } = this.state;

    asc[type] = asc[type] === "asc" ? "desc" : "asc";

    let sort = [];
    let order = {};

    if (type === "type_name") {
      sort = ["type_name", "floor", "name"];
      order = [asc["type_name"], asc["floor"], asc["name"]];
    } else {
      asc["floor"] = asc["floor"] === "asc" ? "desc" : "asc";
      sort = ["floor", "name", "type_name"];
      order = [asc["floor"], asc["name"], asc["type_name"]];
    }

    moveRooms = _.orderBy(moveRooms, sort, order);

    console.log("- handleSort", { sort, order }, moveRooms);

    this.setState({
      moveRooms,
      asc,
    });
  };

  handleRoomClick = (item) => {
    // 이동할 객실이 판매 중인지 검사.
    if (item.room_sale_id) {
      Swal.fire({
        icon: "warning",
        title: "판매 알림",
        text: "이동 하려는 객실은 이미 판매 되었습니다.",
      });
    } else {
      this.setState({
        selected: item,
      });
    }
  };

  handelMoveRoom = (evt) => {
    let {
      data: {
        sale: { id },
        state,
      },
    } = this.props;

    const { selected } = this.state;

    console.log("- handelMoveRoom", this.props.data.state.name, "->", selected.name);

    Swal.fire({
      icon: "warning",
      title: "이동 확인",
      html: "객실 이동 시 매출 정보가 변경 됩니다. <br/>[" + selected.name + "] 객실로 이동 하시겠습나까?",
      showCancelBtn: true,
      confirmButtonText: "이동",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        const room_sale = {
          room_id: selected.id,
          move_from: state.room_id,
        };

        console.log("- changeRoomSale ", id, room_sale);

        // 매출 정보 객실 id 수정.
        this.props.changeRoomSale(id, { room_sale }, (info) => {
          const room_sale_id = state.room_sale_id;

          let prev_state = {};
          prev_state.sale = 0;
          prev_state.room_sale_id = null; // 현재 매출 초기화(null 로 한다)
          prev_state.move_to = selected.id; // 이동할 room_id

          if (!state.clean) prev_state.clean = 1; // 청소 요청.
          if (state.outing) prev_state.outing = 0;

          prev_state.mod_date = new Date();

          // 이전객실 상태.
          this.props.handleSaveRoomState(state.room_id, {
            room_state: prev_state,
          });

          this.props.handleDisplayRoomLayer(false); // 현재 객실 레이어 닫는다.

          let next_state = {};
          next_state.sale = state.sale;
          next_state.room_sale_id = room_sale_id; // 현재 매출
          next_state.move_from = state.room_id; // 이동전 room_id

          // 다음객실 상태.
          this.props.handleSaveRoomState(
            selected.id,
            {
              room_state: next_state,
            },
            () => {
              this.props.handleRoomSelected(selected.id);
              // this.props.handleDisplayRoomLayer(true); // 다음 객실 레이어 오픈.
            }
          );
        });

        this.props.closeRoomMove();
      }
    });
  };

  closeModal = (evt) => {
    this.props.closeRoomMove();
  };

  componentDidMount = () => {
    const {
      room,
      roomState,
      preferences: { room_move_no_key },
    } = this.props;

    let moveRooms = [];

    _.map(room.list, (v) => {
      let state = _.find(roomState.list, { room_id: v.id });
      if (state && !state.room_sale_id && !state.clean && (room_move_no_key ? state.key !== 1 : true)) {
        moveRooms.push(v);
      }
    });

    moveRooms = _.orderBy(moveRooms, ["floor", "name", "type_name"], ["asc", "asc", "asc"]);

    console.log("- moveRooms", moveRooms);

    this.setState({
      moveRooms,
    });
  };

  render() {
    return <RoomMove {...this.props} {...this.state} handleSort={this.handleSort} handleRoomClick={this.handleRoomClick} closeModal={this.closeModal} handelMoveRoom={this.handelMoveRoom} />;
  }
}

export default withSnackbar(Container);
