// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as roomStateAction } from "actions/roomState";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const {
    auth: { user },
    room,
    roomSale,
    roomState,
    preferences,
  } = state;

  return {
    user,
    room,
    roomSale,
    roomState,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeRoomSale: (id, roomSale, callback) => dispatch(roomSaleAction.putRoomSale(id, roomSale, callback)),

    handleDisplayRoomLayer: (display) => dispatch(roomViewAction.setDisplayRoomLayer(display)),

    handleRoomSelected: (selected) => dispatch(roomAction.setRoomSelected(selected)),

    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
