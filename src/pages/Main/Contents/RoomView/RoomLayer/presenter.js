// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";

import BlockUi from "react-block-ui";
import "react-block-ui/style.css";

import Draggable from "react-draggable"; // The default

//room-popup
import { Tab, Nav, NavItem, Form } from "react-bootstrap";

import swal from "sweetalert";
import Swal from "sweetalert2";
// import Alert from "sweetalert-react";

// styles.
import "./styles.scss";

// functional component
const RoomLayer = (props) => {
  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div className="room-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <Draggable handle="h1">
          {/* 폰트 사이즈 기준 설정 */}
          <div className="room-checkin" style={{ fontSize: props.preferences.font_size_2 }}>
            <h1 className={"sale-color-" + props.roomState.item.sale}>
              {props.room.item.name} [{props.room.item.type_name}] {props.tabs[props.selected - 1].name}
            </h1>
            <div className="checkin-form-wrapper">
              {/* 마일리지 체크 시 on */}
              <Form className={cx("checkin-form", props.useMileage && props.showMileage ? "on" : "off")}>
                <Tab.Container id="checkin-form-tab" defaultActiveKey={1} activeKey={props.selected} onSelect={(key) => props.clickTab(key)}>
                  <div className="clearfix">
                    <div className="checkin-tabs">
                      <Nav bsStyle="pills" stacked>
                        {props.tabs.map((tab, i) => (
                          <NavItem key={i} eventKey={i + 1}>
                            {tab.name}
                          </NavItem>
                        ))}
                      </Nav>
                    </div>
                    <div className="checkin-contents">
                      {/* 다른 기기에서 사용 중 일때 인터럽트 */}
                      <BlockUi
                        tag="div"
                        blocking={props.preferences.interrupt_use === 1 && props.isMyInterrupt !== true && props.interrupt && (props.interrupt.sale === 1 || props.interrupt.state === 1)}
                        message={
                          props.interrupt
                            ? props.interrupt.channel === 1
                              ? "이객실은 다른 직원이 조작 중 입니다. 잠시만 기다려 주세요."
                              : props.interrupt.channel === 2
                              ? "이객실은 무인 판매기에서 조작 중 입니다. 잠시만 기다려 주세요."
                              : props.interrupt.channel === 3
                              ? "이객실은 모바일에서 조작 중 입니다. 잠시만 기다려 주세요."
                              : props.interrupt.channel === 4
                              ? "이객실은 터치큐브에서 조작 중 입니다. 잠시만 기다려 주세요."
                              : ""
                            : null
                        }
                      >
                        <Tab.Content animation>
                          {props.tabs.map(
                            (tab, i) =>
                              tab.RoomTab && (
                                <Tab.Pane key={i} eventKey={i + 1} mountOnEnter unmountOnExit>
                                  <tab.RoomTab closeModal={props.closeModal} clickTab={props.clickTab} />
                                </Tab.Pane>
                              )
                          )}
                        </Tab.Content>
                      </BlockUi>
                    </div>
                  </div>
                </Tab.Container>
              </Form>
            </div>
            <a onClick={(evt) => props.closeModal(evt)} className="checkin-close">
              Close
            </a>
          </div>
        </Draggable>
      </Modal>
    </div>
  );
};

export default RoomLayer;
