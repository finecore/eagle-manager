// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import _ from "lodash";
import moment from "moment";
import { CheckSubscribe } from "utils/subscribe-util";

import Swal from "sweetalert2";
import { encrypt, decrypt } from "utils/aes256-util";
import format from "utils/format-util";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import RoomTab02 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    roomReserv: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    roomFee: PropTypes.object.isRequired,
    clickTab: PropTypes.func.isRequired,
    subscribe: PropTypes.object.isRequired,
    placeSubscribe: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      reserv: {}, // 현재 예약 정보
      reservList: [],
      reservRooms: [],
      roomTypeList: [],
      roomList: [],
      modify: {
        ota_code: 1,
        stay_type: 1,
        state: "A",
        room_fee: 0,
        default_fee: 0,
        reserv_fee: 0,
        add_fee: 0,
        prepay_ota_amt: 0,
        prepay_cash_amt: 0,
        prepay_card_amt: 0,
      }, // 변경 예약 정보
      assign_room: null, // 객실 배정.
    };
  }

  // 권한 체크.
  checkAuth = (type) => {
    let { modify } = this.state;

    const {
      auth: {
        user: { id, level },
      },
    } = this.props;

    // 본인 또는 매니저 권한 이상만 수정 가능.
    if (modify.user_id !== id && level > 2) {
      if (type) {
        Swal.fire({
          icon: "error",
          title: "권한 공지",
          html: type + " 권한이 없습니다.",
          showCloseButton: true,
          confirmButtonText: "확인",
        });
      }
      return false;
    }

    return true;
  };

  getDefault = () => {
    const {
      auth: {
        user: { place_id },
      },
      room: {
        item: { id: room_id, room_type_id },
      },
      roomType,
      roomFee,
      preferences: { use_rent_begin, use_rent_end, reserv_auto_assign_room },
    } = this.props;

    let hour = Number(moment().format("HH"));

    // 숙박/대실 시간대 (대실 가능 시간 이외는 숙박으로 처리)
    const stay_type = Number(hour) >= Number(use_rent_begin) && Number(hour) < Number(use_rent_end) ? 2 : 1;

    // 입실 시간(현재 시간 + 1)
    let check_in_exp = moment().add(1, "hour").format("YYYY-MM-DD HH:00");
    let check_out_exp = this.setCheckOutExp(check_in_exp, stay_type);

    let {
      default_fee_stay = 0,
      default_fee_rent = 0,
      reserv_discount_fee_stay = 0,
      reserv_discount_fee_rent = 0,
    } = room_type_id
      ? _.find(roomType.list, {
          id: room_type_id,
        })
      : {};

    let room_fee = stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
    let reserv_fee = stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

    let modify = {
      place_id,
      room_type_id,
      room_id: reserv_auto_assign_room ? room_id : null, // 자동 배정.
      ota_code: 1,
      stay_type,
      state: "A",
      check_in_exp,
      check_out_exp,
      reserv_date: moment().format("YYYY-MM-DD HH:mm"),
      room_fee,
      default_fee: room_fee,
      reserv_fee,
      add_fee: 0,
      prepay_ota_amt: reserv_fee,
      prepay_cash_amt: 0,
      prepay_card_amt: 0,
      memo: "",
    };

    let f_out = moment(modify.check_out_exp).format("YYYYMMDD");
    let f_in = moment(modify.check_in_exp).format("YYYYMMDD");

    // 요금 재설정.
    let stayDays = moment(f_out).diff(moment(f_in), "days");

    if (stayDays === 0) stayDays = 1;

    console.log("- stayDays ", { f_in, f_out, stayDays });

    for (var i = 0; i < stayDays; i++) {
      let date = moment(modify.check_in_exp).add("day", i);
      let hhmm = moment(modify.check_in_exp).format("HHmm");

      let day = date.isoWeekday(); // 해당 요일의 해당 시간대의 추가 요금 조회.

      let roomFeeTypeList = _.filter(roomFee.list, (v) => v.room_type_id === modify.room_type_id && v.day === day);

      console.log("- roomFeeTypeList ", { date, hhmm, day, roomFeeTypeList });

      // 요일 / 시간 요금.
      let add_fee = 0;

      roomFeeTypeList.forEach((v) => {
        // 적용 채널 (0: 카운터, 1:자판기)
        if (v.channel === 0 && v.stay_type === modify.stay_type) {
          let st = Number(v.begin);
          let et = Number(v.end);
          let nt = Number(hhmm);

          console.log("- fee ", v, st, "<=", nt, "~", nt, "<", et);

          // 이전일 ~ 다음일 시간 이라면.
          if (st > et) {
            if (st <= nt || et > nt) {
              add_fee += v.add_fee;
            }
          } else {
            if (st <= nt && et > nt) {
              add_fee += v.add_fee;
            }
          }
        }
      });

      console.log("- add_fee ", add_fee);

      modify.add_fee += add_fee;
    }

    modify.default_fee = modify.default_fee * stayDays;
    modify.room_fee = modify.default_fee + modify.add_fee;
    modify.reserv_fee = modify.reserv_fee * stayDays + modify.add_fee;

    console.log("- getDefault", modify);

    this.setState({
      reserv: _.cloneDeep(modify),
    });

    return modify;
  };

  selReserv = (id) => {
    const { reservRooms } = this.state;
    let {
      room: { enable_list },
    } = this.props;

    let modify = _.find(reservRooms, { id: Number(id || 0) });

    console.log("- selReserv", id, modify);

    if (!modify) modify = this.getDefault();

    // 예약 정보 셋팅.
    this.setState(
      {
        modify: _.cloneDeep(modify),
        reserv: _.cloneDeep(modify),
        assign_room: null,
      },
      () => {
        // 예약 가능 객실.
        this.setEnableReservRooms(_.filter(enable_list, (v) => v.room_type_id === modify.room_type_id));
      }
    );
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value, save) => {
    let { reserv, modify, assign_room } = this.state;
    let {
      room: { enable_list },
      roomType: { list: roomTypeList },
      roomFee,
    } = this.props;

    // 아이템 정보 변경.
    modify[name] = !value || isNaN(value) || name.lastIndexOf("_id") === -1 ? value : Number(value);

    console.log("- handleInputChange name", name, "value", modify[name]);

    if (name === "state") {
      // 상태 변경은 매니저 권한 이상만 수정 가능.
      if (modify.id && reserv.state !== modify.state && !this.checkAuth("수정")) return;
      if (value === 9) {
        modify.prepay_cash_amt = 0;
        modify.prepay_card_amt = 0;
      } else {
        modify.prepay_ota_amt = 0;
      }
    } else if (name === "stay_type" || name === "ota_code" || name === "room_type_id" || name === "check_in_exp" || name === "check_out_exp") {
      let room_type =
        _.find(roomTypeList, {
          id: modify.room_type_id,
        }) || {};

      let { default_fee_stay = 0, default_fee_rent = 0, reserv_discount_fee_stay = 0, reserv_discount_fee_rent = 0 } = room_type;

      modify.room_fee = modify.stay_type === 2 ? default_fee_rent : default_fee_stay; // 숙박 기본 요금
      modify.default_fee = modify.room_fee; // 숙박 기본 요금
      modify.reserv_fee = modify.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금
      modify.add_fee = 0; // 추가 요금

      modify.prepay_ota_amt = 0;
      modify.prepay_cash_amt = 0;
      modify.prepay_card_amt = 0;

      // 객실 배정 초기화.
      if (name === "stay_type" || name === "room_type_id") modify.room_id = null;
    } else if (name === "reserv_num") {
      modify.reserv_num = String(modify.reserv_num || "").replace(/[^\d]/g, "");
    }

    if (name === "stay_type" || name === "check_in_exp") {
      if (name === "stay_type") {
        // 숙박/대실 입실 시간 초기 셋팅.
        modify.check_in_exp = moment().add(1, "hour").format("YYYY-MM-DD HH:00");
      }

      if (name === "stay_type") modify.check_out_exp = this.setCheckOutExp(modify.check_in_exp);
    }

    // 입실일자가 퇴실일자 보다 크다면 퇴실일시는 최소 1시간 이후로 설정.
    if (moment(modify.check_in_exp).format("YYYYMMDDHHmm") >= moment(modify.check_out_exp).format("YYYYMMDDHHmm")) {
      modify.check_out_exp = this.setCheckOutExp(modify.check_in_exp);
    }

    // 객실 배정
    if (name === "room_id") {
      assign_room = modify.room_id;
    }

    if (!modify.room_id) assign_room = null;

    // 예약 일시 | 숙박 형태 | 객실 타입 변경 시 예약 가능 객실 초기화.
    if (name === "check_in_exp" || name === "check_out_exp" || name === "stay_type" || name === "room_type_id") {
      // 예약 가능 객실.
      this.setEnableReservRooms(_.filter(enable_list, (v) => v.room_type_id === modify.room_type_id));

      let f_out = moment(modify.check_out_exp).format("YYYYMMDD");
      let f_in = moment(modify.check_in_exp).format("YYYYMMDD");

      // 요금 재설정.
      let stayDays = moment(f_out).diff(moment(f_in), "days");

      if (stayDays === 0) stayDays = 1;

      console.log("- stayDays ", { f_in, f_out, stayDays });

      for (var i = 0; i < stayDays; i++) {
        let date = moment(modify.check_in_exp).add("day", i);
        let hhmm = moment(modify.check_in_exp).format("HHmm");

        let day = date.isoWeekday(); // 해당 요일의 해당 시간대의 추가 요금 조회.

        let roomFeeTypeList = _.filter(roomFee.list, (v) => v.room_type_id === modify.room_type_id && v.day === day);

        console.log("- roomFeeTypeList ", { date, hhmm, day, roomFeeTypeList });

        // 요일 / 시간 요금.
        let add_fee = 0;

        roomFeeTypeList.forEach((v) => {
          // 적용 채널 (0: 카운터, 1:자판기)
          if (v.channel === 0 && v.stay_type === modify.stay_type) {
            let st = Number(v.begin);
            let et = Number(v.end);
            let nt = Number(hhmm);

            console.log("- fee ", v, st, "<=", nt, "~", nt, "<", et);

            // 이전일 ~ 다음일 시간 이라면.
            if (st > et) {
              if (st <= nt || et > nt) {
                add_fee += v.add_fee;
              }
            } else {
              if (st <= nt && et > nt) {
                add_fee += v.add_fee;
              }
            }
          }
        });

        console.log("- add_fee ", add_fee);

        modify.add_fee += add_fee;
      }

      modify.default_fee = modify.default_fee * stayDays;
      modify.room_fee = modify.default_fee + modify.add_fee;
      modify.reserv_fee = modify.reserv_fee * stayDays + modify.add_fee;
    }

    // OTA 예약 결제 요금 셋팅
    if (modify.ota_code && modify.ota_code !== 9) modify.prepay_ota_amt = modify.reserv_fee;

    // 결제 금액 조정.
    modify.prepay_cash_amt = modify.prepay_cash_amt > modify.reserv_fee ? modify.reserv_fee : modify.prepay_cash_amt;
    modify.prepay_card_amt = modify.prepay_card_amt > modify.reserv_fee ? modify.reserv_fee : modify.prepay_card_amt;

    if (modify.prepay_cash_amt + modify.prepay_card_amt > modify.reserv_fee) modify.prepay_card_amt = modify.reserv_fee - modify.prepay_cash_amt;

    console.log("- modify", modify);

    this.setState({
      modify,
      assign_room,
    });
  };

  setEnableReservRooms = (enable_list) => {
    let { modify, reservList } = this.state;

    const {
      preferences: { reserv_auto_assign_room },
    } = this.props;

    reservList = _.filter(reservList, (r) => modify.room_type_id === r.room_type_id);

    console.log("- setEnableReservRooms enable_list", enable_list.length, reservList.length);

    let roomList = [];

    let mie = Number(moment(modify.check_in_exp).format("YYYYMMDDHHmm"));
    let moe = Number(moment(modify.check_out_exp).format("YYYYMMDDHHmm"));

    console.log("- setEnableReservRooms ", { mie, moe });

    // 예약 가능 객실 목록.
    _.map(enable_list, (room) => {
      const { id, name, sale, check_in, check_out, check_out_exp } = room;

      let ie = Number(moment(check_in).format("YYYYMMDDHHmm"));
      let oe = Number(moment(check_out || check_out_exp).format("YYYYMMDDHHmm"));

      // console.log("- setEnableReservRooms room", { id, name, sale, ie, oe });

      let isSaled = sale !== 0 && mie < oe;

      let isReserved = false;

      if (!isSaled) {
        // 현재 객실 예약이 있는지 여부.
        _.map(_.filter(reservList, { room_id: id }), (reserv) => {
          const { state, reserv_num, check_in_exp, check_out_exp } = reserv;

          // 현재 선택 예약 여부.
          let isModify = modify.reserv_num && modify.reserv_num === reserv_num;

          let ie2 = Number(moment(check_in_exp).format("YYYYMMDDHHmm"));
          let oe2 = Number(moment(check_out_exp).format("YYYYMMDDHHmm"));

          // 정상 예약만 검사 한다.
          if (state === "A") {
            let isAlreadyReserv = mie < oe2 && moe > ie2; // 다른 예약에 겹치는지 체크.

            if (isModify) isAlreadyReserv = false; // 현재 선택 예약건은 자기자신은 체크 안한다.

            if (isAlreadyReserv) {
              isReserved = true;
              // console.log("- setEnableReservRooms", { name, ie2, oe2, mie, moe, isAlreadyReserv, isNoReservRoom });
              return false;
            }
          }
        });
      }

      console.log("- setEnableReservRooms isReserved", { name, isSaled, isReserved });

      if (!isSaled && !isReserved) {
        roomList.push({ id, name }); // 예약 가능 객실 목록.
      }
    });

    roomList = _.sortBy(_.uniqBy(roomList, "id"), ["name"]);

    // console.log("- setEnableReservRooms room_id", modify.room_id, roomList);

    // 객실 자동 배정.
    if (reserv_auto_assign_room) {
      // 배정 객실이 있고 목록에 없다면 재 배정.
      if (modify.room_id && !_.find(roomList, { id: modify.room_id })) {
        modify.room_id = null;
      }

      if (!modify.room_id) {
        // 첫번째 객실 배정.
        let assign_room = null;

        _.each(roomList, (room) => {
          if (modify.room_name !== room.name) {
            assign_room = room;
            return false;
          }
        });

        if (assign_room) {
          modify.room_id = assign_room.id;
          modify.room_name = assign_room.name;

          console.log("- setEnableReservRooms 자동 배정", { modify });
        }
      }
    }

    // console.log("- setEnableReservRooms", { modify });

    // 화면 랜더링을 위해 딜레이 준다(알자 선택 시 state 최신 반영 안되는 현상 때문)
    setTimeout(() => {
      this.setState({
        roomList,
        modify,
      });
    }, 100);
  };

  // OTA/현금/카드 결제금액 설정.
  handlePayCheck = (type) => {
    console.log("- handlePayCheck", type);

    let { modify } = this.state;

    if (type === "prepay_ota_amt") {
      modify.prepay_ota_amt = modify.reserv_fee;
      modify.prepay_cash_amt = 0;
      modify.prepay_card_amt = 0;
    } else if (type === "prepay_cash_amt") {
      modify.prepay_ota_amt = 0;
      modify.prepay_cash_amt = modify.reserv_fee - modify.prepay_card_amt;
      modify.prepay_card_amt = modify.reserv_fee === modify.prepay_cash_amt ? modify.reserv_fee : modify.reserv_fee - modify.prepay_cash_amt;
    } else if (type === "prepay_card_amt") {
      modify.prepay_ota_amt = 0;
      modify.prepay_card_amt = modify.reserv_fee === modify.prepay_cash_amt ? modify.reserv_fee : modify.reserv_fee - modify.prepay_cash_amt;
      modify.prepay_cash_amt = modify.reserv_fee - modify.prepay_card_amt;
    }

    // console.log("- handlePayCheck modify", modify);

    this.handleInputChange("prepay_ota_amt", modify.prepay_ota_amt);
    this.handleInputChange("prepay_cash_amt", modify.prepay_cash_amt);
    this.handleInputChange("prepay_card_amt", modify.prepay_card_amt);
  };

  onSave = () => {
    let { reserv, modify } = this.state;

    modify.check_in_exp = moment(modify.check_in_exp).format("YYYY-MM-DD HH:mm");
    modify.check_out_exp = moment(modify.check_out_exp).format("YYYY-MM-DD HH:mm");
    modify.reserv_date = moment(modify.reserv_date || new Date()).format("YYYY-MM-DD HH:mm");

    console.log("onSave room_reserv", modify);

    // 상태 변경은 매니저 권한 이상만 수정 가능.
    if (modify.id && reserv.state !== modify.state && !this.checkAuth("수정")) return;

    let validate = "";
    if (!modify.state) validate = "예약상태를 선택해 주세요.";
    if (!modify.ota_code && !validate) validate = "OTA업체를 선택해 주세요.";
    if (!modify.room_type_id && !validate) validate = "객실타입을 선택해 주세요.";
    // if (!modify.hp && !validate) validate = "휴대폰번호를 입력해주세요.";
    if (!modify.reserv_num && !validate) validate = "예약번호를 입력해주세요.";
    // if (!modify.name && !validate) validate = "고객명을 입력해주세요.";
    if (!modify.reserv_fee && !validate) validate = "예약가를 입력해주세요.";

    if (validate) {
      Swal.fire({
        icon: "warning",
        title: "입력 확인",
        html: validate,
        showCloseButton: true,
        confirmButtonText: "확인",
      });
      return false;
    }

    Swal.fire({
      icon: "warning",
      title: "예약 " + (!modify.id ? "등록" : "수정") + " 확인",
      html: "예약을 " + (!modify.id ? "등록" : "수정") + " 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        if (!modify.id) {
          this.props.handleNewRoomReserv({ room_reserv: modify }).then((res) => {
            if (res) {
              Swal.fire({
                icon: "success",
                title: "등록 완료",
                html: "예약이 정상적으로 등록 되었습니다. <br/>예약번호를 SMS 발송 하시겠습니까?",
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonText: "예",
                cancelButtonText: "아니오",
              }).then((result) => {
                if (result.value) {
                  this.sendReservNum();
                }
                this.props.clickTab(1);
              });
            }
          });
        } else {
          this.props.handleSaveRoomReserv(modify.id, { room_reserv: modify }).then((res) => {
            if (res) {
              this.props.clickTab(1);
            }
          });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onCancel = () => {
    let { modify } = this.state;

    // 상태 변경은 매니저 권한 이상만 수정 가능.
    if (!this.checkAuth("수정")) return;

    Swal.fire({
      icon: "warning",
      title: "예약 취소 확인",
      html: "예약을 취소 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        let room_reserv = {
          state: "B",
        };

        this.props.handleSaveRoomReserv(modify.id, { room_reserv }).then((res) => {
          // if (res) this.props.changeTab(1, {});
        });
      }
    });
  };

  setCheckOutExp = (check_in_exp = new Date(), stay_type) => {
    let { modify } = this.state;

    const {
      preferences: { stay_time_type, stay_time_weekend, stay_time, rent_time_am_weekend, rent_time_pm_weekend, rent_time_am, rent_time_pm },
    } = this.props;

    stay_type = stay_type || modify.stay_type;

    const day = moment(check_in_exp).isoWeekday();
    let hour = moment(check_in_exp).hour();

    // 기본 숙박 시간
    let stayTime = day > 5 ? stay_time_weekend : stay_time;

    // 기본 대실 시간
    let rentTime =
      Number(hour) < 12 // 오전
        ? day > 5 // 주말
          ? rent_time_am_weekend
          : rent_time_am
        : day > 5
        ? rent_time_pm_weekend
        : rent_time_pm;

    rentTime = rentTime < 1 ? 1 : rentTime;

    console.log("- setCheckOutExp", stay_type, stayTime, rentTime);

    let check_out_exp = moment(check_in_exp).format("YYYY-MM-DD HH:mm"); // 체크아웃 예정일.

    if (stay_type === 1) {
      // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
      if (stay_time_type === 1) {
        check_out_exp = moment(check_in_exp).hour(stayTime).format("YYYY-MM-DD HH:00");

        // 퇴실 시간이 입실 시간 이전이라면 1일 추가.
        if (moment(check_in_exp).isSameOrAfter(moment(check_out_exp))) {
          check_out_exp = moment(check_out_exp).add(1, "day");
        }
        // 퇴실 까지 이용 시간이 8 시간 이내라면 1일 추가
        else if (moment(check_in_exp).date() === moment(check_out_exp).date() && moment(check_in_exp).add(8, "hour").hour() > moment(check_out_exp).hour()) {
          check_out_exp = moment(check_out_exp).add(1, "day").format("YYYY-MM-DD HH:00"); // 체크아웃 예정일.
        }
      } else {
        check_out_exp = moment(check_in_exp).add(stayTime, "hour").format("YYYY-MM-DD HH:00"); // 체크아웃 예정일.
      }
    } else {
      check_out_exp = moment(check_in_exp).add(rentTime, "hour").format("YYYY-MM-DD HH:00"); // 체크아웃 예정일.
    }

    console.log("- check_out_exp", check_out_exp);

    return check_out_exp;
  };

  // 달력
  onFocusChange = ({ focused1, focused2, focused3 }) => {
    let state = _.merge({}, this.state, { focused1, focused2, focused3 });
    this.setState({ ...state });
  };

  setReservNum = () => {
    let { modify } = this.state;

    modify.reserv_num = String(modify.hp || "").replace(/[^\d]/g, "");

    console.log("- setReservNum");

    this.setState({
      modify,
    });
  };

  getReservNum = () => {
    let { modify } = this.state;

    const {
      preferences: { place_id },
    } = this.props;

    const num = Math.floor(Math.random() * 1000);

    let reserv_num = String(place_id + num).padStart(4, "0") + moment().format("mmSSHHss"); //  업소 고유번호 + 시간 섞음.

    modify.reserv_num = reserv_num;

    console.log("- getReservNum", { reserv_num });

    this.setState({
      modify,
    });
  };

  sendReservNum = (phone) => {
    let { modify } = this.state;
    const { place, placeSubscribe, roomType } = this.props;

    let { name: typeName } = _.find(roomType.list, {
      id: modify.room_type_id,
    });

    if (process.env.REACT_APP_MODE !== "PROD" && !phone) {
      var result = confirm("테스트 SMS는 01098440098 번호로 발송 됩니다. 발송 하시겠습니까?");
      if (result) {
        this.sendReservNum("01098440098");
      }
      return;
    }

    let sms = {
      tr_phone: format.toPhone(phone || modify.hp),
      tr_msg: `[${place.item.name}] \n예약번호: ${String(modify.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")} \n예약일자: ${moment(modify.check_in_exp).format(
        "YYYY/MM/DD HH:mm"
      )} \n객실정보: ${typeName}`,
    };

    console.log("- sendReservNum", { sms }, placeSubscribe.list);

    // SMS 문자 발송 서비스 구독여부.
    if (!CheckSubscribe(this.props, "10")) {
      return;
    }

    // 인증 번호 발송.
    this.props.handleSmsSendMsg({ sms });

    Swal.fire({
      icon: "success",
      title: "문자 발송 알림",
      html: `휴대전화 번호로 SMS 문자 발송을 했습니다.`,
      confirmButtonText: "확인",
    });
  };

  sendQRReservNum = (phone) => {
    let { modify } = this.state;
    const { place, placeSubscribe, roomType } = this.props;

    // SMS QR 문자 발송 서비스 구독여부.
    if (!CheckSubscribe(this.props, "31")) {
      return;
    }

    let { name: typeName } = _.find(roomType.list, {
      id: modify.room_type_id,
    });

    if (process.env.REACT_APP_MODE !== "PROD" && !phone) {
      var result = confirm("테스트 SMS는 01098440098 번호로 발송 됩니다. 발송 하시겠습니까?");
      if (result) {
        this.sendQRReservNum("01098440098");
      }
      return;
    }

    let hash = encrypt(modify.id);

    hash = encodeURIComponent(hash);

    let host = process.env.REACT_APP_HOST.replace("localhost", location.hostname);

    let qr_link = `http://${host}/QR/reserv/${hash}`;

    let tr_msg = `${place.item.name || ""}\nQR:${qr_link}`;

    let sms = {
      tr_phone: format.toPhone(phone || modify.hp),
      tr_msg,
    };

    console.log("- sendQRReservNum", { sms });

    // 인증 번호 발송.
    this.props.handleSmsSendMsg({ sms });

    Swal.fire({
      icon: "success",
      title: "QR문자 발송 알림",
      html: `휴대전화 번호로 QR 문자 발송을 했습니다.`,
      confirmButtonText: "확인",
    });
  };

  closeModal = (evt) => {
    this.props.handleDisplayRoomLayer(false);
  };

  reOpenModal = (evt) => {
    this.props.handleDisplayRoomLayer(false);
    setTimeout(() => {
      this.props.handleDisplayRoomLayer(true);
    }, 200);
  };

  // 환경 설정.
  handlePreferencesSave = (name, value) => {
    console.log("- handlePreferencesSave", name, value);

    let { roomList, modify } = this.state;
    const { preferences, room } = this.props;

    let modifyClone = _.cloneDeep(preferences);

    modifyClone[name] = value;

    // Object To Json.
    if (modifyClone.voice_opt) modifyClone.voice_opt = JSON.stringify(modifyClone.voice_opt);
    if (modifyClone.stay_type_color) modifyClone.stay_type_color = JSON.stringify(modifyClone.stay_type_color);
    if (modifyClone.key_type_color) modifyClone.key_type_color = JSON.stringify(modifyClone.key_type_color);
    if (modifyClone.room_name_color) modifyClone.room_name_color = JSON.stringify(modifyClone.room_name_colors);
    if (modifyClone.time_bg_color) modifyClone.time_bg_color = JSON.stringify(modifyClone.time_bg_color);

    this.props.handleSavePreferences(preferences.id, {
      preferences: modifyClone,
    });

    // 객실 자동 배정.
    if (name === "reserv_auto_assign_room" && value === 1) {
      let { enable_list, item } = room;

      if (enable_list) {
        if (!roomList.length) roomList = _.filter(enable_list, (v) => v.room_type_id === item.room_type_id);

        if (roomList.length) {
          // 첫번째 객실 배정.
          let assign_room = roomList[0];

          if (roomList.length && !modify.room_id) {
            modify.room_id = assign_room.id;
            modify.room_name = assign_room.name;

            this.setState({
              modify,
            });
          }
        }
      }
    }
  };

  componentDidMount = () => {
    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    this.setState({ modify: this.getDefault() });

    // 객실 예약 정보 .
    let filter = `a.place_id=${place_id}`;
    let begin = moment().add(-3, "day").format("YYYY-MM-DD 00:00");
    let end = moment().add(1, "month").format("YYYY-MM-DD 00:00");

    // 예약 정보.
    this.props.initRoomReservs(filter, begin, end).then((res) => {
      // 현재 예약 가능 객실 목록.
      this.props.initRoomEnableReservs(place_id).then((res) => {
        console.log("- initRoomEnableReservs", res);

        if (res) {
          let { roomType } = this.props;

          // 배정 가능 객실 목록
          if (roomType && roomType.list[0]) this.handleInputChange("room_type_id", roomType.list[0].id);
        }
      });
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 예약 목록.
    if (nextProps.roomReserv && nextProps.roomReserv !== this.props.roomReserv && nextProps.roomReserv.list !== this.props.roomReserv.list) {
      let { list } = nextProps.roomReserv;
      let {
        room: {
          item: { room_type_id, id },
        },
      } = this.props;

      let reservRooms = _.filter(list, (v) => {
        // 퇴실 예정 시간 지났는지 여부.
        let isOvertime = moment(v.check_out_exp).isBefore(moment());
        return !isOvertime && v.state === "A" && v.room_type_id === room_type_id;
      });

      console.log("- componentWillReceiveProps roomReserv", list.length, reservRooms);

      reservRooms = _.orderBy(reservRooms, ["room_id", "check_in_exp"], ["asc", "asc"]);

      this.setState({
        reservList: list,
        reservRooms,
      });
    }

    // 배정 가능 객실 목록
    if (nextProps.room && nextProps.room !== this.props.room) {
      let { enable_list, item } = nextProps.room;

      console.log("- componentWillReceiveProps enable_list", enable_list);

      if (enable_list !== this.props.room.enable_list || item !== this.props.room.item) {
        this.setEnableReservRooms(_.filter(enable_list, (v) => v.room_type_id === item.room_type_id));
      }
    }
  }

  render() {
    return (
      <RoomTab02
        {...this.props}
        {...this.state}
        selReserv={this.selReserv}
        onCancel={this.onCancel}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSave={this.onSave}
        checkAuth={this.checkAuth}
        onFocusChange={this.onFocusChange}
        handlePayCheck={this.handlePayCheck}
        setReservNum={this.setReservNum}
        getReservNum={this.getReservNum}
        sendReservNum={this.sendReservNum}
        sendQRReservNum={this.sendQRReservNum}
        handlePreferencesSave={this.handlePreferencesSave}
      />
    );
  }
}

export default withSnackbar(Container);
