// imports.
import React from "react";
import { ControlLabel, Checkbox, FormGroup, FormControl, Table, ButtonToolbar, Button } from "react-bootstrap";

import ReactTooltip from "react-tooltip";
import cx from "classnames";
import { keyToValue } from "constants/key-map";

import moment from "moment";
import format from "utils/format-util";
import _ from "lodash";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

const RoomTab02 = (props) => {
  let isDisable = props.modify.state === "C" || props.modify.check_out;

  return (
    <div>
      <ReactTooltip
        id={"roomReserv"}
        place="bottom"
        type="info"
        effect="solid"
        delayShow={1000}
        delayHide={0}
        html={true}
        getContent={(dataTip) => `${dataTip}`}
        className={cx("tooltop")}
        disable={props.preferences.tooltip_show !== 1 || props.edit} // 기타 정보
      />

      <FormGroup className="reserve-date-select">
        <FormControl componentClass="select" placeholder="예약 목록" value={props.modify.id} onChange={(evt) => props.selReserv(evt.target.value)}>
          <option>
            [{props.room.item.type_name}] {props.reservRooms.length ? "예약 목록 선택 " : "예약 없음"}
          </option>
          {props.reservRooms.map((item, i) => (
            <option key={i} value={item.id}>
              {"[" +
                keyToValue("room_sale", "stay_type", item.stay_type) +
                "] " +
                (item.room_id ? "【" + item.room_name + "】" : "(객실 미배정)") +
                " (" +
                moment(item.check_in_exp).format("MM/DD HH:mm") +
                " ~ " +
                moment(item.check_out_exp).format("MM/DD HH:mm") +
                ") " +
                String(item.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-") +
                " " +
                (item.name || "미등록고객")}
            </option>
          ))}
        </FormControl>
      </FormGroup>
      <ul className="list-bul">
        <li>
          <h2>예약 정보 입력</h2>
          <div className="form-content">
            <Table>
              <tbody>
                <tr>
                  <th>
                    <ControlLabel htmlFor="reserve02-01">숙박 형태</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      id="reserve02-01"
                      componentClass="select"
                      placeholder="숙박형태"
                      value={props.modify.stay_type}
                      onChange={(evt) => props.handleInputChange("stay_type", Number(evt.target.value))}
                      style={{
                        color: props.preferences.stay_type_color[props.modify.stay_type],
                      }}
                    >
                      <option value={1}>숙박</option>
                      <option value={2}>대실</option>
                    </FormControl>
                  </td>
                  <th>
                    <ControlLabel htmlFor="reserve02-02">예약 상태</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      id="reserve02-02"
                      componentClass="select"
                      placeholder="예약상태"
                      value={props.modify.state}
                      onChange={(evt) => props.handleInputChange("state", evt.target.value)}
                      style={{
                        color: props.modify.state === "C" ? "#a000ff" : props.modify.state === "B" ? "#ff0000" : "#0000ff",
                      }}
                      disabled={!props.checkAuth()}
                    >
                      <option value={"A"}>정상예약</option>
                      {props.modify.id && <option value={"B"}>취소예약</option>}
                      {props.modify.id && <option value={"C"}>사용완료</option>}
                    </FormControl>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-03">예약 방법</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      id="info02-03"
                      componentClass="select"
                      placeholder="예약 방법"
                      value={props.modify.ota_code}
                      onChange={(evt) => props.handleInputChange("ota_code", Number(evt.target.value))}
                      style={{
                        color: props.modify.ota_code === 9 ? "#f60ea2" : "",
                      }}
                      disabled={isDisable}
                    >
                      <option value={1}>야놀자</option>
                      <option value={2}>여기어때</option>
                      <option value={3}>네이버</option>
                      <option value={4}>에어비앤비</option>
                      <option value={5}>호텔나우</option>
                      <option value={8}>기타</option>
                      <option value={9}>직접예약</option>
                    </FormControl>
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-04">예약일</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      id="info02-04"
                      placeholder="예약일"
                      value={moment(props.modify.reserv_date || new Date()).format("YYYY-MM-DD HH:mm")}
                      onChange={(evt) => props.handleInputChange("reserv_date", evt.target.value)}
                      readOnly={true}
                      disabled={isDisable}
                    />
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-05">객실 타입</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      componentClass="select"
                      id="info02-05"
                      placeholder="객실 타입"
                      value={props.modify.room_type_id || ""}
                      onChange={(evt) => props.handleInputChange("room_type_id", evt.target.value)}
                      disabled={isDisable}
                    >
                      <option value="">객실타입 선택</option>
                      {props.roomType.list.map((item, i) => (
                        <option key={i} value={item.id}>
                          {item.name}
                        </option>
                      ))}
                    </FormControl>
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-06">객실 배정</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      id="info02-06"
                      componentClass="select"
                      placeholder="배정 가능 객실"
                      value={props.modify.room_id || ""}
                      onChange={(evt) => props.handleInputChange("room_id", evt.target.value ? evt.target.value : null)}
                      style={{
                        color: props.modify.room_id === props.reserv.room_id ? "#2d43e8" : "#ff0000",
                      }}
                      disabled={isDisable || !props.modify.room_type_id}
                    >
                      <option value="">{!props.modify.room_type_id ? "객실타입을 선택해주세요" : props.roomList.length ? "객실 미배정" : "배정가능 객실 없음"}</option>
                      {props.roomList.map((item, i) => (
                        <option key={i} value={item.id}>
                          {item.name}
                          {item.id === props.modify.room_id && props.modify.room_id !== props.reserv.room_id && !props.assign_room ? " [자동 배정]" : null}
                        </option>
                      ))}
                    </FormControl>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.preferences.reserv_auto_assign_room === 1 && "checked-on")}
                      defaultChecked={props.preferences.reserv_auto_assign_room === 1}
                      onChange={(evt) => props.handlePreferencesSave("reserv_auto_assign_room", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      자동 배정 설정
                    </Checkbox>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-07">입실 예정일</ControlLabel>
                  </th>
                  <td>
                    <SingleDatePicker
                      id="info02-07"
                      placeholder="입실일 선택"
                      displayFormat="YYYY-MM-DD"
                      focused={props.focused1}
                      onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                      date={moment(props.modify.check_in_exp)}
                      onDateChange={(date) => {
                        props.handleInputChange("check_in_exp", moment(date).format("YYYY-MM-DD HH:mm"));
                      }}
                      isOutsideRange={(day) => moment().add(-7, "day").isSameOrAfter(day)}
                      disabled={isDisable}
                    />
                    <div className="reserve-time-form">
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control sm75"
                          value={Number(moment(props.modify.check_in_exp).format("HH"))}
                          step={1}
                          min={0}
                          max={23}
                          formatter={(value) => `${value}시`}
                          onChange={(value) => {
                            props.handleInputChange("check_in_exp", moment(props.modify.check_in_exp).hour(value).format("YYYY-MM-DD HH:mm"));
                          }}
                          disabled={isDisable}
                        />
                      </span>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control sm75"
                          value={Number(moment(props.modify.check_in_exp).format("mm"))}
                          step={10}
                          min={0}
                          max={59}
                          formatter={(value) => `${value}분`}
                          onChange={(value) => {
                            props.handleInputChange("check_in_exp", moment(props.modify.check_in_exp).minute(value).format("YYYY-MM-DD HH:mm"));
                          }}
                          disabled={isDisable}
                        />
                      </span>
                    </div>
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-08">퇴실 예정일</ControlLabel>
                  </th>
                  <td>
                    <SingleDatePicker
                      id="info02-08"
                      placeholder="퇴실일 선택"
                      displayFormat="YYYY-MM-DD"
                      focused={props.focused2}
                      onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                      date={props.modify.check_out_exp ? moment(props.modify.check_out_exp) : moment()}
                      onDateChange={(date) => {
                        props.handleInputChange("check_out_exp", moment(date).format("YYYY-MM-DD HH:mm"));
                      }}
                      isOutsideRange={(day) => moment(props.modify.check_in_exp).isAfter(day)}
                      disabled={isDisable || props.modify.stay_type === 2}
                    />
                    <div className="reserve-time-form">
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control sm75"
                          value={Number(moment(props.modify.check_out_exp).format("HH"))}
                          step={1}
                          min={0}
                          max={23}
                          formatter={(value) => `${value}시`}
                          onChange={(value) => {
                            props.handleInputChange("check_out_exp", moment(props.modify.check_out_exp).hour(value).format("YYYY-MM-DD HH:mm"));
                          }}
                          disabled={isDisable}
                        />
                      </span>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control sm75"
                          value={Number(moment(props.modify.check_out_exp).format("mm"))}
                          step={10}
                          min={0}
                          max={59}
                          formatter={(value) => `${value}분`}
                          onChange={(value) => {
                            props.handleInputChange("check_out_exp", moment(props.modify.check_out_exp).minute(value).format("YYYY-MM-DD HH:mm"));
                          }}
                          disabled={isDisable}
                        />
                      </span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-09">휴대폰 번호</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="tel"
                      id="info02-09"
                      placeholder="휴대전화 번호"
                      value={props.modify.hp || "010"}
                      onChange={(evt) => props.handleInputChange("hp", evt.target.value.replace(/[^\d]/g, "").replace(/(^02.{0}|^01.{1}|^05.{2}|[0-9]{3})([0-9]+)([0-9]{4})/, "$1-$2-$3"))}
                      maxLength={15}
                      disabled={isDisable}
                    />
                    {/* {!props.modify.id && (
                      <Button onClick={evt => props.setReservNum()} className="green-btn" disabled={isDisable}>
                        예약번호 입력
                      </Button>
                    )} */}
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-10">예약 번호</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      id="info02-10"
                      placeholder="예약번호 입력"
                      value={String(props.modify.reserv_num || "").replace(/\B(?=(\d{4})+(?!\d))/g, "-")}
                      onChange={(evt) => props.handleInputChange("reserv_num", evt.target.value.replace(/[^\d]/g, ""))}
                      maxLength={20}
                      disabled={isDisable || props.modify.id}
                    />
                    {!props.modify.id && (
                      <Button onClick={(evt) => props.getReservNum()} className="blue-btn" disabled={isDisable}>
                        번호 자동생성
                      </Button>
                    )}
                    {props.modify.id && format.isPhone(props.modify.hp || "") && props.modify.room_type_id && props.modify.reserv_num ? (
                      <FormGroup>
                        <Button onClick={(evt) => props.sendReservNum()} className="puple-btn">
                          SMS 발송
                        </Button>
                        <Button onClick={(evt) => props.sendQRReservNum()} className="puple-btn">
                          QR코드 발송
                        </Button>
                      </FormGroup>
                    ) : null}
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-11">고객명</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      id="info02-11"
                      placeholder="고객명"
                      value={props.modify.name || ""}
                      onChange={(evt) => props.handleInputChange("name", evt.target.value)}
                      disabled={isDisable}
                    />
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-12">방문형태</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      id="info02-12"
                      componentClass="select"
                      placeholder="방문형태"
                      value={props.modify.visit_type || ""}
                      onChange={(evt) => props.handleInputChange("visit_type", evt.target.value)}
                      disabled={isDisable}
                    >
                      <option value={1}>도보방문</option>
                      <option value={2}>차량방문</option>
                      <option value={3}>대중교통</option>
                    </FormControl>
                  </td>
                </tr>
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-13">판매가</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      id="info02-13"
                      placeholder="정상 판매요금"
                      value={`${props.modify.room_fee}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      onChange={(evt) => props.handleInputChange("room_fee", evt.target.value)}
                      readOnly={true}
                      disabled={isDisable}
                    />
                    {props.modify.add_fee > 0 && (
                      <span>
                        <span id="info02-19">기본: {format.toMoney(props.modify.default_fee)}</span> <span id="info02-191">추가: {format.toMoney(props.modify.add_fee)}</span>
                      </span>
                    )}
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-14">예약가</ControlLabel>
                  </th>
                  <td>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control"
                        id="info02-14"
                        value={props.modify.reserv_fee}
                        step={props.preferences.stay_fee_unit}
                        min={0}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("reserv_fee", value)}
                        disabled={isDisable}
                      />
                    </span>
                    {props.modify.room_fee - props.modify.reserv_fee > 0 && <span id="info02-20">할인: {format.toMoney(props.modify.room_fee - props.modify.reserv_fee)}</span>}
                  </td>
                </tr>
                {props.modify.ota_code < 9 && (
                  <tr>
                    <th>
                      <ControlLabel htmlFor="info02-15">OTA 결제</ControlLabel>
                    </th>
                    <td>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="info02-15"
                          value={props.modify.prepay_ota_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_ota_amt", value)}
                          disabled={isDisable || !props.modify.prepay_ota_amt}
                        />
                      </span>
                      <Button onClick={(evt) => props.handlePayCheck("prepay_ota_amt")} data-for="roomReserv" data-type="info" data-effect="solid" data-tip="클릭 시 OTA 결제 잔액이 자동 입력 됩니다.">
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </td>
                    <th></th>
                    <td></td>
                  </tr>
                )}
                {props.modify.ota_code === 9 && (
                  <tr>
                    <th>
                      <ControlLabel htmlFor="info02-15">현금 결제</ControlLabel>
                    </th>
                    <td>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="info02-15"
                          value={props.modify.prepay_cash_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          max={props.modify.reserv_fee - props.modify.prepay_card_amt}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_cash_amt", value)}
                          disabled={isDisable || !props.modify.prepay_cash_amt}
                        />
                      </span>
                      <Button
                        onClick={(evt) => props.handlePayCheck("prepay_cash_amt")}
                        data-for="roomReserv"
                        data-type="info"
                        data-effect="solid"
                        data-tip="클릭 시 카드 결제 잔액이 자동 입력 됩니다."
                      >
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </td>
                    <th>
                      <ControlLabel htmlFor="info02-16">카드 결제</ControlLabel>
                    </th>
                    <td>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="info02-16"
                          value={props.modify.prepay_card_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          max={props.modify.reserv_fee - props.modify.prepay_cash_amt}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_card_amt", value)}
                          disabled={isDisable || !props.modify.prepay_card_amt}
                        />
                      </span>
                      <Button
                        onClick={(evt) => props.handlePayCheck("prepay_card_amt")}
                        data-for="roomReserv"
                        data-type="info"
                        data-effect="solid"
                        data-tip="클릭 시 현금 결제 잔액이 자동 입력 됩니다."
                      >
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </td>
                  </tr>
                )}
                <tr>
                  <th>
                    <ControlLabel htmlFor="info02-17">결제 잔금</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      type="text"
                      id="info02-17"
                      placeholder="결제 잔금"
                      value={`${props.modify.reserv_fee - (props.modify.ota_code < 9 ? props.modify.prepay_ota_amt : props.modify.prepay_cash_amt + props.modify.prepay_card_amt)}원`.replace(
                        /\B(?=(\d{3})+(?!\d))/g,
                        ","
                      )}
                      readOnly={true}
                    />
                  </td>
                  <th>
                    <ControlLabel htmlFor="info02-18">고객 메모</ControlLabel>
                  </th>
                  <td>
                    <FormControl
                      componentClass="textarea"
                      id="info02-18"
                      placeholder="메모"
                      value={props.modify.memo || ""}
                      onChange={(evt) => props.handleInputChange("memo", evt.target.value)}
                      disabled={isDisable}
                    />
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </li>
        <li>
          <div className="form-content">
            <ButtonToolbar className="btn-room btn-set-close">
              {!props.modify.check_out && <Button onClick={(evt) => props.onSave(evt)}>{props.modify.id ? "예약 수정" : "예약 등록"}</Button>}
              {props.checkAuth() && props.reserv.id && props.reserv.state === "A" && (
                <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-warning">
                  예약 취소
                </Button>
              )}
              <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                닫기
              </Button>
            </ButtonToolbar>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default RoomTab02;
