// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import moment from "moment";
import _ from "lodash";

// Actions.
import { actionCreators as roomReservAction } from "actions/roomReserv";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomFeeAction } from "actions/roomFee";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as smsAction } from "actions/sms";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, room, place, roomType, roomReserv, roomSale, preferences, subscribe, placeSubscribe, sms, roomFee } = state;

  return {
    auth,
    room,
    place,
    roomType,
    roomReserv,
    roomSale,
    roomFee,
    preferences: preferences.item,
    subscribe,
    placeSubscribe,
    sms,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomEnableReservs: (placeId) => dispatch(roomAction.getRoomEnableReservs(placeId)),

    initAllRoomReservs: (palceId) => dispatch(roomReservAction.getNowRoomReservs(palceId)),

    initRoomReservs: (filter, begin, end) => dispatch(roomReservAction.getRoomReservs(filter, begin, end)),

    handleNewRoomReserv: (room_reserv) => dispatch(roomReservAction.newRoomReserv(room_reserv)),

    handleSaveRoomReserv: (id, room_reserv) => dispatch(roomReservAction.putRoomReserv(id, room_reserv)),

    handleSmsSendMsg: (sms) => dispatch(smsAction.newSms(sms)),

    handleDisplayRoomLayer: (display) => dispatch(roomViewAction.setDisplayRoomLayer(display)),

    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
