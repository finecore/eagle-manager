// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as voiceAction } from "actions/voice";

import { withSnackbar } from "notistack";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { layout, preferences, roomInterrupt, roomViewItem, room, roomType, roomTheme, roomSale, roomState, roomView, roomReserv, timeOption, device } = state;

  const { vItem, viewItems } = ownProps;

  return {
    layout,
    preferences: preferences.item,
    roomInterrupt,
    vItem,
    room,
    roomType,
    roomTheme,
    roomSale,
    roomState,
    roomView,
    roomViewItem,
    timeOption,
    viewItems,
    roomReserv,
    device,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleRoomSelected: (selected) => dispatch(roomAction.setRoomSelected(selected)),

    handleNewRoom: (room, callback) => dispatch(roomAction.newRoom(room, callback)),

    handleSaveRoom: (id, room) => dispatch(roomAction.putRoom(id, room)),

    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),

    handleSaveRoomViewItems: (viewId, items) => dispatch(roomViewItemAction.updateAllRoomViewItems(viewId, items)),

    handleSaveRoomViewItem: (id, item) => dispatch(roomViewItemAction.putRoomViewItem(id, item)),

    handleNewRoomViewItem: (item) => dispatch(roomViewItemAction.newRoomViewItem(item)),

    handleSaveRoomSale: (id, roomSale) => dispatch(roomSaleAction.putRoomSale(id, roomSale)),

    getVoice: (text, callback) => dispatch(voiceAction.getVoice(text, callback)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
