// imports.
import React from "react";

import moment from "moment";
import cx from "classnames";
import format from "utils/format-util";
import _ from "lodash";

import ReactTooltip from "react-tooltip";

import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";

import { FormControl, Button } from "react-bootstrap";
import { displayState, keyToValue } from "constants/key-map";

// icons
import PowerOnIcon from "@material-ui/icons/Power";
import PowerOffIcon from "@material-ui/icons/PowerOff";
import ToysIcon from "@material-ui/icons/Toys";
// import CastIcon from "@material-ui/icons/Cast";
import WifiOnIcon from "@material-ui/icons/Wifi";
import WifiOffIcon from "@material-ui/icons/WifiOff";
import DriveEtaIcon from "@material-ui/icons/DriveEta";
import AccessibleIcon from "@material-ui/icons/Accessible";
import AcUnit from "@material-ui/icons/AcUnit";

// styles.
import "./styles.scss";

// room themes
import roomTheme1 from "./img/bg_room.png";
import roomTheme2 from "./img/bg_room.png";

// functional component
const Room = (props) => {
  // 테마 배경.
  const roomThemes = { default: roomTheme1, text: roomTheme2, color: "" };
  const roomTheme = roomThemes[props.view.theme];

  // state : 0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료, 6:퇴실
  const stateCode = props.roomStateItem.id ? displayState(props.roomStateItem, props.roomSaleItem, props.preferences) : 0;

  const remainRserv = _.filter(props.reservs, { state: "A" });

  // if (stateCode) console.log("- stateCode", props.roomItem.name, stateCode);

  return (
    <span>
      <ReactTooltip
        id={"roomTip" + props.vItem.room_id}
        place="bottom"
        type="info"
        effect="solid"
        html={true}
        getContent={(dataTip) => `${dataTip}`}
        className={cx("tooltop")}
        disable={!props.preferences.tooltip_show || props.edit} // 주요 정보
      />

      <ReactTooltip
        id={"roomSubTip" + props.vItem.room_id}
        place="bottom"
        type="info"
        effect="solid"
        html={true}
        getContent={(dataTip) => `${dataTip}`}
        className={cx("tooltop")}
        disable={props.preferences.tooltip_show !== 1 || props.edit} // 기타 정보
      />

      <span>
        <dl
          className={cx(
            "room-box",
            stateCode, // debug 용
            `${props.view.theme}-mode`,
            !props.edit &&
              (!props.roomView.filter.key[props.roomStateItem.key] || !props.roomView.filter.sale[props.roomStateItem.sale] || (!props.roomView.filter.outing[0] && props.roomStateItem.outing))
              ? "item-off"
              : "item-on", // 객실 화면 필터링.

            props.roomStateItem.door === 1 ? "door-on" : "", // 문열림

            // 공실/숙박/대실/장기 (공실 표시 안함)
            stateCode < 2 &&
              (props.roomStateItem.sale === 0
                ? props.device.isc.length
                  ? props.preferences.isc_sale === 1 ||
                    ((props.roomStateItem.isc_sale_1 === 1 || props.preferences.isc_sale_1 === 1) &&
                      (props.roomStateItem.isc_sale_2 === 1 || props.preferences.isc_sale_2 === 1) &&
                      (props.roomStateItem.isc_sale_3 === 1 || props.preferences.isc_sale_3 === 1))
                    ? "isc-sale-stop-on"
                    : ""
                  : null
                : props.roomStateItem.sale === 1
                ? "stay-on"
                : props.roomStateItem.sale === 2
                ? "rent-on"
                : "long-on"),

            // state : 0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료, 6:퇴실
            stateCode === 1 && props.roomStateItem.key === 1 && props.idm.maker !== "ShindoEDS" ? "men-on" : "", // 입실.
            stateCode === 2 ? "out-on" : "", // 외출.
            stateCode === 3 && props.idm.maker !== "ShindoEDS" ? "clean-ready-on" : "", // 청소대기
            stateCode === 4 && props.idm.maker !== "ShindoEDS" ? "clean-ing-on" : "", // 청소중.
            stateCode === 5 && props.idm.maker !== "ShindoEDS" ? "clean-end-on" : "", // 청소 완료
            stateCode === 6 ? "checkout-on" : "", // 퇴실.
            stateCode === 7 ? "inspect-ready-on" : "", // 인스펙트대기.
            stateCode === 8 ? "inspect-ing-on" : "", // 인스펙트 중.
            stateCode === 9 ? "inspect-end-on" : "", // 인스펙트 완료.

            props.roomSaleItem && props.roomSaleItem.alarm === 1 ? "alarm-on" : "", // 알람.
            props.roomStateItem.theft === 1 ? "theft-on" : "", // 보안
            // props.roomStateItem.security === 1 ? "security-on" : "", // 보안
            props.roomStateItem.light && props.roomStateItem.light.indexOf("1") === -1 ? "dim-on" : "", // 전등오프
            // props.roomStateItem.main_relay === 0 ? "dim-on" : "", // 메인 전원
            // props.roomStateItem.curtain && props.roomStateItem.curtain.indexOf("1") === -1 ? "curtain-on" : "", // 커튼 동작
            // props.roomStateItem.dimmer && props.roomStateItem.dimmer.indexOf("1") === -1 ? "dimmer-on" : "", // 딤머 동작
            props.interrupt && props.preferences.interrupt_use && props.interrupt.channel === 1 && (props.interrupt.sale === 1 || props.interrupt.state === 1) ? "interrupt-on" : "", // 프론트 조작중..
            props.interrupt && props.preferences.interrupt_use && props.interrupt.channel === 2 && (props.interrupt.sale === 1 || props.interrupt.state === 1) ? "isc-saling-on" : "", // 무인 판매중..
            props.interrupt && props.preferences.interrupt_use && props.interrupt.channel === 3 && (props.interrupt.sale === 1 || props.interrupt.state === 1) ? "interrupt-on" : "", // 모바일 조작중..
            props.interrupt && props.preferences.interrupt_use && props.interrupt.channel === 4 && (props.interrupt.sale === 1 || props.interrupt.state === 1) ? "interrupt-on" : "" // 터치큐브 조작중..
          )}
          onClick={!props.isNewRoom ? (evt) => props.clickRoom(evt, props.vItem.room_id) : null}
          style={{
            background: props.stayTypeColor[props.roomStateItem.sale],
            boxShadow: !props.edit ? `1px 1px 3px 2px ${props.stayTypeColor[props.roomStateItem.sale]}` : "",
          }}
        >
          <dt
            className={cx("room-title", props.roomStateItem.sale > 0 && props.roomSaleItem && props.roomSaleItem.check_out && "checkout")}
            // 공실 - room-txtcolor0,
            // 숙박 - room-txtcolor1,
            // 대실 - room-txtcolor2,
            // 장기 - room-txtcolor2,
            style={{ background: props.stayTypeColor[props.roomStateItem.sale] }}
          >
            {!props.edit && (
              <div className="room-sign">
                <div
                  data-for={"roomSubTip" + props.vItem.room_id}
                  data-type={props.roomStateItem.air_power_type === 0 ? "dark" : "error"}
                  data-place="left"
                  data-effect="solid"
                  data-tip={props.roomStateItem.air_power_type === 0 ? "예약 냉방 OFF" : "예약 냉방 ON"}
                  className={cx("room-air", props.roomStateItem.air_power_type === 0 ? "" : "on")}
                >
                  {props.roomStateItem.air_power_type === 0 ? null : <AcUnit />}
                </div>
                <ul>
                  {/* 0:없음 1: 배차요청 2:배차완료, 3:배차취소 */}
                  {props.preferences.use_car_call === 1 && (
                    <li
                      data-for={"roomTip" + props.vItem.room_id}
                      data-type={props.roomStateItem.car_call === 1 ? "info" : props.roomStateItem.car_call === 2 ? "success" : "error"}
                      data-effect="float"
                      data-tip={
                        props.roomStateItem.car_call === 1
                          ? `<font color=blue>배차 요청</font> ${props.roomSaleItem && props.roomSaleItem.car_no ? props.roomSaleItem.car_no : ""}`
                          : props.roomStateItem.car_call === 2
                          ? `배차 완료`
                          : ""
                      }
                      className={cx("car", props.roomStateItem.car_call === 0 || props.roomStateItem.car_call === 3 ? "disabled" : "")}
                    >
                      <Button className={cx(props.roomStateItem.car_call === 1 ? "on_call" : "off_call")} onClick={(evt) => props.showCarCall(props.roomStateItem)}>
                        <DriveEtaIcon />
                      </Button>
                    </li>
                  )}
                  {/* 비상호출 0:없음, 1:발생, 2:확인완료 */}
                  {props.preferences.use_emerg === 1 && (
                    <li
                      data-for={"roomTip" + props.vItem.room_id}
                      data-type={props.roomStateItem.emerg === 1 ? "warning" : props.roomStateItem.emerg === 2 ? "success" : "info"}
                      data-effect="float"
                      data-tip={
                        props.roomStateItem.sale && props.roomStateItem.emerg
                          ? props.roomStateItem.emerg === 1
                            ? `<font color=red>비상호출</font>`
                            : props.roomStateItem.emerg === 2
                            ? `비상호출 완료`
                            : ""
                          : ""
                      }
                      className={cx("emerg", props.roomStateItem.sale === 0 || props.roomStateItem.emerg === 0 ? "disabled" : "")}
                    >
                      <Button className={cx(props.roomStateItem.emerg === 1 ? "on_call" : "off_call")} onClick={(evt) => props.showEmergCall(props.roomStateItem)}>
                        <AccessibleIcon />
                      </Button>
                    </li>
                  )}
                  {/* 자판기 판매 여부 */}
                  <li
                    data-for={"roomTip" + props.vItem.room_id}
                    data-type="info"
                    data-effect="float"
                    data-tip={
                      props.roomSaleItem
                        ? `무인 판매 <br/>기본요금 ${String(props.roomSaleItem.default_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}<br/>추가요금 ${String(props.roomSaleItem.add_fee).replace(
                            /\B(?=(\d{3})+(?!\d))/g,
                            ","
                          )}`
                        : ""
                    }
                    className={cx("machine", props.roomSaleItem && props.roomSaleItem.channel === "isc" ? "on_sign" : "off_sign")}
                  >
                    무인
                  </li>
                </ul>
              </div>
            )}
            {!props.edit && (
              <div className={cx("key-wrap")}>
                <div
                  className={cx("name", props.timeOver && "over", props.bounce && "bounce", !props.roomStateItem.sale && props.reservs.length ? "reserv" : "")}
                  style={{ color: props.preferences.room_name_color[!props.timeOver ? 1 : 2] }}
                >
                  <span
                    data-for={"roomTip" + props.vItem.room_id}
                    data-type="info"
                    data-effect="float"
                    data-tip={
                      props.reservs.length
                        ? props.reservs
                            .map((v, k) => {
                              return (
                                "예약 [" +
                                keyToValue("room_sale", "stay_type", v.stay_type) +
                                "] " +
                                moment(v.check_in_exp).format("MM/DD HH:mm") +
                                " ~ " +
                                moment(v.check_out_exp).format("MM/DD HH:mm") +
                                " [" +
                                (v.room_id ? v.room_name : "자동배정") +
                                "] (" +
                                String(v.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-") +
                                ") " +
                                (v.name || "미등록고객")
                              );
                            })
                            .join("<br/>")
                        : ""
                    }
                    className={cx(
                      (!props.roomSaleItem || !props.roomSaleItem.room_reserv_id) && !props.reservs.length
                        ? "hide"
                        : props.roomSaleItem && props.roomSaleItem.room_reserv_id
                        ? "on_reserv in"
                        : "on_reserv"
                    )}
                  >
                    예{remainRserv.length ? remainRserv.length : ""}
                  </span>
                  <span
                    data-for={"roomTip" + props.vItem.room_id}
                    data-type="info"
                    data-effect="float"
                    data-tip={
                      props.timeOptionItem.id ? `[${props.timeOptionItem.name}] ${props.timeOptionItem.add_fee > 0 ? "추가 요금" : "할인 요금"} : ${format.toMoney(props.timeOptionItem.add_fee)}` : ""
                    }
                    className={cx("on_time_option", props.timeOptionItem.id ? "" : "hide")}
                  >
                    옵
                  </span>

                  {/* ----- 객실명 ----- */}
                  <div
                    className={cx("room-name animate")}
                    data-for={"roomTip" + props.vItem.room_id}
                    data-type={!props.timeOver ? "info" : "dark"}
                    data-effect="solid"
                    data-place="top"
                    data-tip={
                      !props.roomSaleItem && props.reservs.length
                        ? props.reservs
                            .map((v, k) => {
                              return (
                                "예약 [" +
                                keyToValue("room_sale", "stay_type", v.stay_type) +
                                "] " +
                                moment(v.check_in_exp).format("MM/DD HH:mm") +
                                " ~ " +
                                moment(v.check_out_exp).format("MM/DD HH:mm") +
                                " [" +
                                (v.room_id ? v.room_name : "자동배정") +
                                "] (" +
                                String(v.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-") +
                                ") " +
                                (v.name || "미등록고객")
                              );
                            })
                            .join("<br/>")
                        : (props.preferences.show_type_name < 2 ? props.roomItem.type_name : "") +
                          " " +
                          props.roomItem.name +
                          " " +
                          (props.preferences.show_type_name === 2 ? props.roomItem.type_name : "") +
                          " [" +
                          keyToValue("room", "state", stateCode) +
                          (props.roomStateItem.sale ? " / " + keyToValue("room_state", "sale", props.roomStateItem.sale) : "") +
                          "]" +
                          (props.roomStateItem.sale > 0 && props.roomSaleItem
                            ? (props.roomSaleItem && props.roomSaleItem.channel === "isc" ? " [무인 판매]" : "") +
                              "<br/>기본요금 <span class='money'>" +
                              String(props.roomSaleItem.default_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>추가요금 <span class='money'>" +
                              String(props.roomSaleItem.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>OTA결제 <span class='money'>" +
                              String(props.roomSaleItem.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>현금결제 <span class='money'>" +
                              String(props.roomSaleItem.pay_cash_amt + props.roomSaleItem.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>카드결제 <span class='money'>" +
                              String(props.roomSaleItem.pay_card_amt + props.roomSaleItem.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>결제금액 <span class='money'>" +
                              String(
                                props.roomSaleItem.pay_cash_amt +
                                  props.roomSaleItem.pay_card_amt +
                                  props.roomSaleItem.prepay_ota_amt +
                                  props.roomSaleItem.prepay_cash_amt +
                                  props.roomSaleItem.prepay_card_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>미수금액 <span class='money " +
                              (props.repAmt ? "red" : "") +
                              "'>" +
                              String(props.repAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                              "</span><br/>입실 <span class='date'>" +
                              moment(props.roomSaleItem.check_in).format("YYYY-MM-DD HH:mm") +
                              "</span><br/>" +
                              (props.roomSaleItem.check_out ? "퇴실 <span class='date yellowCCC'>" : "퇴실예정 <span class='date'>") +
                              moment(props.roomSaleItem.check_out || props.roomSaleItem.check_out_exp).format("YYYY-MM-DD HH:mm") +
                              (props.timeOver ? "</span><br/>초과시간 <span class='date red'>" + Number(props.overTime.hh) + " : " + props.overTime.mm + "</span>" : "")
                            : "")
                    }
                  >
                    {props.preferences.show_type_name === 1 && props.roomItem.type_name
                      ? (props.roomItem.type_name.length > 8 ? props.roomItem.type_name.substring(0, 8) : props.roomItem.type_name) + " "
                      : ""}
                    {props.roomItem.name && props.roomItem.name.length > 8 ? props.roomItem.name.substring(0, 8) : props.roomItem.name}
                    {props.preferences.show_type_name === 2
                      ? " " + (props.roomItem.type_name && props.roomItem.type_name.length > 8 ? props.roomItem.type_name.substring(0, 8) : props.roomItem.type_name)
                      : ""}
                  </div>
                </div>
                <div
                  data-for={"roomSubTip" + props.vItem.room_id}
                  data-type="dark"
                  data-place="right"
                  data-effect="solid"
                  data-tip={keyToValue("room_state", "door", props.roomStateItem.door) + " / " + keyToValue("room_state", "key", props.roomStateItem.key)}
                  className={cx(
                    "key",
                    // (0:없음, 1:고객키,2:마스터키 3:청소키, 4:고객키 제거, 5:마스터키 제거, 6:청소키 제거)
                    props.roomStateItem.key === 1 ? "mdi mdi-key" : props.roomStateItem.key === 2 ? "mdi mdi-eye" : props.roomStateItem.key === 3 ? "mdi mdi-broom" : ""
                  )}
                  style={{
                    color: props.roomStateItem.key ? props.preferences.key_type_color[props.roomStateItem.key] : null,
                  }}
                />
              </div>
            )}
            {props.edit && (
              <div>
                <DragIndicatorIcon className={cx("drag")} color="action" onMouseDown={(evt) => props.onMouseDown(evt, props.vIdx)} />

                {!props.isNewRoom && (
                  <FormControl componentClass="select" placeholder="객실명" name="room_id" value={props.vItem.room_id} onChange={(evt) => props.handleInputChange(evt)}>
                    {props.difItemList.map((v, k) => (
                      <option key={k} value={v.id}>
                        {v.name}
                      </option>
                    ))}
                    {props.vItem.mode === "new" && <option value="">객실 등록</option>}
                  </FormControl>
                )}
                {props.isNewRoom && <FormControl type="text" name="name" placeholder="객실명" onChange={(evt) => props.handleInputChange(evt)} />}
                <DeleteForeverIcon className={cx("remove")} color="action" onClick={(evt) => props.onRemoveItem(props.vIdx)} />
              </div>
            )}
          </dt>
          <dd>
            <div className="room-mode">
              <div
                data-for={"roomSubTip" + props.vItem.room_id}
                data-type={props.roomStateItem.signal === 1 ? "dark" : "success"}
                data-place="left"
                data-effect="solid"
                data-tip={props.roomStateItem.signal === 1 ? "통신 OFF" : "통신 ON"}
                className={cx("signal", props.roomStateItem.signal === 0 ? "on" : "off")}
              >
                {props.roomStateItem.signal === 0 ? <WifiOnIcon /> : <WifiOffIcon />}
              </div>
              {props.idm.maker && props.idm.maker.toLowerCase() === "icrew" ? (
                <div
                  data-for={"roomSubTip" + props.vItem.room_id}
                  data-type={props.roomStateItem.main_relay === 0 ? "dark" : "success"}
                  data-place="left"
                  data-effect="solid"
                  data-tip={props.roomStateItem.main_relay === 0 ? "메인전원 OFF" : "메인전원 ON"}
                  className={cx("power", props.roomStateItem.main_relay === 0 ? "" : "on")}
                >
                  {props.roomStateItem.main_relay === 0 ? <PowerOffIcon /> : <PowerOnIcon />}
                </div>
              ) : (
                <div
                  data-for={"roomSubTip" + props.vItem.room_id}
                  data-type={props.roomStateItem.main_relay === 0 || props.roomStateItem.key === 0 || props.roomStateItem.key > 3 ? "dark" : "success"}
                  data-place="left"
                  data-effect="solid"
                  data-tip={props.roomStateItem.main_relay === 0 || props.roomStateItem.key === 0 || props.roomStateItem.key > 3 ? "메인전원 OFF" : "메인전원 ON"}
                  className={cx("power", props.roomStateItem.main_relay === 0 || props.roomStateItem.key === 0 || props.roomStateItem.key > 3 ? "" : "on")}
                >
                  {props.roomStateItem.main_relay === 0 || props.roomStateItem.key === 0 || props.roomStateItem.key > 3 ? <PowerOffIcon /> : <PowerOnIcon />}
                </div>
              )}
              <div
                data-for={"roomSubTip" + props.vItem.room_id}
                data-type={props.roomStateItem.air_power === 0 ? "dark" : "success"}
                data-place="left"
                data-effect="solid"
                data-tip={props.roomStateItem.air_power === 0 ? "에어컨 OFF" : "에어컨 ON"}
                className={cx("air", props.roomStateItem.air_power === 1 && "on")}
              >
                <ToysIcon />
              </div>
            </div>
            <div className="room-info">
              <div
                data-for={"roomSubTip" + props.vItem.room_id}
                data-type={"success"}
                data-place="bottom"
                data-effect="solid"
                data-tip={
                  (props.roomSaleItem && props.roomSaleItem.alarm === 1
                    ? `- 알람 - <pre>${
                        props.roomSaleItem.alarm_hour +
                        ":" +
                        props.roomSaleItem.alarm_min +
                        " " +
                        props.roomSaleItem.alarm_term +
                        "분간격 " +
                        props.roomSaleItem.alarm_repeat +
                        "회반복" +
                        (props.roomSaleItem.alarm_memo_play ? "<br/>알람 시 메모 재생" : "")
                      }</pre>`
                    : "") + (props.roomSaleItem && props.roomSaleItem.memo ? `- 메모 - <pre>${props.roomSaleItem.memo}</pre>` : "")
                }
                className="room-status" // 청소대기, 청소 중은 배경 흐리게.
                style={{
                  backgroundColor: stateCode !== 3 && stateCode !== 4 ? props.stayTypeColor[props.roomStateItem.sale] : `${props.stayTypeColor[props.roomStateItem.sale]}5d`,
                }}
              >
                {roomTheme && <img src={roomTheme} alt={props.room.theme} />}
                <span className="door-close" />
                <span className="door-open" />
                <span className="men" />
                <span className="clean-ing" />
                <span className="clean-ready" />
                <span className="clean-end" />
                <span className="inspect-ready" />
                <span className="inspect-ing" />
                <span className="inspect-end" />
                <span className="notice">
                  {props.roomStateItem.notice && <div style={{ opacity: props.roomStateItem.notice_opacity * 0.1 }} dangerouslySetInnerHTML={{ __html: props.roomStateItem.notice }}></div>}
                </span>
                <span className="alarm" />
                <span className="theft" />
                <span className="security" />
                <span className="checkout" />
                <span className="checkin" />
                <span className="empty" />
                <span className="rent" />
                <span className="stay" />
                <span className="long" />
                <span className="isc-sale-stop" />
                <span className="isc-saling" />
                <span className="out" />
                <div className="dim" />
                <div className="interrupt" />
              </div>
              {props.roomView.mode === 1 && (
                <div
                  className={cx(
                    "room-count",
                    props.saleSum.sum.default_fee + props.saleSum.sum.add_fee > 0 ? "sale" : "",
                    props.saleSum.sum.default_fee +
                      props.saleSum.sum.add_fee -
                      props.saleSum.sum.pay_cash_amt -
                      props.saleSum.sum.pay_card_amt -
                      props.saleSum.sum.prepay_ota_amt -
                      props.saleSum.sum.prepay_cash_amt -
                      props.saleSum.sum.prepay_card_amt >
                      0
                      ? "recept"
                      : ""
                  )}
                >
                  <dl>
                    <dt>대실 : ({props.saleSum.rent.count})</dt>
                    <dd>
                      <span>{String(props.saleSum.rent.pay_cash_amt + props.saleSum.rent.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>숙박 : ({props.saleSum.day.count})</dt>
                    <dd>
                      <span>{String(props.saleSum.day.pay_cash_amt + props.saleSum.day.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>장기 : ({props.saleSum.long.count})</dt>
                    <dd>
                      <span>{String(props.saleSum.long.pay_cash_amt + props.saleSum.long.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl className="space">
                    <dt>매출 : </dt>
                    <dd>
                      <span>{String(props.saleSum.sum.default_fee + props.saleSum.sum.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>현금 : </dt>
                    <dd>
                      <span>{String(props.saleSum.sum.pay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>카드 : </dt>
                    <dd>
                      <span>{String(props.saleSum.sum.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>결제금 : </dt>
                    <dd>
                      <span>{String(props.saleSum.sum.pay_cash_amt + props.saleSum.sum.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt>미수금 : </dt>
                    <dd>
                      <span>
                        {String(
                          props.saleSum.sum.default_fee +
                            props.saleSum.sum.add_fee -
                            props.saleSum.sum.pay_cash_amt -
                            props.saleSum.sum.pay_card_amt -
                            props.saleSum.sum.prepay_ota_amt -
                            props.saleSum.sum.prepay_cash_amt -
                            props.saleSum.sum.prepay_card_amt
                        ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </span>
                    </dd>
                  </dl>
                </div>
              )}
              <div
                data-for={"roomTip" + props.vItem.room_id}
                data-type={!props.timeOver ? "info" : "dark"}
                data-place="bottom"
                data-effect="solid"
                data-tip={
                  props.roomSaleItem
                    ? "입실 <span class='date'>" +
                      moment(props.roomSaleItem.check_in).format("YYYY-MM-DD HH:mm") +
                      "</span><br/>" +
                      (props.roomSaleItem.check_out ? "퇴실 <span class='date yellow'>" : "퇴실예정 <span class='date'>") +
                      moment(props.roomSaleItem.check_out || props.roomSaleItem.check_out_exp).format("YYYY-MM-DD HH:mm") +
                      "</span><br/>이용시간 <span class='date'>" +
                      Number(props.useTime.hh) +
                      " : " +
                      props.useTime.mm +
                      "</span>" +
                      (props.timeOver ? "</span><br/>초과시간 <span class='date red'>" + Number(props.overTime.hh) + " : " + props.overTime.mm + "</span>" : "") +
                      "</span>" +
                      (props.roomStateItem.clean
                        ? "<br/>청소요청 <span class='date'>" +
                          moment(props.roomStateItem.clean_change_time).format("YYYY-MM-DD HH:mm") +
                          "</span><br/>청소진행 <span class='date yellow'>" +
                          Number(props.cleaningTime.hh) +
                          " : " +
                          props.cleaningTime.mm +
                          "</span>"
                        : "")
                    : ""
                }
                className={cx("room-data", props.isCleanTime && props.roomStateItem.clean && props.roomStateItem.clean_change_time ? "clean" : "")}
                onClick={(evt) => props.toggleUseTime(evt)}
                style={{
                  borderTop: `1px solid ${props.stayTypeColor[props.roomStateItem.sale]}`,
                  backgroundColor: stateCode === 0 ? `${props.stayTypeColor[props.roomStateItem.sale]}` : `${props.stayTypeColor[props.roomStateItem.sale]}`,
                }}
              >
                {!props.edit ? (
                  <div
                    style={{
                      backgroundColor:
                        props.isCleanTime && props.roomStateItem.clean && props.roomStateItem.clean_change_time
                          ? `${props.timeBgColor[2]}`
                          : props.roomStateItem.sale > 0
                          ? `${props.timeBgColor[1]}`
                          : null,
                    }}
                  >
                    {props.preferences.show_air_temp === 1 && props.roomStateItem.air_temp < 63 ? (
                      <div
                        data-for="roomSubTip"
                        data-type={props.roomStateItem.air_temp < 63 ? "success" : "dark"}
                        data-place="left"
                        data-effect="solid"
                        data-tip={props.roomStateItem.air_temp < 63 ? `객실온도 ${props.roomStateItem.air_temp} ℃` : "객실온도 OFF"}
                        className="temp"
                      >
                        {props.roomStateItem.air_temp}
                        <span>℃</span>
                      </div>
                    ) : null}
                    <div
                      className={cx(
                        "time",
                        props.isCleanTime && props.roomStateItem.clean && props.roomStateItem.clean_change_time // 청소 중 표시.
                          ? "mdi mdi-broom"
                          : props.roomStateItem.sale > 0 // 사용 중 표시.
                          ? "mdi mdi-account"
                          : props.preferences.show_sale_change_time === 1
                          ? "mdi mdi-clock-fast"
                          : ""
                      )}
                    >
                      <span className="in">
                        {props.isCleanTime && props.roomStateItem.clean && props.roomStateItem.clean_change_time
                          ? moment(props.roomStateItem.clean_change_time).format(Number(props.cleaningTime.hh) >= 24 ? "MM/DD" : "HH:mm")
                          : props.roomStateItem.sale > 0 && props.roomSaleItem
                          ? moment(props.roomSaleItem.check_in).format(Number(props.useTime.hh) >= 24 ? "MM/DD" : "HH:mm")
                          : moment(props.roomStateItem.sale_change_time).format(Number(props.saleTime.hh) >= 24 ? "MM/DD" : "HH:mm")}
                      </span>
                      {props.isCleanTime && props.roomStateItem.clean && props.roomStateItem.clean_change_time ? (
                        <span className={cx("use")}>
                          {Number(props.cleaningTime.hh)}
                          {<font color={props.tickColor}>:</font>}
                          {props.cleaningTime.mm}
                        </span>
                      ) : props.roomStateItem.sale > 0 && props.roomSaleItem ? (
                        <span className={cx("use", props.timeOver && "over", !props.roomSaleItem.check_out && props.checkOutAlert && props.tick && "tick")}>
                          {Number(props.useTime.hh)}
                          {<font color={!props.roomSaleItem || !props.roomSaleItem.check_out ? props.tickColor : "#000"}>:</font>}
                          {props.useTime.mm}
                        </span>
                      ) : props.preferences.show_sale_change_time === 1 ? (
                        <span className={cx("empty")}>
                          {Number(props.saleTime.hh)}
                          {<font color={props.tickColor}>:</font>}
                          {props.saleTime.mm}
                        </span>
                      ) : null}
                    </div>
                  </div>
                ) : (
                  <div style={{ textAlign: "center" }}>
                    <FormControl componentClass="select" placeholder="객실타입" name="room_type_id" value={props.vItem.room_type_id || ""} onChange={(evt) => props.handleInputChange(evt)}>
                      {props.roomType.list.map((v, k) => (
                        <option key={k} value={v.id}>
                          {v.name}
                        </option>
                      ))}
                    </FormControl>
                  </div>
                )}
              </div>
            </div>
          </dd>
        </dl>
      </span>
    </span>
  );
};

export default Room;
