// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

import date from "utils/date-util";
import format from "utils/format-util";

import { keyToValue } from "constants/key-map";

import swal from "sweetalert";
import Swal from "sweetalert2";

// UI for notification
//import CustomizedStackSnackbar from "components/Notification/CustomizedStackSnackbar";

// Components.
import Room from "./presenter";

class Container extends Component {
  // un mounted check
  _isMounted = false;

  static propTypes = {
    layout: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    roomInterrupt: PropTypes.object.isRequired,
    view: PropTypes.object.isRequired,
    vItem: PropTypes.object.isRequired,
    viewItems: PropTypes.array.isRequired,
    roomView: PropTypes.object.isRequired,
    roomViewItem: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    roomTheme: PropTypes.object.isRequired,
    roomReserv: PropTypes.object.isRequired,
    timeOption: PropTypes.object.isRequired,
    onChaneItem: PropTypes.func.isRequired,
    device: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      loading: true,
      mode: "default",
      difItemList: [],
      isNewRoom: false,
      stayTypeColor: { 0: "#c8c8c8", 1: "#ffab3e", 2: "#52d3ee", 3: "#ff727a", 4: "#a1a1a1" }, // 숙박 색상 (초기화는 preferences reducer 에서함)
      keyTypeColor: {
        1: "#5c56af",
        2: "#548b8d",
        3: "#bb3e3eda",
      }, // 키 색상 (초기화는 preferences reducer 에서함)
      timeBgColor: {
        1: "#f3f3f394",
        2: "#ff8484a8",
      }, // 시간 배경색상 (초기화는 preferences reducer 에서함)
      tooltip: "",
      interrupt: undefined,
      idm: {}, // 데몬
      isc: [], // 자판기
      roomItem: {},
      roomStateItem: {},
      roomSaleItem: {},
      saleSum: {
        day: 0,
        rent: 0,
        long: 0,
        rooms: 0,
        total: 0,
      },
      repAmt: 0,
      reservs: [], // 예약 목록
      timeOptionItem: {}, // 옵션 정보
      useTimer: undefined,
      useTime: { hh: "00", mm: "00", ss: "00" },
      saleTime: { hh: "00", mm: "00", ss: "00" },
      overTime: { hh: "00", mm: "00", ss: "00" }, // 초과 시간.
      timeOver: false,
      checkOutAlert: false, // 사용 시간 만료 알림.
      cleaningTime: { hh: "00", mm: "00" }, // 청소 시간.
      isCleanTime: false, // 청소 시간 보기.
      isAlarmDelay: false,
      isAlertCarCall: false, // 차량 호출 여부. 화면 로딩 시 중지.(로딩 후 이벤트 받음)
      isAlertEmergCall: false, // 비상 호출 여부.
      isAlertTheftCall: false, // 도난 알림 여부.
      isAlertCallDelay: false, // 호출 지연 시간 내인지 여부.
      tickColor: "#ddd",
      tick: false,
    };
  }

  clickRoom = (evt, roomId) => {
    if (!this.props.edit) this.props.handleRoomSelected(roomId);
  };

  // 객실 정보 변경 이벤트.
  handleInputChange = (evt) => {
    const target = evt.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    let { vItem, room } = this.props;
    let { roomItem, isNewRoom } = this.state;

    // 초기 룸정보 없을때..
    if (!roomItem) roomItem = _.find(room.list, { id: vItem.room_id });

    // 뷰 아이템 정보 변경.
    vItem[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleInputChange name", name, "value", value);

    if (!isNewRoom && name === "room_id" && !value) isNewRoom = true;

    console.log("-- vItem", vItem, roomItem);

    // 객실 정보 변경 시.
    if (name === "room_id" && value !== "") {
      const { id, view_id, room_id, row, col } = vItem;

      console.log("-- vItem", { id, view_id, room_id, row, col });

      // 뷰 화면 변경 여부.
      this.props.onChaneItem(true, () => {
        // 기존 객실 변경 사항 업데이트
        this.props.handleSaveRoomViewItem(id, {
          view_id,
          room_id,
          row,
          col,
        });
      });
    }
    // 객실 타입 변경
    else if (name === "room_type_id" && vItem.room_id) {
      const { id } = roomItem;

      // 뷰 화면 변경 여부.
      this.props.onChaneItem(true, () => {
        // 객실 변경
        this.props.handleSaveRoom(id, {
          id,
          room_type_id: value,
        });
      });

      roomItem[name] = value; // 객실 기본 정보 업데이트.
    }

    console.log("- vItem", vItem, "isNewRoom", isNewRoom);

    this._isMounted &&
      this.setState({
        roomItem,
        isNewRoom,
      });
  };

  handleToolTip = (tooltip) => {
    console.log("- handleToolTip", tooltip);

    this._isMounted &&
      this.setState({
        tooltip,
      });
  };

  // 객실 변경시 정보 재설정.
  roomReset = (props) => {
    const {
      view: { all_room, room_order },
      vItem,
      viewItems,
      roomViewItem,
      room,
    } = props;

    const roomItem = _.find(room.list, { id: vItem.room_id });

    //console.log("- roomReset ", vItem, vItem.room_id, roomItem);

    const viewItemsList = _.map(viewItems, (v) => v.item).filter((v) => v && v.mode);

    // 변경 가능한 객실 목록.
    let difItemList = _.filter(room.list, (v) => !_.find(viewItemsList, { room_id: v.id }) || v.id === vItem.room_id);

    // console.log("- difItemList ", difItemList);

    if (difItemList.length) {
      // 객실추가순서 (1:객실명순서, 2:객실명역순, 3:객실타입순서, 4:객실타입역순)
      difItemList = _.orderBy(difItemList, [room_order === 1 || room_order === 2 ? "name" : "room_type_name", room_order === 3 || room_order === 4 ? "room_type_name" : "name"]);

      if (room_order === 2 || room_order === 4) difItemList = difItemList.reverse();

      // 객실 등록 시 전체 객실 여부(0: 나머지 객실, 1:전체객실)
      if (!vItem.room_id && all_room === 0) {
        const allViewItems = _.uniqBy(roomViewItem.list, "room_id");
        difItemList = _.filter(difItemList, (v) => !_.find(allViewItems, { room_id: v.id }));
      }
    }

    //console.log("- difItemList  ", difItemList);

    // 신규 객실 이면서 변경 가능한 객실 이라면.
    if (!vItem.room_id && difItemList.length) {
      vItem.room_id = difItemList[0].id;
      vItem.name = difItemList[0].name;
      vItem.room_type_id = difItemList[0].room_type_id;
      vItem.room_type_name = difItemList[0].room_type_name;
    }

    //console.log("- roomReset difItemList", difItemList.length);

    this._isMounted &&
      this._isMounted &&
      this.setState({
        roomItem,
        difItemList,
        isNewRoom: !this.state.isNewRoom ? difItemList.length < 1 : true,
      });
  };

  showCarCall = (roomStateItem) => {
    let { roomItem, roomSaleItem, isAlertCarCall } = this.state;
    const {
      preferences: { call_alert_term },
    } = this.props;

    let { room_id, sale, car_call } = roomStateItem;

    console.log("- showCarCall", roomItem.name, { isAlertCarCall }, roomStateItem);

    // 호출 지연 시간이 안지났다면 알림 보이지 않는다.
    if (isAlertCarCall) return;

    let title = keyToValue("room_state", "car_call", car_call || 1);

    var elem = document.createElement("div");
    elem.innerHTML = `<font size="5">${roomItem.type_name} ${roomItem.name} (${
      roomSaleItem && roomSaleItem.car_no ? "<font color=blue>" + roomSaleItem.car_no + "</font>" : "<font color=gray>차량 번호 없음</font>"
    }) <font color=${car_call === 1 ? "red" : "green"}><br/>${title}</font> ${car_call === 1 ? "입니다" : "되었습니다"}</font>`;

    swal({
      icon: car_call !== 2 ? "info" : "success",
      title: "차량 호출 알림",
      content: elem,
      buttons: {
        ready: {
          text: "배차대기",
          value: 1,
          className: "blue-btn",
        },
        done: {
          text: "배차완료",
          value: 2,
          className: "green-btn",
        },
        cancle: {
          text: "배차취소",
          value: 3,
          className: "red-btn",
        },
        default: {
          text: "확인",
          value: -1,
          className: "btn-set-confirm",
        },
      },
      closeOnClickOutside: true,
    }).then((value) => {
      if (value > -1) {
        // 0:배차 취소, 1:배차 요청	, 2: 배차 완료
        const room_state = { car_call: value };

        setTimeout(() => {
          this.props.handleSaveRoomState(room_id, {
            room_state,
          });
        }, 100);
      }
    });

    // 호출 지연 시간 설정 시
    console.log("- isDelay", call_alert_term);

    setTimeout(() => {
      this.setState({
        isAlertCarCall: false,
      });
    }, call_alert_term * 1000 * 60);
  };

  showEmergCall = (roomStateItem) => {
    let { roomItem, isAlertEmergCall } = this.state;
    const {
      preferences: { call_alert_term },
    } = this.props;

    console.log("- showEmergCall", { isAlertEmergCall });

    // 호출 지연 시간이 안지났다면 알림 보이지 않는다.
    if (isAlertEmergCall) return;

    Swal.fire({
      icon: roomStateItem.emerg === 1 ? "info" : "success",
      title: roomStateItem.emerg === 1 ? "비상호출 확인" : "비상호출 완료",
      html: roomItem.name + (roomStateItem.emerg === 1 ? " 에서 비상호출을 했습니다." : " 비상호출 확인완료 했습니다.") + "<br/>확인해 주세요.",
      showCancelButton: true,
      confirmButtonText: roomStateItem.emerg === 1 ? "호출확인" : "호출대기 전환",
      cancelButtonText: roomStateItem.emerg === 1 ? "호출대기" : "확인",
      timer: 60000,
    }).then((result) => {
      if (result.value) {
        // 0:없음, 1:발생, 2:확인완료
        const room_state = { emerg: roomStateItem.emerg === 1 ? 2 : 1 };

        setTimeout(() => {
          this.props.handleSaveRoomState(roomStateItem.room_id, {
            room_state,
          });
        }, 100);

        // 호출 지연 시간 설정 시
        let isDelay = call_alert_term > 0 ? true : false;

        console.log("- isDelay", isDelay, call_alert_term);

        if (isDelay) {
          setTimeout(() => {
            this.setState({
              isAlertEmergCall: false,
            });
          }, call_alert_term * 1000 * 60);
        } else {
          this.setState({
            isAlertEmergCall: false,
          });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.setState({
          isAlertEmergCall: false,
        });
      }
    });

    this.setState({
      isAlertEmergCall: true, // 팝업 알림 여부.
    });
  };

  timer = () => {
    let { roomItem, roomSaleItem, roomStateItem, tick, bounce, isAlarmDelay, useTime, saleTime, isAlertTheftCall } = this.state;

    const {
      preferences: { check_out_alarm_time },
    } = this.props;

    // console.log("- tiemr");

    if (roomSaleItem && roomSaleItem.state === "A" && roomStateItem.sale > 0) {
      let checkOutAlert = moment(roomSaleItem.check_out_exp || new Date()).add(-check_out_alarm_time, "minute") < moment();

      // 서버의 체크인 시간이 PC 의 현재 시간보다 크다면 현재 PC 시간으로 설정.
      let check_in = moment(roomSaleItem.check_in) < moment() ? roomSaleItem.check_in : new Date();

      // 이용 시간.
      useTime = date.diff(check_in, roomSaleItem.check_out || new Date());

      // 초과 여부.
      let timeOver = moment(roomSaleItem.check_out_exp || new Date()) < moment(roomSaleItem.check_out || new Date());

      // 초과 시간.
      let overTime = timeOver ? date.diff(roomSaleItem.check_out_exp || new Date(), roomSaleItem.check_out || new Date()) : { hh: "00", mm: "00", ss: "00" };

      // 사용 시간 지나면 박스 테두리 깜빡임 표시.
      if (timeOver) checkOutAlert = true;

      // 매출 변경 경과 시간.
      saleTime = date.diff(roomStateItem.sale_change_time, new Date());

      // console.log("- tiemr", { timeOver, useTime, saleTime, overTime, checkOutAlert });

      const { id, alarm, alarm_hour, alarm_min, alarm_term, alarm_repeat, alarm_memo_play, memo } = roomSaleItem;

      // if (alarm) console.log("- alarm", alarm, isAlarmDelay);

      if (alarm && !isAlarmDelay) {
        let alarmStartTime = moment({ hour: alarm_hour, minute: alarm_min, second: 3 }); // 정시에  reload Preferences PlaceLock 하므로 딜레이 준다.

        let alarmEndTime = moment({ hour: alarm_hour, minute: alarm_min, second: 3 }).add(alarm_term * alarm_repeat, "minute"); // 반복 간격 * 반복 횟수 만큼 시간 연장 한다.

        let currTime = moment();

        // 알람 가능 시간..
        if (alarmStartTime <= currTime && currTime < alarmEndTime) {
          isAlarmDelay = true;

          console.log("- alarm time ", currTime.format("YYYYMMDD HHmmss"), alarmStartTime.format("YYYYMMDD HHmmss"), alarmEndTime.format("YYYYMMDD HHmmss"));

          console.log("- alarm_memo_play", alarm_memo_play, alarm_term * 60 * 1000);

          // 한번만 재생
          if (alarm_memo_play) {
            console.log("- alarm speech ", memo);
            this.props.getVoice(memo); // 음성 지원.
          }

          // 알람 팝업.
          Swal.fire({
            icon: "info",
            title: `[${roomItem.name}] 객실 알람`,
            html: "알람 시간 입니다. <br/>알람을 중단 하시겠습니까?",
            showCancelButton: true,
            confirmButtonText: "예",
            cancelButtonText: "아니오",
          }).then((result) => {
            if (result.value) {
              const { roomSaleItem } = this.state;

              // 알람 중단.
              const { id } = roomSaleItem;
              this.props.handleSaveRoomSale(id, { room_sale: { alarm: 0 } });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
          });

          setTimeout(() => {
            this.setState({
              isAlarmDelay: false,
            });
          }, alarm_term * 60 * 1000);

          this.setState({
            isAlarmDelay,
          });
        } else if (currTime > alarmEndTime) {
          isAlarmDelay = true;

          this.setState({
            isAlarmDelay,
          });

          if (id) this.props.handleSaveRoomSale(id, { room_sale: { alarm: 0 } });
        }
      }

      this._isMounted &&
        this.setState({
          useTime,
          saleTime,
          overTime,
          checkOutAlert,
          tickColor: tick ? "#ddd" : "#000",
          bounce: !roomSaleItem.check_out ? (timeOver && Number(useTime.ss) % 5 === 0 ? !bounce : bounce) : false,
          timeOver,
          tick: !tick,
        });
    } else {
      // 공실 경과 시간.
      saleTime = date.diff(roomStateItem.sale_change_time, new Date());

      this._isMounted &&
        this.setState({
          useTime: { hh: "00", mm: "00" },
          saleTime,
          overTime: { hh: "00", mm: "00" },
          tickColor: tick ? "#ddd" : "#000",
          bounce: false,
          timeOver: false,
          checkOutAlert: false,
          tick: !tick,
        });
    }

    // 청소 요청/중 경과 시간.
    if (roomStateItem.clean && roomStateItem.clean_change_time) {
      let cleaningTime = date.diff(roomStateItem.clean_change_time, new Date());

      this._isMounted &&
        this.setState({
          cleaningTime,
          tickColor: tick ? "#ddd" : "#000",
          tick: !tick,
        });
    }

    // 도난 알림.
    // 도난 알림 팝업.
    if (!isAlertTheftCall && roomStateItem.theft === 1) {
      this.setState(
        {
          isAlertTheftCall: true,
        },
        () => {
          Swal.fire({
            icon: "warning",
            title: `[${roomItem.name}] 도난 발생!`,
            html: "객실 상태를 확인해 주세요!",
            showCancelButton: false,
            confirmButtonText: "확인",
          }).then((result) => {
            if (result.value) {
              setTimeout(() => {
                this.setState({
                  isAlertTheftCall: false,
                });
              }, 10 * 60 * 1000);
            }
          });
        }
      );
    }
  };

  setEnableReserv = (roomReserv) => {
    let { roomSaleItem } = this.state;
    const {
      vItem,
      preferences: { reserv_notice_day = 1 },
    } = this.props;
    let { list } = roomReserv;

    let reservs = _.filter(list, (item) => {
      let { id, room_id, state, check_in_exp, check_out_exp } = item;

      let isEnable = false;

      if (room_id === vItem.room_id) {
        // 1일 이내에 퇴실 예정 일시가 남아 있는것만 유효
        isEnable = state === "A" && moment(check_in_exp).isBefore(moment().add(reserv_notice_day, "day")) && moment(check_out_exp).isAfter(moment());

        // console.log("- setEnableReserv reservs room", item.room_name, reserv_notice_day, isEnable);

        if (roomSaleItem) {
          // 현제 체크인이 예약 매출 이면 추가.
          if (roomSaleItem.room_reserv_id === id && roomSaleItem.state === "A" && state === "C") {
            isEnable = true;
          }
        }
      }

      return isEnable;
    });

    reservs = _.orderBy(reservs, ["check_in_exp"], ["asc"]);

    if (reservs.length) console.log("- Room setEnableReserv reservs", vItem.name, reservs);

    this._isMounted &&
      this._isMounted &&
      this.setState({
        reservs,
      });
  };

  toggleUseTime = (evt) => {
    const { roomStateItem, isCleanTime } = this.state;

    // console.log("-- toggleUseTime", isCleanTime);

    if (roomStateItem.clean) {
      this._isMounted &&
        this.setState({
          isCleanTime: !isCleanTime,
        });
    }
  };

  componentDidMount = () => {
    this._isMounted = true;

    const { room, vItem, roomInterrupt } = this.props;

    const roomItem = _.find(room.list, { id: vItem.room_id }) || {};
    let interrupt = _.find(roomInterrupt.list, { room_id: vItem.room_id });

    if (roomItem.name && interrupt) {
      console.info("-- Room componentDidMount ", roomItem.name, {
        interrupt,
      });
    }

    let { idm, isc } = this.props.device;

    // idm = { maker: "sindo" };

    // console.info("-- Room componentDidMount ", { idm, isc });

    this._isMounted &&
      this.setState(
        {
          interrupt,
          idm,
          isc,
          roomItem,
        },
        () => {
          this.roomReset(this.props);
        }
      );

    if (this.state.useTimer) clearInterval(this.state.useTimer);

    const { stay_type_color, key_type_color, time_bg_color, room_name_color } = this.props.preferences;

    // store intervalId in the state so it can be accessed later:
    this._isMounted &&
      this.setState({
        stayTypeColor: stay_type_color,
        keyTypeColor: key_type_color,
        roomNmColor: room_name_color,
        timeBgColor: time_bg_color,
        useTimer: setInterval(this.timer, 1000),
      });
  };

  componentWillUnmount() {
    if (this.state.useTimer) clearInterval(this.state.useTimer);

    this._isMounted = false;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { preferences, vItem } = this.props;
    const { roomItem } = this.state;

    // 화면 객실 위치 이동 업데이트
    if (nextProps.vItem !== this.props.vItem) {
      if (JSON.stringify(nextProps.vItem) !== JSON.stringify(this.props.vItem)) {
        // console.info("-- Main > Contents > RoomView > Room 화면 객실 위치 이동 변경.");

        this.roomReset(nextProps);
      }
    }

    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      const { stay_type_color, key_type_color, time_bg_color, room_name_color, reserv_notice_day } = nextProps.preferences;

      if (
        JSON.stringify(this.state.stayTypeColor) !== JSON.stringify(stay_type_color) ||
        JSON.stringify(this.state.keyTypeColor) !== JSON.stringify(key_type_color) ||
        JSON.stringify(this.state.timeBgColor) !== JSON.stringify(time_bg_color) ||
        JSON.stringify(this.state.roomNmColor) !== JSON.stringify(room_name_color)
      ) {
        // console.log("- componentWillReceiveProps stay_type_color", stay_type_color);

        this._isMounted &&
          this.setState({
            stayTypeColor: stay_type_color,
            keyTypeColor: key_type_color,
            roomNmColor: room_name_color,
            timeBgColor: time_bg_color,
          });
      }

      console.log("- componentWillReceiveProps reserv_notice_day", reserv_notice_day);

      if (this.state.reserv_notice_day !== reserv_notice_day) {
        this.setEnableReserv(this.props.roomReserv);
      }
    }

    // 객실 인터럽트 여부 표시.
    if (preferences.interrupt_use && nextProps.roomInterrupt && nextProps.roomInterrupt !== this.props.roomInterrupt) {
      let interrupt = _.find(nextProps.roomInterrupt.list, {
        room_id: vItem.room_id,
      });

      if (interrupt) console.log("- Room interrupt", vItem.room_id, interrupt);

      this._isMounted &&
        this.setState({
          interrupt,
        });
    }

    // 화면 객실 정보 변경.
    if (nextProps.viewItems && nextProps.viewItems !== this.props.viewItems) {
      //console.info("-- Main > Contents > RoomView > Room 화면 객실 정보 변경.");

      this.roomReset(nextProps);
    }

    // 화면 객실 위치 이동 업데이트
    if (nextProps.vItem !== this.props.vItem) {
      if (JSON.stringify(nextProps.vItem) !== JSON.stringify(this.props.vItem)) {
        //console.info("-- Main > Contents > RoomView > Room 화면 객실 위치 이동 변경.");

        this.roomReset(nextProps);
      }
    }

    // 객실 상태 변경.
    if (nextProps.roomState && nextProps.roomState !== this.props.roomState) {
      const { list, item } = nextProps.roomState;

      let { isAlertCarCall, isAlertEmergCall } = this.state;

      let roomStateItem = _.find(list, {
        room_id: Number(this.props.vItem.room_id),
      });

      if (roomStateItem && JSON.stringify(roomStateItem) !== JSON.stringify(this.state.roomStateItem)) {
        // console.info("-- Main > Contents > RoomView > Room > roomState 객실 상태 변경.", roomStateItem);

        if (this.state.roomStateItem.car_call !== roomStateItem.car_call || this.state.roomStateItem.emerg !== roomStateItem.emerg) {
          Swal.clickCancel();
        }

        // 차량호출 팝업.
        if (
          !isAlertCarCall &&
          this.props.preferences.use_car_call === 1 && // 차량호출이 오류로 발생시 끄는 옵션 기능
          this.state.roomStateItem.car_call === 0 &&
          roomStateItem.car_call === 1
        ) {
          this.showCarCall(roomStateItem); // 현재 props
        }

        // 장애인 비상호출 팝업.
        if (
          !isAlertEmergCall &&
          this.props.preferences.use_emerg === 1 && // 비상호출이 오류로 발생시 끄는 옵션 기능
          this.state.roomStateItem.emerg === 0 &&
          roomStateItem.emerg === 1
        ) {
          this.showEmergCall(roomStateItem); // 현재 propss
        }

        // 호출 취소 시 초기화.(데몬에서 car_call : 0 으로 계속 전송 해서 문제 발생!)
        //  if (roomStateItem.car_call === 0) isAlertCarCall = false;
        //  if (roomStateItem.emerg === 0) isAlertEmergCall = false;

        // 공실 시 초기화.
        if (roomStateItem.sale === 0) {
          this._isMounted &&
            this.setState({
              roomSaleItem: null, // 판매 상태 초기화.
              useTime: { hh: "00", mm: "00" },
              overTime: { hh: "00", mm: "00" }, // 초과 시간.
              timeOver: false,
              checkOutAlert: false, // 사용 시간 만료 알림.
              cleaningTime: { hh: "00", mm: "00" }, // 청소 시간.
            });
        }

        // 입실 변경 경과 시간.
        let saleTime = date.diff(roomStateItem.sale_change_time, new Date());

        this._isMounted &&
          this.setState({
            roomStateItem,
            saleTime,
            // isAlertCarCall,
            // isAlertEmergCall,
            isCleanTime: roomStateItem.clean === 1,
          });

        // 매출 변경 시.
        if (this.state.roomStateItem.id && this.state.roomStateItem.sale !== roomStateItem.sale) {
          if (this.state.useTimer) clearInterval(this.state.useTimer);

          // 체크인 시 사용 시간 시작.
          if (roomStateItem.sale)
            this._isMounted &&
              this.setState({
                useTimer: setInterval(this.timer, 1000),
                timeOver: false,
                isAlarmDelay: false,
              });
        }

        if (!roomStateItem.clean) {
          this._isMounted &&
            this.setState({
              isCleanTime: false,
            });
        }
      }
    }

    // 판매 상태 변경.
    if (nextProps.roomSale && nextProps.roomSale !== this.props.roomSale) {
      const { list, rooms } = nextProps.roomSale;
      let { roomStateItem } = this.state;
      const {
        vItem: { room_id },
      } = this.props;

      // 현재 객실 판매 정보.
      const roomSaleItem =
        roomStateItem && roomStateItem.sale
          ? _.find(list, {
              id: roomStateItem.room_sale_id,
            })
          : null;

      // console.info("-- Main > Contents > RoomView > Room > roomSaleItem ", roomSaleItem);

      let timeOptionItem = {};

      if (roomSaleItem && roomSaleItem.id && roomSaleItem.time_option_id) {
        timeOptionItem =
          _.find(this.props.timeOption.list, {
            id: roomSaleItem.time_option_id,
          }) || {};

        // console.info("-- timeOptionItem ", timeOptionItem);
      }

      // 미수금액
      const repAmt = roomSaleItem
        ? roomSaleItem.default_fee +
          roomSaleItem.add_fee -
          roomSaleItem.pay_cash_amt -
          roomSaleItem.prepay_ota_amt -
          roomSaleItem.prepay_cash_amt -
          roomSaleItem.pay_card_amt -
          roomSaleItem.prepay_card_amt
        : 0;

      // 입실 변경 경과 시간.
      let saleTime = date.diff(roomStateItem.sale_change_time, new Date());

      this._isMounted &&
        this.setState(
          {
            timeOptionItem,
            roomSaleItem,
            repAmt,
            saleTime,
            loading: false,
          },
          () => {
            this.setEnableReserv(this.props.roomReserv);
          }
        );

      if (rooms) {
        const set = {
          count: 0,
          default_fee: 0,
          add_fee: 0,
          pay_cash_amt: 0,
          pay_card_amt: 0,
          prepay_ota_amt: 0,
          prepay_cash_amt: 0,
          prepay_card_amt: 0,
        };

        const roomSum = _.filter(rooms, { room_id });

        const saleSum = {
          day: _.find(roomSum, { stay_type: 1 }) || set,
          rent: _.find(roomSum, { stay_type: 2 }) || set,
          long: _.find(roomSum, { stay_type: 3 }) || set,
          sum: _.find(roomSum, { stay_type: null }) || set,
          total: _.find(roomSum, { room_id: null }) || set,
        };

        // console.info("--  ", { roomSum, saleSum });

        this._isMounted &&
          this.setState({
            saleSum,
          });
      }
    }

    // 객실 유효 예약 목록.
    if (nextProps.roomReserv && nextProps.roomReserv !== this.props.roomReserv && nextProps.roomReserv.list !== this.props.roomReserv.list) {
      this.setEnableReserv(nextProps.roomReserv);
    }
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    if (nextProps.roomView.displayLayer || nextProps.layout.displayLayer) {
      return false;
    }
    return true;
  };

  render() {
    return this._isMounted && (this.props.edit || !this.state.loading) ? (
      <Room
        {...this.props}
        {...this.state}
        clickRoom={this.clickRoom}
        handleInputChange={this.handleInputChange}
        showCarCall={this.showCarCall}
        showEmergCall={this.showEmergCall}
        handleToolTip={this.handleToolTip}
        toggleUseTime={this.toggleUseTime}
      />
    ) : (
      <div></div>
    );
  }
}

export default Container;
