// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomViewAction } from "actions/roomView";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as roomSaleAction } from "actions/roomSale";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room, roomState, roomSale, roomView, roomType, roomTheme, roomViewItem } = state;

  return {
    auth,
    preferences: preferences.item,
    room,
    roomState,
    roomSale,
    roomView,
    roomType,
    roomTheme,
    roomViewItem,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRoomStates: (palceId) => dispatch(roomStateAction.getRoomStateList(palceId)),

    initAllRoomSales: (palceId) => dispatch(roomSaleAction.getNowRoomSales(palceId)),

    initRoomViewItems: (viewId) => dispatch(roomViewItemAction.getRoomViewItemList(viewId)),

    handleNewRoom: (room, callback) => dispatch(roomAction.newRoom(room, callback)),

    handleRoomSelected: (selected) => dispatch(roomAction.setRoomSelected(selected)),

    handleSaveRoomViewItems: (viewId, items, callcack) => dispatch(roomViewItemAction.putRoomViewItems(viewId, items, callcack)),

    handleSaveRoomViewElement: (viewId, items, callcack) => dispatch(roomViewItemAction.setRoomViewItemElement(viewId, items, callcack)),

    handleSaveRoomViewItem: (id, item) => dispatch(roomViewItemAction.putRoomViewItem(id, item)),

    handleNewRoomViewItem: (item) => dispatch(roomViewItemAction.newRoomViewItem(item)),

    handleReomveRoomViewItem: (id) => dispatch(roomViewItemAction.deleteRoomViewItem(id)),

    handleDisplayRoomLayer: (display) => dispatch(roomViewAction.setDisplayRoomLayer(display)),

    handleDeleteRoomView: (place_id, id) => dispatch(roomViewAction.delRoomView(place_id, id)),

    handleUpdateRoomView: (room_view) => dispatch(roomViewAction.setRoomView(room_view)),

    handleSaveRoomView: (id, room_view) => dispatch(roomViewAction.putRoomView(id, room_view)),

    updateRoomViewSelected: (selected) => dispatch(roomViewAction.setRoomViewSelected(selected)),

    setStopReload: (stop) => dispatch(preferencesAction.setStopReload(stop)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
