// imports.
import React from "react";
import _ from "lodash";

import cx from "classnames";

//import { FormControl } from "react-bootstrap";

import DashboardIcon from "@material-ui/icons/Dashboard";
import SaveIcon from "@material-ui/icons/Save";
import AddBoxIcon from "@material-ui/icons/AddBoxOutlined";
import SettingsBackupRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import DeleteForever from "@material-ui/icons/DeleteForever";

import swal from "sweetalert";
import Swal from "sweetalert2";
// import Alert from "sweetalert-react";

import { Table } from "react-bootstrap";
import InputNumber from "rc-input-number";

// Components.
import RoomLayer from "./RoomLayer";

import RoomList from "./RoomList";

// Styles.
import "./styles.scss";

// grid layout
const RoomViewer = (props) => {
  let gridWrapper = {
    gridTemplateColumns: `repeat(${props.view.cols}, ${props.view.width}px)`, // 가로 크기 설정 값
    gridTemplateRows: `repeat(${props.view.rows}, ${props.view.height}px)`, // 세로 크기 설정 값
    gridGap: `${props.view.row_gap}px ${props.view.col_gap}px`,
  };

  // console.log("--> rows", props.rows, gridWrapper);

  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div id="tabContents">
      <div className="room-icon">
        <div className={cx("grid icon", props.edit ? "on" : "off")}>
          <div>
            <Table>
              <tbody>
                <tr>
                  <td>
                    <select placeholder="객실테마타입" value={props.view.theme_id} onChange={(evt) => props.handleInputChange("theme_id", evt.target.value)}>
                      {props.roomTheme &&
                        props.roomTheme.list.map((v, k) => (
                          <option key={k} value={v.id}>
                            {v.name}
                          </option>
                        ))}
                    </select>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span>층수</span>
                      <InputNumber className="form-control small" placeholder="층수" value={props.view.rows} step={1} min={1} max={20} onChange={(value) => props.handleInputChange("rows", value)} />
                    </span>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span className="title">
                        층별
                        <br />
                        객실수
                      </span>
                      <InputNumber
                        className="form-control small"
                        placeholder="층별객실수"
                        value={props.view.cols}
                        step={1}
                        min={1}
                        max={50}
                        onChange={(value) => props.handleInputChange("cols", value)}
                      />
                    </span>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span className="title">
                        객실
                        <br />
                        넓이
                      </span>
                      <InputNumber
                        className="form-control lmid"
                        placeholder="객실넓이"
                        value={props.view.width}
                        step={5}
                        min={50}
                        max={500}
                        onChange={(value) => props.handleInputChange("width", value)}
                      />
                    </span>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span className="title">
                        {" "}
                        객실
                        <br />
                        높이
                      </span>
                      <InputNumber
                        className="form-control lmid"
                        placeholder="객실높이"
                        value={props.view.height}
                        step={5}
                        min={30}
                        max={500}
                        onChange={(value) => props.handleInputChange("height", value)}
                      />
                    </span>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span className="title">
                        객실
                        <br />
                        간격
                      </span>
                      <InputNumber
                        className="form-control lmid"
                        placeholder="객실간격"
                        value={props.view.col_gap}
                        step={1}
                        min={1}
                        max={500}
                        onChange={(value) => props.handleInputChange("col_gap", value)}
                      />
                    </span>
                  </td>
                  <td>
                    <span className="numeric-wrap">
                      <span className="title">
                        층별
                        <br />
                        간격
                      </span>
                      <InputNumber
                        className="form-control lmid"
                        placeholder="층별간격"
                        value={props.view.row_gap}
                        step={1}
                        min={1}
                        max={500}
                        onChange={(value) => props.handleInputChange("row_gap", value)}
                      />
                    </span>
                  </td>
                  <td>
                    <select placeholder="객실추가타입" value={props.view.all_room} onChange={(evt) => props.handleInputChange("all_room", evt.target.value)}>
                      <option value={1}>모든객실추가</option>
                      <option value={0}>남은객실추가</option>
                    </select>
                  </td>
                  <td>
                    <select placeholder="객실추가순서" value={props.view.room_order} onChange={(evt) => props.handleInputChange("room_order", evt.target.value)}>
                      <option value={1}>객실명순</option>
                      <option value={2}>객실명역순</option>
                      <option value={3}>타입명순</option>
                      <option value={4}>타입명역순</option>
                    </select>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </div>

        <DeleteForever className={cx("icon", "save", props.edit ? "on" : "off")} onClick={() => props.onDeleteView()} />
        <AddBoxIcon className={cx("icon", "add", props.edit ? "on" : "off")} color="action" onClick={() => props.onAddItem()} />
        <SettingsBackupRestoreIcon className={cx("icon", "redo", props.edit ? "on" : "off")} color={!props.change ? "disabled" : "action"} onClick={() => props.onReloadItem()} />
        <SaveIcon
          className={cx("icon", "save", props.edit ? "on" : "off")}
          color={!props.change && !props.viewChange ? "disabled" : "action"}
          style={{ pointerEvents: !props.change && !props.viewChange ? "none" : "auto" }}
          onClick={() => props.onSaveItem(false)}
        />
        <DashboardIcon className={cx("edit", props.edit ? "on" : "off")} color="action" onClick={() => props.onEditView()} />
      </div>
      {/* 체크인 팝업 */}
      {props.roomView.displayLayer && props.room.selected === props.room.item.id && <RoomLayer {...props} />}
      <div
        className={cx("grid-wrapper", props.edit ? "edit" : "")}
        style={{ ...gridWrapper, fontSize: props.preferences.font_size_3 }}
        onMouseMove={(evt) => props.onMouseDrag(evt)}
        onMouseUp={(evt) => props.onMouseUp(evt)}
      >
        {_.map(props.items, (el, i) => {
          if (el) {
            return <RoomList key={i} {...props} el={el} />;
          }
        })}
      </div>
    </div>
  );
};

export default RoomViewer;
