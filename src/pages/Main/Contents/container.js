// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

// Components.
import Contents from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomState: undefined,
      roomSale: undefined,
      room_view: {
        title: "",
        comment: "",
        cols: 0,
        order: 0,
        all_room: 0,
        theme_id: 0,
        room_order: 1,
      },
      alert: { show: false },
    };
  }

  _isMounted = false;

  static propTypes = {
    auth: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    roomTheme: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
    roomSale: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { room_view } = this.state;

    // 뷰 아이템 정보 변경.
    room_view[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleInputChange name", name, "value", value);

    this.setState({
      room_view,
    });
  };

  onSave = () => {
    const {
      user: { place_id },
    } = this.props.auth;

    let { room_view } = this.state;

    if (!room_view.theme_id) room_view.theme_id = this.props.roomTheme.list[0].id;

    // 입력값 검사.
    const isValid = [];

    _.map(room_view, (v, k) => {
      if (v === undefined) isValid.push(k);
    });

    console.log("-- Main > Contents onSave", room_view, isValid);

    if (isValid.length) {
      this.setState({
        alert: { show: true, text: isValid[0] + " 이 없습니다." },
      });
    } else {
      room_view.place_id = place_id;

      this.props.addRoomView(place_id, { room_view });

      // 화면 갱신을 위해 선택 초기화.
      this.props.updateRoomViewSelected(-1);

      this.props.initAllRoomViews(place_id);

      setTimeout(() => {
        this.onSelect(room_view.order - 1);
      }, 1000);

      this.setState({
        room_view: {
          title: "",
          comment: "",
          cols: 0,
          order: 0,
          all_room: 0,
          theme_id: 0,
          room_order: 1,
        },
      });
    }
  };

  onCancel = () => {
    const {
      user: { place_id },
    } = this.props.auth;

    // 화면 갱신을 위해 선택 초기화.
    this.props.updateRoomViewSelected(-1);

    this.props.initAllRoomViews(place_id);
    this.onSelect(0);

    this.setState({
      room_view: {
        title: "",
        comment: "",
        cols: 0,
        order: 0,
        all_room: 0,
        theme_id: 0,
        room_order: 1,
      },
    });
  };

  onConfirm = () => {
    this.setState({
      alert: { show: false, title: "" },
    });
  };

  onSelect = (key) => {
    //console.log("-- Main > Contents onSelect", key, this.props.roomView.selected);
    this.props.updateRoomViewSelected(key);

    if (key === 99) {
      const { list } = this.props.roomView;
      let { room_view } = this.state;

      // 화면 등록 시.
      if (!room_view.title) {
        room_view = {
          title: "객실화면 " + (list.length + 1),
          comment: "객실화면 " + (list.length + 1),
          cols: 10,
          order: list.length + 1,
          all_room: 0,
          room_order: 1,
          theme_id: 0,
        };
      }

      console.log("-- room_view", room_view);

      this.setState({
        room_view,
      });
    }
  };

  componentDidMount = () => {
    this._isMounted = true;

    const {
      auth: {
        user: { place_id },
      },
      preferences: { doorlock_send_qr_code },
    } = this.props;

    console.info("----------------------------------------------------------");
    console.info("-- Main > Contents 업소 기본 정보 모두 조회 componentDidMount ");
    console.info("----------------------------------------------------------");

    // 업소 기본 정보 조회.
    this.props.initRoomThemes();
    this.props.initAllRoomTypes(place_id);
    this.props.initAllRoomViews(place_id);
    this.props.initAllRoomViewItems(place_id);
    this.props.initAllSubscribes();

    // 도어락 정보 조회.(QR 도어락 사용 시)
    if (doorlock_send_qr_code) this.props.initAllRoomDoorLock(place_id);

    // 객실 정보 조회.
    this.props.initAllRooms(place_id, () => {});

    setTimeout(() => {
      this.props.initAllSeasonPremium(place_id);
      this.props.initTimeOptions(place_id);
      this.props.initAllPlaceSubscribes(place_id);

      // 객실 예약 정보
      let filter = `a.place_id=${place_id}`; // 1 일내의 데이터 전송.
      let begin = moment().add(-7, "day").format("YYYY-MM-DD 00:00");
      let end = moment().add(1, "month").format("YYYY-MM-DD 00:00");

      this.props.initRoomReservs(filter, begin, end);
    }, 2000);
  };

  componentWillUnmount = () => {
    this._isMounted = false;
    if (this.timerReload) clearInterval(this.timerReload);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this._isMounted) return;
  }

  render() {
    return this.props.room.list && this.props.roomView.list && this.props.preferences ? (
      <Contents
        {...this.props}
        {...this.state}
        openModal={this.openModal}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSelect={this.onSelect}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default Container;
