// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";
import _ from "lodash";
import moment from "moment";
import crypto from "crypto";

import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";

import { YYDoorLock } from "utils/doorlock-util";
// import { CheckLicense } from "utils/subscribe-util";

// Components.
import Main from "./presenter";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      mouseMoveTime: moment(), // 마우스 움직임 시간.
      isOverTimeLock: false, // 자동 잠김 여부.
      isLock: false,
      lockDelay: false,
      lock: false,
      lockItem: {},
      sysLock: false,
      error: {
        type: "info",
        message: "오류 메세지 입니다.",
        show: false,
      },
      pageDataReload: false,
      pageReload: false,
    };

    this.timer = null;

    this.loading = false;

    this._isMounted = false;
  }

  static propTypes = {
    common: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    contents: PropTypes.object.isRequired,
    network: PropTypes.object.isRequired,
    placeLock: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
    roomDoorLock: PropTypes.object.isRequired,
  };

  timerNetwork = null;

  onLockClick = (value) => {
    const { lock } = this.state;

    const {
      user: { id, pwd, level },
      placeLock: { list },
      preferences: { place_lock_time },
    } = this.props;

    let lockItem = list.length ? list[0] : { auto_lock: 0 };

    if (!lock) {
      if (!value) {
        Swal.fire({
          icon: "info",
          title: lockItem.auto_lock ? "자동 잠금 해제 알람" : "잠금 해제 알림",
          html: `${lockItem.auto_lock ? place_lock_time + "분간 자리비움 상태가 지속되어 화면이 자동으로 잠겼습니다.<br/><br/>" : ""} 화면 잠금을 해제 하려면 로그인 비밀번호를 입력 하세요`,
          input: "password",
          inputPlaceholder: "로그인 비밀번호를 입력해 주세요",
          inputAttributes: {
            maxlength: 20,
          },
          showCancelButton: true,
          showCloseButton: false,
          confirmButtonText: "확인",
          cancelButtonText: "취소",
        }).then((result) => {
          if (result.value) {
            this.onLockClick(result.value);
          }
        });
      } else {
        // 해시 생성
        var shasum = crypto.createHash("sha1");
        shasum.update(value);

        // 비밀번호 sha1 형식으로 암호화.
        var pwdHex = shasum.digest("hex");
        // console.log("- pwd sha1 ", pwdHex, pwd);

        // 상위/하위 레벨 잠금 직원 (직원 level 은 낮을 수록 권한이 높다)
        const highLevel = _.filter(list, (lock) => lock.level < level)
          .map((v) => v.user_id)
          .join(",");

        const lowLevel = _.filter(list, (lock) => (lock.level > level || lock.user_id === id) && lock.type < 9); // 하위 레벨은 내잠금 포함.(시스템 잠금은 제외!)

        console.log("- highLevel", highLevel, ", lowLevel", lowLevel, lock, id);

        if (pwdHex !== pwd) {
          Swal.fire({
            icon: "error",
            title: "잠금 해제 비밀번호 오류 ",
            html: "로그인 비밀번호가 올바르지 않습니다.<br/><br/>다시 입력해 주세요.",
            input: "password",
            inputPlaceholder: "로그인 비밀번호를 입력해 주세요",
            inputAttributes: {
              maxlength: 20,
            },
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonText: "확인",
            cancelButtonText: "취소",
          }).then((result) => {
            if (result.value) {
              this.onLockClick(result.value);
            }
          });
        } else if (lowLevel.length === 0) {
          Swal.fire({
            icon: "error",
            title: "잠금 해제 권한 알림",
            html: highLevel + " 님이 설정한 잠금을 해제 할 수 없습니다. <br/><br/>로그 아웃 하시겠습니까?",
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonText: "확인",
            cancelButtonText: "취소",
          }).then((result) => {
            if (result.value) {
              this.props.handleLogout();
            }
          });
        } else {
          // 나 또는 하위 직원 잠금 해제.
          _.map(lowLevel, (lock) => {
            this.props.handleDelPlaceLock(lock.id);
          });

          this.setState({
            isOverTimeLock: false,
          });
        }
      }
    }
  };

  handleDismiss = (close) => {
    const { sysLock, lockItem, error } = this.state;

    console.log("- handleDismiss", sysLock, lockItem);

    this.setState({
      error: _.merge({}, error, {
        show: false,
      }),
      lockItem: sysLock ? lockItem : {},
    });
  };

  // 통신 유실시 업데이트 못한것 적용 .
  loadConfig = () => {
    const {
      preferences: { id, data_reload_term, page_reload },
      stopLoad,
      user: { place_id },
    } = this.props;

    const { pageDataReload, pageReload } = this.state;

    if (place_id && !this.props.roomView.displayLayer && !this.props.layout.displayLayer && !this.state.isLock && !stopLoad) {
      console.log("====> reload Preferences PlaceLock data term: ", data_reload_term, pageDataReload, pageReload);

      if (page_reload || pageReload) {
        // 페이지 갱신 플레그 초기화.
        this.props.handleSavePreferences(id, {
          preferences: { place_id, page_reload: 0 },
        });

        // 페이지 갱신
        setTimeout(() => {
          window.location.href = "/";
        }, 1000);
      } else {
        this.props.initPreferences(place_id);
        this.props.initAllPlaceLock(place_id); // 화면 잠금.
        this.props.initAllInterrupts(place_id);

        if (pageDataReload) {
          this.props.initAllRoomStates(place_id);
          setTimeout(() => {
            this.props.initAllRoomSales(place_id);
          }, 100);
        }
      }
    }

    // 데이터 리로딩(사용중이면 건너뛴다)
    this.setState({
      pageDataReload: false,
      pageReload: false,
    });
  };

  onMouseMove = (evt) => {
    // console.log("- onMouseMove ");

    this.setState({
      mouseMoveTime: moment(),
    });
  };

  // 아이크루 어드민 여부.
  isAdmin = () => {
    const {
      user: { type, level },
      isViewerLogin,
      isIcrewViewer,
    } = this.props.auth;

    console.log("- isAdmin", { type, level, isViewerLogin, isIcrewViewer });

    return isIcrewViewer || isViewerLogin || (type === 1 && level < 3);
  };

  // QR 도어락 로그인.
  loginQrDoorLock = (props) => {
    const {
      preferences: { doorlock_send_qr_code },
      roomDoorLock: {
        login: { token },
      },
    } = props;

    console.log("- loginQrDoorLock", { doorlock_send_qr_code, token });

    // YY 도어락 API 유틸.
    global.yYDoorLock = new YYDoorLock(props);

    // 도어락 로그인 토큰 조회.(QR 도어락 사용 시)
    if (doorlock_send_qr_code && !token) {
      // YY 도어락 API 로그인.
      // Cors 회피를 위해 YY Lock 사이트 연동은 API 서버에서 한다.
      let { APPID, AT, NONCESTR, USERNAME, PASSWORD } = global.yYDoorLock;

      console.log("- global yYDoorLock", { APPID, AT, NONCESTR, USERNAME, PASSWORD });

      if (APPID && USERNAME !== "null" && PASSWORD !== "null") {
        let params = { APPID, AT, NONCESTR, USERNAME, PASSWORD };

        var query = global.yYDoorLock.orderedQuery(params);

        this.props.handleLoginQrCode({ query }).then(({ success, data }) => {
          if (success) {
            let {
              result,
              token,
              dhUser: { USERNAME, USER_ID },
              msg,
            } = data || {};

            global.yYDoorLock.TOKEN = token;
          }
        });
      } else {
        console.error("- yYDoorLock Login Error", { doorlock_send_qr_code, APPID, USERNAME, PASSWORD });
      }
    }
  };

  componentDidMount = () => {
    const {
      user: { place_id },
    } = this.props;

    console.log("- isAdmin", this.isAdmin());

    this._isMounted = true;

    // 업소 정보 조회.
    this.props.initPlace(place_id);
    this.props.initPreferences(place_id);
    this.props.initAllPlaceLock(place_id);
    this.props.initAllInterrupts(place_id);
    this.props.initAllRoomFees(place_id, 0); // 적용 채널 (0: 카운터, 1:자판기)

    // 장비 조회.
    this.props.initDevices(place_id);

    setTimeout(() => {
      this.setState({
        lockDelay: true,
      });

      // QR 도어락 로그인.
      this.loginQrDoorLock(this.props);
    }, 2000);

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      const hour = Number(moment().format("HH"));
      const min = Number(moment().format("mm"));
      const sec = Number(moment().format("ss"));

      const url = window.location.href;

      let { mouseMoveTime, pageReload, pageDataReload, isLock, isOverTimeLock } = this.state;

      const {
        user: { id, level },
        preferences: { place_id, data_reload_term, eagle_url, place_lock_use, place_lock_time },
        placeLock: { list },
      } = this.props;

      // 업소 를 10개 그룹으로 나누어서 해당 시간(분)에 각각 업데이트 한다.(한번에 모두 하면 트래픽이 너무 많이 발생 한다.)
      let place_min = place_id % 10;

      // console.log("- time ", { hour, min, sec, place_id, place_min });

      // 일정시간 자리비움 시 전체 화면 잠금
      if (place_lock_use && !isOverTimeLock) {
        let isOverTime = moment(mouseMoveTime).add(place_lock_time, "minute").isBefore(moment());

        if (isOverTime) {
          console.log("- isOverTime ", isOverTime, place_lock_time, mouseMoveTime.format("YYYY-MM-DD HH:mm:ss"), moment().format("YYYY-MM-DD HH:mm:ss"));

          this.setState(
            {
              isOverTimeLock: true,
            },
            () => {
              // 잠금 설정(하위 권한 잠금은 어드민에서만 가능 하다)
              const place_lock = { place_id, user_id: id, type: 0, auto_lock: 1 };
              this.props.handleNewPlaceLock({ place_lock });
            }
          );
        }
      }

      // 화면 잠금/해제 체크
      const isNowLock = _.filter(list, (lock) => (lock.type > 0 && lock.level < level) || lock.user_id === id).length > 0;

      if (isNowLock !== isLock) {
        this.setState({
          isLock,
        });
      }

      // 접속 URL 체크
      if (sec === 0) {
        if (eagle_url && !this.isAdmin() && eagle_url.replace(/\//g, "") !== url.replace(/\//g, "")) {
          console.log("- url", { eagle_url, url });

          Swal.fire({
            icon: "warning",
            title: "잘못된 접속 알림",
            html: "비정상 접속 입니다. 보안을 위해 자동 로그아웃 됩니다. <br/><br/>다시 로그인을 해주세요.",
            showCancelButton: false,
            showCloseButton: false,
            confirmButtonText: "확인",
          }).then((result) => {
            window.location.href = eagle_url;
          });

          // 화면 갱신 중단.
          pageReload = false;
          pageDataReload = false;

          if (this.timer) clearInterval(this.timer);
          return;
        }
      } else {
        // 설정 분마다 데이터 리로딩
        if (data_reload_term && min % data_reload_term === 0 && sec === 0) {
          pageDataReload = true;
        }
      }

      // 환경 설정 데이터는 1분 마다 조회.
      if (sec === 0) {
        this.setState(
          {
            pageDataReload,
            pageReload,
          },
          () => this.loadConfig()
        );
      }

      // QR 도어락 로그인은 1시간 마다 수행.
      if (min === 0 && sec === 0) {
        this.loginQrDoorLock(this.props);
      }
    }, 1000);
  };

  componentWillUnmount = () => {
    this._isMounted = false;

    if (this.timerNetwork) clearInterval(this.timerNetwork);
    if (this.timer) clearInterval(this.timer);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this._isMounted) return;

    // 로딩바
    if (nextProps.common && nextProps.common !== this.props.common) {
      const { loading } = nextProps.common;

      console.log("- Swal.isVisible ", Swal.isVisible());

      if (!Swal.isVisible()) {
        // 로딩바 엉뚱하게 발생 해서 주석처리
        // this.setState({
        //   loading,
        // });
      } else {
        this.setState({
          loading: false,
        });
      }
    }

    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      console.log("- nextProps.preferences ", nextProps.preferences);

      this.loginQrDoorLock(nextProps);
    }

    if (nextProps.placeLock && nextProps.placeLock !== this.props.placeLock) {
      const { list } = nextProps.placeLock;
      const { user } = this.props;

      console.log("--- componentWillReceiveProps placeLock ", list);

      const isLock = _.filter(list, (lock) => (lock.type > 0 && lock.level < user.level) || lock.user_id === user.id).length > 0; // 하위 권한 잠금 또는 내가 잠금 시 화면 잠김.(직원 level 은 낮을 수록 권한이 높다)

      this.setState({
        isLock,
      });
    }

    // 네트웤 오류.
    if (nextProps.network && nextProps.network !== this.props.network) {
      const { item } = nextProps.network;
      const { sysLock, lockItem, error } = this.state;

      console.log("--- componentWillReceiveProps network ", JSON.stringify(item) !== JSON.stringify(lockItem));

      if (this.timerNetwork) clearTimeout(this.timerNetwork);

      if (item) {
        let name = item.mode === "SOCKET" ? "인터넷" : item.mode === "DEVICE" ? "장치" : item.mode;

        if (!item.status && (lockItem.mode === item.mode || !lockItem.mode)) {
          this.timerNetwork = setTimeout(() => {
            let message = item.mode === "SOCKET" ? "인터넷 연결을 확인해 주세요!" : name + " 연동 오류 입니다!";

            if (!error.show && !item.status) {
              this._isMounted &&
                this.setState(
                  {
                    error: _.merge({}, error, {
                      show: false,
                    }),
                  },
                  () => {
                    this.setState({
                      error: {
                        type: "warning",
                        message,
                        show: true,
                      },
                      dialog: false,
                      lockItem: item,
                      sysLock: true,
                      lock: true,
                    });
                  }
                );
            }
          }, 10 * 1000);
        } else if (item.status && sysLock && lockItem.mode === item.mode && error.show) {
          console.log("- lock off alert ", item);
          this._isMounted &&
            this.setState(
              {
                error: _.merge({}, error, {
                  show: false,
                }),
              },
              () => {
                this.setState({
                  error: {
                    type: "success",
                    message: item.mode === "SOCKET" ? "인터넷 연결이 정상화 되었습니다." : name + " 연동이 정상화 되었습니다.",
                    show: true,
                  },
                  sysLock: false,
                  lock: false,
                });

                // 연동 오류 삭제
                this.props.handleDelNetworkInfo(item);
              }
            );

          setTimeout(() => {
            if (this.state.error.show) {
              console.log("- lock setTimeout ");
              this.handleDismiss();
            }
          }, 5000);
        }
      }
    }
  }

  render() {
    return this._isMounted ? (
      <Main {...this.props} {...this.state} onLockClick={this.onLockClick} onMouseMove={this.onMouseMove} handleDismiss={this.handleDismiss} />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
