import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

//import Clock from "./Clock";
import RoomInfo from "./RoomInfo";
import RoomSales from "./RoomSales";
import RoomHistory from "./RoomHistory";
import Clock from "./Clock";
import RollingBanner from "./RollingBanner";

import "./styles.scss";

class LeftMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      leftMenuHeight: 0,
    };
  }

  leftMenu = undefined;
  heightTimer = null;

  componentDidMount() {
    if (this.leftMenu) clearInterval(this.heightTimer);

    this.heightTimer = setInterval(() => {
      if (this.leftMenu) {
        clearInterval(this.heightTimer);

        var calculatedHeight = this.leftMenu.clientHeight;
        console.log("- leftMenuHeight", calculatedHeight);

        this.setState({
          leftMenuHeight: calculatedHeight,
        });
      }
    }, 1000);
  }

  componentWillUnmount() {}

  render() {
    return (
      <div ref={(ref) => (this.leftMenu = ref)}>
        <ul>
          <li>
            <div className="clock">
              <Clock />
            </div>
          </li>
          <li>
            <div className="ad">
              <RollingBanner />
            </div>
          </li>
          <li>
            <RoomInfo leftMenu={this.leftMenu} />
          </li>
          <li>
            <RoomSales leftMenu={this.leftMenu} />
          </li>
          <li>
            <RoomHistory leftMenu={this.leftMenu} />
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default withRouter(connect(mapStateToProps)(LeftMenu));
