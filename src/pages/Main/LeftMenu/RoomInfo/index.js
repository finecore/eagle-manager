import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";
import { keyToValue, displayStateName } from "constants/key-map";
import date from "utils/date-util";
import $ from "jquery";

// Style
import { Collapse } from "react-bootstrap";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

class RoomInfo extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
    roomView: PropTypes.object.isRequired,
    roomViewItem: PropTypes.object.isRequired,
    roomSale: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      data: { room: {}, sale: {}, state: {} },
      loading: true,
      formMenuOpen: false,
      showRoom: true,
      timer: null,
      diffTime: 0,
      element: undefined,
      defaultFee: {
        state: "", // 미입실 상태.
        stay_type: "", // 미입실 상태.
        person: 0,
        room_reserv_id: 0,
        default_fee: 0,
        add_fee: 0,
        pay_card_amt: 0,
        pay_cash_amt: 0,
        prepay_ota_amt: 0,
        prepay_cash_amt: 0,
        prepay_card_amt: 0,
        alarm: 0,
        alarm_hour: 0,
        alarm_min: 0,
      },
    };
  }

  setRoomInfo = (props) => {
    const {
      roomView,
      roomViewItem,
      room: { item, selected },
      roomSale,
      roomState,
    } = props;

    if (item) {
      const state = _.find(roomState.list, { room_id: item.id }) || {};

      let sale = _.find(roomSale.list, { id: state.room_sale_id }) || this.state.defaultFee;

      sale.pay_amt = sale.pay_cash_amt + sale.pay_card_amt;
      sale.receive_amt = sale.default_fee + sale.add_fee - sale.pay_cash_amt - sale.pay_card_amt - sale.prepay_ota_amt - sale.prepay_cash_amt - sale.prepay_card_amt;

      // console.log("- RoomInfo sale", sale, state, selected);

      this.setState({
        data: { room: item, sale, state },
      });

      if (selected === item.id) {
        const viewItem = _.find(roomViewItem.viewElement[roomView.selected], { item: { room_id: item.id } }) || {};

        const ele = document.getElementById("roomListDiv_" + roomView.selected + "_" + viewItem.i);

        // console.log("- RoomInfo ele", ele, "roomListDiv_" + roomView.selected + "_" + viewItem.i);

        const roomElement = document.getElementById("roomElement");

        if (ele) {
          const element = ele.cloneNode(true);

          // context menu 제거.
          $(element).find(".react-contextmenu").remove();

          element.style.fontSize = "20px";

          const vDstElements = element.getElementsByTagName("*");
          for (var i = vDstElements.length; i--; ) {
            var vDstElement = vDstElements[i];
            vDstElement.style.fontSize = roomView.mode === 0 ? "14px" : "16px"; // mode 0: 객실상태보기, 1:객실매출보기
          }

          if (roomElement) {
            roomElement.innerHTML = "";
            roomElement.appendChild(element);
          }
        }
      }
    }
  };

  componentDidMount() {
    this.setRoomInfo(this.props);

    const timer = setInterval(() => {
      if (this.state.data.sale.check_in) {
        const diff = date.diff(this.state.data.sale.check_in, new Date());
        //console.log(diff);
        this.setState({
          diffTime: diff.hh + ":" + diff.mm,
        });
      }
    }, 1000);

    // 로딩 상태 종료
    this.setState({
      loading: false,
      timer,
    });
  }

  componentWillUnmount = () => {
    clearInterval(this.state.timer);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 객실 정보 업데이트.
    if (nextProps.room && nextProps.room !== this.props.room) {
      // console.log(
      //   "--- Main > Contents > LeftMenu > RoomInfo 좌측메뉴 객실 정보 업데이트 room ",
      //   nextProps.room.item.id
      // );

      this.setRoomInfo(nextProps);
    }

    // 객실 상태 업데이트.
    if (nextProps.roomState && nextProps.roomState !== this.props.roomState && nextProps.roomState.item.room_id === this.props.room.selected) {
      // console.log(
      //   "--- Main > Contents > LeftMenu > RoomInfo 좌측메뉴 객실 상태 업데이트 roomState ",
      //   nextProps.roomState.item.room_id
      // );

      this.setRoomInfo(nextProps);
    }

    // 객실 판매 업데이트.
    if (nextProps.roomSale && nextProps.roomSale !== this.props.roomSale && nextProps.roomSale.item.room_id === this.props.room.selected) {
      // console.log(
      //   "--- Main > Contents > LeftMenu > RoomInfo 좌측메뉴 객실 상태 업데이트 roomSale ",
      //   nextProps.roomSale.item.room_id
      // );

      this.setRoomInfo(nextProps);
    }
  }

  render() {
    if (this.state.loading) {
      // 로딩중일때 로더 보여주기
      return <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>;
    } else {
      return (
        <div className="room-info">
          <dl className="content">
            <dt className={this.state.formMenuOpen ? "active" : null}>
              <h2>객실정보</h2>
              <div className="room">
                <span>{this.state.data.room.name}</span>
                <span>{this.state.data.room.type_name}</span>
              </div>
              <a onClick={() => this.setState({ formMenuOpen: !this.state.formMenuOpen })} data-toggle="collapse">
                <b className="caret" />
              </a>
            </dt>
            <Collapse in={this.state.formMenuOpen}>
              <dd>
                <ul>
                  <li>
                    <span className="tit">객실상태 :</span>
                    <span className="info">
                      {keyToValue("room_state", "sale", this.state.data.state.sale || "")} {displayStateName(this.state.data.state, this.state.data.sale, this.state.data.room, this.props.preferences)}
                    </span>
                  </li>
                  <li>
                    <span className="tit">기본요금 :</span>
                    <span className="info">{String(this.state.data.sale.default_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                  </li>
                  <li>
                    <span className="tit">추가요금 :</span>
                    <span className="info">{Number(this.state.data.sale.add_fee || 0) > 0 ? String(this.state.data.sale.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0}</span>
                  </li>
                  <li>
                    <span className="tit">할인요금 :</span>
                    <span className="info">{Number(this.state.data.sale.add_fee || 0) < 0 ? String(this.state.data.sale.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0}</span>
                  </li>
                  <li>
                    <span className="tit">결제요금 :</span>
                    <span className="info green">{String(this.state.data.sale.pay_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                  </li>
                  <li>
                    <span className="tit">미수금 :</span>
                    <span className="info red">{String(this.state.data.sale.receive_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                  </li>
                  <li>
                    <span className="tit">입실시간 : </span>
                    <span className="info">{this.state.data.state.sale ? moment(this.state.data.sale.check_in).format("HH:mm") : "-"}</span>
                  </li>
                  <li>
                    <span className="tit">이용시간 : </span>
                    <span className="info">{this.state.data.state.sale && this.state.diffTime ? this.state.diffTime : "-"}</span>
                  </li>
                </ul>
                <div className={cx("room-btn", this.state.showRoom && "active")}>
                  <a onClick={() => this.setState({ showRoom: !this.state.showRoom })} data-toggle="collapse">
                    <b className="caret" />
                  </a>
                </div>
                {/* <div className={cx("room-ele", this.state.showRoom && "on")}>
                  <div id="roomElement" />
                </div> */}
                <div className="light">{this.state.data.state.light && this.state.data.state.light.split("/").map((v, k) => <span key={k} className={Number(v) === 1 ? "on" : ""} />)}</div>
              </dd>
            </Collapse>
          </dl>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const { roomView, roomViewItem, room, roomSale, roomState, preferences } = state;

  return {
    roomView,
    roomViewItem,
    room,
    roomSale,
    roomState,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomInfo);
