import React, { Component } from "react";
import { connect } from "react-redux";
// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";
import moment from "moment";

class Clock extends Component {
  state = {
    date: null,
    ampm: null,
    hour: null,
    min: null,
    sec: true,
    hhmm: false, // 시간 표기 타입.
    tick: false,
  };

  onTimeClick() {
    let { hhmm } = this.state;

    hhmm = !hhmm;

    this.setState({
      hhmm,
      date: moment().format("YYYY MM DD dddd"),
      ampm: moment().format("HH") < 12 ? "AM" : "PM",
      hour: moment().format(hhmm ? "HH" : "h"),
      min: moment().format("mm"),
      sec: moment().format("ss"),
    });
  }

  componentDidMount() {
    setInterval(() => {
      //console.log("- sec ", this.state.sec);
      this.setState({
        date: moment().format("YYYY MM DD dddd"),
        ampm: moment().format("HH") < 12 ? "AM" : "PM",
        hour: moment().format(this.state.hhmm ? "HH" : "h"),
        min: moment().format("mm"),
        sec: moment().format("ss"),
        tick: !this.state.tick,
      });
    }, 1000);
  }

  componentWillUnmount() {}

  render() {
    return (
      <div onClick={() => this.onTimeClick()}>
        <div className="date">{this.state.date}</div>
        {!this.state.hhmm && <span className={cx("ampm", this.state.ampm)}>{this.state.ampm}</span>}
        <span>
          <span className="hour">{this.state.hour}</span>
          <span className="min">
            <span className={this.state.tick ? "on" : "off"}>:</span>
            {this.state.min}
          </span>
          <span className="sec">
            {/* <span className={this.state.tick ? "on" : "off"}>:</span> */}
            {this.state.sec}
          </span>
        </span>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(Clock);
