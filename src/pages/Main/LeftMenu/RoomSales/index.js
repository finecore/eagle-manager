import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import moment from "moment";
import _ from "lodash";

// Style
import { Collapse, Table, FormGroup, Checkbox } from "react-bootstrap";
import { Wave } from "better-react-spinkit";
import cx from "classnames";

// Actions.
import { actionCreators as roomSaleAction } from "actions/roomSale";

class RoomSales extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    placeLock: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
    roomSale: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      timer: null,
      start: moment(),
      today: undefined,
      loading: true,
      viewOption: { show_sale_3: "0", show_sale_1_detail: "0", show_sale_2_detail: "0", show_sale_3_detail: "0" },
      formMenuOpen: true,
    };

    this._isMounted = false;
  }

  // 통신 유실시 업데이트 못한것 적용 .
  loadData = () => {
    const {
      preferences: { place_id, day_sale_end_time, data_reload_term = 1 },
      stopLoad,
      user: { level, id },
      placeLock: { list },
    } = this.props;

    const isLock = _.filter(list, (lock) => (lock.type > 0 && lock.level < level) || lock.user_id === id).length > 0; // 하위 권한 잠금 또는 내가 잠금 시 화면 잠김.(직원 level 은 낮을 수록 권한이 높다)

    if (place_id && !this.props.roomView.displayLayer && !this.props.layout.displayLayer && !isLock && !stopLoad) {
      console.log("====> reload RoomSaleDaySum ", day_sale_end_time, data_reload_term, this.props.roomView.displayLayer, this.props.layout.displayLayer, isLock, stopLoad);

      let start = moment({ hour: day_sale_end_time, minute: 0 });

      if (start > moment()) {
        // 현재 시간 이전이라면 어제 일자로.
        start = start.add(-1, "day");
      }

      // 일일 업소 매출.
      this.props.initPlaceRoomSale(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

      // 일일 객실별 매출
      this.props.initPlaceRoomSaleRoom(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

      this._isMounted &&
        this.setState({
          start,
        });
    }
  };

  setOption = (name, value) => {
    let { viewOption } = this.state;

    viewOption[name] = value;
    sessionStorage.setItem(name, value);

    console.log("- setOption", { name, value });

    this.setState({
      viewOption,
    });
  };

  componentDidMount() {
    const {
      preferences: { data_reload_term = 1 },
    } = this.props;

    console.log("- componentDidMount data_reload_term", data_reload_term);

    let { timer, viewOption } = this.state;
    if (timer) clearInterval(timer);

    // 일정 시간마다 재조회.
    timer = setInterval(() => {
      this.loadData();
    }, data_reload_term * 60 * 1000);

    _.map(viewOption, (v, k) => {
      viewOption[k] = sessionStorage.getItem(k);
    });

    console.log("- componentDidMount viewOption", viewOption);

    this.setState({
      timer,
      loading: false,
      viewOption,
    });

    this._isMounted = true;
  }

  componentWillUnmount() {
    let { timer } = this.state;
    if (timer) clearInterval(timer);

    this._isMounted = false;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 객실 판매 정보.
    if (nextProps.roomSale && nextProps.roomSale.place !== this.props.roomSale.place) {
      // console.log("--- Main > Contents > LeftMenu > RoomSale 좌측메뉴 매출 정보 업데이트 roomSale", nextProps.roomSale.place);

      const { place } = nextProps.roomSale;

      const set = {
        count: 0,
        default_fee: 0,
        add_fee: 0,
        pay_cash_amt: 0,
        pay_card_amt: 0,
        pay_point_amt: 0,
        prepay_ota_amt: 0,
        prepay_cash_amt: 0,
        prepay_card_amt: 0,
        prepay_point_amt: 0,
      };

      this._isMounted &&
        this.setState(
          {
            today: {
              day: _.find(place, { stay_type: 1 }) || set,
              rent: _.find(place, { stay_type: 2 }) || set,
              long: _.find(place, { stay_type: 3 }) || set,
              sum: _.find(place, { stay_type: null }) || set,
            },
          },
          () => {
            const { today } = this.state;
            const { day, rent, long } = today;

            today.day.recept =
              day.pay_cash_amt + day.prepay_ota_amt + day.prepay_cash_amt + day.pay_card_amt + day.prepay_card_amt + day.pay_point_amt + day.pay_point_amt - (day.default_fee + day.add_fee);
            today.rent.recept =
              rent.pay_cash_amt + rent.prepay_ota_amt + rent.prepay_cash_amt + rent.pay_card_amt + rent.prepay_card_amt + rent.pay_point_amt + rent.pay_point_amt - (rent.default_fee + rent.add_fee);
            today.long.recept =
              long.pay_cash_amt + long.prepay_ota_amt + long.prepay_cash_amt + long.pay_card_amt + long.prepay_card_amt + long.pay_point_amt + long.pay_point_amt - (long.default_fee + long.add_fee);
            today.sum.recept = day.recept + rent.recept + long.recept;

            console.log("- recept", day.recept, rent.recept, long.recept);

            this._isMounted &&
              this.setState({
                today,
              });
          }
        );
    }

    // 객실 입실 정보 변경 시 조회.(로딩 후)
    if (this.state.today && nextProps.roomState && nextProps.roomState.item !== this.props.roomState.item) {
      const { item } = nextProps.roomState;

      let roomItem = _.find(this.props.roomState.list, { room_id: item.room_id });

      if (roomItem && item.sale !== roomItem.sale) {
        console.log("----> RoomSale roomState.item.sale change", item, roomItem, item.sale, roomItem.sale);

        this.loadData();
      }
    }

    // 정산 시간 변경 시 조회.
    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      console.log("----> RoomSale preferences.day_sale_end_time change", nextProps.preferences.day_sale_end_time);

      // 초기 로딩 || 정산시간 변경 시 로딩
      if (!this.state.today || nextProps.preferences.day_sale_end_time !== this.props.preferences.day_sale_end_time) {
        setTimeout(() => {
          this.loadData();
        }, 1000);
      }
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <div className={cx("loader")}>
          <Wave color="white" size={60} />
        </div>
      );
    } else {
      return (
        <div className="room-count-table">
          <dl className="content">
            <dt className={this.state.formMenuOpen ? "active" : null}>
              <h2>오늘매출</h2>
              <span className="datetime">{this.state.start.format("MM/DD HH:mm") + " ~ " + moment().format("MM/DD HH:mm")}</span>
              <a onClick={() => this._isMounted && this.setState({ formMenuOpen: !this.state.formMenuOpen })} data-toggle="collapse">
                <b className="caret" />
              </a>
            </dt>
            <Collapse in={this.state.formMenuOpen}>
              <dd>
                <div className="table-wrapper">
                  <Table>
                    <thead>
                      <tr>
                        <th>
                          <FormGroup className="form-check">
                            <Checkbox inline checked={this.state.viewOption.show_sale_3 === "1"} onChange={(evt) => this.setOption("show_sale_3", evt.target.checked ? "1" : "0")}>
                              <span />
                              장기
                            </Checkbox>
                          </FormGroup>
                        </th>
                        <th>건수/요금</th>
                        <th>결제 금액</th>
                        <th>합계/미수</th>
                      </tr>
                    </thead>
                    {this.state.today ? (
                      <tbody>
                        <tr>
                          <td>
                            대실
                            {/* <br />
                            <FormGroup className="form-check detail">
                              <Checkbox inline checked={this.state.viewOption.show_sale_2_detail === "1"} onChange={evt => this.setOption("show_sale_2_detail", evt.target.checked ? "1" : "0")}>
                                <span />
                                <div>상세</div>
                              </Checkbox>
                            </FormGroup> */}
                          </td>
                          <td>
                            {this.state.today.rent.count}
                            <p>{String(this.state.today.rent.default_fee + this.state.today.rent.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                          </td>
                          <td>
                            <span className="pay-type txt-cash">현</span>
                            {String(this.state.today.rent.pay_cash_amt + this.state.today.rent.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <p>
                              <span className="pay-type txt-card">카</span>
                              {String(this.state.today.rent.pay_card_amt + this.state.today.rent.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                            {this.state.today.rent.prepay_ota_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-ota">예</span>
                                {String(this.state.today.rent.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                            {this.state.today.rent.pay_point_amt + this.state.today.rent.prepay_point_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-point">포</span>
                                {String(this.state.today.rent.pay_point_amt + this.state.today.rent.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                          </td>
                          <td>
                            {String(
                              this.state.today.rent.pay_cash_amt +
                                this.state.today.rent.prepay_cash_amt +
                                this.state.today.rent.pay_card_amt +
                                this.state.today.rent.prepay_card_amt +
                                this.state.today.rent.pay_point_amt +
                                this.state.today.rent.prepay_point_amt +
                                this.state.today.rent.prepay_ota_amt
                            ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <p className={cx(this.state.today.rent.recept > 0 ? "plus" : this.state.today.rent.recept < 0 ? "minus" : "")}>
                              {(this.state.today.rent.recept > 0 ? "+" : "") + String(this.state.today.rent.recept).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            숙박
                            {/* <br />
                            <FormGroup className="form-check detail">
                              <Checkbox inline checked={this.state.viewOption.show_sale_1_detail === "1"} onChange={evt => this.setOption("show_sale_1_detail", evt.target.checked ? "1" : "0")}>
                                <span />
                                <div>상세</div>
                              </Checkbox>
                            </FormGroup> */}
                          </td>
                          <td>
                            {this.state.today.day.count} <p>{String(this.state.today.day.default_fee + this.state.today.day.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                          </td>
                          <td>
                            <span className="pay-type txt-cash">현</span>
                            {String(this.state.today.day.pay_cash_amt + this.state.today.day.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <p>
                              <span className="pay-type txt-card">카</span>
                              {String(this.state.today.day.pay_card_amt + this.state.today.day.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                            {this.state.today.day.prepay_ota_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-ota">예</span>
                                {String(this.state.today.day.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                            {this.state.today.day.pay_point_amt + this.state.today.day.prepay_point_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-point">포</span>
                                {String(this.state.today.day.pay_point_amt + this.state.today.day.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                          </td>
                          <td>
                            {String(
                              this.state.today.day.pay_cash_amt +
                                this.state.today.day.prepay_cash_amt +
                                this.state.today.day.pay_card_amt +
                                this.state.today.day.prepay_card_amt +
                                this.state.today.day.pay_point_amt +
                                this.state.today.day.prepay_point_amt +
                                this.state.today.day.prepay_ota_amt
                            ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}

                            <p className={cx(this.state.today.day.recept > 0 ? "plus" : this.state.today.day.recept < 0 ? "minus" : "")}>
                              {(this.state.today.day.recept > 0 ? "+" : "") + String(this.state.today.day.recept).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                          </td>
                        </tr>
                        {this.state.viewOption.show_sale_3 === "1" && (
                          <tr>
                            <td>
                              장기
                              {/* <br />
                              <FormGroup className="form-check detail">
                                <Checkbox inline checked={this.state.viewOption.show_sale_3_detail === "1"} onChange={evt => this.setOption("show_sale_3_detail", evt.target.checked ? "1" : "0")}>
                                  <span />
                                  <div>상세</div>
                                </Checkbox>
                              </FormGroup> */}
                            </td>
                            <td>
                              {this.state.today.long.count} <p>{String(this.state.today.long.default_fee + this.state.today.long.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                            </td>

                            <td>
                              <span className="pay-type txt-cash">현</span>
                              {String(this.state.today.long.pay_cash_amt + this.state.today.long.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              <p>
                                <span className="pay-type txt-card">카</span>
                                {String(this.state.today.long.pay_card_amt + this.state.today.long.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                              {this.state.today.long.prepay_ota_amt > 0 ? (
                                <p>
                                  <span className="pay-type txt-ota">예</span>
                                  {String(this.state.today.long.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                </p>
                              ) : null}
                              {this.state.today.long.pay_point_amt + this.state.today.long.prepay_point_amt > 0 ? (
                                <p>
                                  <span className="pay-type txt-point">포</span>
                                  {String(this.state.today.long.pay_point_amt + this.state.today.long.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                </p>
                              ) : null}
                            </td>
                            <td>
                              {String(
                                this.state.today.long.pay_cash_amt +
                                  this.state.today.long.prepay_cash_amt +
                                  this.state.today.long.pay_card_amt +
                                  this.state.today.long.pay_point_amt +
                                  this.state.today.long.prepay_point_amt +
                                  this.state.today.long.prepay_card_amt +
                                  this.state.today.long.prepay_ota_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}

                              <p className={cx(this.state.today.long.recept > 0 ? "plus" : this.state.today.long.recept < 0 ? "minus" : "")}>
                                {(this.state.today.long.recept > 0 ? "+" : "") + String(this.state.today.long.recept).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            </td>
                          </tr>
                        )}
                        <tr>
                          <td>
                            합계
                            {/* <br />
                            <FormGroup className="form-check detail">
                              <Checkbox inline checked={this.state.viewOption.show_sale_sum_detail === "1"} onChange={evt => this.setOption("show_sale_sum_detail", evt.target.checked ? "1" : "0")}>
                                <span />
                                <div>상세</div>
                              </Checkbox>
                            </FormGroup> */}
                          </td>
                          <td>
                            {this.state.today.sum.count}
                            <p>{String(this.state.today.sum.default_fee + this.state.today.sum.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                          </td>
                          <td>
                            <span className="pay-type txt-cash">현</span> {String(this.state.today.sum.pay_cash_amt + this.state.today.sum.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <p>
                              <span className="pay-type txt-card">카</span>
                              {String(this.state.today.sum.pay_card_amt + this.state.today.sum.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                            {this.state.today.sum.prepay_ota_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-ota">예</span>
                                {String(this.state.today.sum.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                            {this.state.today.sum.pay_point_amt + this.state.today.sum.prepay_point_amt > 0 ? (
                              <p>
                                <span className="pay-type txt-point">포</span>
                                {String(this.state.today.sum.pay_point_amt + this.state.today.sum.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </p>
                            ) : null}
                          </td>
                          <td>
                            {String(
                              this.state.today.sum.pay_cash_amt +
                                this.state.today.sum.prepay_cash_amt +
                                this.state.today.sum.pay_card_amt +
                                this.state.today.sum.pay_point_amt +
                                this.state.today.sum.prepay_point_amt +
                                this.state.today.sum.prepay_card_amt +
                                this.state.today.sum.prepay_ota_amt
                            ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}

                            <p className={cx(this.state.today.sum.recept > 0 ? "plus" : this.state.today.sum.recept < 0 ? "minus" : "")}>
                              {(this.state.today.sum.recept > 0 ? "+" : "") + String(this.state.today.sum.recept).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </p>
                          </td>
                        </tr>
                        {/* <tr>
                          <td>매출</td>
                          <td colSpan="3" className="sum">
                            <strong>{String(this.state.today.sum.default_fee + this.state.today.sum.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>미수</td>
                          <td colSpan="3" className={cx(this.state.today.sum.recept > 0 ? "plus" : "minus")}>
                            {(this.state.today.sum.recept > 0 ? "+" : "") + String(this.state.today.sum.recept).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </td>
                        </tr> */}
                      </tbody>
                    ) : (
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    )}
                  </Table>
                </div>
              </dd>
            </Collapse>
          </dl>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const { layout, auth, preferences, placeLock, room, roomView, roomSale, roomState } = state;

  return {
    layout,
    user: auth.user,
    preferences: preferences.item,
    stopLoad: preferences.stopLoad,
    placeLock,
    roomView,
    room,
    roomSale,
    roomState,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initPlaceRoomSale: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSum(placeId, begin, end)),

    initPlaceRoomSaleRoom: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSumRooms(placeId, begin, end)),

    initAllRoomSales: (palceId) => dispatch(roomSaleAction.getNowRoomSales(palceId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomSales);
