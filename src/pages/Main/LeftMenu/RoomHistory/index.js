/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";
import { keyToValue } from "constants/key-map";

// Style.
import { Collapse } from "react-bootstrap";
// import { CubeGrid } from "better-react-spinkit";
import { Form, FormGroup, FormControl, Checkbox, Table } from "react-bootstrap";
import cx from "classnames";
import $ from "jquery";

// Actions.
import { actionCreators as roomStateLogAction } from "actions/roomStateLog";
import { actionCreators as voiceAction } from "actions/voice";

class RoomHistory extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomStateLog: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      days: [],
      history: [],
      isFirstLog: false,
      maxCnt: 500, // 로그 최대 표시 갯수.
      filter: {
        key: true,
        door: true,
        rent: true,
        day: true,
        long: true,
        empty: true,
        clean: true,
        power: false,
        signal: false,
        air_temp: false,
        car_call: false,
        state: true,
        room_id: undefined,
      }, // 필터 적용
      talbeHeight: 30,
      formMenuOpen: true,

      idx: 0,
    };

    this.prevLog = {};

    // 테이블 Reffer.
    this.tableWrap = undefined;

    this.tracks = [];
    this.doors = [];
    this.keys = [];
    this.voiceRoom = 0;
    this.timer = null;
    this.diff = 0;

    this.lastLog = {};
    this.lastUpdate = 0;

    this.idx = 0;

    this._isMounted = false;
  }

  handleSelectRoom = (room_id) => {
    console.log("- handleSelectRoom", room_id);

    this._isMounted &&
      this.setState(
        {
          filter: { ...this.state.filter, room_id: room_id },
        },
        () => this.handleFilter()
      );
  };

  handleFilter = (field) => {
    let filter = _.merge({}, this.state.filter, field || {});

    const { key, door, rent, day, long, empty, state, clean, room_id } = filter;

    console.log("- handleFilter", field, {
      key,
      door,
      rent,
      day,
      long,
      empty,
      state,
      clean,
      room_id,
    });

    let nodes = document.querySelectorAll(".slide");

    if (nodes) {
      _.map(nodes, (node) => {
        if (node.dataset.log) {
          let log = JSON.parse(node.dataset.log);
          let classes = node.classList;

          // console.log("- node", log);

          let { data = {} } = log;

          classes.remove("hide");

          if (room_id && Number(log.room_id) !== Number(room_id)) classes.add("hide");
          else {
            let spans = $(node).find(`span[data-key]`);
            _.map(spans, (span) => {
              span.classList.remove("hide");
              let p = $(span).find(`p`);
              if (p) _.map(p, (s) => s.classList.remove("line-off")); // 위아래 분리선 보이기.
            });

            if ((data.check_in || data.check_out) && ((!rent && Number(data.stay_type) === 2) || (!day && Number(data.stay_type) === 1) || (!long && Number(data.stay_type) === 3))) {
              let span = $(node).find(`span[data-key="check_in"], span[data-key="check_out"]`).filter(`[data-value="${data.stay_type}"]`);
              if (span) _.map(span, (s) => s.classList.add("hide"));
            }

            if (data.door !== null && data.door !== undefined && !door) {
              let span = $(node).find(`span[data-key="door"]`);
              if (span) _.map(span, (s) => s.classList.add("hide"));
            }

            if (data.key !== null && data.key !== undefined && !key) {
              let span = $(node).find(`span[data-key="key"]`);
              if (span) _.map(span, (s) => s.classList.add("hide"));
            }

            if (data.clean !== null && data.clean !== undefined && !clean) {
              let span = $(node).find(`span[data-key="clean"]`);
              if (span) _.map(span, (s) => s.classList.add("hide"));
            }

            if (!state) {
              let span = $(node).find(
                `span[data-key="state"], span[data-key="prev_sale"], span[data-key="outing"], span[data-key="channel"], span[data-key="change_room_id"], span[data-key="signal"]`
              );
              if (span) _.map(span, (s) => s.classList.add("hide"));
            }

            spans = $(node).find("span[data-key]:not(.hide)");
            if (!spans.length) classes.add("hide");
            else if (spans.length === 1) {
              let p = $(spans).find(`p`);
              if (p) _.map(p, (s) => s.classList.add("line-off")); // 위아래 분리선 감추기.
            }
          }
        }
      });
    }
    this._isMounted &&
      this.setState({
        filter,
      });
  };

  makeLog = (log, voice) => {
    const { websocket = false, name = null, room_id, data = {}, reg_date } = log;

    let {
      preferences: { stay_type_color, key_type_color },
    } = this.props;

    const { filter } = this.state;

    let {
      channel = "web",
      key = null,
      door = null,
      sale = null,
      clean = null,
      outing = null,
      check_in = null,
      check_out = null,
      state = null,
      change_room_id = null,
      stay_type = null,
      user_id = "",
      prev_sale = null,
      signal = null,
      air_temp = null,
      main_relay = null,
      car_call = null,
      inspect = null,
      rollback = null,
    } = data;

    // console.log("- makeLog", { channel, websocket, data });

    if (
      stay_type_color &&
      (key !== null ||
        door !== null ||
        sale !== null ||
        clean !== null ||
        outing !== null ||
        check_in !== null ||
        check_out !== null ||
        state !== null ||
        change_room_id !== null ||
        prev_sale !== null ||
        signal !== null ||
        air_temp !== null ||
        main_relay !== null ||
        car_call !== null ||
        inspect !== null ||
        rollback !== null)
    ) {
      key = Number(key || -1);

      let { voice_opt, inspect_use } = this.props.preferences;

      // 음성 지원 기능.
      if (voice) {
        console.log("- voice channel websocket", { channel, websocket, voice_opt, inspect_use, inspect });
        if (channel !== "web" && !websocket) console.log("- 현재 접속 브라우저의 처리는 재생 안함");

        // 현재 접속 브라우저의 처리는 재생 안함(웹소켓에서 받은것만 처리)
        if ((channel !== "web" || websocket) && this.tracks.length) {
          console.log("- voice make start", log);

          if (door !== null && voice_opt["door_" + door]) {
            this.doors.push({
              inx: Number(door) === 1 ? 2 : 4,
              name: "door_" + door,
              text: Number(door) === 1 ? "문이 열렸습니다." : "문이 닫혔습니다.",
            });
          }

          if (
            key > 0 && // NO KEY는 상태가 아니다.
            voice_opt["key_" + key]
          ) {
            this.keys.push({
              inx: 3,
              name: "key_" + key,
              text:
                key === 1
                  ? "고객키가 삽입 되었습니다."
                  : key === 2
                  ? "마스터가 삽입 되었습니다."
                  : key === 3
                  ? "청소키가 삽입 되었습니다."
                  : key === 4
                  ? "고객키가 제거 되었습니다."
                  : key === 5
                  ? "마스터가 제거 되었습니다."
                  : key === 6
                  ? "청소키가 제거 되었습니다."
                  : "",
            });
          }

          if (Number(sale || stay_type) > 0) {
            if (prev_sale !== null) {
              this.tracks.push({
                inx: 5,
                name: "prev_sale_" + prev_sale,
                text: prev_sale === 1 ? "대실로 변경 되었습니다." : "숙박으로 변경 되었습니다.",
              });
            } else if (change_room_id !== null) {
              this.tracks.push({
                inx: 5,
                name: "change_room",
                text: "객실이 이동 되었습니다.",
              });
            }
          }

          // 대실/숙박/장기 (공실은 제외)
          if ((check_in !== null || check_out !== null) && voice_opt["stay_type"]) {
            if (sale !== null || stay_type !== null) {
              this.tracks.push({
                inx: 5,
                name: "sale_" + (sale || stay_type),
                text: keyToValue("room_state", "sale", sale || stay_type),
              });
            }

            if (check_in !== null) {
              this.tracks.push({
                inx: 7,
                name: user_id === "auto" && voice_opt["auto_check_in"] ? "auto_check_in" : "check_in",
                text: user_id === "auto" && voice_opt["auto_check_in"] ? "자동 입실" : "입실",
              });
            } else {
              this.tracks.push({
                inx: 7,
                name: user_id === "auto" && voice_opt["auto_check_out"] ? "auto_check_out" : "check_out",
                text: user_id === "auto" && voice_opt["auto_check_in"] ? "자동 퇴실" : "퇴실",
              });
            }
          }

          // 외출/외출 복귀
          if (outing !== null && voice_opt["outing_" + outing]) {
            this.tracks.push({
              inx: 7,
              name: "outing_" + outing,
              text: keyToValue("room_state", "outing", outing),
            });
          }

          // 청소요청/청소완료
          if (clean !== null && voice_opt["clean_" + clean]) {
            this.tracks.push({
              inx: Number(clean) === 0 ? 6 : 8,
              name: "clean_" + clean,
              text: keyToValue("room_state", "clean", clean),
            });
          }

          // 인스펙트
          if (inspect_use && inspect && voice_opt["inspect_" + inspect]) {
            this.tracks.push({
              inx: 9,
              name: "inspect_" + inspect,
              text: keyToValue("room_state", "inspect", inspect),
            });
          }

          // 메인 전원
          if (main_relay !== null && voice_opt["main_relay_" + main_relay]) {
            this.tracks.push({
              inx: 10,
              name: "main_relay_" + main_relay,
              text: keyToValue("room_state", "main_relay", main_relay),
            });
          }

          // 차량 호출 (입실 시에만 음설 출력)
          if (car_call !== null && voice_opt["car_call_" + car_call]) {
            this.tracks.push({
              inx: 11,
              name: "car_call_" + car_call,
              text: keyToValue("room_state", "car_call", car_call),
            });
          }
        }
      }

      // console.log("- this.tracks length", this.tracks.length);

      let prevRoomLog = this.prevLog[room_id] || data;

      let html = (
        <tr key={this.idx++} className={cx("slide", this.lastLog.room_id && this.lastLog.room_id !== log.room_id ? "new_room_log" : "")} data-log={JSON.stringify(log)}>
          <td>{name}</td>
          <td>
            {filter.power && main_relay !== null && Number(main_relay) === 1 ? (
              <span data-key={"main_relay"} data-value={main_relay} style={{ color: "#2a8d98" }}>
                <p>전원 공급</p>
              </span>
            ) : null}

            {filter.power && main_relay !== null && Number(main_relay) === 0 ? (
              <span data-key={"main_relay"} data-value={main_relay} style={{ color: "#ea3131" }}>
                <p>전원 차단</p>
              </span>
            ) : null}

            {clean !== null && Number(clean) === 1 ? (
              <span data-key={"state"} data-value={clean} style={{ color: "#9370DB" }}>
                <p>
                  청소
                  <span style={{ color: "#9567c5" }}> 요청</span>
                </p>
              </span>
            ) : null}

            {inspect_use && inspect === 1 ? (
              <span data-key={"state"} data-value={inspect} style={{ color: "#12435a" }}>
                <p>
                  점검
                  <span style={{ color: "#9567c5" }}>대기</span>
                </p>
              </span>
            ) : null}

            {sale !== null && !check_in && !check_out ? (
              <span data-key={"state"} data-value={sale} style={{ color: "#999" }}>
                <p> {keyToValue("room_state", "sale", sale)} 상태</p>
              </span>
            ) : null}

            {sale !== null && prev_sale !== null ? (
              <span data-key={"prev_sale"} data-value={prev_sale} style={{ color: "#666" }}>
                <p>
                  <span style={{ color: stay_type_color[Number(prev_sale)] }}>{keyToValue("room_state", "sale", prev_sale)}</span>
                  <span style={{ color: stay_type_color[Number(sale)] }}>{keyToValue("room_state", "sale", sale)}</span>
                </p>
              </span>
            ) : null}

            {check_in !== null && !check_out && state !== null && channel !== null ? (
              <span data-key={"channel"} data-value={channel} style={{ color: "#248baa" }}>
                <p>{keyToValue("room_sale", "channel", channel)} 판매</p>
              </span>
            ) : null}

            {door !== null && Number(door) === 0 ? (
              <span data-key={"door"} data-value={door}>
                <p>{"문 닫】【힘"}</p>
              </span>
            ) : null}

            {check_out !== null ? (
              <span data-key={"check_out"} data-value={stay_type} style={{ color: "#e34430" }}>
                <p>
                  <span style={{ color: stay_type_color[Number(stay_type)] }}>{keyToValue("room_sale", "stay_type", stay_type)}</span> 퇴실
                </p>
              </span>
            ) : null}

            {rollback !== null && rollback === 2 ? (
              <span data-key={"check_out"} data-value={stay_type} style={{ color: "#ec2222" }}>
                <p>
                  <span style={{ color: stay_type_color[Number(stay_type)] }}>{keyToValue("room_sale", "stay_type", stay_type)}</span> 퇴실취소
                </p>
              </span>
            ) : null}

            {outing !== null && Number(outing) === 1 ? (
              <span data-key={"outing"} data-value={outing} style={{ color: stay_type_color[4] }}>
                <p>{"외출"}</p>
              </span>
            ) : null}

            {clean !== null && Number(clean) === 0 ? (
              <span data-key={"state"} data-value={clean} style={{ color: "#9370DB" }}>
                <p>
                  청소
                  <span style={{ color: "#248827" }}> 완료</span>
                </p>
              </span>
            ) : null}

            {inspect_use && inspect === 3 ? (
              <span data-key={"state"} data-value={inspect} style={{ color: "#12435a" }}>
                <p>
                  점검
                  <span style={{ color: "#248827" }}> 완료</span>
                </p>
              </span>
            ) : null}

            {key > 3 ? ( // NO KEY는 상태가 아니므로 안보여준다.
              <span
                data-key={"key"}
                data-value={key}
                style={{
                  color: key_type_color[key] || "#A4A4A4",
                }}
              >
                <p>{key === 4 ? "고객키 △" : key === 5 ? "마스터 △" : key === 6 ? "청소키 △" : "NO KEY"}</p>
              </span>
            ) : null}

            {outing !== null && Number(outing) === 0 ? (
              <span data-key={"outing"} data-value={outing} style={{ color: stay_type_color[4] }}>
                <p>{"외출 복귀"}</p>
              </span>
            ) : null}

            {check_in !== null ? (
              <span data-key={"check_in"} data-value={stay_type} style={{ color: "#1937b6" }}>
                <p>
                  <span style={{ color: stay_type_color[Number(stay_type)] }}>{keyToValue("room_sale", "stay_type", stay_type)}</span> 입실
                </p>
              </span>
            ) : null}

            {rollback !== null && rollback === 1 ? (
              <span data-key={"check_in"} data-value={stay_type} style={{ color: "#ec2222" }}>
                <p>
                  <span style={{ color: stay_type_color[Number(stay_type)] }}>{keyToValue("room_sale", "stay_type", stay_type)}</span> 입실취소
                </p>
              </span>
            ) : null}

            {key === 3 ? (
              <span data-key={"state"} data-value={clean} style={{ color: "#9370DB" }}>
                <p>
                  ★ 청소
                  <span style={{ color: "#ea3131" }}> 중</span> ★
                </p>
              </span>
            ) : null}

            {inspect_use && inspect === 2 ? (
              <span data-key={"state"} data-value={inspect} style={{ color: "#12435a" }}>
                <p>
                  ▶ 점검
                  <span style={{ color: "#ea3131" }}> 중</span> ◀
                </p>
              </span>
            ) : null}

            {key > -1 && key < 4 ? (
              <span
                data-key={"key"}
                data-value={key}
                style={{
                  color: key_type_color[key] || "#A4A4A4",
                }}
              >
                <p>{key === 1 ? "고객키 ▼" : key === 2 ? "마스터 ▼" : key === 3 ? "청소키 ▼" : "NO KEY"}</p>
              </span>
            ) : null}

            {door !== null && Number(door) === 1 ? (
              <span data-key={"door"} data-value={door}>
                <p>{"문 【열림】"}</p>
              </span>
            ) : null}

            {change_room_id !== null ? (
              <span data-key={"change_room_id"} data-value={change_room_id} style={{ color: "#31B404" }}>
                <p>객실 이동</p>
              </span>
            ) : null}

            {filter.signal && signal !== null ? (
              <span data-key={"signal"} data-value={signal} style={{ color: signal === 0 ? "#8fda9a" : "#df2a28" }}>
                <p> {keyToValue("room_state", "signal", signal)}</p>
              </span>
            ) : null}

            {filter.air_temp && air_temp !== null && prevRoomLog.air_temp && Number(prevRoomLog.air_temp) !== Number(air_temp) ? (
              <span
                data-key={"air_temp"}
                data-value={air_temp}
                style={{
                  color: Number(prevRoomLog.air_temp) < Number(air_temp) ? "#e62ced" : "#3c33ed",
                }}
              >
                <p className="air_temp">
                  {prevRoomLog.air_temp}
                  <span>→</span>
                  {air_temp} ℃
                </p>
              </span>
            ) : null}

            {filter.car_call && car_call !== null && Number(car_call) > 0 ? (
              <span data-key={"car_call"} data-value={car_call} style={{ color: Number(car_call) === 1 ? "#518bf9" : Number(car_call) === 2 ? "#1e752b" : "#ec3c3c" }}>
                <p>{keyToValue("room_state", "car_call", car_call)}</p>
              </span>
            ) : null}
          </td>
          <td>{moment(reg_date).format("HH:mm:ss")}</td>
        </tr>
      );

      this.prevLog[room_id] = data;

      return html;
    } else {
      return null;
    }
  };

  // 통신 유실시 업데이트 못한것 적용 .
  loadData = () => {
    const {
      stopLoad,
      user: { place_id },
    } = this.props;

    if (place_id && !stopLoad && this.lastLog) {
      let { id } = this.lastLog;

      console.log("====> reload RoomStateLastLogs id:", id);

      // 마지막 로그 이후 로그 정보 조회.
      this.props.initRoomStateLastLogs(place_id, id);
    }
  };

  componentDidMount() {
    const {
      user: { place_id },
      preferences,
    } = this.props;

    // 초기 로그 조회 place_id 가져오게 delay 준다(Max 300 rows)
    setTimeout(() => {
      this.props.initRoomStateDayLogs(place_id, -1);
    }, 10);

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      if (this.props.leftMenu && this.tableWrap) {
        const leftMenuRect = this.props.leftMenu.getBoundingClientRect();
        const rect = this.tableWrap.getBoundingClientRect();
        let talbeHeight = leftMenuRect.bottom - rect.top - 5;

        // console.log("- talbeHeight", leftMenuRect.bottom , rect.top, leftMenuRect, rect, leftMenuRect.bottom - rect.top);

        if (talbeHeight !== this.state.talbeHeight) this._isMounted && this.setState({ talbeHeight });
      }

      if (this.lastLog && this.lastLog.id) {
        let { reg_date } = this.lastLog;

        var now = moment().format("YYYYMMDDhhmmss");
        var ms = moment().diff(moment(reg_date));
        var sec = ms / 1000;

        // console.log("- last_time", sec, now - this.lastUpdate);

        // 마지막 업데이트 시간이 10 초 지났을때 조회.
        if (now - this.lastUpdate > 30 && sec > 30) {
          this.loadData();
          this.lastUpdate = now;
        }
      }
    }, 1000);

    this._isMounted = true;

    this.loadData();
    this.lastUpdate = moment().format("YYYYMMDDhhmmss");
  }

  componentWillUnmount = () => {
    if (this.timer) clearInterval(this.timer);
    this._isMounted = false;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      let { filter } = this.state;

      // room_log_show_type 옵션 초기화.
      filter = _.merge({}, filter, nextProps.preferences.room_log_show_type);

      console.log("- componentWillReceiveProps filter", filter, nextProps.preferences);

      this.setState({
        filter,
      });
    }

    // 객실 로그 변경.
    if (nextProps.roomStateLog && nextProps.roomStateLog !== this.props.roomStateLog) {
      const { days, item, isDayLog } = nextProps.roomStateLog;
      let { history, isFirstLog } = this.state;

      let { voice } = this.props.preferences;

      console.log("- voice RoomHistory componentWillReceiveProps", { item, days, isDayLog });

      if (isDayLog && !isFirstLog) {
        // 로딩 시 로그 셋팅.
        history = _.map(days, (log, k) => {
          let tr = this.makeLog(log, false);
          return tr;
        });
        if (days[0]) this.lastLog = _.cloneDeep(days[0]);
        this._isMounted &&
          this.setState({
            isFirstLog: true,
          });
      } else if (isDayLog && this.lastLog.id) {
        let last = _.cloneDeep(this.lastLog);

        let lostCnt = days.length > 100 ? 100 : days.length; // 최대 100 개만 검사.

        // console.log("- voice days lost cnt ", lostCnt, ", last.id", last.id);

        if (lostCnt) {
          // 체크할 로그를 잘라서 역순 정렬 한다.
          _.map(days.slice(0, lostCnt).reverse(), (log, k) => {
            // console.log("- voice log.reg_date", log.id);

            // 최종 로그 보다 이후 로그만 추가.
            if (Number(last.id) < Number(log.id)) {
              // console.log("- voice days log id", log.id);

              let tr = this.makeLog(log, false);
              history.splice(0, 0, tr); // 객실 이력 상위에 추가.

              if (log) this.lastLog = _.cloneDeep(log);
            } else {
              return false;
            }
          });
        }
      } else {
        // 로그 추가 및 음성 안내(같은 로그 제외)
        if (item.room_id && item.data && item.reg_date !== this.props.roomStateLog.item.reg_date) {
          item.data = typeof item.data === "object" ? item.data : JSON.parse(item.data);

          const roomItem = _.find(this.props.room.list, { id: Number(item.room_id) });
          const roomState = _.find(this.props.roomState.list, { room_id: Number(item.room_id) });

          if (roomItem) {
            if (!item.name) item.name = roomItem.name; // 객실명.
            item.room_type_id = roomItem.room_type_id;
            item.type_name = roomItem.type_name;
            if (!item.data.stay_type) item.data.stay_type = roomState.sale;

            console.log("- voice data", item.data);

            let voice_type_name_play = false;

            if (voice) {
              // 새문장 생성.
              this.tracks = [];
              this.doors = [];
              this.keys = [];
              this.voiceRoom = item.room_id;

              console.log("- new voice tracks", item.name);

              // 객실 타입 음성 앞에 추가.
              if (this.props.preferences.voice_type_name_play === 1 && this.props.preferences.show_type_name < 2) {
                this.tracks.push({
                  inx: 0,
                  name: "room_type_" + item.room_type_id,
                  text: item.type_name,
                });

                voice_type_name_play = true;
              }

              this.tracks.push({
                inx: this.tracks.length,
                name: "room_" + item.room_id,
                text: item.name + (isNaN(item.name) ? "" : " 호, "),
              });

              // 객실 타입 음성 뒤에 추가.
              if (this.props.preferences.voice_type_name_play === 1 && this.props.preferences.show_type_name === 2) {
                this.tracks.push({
                  inx: this.tracks.length,
                  name: "room_type_" + item.room_type_id,
                  text: item.type_name,
                });

                voice_type_name_play = true;
              }
            }

            // 로그 추가.
            let log = this.makeLog(item, true);

            history.splice(0, 0, log); // 객실 이력 상위에 추가.

            if (voice) {
              console.log("- new voice tracks", this.tracks, "door", this.doors, "key", this.keys);

              // 객실명[&타입명]과 상태만 있으면 상태 제거.
              if (this.tracks.length === 2 && this.tracks[1].name.indexOf("sale_") === 0) {
                this.tracks.splice(1, 1);
              }

              // 도어.
              if (this.doors.length) this.tracks = this.tracks.concat(this.doors);

              if (this.keys.length) this.tracks = this.tracks.concat(this.keys);

              //  console.log("- new voice tracks", this.tracks);

              if ((!voice_type_name_play && this.tracks.length > 1) || (voice_type_name_play && this.tracks.length > 2)) {
                let uniqTrack = _.uniqBy(this.tracks, "inx"); // tracks uniq
                let sortTrack = _.sortBy(uniqTrack, ["inx"]); // 순서 정렬.

                console.log("- new voice tracks put queue ", { uniqTrack, sortTrack });
                this.props.putVoiceQueue(sortTrack); // 음성 지원.
              } else {
                console.log("- ignore voice tracks", item.name);
              }

              this.tracks = [];
              this.doors = [];
              this.keys = [];
              this.voiceRoom = 0;
            }

            if (item) this.lastLog = _.cloneDeep(item);
          }
        }
      }

      // 로그 최대 갯수 넘으면 잘라낸다.
      if (history.length > this.state.maxCnt) history = history.slice(0, this.state.maxCnt - 10);

      console.log("- history count", history.length);

      this._isMounted &&
        this.setState(
          {
            history,
          },
          () => {
            // 필터링 적용.
            this.handleFilter();
          }
        );
    }
  }

  render() {
    return (
      <div className="room-record">
        <Form>
          <dl className="content">
            <dt className={this.state.formMenuOpen ? "active" : null}>
              <h2>객실이력</h2>
              <div>
                <FormControl componentClass="select" placeholder="전체객실" value={this.state.filter.room_id} onChange={(evt) => this.handleSelectRoom(evt.target.value)}>
                  <option value="">전체객실</option>
                  {this.props.room.list.map((room, k) => (
                    <option key={k} value={room.id}>
                      {room.name}
                    </option>
                  ))}
                </FormControl>
              </div>
              <a onClick={() => this._isMounted && this.setState({ formMenuOpen: !this.state.formMenuOpen })} data-toggle="collapse">
                <b className="caret" />
              </a>
            </dt>
            <Collapse in={this.state.formMenuOpen}>
              <dd>
                <FormGroup className="form-check">
                  <Checkbox inline checked={this.state.filter.rent} onChange={(evt) => this.handleFilter({ rent: evt.target.checked })}>
                    <span />
                    대실
                  </Checkbox>
                  <Checkbox inline checked={this.state.filter.day} onChange={(evt) => this.handleFilter({ day: evt.target.checked })}>
                    <span />
                    숙박
                  </Checkbox>
                  <Checkbox inline checked={this.state.filter.long} onChange={(evt) => this.handleFilter({ long: evt.target.checked })}>
                    <span />
                    장기
                  </Checkbox>
                  <Checkbox inline checked={this.state.filter.door} onChange={(evt) => this.handleFilter({ door: evt.target.checked })}>
                    <span />문
                  </Checkbox>
                  <Checkbox inline checked={this.state.filter.key} onChange={(evt) => this.handleFilter({ key: evt.target.checked })}>
                    <span />키
                  </Checkbox>
                  <Checkbox inline checked={this.state.filter.state} onChange={(evt) => this.handleFilter({ state: evt.target.checked })}>
                    <span />
                    객실상태
                  </Checkbox>
                </FormGroup>
                <div className="table-wrapper" ref={(ref) => (this.tableWrap = ref)} style={{ maxHeight: this.state.talbeHeight }}>
                  <Table>
                    <thead>
                      <tr>
                        <th>객실명</th>
                        <th>상태</th>
                        <th>일시</th>
                      </tr>
                    </thead>
                    <tbody>{this.state.history}</tbody>
                  </Table>
                </div>
              </dd>
            </Collapse>
          </dl>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room, roomState, roomStateLog } = state;

  return {
    user: auth.user,
    preferences: preferences.item,
    room,
    roomState,
    roomStateLog,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomStateDayLogs: (placeId, day) => dispatch(roomStateLogAction.getRoomStateDayLogs(placeId, day)),
    initRoomStateLastLogs: (placeId, lastId) => dispatch(roomStateLogAction.getRoomStateLastLogs(placeId, lastId)),
    putVoiceQueue: (tracks) => dispatch(voiceAction.putVoiceQueue(tracks)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomHistory);
