import React, { Component } from "react";
import { connect } from "react-redux";

import Slider from "react-slick";

import AD0 from "../img/creweagle_banner00.jpg";
import AD1 from "../img/creweagle_banner01.jpg";
import AD2 from "../img/creweagle_banner02.jpg";
import AD3 from "../img/creweagle_banner03.jpg";
import AD4 from "../img/creweagle_banner04.jpg";

import AD5 from "../img/creweagle_banner05.jpg";
import AD6 from "../img/creweagle_banner06.jpg";
import AD7 from "../img/creweagle_banner07.png";

class RollingBanner extends Component {
  render() {
    const settings = {
      dots: true,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      lazyLoad: true,
      swipe: false,
      speed: 500,
    };

    return (
      <Slider {...settings}>
        <a href="https://smartstore.naver.com/icrewshop" target="_blank" rel="noopener noreferrer">
          <img src={AD5} alt="배너1" />
        </a>
        <a href="http://www.icrew.kr" target="_blank" rel="noopener noreferrer">
          <img src={AD6} alt="배너2" />
        </a>
        <a href="http://hsoperation.com/" target="_blank" rel="noopener noreferrer">
          <img src={AD7} alt="배너3" />
        </a>
      </Slider>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(RollingBanner);
