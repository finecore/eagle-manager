/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";

// Style.
import { Nav, NavItem } from "react-bootstrap";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";

import Gnb0101 from "../../Setting/Gnb01/Gnb0101";
import Gnb0102 from "../../Setting/Gnb01/Gnb0102";
import Gnb0301 from "../../Setting/Gnb03/Gnb0301";
import CardDeckView from "../../Setting/Gnb03/Gnb0301/SetTab09/CardDeckView";

class QuickMenu extends Component {
  state = {
    devices: [], // 자판기 목록.
    loading: true,
  };

  onSelect = (eventKey) => {
    console.log("- QuickMenu::onSelect", eventKey);

    if (eventKey === 1) {
      this.props.history("/");
    } else if (eventKey === 2) {
      window.open("https://939.co.kr/icrew7001");
    } else if (eventKey === 3) {
      this.openModal(null, Gnb0101);
    } else if (eventKey === 4) {
      this.openModal(null, Gnb0102);
    } else if (eventKey === 5) {
      this.openModal(null, Gnb0301);
    }

    let { left } = this.props.contents;
    const contents = {
      left,
      right: false,
    };

    this.props.handleLayoutContents(contents);
  };

  onSelectDev = (serialno) => {
    console.log("- QuickMenu::onSelectDev", serialno);

    this.props.handleDisplayLayer(CardDeckView, { serialno });

    let { left } = this.props.contents;
    const contents = {
      left,
      right: false,
    };

    this.props.handleLayoutContents(contents);
  };

  openModal = (evt, displayLayer) => {
    this.props.handleDisplayLayer(displayLayer);
  };

  componentDidMount() {
    // 로딩 상태 종료
    this.setState({
      loading: false,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.device && nextProps.device.list.length && nextProps.device.list !== this.props.device.list) {
      const { list } = nextProps.device;

      let devices = _.filter(list, (dev) => dev.type === "02");

      console.log("- devices", devices);

      this.setState({
        devices,
      });
    }
  }

  render() {
    return (
      <div className={`quick-menu ${this.props.contents.right ? "on" : ""}`}>
        <Nav bsStyle="pills" stacked onSelect={(eventKey) => this.onSelect(eventKey)}>
          <NavItem eventKey={1} href="#">
            HOME
          </NavItem>
          <NavItem eventKey={2} href="">
            원격지원
          </NavItem>
          <NavItem eventKey={3} href="#">
            매출조회
          </NavItem>
          {/* <NavItem eventKey={4} href="#">
            객실이력
          </NavItem> */}
          <NavItem eventKey={5} href="#">
            환경설정
          </NavItem>
          {_.map(this.state.devices, (dev, i) => (
            <NavItem key={i} eventKey={6 + i} onSelect={(evt) => this.onSelectDev(dev.serialno)} href="#">
              무인판매 {i > 0 ? i + 1 : ""}
            </NavItem>
          ))}
        </Nav>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { auth, device } = state;

  return {
    user: auth.user,
    device,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer, props) => dispatch(layoutAction.setDisplayLayer(displayLayer, props)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QuickMenu);
