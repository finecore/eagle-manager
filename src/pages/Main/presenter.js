import React from "react";

// layout component.
import Header from "./Header";
import SearchBar from "./SearchBar";
import Footer from "./Footer";
import LeftMenu from "./LeftMenu";
import Contents from "./Contents";
import QuickMenu from "./QuickMenu";

import { withSnackbar } from "notistack";
import CustomizedSnackbars from "components/Notification/CustomizedSnackbars";

import cx from "classnames";

import lock from "../../assets/images/lock/lock1.png";

import { CubeGrid } from "better-react-spinkit";

import "./styles.scss";

const Main = (props) => {
  return (
    <div className="wrapper" onMouseMove={(evt) => props.onMouseMove(evt)}>
      {props.loading && (
        // 로딩중일때 로더 보여주기
        <div className={cx("loader")}>{<CubeGrid color="#eee" size={60} />}</div>
      )}

      <CustomizedSnackbars
        type={props.error.type}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        open={props.error.show}
        onClose={props.handleDismiss}
        autoHideDuration={30000}
        message={props.error.message}
      />

      {/* 화면 잠금 */}
      <div className={cx("view-lock door top", props.isLock && props.lockDelay ? "on" : "")} onClick={(evt) => props.onLockClick()} />
      <img className={cx("view-lock icon", props.isLock && props.lockDelay ? "on" : "")} src={lock} alt="Lock" onClick={(evt) => props.onLockClick()} />
      <div className={cx("main-panel", props.isLock ? "lock" : "")}>
        <div className="content-system">
          <Header />
          <SearchBar />
          <div className="content">
            <div className="container-fluid">
              <div
                className={cx("leftmenu", props.contents.left && "on")}
                style={{
                  minWidth: props.contents.left && props.preferences.layout_width_left,
                  fontSize: props.preferences.font_size_1,
                }}
              >
                {/* 레프트 메뉴 폰트 크기 기준 설정 */}
                <LeftMenu {...props} />
              </div>
              <div className={cx("content-info", props.contents.left && "to-left")}>
                <Contents />
              </div>
              <QuickMenu {...props} />
            </div>
          </div>
          <Footer />
        </div>
      </div>
      <div className={cx("view-lock door bottom", props.isLock && props.lockDelay ? "on" : "")} onClick={(evt) => props.onLockClick()} />
    </div>
  );
};

export default withSnackbar(Main);
