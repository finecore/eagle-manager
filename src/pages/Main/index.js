// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as placeAction } from "actions/place";
import { actionCreators as placeLockAction } from "actions/placeLock";
import { actionCreators as roomInterruptAction } from "actions/roomInterrupt";
import { actionCreators as networkAction } from "actions/network";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as deviceAction } from "actions/device";
import { actionCreators as authAction } from "actions/auth";
import { actionCreators as mailAction } from "actions/mail";
import { actionCreators as roomFeeAction } from "actions/roomFee";
import { actionCreators as roomDoorLockAction } from "actions/roomDoorLock";

import { api, ajax } from "utils/api-util";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { common, place, preferences, layout, auth, placeLock, roomView, network, placeSubscribe, subscribe, roomDoorLock, mail } = state;

  return {
    common,
    place,
    preferences: preferences.item,
    stopLoad: preferences.stopLoad,
    layout,
    contents: layout.contents,
    auth,
    user: auth.user,
    network,
    placeLock,
    roomView,
    placeSubscribe,
    subscribe,
    roomDoorLock,
    mail,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  // api dispatch 설정.
  ajax.dispatch = dispatch;

  return {
    initPlace: (id) => dispatch(placeAction.getPlace(id)),
    initPreferences: (palceId) => dispatch(preferencesAction.getPreferenceList(palceId)),
    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),
    initAllInterrupts: (palceId) => dispatch(roomInterruptAction.getRoomInterruptList(palceId)),
    initAllRoomSales: (palceId) => dispatch(roomSaleAction.getNowRoomSales(palceId)),
    initPlaceRoomSale: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSum(placeId, begin, end)),
    initPlaceRoomSaleRoom: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSumRooms(placeId, begin, end)),
    initAllRoomStates: (palceId) => dispatch(roomStateAction.getRoomStateList(palceId)),
    initAllRoomFees: (placeId, channel) => dispatch(roomFeeAction.getRoomFeeList(placeId, channel)),
    initAllPlaceLock: (palceId) => dispatch(placeLockAction.getPlaceLocList(palceId)),
    initDevices: (place_id) => dispatch(deviceAction.getDeviceList(place_id)),
    handleNewPlaceLock: (place_lock) => dispatch(placeLockAction.newPlaceLock(place_lock)),
    handleLayoutContents: (contents) => dispatch(layoutAction.setLayoutContents(contents)),
    handleDelPlaceLock: (id) => dispatch(placeLockAction.delPlaceLock(id)),
    handleDelSysLock: (place_lock) => dispatch(placeLockAction.setPlaceLock(place_lock)),
    handleDelNetworkInfo: (network) => dispatch(networkAction.delNetWorkInfo(network)),
    handleLogout: () => dispatch(authAction.doLogout()),
    sendMail: (mail) => dispatch(mailAction.newMail(mail)),
    handleLoginQrCode: (query) => dispatch(roomDoorLockAction.loginQrCode(query)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
