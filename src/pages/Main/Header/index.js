// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as placeLockAction } from "actions/placeLock";
import { actionCreators as placeAction } from "actions/place";
import { actionCreators as authAction } from "actions/auth";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, layout, preferences, device, subscribe, placeSubscribe } = state;
  return {
    place,
    auth,
    user: auth.user,
    layout,
    header: layout.header,
    contents: layout.contents,
    preferences: preferences.item || {},
    device,
    subscribe,
    placeSubscribe,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),

    handleNewPlaceLock: (place_lock) => dispatch(placeLockAction.newPlaceLock(place_lock)),

    initPlace: (id) => dispatch(placeAction.getPlace(id)),

    handleLogout: () => dispatch(authAction.doLogout()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
