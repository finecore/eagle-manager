import React from "react";
import { Navbar, Nav, NavDropdown, MenuItem, ButtonToolbar, Button, FormGroup, ControlLabel } from "react-bootstrap";

import { envProps } from "config/configureStore";
import { keyToValue } from "constants/key-map";
import Voice from "./Voice";

import cx from "classnames";

import "./styles.scss";

import tmrLogo from "assets/images/logo_tmr_s.png";
import sindoLogo from "assets/images/logo_sindo_s.jpg";
import icrewLogo from "assets/images/logo_icrew_b.png";

// Components.
import Gnb0101 from "../../Setting/Gnb01/Gnb0101";
import Gnb0102 from "../../Setting/Gnb01/Gnb0102";
import Gnb0103 from "../../Setting/Gnb01/Gnb0103";

// import Gnb0104 from "../../Setting/Gnb01/Gnb0104";
// import Gnb0105 from "../../Setting/Gnb01/Gnb0105";
import Gnb0201 from "../../Setting/Gnb02/Gnb0201";
import Gnb0202 from "../../Setting/Gnb02/Gnb0202";
import Gnb0203 from "../../Setting/Gnb02/Gnb0203";
import Gnb0204 from "../../Setting/Gnb02/Gnb0204";

import Gnb0301 from "../../Setting/Gnb03/Gnb0301";
import Gnb0401 from "../../Setting/Gnb04/Gnb0401";

import Gnb0501 from "../../Setting/Gnb05/Gnb0501";
import Gnb0502 from "../../Setting/Gnb05/Gnb0502";
import Gnb0503 from "../../Setting/Gnb05/Gnb0503";

const Header = (props) => {
  const serviceMode = envProps.SERVICE_MODE === "BETA" ? "BMG" : envProps.SERVICE_MODE === "PROD" ? "PMG" : envProps.SERVICE_MODE === "TEST" ? "TMG" : "LOC";

  const serviceType = props.isProVer ? "Pro" : "Air";

  return (
    <div className="header">
      <Navbar fluid={true}>
        <h1 className="logo">
          {props.idm.maker === "tmr" ? <img src={tmrLogo} alt="더엠알" /> : props.idm.maker === "sindo" ? <img src={sindoLogo} alt="신도EDS" /> : <img src={icrewLogo} alt="아이크루" />}
          <span className="ver">{serviceMode + " " + serviceType + " " + process.env.REACT_APP_VER}</span>
        </h1>
        <span className="place">{props.place.item.name}</span>
        {props.expireDays < props.expireCountDay ? (
          <span className={cx("expire", props.expireDays <= 0 ? "on" : "off")}>
            사용기간 만료 <font color="red">{props.expireDays > 0 ? props.expireDays : props.expireHours > 0 ? props.expireHours : ""}</font>
            {props.expireDays > 0 ? " 일 전" : props.expireHours > 0 ? " 시간 전" : ""}
          </span>
        ) : null}
        <Navbar.Header>
          <Nav>
            <NavDropdown title="조회" id="basic-nav-dropdown">
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0101)}>매출 조회</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0102)}>객실 이력</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0103, "02")}>포인트 이력</MenuItem>
              {/*<MenuItem onClick={evt => props.openModal(evt, Gnb0104)}>RCU이력조회</MenuItem> 
              <MenuItem onClick={evt => props.openModal(evt, Gnb0105)}>
                운영정보조회
              </MenuItem> */}
            </NavDropdown>
            <NavDropdown title="관리" id="tool-nav-dropdown">
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0201)}>객실정보 관리</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0202)}>객실타입 관리</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0203)}>일괄 처리</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0204, "02")}>포인트 관리</MenuItem>
            </NavDropdown>
            <NavDropdown title="설정" id="system-nav-dropdown" onClick={(evt) => props.openModal(evt, Gnb0301)}>
              {/* <MenuItem onClick={evt => props.alertOpen(3)}>CCU리셋</MenuItem> */}
              {/* <MenuItem onClick={evt => props.alertOpen(4)}>
                오프라인 인증
              </MenuItem> */}
              {/* <MenuItem onClick={evt => props.alertOpen(5)}>
                근무자 전환
              </MenuItem> */}
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0301)}>환경설정</MenuItem>
            </NavDropdown>
            <NavDropdown title="예약관리" id="reserve-nav-dropdown" onClick={(evt) => props.openModal(evt, Gnb0401)}>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0401)}>예약관리</MenuItem>
            </NavDropdown>
            <NavDropdown title="게시판" id="reserve-nav-dropdown">
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0501)}>공지 게시판</MenuItem>
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0502)}>업소 게시판</MenuItem>
            </NavDropdown>
            <NavDropdown title="AS/건의" id="reserve-nav-dropdown">
              <MenuItem onClick={(evt) => props.openModal(evt, Gnb0503)}>AS/건의 사항</MenuItem>
            </NavDropdown>
          </Nav>
        </Navbar.Header>
        <Navbar.Collapse>
          <div className="voice">
            <Voice />
          </div>
          <FormGroup className="user">
            <ControlLabel>{props.user.id}</ControlLabel>
            <ControlLabel>[{keyToValue("user", "level", props.user.level)}]</ControlLabel>
          </FormGroup>

          <Navbar.Form pullRight className="btn-layout">
            <ButtonToolbar>
              <Button bsSize="xsmall" className="on left" onClick={(evt) => props.clickLock()}>
                Logout
              </Button>
              <Button bsSize="xsmall" className="right" onClick={(evt) => props.clickLogtout()}>
                Close
              </Button>
            </ButtonToolbar>
          </Navbar.Form>
        </Navbar.Collapse>
      </Navbar>
      {/* 팝업 */}
      {props.layout.displayLayer && <props.layout.displayLayer {...props.layout.props} />}
    </div>
  );
};

export default Header;
