// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import cx from "classnames";
import moment from "moment";

import Swal from "sweetalert2";

import { CheckSubscribe } from "utils/subscribe-util";
import { setSocketServer } from "socket/web/client/websocket";

import crypto from "crypto";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Header from "./presenter";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      expireCheck: false,
      expireCheckInterval: 60,
      expireCountDay: 0, // 유효기간 종료 알림 표시 일 수.
      expireDays: 9999, // 유효기간 만료 일자.
      expireHours: 0,
      isProVer: false,
      idm: {}, // 데몬 장비 정보.
      isc: {}, // 자판기 장비 정보.
      dialog: false,
      confirm: false,
      places: [], // 관리 업소 목록(
    };

    this._isMounted = false;
  }

  static propTypes = {
    place: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    subscribe: PropTypes.object.isRequired,
    placeSubscribe: PropTypes.object.isRequired,
    device: PropTypes.object.isRequired,
  };

  openModal = (evt, displayLayer, subscribeCode) => {
    console.log("- openMoaal", { subscribeCode });

    // 마일리지 포인트 서비스 구독여부.
    if (subscribeCode && !CheckSubscribe(this.props, subscribeCode)) {
      return;
    }

    this.props.handleDisplayLayer(displayLayer);
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  clickLock = (value) => {
    if (!value) {
      Swal.fire({
        icon: "info",
        title: "잠금 알림",
        html: "화면을 잠그려면 로그인 비밀번호를 입력 하세요",
        input: "password",
        inputPlaceholder: "로그인 비밀번호를 입력해 주세요",
        inputAttributes: {
          maxlength: 20,
        },
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
      }).then((result) => {
        if (result.value) {
          this.clickLock(result.value, true);
        }
      });
    } else {
      const {
        user: { id, pwd, place_id },
      } = this.props;

      // 해시 생성
      var shasum = crypto.createHash("sha1");
      shasum.update(value);

      // 비밀번호 sha1 형식으로 암호화.
      var pwdHex = shasum.digest("hex");
      console.log("- pwd sha1 ", pwdHex, pwd);

      if (pwdHex === pwd) {
        // 잠금 설정(하위 권한 잠금은 어드민에서만 가능 하다)
        const place_lock = { place_id, user_id: id, type: 0, auto_lock: 0 };
        this.props.handleNewPlaceLock({ place_lock });
      } else {
        this._isMounted &&
          Swal.fire({
            icon: "warning",
            title: "비밀번호 오류",
            html: "로그인 비밀번호가 올바르지 않습니다.<br/><br/>다시 입력해 주세요.",
            input: "password",
            inputPlaceholder: "로그인 비밀번호를 입력해 주세요",
            inputAttributes: {
              maxlength: 20,
            },
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonText: "확인",
            cancelButtonText: "취소",
          }).then((result) => {
            if (result.value) {
              this.clickLock(result.value);
            }
          });
      }
    }
  };

  clickLogtout = (confirm) => {
    if (!confirm) {
      this._isMounted &&
        Swal.fire({
          icon: "info",
          title: "로그아웃 알림",
          html: "로그 아웃 하시겠습니까?",
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonText: "로그아웃",
          cancelButtonText: "취소",
        }).then((result) => {
          if (result.value) {
            this.clickLogtout(true);
          }
        });
    } else {
      this.props.handleLogout();
    }
  };

  // 사용 유효 기간 조회.
  checkExpire = () => {
    const {
      place: {
        item: { id },
      },
      preferences: { expire_check_interval: expireCheckInterval },
    } = this.props;

    console.log("- checkExpire", id, expireCheckInterval);

    if (id) {
      this._isMounted &&
        this.setState(
          {
            expireCheck: true,
          },
          () => {
            this.props.initPlace(id);
          }
        );
    }

    // 유효기간 반복 조회.
    setTimeout(
      () => {
        this.checkExpire();
      },
      id && expireCheckInterval ? expireCheckInterval * 60 * 1000 : 60 * 1000
    );
  };

  componentDidMount = () => {
    this._isMounted = true;

    this.checkExpire();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.preferences && nextProps.preferences !== this.props.preferences && nextProps.preferences.expire_pre_alert_day !== this.props.preferences.expire_pre_alert_day) {
      const { expire_pre_alert_day, svc_mode } = nextProps.preferences;

      // 프로버전
      let isProVer = svc_mode === 1; // 서비스 모드 (0: Air, 1: Pro)

      console.log("- isProVer", isProVer);

      this._isMounted &&
        this.setState({
          expireCountDay: expire_pre_alert_day,
          isProVer,
        });
    }

    if (nextProps.place && nextProps.place !== this.props.place && nextProps.place.item !== this.props.place.item) {
      const {
        item: { expire_date },
      } = nextProps.place;

      let { expireCheck, expireCountDay } = this.state;

      // 유효기간 체크 조회 시만 보여준다.
      if (expire_date && expireCountDay && expireCheck) {
        let ed = moment(expire_date);
        let nd = moment();

        let expireMiss = ed.diff(nd);
        let expireDays = ed.diff(nd, "days");
        let expireHours = ed.diff(nd, "hours");

        console.log("- componentWillReceiveProps expire_date", ed.format("YYYYMMDDHHmm"), nd.format("YYYYMMDDHHmm"), expireMiss, {
          expireDays,
          expireHours,
          expireCountDay,
        });

        // 유료기간 만료!
        if (expireMiss < 0) {
          this._isMounted &&
            Swal.fire({
              icon: "warning",
              title: "크루이글 유효기간 만료!",
              html: "크루이글의 유효기간이 만료 되었습니다. <br/><br/>유효기간 연장 후 사용 하세요.<br/><br/>고객센터 1600 - 5356",
              confirmButtonText: "확인",
            }).then((result) => {
              if (result.value) {
                this.props.handleLogout();
              }
            });
        }
        // 유효기간 만료 n 일전 알림 띄움.
        else if (expireDays <= expireCountDay) {
          this._isMounted &&
            Swal.fire({
              icon: "info",
              title: "크루이글 유효기간 만료 안내",
              html: `사용이 제한 될 수 있는 유효기간 만료가 <font color='red'>${expireDays > 0 ? expireDays : expireHours}</font>${
                expireDays > 0 ? "일" : "시간"
              } 남았습니다. <br/><br/>유효기간 만료 시 크루이글 일부 기능과 AS 지원이 제한 됩니다.<br/>만료 전 유효기간을 연장해 주세요.<br/><br/>고객센터 1600 - 5356`,
              confirmButtonText: "확인",
            });
        }

        this._isMounted &&
          this.setState({
            expireDays,
            expireHours,
            expireCheck: false,
          });
      }
    }

    if (nextProps.device && nextProps.device !== this.props.device) {
      const { idm, isc } = nextProps.device;

      // IDM 소켓 서버 접속 변경 시 websocket 도 변경.
      if (idm && idm.connect === 0 && idm.server_ip !== this.state.idm.server_ip) {
        console.log("- websocket reconnect server_ip for idm server_ip", idm.server_ip, this.state.idm.server_ip);
        if (setSocketServer) {
          setSocketServer(idm.server_ip); // 서버 IP 변경. 웹소켓 재연결..
        }
      }

      if (idm !== this.state.idm || isc !== this.state.isc) {
        this._isMounted &&
          this.setState({
            idm,
            isc,
          });
      }
    }

    if (nextProps.placeSubscribe && nextProps.placeSubscribe !== this.props.placeSubscribe) {
      // 프로버전 : 무인 판매 메일링 서비스, 마일리지 포인트 서비스, 무인 셀프 인증 서비스, OTA 자동 예약 서비스
      // ===> 직접 설정 하는 방식으로 변경.
      // const isProVer = CheckServiceGroup(nextProps, ["01", "02", "03", "11"]);
      // console.log("- CheckServiceGroup isProVer", isProVer);
      // this.setState({
      //   isProVer,
      // });
    }
  }

  render() {
    return this.props.preferences ? (
      <Header {...this.props} {...this.state} clickLock={this.clickLock} clickLogtout={this.clickLogtout} openModal={this.openModal} closeModal={this.closeModal} />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
