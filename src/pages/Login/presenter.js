// imports.
import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";

import { FormControl } from "react-bootstrap";

import _ from "lodash";

// Components.
import renderField from "components/FormInputs/renderField";

// field validate.
const validate = (values) => {
  const errors = {};
  if (!values.id) {
    errors.id = "아이디를 입력해 주세요.";
  }

  if (!values.pwd) {
    errors.pwd = "비밀번호를 입력해 주세요.";
  } else if (values.pwd.length < 5) {
    errors.pwd = "비밀번호는 5자리 이상 입니다.";
  }
  return errors;
};

// local data
let data = {
  id: localStorage.getItem("loginId"),
  pwd: "",
};

let Login = (props) => {
  // onSubmit <- handleSubmit 연결.
  const { handleSubmit, /*load, pristine, reset, */ submitting, error } = props;

  return (
    <div className="card">
      <div className="header">
        <h4>직원 로그인</h4>
      </div>
      <div className="content">
        <form className="form-horizontal" onSubmit={handleSubmit}>
          {props.auth.isIcrewViewer ? (
            <div className="form-group form-radius">
              <label className="col-md-3 control-label">업소 선택</label>
              <div className="col-md-9">
                {/* 아이크루 뷰어는 모든 업소 조회 가능 */}
                <FormControl componentClass="select" placeholder="업소 목록" value={props.place.item.id || ""} onChange={(evt) => props.onChangePlace(Number(evt.target.value || 0))}>
                  <option value="">업소 선택</option>
                  {props.place.list &&
                    _.map(props.place.list, (item, i) => (
                      <option key={i} value={item.id}>
                        {item.name}
                      </option>
                    ))}
                </FormControl>
              </div>
            </div>
          ) : null}
          <div className="form-group form-radius">
            <label className="col-md-3 control-label">아이디</label>
            <div className="col-md-9">
              <Field name="id" type="text" placeholder="관리자" component={renderField} label="ID" />
            </div>
          </div>
          <div className="form-group form-radius form-pw">
            <label className="col-md-3 control-label">비밀번호</label>
            <div className="col-md-9">
              <Field name="pwd" type="password" placeholder="password" component={renderField} label="Password" />
            </div>
          </div>
          {/*<div className="form-group">
          <label className="col-md-3"></label>
          <div className="col-md-9">
            <Field
              name="rememberMe"
              type="checkbox"
              component={renderField}
              label="Remember Me"
              />
          </div>
        </div>*/}
          {error && <strong>{error}</strong>}
          <div className="form-group">
            <button type="submit" disabled={submitting} className="btn btn-fill btn-info">
              LOG IN
            </button>
            {/* <button
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button> */}
          </div>
        </form>
        <div className="callcenter">Call Center 1600-5356</div>
        <div className="copyright">Copyrightⓒ 2020 All rights reserved by icrewcompany.</div>
      </div>
    </div>
  );
};

Login = reduxForm({
  form: "login",
  validate,
})(Login);

Login = connect((state) => ({
  initialValues: data, // pull initial values
}))(Login);

export default Login;
