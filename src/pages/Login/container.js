// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { sign } from "utils/jwt-util";

import _ from "lodash";

// Component.
import Login from "./presenter";

// Styles.
import "./login.scss";

import logoIcrew from "../../assets/images/logo_icrew_b.png";
// import logoTmr from "../../assets/images/logo_tmr_b.png";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  };

  submitForm = (values) => {
    const { isIcrewViewer, user, token } = this.props.auth;

    console.log("- submitForm", isIcrewViewer);

    // 로딩..
    this.setState({
      loading: true,
    });

    if (!isIcrewViewer) this.props.handleLogin(values);
    else this.props.setLogin(user, token, true);

    // 로딩 상태 종료
    this.setState({
      loading: false,
    });

    return false;
  };

  onChangePlace = (id) => {
    console.log("- onChangePlace", id);

    if (id) this.props.initPlaceUsers(id);
  };

  componentDidMount = () => {
    // 로딩 상태 종료
    this.setState({
      loading: false,
    });

    this.props.closeWebsocket();
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.auth && nextProps.auth !== this.props.auth) {
      const { user } = nextProps.auth;

      console.log("- login auth user", user);

      let filter = "1=1";
      this.props.initAllPlace(filter);
    }

    if (nextProps.place && nextProps.place !== this.props.place) {
      const { list } = nextProps.place;
      if (list) console.log("- login place list", list.length);
    }

    if (nextProps.user && nextProps.user !== this.props.user) {
      const { list } = nextProps.user;
      const {
        user: { type, uuid },
      } = this.props.auth;

      console.log("- login user list", list);

      // 관리자는 해당 업소 라이선스 없이 로그인.
      let user = (_.filter(list, (item) => item.level < 2) || [])[0];

      if (type === 1 && user) {
        // JWT(Json Web Token) 생성.
        const { channel = "web", id, place_id, level, type } = user;

        const payload = {
          channel,
          id,
          place_id,
          level,
          type,
          uuid,
          admin: "Y",
        };

        console.log("- login payload", payload);

        user.admin = "Y"; // 라이선스 검증 패스용 플래그

        sign(payload, (err, token) => {
          this.props.setLogin(user, token, true);
        });
      }
    }
  }
  render() {
    if (this.state.loading) {
      // 로딩중일때 로더 보여주기
      return <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>;
    }

    return (
      <div className="wrapper">
        <div className="main-panel">
          <div className="content">
            <div className="container-fluid">
              <div className="col-md-6">
                <h3>
                  <img src={logoIcrew} alt="로고" />
                </h3>
                <Login {...this.props} {...this.state} onSubmit={this.submitForm} onChangePlace={this.onChangePlace} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Container;
