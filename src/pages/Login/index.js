// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as authAction } from "actions/auth";
import { actionCreators as userAction } from "actions/user";
import { actionCreators as placeAction } from "actions/place";
import { actionCreators as wsAction } from "actions/ws";

// Component.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, error, place, user } = state;
  return {
    auth,
    place,
    user,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  handleLogin: (values) => dispatch(authAction.doLogin(values)),

  setLogin: (user, token, isViewerLogin) => dispatch(authAction.succssLogin(user, token, isViewerLogin)),

  closeWebsocket: () => dispatch(wsAction.close()),

  initAllPlace: (filter) => dispatch(placeAction.getPlaceList(filter)),

  initPlaceUsers: (palceId) => dispatch(userAction.getPlaceUser(palceId)),

  setPlace: (id) => dispatch(placeAction.setPlace(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
