import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Container from "./container";

// Actions.
import { actionCreators as wsAction } from "actions/ws";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as authAction } from "actions/auth";
import { actionCreators as errorAction } from "actions/error";
import { actionCreators as mailAction } from "actions/mail";

const mapStateToProps = (state, ownProps) => {
  const {
    preferences,
    auth,
    router: { location },
    error,
    ws,
  } = state;

  return {
    preferences,
    auth,
    pathname: location.pathname,
    error,
    ws,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleJoin: (user) => dispatch(wsAction.join(user, true)),
    handleLogout: () => dispatch(authAction.doLogout()),
    handleClearError: () => dispatch(errorAction.clearError()),

    succssLogin: (user, token) => dispatch(authAction.succssLogin(user, token)),
    tempLogin: (user, token) => dispatch(authAction.tempLogin(user, token)),

    sendMail: (mail) => dispatch(mailAction.newMail(mail)),
    setStopReload: (stop) => dispatch(preferencesAction.setStopReload(stop)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
