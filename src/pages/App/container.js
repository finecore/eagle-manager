import React, { Component } from "react";
import PropTypes from "prop-types";

import { withRouter } from "react-router-dom";
import { envProps } from "config/configureStore";
import { sign } from "utils/jwt-util";

import Swal from "sweetalert2";

import App from "./presenter";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      tcpServer: null,
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
    };
  }

  static propTypes = {
    preferences: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    ws: PropTypes.object.isRequired,
  };

  componentDidMount = () => {
    const { auth, pathname } = this.props;

    let { isLogined, user, token } = auth;

    let { id, place_id, level } = user || {};

    console.log("-- pathname ", pathname);
    console.log("-- login user ", { id, place_id, level });

    const mode =
      envProps.SERVICE_MODE === "BETA"
        ? " == BETA =="
        : envProps.SERVICE_MODE === "PROD"
        ? "Manager" + (process.env.REACT_APP_SVR_NAME === "BACKUP" ? " 2" : "")
        : envProps.SERVICE_MODE === "TEST"
        ? " == TEST =="
        : " == LOCAL ==";

    document.title = `iCrew Eagel ${mode}`;

    if (token && place_id) {
      if (pathname.indexOf("/QR/") === 0) {
        this.props.history.push("/");
      } else if (pathname !== "/Logout") {
        // user 값을 web socket server session 에 설정 한다.(CCU, FSC 와 통신시 웹매니저와 연동을 위한 키는 place_id 이다.)
        this.props.handleJoin(user);
      } else {
        if (sessionStorage.getItem("token")) {
          sessionStorage.removeItem("token");
        }
      }
    } else {
      if (pathname.indexOf("/QR/") === 0) {
        const user = { channel: "web", id: "qrcode", place_id: 0, level: 0, type: 0 };
        sign(user, (err, token) => {
          if (token) {
            this.props.tempLogin(user, token);
          }
        });
      } else {
        this.props.history.push("/");
      }
    }
  };

  onConfirmError = (confirm, value) => {
    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(value);

          // 확인 상태 초기화.
          setTimeout(() => {
            this.setState({
              confirm: false,
            });
          }, 100);
        }
      }
    );
  };

  customerMsg = (code, message) => {
    if (code === "ER_DUP_ENTRY") {
      message = message.indexOf("reserv_num") > -1 ? "이미 있는 예약번호 입니다." : "입력 항목 중복 오류 입니다.";
    } else {
      message = "요청 처리중 오류가 발생 했습니다.";
    }

    return message;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.auth && nextProps.auth !== this.props.auth) {
      const { isLogined } = nextProps.auth;

      // 로그인/로그 아웃 시 리로드 시작/중지
      this.props.setStopReload(!isLogined);
    }

    if (nextProps.error && nextProps.error !== this.props.error) {
      // let { code, message, detail, logout } = nextProps.error;
      // console.log(" App::error --->", { code, message, detail });
      // if (message && message.indexOf("Network") === -1 && message.indexOf("/mail/") === -1) {
      //   if (envProps.SERVICE_MODE === "LOCAL") {
      //     if (detail) {
      //       const { sqlMessage = undefined, sql = undefined, ...arg } = detail;
      //       if (sqlMessage) detail = sqlMessage + "<br/>" + sql + " " + arg;
      //       message += "<br/><br/>" + detail;
      //     }
      //   } else {
      //     message = this.customerMsg(code, message);
      //   }
      //   message = "[" + code + "] " + message;
      //   // 기존 알림 창 닫힐 시간 준다.
      //   setTimeout(() => {
      //     Swal.fire({
      //       icon: "error",
      //       title: "처리 오류",
      //       html: message,
      //       showCloseButton: true,
      //       confirmButtonText: "확인",
      //     }).then((result) => {
      //       // 인증 오류는 로그아웃 시킨다.
      //       if ((code === 401 && !envProps.IS_LOCAL) || logout) {
      //         this.props.handleClearError();
      //       }
      //       this.setState({
      //         error: null,
      //       });
      //     });
      //   }, 500);
      // }
    }
  }

  render() {
    return <App {...this.props} {...this.state} onConfirmError={this.onConfirmError} />;
  }
}

export default withRouter(Container);
