import React from "react";
import PropTypes from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";
import $ from "jquery";

import "./styles.scss";

/**
 * Pages
 */
import Login from "pages/Login";
import Main from "pages/Main";
import QRReserv from "pages/QR/Reserv";
import QRKey from "pages/QR/Key";
//import NotLogin from "pages/Error/NotLogin";
import NotFound from "pages/Error/NotFound";

const App = (props) => {
  const {
    preferences: {
      item: { font_size = 14 },
    },
    auth,
    pathname,
  } = props;

  let { isLogined, isTempLogined, user, token } = auth;

  let { id, place_id, level } = user || {};

  let isQrCode = pathname.indexOf("/QR/") === 0;

  // device detection
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

  // console.log("- App Route ", { isLogined, isTempLogined, isQrCode });

  // 글자 크기 설정.
  $("html,body").css("font-size", font_size + "px");

  return <div>{isLogined ? <Private key={1} {...props} /> : isMobile && isTempLogined ? <PublicQr key={2} {...props} /> : <Public key={3} {...props} />}</div>;
};

const Private = (props) => {
  return (
    <Switch>
      <Route path="/" component={Main} />
      <Route component={NotFound} />
    </Switch>
  );
};

const Public = (props) => (
  <Switch>
    <Route path="/" component={Login} />
    <Route component={NotFound} />
  </Switch>
);

const PublicQr = (props) => (
  <Switch>
    <Route path="/QR/key/:room_id" component={QRKey} />
    <Route path="/QR/reserv/:reserv_id" component={QRReserv} />
    <Route path="/Login" component={Login} />
    <Route component={NotFound} />
  </Switch>
);

export default App;
