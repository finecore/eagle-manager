// imports.
import React from "react";
import cx from "classnames";

// Components.
import BasicCountSet from "./BasicCountSet";
import IscSaleTimeSet from "./IscSaleTimeSet";

import { Checkbox, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";

import format from "utils/format-util";

// styles.
import "./styles.scss";

const SetTab01 = (props) => {
  return (
    <div>
      {/* 요금 설정 팝업 */}
      {props.displayFeeLayer && <BasicCountSet {...props} />}
      {props.displayIscLayer && <IscSaleTimeSet {...props} />}

      <div className="setting-form" id="gnb030101">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>요금 변경 시 금액 단위 설정</h2>
              <div className="form-content">
                대실
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={props.modify.rent_fee_unit}
                    step={5000}
                    min={1000}
                    max={10000}
                    onChange={(value) => props.handleInputChange("rent_fee_unit", value === 6000 ? 5000 : value)}
                    formatter={(value) => format.toMoney(value)}
                  />
                </span>
                원 <br />
                숙박
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={props.modify.stay_fee_unit}
                    step={5000}
                    min={1000}
                    max={10000}
                    onChange={(value) => props.handleInputChange("stay_fee_unit", value === 6000 ? 5000 : value)}
                    formatter={(value) => format.toMoney(value)}
                  />
                </span>
                원
              </div>
            </li>
            <li>
              <h2>객실 추가/할인 요금 설정</h2>
              <div className="form-content clearfix">
                - 타입별/요일별/시간별 추가 요금 설정
                <ButtonToolbar className="btn-room">
                  <Button onClick={(evt) => props.openFeeLayer(evt)}>설정하기</Button>
                  {/* <Button onClick={evt => props.openFeeLayer(evt)}>특정일 요금설정</Button> */}
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <h2>무인 판매가능 시간 및 요금 설정</h2>
              <div className="form-content clearfix">
                - 타입별/요일별/시간별 판매 가능 시간 및 요금 설정
                <ButtonToolbar className="btn-room">
                  <Button onClick={(evt) => props.openIscLayer(evt)}>설정하기</Button>
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <h2>체크인 기본 요금 타입 설정</h2>
              <div className="form-content clearfix">
                <ButtonGroup aria-label="default_pay_type" onChange={(evt) => props.handleInputChange("default_pay_type", evt.target.value)}>
                  <Radio inline name="default_pay_type" value={1} defaultChecked={props.modify.default_pay_type === 1} className={cx(props.modify.default_pay_type === 1 && "checked-on")}>
                    <span />
                    현금
                  </Radio>
                  <Radio inline name="default_pay_type" value={2} defaultChecked={props.modify.default_pay_type === 2} className={cx(props.modify.default_pay_type === 2 && "checked-on")}>
                    <span />
                    카드
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2>기타 설정</h2>
              <div className="form-content clearfix">
                <Checkbox inline className="checkbox04" defaultChecked disabled={true}>
                  <span />
                  연박 시 해당 일/시간의 요금 적용함
                </Checkbox>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          {/* 매니저 권한 이상만 수정 가능.  */}
          <Button onClick={(evt) => props.onSave()} disabled={props.user.level > 2}>
            저장
          </Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab01;
