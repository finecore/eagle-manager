// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

import { Wave } from "better-react-spinkit";
import cx from "classnames";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import BasicCountSet from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: [], // 설정 요일
      times: [], // 설정 시간
      org: [], // 원본 데이터.
      data: [], // 데이터.
      save: { del: [], add: [], mod: [] }, // 삭제 추가 수정 데이터.
      fees_org: [], // 원본 요금 정보.
      fees: [], // 요금 정보.
      add_fee: [], // 설정요금.
      room_type_id: 0,
      channel: 0, // 적용 채널 (0: 카운터, 1:자판기)
      importRoomTypeId: 0, // 가져올 객실타입.
      timeStart: {},
      timeEnd: {},
      timeAdd: undefined,
      timeDel: undefined,
      timeSel: undefined,
      selDay: [], // 요일 선택.
      selCopyDay: 0, // 복제할 요일.
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
      loading: false,
    };
  }

  isMouseDown = false;

  static propTypes = {
    user: PropTypes.object.isRequired,
    roomType: PropTypes.array.isRequired,
    roomFee: PropTypes.array.isRequired,
    seasonPremiums: PropTypes.array.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  // 객실 타입 변경.
  onRoomTypeSelect = (room_type_id) => {
    console.log("-- onSelect", room_type_id);

    let { fees, days, times, add_fee } = this.state;

    const fee = _.filter(fees, (v) => v.room_type_id === room_type_id);

    // 요일에 대실/숙박 타임테이블 초기화.
    let data = _.map(days, (day) => {
      add_fee[day - 1] = [0, 0];
      return [_.cloneDeep(times), _.cloneDeep(times)];
    });

    _.map(fee, (f, k) => {
      const idx = k + 1;
      const times = data[f.day - 1][f.stay_type - 1]; // 배열은 0번부터 채운다.

      _.map(times, (t, i) => {
        if (Number(t.hhmm) >= Number(f.begin) && Number(t.hhmm) < Number(f.end)) {
          t.idx = idx;
          t.fee = f;
        }
      });
    });

    console.log("-- data", data);

    this.setState({
      data,
      org: _.cloneDeep(data),
      room_type_id,
      loading: true,
      add_fee,
      timeDel: undefined,
      timeAdd: undefined,
      timeSel: undefined,
    });
  };

  onTimeMouseDown = (evt, day, stay_type, time, t) => {
    console.log("--- onTimeMouseDown ", day, stay_type, time, t);
    let { fees, data, timeDel, timeAdd, timeSel, add_fee, room_type_id } = this.state;

    const fee = _.filter(fees, (v) => v.room_type_id === room_type_id);

    // 요금 구간 초기화.
    _.map(fee, (f, k) => {
      const times = data[f.day - 1][f.stay_type - 1]; // 배열은 0번부터 채운다.
      add_fee[f.day - 1][f.stay_type - 1] = 0;

      _.map(times, (t, i) => {
        t.sel = "";
      });
    });

    // 추가 요금 적용.
    if (t.fee) add_fee[day - 1][stay_type - 1] = t.fee.add_fee;
    else add_fee[day - 1][stay_type - 1] = 0;

    timeSel = undefined;

    if (!this.isMouseDown && (timeAdd || timeDel)) {
      let idx = -1;

      if (timeDel && timeDel.day === day && timeDel.stay_type === stay_type) {
        idx = 0; // 삭제
      } else if (timeAdd && timeAdd.day === day && timeAdd.stay_type === stay_type) {
        idx = 25; // 추가
      }

      console.log("--- timeAdd ", timeAdd, timeDel, idx);

      if (idx > -1) {
        // 선택 요일/타입/시작시간 (idx 가 0 이상이면 add_fee 적용 됨.)
        data[day - 1][stay_type - 1][time].idx = idx;

        this.setState(
          {
            data,
            timeStart: { day, stay_type, time, idx, t },
          },
          () => (this.isMouseDown = true)
        );
      } else {
        this.setState({
          timeDel: undefined,
          timeAdd: undefined,
          timeSel: undefined,
        });
      }
    } else {
      const fee = t.fee;
      let times = data[day - 1][stay_type - 1]; // 배열은 0번부터 채운다.

      // 선택한 요금 시간 구간 설정.
      _.map(times, (t, i) => {
        if (fee && Number(t.hhmm) >= Number(fee.begin) && Number(t.hhmm) < Number(fee.end)) {
          if (Number(t.hhmm) === Number(fee.begin)) t.sel = "sel-first";
          else if (Number(t.hhmm) === Number(fee.end) - (fee.end.substring(2) === "00" ? 70 : 30)) t.sel = "sel-last";
          else t.sel = "sel";

          timeSel = t.fee;
        }
      });

      this.setState({
        data,
      });
    }

    this.setState({
      add_fee,
      timeSel,
    });
  };

  onTimeMouseEnter = (evt, day, stay_type, time) => {
    if (this.isMouseDown) {
      let { data, org, timeStart } = this.state;

      //console.log("--- onTimeMouseEnter ", day, stay_type, time, timeStart);

      // 같은요일/타입 일때..
      if (day === timeStart.day && stay_type === timeStart.stay_type) {
        const start = time > timeStart.time ? timeStart.time : time;
        const end = time > timeStart.time ? time : timeStart.time;

        // 마우스가 건너뛴 공간 있다면 채워준다.
        _.map(org[timeStart.day - 1][timeStart.stay_type - 1], (t, i) => {
          data[timeStart.day - 1][timeStart.stay_type - 1][i].idx = start <= i && end >= i ? timeStart.idx : t.idx;
        });

        this.setState({
          data,
          timeEnd: { time },
        });
      }
    }
  };

  onTimeMouseUp = (evt) => {
    if (this.isMouseDown) {
      let {
        room_type_id,
        channel,
        fees_org,
        fees,
        data,
        timeStart,
        timeAdd,
        save: { del, add, mod },
      } = this.state;

      console.log("--- onTimeMouseUp ", timeStart, timeAdd);

      let times = data[timeStart.day - 1][timeStart.stay_type - 1];

      // 현재 변경 요금.
      let type_fee = _.filter(fees, (v) => v.room_type_id === room_type_id);

      // 변경 요일/숙박형태 제외 목록 반환.
      let day_stay_fee = _.filter(type_fee, (f, k) => f.day !== timeStart.day || (f.day === timeStart.day && f.stay_type !== timeStart.stay_type));

      let newFee = [];

      let idx = 0;
      let newIdx = 0;

      // 요금 구간 재 정렬.
      _.map(times, (t, i) => {
        // time end.
        if (newFee[newIdx] && t.idx !== idx) {
          newFee[newIdx].end = t.hhmm;
          ++newIdx;
        }

        // time begin.
        if (!newFee[newIdx] && t.idx > 0) {
          newFee[newIdx] = {
            room_type_id,
            channel,
            day: timeStart.day,
            stay_type: timeStart.stay_type,
            begin: t.hhmm,
            add_fee: t.fee ? t.fee.add_fee : 0,
          };
          idx = t.idx;
        }
      });

      console.log("--- newFee ", newFee);

      // 초기화.
      _.map(times, (t, i) => {
        t.fee = undefined;
      });

      // 변경 요금 적용.
      _.map(newFee, (f, k) => {
        const idx = k + 1;
        const times = data[f.day - 1][f.stay_type - 1]; // 배열은 0번부터 채운다.

        _.map(times, (t, i) => {
          if (Number(t.hhmm) >= Number(f.begin) && Number(t.hhmm) < Number(f.end)) {
            t.idx = t.idx || idx; // 기존 항목 색상 유지.
            t.fee = f;
          }
        });
      });

      day_stay_fee = day_stay_fee.concat([...newFee]);

      fees = _.filter(fees, (v) => v.room_type_id !== room_type_id).concat([...day_stay_fee]);

      // 삭제된 시간 요금.
      del = [];
      _.map(fees_org, (fee) => {
        if (
          !_.find(fees, {
            room_type_id: fee.room_type_id,
            channel,
            day: fee.day,
            stay_type: fee.stay_type,
            begin: fee.begin,
            end: fee.end,
          })
        )
          del.push(fee);
      });

      // 추가된 시간 요금.
      add = [];
      _.map(fees, (fee) => {
        const orgFee = _.find(fees_org, {
          room_type_id: fee.room_type_id,
          channel,
          day: fee.day,
          stay_type: fee.stay_type,
          begin: fee.begin,
          end: fee.end,
        });

        if (!orgFee) add.push(fee);
      });

      console.log("--- del ", del, " add ", add);

      this.setState(
        {
          data,
          org: _.cloneDeep(data),
          fees,
          save: { del, add, mod },
          timeDel: undefined,
          timeAdd: undefined,
        },
        () => this.onTimeMouseDown(evt, timeStart.day, timeStart.stay_type, timeStart.time, timeStart.t)
      );
    }

    this.isMouseDown = false;
  };

  onTimeDelete = (day, stay_type) => {
    console.log("--- onTimeDelete ", day, stay_type);

    let { timeDel } = this.state;

    timeDel = timeDel && timeDel.day === day && timeDel.stay_type === stay_type ? undefined : { day, stay_type };

    this.setState({
      timeDel,
      timeAdd: undefined,
      timeSel: undefined,
    });
  };

  onTimeAdd = (day, stay_type) => {
    console.log("--- onTimeAdd ", day, stay_type);

    let { timeAdd } = this.state;

    timeAdd = timeAdd && timeAdd.day === day && timeAdd.stay_type === stay_type ? undefined : { day, stay_type };

    this.setState({
      timeAdd,
      timeDel: undefined,
      timeSel: undefined,
    });
  };

  // 요금 변경 이벤트.
  onAddFee = (day, stay_type, value) => {
    console.log("----> onAddFee", day, stay_type, value);

    let {
      channel,
      fees_org,
      fees,
      add_fee,
      timeSel,
      save: { del, add, mod },
    } = this.state;

    // 시간 구간 추가요금 적용.
    timeSel.add_fee = value;

    add_fee[day - 1][stay_type - 1] = value;

    // 추가된 시간 요금.
    mod = [];
    _.map(fees, (fee) => {
      const orgFee = _.find(fees_org, {
        room_type_id: fee.room_type_id,
        channel,
        day: fee.day,
        stay_type: fee.stay_type,
        begin: fee.begin,
        end: fee.end,
      });

      if (orgFee && orgFee.add_fee !== fee.add_fee) mod.push(fee);
    });

    console.log("--- mod ", mod);

    this.setState({
      add_fee,
      save: { del, add, mod },
    });
  };

  onDayClick = (day) => {
    let { selDay } = this.state;

    selDay[day] = !selDay[day];

    console.log("--- onDayClick ", day, selDay[day]);

    this.setState({ selDay });
  };

  onDayCopyChange = (day) => {
    console.log("--- onDayCopyChange ", day);

    this.setState({ selCopyDay: Number(day) });
  };

  handleDayCopy = (evt) => {
    let { confirm, selCopyDay, selDay, fees, room_type_id } = this.state;

    console.log("- handleDayCopy", selCopyDay);

    if (!confirm) {
      this.setState({
        alert: {
          type: "warning",
          title: "요금 변경 확인",
          text: "요금 복제를 하면 해당 요일의 요금 정보는 삭제 됩니다.<br/>[" + moment.weekdays()[selCopyDay < 7 ? selCopyDay : 0] + "] 요일 요금으로 복제 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: () => this.handleDayCopy(evt),
        },
        dialog: true,
      });
      return;
    }

    let sel_days = [];

    _.map(selDay, (v, k) => (v && k + 1 !== selCopyDay ? sel_days.push({ day: k + 1 }) : null));

    // 삭제할 요일 요금.
    let del_fees = _.filter(fees, (v) => v.room_type_id === room_type_id && _.find(sel_days, { day: v.day }));

    //console.log("- sel_days", sel_days, " del_fees", del_fees);

    // 요일 요금 삭제.
    if (del_fees.length) this.props.handleDelRoomFees({ room_fees: del_fees });

    setTimeout(() => {
      // 복제할 요일 요금.
      let copy_fees = _.filter(fees, (v) => v.room_type_id === room_type_id && v.day === selCopyDay);

      // 요금 복제.
      const room_fees = [];

      _.map(sel_days, (d) =>
        _.map(_.cloneDeep(copy_fees), (v) => {
          v.day = d.day;
          room_fees.push(v);
        })
      );

      //console.log("- copy_fees", copy_fees, " room_fees", room_fees);

      // 요일 요금 복제.
      if (room_fees.length) this.props.handleNewRoomFees({ room_fees });

      this.setState({
        selDay: [],
        selCopyDay: 0,
        confirm: false,
      });
    }, 100);
  };

  onDayClear = () => {
    let { confirm, selCopyDay, selDay, fees, room_type_id } = this.state;

    if (!confirm) {
      if (!confirm) {
        this.setState({
          alert: {
            type: "warning",
            title: "초기화 확인",
            text: "초기화를 하면 해당 요일의 요금 정보는 삭제 됩니다.<br/>초기화를 하시겠습니까?",
            confirmButtonText: "예",
            cancelButtonText: "아니오",
            showCancelBtn: true,
            close: false,
            callback: () => this.onDayClear(),
          },
          dialog: true,
        });
        return;
      }

      return;
    }

    let sel_days = [];

    _.map(selDay, (v, k) => (v && k + 1 !== selCopyDay ? sel_days.push({ day: k + 1 }) : null));

    // 삭제할 요일 요금.
    let del_fees = _.filter(fees, (v) => v.room_type_id === room_type_id && _.find(sel_days, { day: v.day }));

    //console.log("- sel_days", sel_days, " del_fees", del_fees);

    // 요일 요금 삭제.
    if (del_fees.length) this.props.handleDelRoomFees({ room_fees: del_fees });

    this.setState({
      selDay: [],
      selCopyDay: 0,
      confirm: false,
    });
  };

  onSave = () => {
    console.log("--- onSave ");

    let {
      save: { del, add, mod },
      confirm,
    } = this.state;

    if (!confirm && (del.length || add.length || mod.length)) {
      this.setState({
        alert: {
          type: "warning",
          title: "저장 확인",
          text: "요금이 변경 되었습니다. <br/>저장 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: () => this.onSave(),
        },
        dialog: true,
      });
      return;
    }

    console.log("--- save del ", del, " add ", add, " mod ", mod);

    // 원본 요금이 없어졌으면 삭제.
    if (del.length) this.props.handleDelRoomFees({ room_fees: del });

    setTimeout(() => {
      // 추가 요금.
      // _.map(add, room_fee => {
      //   if (room_fee.add_fee === 0) {
      //     room_fee.add_fee =
      //       room_fee.stay_type === 1 ? preferences.stay_fee_unit : preferences.rent_fee_unit; // 기본 단위로 설정 한다.
      //   }
      // });

      if (add.length) this.props.handleNewRoomFees({ room_fees: add });

      setTimeout(() => {
        // 변경 요금.
        _.map(mod, (room_fee) => {
          // if (room_fee.add_fee === 0) {
          //  this.props.handleDelRoomFee(room_fee.id); // 추가 요금 없으면 삭제.
          //} else {
          this.props.handlePutRoomFee(room_fee.id, { room_fee });
          //}
        });
      }, 100);
    }, 100);
  };

  onImportRoomTypeChange = (importRoomTypeId) => {
    console.log("- onImportRoomTypeChange", importRoomTypeId);

    this.setState({
      importRoomTypeId: Number(importRoomTypeId),
    });
  };

  handleImportRoomTypeFee = (evt) => {
    let { confirm, importRoomTypeId, fees_org, room_type_id, channel } = this.state;

    console.log("- handleImportRoomTypeFee", importRoomTypeId);

    const roomType = _.find(this.props.roomType, { id: importRoomTypeId });

    if (!confirm) {
      this.setState({
        alert: {
          type: "warning",
          title: "요금 변경 확인",
          text: "요금 가져오기를 하면 현재 요금 정보는 삭제 됩니다.<br/>[" + roomType.name + "] 타입 요금을 가져 오시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: true,
          callback: () => this.handleImportRoomTypeFee(evt),
        },
        dialog: true,
      });
      return;
    }

    // 가져올 객실 타입,
    let room_fees_org = _.filter(fees_org, (v) => v.room_type_id === importRoomTypeId);

    console.log("- room_fees_org", room_fees_org.length);

    // 요금 복제.
    let room_fees = _.map(_.cloneDeep(room_fees_org), (v) => {
      v.room_type_id = room_type_id;
      return v;
    });

    console.log("- room_fees", room_fees.length);

    // 객실타입 요금 삭제. 적용 채널 (0: 카운터, 1:자판기)
    this.props.handleDelRoomTypeFee(room_type_id, channel, () => {
      // 가져올 객실 타입 요금 목록.
      if (room_fees.length) this.props.handleNewRoomFees({ room_fees });
    });

    this.setState(
      {
        importRoomTypeId: 0,
      },
      () => this.onRoomTypeSelect(room_type_id)
    );
  };

  onConfirm = (confirm, value) => {
    console.log("- onConfirm", confirm);

    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(value);

          // 확인 상태 초기화.
          setTimeout(() => {
            this.setState({
              confirm: false,
            });
          }, 100);
        }
      }
    );
  };

  closeFeeLayer = (evt) => {
    let {
      confirm,
      save: { del, add, mod },
    } = this.state;

    if (!confirm && (del.length || add.length || mod.length)) {
      this.setState({
        alert: {
          type: "warning",
          title: "저장 확인",
          text: "변경된 요금이 있습니다. <br/>창을 닫으면 저장 하지 않은 정보는 삭제 됩니다. <br/>요금 설정창을 닫으시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: true,
          callback: () => this.closeFeeLayer(),
        },
        dialog: true,
      });
      return;
    } else {
      this.props.closeFeeLayer();
    }
  };

  componentDidMount = () => {
    console.log("--- componentDidMount BasicCountSet ");
    const {
      user: { place_id },
    } = this.props;

    let days = [1, 2, 3, 4, 5, 6, 7];
    let data = [];

    let times = [];
    for (let i = 0; i <= 24; i++) {
      const hh = i < 10 ? "0" + String(i) : String(i);
      times.push({
        time: hh,
        hhmm: hh + "00",
        idx: 0,
      });

      if (i < 24) {
        times.push({
          time: "30",
          hhmm: hh + "30",
          idx: 0,
        });
      }
    }

    // 요일에 대실/숙박 타임테이블 설정.
    data = _.map(days, (day) => [(_.cloneDeep(times), _.cloneDeep(times))]);

    this.setState({
      room_type_id: this.props.roomType[0].id,
      days,
      data,
      times,
      dialog: false,
      confirm: false,
    });

    this.props.initAllRoomFees(place_id, 0); // 적용 채널 (0: 카운터, 1:자판기)
  };

  componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { room_type_id } = this.state;

    const {
      preferences: { speed_checkin },
    } = this.props;

    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    // 요금 정책 업데이트.
    if (nextProps.roomFee && nextProps.roomFee !== this.props.roomFee) {
      // 적용 채널 (0: 카운터, 1:자판기)
      let fees = _.filter(nextProps.roomFee, (v) => v.channel === this.state.channel);

      console.log("- fees", fees);

      this.setState(
        {
          fees_org: fees,
          fees: _.cloneDeep(fees),
        },
        () => this.onRoomTypeSelect(room_type_id || this.props.roomType[0].id)
      );

      // 수정 시.
      if (room_type_id) {
        this.setState({
          save: { del: [], add: [], mod: [] },
        });
      }

      this.setState({
        dialog: false,
        confirm: false,
      });
    }
  }

  render() {
    return this.state.loading ? (
      <BasicCountSet
        {...this.props}
        {...this.state}
        onRoomTypeSelect={this.onRoomTypeSelect}
        closeFeeLayer={this.closeFeeLayer}
        onTimeMouseDown={this.onTimeMouseDown}
        onTimeMouseEnter={this.onTimeMouseEnter}
        onTimeMouseUp={this.onTimeMouseUp}
        onTimeDelete={this.onTimeDelete}
        onTimeAdd={this.onTimeAdd}
        onAddFee={this.onAddFee}
        onDayClick={this.onDayClick}
        onDayCopyChange={this.onDayCopyChange}
        handleDayCopy={this.handleDayCopy}
        onDayClear={this.onDayClear}
        onSave={this.onSave}
        onConfirm={this.onConfirm}
        onImportRoomTypeChange={this.onImportRoomTypeChange}
        handleImportRoomTypeFee={this.handleImportRoomTypeFee}
      />
    ) : (
      <div className={cx("loader")}>
        <Wave color="white" size={60} />
      </div>
    );
  }
}

export default withSnackbar(Container);
