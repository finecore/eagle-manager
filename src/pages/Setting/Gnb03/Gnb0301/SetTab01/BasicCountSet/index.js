// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as roomFeeAction } from "actions/roomFee";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, roomType, preferences, roomFee, seasonPremium, error } = state;
  return {
    user: auth.user,
    roomType: roomType.list,
    roomFee: roomFee.list,
    seasonPremiums: seasonPremium.list,
    preferences: preferences.item,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRoomFees: (placeId, channel) => dispatch(roomFeeAction.getRoomFeeList(placeId, channel)),

    handleDelRoomFee: (id) => dispatch(roomFeeAction.delRoomFee(id)),

    handleNewRoomFee: (room_fee, callback) => dispatch(roomFeeAction.newRoomFee(room_fee, callback)),

    handlePutRoomFee: (id, room_fee) => dispatch(roomFeeAction.putRoomFee(id, room_fee)),

    handleNewRoomFees: (room_fees) => dispatch(roomFeeAction.newRoomFees(room_fees)),

    handleDelRoomFees: (room_fees, callback) => dispatch(roomFeeAction.delRoomFees(room_fees, callback)),

    handleDelRoomTypeFee: (roomTypeId, channel, callback) => dispatch(roomFeeAction.delRoomTypeFee(roomTypeId, channel, callback)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
