// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";
import _ from "lodash";
import moment from "moment";
import color from "utils/color-util";

import { Table, FormControl, ControlLabel, Checkbox, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import swal from "sweetalert";
import Swal from "sweetalert2";

// styles.
import "./styles.scss";

// functional component
const BasicCountSet = (props) => {
  const roomType = _.find(props.roomType, { id: props.room_type_id });

  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeFeeLayer(evt)}>
      <article className="basic-count-set" id="basic-count-pop">
        <h1>객실 타입별/요일별/시간별 추가요금 설정</h1>
        <div className="count-form-wrapper">
          <div className="count-header">
            <span>
              <b>{roomType.name}</b> 숙박: {roomType.default_fee_stay} 대실:
              {roomType.default_fee_rent}
            </span>
          </div>
          <div className="count-content">
            <div className="count-room-list">
              <ul>
                {props.roomType.map((v, k) => (
                  <li key={k}>
                    <Button className={cx(props.room_type_id === v.id ? "selected" : "")} onClick={(evt) => props.onRoomTypeSelect(v.id)}>
                      {v.name}
                    </Button>
                  </li>
                ))}
              </ul>
            </div>
            <div className="form-content">
              <div className="table-wrapper">
                <Table>
                  <thead>
                    <tr>
                      <th>선택</th>
                      <th>요일</th>
                      <th>추가요금</th>
                      <th>
                        <ul>
                          <li>
                            <div>오전</div>
                            <div>오후</div>
                          </li>
                          <li>
                            <table>
                              <tbody>
                                <tr>
                                  {props.times.map((v, k) => (
                                    <td key={k} draggable={false} className={cx(v.time === "30" ? "half" : "hour")}>
                                      <div>{v.time}</div>
                                    </td>
                                  ))}
                                </tr>
                              </tbody>
                            </table>
                          </li>
                        </ul>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {props.days.map((day, k) => (
                      <tr key={k}>
                        <td>
                          <Checkbox inline checked={props.selDay[k] || false} onChange={(evt) => props.onDayClick(k)}>
                            <span />
                          </Checkbox>
                        </td>
                        <td>
                          <Button className={cx(props.idx === k ? "selected" : "")} onClick={(evt) => props.onDayClick(k)}>
                            {moment.weekdaysShort()[day < 7 ? day : 0]}
                          </Button>
                        </td>
                        <td>
                          <ul>
                            <li>
                              <ControlLabel>숙</ControlLabel>
                              <span className="numeric-wrap">
                                <InputNumber
                                  className="form-control mid"
                                  value={props.add_fee[day - 1][0]}
                                  step={props.preferences.stay_fee_unit}
                                  min={-roomType.default_fee_stay}
                                  onChange={(value) => props.onAddFee(day, 1, value)}
                                  disabled={!props.timeSel || props.timeSel.day !== day || props.timeSel.stay_type !== 1}
                                />
                              </span>
                              <Button onClick={(evt) => props.onTimeDelete(day, 1)} className={cx(props.timeDel && props.timeDel.day === day && props.timeDel.stay_type === 1 ? "del" : "")}>
                                <span className="mdi mdi-eraser" />
                              </Button>
                              <Button onClick={(evt) => props.onTimeAdd(day, 1)} className={cx(props.timeAdd && props.timeAdd.day === day && props.timeAdd.stay_type === 1 ? "add" : "")}>
                                <span className="mdi mdi-plus-circle-outline" />
                              </Button>
                            </li>
                            <li>
                              <ControlLabel>대</ControlLabel>
                              <span className="numeric-wrap">
                                <InputNumber
                                  className="form-control mid"
                                  value={props.add_fee[day - 1][1]}
                                  step={props.preferences.rent_fee_unit}
                                  min={-roomType.default_fee_rent}
                                  onChange={(value) => props.onAddFee(day, 2, value)}
                                  disabled={!props.timeSel || props.timeSel.day !== day || props.timeSel.stay_type !== 2}
                                />
                              </span>
                              <Button onClick={(evt) => props.onTimeDelete(day, 2)} className={cx(props.timeDel && props.timeDel.day === day && props.timeDel.stay_type === 2 ? "del" : "")}>
                                <span className="mdi mdi-eraser" />
                              </Button>
                              <Button onClick={(evt) => props.onTimeAdd(day, 2)} className={cx(props.timeAdd && props.timeAdd.day === day && props.timeAdd.stay_type === 2 ? "add" : "")}>
                                <span className="mdi mdi-plus-circle-outline" />
                              </Button>
                            </li>
                          </ul>
                        </td>
                        <td>
                          <table onMouseUp={(evt) => props.onTimeMouseUp(evt)} className="time-table">
                            <tbody>
                              <tr>
                                {props.data[day - 1][0].map((time, i) => (
                                  <td
                                    key={i}
                                    draggable={false}
                                    className={cx(time.idx > 0 ? "time" : "", time.sel)}
                                    style={{
                                      backgroundColor: color.get((time.idx % 24) + (time.idx < 24 ? 0 : 1)),
                                    }}
                                    onMouseDown={(evt) => props.onTimeMouseDown(evt, day, 1, i, time)}
                                    onMouseEnter={(evt) => props.onTimeMouseEnter(evt, day, 1, i, time)}
                                  >
                                    <span>{time.fee && time.fee.begin === time.hhmm && String(roomType.default_fee_stay + time.fee.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                                  </td>
                                ))}
                              </tr>
                              <tr>
                                {props.data[day - 1][1].map((time, i) => (
                                  <td
                                    key={i}
                                    draggable={false}
                                    className={cx(time.idx > 0 ? "time" : "", time.sel)}
                                    style={{
                                      backgroundColor: color.get((time.idx % 24) + (time.idx < 24 ? 0 : 1)),
                                    }}
                                    onMouseDown={(evt) => props.onTimeMouseDown(evt, day, 2, i, time)}
                                    onMouseEnter={(evt) => props.onTimeMouseEnter(evt, day, 2, i, time)}
                                  >
                                    <span>{time.fee && time.fee.begin === time.hhmm && String(roomType.default_fee_rent + time.fee.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                                  </td>
                                ))}
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
          <div className="count-footer">
            <div>
              <FormControl componentClass="select" placeholder="선택" value={props.importRoomTypeId} onChange={(evt) => props.onImportRoomTypeChange(evt.target.value)}>
                <option value="">객실타입선택 </option>
                {props.roomType
                  .filter((v) => v.id !== props.room_type_id)
                  .map((v, k) => (
                    <option key={k} value={v.id}>
                      {v.name}
                    </option>
                  ))}
              </FormControl>
              <ButtonToolbar className="btn-room">
                <Button onClick={(evt) => props.handleImportRoomTypeFee(evt)} className={cx(props.importRoomTypeId ? "" : "off")}>
                  요금 가져오기
                </Button>
              </ButtonToolbar>
            </div>
            {props.selDay.filter((v) => v === true).length > 1 && (
              <div>
                {props.selDay.map(
                  (v, k) =>
                    v &&
                    k + 1 !== props.selCopyDay && (
                      <span key={k}>
                        [<b>{moment.weekdaysShort()[k + 1 < 7 ? k + 1 : 0]}</b>]{" "}
                      </span>
                    )
                )}{" "}
                요일의 요금을{" "}
                <FormControl componentClass="select" className="day-copy" value={props.selCopyDay || 0} onChange={(evt) => props.onDayCopyChange(evt.target.value)}>
                  <option value="0">요일선택 </option>
                  {props.selDay.map(
                    (v, k) =>
                      v && (
                        <option key={k} value={k + 1}>
                          {moment.weekdays()[k + 1 < 7 ? k + 1 : 0]}
                        </option>
                      )
                  )}
                </FormControl>
                <ButtonToolbar className="btn-room">
                  <Button className={cx(props.selCopyDay ? "" : "off")} onClick={(evt) => props.handleDayCopy(evt)}>
                    요금으로 복제
                  </Button>
                </ButtonToolbar>
              </div>
            )}
            <ButtonToolbar className="btn-set-close">
              <Button onClick={(evt) => props.onDayClear(evt)} className={cx(props.selDay.filter((v) => v === true).length ? "" : "off")}>
                초기화
              </Button>
              <Button onClick={(evt) => props.onSave()} className={cx(props.save.del.length || props.save.add.length || props.save.mod.length ? "" : "off")}>
                요금저장
              </Button>
              <Button onClick={(evt) => props.closeFeeLayer(evt)}>닫기</Button>
            </ButtonToolbar>
          </div>
        </div>
        <a onClick={(evt) => props.closeFeeLayer(evt)} className="count-close">
          Close
        </a>
      </article>
    </Modal>
  );
};

export default BasicCountSet;
