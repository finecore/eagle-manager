// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    preferences: { modify },
  } = state;

  return {
    user: auth.user,
    modify,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
