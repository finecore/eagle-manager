// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab01 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayFeeLayer: false,
      displayIscLayer: false,
    };
  }

  static propTypes = {
    modify: PropTypes.object.isRequired,
  };

  openFeeLayer = (evt) => {
    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    this.setState({
      displayFeeLayer: true,
    });
  };

  openIscLayer = (evt) => {
    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    this.setState({
      displayIscLayer: true,
    });
  };

  closeFeeLayer = (evt) => {
    this.setState({
      displayFeeLayer: false,
      displayIscLayer: false,
    });
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(false);
  };

  componentDidMount = () => {};

  UNSAFE_componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <SetTab01
        {...this.props}
        {...this.state}
        openFeeLayer={this.openFeeLayer}
        openIscLayer={this.openIscLayer}
        closeFeeLayer={this.closeFeeLayer}
        closeModal={this.closeModal}
        handleInputChange1={this.handleInputChange1}
      />
    );
  }
}

export default withSnackbar(Container);
