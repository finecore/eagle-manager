// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as timeOptionAction } from "actions/timeOption";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, roomType, timeOption, preferences, error } = state;
  return {
    auth,
    roomType,
    timeOption,
    preferences: preferences.item,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initTimeOptions: (palceId) => dispatch(timeOptionAction.getTimeOptions(palceId)),

    handleNewTimeOption: (timeOption) => dispatch(timeOptionAction.newTimeOption(timeOption)),

    handleSaveTimeOption: (id, timeOption) => dispatch(timeOptionAction.putTimeOption(id, timeOption)),

    handleDeleteTimeOption: (palceId, id) => dispatch(timeOptionAction.delTimeOption(palceId, id)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
