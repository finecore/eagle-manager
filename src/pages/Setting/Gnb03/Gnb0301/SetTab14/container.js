// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

// UI for notification
import { withSnackbar } from "notistack";

import cx from "classnames";

// Components.
import SetTab14 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeOptions: [], // 업소 옵션 목록.
      org: {}, // 수정 시 사용 할 원래 아이디값.
      data: {},
      idx: -1,
      isNew: false,
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    timeOption: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { data, idx, isNew } = this.state;

    console.log("-- handleInputChange", name, value);

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    data[name] = value;

    if (name === "stay_type" && isNew) {
      data = this.setTime(data);
    }

    // 수정 아니고 입력값이 있으면 신규 설정.
    if (idx === -1 && !isNew) isNew = true;

    this.setState({
      data,
      isNew,
    });
  };

  onSelect = (timeOption, idx) => {
    console.log("-- onSelect", timeOption, idx);

    let data = _.cloneDeep(timeOption);

    this.setState({
      isNew: false,
      org: timeOption,
      data,
      selected: data.selected,
      idx,
    });
  };

  setTime = (data) => {
    const {
      preferences: { stay_time_type, stay_time, use_rent_begin, use_rent_end, rent_time_am, rent_time_pm },
    } = this.props;

    let hour = Number(moment().format("HH"));

    // 숙박/대실 시간대 (대실 가능 시간 이외는 숙박으로 처리)
    data.stay_type = data.stay_type || (Number(hour) >= Number(use_rent_begin) && Number(hour) < Number(use_rent_end) ? 2 : 1);

    // 기본 숙박 시간 (입실 시간 기준이면 이용 시간 더하고 퇴실 시간 기준이면 퇴실 시간 설정.)
    let stayTime = stay_time_type === 0 ? (use_rent_end + stay_time) % 24 : stay_time;

    // 기본 대실 시간
    let rentTime =
      Number(hour) < 12 // 오전
        ? rent_time_am
        : rent_time_pm;

    data.begin = data.stay_type === 1 ? use_rent_end : use_rent_begin;
    data.end = data.stay_type === 1 ? use_rent_begin : use_rent_end;
    data.use_time = data.stay_type === 1 ? 0 : rentTime;
    data.check_out_time = data.stay_type === 1 ? stayTime : 0; // 숙박 기본 시간은 옵션 종료 시간 + 숙박 기본 시간이다.

    console.log("-- setTime", data);

    return data;
  };

  onAdd = () => {
    const {
      preferences: { place_id },
      roomType,
    } = this.props;

    console.log("-- onAdd");

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let selected = [];

    // 객실 타입 초기화.
    _.map(roomType.list, (item) => {
      selected.push(false);
    });

    let data = {
      place_id,
      name: "",
      stay_type: 0,
      begin: 0,
      end: 0,
      use_time: 0,
      check_out_time: 0,
      add_fee: 0,
      room_types: "",
      sale_isc: 0,
      type_names: "",
      selected,
    };

    data = this.setTime(data);

    this.setState({
      isNew: true,
      idx: -1,
      org: {},
      data,
    });
  };

  onDelete = () => {
    const { data, idx, confirm } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let time_option = data;

    console.log("-- onDelete", time_option, confirm);

    if (idx === -1) {
      this.setState({
        alert: {
          type: "warning",
          title: "선택 확인",
          text: "삭제 할 옵션을 선택해 주세요.",
          confirmButtonText: "확인",
          showCancelBtn: false,
          close: true,
        },
        dialog: true,
      });

      return;
    }

    if (!confirm) {
      this.setState({
        alert: {
          type: "warning",
          title: "삭제 확인",
          text: "[" + time_option.name + "] 옵션을 정말 삭제 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: () => this.onDelete(),
        },
        dialog: true,
      });
    } else {
      this.props.handleDeleteTimeOption(time_option.place_id, time_option.id);
    }
  };

  onChecked = (idx, checked) => {
    console.log("-- onChecked", idx, checked);

    let { data } = this.state;

    data.selected[idx] = checked;

    const { roomType } = this.props;

    let room_types = [];

    _.map(data.selected, (v, k) => {
      if (v) room_types.push(roomType.list[k].id);
    });

    data.room_types = room_types.join(",");

    console.log("-- room_types", data.room_types);

    this.setState({
      data,
    });
  };

  onSave = () => {
    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let { org, data, isNew, confirm } = this.state;

    isNew = !org.id;

    console.log("-- onSave", data, isNew);

    if (!data.name || !data.room_types || !data.use_time_type || !data.use_time) {
      this.setState({
        alert: {
          type: "warning",
          title: "입력값 확인",
          text:
            (!data.name ? "옵션명은" : !data.room_types ? "객실 타입은" : !data.use_time_type ? "적용기준 선택은" : data.use_time_type === 1 ? "이용 시간은" : "퇴실 시간은") + " 필수 입력 입니다.",
          confirmButtonText: "확인",
          showCancelBtn: false,
          close: true,
        },
        dialog: true,
      });

      return;
    }

    if (!confirm) {
      this.setState({
        alert: {
          title: isNew ? "등록 확인" : "저장 확인",
          text: "[" + (isNew ? data.name : org.name) + "] " + (isNew ? "옵션을 등록 하시겠습니까?" : "옵션을 수정 하시겠습니까?"),
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: () => this.onSave(),
        },
        dialog: true,
      });
    } else {
      let time_option = data;

      if (isNew) this.props.handleNewTimeOption({ time_option });
      else this.props.handleSaveTimeOption(org.id, { time_option });
    }
  };

  onCancel = () => {
    this.setState({
      isNew: false,
      idx: -1,
      org: {},
      data: {},
    });
  };

  onConfirm = (confirm, value) => {
    console.log("- onConfirm", confirm);

    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(value);

          // 확인 상태 초기화.
          setTimeout(() => {
            this.setState({
              confirm: false,
            });
          }, 100);
        }
      }
    );
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      preferences: { place_id },
    } = this.props;

    this.props.initTimeOptions(place_id);
  };

  componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { data } = this.state;

    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    if (nextProps.timeOption && nextProps.timeOption !== this.props.timeOption) {
      let { list } = nextProps.timeOption;

      const { roomType } = this.props;

      // 객실 타입 설정.
      _.map(list, (item) => {
        let types = item.room_types.split(",");

        item.selected = [];
        item.type_names = "";

        console.log("- timeOption types ", types);

        _.map(roomType.list, (type) => {
          let isSelected = _.includes(types, String(type.id));

          if (isSelected) item.type_names += (item.type_names ? "/" : "") + type.name;

          item.selected.push(isSelected);
        });
      });

      console.log("- timeOption  ", list);

      this.setState({
        timeOptions: list,
      });

      if (data.name) {
        this.setState({
          org: {},
          isNew: false,
          data: {},
          idx: -1,
          isChangePwd: false,
          dialog: false,
          confirm: false,
        });
      }
    }
  }

  render() {
    return this.state.timeOptions ? (
      <SetTab14
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSelect={this.onSelect}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
        onChecked={this.onChecked}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
