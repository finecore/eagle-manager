// imports.
import React from "react";
import _ from "lodash";
import cx from "classnames";

import format from "utils/format-util";

import { Table, FormGroup, FormControl, ControlLabel, ButtonToolbar, Checkbox, ButtonGroup, Radio, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";

import { keyToValue } from "constants/key-map";

import swal from "sweetalert";
import Swal from "sweetalert2";

// styles.
import "./styles.scss";

const SetTab14 = (props) => {
  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div>
      <div className="setting-form" id="gnb030114">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>입실 시간 옵션</h2>
              <div className="form-content">
                <div className="search-result">
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          <th>옵션명</th>
                          <th>타입</th>
                          <th>시작</th>
                          <th>종료</th>
                          <th>적용 시간</th>
                          <th>추가/할인</th>
                          <th>객실타입</th>
                          <th>무인</th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.map(props.timeOptions, (item, k) => (
                          <tr key={k} onClick={(evt) => props.onSelect(item, k)} className={cx(props.idx === k && "on")}>
                            <td>{item.name}</td>
                            <td>{keyToValue("room_sale", "stay_type", item.stay_type)}</td>
                            <td>{item.begin}</td>
                            <td>{item.end}</td>
                            {item.use_time_type === 1 ? (
                              <td>
                                {item.use_time} 시간 <span className="time-type1">이용</span>
                              </td>
                            ) : (
                              <td>
                                {item.use_time} 시에 <span className="time-type2">퇴실</span>
                              </td>
                            )}
                            <td>{format.toMoney(item.add_fee)}</td>
                            <td>{item.type_names}</td>
                            <td>{item.sale_isc === 0 ? "판매" : "X"}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                  <ButtonToolbar className="btn-room">
                    <Button
                      // 매니저 이상 추가.
                      disabled={props.auth.user.level > 2}
                      onClick={(evt) => props.onAdd()}
                    >
                      추가
                    </Button>
                    <Button
                      // 매니저 이상 삭제.
                      disabled={props.auth.user.level > 2}
                      onClick={(evt) => props.onDelete()}
                    >
                      삭제
                    </Button>
                  </ButtonToolbar>
                </div>
              </div>
            </li>
          </ul>
          <div className="comment">
            <div>- 옵션 시작 ~ 옵션 종료 시간내에만 옵션 적용이 가능 합니다.</div>
            <div>- 이용 시간 또는 퇴실 시간 중 선택 설정 합니다.</div>
          </div>
          <ul className={cx("list-bul", "add-list", props.isNew || props.idx > -1 ? "show" : "hide")}>
            <li>
              <h2>옵션 {props.isNew ? "등록" : "정보"}</h2>
              <div className="form-content">
                <div>
                  <FormGroup>
                    <ControlLabel>옵션명</ControlLabel>
                    <FormControl type="text" placeholder="옵션명" value={props.data.name || ""} onChange={(evt) => props.handleInputChange("name", evt.target.value)} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>투숙 형태</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="투숙형태"
                      value={props.data.stay_type || 1}
                      onChange={(evt) => props.handleInputChange("stay_type", Number(evt.target.value))}
                      className={cx(props.data.stay_type === 1 ? "stay" : "rent")}
                    >
                      <option value="1">숙박</option>
                      <option value="2">대실</option>
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>옵션 시작</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.data.begin}
                        step={1}
                        min={0}
                        max={23}
                        formatter={(value) => `${value} 시`}
                        onChange={(value) => props.handleInputChange("begin", Number(value))}
                      />
                    </span>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>옵션 종료</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.data.end}
                        step={1}
                        min={0}
                        max={23}
                        formatter={(value) => `${value} 시`}
                        onChange={(value) => props.handleInputChange("end", Number(value))}
                      />
                    </span>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>적용 기준</ControlLabel>
                    <ButtonGroup aria-label="use_time_type">
                      <Radio inline name="use_time_type" value={1} checked={props.data.use_time_type === 1} onChange={(evt) => props.handleInputChange("use_time_type", Number(evt.target.value))}>
                        <span />
                        이용시간 기준
                      </Radio>
                      <Radio inline name="use_time_type" value={2} checked={props.data.use_time_type === 2} onChange={(evt) => props.handleInputChange("use_time_type", Number(evt.target.value))}>
                        <span />
                        퇴실시간 기준
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>{props.data.use_time_type === 1 ? "이용 시간" : "퇴실 시간"}</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.data.use_time}
                        step={1}
                        min={1}
                        max={24}
                        formatter={(value) => (props.data.use_time_type === 1 ? `${value} 시간` : `${value} 시`)}
                        onChange={(value) => props.handleInputChange("use_time", Number(value))}
                      />
                    </span>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>추가/할인 요금</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.data.add_fee}
                        step={props.data.stay_type === 2 ? props.preferences.rent_fee_unit : props.preferences.stay_fee_unit}
                        min={-9999999}
                        max={9999999}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("add_fee", Number(value))}
                      />
                    </span>
                  </FormGroup>
                  <FormGroup colSpan="2">
                    <ControlLabel>적용 객실타입</ControlLabel>
                    <div>
                      {props.roomType.list.map((v, k) => (
                        <Checkbox key={k} inline checked={props.data.selected ? props.data.selected[k] : false} onChange={(evt) => props.onChecked(k, evt.target.checked)}>
                          {v.name}
                          <span />
                        </Checkbox>
                      ))}
                    </div>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>무인 판매</ControlLabel>
                    <ButtonGroup aria-label="sale_isc" onChange={(evt) => props.handleInputChange("sale_isc", Number(evt.target.value))}>
                      <Radio inline name="sale_isc" value={0} defaultChecked={props.data.sale_isc === 0} className={cx(props.data.sale_isc === 0 && "checked-on")}>
                        <span />
                        사용
                      </Radio>
                      <Radio inline name="sale_isc" value={1} defaultChecked={props.data.sale_isc === 1}>
                        <span />
                        사용안함
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup>&nbsp;</FormGroup>
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button disabled={(!props.isNew && props.idx === -1) || props.auth.user.level > props.data.level} onClick={(evt) => props.onSave()}>
                  {props.isNew ? "등록" : "저장"}
                </Button>
                <Button onClick={(evt) => props.onCancel()}>닫기</Button>
              </ButtonToolbar>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab14;
