// imports.
import React from "react";

import { ControlLabel, Checkbox, ButtonToolbar, Button, Radio, ButtonGroup } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab02 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030102">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>대실 시간 설정</h2>
              <div className="form-content">
                <ControlLabel htmlFor="gnb030102-01">평일 오전</ControlLabel>
                <span className="numeric-wrap">
                  <InputNumber className="form-control small" value={props.modify.rent_time_am} step={1} min={1} max={12} onChange={(value) => props.handleInputChange("rent_time_am", value)} />
                </span>
                <ControlLabel htmlFor="gnb030102-02">평일 오후</ControlLabel>
                <span className="numeric-wrap">
                  <InputNumber className="form-control small" value={props.modify.rent_time_pm} step={1} min={1} max={12} onChange={(value) => props.handleInputChange("rent_time_pm", value)} />
                </span>
                <ControlLabel htmlFor="gnb030102-01">주말 오전</ControlLabel>
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control small"
                    value={props.modify.rent_time_am_weekend}
                    step={1}
                    min={1}
                    max={12}
                    onChange={(value) => props.handleInputChange("rent_time_am_weekend", value)}
                  />
                </span>
                <ControlLabel htmlFor="gnb030102-02">주말 오후</ControlLabel>
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control small"
                    value={props.modify.rent_time_pm_weekend}
                    step={1}
                    min={1}
                    max={12}
                    onChange={(value) => props.handleInputChange("rent_time_pm_weekend", value)}
                  />
                </span>
              </div>
            </li>
            <li>
              <ul className="list-bul">
                <li>
                  <h2>숙박 시간 설정</h2>
                  <div className="form-content">
                    <ButtonGroup aria-label="stay_time_type" onChange={(evt) => props.handleInputChange("stay_time_type", evt.target.value)}>
                      <Radio inline name="stay_time_type" value="0" defaultChecked={props.modify.stay_time_type === 0} className={cx(props.modify.stay_time_type === 0 && "checked-on")}>
                        <span />
                        입실 시간 기준
                      </Radio>
                      <Radio inline name="stay_time_type" value="1" defaultChecked={props.modify.stay_time_type === 1} className={cx(props.modify.stay_time_type === 1 && "checked-on")}>
                        <span />
                        퇴실 시간 기준
                      </Radio>
                    </ButtonGroup>
                  </div>
                  <div className="form-content">
                    <ButtonGroup>
                      <ControlLabel>평일</ControlLabel>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid140"
                          value={props.modify.stay_time}
                          step={1}
                          min={1}
                          max={24}
                          formatter={(value) => value + (props.modify.stay_time_type === 1 ? "시 퇴실" : "시간 사용")}
                          onChange={(value) => props.handleInputChange("stay_time", value)}
                        />
                      </span>
                      <ControlLabel>주말</ControlLabel>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid140"
                          value={props.modify.stay_time_weekend}
                          step={1}
                          min={1}
                          max={24}
                          formatter={(value) => value + (props.modify.stay_time_type === 1 ? "시 퇴실" : "시간 사용")}
                          onChange={(value) => props.handleInputChange("stay_time_weekend", value)}
                        />
                      </span>
                    </ButtonGroup>
                  </div>
                </li>
                <li>
                  <h2>손님키 삽입 시 자동입실 처리 </h2>
                  <div className="form-content">
                    <ButtonGroup aria-label="auto_check_in" onChange={(evt) => props.handleInputChange("auto_check_in", evt.target.value)}>
                      <Radio inline name="auto_check_in" value="1" defaultChecked={props.modify.auto_check_in === 1} className={cx(props.modify.auto_check_in === 1 && "checked-on")}>
                        <span />
                        대실시간만 사용
                      </Radio>
                      <Radio inline name="auto_check_in" value="2" defaultChecked={props.modify.auto_check_in === 2} className={cx(props.modify.auto_check_in === 2 && "checked-on")}>
                        <span />
                        숙박시간만 사용
                      </Radio>
                      <Radio inline name="auto_check_in" value="3" defaultChecked={props.modify.auto_check_in === 3} className={cx(props.modify.auto_check_in === 3 && "checked-on")}>
                        <span />
                        대실/숙박 사용
                      </Radio>
                      <Radio inline name="auto_check_in" value="0" defaultChecked={props.modify.auto_check_in === 0}>
                        <span />
                        사용 안함
                      </Radio>
                    </ButtonGroup>
                  </div>
                </li>
                <li>
                  <h2>손님키 제거 시 기본 설정</h2>
                  <div className="form-content">
                    <ButtonGroup aria-label="user_key_out_rent" onChange={(evt) => props.handleInputChange("user_key_out_rent", evt.target.value)}>
                      <ControlLabel>대실</ControlLabel>
                      <Radio inline name="user_key_out_rent" value="0" defaultChecked={props.modify.user_key_out_rent === 0} className={cx(props.modify.user_key_out_rent === 0 && "checked-on")}>
                        <span />
                        외출
                      </Radio>
                      <Radio inline name="user_key_out_rent" value="1" defaultChecked={props.modify.user_key_out_rent === 1} className={cx(props.modify.user_key_out_rent === 1 && "checked-on")}>
                        <span />
                        퇴실
                      </Radio>
                    </ButtonGroup>
                  </div>
                  <div className="form-content">
                    <ButtonGroup aria-label="user_key_out_stay" onChange={(evt) => props.handleInputChange("user_key_out_stay", evt.target.value)}>
                      <ControlLabel>숙박</ControlLabel>
                      <Radio inline name="user_key_out_stay" value="0" defaultChecked={props.modify.user_key_out_stay === 0} className={cx(props.modify.user_key_out_stay === 0 && "checked-on")}>
                        <span />
                        외출
                      </Radio>
                      <Radio inline name="user_key_out_stay" value="1" defaultChecked={props.modify.user_key_out_stay === 1} className={cx(props.modify.user_key_out_stay === 1 && "checked-on")}>
                        <span />
                        퇴실
                      </Radio>
                    </ButtonGroup>
                  </div>
                </li>
                <li>
                  <h2>사용시간 경과 후 자동퇴실 처리 </h2>
                  <div className="form-content">
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.auto_check_out_key_out === 1 && "checked-on")}
                      defaultChecked={props.modify.auto_check_out_key_out === 1}
                      onChange={(evt) => props.handleInputChange("auto_check_out_key_out", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      손님키 제거 시 자동 퇴실처리
                    </Checkbox>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.auto_check_out_door_open === 1 && "checked-on")}
                      defaultChecked={props.modify.auto_check_out_door_open === 1}
                      onChange={(evt) => props.handleInputChange("auto_check_out_door_open", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      출입문름 열리면 자동 퇴실처리
                    </Checkbox>
                  </div>
                </li>
                <li>
                  <h2>인스펙트(객실 점검) 사용 설정</h2>
                  <div className="form-content">
                    <ButtonGroup aria-label="inspect_use" onChange={(evt) => props.handleInputChange("inspect_use", evt.target.value)}>
                      <Radio inline name="inspect_use" value={1} defaultChecked={props.modify.inspect_use === 1} className={cx(props.modify.inspect_use === 1 && "checked-on")}>
                        <span />
                        퇴실 상태만 사용
                      </Radio>
                      <Radio inline name="inspect_use" value={2} defaultChecked={props.modify.inspect_use === 2} className={cx(props.modify.inspect_use === 2 && "checked-on")}>
                        <span />
                        항상 사용
                      </Radio>
                      <Radio inline name="inspect_use" value={0} defaultChecked={props.modify.inspect_use === 0}>
                        <span />
                        사용 안함
                      </Radio>
                    </ButtonGroup>
                    <span className="comment">* 마스터키가 있어야 사용 가능 합니다.</span>
                  </div>
                </li>
                {props.modify.inspect_use === 0 ? (
                  <li>
                    <h2>청소키 제거 시 공실 처리 (퇴실 상태 일때)</h2>
                    <div className="form-content">
                      <ButtonGroup aria-label="clean_key_out_type" onChange={(evt) => props.handleInputChange("clean_key_out_type", evt.target.value)}>
                        <Radio inline name="clean_key_out_type" value="1" defaultChecked={props.modify.clean_key_out_type === 1} className={cx(props.modify.clean_key_out_type === 1 && "checked-on")}>
                          <span />
                          대실만 사용
                        </Radio>
                        <Radio inline name="clean_key_out_type" value="2" defaultChecked={props.modify.clean_key_out_type === 2} className={cx(props.modify.clean_key_out_type === 2 && "checked-on")}>
                          <span />
                          숙박만 사용
                        </Radio>
                        <Radio inline name="clean_key_out_type" value="3" defaultChecked={props.modify.clean_key_out_type === 3} className={cx(props.modify.clean_key_out_type === 3 && "checked-on")}>
                          <span />
                          대실/숙박 사용
                        </Radio>
                        <Radio inline name="clean_key_out_type" value="0" defaultChecked={props.modify.clean_key_out_type === 0}>
                          <span />
                          사용 안함
                        </Radio>
                      </ButtonGroup>
                    </div>
                  </li>
                ) : (
                  <li>
                    <h2>마스터키 제거 시 공실 처리 (퇴실 상태 일때)</h2>
                    <div className="form-content">
                      <ButtonGroup aria-label="master_key_out_type" onChange={(evt) => props.handleInputChange("master_key_out_type", evt.target.value)}>
                        <Radio
                          inline
                          name="master_key_out_type"
                          value="1"
                          defaultChecked={props.modify.master_key_out_type === 1}
                          className={cx(props.modify.master_key_out_type === 1 && "checked-on")}
                        >
                          <span />
                          대실만 사용
                        </Radio>
                        <Radio
                          inline
                          name="master_key_out_type"
                          value="2"
                          defaultChecked={props.modify.master_key_out_type === 2}
                          className={cx(props.modify.master_key_out_type === 2 && "checked-on")}
                        >
                          <span />
                          숙박만 사용
                        </Radio>
                        <Radio
                          inline
                          name="master_key_out_type"
                          value="3"
                          defaultChecked={props.modify.master_key_out_type === 3}
                          className={cx(props.modify.master_key_out_type === 3 && "checked-on")}
                        >
                          <span />
                          대실/숙박 사용
                        </Radio>
                        <Radio inline name="master_key_out_type" value="0" defaultChecked={props.modify.master_key_out_type === 0}>
                          <span />
                          사용 안함
                        </Radio>
                      </ButtonGroup>
                    </div>
                  </li>
                )}
                {props.modify.inspect_use === 0 ? (
                  <li>
                    <h2>청소키 제거 시 무조건 공실 처리</h2>
                    <div className="form-content">
                      <ButtonGroup aria-label="clean_key_out_type_force" onChange={(evt) => props.handleInputChange("clean_key_out_type_force", evt.target.value)}>
                        <Radio
                          inline
                          name="clean_key_out_type_force"
                          value={1}
                          defaultChecked={props.modify.clean_key_out_type_force === 1}
                          className={cx(props.modify.clean_key_out_type_force === 1 && "checked-on")}
                        >
                          <span />
                          사용
                        </Radio>
                        <Radio inline name="clean_key_out_type_force" value={0} defaultChecked={props.modify.clean_key_out_type_force === 0}>
                          <span />
                          사용 안함
                        </Radio>
                      </ButtonGroup>
                    </div>
                  </li>
                ) : null}
              </ul>
              <ul className="list-bul">
                <li>
                  <h2>
                    대실 이용 가능시간 <span>(0~24 시)</span>
                  </h2>
                  <div className="form-content li-label inline">
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        step={1}
                        min={0}
                        max={24}
                        value={props.modify.use_rent_begin}
                        formatter={(value) => value + "시"}
                        onChange={(value) => props.handleInputChange("use_rent_begin", value)}
                      />
                    </span>
                    ~
                    <span className="numeric-wrap time">
                      <InputNumber
                        className="form-control mid"
                        step={1}
                        min={0}
                        max={24}
                        value={props.modify.use_rent_end}
                        formatter={(value) => value + "시"}
                        onChange={(value) => props.handleInputChange("use_rent_end", value)}
                      />
                    </span>
                  </div>
                </li>
                <li>
                  <h2>일일 매출 정산 시간 설정 </h2>
                  <div className="form-content li-label">
                    <div>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid"
                          value={props.modify.day_sale_end_time}
                          step={1}
                          min={1}
                          max={24}
                          formatter={(value) => value + "시"}
                          onChange={(value) => props.handleInputChange("day_sale_end_time", value)}
                        />
                      </span>
                    </div>
                  </div>
                </li>
                <li>
                  <h2>입실취소 가능 시간 설정 </h2>
                  <div className="form-content li-label">
                    <div>
                      체크인 후
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid"
                          value={props.modify.cancel_time}
                          step={5}
                          min={0}
                          max={60}
                          formatter={(value) => value + "분"}
                          onChange={(value) => props.handleInputChange("cancel_time", value)}
                        />
                      </span>
                      이내
                    </div>
                  </div>
                </li>
                <li>
                  <h2>퇴실취소 기능 시간 설정</h2>
                  <div className="form-content li-label">
                    <div>
                      퇴실 후
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid"
                          value={props.modify.check_out_cancle_time}
                          step={5}
                          min={0}
                          max={60}
                          formatter={(value) => value + "분"}
                          onChange={(value) => props.handleInputChange("check_out_cancle_time", value)}
                        />
                      </span>
                      이내
                    </div>
                  </div>
                </li>
                <li>
                  <h2>손님키 제거 시 퇴실 처리 지연 시간 설정</h2>
                  <div className="form-content li-label">
                    <div>
                      제거 시
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control mid"
                          value={props.modify.user_key_out_delay_time}
                          step={1}
                          min={0}
                          max={10}
                          formatter={(value) => value + "분"}
                          onChange={(value) => props.handleInputChange("user_key_out_delay_time", value)}
                        />
                      </span>
                      후 처리
                    </div>
                  </div>
                </li>
                <li>
                  <h2>객실이동 시 손님키 있는 객실</h2>
                  <div className="form-content li-label">
                    <ButtonGroup aria-label="room_move_no_key" onChange={(evt) => props.handleInputChange("room_move_no_key", evt.target.value)}>
                      <Radio inline name="room_move_no_key" value="0" defaultChecked={props.modify.room_move_no_key === 0} className={cx(props.modify.room_move_no_key === 0 && "checked-on")}>
                        <span />
                        포함
                      </Radio>
                      <Radio inline name="room_move_no_key" value="1" defaultChecked={props.modify.room_move_no_key === 1}>
                        <span />
                        제외
                      </Radio>
                    </ButtonGroup>
                  </div>
                </li>
                <li>
                  <h2>객실 타입명 보기 설정</h2>
                  <div className="form-content li-label">
                    <ButtonGroup aria-label="show_type_name" onChange={(evt) => props.handleInputChange("show_type_name", evt.target.value)}>
                      <Radio inline name="show_type_name" value="0" defaultChecked={props.modify.show_type_name === 0}>
                        <span />
                        숨기기
                      </Radio>
                      <Radio inline name="show_type_name" value="1" defaultChecked={props.modify.show_type_name === 1} className={cx(props.modify.show_type_name === 1 && "checked-on")}>
                        <span />
                        객실명 앞
                      </Radio>
                      <Radio inline name="show_type_name" value="2" defaultChecked={props.modify.show_type_name === 2} className={cx(props.modify.show_type_name === 2 && "checked-on")}>
                        <span />
                        객실명 뒤
                      </Radio>
                    </ButtonGroup>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
          <ButtonToolbar className="btn-room btn-set-close">
            <Button onClick={(evt) => props.onSave()}>저장</Button>
            <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
              닫기
            </Button>
          </ButtonToolbar>
        </div>
      </div>
    </div>
  );
};

export default SetTab02;
