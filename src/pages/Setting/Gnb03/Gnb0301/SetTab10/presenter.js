// imports.
import React from "react";

import { Radio, ButtonGroup, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab10 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030110">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <div>
                <h2>처리 확인 알림 팝업</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="speed_checkin">
                    <Radio
                      inline
                      name="speed_checkin"
                      value={0}
                      checked={props.modify.speed_checkin === 0}
                      onChange={(evt) => props.handleInputChange("speed_checkin", evt.target.value)}
                      className={cx(props.modify.speed_checkin === 0 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="speed_checkin" value={1} checked={props.modify.speed_checkin === 1} onChange={(evt) => props.handleInputChange("speed_checkin", evt.target.value)}>
                      <span />
                      사용안함(스피드 체크인)
                    </Radio>
                  </ButtonGroup>{" "}
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>화면 툴팁 도움말</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="tooltip_show">
                    <Radio
                      inline
                      name="tooltip_show"
                      value={1}
                      checked={props.modify.tooltip_show === 1}
                      onChange={(evt) => props.handleInputChange("tooltip_show", evt.target.value)}
                      className={cx(props.modify.tooltip_show === 1 && "checked-on")}
                    >
                      <span />
                      보기
                    </Radio>
                    <Radio
                      inline
                      name="tooltip_show"
                      value={2}
                      checked={props.modify.tooltip_show === 2}
                      onChange={(evt) => props.handleInputChange("tooltip_show", evt.target.value)}
                      className={cx(props.modify.tooltip_show === 2 && "checked-on")}
                    >
                      <span />
                      주요정보만 보기
                    </Radio>
                    <Radio
                      inline
                      name="tooltip_show"
                      value={0}
                      checked={props.modify.tooltip_show === 0}
                      onChange={(evt) => props.handleInputChange("tooltip_show", evt.target.value)}
                      className={cx(props.modify.tooltip_show === 0 && "checked-on")}
                    >
                      <span />
                      숨기기
                    </Radio>
                  </ButtonGroup>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>차량호출 기능</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="use_car_call">
                    <Radio
                      inline
                      name="use_car_call"
                      value={1}
                      checked={props.modify.use_car_call === 1}
                      onChange={(evt) => props.handleInputChange("use_car_call", evt.target.value)}
                      className={cx(props.modify.use_car_call === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="use_car_call" value={0} checked={props.modify.use_car_call === 0} onChange={(evt) => props.handleInputChange("use_car_call", evt.target.value)}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>{" "}
                </div>
              </div>
              <div>
                <h2>비상호출 기능</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="use_emerg">
                    <Radio
                      inline
                      name="use_emerg"
                      value={1}
                      checked={props.modify.use_emerg === 1}
                      onChange={(evt) => props.handleInputChange("use_emerg", evt.target.value)}
                      className={cx(props.modify.use_emerg === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="use_emerg" value={0} checked={props.modify.use_emerg === 0} onChange={(evt) => props.handleInputChange("use_emerg", evt.target.value)}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>{" "}
                </div>
              </div>
              <div>
                <h2>입실 취소 사유 입력 팝업</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="use_sale_cancel_comment">
                    <Radio
                      inline
                      name="use_sale_cancel_comment"
                      value={1}
                      checked={props.modify.use_sale_cancel_comment === 1}
                      onChange={(evt) => props.handleInputChange("use_sale_cancel_comment", evt.target.value)}
                      className={cx(props.modify.use_sale_cancel_comment === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio
                      inline
                      name="use_sale_cancel_comment"
                      value={0}
                      checked={props.modify.use_sale_cancel_comment === 0}
                      onChange={(evt) => props.handleInputChange("use_sale_cancel_comment", evt.target.value)}
                    >
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>{" "}
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>재 호출 지연 시간</h2>
                <div className="form-content li-label">
                  <div>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.modify.call_alert_term}
                        step={1}
                        min={0}
                        max={99}
                        formatter={(value) => value + "분"}
                        onChange={(value) => props.handleInputChange("call_alert_term", value)}
                      />
                    </span>
                    - 차량/비상호출 후 재 호출 시에 설정한 시간내에는 알림이 보이지 않습니다.
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab10;
