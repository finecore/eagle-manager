// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";

// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

// Components.
import Gnb0301 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      modify: {},
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  // 권한 체크.
  checkAuth = () => {
    const {
      user: { level },
    } = this.props.auth;

    // 매니저 권한 이상만 수정 가능.
    if (level > 2) {
      Swal.fire({
        icon: "warning",
        title: "권한 확인",
        html: `매니저 권한 이상만 수정 가능 합니다.`,
        confirmButtonText: "확인",
      });

      return false;
    }

    return true;
  };

  // 권한 체크.
  checkAuth = () => {
    const {
      user: { level },
    } = this.props.auth;

    // 매니저 권한 이상만 수정 가능.
    if (level > 2) {
      Swal.fire({
        icon: "warning",
        title: "권한 확인",
        html: `매니저 권한 이상만 수정 가능 합니다.`,
        confirmButtonText: "확인",
      });

      return false;
    }

    return true;
  };

  // 아이크루 어드민 여부.
  isAdmin = () => {
    const {
      user: { type, level },
    } = this.props.auth;

    console.log("- isAdmin", { type, level });

    return type === 1 && level < 3;
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value, save) => {
    let { modify } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.checkAuth()) return;

    // 아이템 정보 변경.
    modify[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleInputChange name", name, "value", modify[name]);

    this.setState({
      modify,
    });

    this.props.handleModifyPreferences(modify);

    if (save) this.onSave();
  };

  // 음성 옵션 변경 이벤트.
  handleVoiceOptionChange = (name, value) => {
    let { modify } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.checkAuth()) return;

    if (!modify["voice_opt"]) modify["voice_opt"] = {};

    modify["voice_opt"][name] = value === false ? 0 : value === true ? 1 : value;

    console.log("- handleVoiceOptionChange name", name, "value", modify["voice_opt"]);

    this.setState({
      modify,
    });

    this.props.handleModifyPreferences(modify);
  };

  // 컬러 변경 이벤트.
  handleColorChange = (type, name, value) => {
    let { modify } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.checkAuth()) return;

    if (!modify[type]) modify[type] = {};

    modify[type][name] = value;

    console.log("- handleColorChange name", name, "value", modify[type]);

    this.setState({
      modify,
    });

    this.props.handleModifyPreferences(modify);
  };

  onSave = (text, callback, confirm) => {
    const { modify } = this.state;
    const { item } = this.props.preferences;

    console.log("-- onSave", modify, confirm);

    let { id, place_id } = item;

    // 매니저 권한 이상만 수정 가능.
    if (!this.checkAuth()) return;

    if (!confirm && text !== "NO_ALERT") {
      Swal.fire({
        icon: "info",
        title: "저장 확인",
        html: text || `설정을 저장 하시겠습니까?`,
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: "예",
        cancelButtonText: "아니오",
      }).then((result) => {
        if (result.value) {
          this.onSave(text, callback, true);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          if (callback) callback(false);
        }
      });
    } else {
      let modifyClone = { id, place_id };

      // 변경 항목만 업데이트
      _.map(modify, (v, k) => {
        v = typeof v === "object" ? JSON.stringify(v) : v; // 객체는 저장 시 string 형식으로 변환.
        if (String(v) !== String(item[k])) modifyClone[k] = v;
      });

      console.log("- modifyClone", modifyClone);

      this.props.handleSavePreferences(modifyClone.id, { preferences: modifyClone });

      // 음성 재생 속도가 변경 되었다면 새로 만든다.
      if (modifyClone.voice_rate) {
        let { list } = this.props.room;

        let voices = [];

        voices.push({ name: "on", text: "음성 지원 기능을 시작 합니다." });
        voices.push({ name: "off", text: "음성 지원 기능을 중지 합니다." });
        voices.push({ name: "door_0", text: "문이 닫혔습니다." });
        voices.push({ name: "door_1", text: "문이 열렸습니다." });
        voices.push({ name: "clean_0", text: "청소 완료" });
        voices.push({ name: "clean_1", text: "청소 요청" });
        voices.push({ name: "outing_0", text: "외출 복귀" });
        voices.push({ name: "outing_1", text: "외출" });
        voices.push({ name: "key_1", text: "고객키가 삽입 되었습니다." });
        voices.push({ name: "key_2", text: "마스터키가 삽입 되었습니다." });
        voices.push({ name: "key_3", text: "청소키가 삽입 되었습니다." });
        voices.push({ name: "key_4", text: "고객키가 제거 되었습니다." });
        voices.push({ name: "key_5", text: "마스터키가 제거 되었습니다." });
        voices.push({ name: "key_6", text: "청소키가 제거 되었습니다." });
        voices.push({ name: "sale_1", text: "숙박" });
        voices.push({ name: "sale_2", text: "대실" });
        voices.push({ name: "sale_3", text: "장기" });
        voices.push({ name: "check_in", text: "입실" });
        voices.push({ name: "check_out", text: "퇴실" });
        voices.push({ name: "auto_check_in", text: "자동 입실" });
        voices.push({ name: "auto_check_out", text: "자동 퇴실" });
        voices.push({ name: "inspect_1", text: "점검대기" });
        voices.push({ name: "inspect_2", text: "점검 중" });
        voices.push({ name: "inspect_3", text: "점검 완료" });
        voices.push({ name: "main_relay_0", text: "전원이 차단 되었습니다." });
        voices.push({ name: "main_relay_1", text: "전원이 공급 되었습니다." });
        voices.push({ name: "car_call_1", text: "배차 요청 입니다." });
        voices.push({ name: "car_call_2", text: "배차 완료 되었습니다." });
        voices.push({ name: "car_call_3", text: "배차 요청이 취소 되었습니다." });

        // 모든 객실 번호 체크(없으면 서버에 생성)
        _.map(list, (item) => {
          let text = item.name + (isNaN(item.name) ? "" : " 호, ");
          voices.push({ name: "room_" + item.id, text });
        });

        this.props.makeVoiceFileList(voices, true);
      }

      // 수정 사항 로드.
      setTimeout(() => {
        this.props.initPreferences(place_id);
        // this.props.initAllRooms(place_id); // 객실 정보 조회.
        if (callback) callback(true);
      }, 100);
    }
  };

  onCancel = () => {
    this.closeModal();
  };

  onConfirm = (confirm) => {
    console.log("- onConfirm", confirm);

    const { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false,
      },
      () => {
        setTimeout(() => {
          if (alert.callback) alert.callback(_.cloneDeep(confirm));
          this.setState({
            confirm: false,
          });
        }, 100);
      }
    );
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    this.setState({
      modify: this.props.preferences.modify,
    });
  };

  componentWillUnmount() {
    const {
      user: { place_id },
    } = this.props.auth;

    this.props.initPreferences(place_id);
    this.props.initAllRooms(place_id); // 객실정보 재조회.
    this.props.initAllRoomViewItems(place_id);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      preferences: {
        modify: { speed_checkin },
      },
    } = this.props;

    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    if (nextProps.preferences && nextProps.preferences.modify !== this.props.preferences.modify) {
      // console.log(
      //   "-- Setting > Gnb0301 > componentWillReceiveProps preferences modify",
      //   JSON.stringify(nextProps.preferences.modify) !==
      //     JSON.stringify(this.props.preferences.modify)
      // );

      this.setState({
        modify: nextProps.preferences.modify,
      });
    }

    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      // console.log(
      //   "-- Setting > Gnb0301 > componentWillReceiveProps preferences",
      //   JSON.stringify(nextProps.preferences.item) !== JSON.stringify(this.props.preferences.item)
      // );

      let {
        alert: { close },
      } = this.state;

      // 스피드 체크인 사용 시 확인창 닫음.
      let dialog = speed_checkin ? false : !close;

      // 수정 시.
      if (JSON.stringify(nextProps.preferences.item) !== JSON.stringify(this.props.preferences.item)) {
        this.setState({
          confirm: false,
          dialog: false,
        });
      }
    }
  }

  render() {
    return this.state.modify && this.state.modify.id ? (
      <Gnb0301
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        handleVoiceOptionChange={this.handleVoiceOptionChange}
        handleColorChange={this.handleColorChange}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
        checkAuth={this.checkAuth}
        isAdmin={this.isAdmin}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
