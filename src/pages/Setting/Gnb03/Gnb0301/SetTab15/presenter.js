// imports.
import React from "react";

import { ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab15 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030115">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>포인트 적립/사용 설정</h2>
              <div className="form-content">
                <ButtonGroup aria-label="point_use_type" onChange={(evt) => props.handleInputChange("point_use_type", evt.target.value)}>
                  <Radio inline name="point_use_type" value="1" defaultChecked={props.modify.point_use_type === 1} className={cx(props.modify.point_use_type === 1 && "checked-on")}>
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="point_use_type" value="0" defaultChecked={props.modify.point_use_type === 0} className={cx(props.modify.point_use_type === 0 && "checked-on")}>
                    <span />
                    사용 안함
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2>포인트 사용 옵션 설정</h2>
              <div className="form-content">
                <ButtonGroup aria-label="point_pay_type" onChange={(evt) => props.handleInputChange("point_pay_type", evt.target.value)}>
                  <Radio inline name="point_pay_type" value="1" defaultChecked={props.modify.point_pay_type === 1} className={cx(props.modify.point_pay_type === 1 && "checked-on")}>
                    <span />
                    포인트 단독 사용(포인트로만 결제 가능)
                  </Radio>
                  <Radio inline name="point_pay_type" value="2" defaultChecked={props.modify.point_pay_type === 2} className={cx(props.modify.point_pay_type === 2 && "checked-on")}>
                    <span />
                    현금/카드 혼합 사용(잔여 포인트 사용 가능)
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2>적립 포인트 비율 설정</h2>
              <div className="form-content">
                <div>
                  현금 결제 금액의
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.cash_point_rate}
                      step={1}
                      min={0}
                      max={99}
                      onChange={(value) => props.handleInputChange("cash_point_rate", value)}
                      formatter={(value) => value + " %"}
                    />
                  </span>
                  적립
                </div>
              </div>
              <div className="form-content">
                <div>
                  카드 결제 금액의
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.card_point_rate}
                      step={1}
                      min={0}
                      max={99}
                      onChange={(value) => props.handleInputChange("card_point_rate", value)}
                      formatter={(value) => value + " %"}
                    />
                  </span>
                  적립
                </div>
              </div>
              <div className="form-content">
                <div>
                  무인 결제 시
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.isg_point_rate}
                      step={1}
                      min={0}
                      max={99}
                      onChange={(value) => props.handleInputChange("isg_point_rate", value)}
                      formatter={(value) => value + " %"}
                    />
                  </span>
                  추가 적립
                </div>
              </div>
              <div className="form-content">
                <div>
                  예약 결제 시
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.reserv_point_rate}
                      step={1}
                      min={0}
                      max={99}
                      onChange={(value) => props.handleInputChange("reserv_point_rate", value)}
                      formatter={(value) => value + " %"}
                    />
                  </span>
                  추가 적립 (OTA 예약 제외)
                </div>
              </div>
            </li>
            {/* <li>
              <h2>체크인 시 추가 적립 포인트 설정</h2>
              <div className="form-content">
                <div>
                  체크인 시
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.pay_add_point}
                      step={100}
                      onChange={value => props.handleInputChange("pay_add_point", value)}
                      formatter={value => value}
                    />
                  </span>
                  포인트 추가 적립
                </div>
              </div>
            </li> */}
            {/* <li>
              <h2>체크인 시 적립 포인트 수정 기능</h2>
              <div className="form-content">
                <ButtonGroup aria-label="point_add_use" onChange={evt => props.handleInputChange("point_add_use", evt.target.value)}>
                  <Radio inline name="point_add_use" value="1" defaultChecked={props.modify.point_add_use === 1} className={cx(props.modify.point_add_use === 1 && "checked-on")}>
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="point_add_use" value="0" defaultChecked={props.modify.point_add_use === 0} className={cx(props.modify.point_add_use === 0 && "checked-on")}>
                    <span />
                    사용 안함
                  </Radio>
                </ButtonGroup>
              </div>
            </li> */}
            <li>
              <h2>포인트 적립 시 휴대폰번호 미입력 알림</h2>
              <div className="form-content">
                <ButtonGroup aria-label="point_check_phone" onChange={(evt) => props.handleInputChange("point_check_phone", evt.target.value)}>
                  <Radio inline name="point_check_phone" value="1" defaultChecked={props.modify.point_check_phone === 1} className={cx(props.modify.point_check_phone === 1 && "checked-on")}>
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="point_check_phone" value="0" defaultChecked={props.modify.point_check_phone === 0}>
                    <span />
                    사용 안함
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab15;
