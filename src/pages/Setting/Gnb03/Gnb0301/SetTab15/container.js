// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab15 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = { modify: PropTypes.object.isRequired };

  render() {
    return <SetTab15 {...this.props} {...this.state} />;
  }
}

export default withSnackbar(Container);
