// imports.
import React from "react";
import _ from "lodash";

// Components.
import CardDeckView from "./CardDeckView";

import { FormGroup, FormControl, ControlLabel, ButtonToolbar, Button, ButtonGroup, Radio, Checkbox } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

import swal from "sweetalert";
import Swal from "sweetalert2";

// styles.
import "./styles.scss";

const SetTab09 = (props) => {
  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div>
      {/* 카드데크 키 현황 보기 팝업 */}
      {props.iscSet.displayKeyBox && <CardDeckView serialno={props.device.serialno} />}

      <div className="setting-form" id="gnb030109">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>무인 판매기 선택</h2>
              <div className="form-content">
                <FormGroup>
                  <FormControl
                    componentClass="select"
                    placeholder="장치 선택"
                    onChange={(evt) => {
                      let device = _.find(props.isc, { serialno: evt.target.value }) || {};
                      props.onSelDevice(device);
                    }}
                  >
                    {_.map(props.isc, (device) => (
                      <option key={device.id} value={device.serialno}>
                        [{device.maker}] {device.name}
                      </option>
                    ))}
                  </FormControl>
                </FormGroup>
              </div>
            </li>
            <li>
              <h2>{props.device.name} 무인 판매 사용</h2>
              {/* 판매 중지(0:판매, 1:중지) */}
              <div className="form-content">
                <ButtonGroup aria-label="sale_stop">
                  <Radio
                    inline
                    name="sale_stop"
                    value={0}
                    checked={props.device.sale_stop === 0}
                    className={cx(props.device.sale_stop === 0 && "checked-on")}
                    onChange={(evt) => props.handleDeviceChange("sale_stop", Number(evt.target.value))}
                  >
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="sale_stop" value={1} checked={props.device.sale_stop === 1} onChange={(evt) => props.handleDeviceChange("sale_stop", Number(evt.target.value))}>
                    <span />
                    중지
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2> 카드데크 키 현황</h2>
              <ButtonToolbar className="btn-room">
                <Button onClick={(evt) => props.onOpenKeyBoxDisplay(evt)}>카드데크 키 현황 보기</Button>
              </ButtonToolbar>
            </li>
            <li>
              <h2> 성인 인증모드 설정</h2>
              <div className="form-content">
                <div>
                  <ControlLabel htmlFor="gnb030109-02">성인 인증모드</ControlLabel>
                  <Radio
                    inline
                    name="minor_mode"
                    value={1}
                    checked={props.selIscState.minor_mode === 1}
                    className={cx(props.selIscState.minor_mode === 1 && "checked-on")}
                    onChange={(evt) => props.handleIscStateChange("minor_mode", evt.target.value)}
                  >
                    <span />
                    사용
                  </Radio>
                  <Radio
                    inline
                    name="minor_mode"
                    value={0}
                    checked={props.selIscState.minor_mode === 0}
                    className={cx(props.selIscState.minor_mode === 0 && "checked-on")}
                    onChange={(evt) => props.handleIscStateChange("minor_mode", evt.target.value)}
                  >
                    <span />
                    해제
                  </Radio>
                </div>
                <div>
                  <ControlLabel htmlFor="gnb030109-02">1회용 성인인증</ControlLabel>
                  <Radio
                    inline
                    name="minor_auto_disable"
                    value={1}
                    checked={props.selIscState.minor_auto_disable === 1}
                    className={cx(props.selIscState.minor_auto_disable === 1 && "checked-on")}
                    onChange={(evt) => props.handleIscStateChange("minor_auto_disable", evt.target.value)}
                  >
                    <span />
                    사용
                  </Radio>
                  <Radio
                    inline
                    name="minor_auto_disable"
                    value={0}
                    checked={props.selIscState.minor_auto_disable === 0}
                    onChange={(evt) => props.handleIscStateChange("minor_auto_disable", evt.target.value)}
                  >
                    <span />
                    사용안함
                  </Radio>
                  <span className="comment">
                    (성인인증 완료시 자동으로 성인 인증모드 [<font color="red">해제</font>])
                  </span>
                </div>
              </div>
            </li>
            <li>
              <h2> 차량번호 입력 사용</h2>
              {/* 판매 중지(0:사용안함, 1:사용) */}
              <div className="form-content">
                <ButtonGroup aria-label="car_no_use_yn">
                  <Radio
                    inline
                    name="car_no_use_yn"
                    value={1}
                    checked={props.selIscSet.car_no_use_yn === 1}
                    className={cx(props.selIscSet.car_no_use_yn === 1 && "checked-on")}
                    onChange={(evt) => props.handleIscSetChange("car_no_use_yn", Number(evt.target.value))}
                  >
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="car_no_use_yn" value={0} checked={props.selIscSet.car_no_use_yn === 0} onChange={(evt) => props.handleIscSetChange("car_no_use_yn", Number(evt.target.value))}>
                    <span />
                    사용안함
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2> 지폐방출기 설정</h2>
              <div className="form-content">
                <div>
                  <ControlLabel htmlFor="gnb030109-02">좌측 잔여지폐 수</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.selIscSet.bds_l_change_count || 0}
                      step={1}
                      min={0}
                      max={1000}
                      formatter={(value) => value + " 장"}
                      onChange={(value) => props.handleIscSetChange("bds_l_change_count", value)}
                    />
                  </span>
                </div>
                <div>
                  <ControlLabel htmlFor="gnb030109-02">우측 잔여지폐 수</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.selIscSet.bds_r_change_count || 0}
                      step={1}
                      min={0}
                      max={1000}
                      formatter={(value) => value + " 장"}
                      onChange={(value) => props.handleIscSetChange("bds_r_change_count", value)}
                    />
                  </span>
                </div>
              </div>
            </li>
            <li>
              <h2> 영수증프린터 설정</h2>
              <div className="form-content">
                <ButtonGroup aria-label="rcp_ask_print">
                  <ControlLabel>결제후 영수증 출력 여부</ControlLabel>
                  <Radio
                    inline
                    name="rcp_ask_print"
                    value={0}
                    checked={props.selIscSet.rcp_ask_print === 0}
                    className={cx(props.selIscSet.rcp_ask_print === 0 && "checked-on")}
                    onChange={(evt) => props.handleIscSetChange("rcp_ask_print", evt.target.value)}
                  >
                    <span />
                    결제후 묻기
                  </Radio>
                  <Radio
                    inline
                    name="rcp_ask_print"
                    value={1}
                    checked={props.selIscSet.rcp_ask_print === 1}
                    className={cx(props.selIscSet.rcp_ask_print === 1 && "checked-on")}
                    onChange={(evt) => props.handleIscSetChange("rcp_ask_print", evt.target.value)}
                  >
                    <span />
                    무조건 출력
                  </Radio>
                </ButtonGroup>
              </div>
              <div className="form-content">
                <FormGroup controlId="formUserName">
                  <ControlLabel>영수증 하단 추가 메세지</ControlLabel>
                  <FormControl type="text" placeholder="메세지 입력" value={props.selIscSet.rcp_comment || ""} onChange={(evt) => props.handleIscSetChange("rcp_comment", evt.target.value)} />
                </FormGroup>
              </div>
            </li>
            <li>
              <h2> 무인 관리자 비밀번호 변경</h2>
              <div className="form-content">
                <FormGroup controlId="formPwd">
                  <ControlLabel>새로운 비밀번호 숫자만 입력</ControlLabel>
                  <FormControl
                    type="password"
                    placeholder="비밀번호 입력"
                    value={props.device.pwd || ""}
                    onChange={(evt) => props.handleDeviceChange("pwd", evt.target.value)}
                    disabled={!props.checkAuth()}
                  />
                </FormGroup>
              </div>
            </li>
          </ul>
          <ul className="list-bul">
            <li>
              <h2>전체 무인 자판기 사용 여부</h2>
              {/* 판매기 판매 여부 (0:허용, 1:중지) */}
              <div className="form-content">
                <ButtonGroup aria-label="isc_sale">
                  <Radio
                    inline
                    name="isc_sale"
                    value={0}
                    checked={props.modify.isc_sale === 0}
                    className={cx(props.modify.isc_sale === 0 && "checked-on")}
                    onChange={(evt) => props.handleInputChange("isc_sale", Number(evt.target.value))}
                  >
                    <span />
                    사용
                  </Radio>
                  <Radio inline name="isc_sale" value={1} checked={props.modify.isc_sale === 1} onChange={(evt) => props.handleInputChange("isc_sale", Number(evt.target.value))}>
                    <span />
                    사용안함
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
            <li>
              <h2>전체 무인 판매 허용 타입</h2>
              {/* 숙박,대실,예약 무인 판매 여부(0: 허용, 1:중지) */}
              <div className="form-content">
                <Checkbox
                  inline
                  className="checkbox04"
                  checked={props.modify.isc_sale_1 === 0 && props.modify.isc_sale === 0}
                  onChange={(evt) => props.handleInputChange("isc_sale_1", evt.target.checked ? 0 : 1)}
                  disabled={props.modify.isc_sale === 1}
                >
                  <span />
                  숙박 판매
                </Checkbox>
                <Checkbox
                  inline
                  className="checkbox04"
                  checked={props.modify.isc_sale_2 === 0 && props.modify.isc_sale === 0}
                  onChange={(evt) => props.handleInputChange("isc_sale_2", evt.target.checked ? 0 : 1)}
                  disabled={props.modify.isc_sale === 1}
                >
                  <span />
                  대실 판매
                </Checkbox>
                <Checkbox
                  inline
                  className="checkbox04"
                  checked={props.modify.isc_sale_3 === 0 && props.modify.isc_sale === 0}
                  onChange={(evt) => props.handleInputChange("isc_sale_3", evt.target.checked ? 0 : 1)}
                  disabled={props.modify.isc_sale === 1}
                >
                  <span />
                  예약 판매
                </Checkbox>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onDeviceSave(evt)}>저장</Button>
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab09;
