// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as iscKeyBoxAction } from "actions/iscKeyBox";
import { actionCreators as iscSetAction } from "actions/iscSet";
import { actionCreators as layoutAction } from "actions/layout";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { iscKeyBox, device } = state;
  return {
    iscKeyBox,
    device,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initIscKeyBox: (serialno) => dispatch(iscKeyBoxAction.getKeyBoxList(serialno)),

    handleKeyBoxDisplayLayer: (displayKeyBox) => dispatch(iscSetAction.setIscKeyBoxLayer(displayKeyBox)),

    handleDisplayLayer: (displayLayer, props) => dispatch(layoutAction.setDisplayLayer(displayLayer, props)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
