// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Wave } from "better-react-spinkit";
import cx from "classnames";
import _ from "lodash";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import CardDeckView from "./presenter";

class Container extends Component {
  static propTypes = {
    iscKeyBox: PropTypes.object.isRequired,
    serialno: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      keyBoxs: [[], []],
      loading: true,
    };
  }

  onCloseKeyBoxDisplay = (evt) => {
    this.props.handleKeyBoxDisplayLayer(false);
    console.log("- onCloseKeyBoxDisplay", this.props.closeModal);
    // if (!this.props.closeModal) this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const { serialno } = this.props;

    this.props.initIscKeyBox(serialno);
  };

  componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.iscKeyBox && nextProps.iscKeyBox.list.length && nextProps.iscKeyBox.list !== this.props.iscKeyBox.list) {
      const {
        iscKeyBox: { list },
      } = nextProps;

      console.log("- list", list);

      let { keyBoxs } = this.state;

      for (let i = 0; i < 76; i++) {
        keyBoxs[0][i] = {};
        keyBoxs[1][i] = {};
      }

      _.map(list, (box, i) => {
        keyBoxs[box.gid - 1][box.did - 1] = box;
      });

      console.log("- keyBoxs", keyBoxs);

      this.setState({
        keyBoxs,
      });
    }
  }

  render() {
    return this.state.loading ? (
      <CardDeckView {...this.props} {...this.state} onCloseKeyBoxDisplay={this.onCloseKeyBoxDisplay} />
    ) : (
      <div className={cx("loader")}>
        <Wave color="white" size={60} />
      </div>
    );
  }
}

export default withSnackbar(Container);
