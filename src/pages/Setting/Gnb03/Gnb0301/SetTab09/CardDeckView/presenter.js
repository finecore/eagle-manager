// imports.
import React from "react";
import Modal from "react-awesome-modal";
import _ from "lodash";

import { Table } from "react-bootstrap";

// styles.
import "./styles.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCreditCard } from "@fortawesome/free-regular-svg-icons";
import { faQuestion, faTimes } from "@fortawesome/free-solid-svg-icons";

// functional component
const CardDeckView = (props) => {
  return (
    <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeFeeLayer(evt)}>
      <article className="card-deck-set" id="card-deck-pop">
        <h1>카드데크 키 현황 보기</h1>
        <div>
          <div className="comment-box">
            <strong className="txt-question">
              <FontAwesomeIcon icon={faQuestion} />
            </strong>{" "}
            : 미 등록 바코드
            <strong className="txt-signal">
              <FontAwesomeIcon icon={faTimes} />
            </strong>{" "}
            : 통신 단절
            <strong className="txt-sensor">
              <FontAwesomeIcon icon={faCreditCard} />
            </strong>{" "}
            : 카드 있음
          </div>
          <div className="card-deck-wrapper">
            <div className="card-deck-layout">
              <div className="form-content count-table">
                <Table>
                  <thead>
                    <tr>
                      <th colSpan="2">카드 데크 1 (좌측)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      {_.map(props.keyBoxs[0], (box, i) => (
                        <React.Fragment key={i}>
                          {
                            <td>
                              {box.sensor ? (
                                <strong className="txt-sensor">
                                  <FontAwesomeIcon icon={faCreditCard} />
                                </strong>
                              ) : null}
                              <strong>{box.name}</strong>
                              {!box.signal ? (
                                <strong className="txt-signal">
                                  <FontAwesomeIcon icon={faTimes} />
                                </strong>
                              ) : null}
                              {!box.name && box.sensor ? (
                                <strong className="txt-question">
                                  <FontAwesomeIcon icon={faQuestion} />
                                </strong>
                              ) : null}

                              <span>{i + 1}</span>
                            </td>
                          }
                        </React.Fragment>
                      ))}
                    </tr>
                  </tbody>
                </Table>
              </div>
            </div>

            <div className="card-deck-layout">
              <div className="form-content count-table">
                <Table>
                  <thead>
                    <tr>
                      <th colSpan="2">카드 데크 2 (우측)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      {_.map(props.keyBoxs[1], (box, i) => (
                        <React.Fragment key={i}>
                          {
                            <td>
                              {box.sensor ? (
                                <strong className="txt-sensor">
                                  <FontAwesomeIcon icon={faCreditCard} />
                                </strong>
                              ) : null}
                              <strong>{box.name}</strong>
                              {!box.signal ? (
                                <strong className="txt-signal">
                                  <FontAwesomeIcon icon={faTimes} />
                                </strong>
                              ) : null}
                              {!box.name && box.sensor ? (
                                <strong className="txt-question">
                                  <FontAwesomeIcon icon={faQuestion} />
                                </strong>
                              ) : null}

                              <span>{i + 1}</span>
                            </td>
                          }
                        </React.Fragment>
                      ))}
                    </tr>
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
        <a onClick={(evt) => props.onCloseKeyBoxDisplay(evt)} className="pop-close">
          Close
        </a>
      </article>
    </Modal>
  );
};

export default CardDeckView;
