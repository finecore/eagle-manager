// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab09 from "./presenter";

class Container extends Component {
  static propTypes = {
    device: PropTypes.object.isRequired,
    iscSet: PropTypes.object.isRequired,
    iscKeyBox: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isc: [],
      device: {},
      parts: [], // 장비 파트.
      iscStates: [],
      orgIscSet: {},
      orgIscState: {},
      selIscSet: {},
      selIscState: {},
      alert: {
        type: "info",
        title: "확인",
        text: "",
        cancel: true,
        confirm: true,
        callback: undefined,
      },
      dialog: false,
      confirm: false,
    };
  }

  // 자판기 선택
  onSelDevice = (device) => {
    const { parts, iscStates } = this.state;

    const { id, serialno, name, maker, type, model, sale_stop } = _.cloneDeep(device || {});

    let selIscSet = _.find(parts, { serialno }) || { serialno, rcp_ask_print: 0 };
    let selIscState = _.find(iscStates, { serialno }) || { serialno };

    console.log("- onSelDevice", serialno, { selIscSet, selIscState });

    this.setState({
      device: { id, pwd: "", serialno, name, maker, type, model, sale_stop },
      selIscSet,
      selIscState,
      orgIscSet: _.cloneDeep(selIscSet),
      orgIscState: _.cloneDeep(selIscState),
    });
  };

  // 장비 정보 변경 이벤트.
  handleDeviceChange = (name, value) => {
    let { device } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    device[name] = value;

    console.log("- handleDeviceChange name", name, "value", device[name], device);

    this.setState({
      device,
    });
  };

  // 파츠 정보 변경 이벤트.
  handleIscSetChange = (name, value) => {
    let { selIscSet } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    selIscSet[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleIscSetChange name", name, "value", selIscSet[name], selIscSet);

    this.setState({
      selIscSet,
    });
  };

  // 상태 변경 이벤트.
  handleIscStateChange = (name, value) => {
    let { selIscState } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    selIscState[name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleIscStateChange name", name, "value", selIscState[name], selIscState);

    this.setState({
      selIscState,
    });
  };

  onOpenKeyBoxDisplay = (evt) => {
    this.props.handleKeyBoxDisplayLayer(true);
  };

  onCloseKeyBoxDisplay = (evt) => {
    this.props.handleKeyBoxDisplayLayer(false);
  };

  onDeviceSave = () => {
    let { confirm, device, selIscSet, selIscState, orgIscSet, orgIscState } = this.state;

    console.log("-- onDeviceSave", { device, selIscSet, selIscState });

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    if (!confirm) {
      this.setState({
        alert: {
          title: "저장 확인",
          text: "무인 설정을 저장 하시겠습니까?",
          type: "info",
          cancel: true,
          confirm: true,
          close: false,
          callback: (confirm) => {
            if (confirm) this.onDeviceSave();
          },
        },
        dialog: true,
      });
    } else {
      // 비밀번호 값 없으면 제외.
      if (!device.pwd) delete device.pwd;

      this.props.handleSaveDevice(device.id, { device });

      let isc_set = {};

      _.map(selIscSet, (v, k) => {
        console.log(k, v, orgIscSet[k]);
        if (v !== orgIscSet[k]) isc_set[k] = v;
      });

      console.log("-- isc_set ", isc_set);

      // 장치 설정 업데이트.
      if (selIscSet.id) {
        if (!_.isEmpty(isc_set)) {
          isc_set.id = selIscSet.id;

          this.props.handleIscSetSave(isc_set.id, { isc_set });
        }
      } else {
        isc_set.serialno = selIscSet.serialno;

        this.props.handleIscSetNew({ isc_set });
      }

      let isc_state = {};

      _.map(selIscState, (v, k) => {
        console.log(k, v, orgIscState[k]);
        if (v !== orgIscState[k]) isc_state[k] = v;
      });

      console.log("-- isc_state ", isc_state);

      // 장치 상태 업데이트.
      if (orgIscState.channel) {
        if (!_.isEmpty(isc_state)) {
          isc_state.serialno = selIscState.serialno;

          if (isc_state.start_time) isc_state.start_time = moment(isc_state.start_time).format("YYYY-MM-DD HH:mm");

          this.props.handleSaveIscState(isc_state.serialno, { isc_state });
        }
      } else {
        isc_state.serialno = selIscState.serialno;

        this.props.handleNewIscState({ isc_state });
      }

      // 환경 설정 업데이트.
      this.props.onSave("NO_ALERT");
    }
  };

  onConfirm = (confirm) => {
    const { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false,
      },
      () => {
        setTimeout(() => {
          if (alert.callback) alert.callback(_.cloneDeep(confirm));
          this.setState({
            confirm: false,
          });
        }, 100);
      }
    );
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    // 자판기 목록
    let {
      preferences: { place_id },
      device: { isc },
    } = this.props;

    // 장비 설정 목록 조회.
    this.props.initIscSets(place_id);
    // 무인 자판기 상태 목록 조회.
    this.props.initIscStates(place_id);

    this.setState({
      isc,
      device: isc[0] || {},
    });
  };

  componentWillUnmount = () => {};

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.iscSet && nextProps.iscSet !== this.props.iscSet) {
      const {
        iscSet: { list },
      } = nextProps;

      this.setState(
        {
          parts: list,
        },
        () => {
          this.onSelDevice(this.state.device);
        }
      );
    }

    if (nextProps.iscState && nextProps.iscState !== this.props.iscState) {
      const { list: iscStates } = nextProps.iscState;

      console.log("- iscState", iscStates);

      this.setState(
        {
          iscStates,
        },
        () => {
          this.onSelDevice(this.state.device);
        }
      );
    }
  }

  render() {
    return this.state.selIscSet.serialno && this.state.selIscState.channel ? (
      <SetTab09
        {...this.props}
        {...this.state}
        onSelDevice={this.onSelDevice}
        handleDeviceChange={this.handleDeviceChange}
        handleIscSetChange={this.handleIscSetChange}
        handleIscStateChange={this.handleIscStateChange}
        onOpenKeyBoxDisplay={this.onOpenKeyBoxDisplay}
        onCloseKeyBoxDisplay={this.onCloseKeyBoxDisplay}
        onDeviceSave={this.onDeviceSave}
        onConfirm={this.onConfirm}
        closeModal={this.closeModal}
      />
    ) : null;
  }
}

export default withSnackbar(Container);
