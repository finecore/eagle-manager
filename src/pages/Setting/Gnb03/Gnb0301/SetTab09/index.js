// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as iscSetAction } from "actions/iscSet";
import { actionCreators as deviceAction } from "actions/device";
import { actionCreators as iscStateAction } from "actions/iscState";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { preferences, device, iscKeyBox, iscSet, iscState } = state;
  return {
    preferences: preferences.item,
    device,
    iscKeyBox,
    iscSet,
    iscState,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),

    handleSaveDevice: (id, device) => dispatch(deviceAction.putDevice(id, device)),

    initIscSets: (place_id) => dispatch(iscSetAction.getIscSets(place_id)),

    handleIscSetSave: (id, isc_set) => dispatch(iscSetAction.putIscSet(id, isc_set)),

    handleIscSetNew: (isc_set) => dispatch(iscSetAction.newIscSet(isc_set)),

    initIscStates: (place_id) => dispatch(iscStateAction.getIscStateList(place_id)),

    handleSaveIscState: (serialno, isc_state) => dispatch(iscStateAction.putIscState(serialno, isc_state)),

    handleNewIscState: (isc_state) => dispatch(iscStateAction.newIscState(isc_state)),

    handleKeyBoxDisplayLayer: (displayKeyBox) => dispatch(iscSetAction.setIscKeyBoxLayer(displayKeyBox)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
