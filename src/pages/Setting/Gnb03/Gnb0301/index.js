// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as seasonPremiumAction } from "actions/seasonPremium";
import { actionCreators as preferencesAction } from "actions/preferences";
import { actionCreators as voiceAction } from "actions/voice";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room, error, subscribe, placeSubscribe } = state;
  return {
    auth,
    user: auth.user,
    room,
    preferences,
    error,
    subscribe,
    placeSubscribe,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllSeasonPremium: (palceId) => dispatch(seasonPremiumAction.getSeasonPremiumList(palceId)),
    initPreferences: (palceId) => dispatch(preferencesAction.getPreferenceList(palceId)),
    initAllRooms: (palceId, callback) => dispatch(roomAction.getRoomList(palceId, callback)),
    initAllRoomViewItems: (palceId) => dispatch(roomViewItemAction.getPlaceRoomViewItemList(palceId)),
    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),
    handleModifyPreferences: (modify) => dispatch(preferencesAction.setModify(modify)),
    makeVoiceFileList: (voices, reset, callback) => dispatch(voiceAction.makeVoiceFileList(voices, reset, callback)),
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
