// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";
import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";

import cx from "classnames";

// Components.
import SetTab13 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscribesAll: [], // 구독 서비스 목록.
      subscribesRemain: [], // 구독 가능한 서비스 목록.
      placeSubscribes: [], // 업소 구독 목록.
      org: {}, // 수정 시 사용 할 원래 아이디값.
      data: {},
      idx: -1,
      isNew: false,
      isChangePwd: false,
      isChangeDate: false,
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    subscribe: PropTypes.object.isRequired,
    placeSubscribe: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { data, idx, isNew, subscribesAll } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    data[name] = value;

    if (name === "subscribe_id") {
      let subscribe = _.find(subscribesAll, { id: value });
      data = _.merge({}, data, subscribe);
    }

    // 수정 아니고 입력값이 있으면 신규 설정.
    if (idx === -1 && !isNew) isNew = true;

    this.setState({
      data,
      isNew,
    });
  };

  onSelect = (placeSubscribe, idx) => {
    console.log("-- onSelect", placeSubscribe, idx);

    this.setState({
      isNew: false,
      isChangePwd: false,
      isChangeDate: false,
      org: placeSubscribe,
      data: _.cloneDeep(placeSubscribe),
      idx,
    });
  };

  onAdd = () => {
    console.log("-- onAdd");

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    if (!this.state.subscribesRemain.length) {
      Swal.fire({
        icon: "info",
        title: "구독 알림",
        html: "<font color='red'>이미 모든 서비스를 구독 중 입니다.</font>",
        confirmButtonText: "확인",
      }).then((result) => {
        console.log("- result", result);
        if (result.value) {
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });

      return;
    }

    this.setState({
      isNew: true,
      idx: -1,
      org: {},
      data: {
        apply_begin_date: moment(),
        apply_end_date: moment().add(1, "month"),
        trial_end_date: moment(),
        discount: 0,
      },
    });
  };

  onDelete = () => {
    const { data, idx, confirm } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let place_subscribe = data;

    console.log("-- onDelete", place_subscribe, confirm);

    if (idx === -1) {
      Swal.fire({
        icon: "warning",
        title: "선택 확인",
        html: "삭제 할 구독을 선택해 주세요.",
        confirmButtonText: "확인",
      }).then((result) => {
        console.log("- result", result);
        if (result.value) {
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });

      return;
    }

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: "[" + place_subscribe.name + "] 구독을 정말 삭제 하시겠습니까?",
      showCancelButton: true,
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      console.log("- result", result);
      if (result.value) {
        this.props.handleDeletePlaceSubscribe(place_subscribe.place_id, place_subscribe.id);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  // 달력
  onFocusChange = ({ focused1, focused2, focused3 }) => {
    let state = _.merge({}, this.state, { focused1, focused2, focused3 });
    this.setState({ ...state });
  };

  onChangeDate = () => {
    this.setState({ isChangeDate: !this.state.isChangeDate });
  };

  onSave = () => {
    const {
      preferences: { place_id },
    } = this.props;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let { org, data, isNew } = this.state;

    isNew = !org.id;

    console.log("-- onSave", data, isNew);

    if (!data.subscribe_id || !data.apply_begin_date || !data.apply_end_date || !data.trial_end_date) {
      Swal.fire({
        icon: "warning",
        title: "입력값 확인",
        html: (!data.subscribe_id ? "구독 서비스는" : !data.apply_begin_date ? "구독 시작일은" : !data.apply_end_date ? "구독 만료일은" : "평가판 종료일은") + " 필수 입력 사항 입니다.",
        showCancelButton: false,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
      }).then((result) => {
        console.log("- result", result);
        if (result.value) {
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });

      return;
    }

    Swal.fire({
      icon: "info",
      title: isNew ? "등록 확인" : "저장 확인",
      html:
        "[" +
        (isNew ? data.name : org.name) +
        "] " +
        (isNew
          ? "구독을 등록 하시겠습니까? <br/><br/>신규 등록은 다음날 부터 적용 됩니다. <br/>긴급 적용은 콜센터( 1600-5356 )로 문의 바랍니다."
          : "구독을 수정 하시겠습니까? <br/><br/>변경 사항은 다음날 부터 적용 됩니다. <br/>긴급 적용은 콜센터( 1600-5356 )로 문의 바랍니다."),
      showCancelButton: true,
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      console.log("- result", result);
      if (result.value) {
        let place_subscribe = data;

        place_subscribe.place_id = place_id;
        place_subscribe.apply_begin_date = moment(place_subscribe.apply_begin_date).format("YYYY-MM-DD");
        place_subscribe.apply_end_date = moment(place_subscribe.apply_end_date).format("YYYY-MM-DD");
        place_subscribe.trial_end_date = moment(place_subscribe.trial_end_date).format("YYYY-MM-DD");
        place_subscribe.is_apply = isNew ? 0 : 1;

        if (isNew) {
          delete place_subscribe.id;
          this.props.handleNewPlaceSubscribe({ place_subscribe });
        } else {
          this.props.handleSavePlaceSubscribe(org.id, { place_subscribe });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onCancel = () => {
    this.setState({
      isNew: false,
      isChangeDate: false,
      idx: -1,
      org: {},
      data: {},
    });
  };

  onConfirm = (confirm, value) => {
    console.log("- onConfirm", confirm);

    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(value);

          // 확인 상태 초기화.
          setTimeout(() => {
            this.setState({
              confirm: false,
            });
          }, 100);
        }
      }
    );
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      preferences: { place_id },
    } = this.props;

    this.props.initAllSubscribes();
    this.props.initAllPlaceSubscribes(place_id);
  };

  componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { alert, data } = this.state;

    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    if (nextProps.subscribe && nextProps.subscribe !== this.props.subscribe) {
      let { list } = nextProps.subscribe;
      let { subscribesRemain } = this.state;
      const { placeSubscribe } = this.props;

      if (placeSubscribe) {
        subscribesRemain = _.cloneDeep(list);

        // 등록 가능한 구독 목록에서 제외.
        _.map(placeSubscribe.list, (v) => {
          _.remove(subscribesRemain, (item) => item.id === v.subscribe_id);
        });

        this.setState({
          subscribesRemain,
        });
      }

      this.setState({
        subscribesAll: list,
      });
    }

    if (nextProps.placeSubscribe && nextProps.placeSubscribe !== this.props.placeSubscribe) {
      let { list } = nextProps.placeSubscribe;
      let { subscribesRemain } = this.state;
      const { subscribe } = this.props;

      console.log("- placeSubscribe  ", list);

      if (subscribe) {
        subscribesRemain = _.cloneDeep(subscribe.list);

        // 등록 가능한 구독 목록에서 제외.
        _.map(list, (v) => {
          _.remove(subscribesRemain, (item) => item.id === v.subscribe_id);
        });

        this.setState({
          subscribesRemain,
        });
      }

      this.setState({
        placeSubscribes: list,
      });

      const {
        preferences: { speed_checkin }, // 바로 체크인 기능 (0:사용안함, 1:사용)
      } = this.props;

      // 스피드 체크인 사용 시 확인창 닫음.
      let dialog = speed_checkin ? false : !alert.close;

      if (data.subscribe_id) {
        this.setState({
          org: {},
          isNew: false,
          data: {},
          idx: -1,
          isChangePwd: false,
          alert,
          dialog: false,
          confirm: false,
        });
      }
    }
  }

  render() {
    return this.state.placeSubscribes ? (
      <SetTab13
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSelect={this.onSelect}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
        onChangeDate={this.onChangeDate}
        onFocusChange={this.onFocusChange}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
