// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as subscribeAction } from "actions/subscribe";
import { actionCreators as placeSubscribeAction } from "actions/placeSubscribe";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, subscribe, placeSubscribe, preferences, error } = state;
  return {
    auth,
    subscribe,
    placeSubscribe,
    preferences: preferences.item,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllSubscribes: () => dispatch(subscribeAction.getSubscribeList()),

    initAllPlaceSubscribes: (palceId) => dispatch(placeSubscribeAction.getPlaceSubscribeList(palceId)),

    handleNewPlaceSubscribe: (placeSubscribe) => dispatch(placeSubscribeAction.newPlaceSubscribe(placeSubscribe)),

    handleSavePlaceSubscribe: (id, placeSubscribe) => dispatch(placeSubscribeAction.putPlaceSubscribe(id, placeSubscribe)),

    handleDeletePlaceSubscribe: (palceId, id) => dispatch(placeSubscribeAction.delPlaceSubscribe(palceId, id)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
