// imports.
import React from "react";
import _ from "lodash";
import cx from "classnames";
import moment from "moment";

import format from "utils/format-util";

import { Table, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button } from "react-bootstrap";
import InputNumber from "rc-input-number";

import { SingleDatePicker } from "react-dates";

import { keyToValue } from "constants/key-map";

// styles.
import "./styles.scss";

const SetTab13 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030113">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>구독 목록</h2>
              <div className="form-content">
                <div className="search-result">
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          <th>서비스명</th>
                          <th>유료여부</th>
                          <th>결제유형</th>
                          <th>구독 시작일</th>
                          <th>구독 만료일</th>
                          <th>평가판 만료일</th>
                          <th>신청 상태</th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.map(props.placeSubscribes, (item, k) => (
                          <tr key={k} onClick={(evt) => props.onSelect(item, k)} className={cx(props.idx === k && "on", props.idx !== k && item.is_apply !== 2 && "apply")}>
                            <td>{item.name}</td>
                            <td>{keyToValue("com", "payYn", item.pay_yn)}</td>
                            <td>{keyToValue("subscribe", "pay_type", item.pay_type)}</td>
                            <td>{item.begin_date ? moment(item.begin_date).format("YYYY-MM-DD") : "-"}</td>
                            <td>{item.end_date ? moment(item.end_date).format("YYYY-MM-DD") : "-"}</td>
                            <td>{item.trial_end_date ? moment(item.trial_end_date).format("YYYY-MM-DD") : "-"}</td>
                            <td>{keyToValue("place_subscribe", "applyState", item.is_apply)}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                  <ButtonToolbar className="btn-room">
                    <Button
                      // 매니저 이상 추가.
                      disabled={props.auth.user.level > 2}
                      onClick={(evt) => props.onAdd()}
                    >
                      추가
                    </Button>
                    <Button
                      // 매니저 이상 삭제.
                      disabled={props.auth.user.level > 2}
                      onClick={(evt) => props.onDelete()}
                    >
                      삭제
                    </Button>
                  </ButtonToolbar>
                </div>
              </div>
            </li>
          </ul>
          <ul className={cx("list-bul", "add-list", props.isNew || props.idx > -1 ? "show" : "hide")}>
            <li>
              <h2>구독 {props.isNew ? "등록" : "정보"}</h2>
              <div className="form-content">
                <div>
                  <FormGroup>
                    <ControlLabel>구독 서비스명</ControlLabel>
                    {!props.isNew && props.data.name ? (
                      <FormControl type="text" placeholder="구독 서비스명" value={props.data.name || ""} readOnly={true} />
                    ) : (
                      <FormControl componentClass="select" placeholder="구독 선택" onChange={(evt) => props.handleInputChange("subscribe_id", Number(evt.target.value))}>
                        {props.subscribesRemain.length ? <option value="">구독을 선택 해주세요.</option> : <option value="">이미 모든 서비스를 구독 중 입니다</option>}
                        {_.map(props.subscribesRemain, (item) => (
                          <option key={item.id} value={item.id}>
                            {item.name}
                          </option>
                        ))}
                      </FormControl>
                    )}
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>유료여부</ControlLabel>
                    <FormControl type="text" className="sm" placeholder="유료여부" value={props.data.pay_yn ? keyToValue("com", "payYn", props.data.pay_yn) : ""} readOnly={true} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>구독 신청 상태</ControlLabel>
                    <FormControl
                      type="text"
                      className={cx("sm", Number(props.data.is_apply) !== 2 && "apply")}
                      placeholder="구독 신청 상태"
                      value={keyToValue("place_subscribe", "applyState", props.data.is_apply || 0)}
                      readOnly={true}
                    />
                  </FormGroup>
                </div>
                {props.isNew || props.data.is_apply !== 2 || props.isChangeDate ? (
                  <div>
                    <FormGroup>
                      <ControlLabel>구독 신청 시작일</ControlLabel>
                      <SingleDatePicker
                        id="startsOnDateInput-1"
                        placeholder="구독 신청 시작일 선택"
                        displayFormat="YYYY-MM-DD"
                        date={moment(props.data.apply_begin_date)}
                        onDateChange={(date) => props.handleInputChange("apply_begin_date", date)}
                        focused={props.focused1}
                        onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                        numberOfMonths={1}
                        disabled={!props.isNew && !props.isChangeDate}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>구독 신청 종료일</ControlLabel>
                      <SingleDatePicker
                        id="startsOnDateInput-2"
                        placeholder="구독 신청 종료일 선택"
                        displayFormat="YYYY-MM-DD"
                        date={moment(props.data.apply_end_date)}
                        isOutsideRange={(day) => moment(props.data.apply_begin_date).isSameOrAfter(day)}
                        onDateChange={(date) => props.handleInputChange("apply_end_date", date)}
                        focused={props.focused2}
                        onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                        numberOfMonths={1}
                        disabled={!props.isNew && !props.isChangeDate}
                      />
                    </FormGroup>
                    {props.data.license_yn === "Y" ? (
                      <FormGroup>
                        <ControlLabel>추가 신청 라이선스 Copy</ControlLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className={cx("form-control mid")}
                            value={props.data.apply_license_copy}
                            step={1}
                            min={0}
                            max={999}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("apply_license_copy", Number(value))}
                            readOnly={!props.isNew && !props.isChangeDate}
                          />
                        </span>
                      </FormGroup>
                    ) : null}
                  </div>
                ) : null}
                {!props.isNew ? (
                  <div>
                    <FormGroup>
                      <ControlLabel>구독 서비스 시작일</ControlLabel>
                      <FormControl type="text" className="sm" placeholder="구독 시작일" value={props.data.begin_date ? moment(props.data.begin_date).format("YYYY-MM-DD") : "-"} readOnly={true} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>구독 서비스 만료일</ControlLabel>
                      <FormControl type="text" className="sm" placeholder="구독 만료일" value={props.data.end_date ? moment(props.data.end_date).format("YYYY-MM-DD") : "-"} readOnly={true} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>평가판 종료일</ControlLabel>
                      <FormControl
                        type="text"
                        className="sm"
                        placeholder="평가판 종료일"
                        value={props.data.trial_end_date ? moment(props.data.trial_end_date).format("YYYY-MM-DD") : "-"}
                        readOnly={true}
                      />
                    </FormGroup>
                  </div>
                ) : null}
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="gnb030113-01">결제유형</ControlLabel>
                    <FormControl type="text" className="sm" placeholder="결제유형" value={props.data.pay_type ? keyToValue("subscribe", "pay_type", props.data.pay_type) : ""} readOnly={true} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>{props.data.license_yn === "Y" ? "라이선스별 구독요금" : "구독 요금"}</ControlLabel>
                    <FormControl type="text" className="sm money" placeholder="구독 요금" value={String(props.data.default_fee || 0).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>구독 할인 요금</ControlLabel>
                    <FormControl type="text" className="sm money" placeholder="구독 할인 요금" value={String(props.data.discount || 0).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} readOnly={true} />
                  </FormGroup>
                </div>
                <div>
                  {props.data.license_yn === "Y" ? (
                    <FormGroup>
                      <ControlLabel>추가 라이선스 Copy</ControlLabel>
                      <FormControl type="text" className="sm" placeholder="추가 라이선스 Copy" value={props.data.license_copy} readOnly={true} />
                    </FormGroup>
                  ) : null}
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button disabled={!props.data.subscribe_id || props.auth.user.level > props.data.level} onClick={(evt) => (props.isNew || props.isChangeDate ? props.onSave() : props.onChangeDate())}>
                  {props.isNew ? "신청" : !props.isNew && !props.isChangeDate ? "구독 변경" : "저장"}
                </Button>
                <Button onClick={(evt) => props.onCancel()}>취소</Button>
              </ButtonToolbar>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab13;
