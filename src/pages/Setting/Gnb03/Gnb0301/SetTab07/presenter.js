// imports.
import React from "react";

import { ControlLabel, Checkbox, ButtonToolbar, Button, Radio, ButtonGroup } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab07 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030107">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <div>
                <h2>음성 기능 사용 설정</h2>
                <div className="form-content">
                  <ButtonGroup aria-label="voice">
                    <Radio
                      inline
                      name="voice"
                      value="1"
                      checked={props.modify.voice === 1}
                      onChange={(evt) => props.handleInputChange("voice", evt.target.value)}
                      className={cx(props.modify.voice === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="voice" value="0" checked={props.modify.voice === 0} onChange={(evt) => props.handleInputChange("voice", evt.target.value)}>
                      <span />
                      안함
                    </Radio>
                  </ButtonGroup>
                </div>
                <div className="audio-set">크롬 재시작 후 음성이 재생 되지 않을때는 화면을 마우스로 한번 클릭 해 주세요.</div>
              </div>
            </li>
            <li>
              <div>
                <h2>키 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_1", evt.target.checked)}>
                    <span />
                    손님키 삽입
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_4 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_4", evt.target.checked)}>
                    <span />
                    손님키 제거
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_3 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_3", evt.target.checked)}>
                    <span />
                    청소키 삽입
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_6 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_6", evt.target.checked)}>
                    <span />
                    청소키 제거
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_2 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_2", evt.target.checked)}>
                    <span />
                    마스터키 삽입
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.key_5 === 1} onChange={(evt) => props.handleVoiceOptionChange("key_5", evt.target.checked)}>
                    <span />
                    마스터키 제거
                  </Checkbox>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>도어 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.door_0 === 1} onChange={(evt) => props.handleVoiceOptionChange("door_0", evt.target.checked)}>
                    <span />
                    객실문 닫힘
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.door_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("door_1", evt.target.checked)}>
                    <span />
                    객실문 열림
                  </Checkbox>
                </div>
              </div>
              <div>
                <h2>청소 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.clean_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("clean_1", evt.target.checked)}>
                    <span />
                    청소 요청
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.clean_0 === 1} onChange={(evt) => props.handleVoiceOptionChange("clean_0", evt.target.checked)}>
                    <span />
                    청소 완료
                  </Checkbox>
                </div>
              </div>
              <div>
                <h2>전원 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.main_relay_0 === 1} onChange={(evt) => props.handleVoiceOptionChange("main_relay_0", evt.target.checked)}>
                    <span />
                    전원 차단
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.main_relay_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("main_relay_1", evt.target.checked)}>
                    <span />
                    전원 공급
                  </Checkbox>
                </div>
              </div>
              <div>
                <h2>차량호출 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.car_call_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("car_call_1", evt.target.checked)}>
                    <span />
                    배차 요청
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.car_call_2 === 1} onChange={(evt) => props.handleVoiceOptionChange("car_call_2", evt.target.checked)}>
                    <span />
                    배차 완료
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.car_call_3 === 1} onChange={(evt) => props.handleVoiceOptionChange("car_call_3", evt.target.checked)}>
                    <span />
                    배차 취소
                  </Checkbox>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>인스펙트(점검) 음성 설정</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.inspect_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("inspect_1", evt.target.checked)}>
                    <span />
                    점검대기
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.inspect_2 === 1} onChange={(evt) => props.handleVoiceOptionChange("inspect_2", evt.target.checked)}>
                    <span />
                    점검 중
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.inspect_3 === 1} onChange={(evt) => props.handleVoiceOptionChange("inspect_3", evt.target.checked)}>
                    <span />
                    점검 완료
                  </Checkbox>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>기타 효과음 설정 여부</h2>
                <div className="form-content clearfix">
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.stay_type === 1} onChange={(evt) => props.handleVoiceOptionChange("stay_type", evt.target.checked)}>
                    <span />
                    숙박/대실 상태
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.alarm === 1} onChange={(evt) => props.handleVoiceOptionChange("alarm", evt.target.checked)}>
                    <span />
                    알람 효과음
                  </Checkbox>
                  <br />
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.auto_check_in === 1} onChange={(evt) => props.handleVoiceOptionChange("auto_check_in", evt.target.checked)}>
                    <span />
                    자동입실
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.auto_check_out === 1} onChange={(evt) => props.handleVoiceOptionChange("auto_check_out", evt.target.checked)}>
                    <span />
                    자동퇴실
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.outing_1 === 1} onChange={(evt) => props.handleVoiceOptionChange("outing_1", evt.target.checked)}>
                    <span />
                    외출
                  </Checkbox>
                  <Checkbox inline className="checkbox04" checked={props.modify.voice_opt.outing_0 === 1} onChange={(evt) => props.handleVoiceOptionChange("outing_0", evt.target.checked)}>
                    <span />
                    외출 복귀
                  </Checkbox>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>기타 설정</h2>
                <Checkbox
                  inline
                  className="checkbox04"
                  defaultChecked={props.modify.voice_type_name_play === 1}
                  onChange={(evt) => props.handleInputChange("voice_type_name_play", evt.target.checked)}
                >
                  <span />
                  객실타입명 음성 출력
                </Checkbox>
                <br />
                <div className="form-content">
                  <ControlLabel htmlFor="gnb030102-01">음성 속도</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber className="form-control small" value={props.modify.voice_rate} step={0.1} min={0.5} max={2} onChange={(value) => props.handleInputChange("voice_rate", value)} />
                  </span>{" "}
                  (숫자가 클 수록 말하기 속도가 빨라 집니다.)
                </div>
                <div className="form-content">
                  <ControlLabel htmlFor="gnb030102-01">음성 버퍼</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control small"
                      value={props.modify.voice_opt.queue_size}
                      step={1}
                      min={0}
                      max={10}
                      onChange={(value) => props.handleVoiceOptionChange("queue_size", value)}
                    />
                  </span>{" "}
                  (순차적으로 재생 할 음성의 최대 갯수 입니다.)
                  <p className="comment">
                    - 재생대기 음성이 버퍼 갯수를 넘으면 다음 재생 할 음성이 제거 되고 신규 음성이 버퍼에 추가 됩니다.
                    <br />- 0 이면 실시간 으로 음성이 재생 됩니다.
                  </p>
                </div>
                {/* <div className="form-content">
                <ControlLabel htmlFor="gnb030102-01">음성 지연 시간</ControlLabel>
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control small"
                    value={props.modify.voice_opt.delay_time || 2}
                    step={1}
                    min={0}
                    max={10}
                    onChange={value => props.handleVoiceOptionChange("delay_time", value)}
                  />
                </span>{" "}
                초 (이벤트 발생 후 음성 출력 시간)
              </div> */}
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab07;
