// imports.
import React from "react";

import { ButtonToolbar, Button, ButtonGroup, Radio, FormGroup, ControlLabel, FormControl } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab17 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030117">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>QR 코드 키 발급</h2>
              <div className="form-content">
                <div>
                  <ButtonGroup aria-label="doorlock_send_qr_code" onChange={(evt) => props.handleInputChange("doorlock_send_qr_code", evt.target.value)}>
                    <Radio
                      inline
                      name="doorlock_send_qr_code"
                      value={1}
                      defaultChecked={props.modify.doorlock_send_qr_code === 1}
                      className={cx(props.modify.doorlock_send_qr_code === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="doorlock_send_qr_code" value={0} defaultChecked={props.modify.doorlock_send_qr_code === 0}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>
                </div>
              </div>
            </li>
            <li>
              <h2>APPID</h2>
              <div className="form-content">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="APPID 입력"
                    defaultValue={props.modify.doorlock_appid || ""}
                    onChange={(evt) => props.handleInputChange("doorlock_appid", evt.target.value)}
                    disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                  />
                </FormGroup>
              </div>
            </li>
            <li>
              <h2>로그인 정보</h2>
              <div className="form-content">
                <FormGroup>
                  USERNAME
                  <FormControl
                    type="text"
                    placeholder="USERNAME 입력"
                    defaultValue={props.modify.doorlock_username || ""}
                    onChange={(evt) => props.handleInputChange("doorlock_username", evt.target.value)}
                    disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                  />
                </FormGroup>
                <FormGroup>
                  PASSWORD
                  <FormControl
                    type="password"
                    placeholder="PASSWORD 입력"
                    defaultValue={props.modify.doorlock_password || ""}
                    onChange={(evt) => props.handleInputChange("doorlock_password", evt.target.value)}
                    disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                  />
                </FormGroup>
              </div>
            </li>
            <li>
              <h2>사용자 전화번호 (USERID)</h2>
              <div className="form-content">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="USERID 입력"
                    defaultValue={props.modify.doorlock_userid || ""}
                    onChange={(evt) => props.handleInputChange("doorlock_userid", evt.target.value)}
                    disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                  />
                </FormGroup>
              </div>

              <h2>로그인 테스트 (변경 사항 저장 후 사용)</h2>
              <div className="form-content">
                <Button onClick={(evt) => props.onLogin()}>로그인</Button>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab17;
