// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import { YYDoorLock } from "utils/doorlock-util";

import moment from "moment";
import Swal from "sweetalert2";

// Components.
import SetTab17 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.yYDoorLock = new YYDoorLock(props);
  }

  static propTypes = {
    preferences: PropTypes.object.isRequired,
    modify: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  };

  onLogin = () => {
    console.log("- onLogin", this.yYDoorLock);

    // 설정 정보 적용.
    this.yYDoorLock = new YYDoorLock(this.props);

    this.yYDoorLock.login().then((data) => {
      if (data.result === 0) {
        let startDate = moment();
        let endDate = moment().add("day", 1);
        let deviceId = "2C58B163"; // 테스트 도어락 id

        this.yYDoorLock.getLockQRCode(startDate, endDate, deviceId).then((res) => {
          console.log("- getLockQRCode", res);

          let { result, data, msg, qrcodedata } = res;

          if (result === 0) {
            Swal.fire({
              icon: "success",
              title: "QR 코드 조회 성공",
              html: `QR 코드 키 생성이 성공 하였습니다.<br/>${msg}`,
              confirmButtonText: "확인",
            });
          }
        });
      }
    });
  };

  render() {
    return <SetTab17 {...this.props} {...this.state} onLogin={this.onLogin} />;
  }
}

export default withSnackbar(Container);
