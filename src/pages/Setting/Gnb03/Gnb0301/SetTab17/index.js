// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { preferences, auth } = state;
  return {
    preferences: preferences.item,
    user: auth.user,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
    initPreferences: (palceId) => dispatch(preferencesAction.getPreferenceList(palceId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
