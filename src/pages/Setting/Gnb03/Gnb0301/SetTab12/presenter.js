/** @format */

// imports.
import React from "react";

import { ControlLabel, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab12 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030112">
        <div className="search-wrapper">
          <ul className="list-bul">
            {props.device.idm.maker && props.device.idm.maker.toLowerCase() === "shindoeds" ? (
              <li>
                <h2>객실 전원 설정</h2>
                <div className="form-content">
                  <ButtonGroup onChange={(evt) => props.handleInputChange("main_power_type", Number(evt.target.value))}>
                    <Radio inline name="main_power_type" value="1" defaultChecked={props.modify.main_power_type === 1} className={cx(props.modify.main_power_type === 1 && "checked-on")}>
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="main_power_type" value="0" defaultChecked={props.modify.main_power_type === 0}>
                      <span />
                      사용 안함
                    </Radio>
                  </ButtonGroup>
                </div>
              </li>
            ) : null}
            <li>
              <h2>공실 시 고객키 삽입 전원차단 타이머</h2>
              <div className="form-content">
                <div>
                  {props.modify.no_sale_user_key_in_power_off_time ? "고객카드 삽입 후 " : <font color="red">전원차단 사용 안함</font>}
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control small"
                      value={props.modify.no_sale_user_key_in_power_off_time}
                      step={1}
                      min={0}
                      max={60}
                      onChange={(value) => props.handleInputChange("no_sale_user_key_in_power_off_time", value)}
                    />
                  </span>
                  {props.modify.no_sale_user_key_in_power_off_time ? "분 후 전원 차단 " : ""}
                </div>
                <span> ※ 0분 이면 전원 차단 안함</span>
              </div>
            </li>
            {props.device.idm.maker && props.device.idm.maker.toLowerCase() === "icrew" ? (
              <li>
                <h2>청소 시 전원차단 타이머</h2>
                <div className="form-content">
                  <div>
                    {props.modify.clean_key_in_power_off_time ? "청소카드 삽입 후 " : <font color="red">전원차단 사용 안함</font>}
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        value={props.modify.clean_key_in_power_off_time}
                        step={5}
                        min={0}
                        max={60}
                        onChange={(value) => props.handleInputChange("clean_key_in_power_off_time", value)}
                      />
                    </span>
                    {props.modify.clean_key_in_power_off_time ? "분 후 전원 차단 " : ""}
                  </div>
                  <span> ※ 0분 이면 전원 차단 안함</span>
                </div>
              </li>
            ) : null}
            {props.device.idm.maker && props.device.idm.maker.toLowerCase() === "icrew" ? (
              <li>
                <h2>고객카드 삽입 상태 퇴실 시 전원차단 타이머</h2>
                <div className="form-content">
                  <div>
                    {props.modify.user_key_in_check_out_power_off_time ? "퇴실 후 " : <font color="red">퇴실 즉시 전원 차단</font>}
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        value={props.modify.user_key_in_check_out_power_off_time}
                        step={5}
                        min={0}
                        max={60}
                        onChange={(value) => props.handleInputChange("user_key_in_check_out_power_off_time", value)}
                      />
                    </span>
                    {props.modify.user_key_in_check_out_power_off_time ? "분 후 전원 차단 " : ""}
                  </div>
                  <span className="red"> ※ 0분 이면 즉시 전원 차단</span>
                </div>
              </li>
            ) : null}
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab12;
