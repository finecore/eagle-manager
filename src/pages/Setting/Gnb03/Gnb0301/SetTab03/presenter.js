// imports.
import React from "react";

import { Radio, ButtonGroup, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab03 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030103">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>객실 동시 사용 잠금 설정 </h2>
              <div className="form-content">
                <div>
                  다른 사용자가 객실 조작 시 체크인 화면 잠금을
                  <ButtonGroup aria-label="interrupt_use" onChange={(evt) => props.handleInputChange("interrupt_use", evt.target.value)}>
                    <Radio inline name="interrupt_use" value="1" defaultChecked={props.modify.interrupt_use === 1} className={cx(props.modify.interrupt_use === 1 && "checked-on")}>
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="interrupt_use" value="0" defaultChecked={props.modify.interrupt_use === 0}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>
                </div>
                <div>
                  동시잠금 상태에서 자동해제 시간
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.interrupt_release}
                      step={1}
                      min={1}
                      max={60}
                      onChange={(value) => props.handleInputChange("interrupt_release", value)}
                    />
                  </span>
                  분
                </div>
              </div>
            </li>
            <li>
              <h2>자리비움 자동 잠금 설정 </h2>
              <div className="form-content">
                <div>
                  일정시간 자리비움 시 전체 화면 잠금을
                  <ButtonGroup aria-label="place_lock_use" onChange={(evt) => props.handleInputChange("place_lock_use", evt.target.value)}>
                    <Radio inline name="place_lock_use" value="1" defaultChecked={props.modify.place_lock_use === 1} className={cx(props.modify.place_lock_use === 1 && "checked-on")}>
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="place_lock_use" value="0" defaultChecked={props.modify.place_lock_use === 0}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>
                </div>
                <div>
                  자리비움 시 자동 잠김 시간
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.place_lock_time}
                      step={1}
                      min={1}
                      max={3600}
                      onChange={(value) => props.handleInputChange("place_lock_time", value)}
                    />
                  </span>
                  분
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab03;
