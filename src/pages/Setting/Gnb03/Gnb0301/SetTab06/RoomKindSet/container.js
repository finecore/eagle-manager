// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import RoomKindSet from "./presenter";

class Container extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(false);
  };

  render() {
    return <RoomKindSet {...this.props} closeModal={this.closeModal} />;
  }
}

export default withSnackbar(Container);
