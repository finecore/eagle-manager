// imports.
import React from "react";
import Modal from "react-awesome-modal";

//room-popup
import { FormGroup, FormControl, ControlLabel, Checkbox, ButtonToolbar, Button } from "react-bootstrap";

// styles.
import "./styles.scss";

// functional component
const RoomKindSet = (props) => {
  return (
    <Modal visible={true} effect="fadeInUp">
      <article className="room-kind-set" id="room-kind">
        <h1>객실종류설정</h1>
        <div className="kind-form-wrapper">
          <div>
            <FormGroup controlId="formRoomKind">
              <ControlLabel>객실종류</ControlLabel>
              <FormControl type="text" placeholder="" />
              <Checkbox inline>
                <span />
                객실에 표시
              </Checkbox>
              <Checkbox inline>
                <span />
                객실에 표시 안함
              </Checkbox>
            </FormGroup>
          </div>
          <div>
            <span>색상설정</span>
            <ButtonToolbar className="btn-room">
              <Button className="font-color-set">글자색</Button>
              <Button className="bg-color-set">배경색</Button>
            </ButtonToolbar>
          </div>
          <p>* 변경 사항은 프로그램 재실행 후 반영됩니다.</p>
          <ButtonToolbar className="btn-room">
            <Button>저장</Button>
            <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
              닫기
            </Button>
          </ButtonToolbar>
        </div>
        <a onClick={(evt) => props.closeModal(evt)} className="kind-close">
          Close
        </a>
      </article>
    </Modal>
  );
};

export default RoomKindSet;
