// imports.
import React from "react";
import { SketchPicker } from "react-color";

import { ButtonToolbar, Button, Checkbox, ControlLabel, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab06 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030106">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>투숙형태별 배경색 지정</h2>
              <div className="form-content">
                <ButtonToolbar className="btn-room">
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(0)} style={{ background: `${props.saleColor[0]}` }}>
                    공실
                    <br />
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(2)} style={{ background: `${props.saleColor[2]}` }}>
                    대실
                    <br />
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(1)} style={{ background: `${props.saleColor[1]}` }}>
                    숙박
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(3)} style={{ background: `${props.saleColor[3]}` }}>
                    장기
                  </Button>
                  <Button className="w2" onClick={(evt) => props.onClickOpenColor(4)} style={{ background: `${props.saleColor[4]}` }}>
                    외출중
                  </Button>
                  <Button onClick={(evt) => props.onClickResetColor()}>기본값 복원</Button>
                  {props.displayColorPicker ? (
                    <div className="color-picker">
                      <div className="picker-close" onClick={(evt) => props.onClickCloseColor()} />
                      <SketchPicker color={props.saleColor[props.selected]} onChange={(color) => props.onClickChooseColor(color)} />
                    </div>
                  ) : null}
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <h2>키 색상 지정</h2>
              <div className="form-content">
                <ButtonToolbar className="btn-room">
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(1, "key")} style={{ background: `${props.keyColor[1]}` }}>
                    고객키
                    <br />
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(3, "key")} style={{ background: `${props.keyColor[3]}` }}>
                    청소키
                    <br />
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(2, "key")} style={{ background: `${props.keyColor[2]}` }}>
                    마스터키
                  </Button>
                  <Button onClick={(evt) => props.onClickResetColor("key")}>기본값 복원</Button>
                  {props.displayColorPickerKey ? (
                    <div className="color-picker">
                      <div className="picker-close" onClick={(evt) => props.onClickCloseColor("key")} />
                      <SketchPicker color={props.keyColor[props.selected]} onChange={(color) => props.onClickChooseColor(color, "key")} />
                    </div>
                  ) : null}
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <h2>객실명 색상 지정</h2>
              <div className="form-content">
                <ButtonToolbar className="btn-room">
                  <Button
                    className="w1"
                    onClick={(evt) => props.onClickOpenColor(1, "name")}
                    style={{
                      color: `${props.roomNmColor[1]}`,
                      background: `${props.saleColor[1]}`,
                    }}
                  >
                    기본
                  </Button>
                  <Button
                    className="w1"
                    onClick={(evt) => props.onClickOpenColor(2, "name")}
                    style={{
                      color: `${props.roomNmColor[2]}`,
                      background: `${props.saleColor[2]}`,
                    }}
                  >
                    오버타임
                  </Button>
                  <Button onClick={(evt) => props.onClickResetColor("name")}>기본값 복원</Button>
                  {props.displayColorPickerRoomNm ? (
                    <div className="color-picker">
                      <div className="picker-close" onClick={(evt) => props.onClickCloseColor("name")} />
                      <SketchPicker color={props.roomNmColor[props.selected]} onChange={(color) => props.onClickChooseColor(color, "name")} />
                    </div>
                  ) : null}
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <h2>사용시간 배경색상 지정</h2>
              <div className="form-content">
                <ButtonToolbar className="btn-room">
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(1, "time")} style={{ background: `${props.timeBgColor[1]}` }}>
                    사용시간
                    <br />
                  </Button>
                  <Button className="w1" onClick={(evt) => props.onClickOpenColor(2, "time")} style={{ background: `${props.timeBgColor[2]}` }}>
                    청소시간
                    <br />
                  </Button>
                  <Button onClick={(evt) => props.onClickResetColor("time")}>기본값 복원</Button>
                  {props.displayColorPickerTime ? (
                    <div className="color-picker">
                      <div className="picker-close" onClick={(evt) => props.onClickCloseColor("time")} />
                      <SketchPicker color={props.timeBgColor[props.selected]} onChange={(color) => props.onClickChooseColor(color, "time")} />
                    </div>
                  ) : null}
                </ButtonToolbar>
              </div>
            </li>
            <li>
              <div>
                <h2>글자크기 설정</h2>
                <div className="form-content">
                  <div>
                    <ControlLabel htmlFor="gnb030106-01">기본 설정</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber className="form-control mid" value={props.modify.font_size} step={1} min={1} max={30} onChange={(value) => props.handleInputChange("font_size", value)} />
                    </span>
                  </div>
                  <div>
                    <ControlLabel htmlFor="gnb030106-01">좌측 메뉴</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber className="form-control mid" value={props.modify.font_size_1} step={1} min={1} max={50} onChange={(value) => props.handleInputChange("font_size_1", value)} />
                    </span>
                  </div>
                  <div>
                    <ControlLabel htmlFor="gnb030106-01">체크인 팝업</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber className="form-control mid" value={props.modify.font_size_2} step={1} min={1} max={50} onChange={(value) => props.handleInputChange("font_size_2", value)} />
                    </span>
                  </div>
                  <div>
                    <ControlLabel htmlFor="gnb030106-01">객실명</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber className="form-control mid" value={props.modify.font_size_3} step={1} min={1} max={50} onChange={(value) => props.handleInputChange("font_size_3", value)} />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>레이아웃 설정</h2>
                <div className="form-content">
                  <div>
                    <ControlLabel htmlFor="gnb030106-01">좌측 메뉴 넓이</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control mid"
                        value={props.modify.layout_width_left}
                        step={5}
                        min={100}
                        max={600}
                        onChange={(value) => props.handleInputChange("layout_width_left", value)}
                      />
                    </span>{" "}
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>퇴실예정 시간 알림 설정</h2>
                <div className="form-content">
                  <div>
                    퇴실예정 시간{" "}
                    <span className="numeric-wrap">
                      <InputNumber
                        id="gnb030111-02"
                        className="form-control mid"
                        value={props.modify.check_out_alarm_time}
                        step={10}
                        min={0}
                        max={999}
                        onChange={(value) => props.handleInputChange("check_out_alarm_time", value)}
                        formatter={(value) => value + " 분"}
                      />
                    </span>{" "}
                    전 부터 알림 표시
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>객실 온도 표시 설정</h2>
                <div className="form-content">
                  <div>
                    <Radio inline className="checkbox04" checked={props.modify.show_air_temp === 1} onChange={(evt) => props.handleInputChange("show_air_temp", 1)}>
                      <span />
                      표시
                    </Radio>
                    <Radio inline className="checkbox04" checked={props.modify.show_air_temp === 0} onChange={(evt) => props.handleInputChange("show_air_temp", 0)}>
                      <span />
                      숨김
                    </Radio>
                  </div>
                </div>
              </div>
              <div>
                <h2>공실 경과 시간 표시 설정</h2>
                <div className="form-content">
                  <div>
                    <Radio inline className="checkbox04" checked={props.modify.show_sale_change_time === 1} onChange={(evt) => props.handleInputChange("show_sale_change_time", 1)}>
                      <span />
                      표시
                    </Radio>
                    <Radio inline className="checkbox04" checked={props.modify.show_sale_change_time === 0} onChange={(evt) => props.handleInputChange("show_sale_change_time", 0)}>
                      <span />
                      숨김
                    </Radio>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div>
                <h2>객실 이력 표시 설정</h2>
                <div className="form-content">
                  <div>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.room_log_show_type.power === 1 && "checked-on")}
                      checked={props.modify.room_log_show_type.power === 1}
                      onChange={(evt) => props.onClickRoomLogShowType("power", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      전원 상태
                    </Checkbox>
                  </div>
                  <div>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.room_log_show_type.signal === 1 && "checked-on")}
                      checked={props.modify.room_log_show_type.signal === 1}
                      onChange={(evt) => props.onClickRoomLogShowType("signal", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      통신 상태
                    </Checkbox>
                  </div>
                  <div>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.room_log_show_type.air_temp === 1 && "checked-on")}
                      checked={props.modify.room_log_show_type.air_temp === 1}
                      onChange={(evt) => props.onClickRoomLogShowType("air_temp", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      온도 상태
                    </Checkbox>
                  </div>
                  <div>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.modify.room_log_show_type.car_call === 1 && "checked-on")}
                      checked={props.modify.room_log_show_type.car_call === 1}
                      onChange={(evt) => props.onClickRoomLogShowType("car_call", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      차량 호출
                    </Checkbox>
                  </div>
                </div>
              </div>
            </li>
            {props.modify.inspect_use ? (
              <li>
                <h2>인스펙트 사용 시 [점검완료] 문구 숨김 타이머</h2>
                <div className="form-content">
                  <div>
                    {props.modify.inspect_done_show_time ? "점검완료 후 " : <font color="red">타이머 사용 안함</font>}
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control small"
                        value={props.modify.inspect_done_show_time}
                        step={1}
                        min={0}
                        max={60}
                        onChange={(value) => props.handleInputChange("inspect_done_show_time", value)}
                      />
                    </span>
                    {props.modify.inspect_done_show_time ? "분 후 문구 숨김 " : ""}
                  </div>
                  <span className="red"> ※ 0분 이면 계속 표시</span>
                </div>
              </li>
            ) : null}
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab06;
