// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import _ from "lodash";

// Components.
import SetTab06 from "./presenter";

class Container extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      saleColor: {},
      keyColor: {},
      roomNmColor: {},
      timeBgColor: {},
      selected: -1,
      displayColorPicker: false,
      displayColorPickerKey: false,
      displayColorPickerRoomNm: false,
      displayColorPickerTime: false,
    };
  }

  onClickOpenColor = (sel, type) => {
    console.log("- onClickOpenColor", sel, type);

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    this.setState({
      selected: sel,
      displayColorPicker: !type,
      displayColorPickerKey: type === "key",
      displayColorPickerRoomNm: type === "name",
      displayColorPickerTime: type === "time",
    });
  };

  onClickCloseColor = () => {
    console.log("- onClickOpenColor");

    this.setState({
      selected: -1,
      displayColorPicker: false,
      displayColorPickerKey: false,
      displayColorPickerRoomNm: false,
      displayColorPickerTime: false,
    });
  };

  onClickChooseColor = (color, type) => {
    let { saleColor, keyColor, roomNmColor, timeBgColor, selected } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    console.log("- onClickChooseColor", color, type);

    if (type === "key") {
      keyColor[selected] = color.hex;
      this.props.handleColorChange("key_type_color", selected, keyColor[selected]);
    } else if (type === "name") {
      roomNmColor[selected] = color.hex;
      this.props.handleColorChange("room_name_color", selected, roomNmColor[selected]);
    } else if (type === "time") {
      timeBgColor[selected] = color.hex;
      this.props.handleColorChange("time_bg_color", selected, timeBgColor[selected]);
    } else {
      saleColor[selected] = color.hex;
      this.props.handleColorChange("stay_type_color", selected, saleColor[selected]);
    }

    this.setState({
      saleColor,
      keyColor,
      roomNmColor,
      timeBgColor,
    });
  };

  onClickResetColor = (color, type) => {
    console.log("- onClickResetColor", color, type);

    if (type === "key") {
      let key_type_color = {
        1: "#5c56af",
        2: "#548b8d",
        3: "#bb3e3eda",
      };

      this.props.handleInputChange("key_type_color", key_type_color);

      this.setState({
        keyColor: key_type_color,
      });
    } else if (type === "name") {
      let room_name_color = {
        1: "#f3f3f394",
        2: "#e02c14fa",
      };

      this.props.handleInputChange("room_name_color", room_name_color);

      this.setState({
        roomNmColor: room_name_color,
      });
    } else if (type === "time") {
      let time_bg_color = {
        1: "#2f2f2fd4",
        2: "#ff8484a8",
      };

      this.props.handleInputChange("time_bg_color", time_bg_color);

      this.setState({
        timeBgColor: time_bg_color,
      });
    } else {
      let stay_type_color = {
        0: "#c8c8c8",
        1: "#ffab3e",
        2: "#52d3ee",
        3: "#ff727a",
        4: "#a1a1a1",
      };

      this.props.handleInputChange("stay_type_color", stay_type_color);

      this.setState({
        saleColor: stay_type_color,
      });
    }
  };

  onClickRoomLogShowType = (key, value) => {
    console.log("- onClickRoomLogShowType", { key, value });

    let { room_log_show_type } = this.props.preferences;

    let modify = _.merge({}, room_log_show_type, { [key]: value });

    this.props.handleInputChange("room_log_show_type", modify);
  };

  componentDidMount = () => {
    const { stay_type_color, key_type_color, room_name_color, time_bg_color } = this.props.preferences;

    this.setState({
      saleColor: stay_type_color,
      keyColor: key_type_color,
      roomNmColor: room_name_color,
      timeBgColor: time_bg_color,
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.preferences && nextProps.preferences !== this.props.preferences) {
      const { stay_type_color, key_type_color, room_name_color, time_bg_color } = nextProps.preferences;

      // console.log("- componentWillReceiveProps time_bg_color", time_bg_color);

      this.setState({
        stayTypeColor: stay_type_color,
        keyColor: key_type_color,
        roomNmColor: room_name_color,
        timeBgColor: time_bg_color,
      });
    }
  }

  render() {
    return (
      <SetTab06
        {...this.props}
        {...this.state}
        onClickOpenColor={this.onClickOpenColor}
        onClickCloseColor={this.onClickCloseColor}
        onClickChooseColor={this.onClickChooseColor}
        onClickResetColor={this.onClickResetColor}
        onClickRoomLogShowType={this.onClickRoomLogShowType}
      />
    );
  }
}

export default withSnackbar(Container);
