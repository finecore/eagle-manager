// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as userAction } from "actions/user";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, user, preferences, error } = state;
  return {
    auth,
    user,
    preferences: preferences.item,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllUsers: (palceId) => dispatch(userAction.getPlaceUser(palceId)),

    handleNewUser: (user) => dispatch(userAction.newUser(user)),

    handleSaveUser: (id, user) => dispatch(userAction.putUser(id, user)),

    handleDeleteUser: (palceId, id) => dispatch(userAction.delUser(palceId, id)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
