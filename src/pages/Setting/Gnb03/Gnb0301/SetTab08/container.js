// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import Swal from "sweetalert2";
import { CheckLicense } from "utils/subscribe-util";

// UI for notification
import { withSnackbar } from "notistack";

// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

// Components.
import SetTab08 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      org: {}, // 수정 시 사용 할 원래 아이디값.
      data: {},
      idx: -1,
      isNew: false,
      isChangePwd: false,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { users, data, idx, isNew } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    if (value.length && (name === "tel" || name === "hp")) {
      value = value.replace(/[^\d]/g, "").replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, "$1-$2-$3");
    }

    // 라이선스 체크
    if (name === "licence_yn" && value === "Y") {
      let use_license_copy = 0;

      _.each(users, (user) => {
        if (user.type !== 1 && user.licence_yn === "Y") use_license_copy++;
      });

      console.log("- use_license_copy", use_license_copy);

      // 매니저 Web 동시 접속 라이선스 체크.
      CheckLicense(this.props, "04", use_license_copy, (res) => {
        if (!res) {
          value = "N";
        }

        // 뷰 아이템 정보 변경.
        data[name] = value;

        // 수정 아니고 입력값이 있으면 신규 설정.
        if (idx === -1 && !isNew) isNew = true;

        this.setState({
          data,
          isNew,
        });
      });
    } else {
      // 뷰 아이템 정보 변경.
      data[name] = value;

      // 수정 아니고 입력값이 있으면 신규 설정.
      if (idx === -1 && !isNew) isNew = true;

      this.setState({
        data,
        isNew,
      });
    }
  };

  onSelect = (user, idx) => {
    console.log("-- onSelect", user, idx);

    this.setState({
      isNew: false,
      org: user,
      data: _.cloneDeep(user),
      idx,
    });
  };

  onChangePwd = () => {
    let { isChangePwd, data } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    isChangePwd = !isChangePwd;

    console.log("-- onChangePwd", isChangePwd);

    if (!isChangePwd) {
      data.pwd = "";
      data.pwd_confirm = "";
    }

    this.setState({
      isChangePwd,
      data,
    });
  };

  onAdd = () => {
    console.log("-- onAdd");

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    this.setState({
      isNew: true,
      idx: -1,
      org: {},
      data: {},
    });
  };

  onDelete = () => {
    const { data, idx } = this.state;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let user = data;

    console.log("-- onDelete", user);

    if (idx === -1) {
      Swal.fire({
        icon: "warning",
        title: "선택 확인",
        html: "삭제 할 직원을 선택해 주세요.",
        confirmButtonText: "확인",
      });

      return;
    }

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: "[" + user.name + "] 직원을 정말 삭제 하시겠습니까?",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        this.props.handleDeleteUser(user.place_id, user.id);
      }
    });
  };

  onSave = () => {
    const {
      preferences: { speed_checkin },
      auth: {
        user: { place_id },
      },
    } = this.props;

    // 매니저 권한 이상만 수정 가능.
    if (!this.props.checkAuth()) return;

    let { org, data, isNew } = this.state;

    let user = data;

    isNew = !org.id;

    console.log("-- onSave", user, isNew);

    if (!data.id || !data.name || isNaN(data.level) || !data.hp || (isNew && !data.pwd)) {
      Swal.fire({
        icon: "warning",
        title: "입력값 확인",
        html: (!data.id ? "아이디는" : !data.name ? "이름은" : isNaN(data.level) ? "권한은" : !data.hp ? "휴대전화번호는" : "비밀번호는") + " 필수 입력 항목 입니다.",
        confirmButtonText: "확인",
      });
      return;
    }

    if (data.pwd || data.pwd_confirm) {
      let text = "";

      if (data.pwd.length < 5) text = "비밀번호는 최소 5자 이상 입니다.";
      else if (data.pwd !== data.pwd_confirm) text = "비밀번호와 비밀번호 확인이 동일 하지 않습니다.";

      if (text) {
        Swal.fire({
          icon: "warning",
          title: "비밀번호 확인",
          html: text,
          confirmButtonText: "확인",
        });
        return;
      }
    } else {
      // 비밀번호 입력 안하면 해당 항목 제거.
      delete data.pwd;
      delete data.pwd_confirm;
    }

    Swal.fire({
      icon: "warning",
      title: isNew ? "등록 확인" : "저장 확인",
      html: "[" + (isNew ? data.name : org.name) + "] " + (isNew ? "직원을 등록 하시겠습니까?" : "직원을 수정 하시겠습니까?"),
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        user.place_id = place_id;
        delete data.pwd_confirm;

        if (isNew) this.props.handleNewUser({ user }).then((res) => {});
        else this.props.handleSaveUser(org.id, { user }).then((res) => {});
      }
    });
  };

  onCancel = () => {
    this.setState({
      isNew: false,
      idx: -1,
      org: {},
      data: {},
    });
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      user: { place_id },
    } = this.props.auth;

    console.log("-- Setting > Gnb0308 > componentDidMount ", this.props.user);

    this.props.initAllUsers(place_id);
  };

  componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { data, isNew, idx } = this.state;

    if (nextProps.user && nextProps.user !== this.props.user) {
      let { list, item } = nextProps.user;
      const {
        user: { id, level, isIcrewViewer },
      } = this.props.auth;

      console.log("-- componentWillReceiveProps ", list, item, isNew, idx);

      if (list) {
        let users = isIcrewViewer
          ? list
          : _.filter(list, (item) => {
              // 본인 또는 권한이 낮은 직원을 보인다.
              if (item.type !== 1 && (item.level > level || item.id === id)) return true;
              else return false;
            });

        // 비밀번호 제거.
        users = _.map(users, (item) => {
          item.pwd = "";
          return item;
        });

        this.setState({
          users,
        });
      }

      if (data.id) {
        this.setState({
          org: {},
          isNew: false,
          data: {},
          idx: -1,
          isChangePwd: false,
        });
      }
    }
  }

  render() {
    return this.state.data ? (
      <SetTab08
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSelect={this.onSelect}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onChangePwd={this.onChangePwd}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
