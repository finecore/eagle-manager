// imports.
import React from "react";
import _ from "lodash";
import cx from "classnames";

import { Table, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import { keyToValue } from "constants/key-map";
import format from "utils/format-util";

// styles.
import "./styles.scss";

const SetTab08 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030108">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>직원 목록</h2>
              <div className="form-content">
                <div className="search-result">
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          <th>아이디</th>
                          <th>이름</th>
                          <th>권한</th>
                          <th>부서</th>
                          <th>라이선스</th>
                          <th>전화번호</th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.map(props.users, (item, k) => (
                          <tr key={k} onClick={(evt) => props.onSelect(item, k)} className={cx(props.idx === k && "on")}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{keyToValue("user", "level", item.level)}</td>
                            <td>{item.dept}</td>
                            <td>{item.licence_yn}</td>
                            <td>{item.hp}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                  <ButtonToolbar className="btn-room">
                    <Button
                      // 매니저 이상 추가.
                      disabled={props.auth.user.level > 2}
                      onClick={(evt) => props.onAdd()}
                    >
                      추가
                    </Button>
                    <Button
                      // 본인 삭제 금지.
                      disabled={props.data.id === props.auth.user.id}
                      onClick={(evt) => props.onDelete()}
                    >
                      삭제
                    </Button>
                    {/* <Button onClick={evt => props.onMode()}>기능부여</Button> */}
                  </ButtonToolbar>
                </div>
              </div>
            </li>
          </ul>
          <ul className={cx("list-bul", "add-list", props.isNew || props.idx > -1 ? "show" : "hide")}>
            <li>
              <h2>직원 {props.isNew ? "등록" : "정보"}</h2>
              <div className="form-content">
                <div>
                  <FormGroup>
                    <ControlLabel>아이디</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="아이디 입력"
                      value={props.data.id || ""}
                      // 자기자신은 아이디 수정 못한다. 하위 권한은 수정 가능.
                      readOnly={!props.isNew && (props.data.id === props.auth.user.id || props.auth.user.level >= props.data.level)}
                      onChange={(evt) => props.handleInputChange("id", evt.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>이름</ControlLabel>
                    <FormControl type="text" placeholder="이름 입력" value={props.data.name || ""} onChange={(evt) => props.handleInputChange("name", evt.target.value)} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="gnb030108-01">권한</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="선택"
                      id="gnb030108-01"
                      value={props.data.level !== undefined ? props.data.level : ""}
                      disabled={!props.isNew && props.data.id === props.auth.user.id}
                      onChange={(evt) => props.handleInputChange("level", evt.target.value)}
                    >
                      {/* 권한 (0: 슈퍼관리자(ICT), 1: 업주(책임자), 2: 매니저(관리자), 3: 카운터(직원), 4:메이드, 5: PMS, 9:뷰어) 본인 권한의 아래 권한만 보인다 */}
                      <option value="">선택</option>
                      {props.auth.user.level < (props.data.id === props.auth.user.id ? 1 : 0) && <option value={0}>iCrew</option>}
                      {props.auth.user.level < (props.data.id === props.auth.user.id ? 2 : 1) && <option value={1}>업주</option>}
                      {props.auth.user.level < (props.data.id === props.auth.user.id ? 3 : 2) && <option value={2}>매니저</option>}
                      <option value={3}>사원</option>
                    </FormControl>{" "}
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>휴대폰번호</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="휴대폰번호 입력"
                      value={props.data.hp || ""}
                      maxLength={13}
                      onChange={(evt) => props.handleInputChange("hp", format.toPhone(evt.target.value))}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>전화번호</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="전화번호 입력"
                      value={props.data.tel || ""}
                      maxLength={13}
                      onChange={(evt) => props.handleInputChange("tel", format.toPhone(evt.target.value))}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>이메일</ControlLabel>
                    <FormControl type="email" placeholder="이메일 입력" value={props.data.email || ""} maxLength={30} onChange={(evt) => props.handleInputChange("email", evt.target.value)} />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>부서</ControlLabel>
                    <FormControl type="text" placeholder="부서 입력" value={props.data.dept || ""} onChange={(evt) => props.handleInputChange("dept", evt.target.value)} />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>직위</ControlLabel>
                    <FormControl type="text" placeholder="직위 입력" value={props.data.rank || ""} onChange={(evt) => props.handleInputChange("rank", evt.target.value)} />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>로그인 라이선스 사용 여부</ControlLabel>
                    <ButtonGroup aria-label="licence_yn">
                      <Radio inline name="licence_yn" value="N" checked={props.data.licence_yn === "N"} onChange={(evt) => props.handleInputChange("licence_yn", evt.target.value)}>
                        <span />
                        없음
                      </Radio>
                      <Radio inline name="licence_yn" value="Y" checked={props.data.licence_yn === "Y"} onChange={(evt) => props.handleInputChange("licence_yn", evt.target.value)}>
                        <span />
                        있음
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>로그인 라이선스 구분</ControlLabel>
                    <ButtonGroup aria-label="licence_type">
                      <Radio inline name="licence_type" value="1" checked={props.data.licence_type === 1} onChange={(evt) => props.handleInputChange("licence_type", Number(evt.target.value))}>
                        <span />
                        고정 접속
                      </Radio>
                      <Radio inline name="licence_type" value="2" checked={props.data.licence_type === 2} onChange={(evt) => props.handleInputChange("licence_type", Number(evt.target.value))}>
                        <span />
                        유동 접속
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                </div>
                {/* <div>
                  <FormGroup>
                    <ControlLabel>메이드 로그인 라이선스</ControlLabel>
                    <ButtonGroup aria-label="licence_yn">
                      <Radio inline name="licence_yn" value="N" checked={props.data.licence_yn === "N"} onChange={(evt) => props.handleInputChange("licence_yn", evt.target.value)}>
                        <span />
                        없음
                      </Radio>
                      <Radio inline name="licence_yn" value="Y" checked={props.data.licence_yn === "Y"} onChange={(evt) => props.handleInputChange("licence_yn", evt.target.value)}>
                        <span />
                        있음
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                </div> */}
                <div>
                  <FormGroup>
                    <ControlLabel>일일 매출 정보 메일 수신 여부</ControlLabel>
                    <ButtonGroup aria-label="daily_report_mail_yn">
                      <Radio
                        inline
                        name="daily_report_mail_yn"
                        value="Y"
                        checked={props.data.daily_report_mail_yn === "Y"}
                        onChange={(evt) => props.handleInputChange("daily_report_mail_yn", evt.target.value)}
                      >
                        <span />
                        수신
                      </Radio>
                      <Radio
                        inline
                        name="daily_report_mail_yn"
                        value="N"
                        checked={props.data.daily_report_mail_yn === "N"}
                        onChange={(evt) => props.handleInputChange("daily_report_mail_yn", evt.target.value)}
                      >
                        <span />
                        안함
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>무인 판매 메일 수신 여부</ControlLabel>
                    <ButtonGroup aria-label="isg_sale_mail_yn">
                      <Radio inline name="isg_sale_mail_yn" value="Y" checked={props.data.isg_sale_mail_yn === "Y"} onChange={(evt) => props.handleInputChange("isg_sale_mail_yn", evt.target.value)}>
                        <span />
                        수신
                      </Radio>
                      <Radio inline name="isg_sale_mail_yn" value="N" checked={props.data.isg_sale_mail_yn === "N"} onChange={(evt) => props.handleInputChange("isg_sale_mail_yn", evt.target.value)}>
                        <span />
                        안함
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>OTA 예약 연동 메일 수신 여부</ControlLabel>
                    <ButtonGroup aria-label="ota_mo_mail_yn">
                      <Radio inline name="ota_mo_mail_yn" value="1" checked={props.data.ota_mo_mail_yn === "1"} onChange={(evt) => props.handleInputChange("ota_mo_mail_yn", evt.target.value)}>
                        <span />
                        수신
                      </Radio>
                      <Radio inline name="ota_mo_mail_yn" value="2" checked={props.data.ota_mo_mail_yn === "2"} onChange={(evt) => props.handleInputChange("ota_mo_mail_yn", evt.target.value)}>
                        <span />
                        실패만 수신
                      </Radio>
                      <Radio inline name="ota_mo_mail_yn" value="0" checked={props.data.ota_mo_mail_yn === "0"} onChange={(evt) => props.handleInputChange("ota_mo_mail_yn", evt.target.value)}>
                        <span />
                        안함
                      </Radio>
                    </ButtonGroup>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>비밀번호</ControlLabel>
                    <FormControl
                      type="password"
                      placeholder="비밀번호 입력"
                      value={props.data.pwd || ""}
                      disabled={!props.isNew && !props.isChangePwd}
                      maxLength={20}
                      onChange={(evt) => props.handleInputChange("pwd", evt.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>비밀번호 확인</ControlLabel>
                    <FormControl
                      type="password"
                      placeholder="비밀번호 확인 입력"
                      value={props.data.pwd_confirm || ""}
                      disabled={!props.isNew && !props.isChangePwd}
                      maxLength={20}
                      onChange={(evt) => props.handleInputChange("pwd_confirm", evt.target.value)}
                    />
                  </FormGroup>{" "}
                  {!props.isNew && (
                    <Button onClick={(evt) => props.onChangePwd()} className="btn-pwd">
                      {!props.isChangePwd ? "변경 하기" : "변경 취소"}
                    </Button>
                  )}
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button disabled={(!props.isNew && props.idx === -1) || props.auth.user.level > props.data.level} onClick={(evt) => props.onSave()}>
                  {props.isNew ? "등록" : "저장"}
                </Button>
                <Button onClick={(evt) => props.onCancel()}>닫기</Button>
              </ButtonToolbar>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab08;
