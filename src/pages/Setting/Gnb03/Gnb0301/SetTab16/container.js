// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab16 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    preferences: PropTypes.object.isRequired,
    modify: PropTypes.object.isRequired,
  };

  render() {
    return <SetTab16 {...this.props} {...this.state} />;
  }
}

export default withSnackbar(Container);
