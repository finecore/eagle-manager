/* eslint-disable react/no-unescaped-entities */
// imports.
import React from "react";

import { ButtonToolbar, Button, ButtonGroup, Radio, FormGroup, ControlLabel, FormControl } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab16 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030116">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>PMS 업체명</h2>
              <div className="form-content">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="업체명 입력"
                    defaultValue={props.modify.pms || ""}
                    onChange={(evt) => props.handleInputChange("pms", evt.target.value)}
                    disabled={!props.checkAuth()}
                  />
                </FormGroup>
              </div>
            </li>
            <li>
              <h2>PMS 업체 ID</h2>
              <div className="form-content">
                <FormGroup>
                  <ControlLabel>PMS 업체에서 발급한 연동 ID (없으면 업체명 입력)</ControlLabel>
                  <FormControl
                    type="text"
                    placeholder="업체ID 입력"
                    defaultValue={props.modify.pms_company_id || (props.modify.pms || "").toUpperCase() || ""}
                    onChange={(evt) => props.handleInputChange("pms_company_id", evt.target.value)}
                    disabled={!props.checkAuth()}
                  />
                </FormGroup>
              </div>
            </li>
            {props.modify.pms === "micronic" ? (
              <li>
                <h2>PMS 소켓 연동 인증 KEY (Micronic)</h2>
                <div className="form-content">
                  <FormGroup>
                    <ControlLabel>PMS 업체에서 발급한 업소 인증키 (이글 -> PMS 전송용)</ControlLabel>
                    <FormControl
                      componentClass="textarea"
                      rows="2"
                      placeholder="소켓 연동 키 입력"
                      defaultValue={(props.checkAuth() ? props.modify.pms_socket_access_key : "********************************************************") || ""}
                      onChange={(evt) => props.handleInputChange("pms_socket_access_key", evt.target.value)}
                      disabled={!props.checkAuth()}
                    />
                  </FormGroup>
                </div>
              </li>
            ) : (
              <li>
                <h2>PMS 연동 API 인증 KEY</h2>
                <div className="form-content">
                  <FormGroup>
                    <ControlLabel>PMS 업체에서 발급한 업소 인증키 (이글 -> PMS 전송용)</ControlLabel>
                    <FormControl
                      componentClass="textarea"
                      rows="2"
                      placeholder="API 연동 키 입력 (없으면 생략)"
                      defaultValue={(props.checkAuth() ? props.modify.pms_api_key : "********************************************************") || ""}
                      onChange={(evt) => props.handleInputChange("pms_api_key", evt.target.value)}
                      disabled={!props.checkAuth()}
                    />
                  </FormGroup>
                </div>
              </li>
            )}
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab16;
