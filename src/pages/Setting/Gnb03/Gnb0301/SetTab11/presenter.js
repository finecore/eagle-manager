// imports.
import React from "react";

import { FormControl, ButtonToolbar, ButtonGroup, Button, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import moment from "moment";
import cx from "classnames";

import { CheckSubscribe } from "utils/subscribe-util";

// styles.
import "./styles.scss";

const SetTab11 = (props) => {
  let pre_check_in_time_stay = props.modify.pre_check_in_time_stay.split(":");
  let pre_check_in_time_rent = props.modify.pre_check_in_time_rent.split(":");

  const {
    user: { type, level },
    isViewerLogin,
    isIcrewViewer,
  } = props.auth;

  // 아이크루 어드민 여부.
  let isAdmin = isIcrewViewer || isViewerLogin || (type === 1 && level < 3);

  console.log("- isAdmin", isAdmin);

  return (
    <div>
      <div className="setting-form" id="gnb030111">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>예약 숙박 입실가능 시간</h2>
              <div className="form-content">
                <div>
                  체크인 시간{" "}
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={pre_check_in_time_stay[0]}
                      step={1}
                      min={0}
                      max={6}
                      onChange={(value) => props.handleInputChange("pre_check_in_time_stay", String(value).padStart(2, "0") + ":" + String(pre_check_in_time_stay[1]).padStart(2, "0"))}
                      formatter={(value) => String(value).padStart(2, "0") + " 시간"}
                    />
                  </span>
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={pre_check_in_time_stay[1]}
                      step={10}
                      min={0}
                      max={59}
                      onChange={(value) => props.handleInputChange("pre_check_in_time_stay", String(pre_check_in_time_stay[0]).padStart(2, "0") + ":" + String(value).padStart(2, "0"))}
                      formatter={(value) => String(value).padStart(2, "0") + " 분"}
                    />
                  </span>{" "}
                  전 부터 입실 가능
                </div>
              </div>
            </li>
            <li>
              <h2>예약 대실 입실가능 시간</h2>
              <div className="form-content">
                체크인 시간{" "}
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={pre_check_in_time_rent[0]}
                    step={1}
                    min={0}
                    max={6}
                    onChange={(value) => props.handleInputChange("pre_check_in_time_rent", String(value).padStart(2, "0") + ":" + String(pre_check_in_time_rent[1]).padStart(2, "0"))}
                    formatter={(value) => String(value).padStart(2, "0") + " 시간"}
                  />
                </span>
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={pre_check_in_time_rent[1]}
                    step={10}
                    min={0}
                    max={59}
                    onChange={(value) => props.handleInputChange("pre_check_in_time_rent", String(pre_check_in_time_rent[0]).padStart(2, "0") + ":" + String(value).padStart(2, "0"))}
                    formatter={(value) => String(value).padStart(2, "0") + " 분"}
                  />
                </span>{" "}
                전 부터 입실 가능
              </div>
            </li>
            <li>
              <h2>예약 객실 정리 시간 (청소 시간)</h2>
              <div className="form-content">
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={props.modify.pre_check_in_time_clean}
                    step={5}
                    min={0}
                    max={180}
                    onChange={(value) => props.handleInputChange("pre_check_in_time_clean", value)}
                    formatter={(value) => value + " 분"}
                  />
                </span>
              </div>
            </li>
            <li>
              <h2>예약 화면표시 일자 설정</h2>
              <div className="form-content">
                체크인 시간{" "}
                <span className="numeric-wrap">
                  <InputNumber
                    className="form-control mid"
                    value={props.modify.reserv_notice_day}
                    step={1}
                    min={1}
                    max={31}
                    onChange={(value) => props.handleInputChange("reserv_notice_day", value)}
                    formatter={(value) => value + " 일"}
                  />
                </span>{" "}
                전 ({moment().add(props.modify.reserv_notice_day, "day").format("YYYY-MM-DD HH:mm")}) 일 이내 표시
              </div>
            </li>
            <li>
              <h2>예약 시 객실 자동배정</h2>
              <div className="form-content">
                <div>
                  <ButtonGroup aria-label="reserv_auto_assign_room" onChange={(evt) => props.handleInputChange("reserv_auto_assign_room", evt.target.value)}>
                    <Radio
                      inline
                      name="reserv_auto_assign_room"
                      value={1}
                      defaultChecked={props.modify.reserv_auto_assign_room === 1}
                      className={cx(props.modify.reserv_auto_assign_room === 1 && "checked-on")}
                    >
                      <span />
                      사용
                    </Radio>
                    <Radio inline name="reserv_auto_assign_room" value={0} defaultChecked={props.modify.reserv_auto_assign_room === 0}>
                      <span />
                      사용안함
                    </Radio>
                  </ButtonGroup>
                </div>
              </div>
            </li>
            <li>
              <h2>예약번호 QR코드 발송 여부</h2>
              <div className="form-content">
                {CheckSubscribe(props, "31", false) ? (
                  <div>
                    <ButtonGroup aria-label="reserv_num_send_qr_code" onChange={(evt) => props.handleInputChange("reserv_num_send_qr_code", evt.target.value)}>
                      <Radio
                        inline
                        name="reserv_num_send_qr_code"
                        value={1}
                        defaultChecked={props.modify.reserv_num_send_qr_code === 1}
                        className={cx(props.modify.reserv_num_send_qr_code === 1 && "checked-on")}
                      >
                        <span />
                        사용
                      </Radio>
                      <Radio inline name="reserv_num_send_qr_code" value={0} defaultChecked={props.modify.reserv_num_send_qr_code === 0}>
                        <span />
                        사용안함
                      </Radio>
                    </ButtonGroup>
                  </div>
                ) : (
                  <span>
                    ※ 구독관리 &gt; 구독추가 &gt; <font color="red">예약번호 QR코드 발송 서비스</font>를 먼저 구독 바랍니다.
                  </span>
                )}
              </div>
            </li>
            {isAdmin && (
              <li>
                <h2>OTA 업체에 등록된 업소명 (아이크루 관리자만 보임)</h2>
                <span>
                  ※ <font color="red">안드로이드 폰이 없거나 문자 전송이 안될때만 사용</font> 하기기 바랍니다.
                </span>
                <br /> <br />
                <div className="form-content">
                  [야놀자]에 등록된 업소명
                  <FormControl
                    type="text"
                    className="text"
                    placeholder="업소명 입력"
                    defaultValue={props.modify.ota_place_name_1}
                    onChange={(evt) => props.handleInputChange("ota_place_name_1", evt.target.value)}
                  />
                </div>
                <div className="form-content">
                  [여기어때]에 등록된 업소명
                  <FormControl
                    type="text"
                    className="text"
                    placeholder="업소명 입력"
                    defaultValue={props.modify.ota_place_name_2}
                    onChange={(evt) => props.handleInputChange("ota_place_name_2", evt.target.value)}
                  />
                </div>
                <div className="form-content">
                  [네이버]에 등록된 업소명
                  <FormControl
                    type="text"
                    className="text"
                    placeholder="업소명 입력"
                    defaultValue={props.modify.ota_place_name_3}
                    onChange={(evt) => props.handleInputChange("ota_place_name_3", evt.target.value)}
                  />
                </div>
                <span> ※ OTA 예약 문자에 표시되는 업소명을 입력해 주세요.</span>
                <br />
                <span>
                  ※{" "}
                  <font color="red">
                    아이크루 공용폰에서 전송 되며 전화기 고장 등의 이유로
                    <b> 예약 문자 전송이 되지 않을 </b>
                  </font>
                  수 있습니다
                </span>
                <br />
                <span> ※ [OTA 예약 자동 등록 서비스] 사용 시 필수 입니다.</span>
              </li>
            )}
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab11;
