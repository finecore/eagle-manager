// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab11 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = { preferences: PropTypes.object.isRequired };

  render() {
    return <SetTab11 {...this.props} {...this.state} onChecked={this.onChecked} handleSave={this.handleSave} />;
  }
}

export default withSnackbar(Container);
