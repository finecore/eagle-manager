// imports.
import React from "react";

import { ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";

// styles.
import "./styles.scss";

const SetTab05 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030105">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>데이터 보관 기간 설정</h2>
              <div className="form-content">
                <div>
                  매출이력 정보
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.data_keep_day_sale}
                      step={1}
                      min={3}
                      max={180}
                      onChange={(value) => props.handleInputChange("data_keep_day_sale", value)}
                    />
                  </span>
                  일 (정산 시간 기준)
                </div>

                <div>
                  객실이력 정보
                  <span className="numeric-wrap">
                    <InputNumber
                      className="form-control mid"
                      value={props.modify.data_keep_day_state}
                      step={1}
                      min={3}
                      max={60}
                      onChange={(value) => props.handleInputChange("data_keep_day_state", value)}
                    />
                  </span>
                  일 (0 시 기준)
                </div>
                <br />
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSaveData()}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab05;
