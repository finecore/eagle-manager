// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab05 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = { preferences: PropTypes.object.isRequired };

  onSaveData = () => {
    let text = "데이터 보관 기간 이후의 데이터는 자동 삭제 됩니다.<br/><br/><font color=red>삭제된 데이터는 복구 할 수 없습니다.</font><br/><br/>정말 수정 하시겠습니까?";

    this.props.onSave(text);
  };

  render() {
    return <SetTab05 {...this.props} {...this.state} onSaveData={this.onSaveData} />;
  }
}

export default withSnackbar(Container);
