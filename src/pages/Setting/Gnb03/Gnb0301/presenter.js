// imports.
import React from "react";
import Modal from "react-awesome-modal";

// Components.
import SetTab01 from "./SetTab01";
import SetTab02 from "./SetTab02";
import SetTab03 from "./SetTab03";
import SetTab04 from "./SetTab04";
import SetTab05 from "./SetTab05";
import SetTab06 from "./SetTab06";
import SetTab07 from "./SetTab07";
import SetTab08 from "./SetTab08";
import SetTab09 from "./SetTab09";
import SetTab10 from "./SetTab10";
import SetTab11 from "./SetTab11";
import SetTab12 from "./SetTab12";
import SetTab13 from "./SetTab13";
import SetTab14 from "./SetTab14";
import SetTab15 from "./SetTab15";
import SetTab16 from "./SetTab16";
import SetTab17 from "./SetTab17";

import { Tab, Nav, NavItem, Form } from "react-bootstrap";

// icons
import PaymentIcon from "@material-ui/icons/Payment";
import AssignmenttIcon from "@material-ui/icons/Assignment";
import LockIcon from "@material-ui/icons/Lock";
import TuneIcon from "@material-ui/icons/Tune";
import ComputerIcon from "@material-ui/icons/Computer";
import CloudCircle from "@material-ui/icons/CloudCircle";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import BallotIcon from "@material-ui/icons/Ballot";
import TouchApp from "@material-ui/icons/TouchApp";
import SupervisedUserCircle from "@material-ui/icons/SupervisedUserCircle";
import Event from "@material-ui/icons/Event";
import PowerSettingsNew from "@material-ui/icons/PowerSettingsNew";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import AvTimer from "@material-ui/icons/AvTimer";
import Share from "@material-ui/icons/Share";
import Power from "@material-ui/icons/Power";
import DoorLock from "@material-ui/icons/MeetingRoom";

import { CheckSubscribe } from "utils/subscribe-util";

// styles.
import "./styles.scss";

// functional component
const Gnb0301 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb03">
          <h1>환경 설정</h1>
          <Form className="setting-tab-scroll">
            <Tab.Container className="setting-form-tab" id="settingTab" defaultActiveKey={2}>
              <div className="clearfix">
                <div className="setting-tabs">
                  <Nav bsStyle="pills" stacked>
                    <NavItem key={1} eventKey={1}>
                      <PaymentIcon />
                      <span>요금 설정</span>
                    </NavItem>
                    <NavItem key={2} eventKey={2}>
                      <AssignmenttIcon />
                      <span>운영 규칙 설정</span>
                    </NavItem>
                    <NavItem key={14} eventKey={14}>
                      <AvTimer />
                      <span>입실 시간 옵션</span>
                    </NavItem>
                    <NavItem key={9} eventKey={9}>
                      <BallotIcon />
                      <span>무인 판매 설정</span>
                    </NavItem>
                    <NavItem key={11} eventKey={11}>
                      <Event />
                      <span>예약 설정</span>
                    </NavItem>
                    {CheckSubscribe(props, "02", false) && (
                      <NavItem key={15} eventKey={15}>
                        <Event />
                        <span>포인트 설정</span>
                      </NavItem>
                    )}
                    <NavItem key={3} eventKey={3}>
                      <LockIcon />
                      <span>잠금 설정</span>
                    </NavItem>
                    <NavItem key={4} eventKey={4}>
                      <TuneIcon />
                      <span>냉/난방 설정</span>
                    </NavItem>
                    <NavItem key={6} eventKey={6}>
                      <ComputerIcon />
                      <span>화면 설정</span>
                    </NavItem>
                    <NavItem key={7} eventKey={7}>
                      <VolumeUpIcon />
                      <span>음성 설정</span>
                    </NavItem>
                    <NavItem key={12} eventKey={12}>
                      <Power />
                      <span>전원 설정</span>
                    </NavItem>
                    <NavItem key={10} eventKey={10}>
                      <TouchApp />
                      <span>알림 설정</span>
                    </NavItem>
                    {CheckSubscribe(props, "12", false) && (
                      <NavItem key={16} eventKey={16}>
                        <Share />
                        <span>PMS 설정</span>
                      </NavItem>
                    )}
                    {CheckSubscribe(props, "32", false) && (
                      <NavItem key={17} eventKey={17}>
                        <DoorLock />
                        <span>도어락 설정</span>
                      </NavItem>
                    )}
                    <NavItem key={5} eventKey={5}>
                      <CloudCircle />
                      <span>데이터 관리</span>
                    </NavItem>
                    <NavItem key={8} eventKey={8}>
                      <SupervisedUserCircle />
                      <span>직원 관리</span>
                    </NavItem>
                    <NavItem key={13} eventKey={13}>
                      <CheckBoxIcon />
                      <span>구독 관리</span>
                    </NavItem>
                  </Nav>
                </div>
                <div className="setting-form-wrapper" id="gnb0301">
                  <Tab.Content animation>
                    <Tab.Pane key={1} eventKey={1} mountOnEnter unmountOnExit>
                      <SetTab01 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={2} eventKey={2} mountOnEnter unmountOnExit>
                      <SetTab02 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={3} eventKey={3} mountOnEnter unmountOnExit>
                      <SetTab03 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={4} eventKey={4} mountOnEnter unmountOnExit>
                      <SetTab04 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={5} eventKey={5} mountOnEnter unmountOnExit>
                      <SetTab05 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={6} eventKey={6} mountOnEnter unmountOnExit>
                      <SetTab06 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={7} eventKey={7} mountOnEnter unmountOnExit>
                      <SetTab07 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={8} eventKey={8} mountOnEnter unmountOnExit>
                      <SetTab08 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={9} eventKey={9} mountOnEnter unmountOnExit>
                      <SetTab09 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={10} eventKey={10} mountOnEnter unmountOnExit>
                      <SetTab10 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={11} eventKey={11} mountOnEnter unmountOnExit>
                      <SetTab11 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={12} eventKey={12} mountOnEnter unmountOnExit>
                      <SetTab12 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={13} eventKey={13} mountOnEnter unmountOnExit>
                      <SetTab13 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={14} eventKey={14} mountOnEnter unmountOnExit>
                      <SetTab14 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={15} eventKey={15} mountOnEnter unmountOnExit>
                      <SetTab15 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={16} eventKey={16} mountOnEnter unmountOnExit>
                      <SetTab16 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={17} eventKey={17} mountOnEnter unmountOnExit>
                      <SetTab17 {...props} />
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </Form>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0301;
