// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomStateAction } from "actions/roomState";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { preferences, roomState } = state;
  return {
    preferences,
    roomState,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
    handleSaveRoomStateAll: (placeId, roomState, callback) => dispatch(roomStateAction.putRoomStateAll(placeId, roomState, callback)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
