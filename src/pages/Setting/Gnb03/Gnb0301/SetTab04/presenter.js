// imports.
import React from "react";

import { ControlLabel, ButtonToolbar, Button, ButtonGroup, Radio } from "react-bootstrap";

import InputNumber from "rc-input-number";
import cx from "classnames";

// styles.
import "./styles.scss";

const SetTab04 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb030104">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>난방 온도 설정</h2>
              <div className="form-content">
                <div>
                  <ControlLabel htmlFor="gnb030104-01">사용 중</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-01"
                      className="form-control small"
                      value={props.modify.temp_key_1}
                      step={1}
                      min={0}
                      max={63}
                      onChange={(value) => props.handleInputChange("temp_key_1", value)}
                    />
                  </span>
                  <ControlLabel htmlFor="gnb030104-02">외출 중</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-02"
                      className="form-control small"
                      value={props.modify.temp_key_2}
                      step={1}
                      min={0}
                      max={63}
                      onChange={(value) => props.handleInputChange("temp_key_2", value)}
                    />
                  </span>
                  <ControlLabel htmlFor="gnb030104-04">{props.modify.inspect_use ? "청소/점검대기" : "청소대기"}</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-04"
                      className="form-control small"
                      value={props.modify.temp_key_5}
                      step={1}
                      min={0}
                      max={63}
                      onChange={(value) => props.handleInputChange("temp_key_5", value)}
                    />
                  </span>
                  <ControlLabel htmlFor="gnb030104-05">{props.modify.inspect_use ? "청소/점검 중" : "청소 중"}</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-05"
                      className="form-control small"
                      value={props.modify.temp_key_4}
                      step={1}
                      min={0}
                      max={63}
                      onChange={(value) => props.handleInputChange("temp_key_4", value)}
                    />
                  </span>
                  <ControlLabel htmlFor="gnb030104-06">공실</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-06"
                      className="form-control small"
                      value={props.modify.temp_key_3}
                      step={1}
                      min={0}
                      max={63}
                      onChange={(value) => props.handleInputChange("temp_key_3", value)}
                    />
                  </span>
                </div>
              </div>
            </li>
            <li>
              <h2>고객 설정 최소/최대 온도</h2>
              <div className="form-content">
                <div>
                  <ControlLabel htmlFor="gnb030104-08">최소</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-08"
                      className="form-control small"
                      value={props.modify.air_set_min}
                      step={1}
                      min={0}
                      max={20}
                      onChange={(value) => props.handleInputChange("air_set_min", value)}
                    />
                  </span>
                  <ControlLabel htmlFor="gnb030104-09">최대</ControlLabel>
                  <span className="numeric-wrap">
                    <InputNumber
                      id="gnb030104-09"
                      className="form-control small"
                      value={props.modify.air_set_max}
                      step={1}
                      min={0}
                      max={50}
                      onChange={(value) => props.handleInputChange("air_set_max", value)}
                    />
                  </span>
                </div>
              </div>
            </li>
            <li>
              <h2>에어컨 전원 설정</h2>
              <div className="form-content">
                <ButtonGroup aria-label="air_power_type" onChange={(evt) => props.handleInputChange("air_power_type", Number(evt.target.value))}>
                  <Radio inline name="air_power_type" value="0" defaultChecked={props.modify.air_power_type === 0} className={cx(props.modify.air_power_type === 0 && "checked-on")}>
                    <span />
                    키에 연동
                  </Radio>
                  <Radio inline name="air_power_type" value="1" defaultChecked={props.modify.air_power_type === 1} className={cx(props.modify.air_power_type === 1 && "checked-on")}>
                    <span />
                    전원 ON
                  </Radio>
                  <Radio inline name="air_power_type" value="2" defaultChecked={props.modify.air_power_type === 2} className={cx(props.modify.air_power_type === 2 && "checked-on")}>
                    <span />
                    전원 OFF
                  </Radio>
                </ButtonGroup>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.handleSave(evt)}>저장</Button>
          <Button onClick={(evt) => props.onCancel(evt)} className="btn-set-cancel">
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab04;
