// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import SetTab04 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
    };
  }

  static propTypes = {
    preferences: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
  };

  onChecked = (no) => {
    let { isAllRoom } = this.state;

    isAllRoom[no] = !isAllRoom[no];

    console.log("-- onChecked", isAllRoom);

    this.setState({
      isAllRoom,
    });
  };

  // 정보 변경 이벤트.
  handleSave = (evt) => {
    // let { isAllRoom } = this.state;
    const {
      preferences: {
        item: {
          place_id,
          air_set_temp,
          temp_key_1, // 사용중
          temp_key_2, // 외출중
          temp_key_3, // 공실
          temp_key_4, // 청소중
          temp_key_5, // 청소대기
          air_set_min,
          air_set_max,
        },
      },
    } = this.props;

    let text = "설정 온도를 모든 객실에 적용 하시겠습니까?";

    this.props.onSave(text, (confirm) => {
      if (confirm) {
        // 모든 객실 적용.
        const room_state = {};

        if (this.props.modify.air_set_temp && this.props.modify.air_set_temp !== air_set_temp) room_state.air_set_temp = this.props.modify.air_set_temp;
        if (this.props.modify.temp_key_1 && this.props.modify.temp_key_1 !== temp_key_1) room_state.temp_key_1 = this.props.modify.temp_key_1;
        if (this.props.modify.temp_key_2 && this.props.modify.temp_key_2 !== temp_key_2) room_state.temp_key_2 = this.props.modify.temp_key_2;
        if (this.props.modify.temp_key_3 && this.props.modify.temp_key_3 !== temp_key_3) room_state.temp_key_3 = this.props.modify.temp_key_3;
        if (this.props.modify.temp_key_4 && this.props.modify.temp_key_4 !== temp_key_4) room_state.temp_key_4 = this.props.modify.temp_key_4;
        if (this.props.modify.temp_key_5 && this.props.modify.temp_key_5 !== temp_key_5) room_state.temp_key_5 = this.props.modify.temp_key_5;

        // 고객 설정 최소/최대 온도.
        if (this.props.modify.air_set_min && this.props.modify.air_set_min !== air_set_min) room_state.air_set_min = this.props.modify.air_set_min;
        if (this.props.modify.air_set_max && this.props.modify.air_set_max !== air_set_max) room_state.air_set_max = this.props.modify.air_set_max;

        if (!_.isEmpty(room_state))
          this.props.handleSaveRoomStateAll(place_id, {
            room_state,
          });
      }
    });
  };

  render() {
    return <SetTab04 {...this.props} {...this.state} onChecked={this.onChecked} handleSave={this.handleSave} />;
  }
}

export default withSnackbar(Container);
