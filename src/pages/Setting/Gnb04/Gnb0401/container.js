// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Gnb0401 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      tabKey: 1,
      item: {},
    };
  }

  setActiveTab = (tabKey, item) => {
    // console.log("- setActiveTab", tabKey, item);

    this.setState({ tabKey, item });
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {};

  componentWillUnmount = () => {};

  UNSAFE_componentWillReceiveProps(nextProps) {}

  render() {
    return <Gnb0401 {...this.props} {...this.state} closeModal={this.closeModal} changeTab={this.setActiveTab} />;
  }
}

export default withSnackbar(Container);
