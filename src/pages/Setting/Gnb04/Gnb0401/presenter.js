// imports.
import React from "react";
import Modal from "react-awesome-modal";

// Components.
import SetTab01 from "./SetTab01";
import SetTab02 from "./SetTab02";

import { Tab, Nav, NavItem, Form } from "react-bootstrap";

// icons
import ListAlt from "@material-ui/icons/ListAlt";
import Publish from "@material-ui/icons/Publish";
import Edit from "@material-ui/icons/Edit";

// styles.
import "./styles.scss";

// functional component
const Gnb0401 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb04">
          <h1>예약 관리</h1>
          <Form className="setting-tab-scroll">
            <Tab.Container className="setting-form-tab" id="settingTab" defaultActiveKey={1} activeKey={props.tabKey} onSelect={(key) => props.changeTab(key, {})}>
              <div className="clearfix">
                <div className="setting-tabs">
                  <Nav bsStyle="pills" stacked>
                    <NavItem key={1} eventKey={1}>
                      <ListAlt />
                      예약 목록
                    </NavItem>
                    <NavItem key={2} eventKey={2}>
                      {props.item.id ? <Edit /> : <Publish />}
                      {props.item.id ? "예약 수정" : "예약 등록"}
                    </NavItem>
                  </Nav>
                </div>
                <div className="setting-form-wrapper" id="gnb0401">
                  <Tab.Content animation>
                    <Tab.Pane key={1} title="예약목록" eventKey={1} mountOnEnter unmountOnExit>
                      <SetTab01 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={2} title="예약등록" eventKey={2} mountOnEnter unmountOnExit>
                      <SetTab02 {...props} />
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </Form>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0401;
