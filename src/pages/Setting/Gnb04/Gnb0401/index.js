// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomAction } from "actions/room";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room } = state;
  return {
    auth,
    user: auth.user,
    room,
    preferences,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomEnableReservs: (placeId) => dispatch(roomAction.getRoomEnableReservs(placeId)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
