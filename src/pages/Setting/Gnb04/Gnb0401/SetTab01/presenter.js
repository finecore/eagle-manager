// imports.
import React from "react";

import { FormGroup, FormControl, Radio, Table, ButtonToolbar, Button } from "react-bootstrap";
import { SingleDatePicker } from "react-dates";

import cx from "classnames";
import moment from "moment";
import { keyToValue } from "constants/key-map";
import format from "utils/format-util";

// Components.
import SearchInput from "components/FormInputs/SearchInput";

// styles.
import "./styles.scss";

const SetTab01 = (props) => {
  let column = ["ota_type", "reserv_num", "stay_type", "state", "check_in_exp", "check_out_exp", "hp", "room_type_name", "room_name", "name", "sale_fee", "reserv_fee", "reserv_date"];
  let title = ["OTA", "예약번호", "타입", "상태", "입실일시", "퇴실일시", "휴대폰번호", "객실타입", "배정객실", "고객명", "판매가", "예약가", "접수일자"];

  return (
    <div>
      <div className="setting-form" id="gnb040101">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>객실 예약 목록</h2>
              <div className="form-content">
                <FormGroup>
                  <div className="form-content">
                    <Radio inline name="search_term" checked={props.mode === 1} onChange={(evt) => props.handleTerm(1, "day", -1)}>
                      <span />
                      어제
                    </Radio>
                    <Radio inline name="search_term" checked={props.mode === 2} onChange={(evt) => props.handleTerm(2, "day", 0)}>
                      <span />
                      오늘
                    </Radio>
                    <Radio inline name="search_term" checked={props.mode === 3} onChange={(evt) => props.handleTerm(3, "week")}>
                      <span />
                      이번주
                    </Radio>
                    <Radio inline name="search_term" checked={props.mode === 4} onChange={(evt) => props.handleTerm(4, "month")}>
                      <span />
                      이번달
                    </Radio>
                  </div>
                  <div className="date-set">
                    <SingleDatePicker
                      id="startsOnDateInput-1"
                      placeholder="검색 시작일 선택"
                      displayFormat="YYYY-MM-DD"
                      date={props.begin}
                      onDateChange={(date) => props.onDataChange({ begin: date })}
                      focused={props.focused1}
                      onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                      isOutsideRange={(day) => moment().add(-1, "year").isSameOrAfter(day)}
                      disabled={false}
                    />
                    ~
                    <SingleDatePicker
                      id="endsOnDateInput-2"
                      placeholder="검색 종료일 선택"
                      displayFormat="YYYY-MM-DD"
                      date={props.end}
                      onDateChange={(date) => props.onDataChange({ end: date })}
                      focused={props.focused2}
                      onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                      isOutsideRange={(day) => moment(props.begin).isSameOrAfter(day)}
                    />
                  </div>
                  <FormControl componentClass="select" placeholder="예약 상태" className="reserv-state" value={props.selState || ""} onChange={(evt) => props.onStateSelected(evt.target.value)}>
                    <option value="">전체 예약</option>
                    <option value="A">정상예약</option>
                    <option value="B">취소예약</option>
                    <option value="C">사용완료</option>
                  </FormControl>
                  <FormControl componentClass="select" placeholder="객실 타입" value={props.selRoomTypeId || ""} onChange={(evt) => props.onRoomTypeSelected(evt.target.value)}>
                    <option value="">예약 객실타입 선택</option>
                    {props.roomTypeList.map((item, i) => (
                      <option key={i} value={item.room_type_id}>
                        {item.room_type_name}
                      </option>
                    ))}
                  </FormControl>
                  <FormControl componentClass="select" placeholder="객실명" onChange={(evt) => props.onRoomSelected(evt.target.value)}>
                    <option value="">예약 객실 선택</option>
                    {props.roomList.map((item, i) => (
                      <option key={i} value={item.room_id}>
                        {item.room_name}
                      </option>
                    ))}
                  </FormControl>
                  <SearchInput colWidth="col-md-2" type="text" placeholder="검색어 입력" value={props.searchText} onSearch={(value) => props.onReservSearch(value)} maxLength={20} />
                </FormGroup>
                <div className="search-result">
                  <div className="count">
                    <span>
                      조회 건수 : <strong>{props.list.length}</strong>
                    </span>
                  </div>
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          {column.map((col, i) => (
                            <th key={i} className={cx(props.sort === col ? "on" : "off")} onClick={(evt) => props.onSortClick(col)}>
                              {title[i]}
                              <b className={cx("caret", props.reverse ? "desc" : "asc")} />
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {props.list.map((item, i) => (
                          <tr
                            key={i}
                            onClick={(evt) => props.onItemClick(item)}
                            className={cx(item.state === "A" ? (moment(item.check_out_exp).isBefore(moment()) ? "over" : "") : item.state === "B" ? "cancel" : item.state === "C" ? "done" : "")}
                          >
                            <td>{keyToValue("room_reserv", "ota_code", item.ota_code)}</td>
                            <td>{String(item.reserv_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")}</td>
                            <td style={{ color: props.preferences.stay_type_color[Number(item.stay_type)] }}>{keyToValue("room_sale", "stay_type", item.stay_type)}</td>
                            <td>{item.state === "A" && moment(item.check_out_exp).isBefore(moment()) ? <font color="red">시간초과</font> : keyToValue("room_reserv", "state", item.state)}</td>
                            <td>{moment(item.check_in_exp).format("YYYY-MM-DD HH:mm")}</td>
                            <td>{moment(item.check_out_exp).format("YYYY-MM-DD HH:mm")}</td>
                            <td>{format.toPhone(item.hp)}</td>
                            <td style={{ color: item.room_type_name ? "#0909ff" : "#f50a0a" }}>{item.room_type_name || "미배정"}</td>
                            <td style={{ color: item.room_name ? "#0909ff" : "#f50a0a" }}>{item.room_name || "미배정"}</td>
                            <td>{item.name || "미등록고객"}</td>
                            <td>{`${item.room_fee || 0}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{`${item.reserv_fee || 0}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{moment(item.reserv_date).format("YYYY-MM-DD HH:mm")}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab01;
