// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import moment from "moment";
import _ from "lodash";
import format from "utils/format-util";
import Swal from "sweetalert2";

// Components.
import RoomTab02 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    roomReserv: PropTypes.object.isRequired, // 현재 객실 예약 정보.
    preferences: PropTypes.object.isRequired,
    changeTab: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: [],
      list: [],
      roomList: [],
      roomTypeList: [],
      sort: "check_in_exp",
      reverse: true,
      mode: 2,
      selState: "A",
      selRoomTypeId: 0,
      selRoomId: 0,
      searchText: "",
      begin: moment(),
      end: moment(),
      focused1: null,
      focused2: null,
    };

    this.timer = null;
  }

  handleTerm = (mode, type, value) => {
    let { begin, end } = this.state;

    if (type === "day") {
      // 어제, 오늘
      begin = moment().add(value, type);
      end = moment().add(value, type);
    } else if (type === "week" || type === "month") {
      // 이번주,이번달
      begin = moment().startOf(type);
      end = moment().endOf(type);
    }

    console.log("- handleTerm", mode, value, type, begin.format("YYYY-MM-DD HH:mm"), end.format("YYYY-MM-DD HH:mm:ss"));

    this.setState(
      {
        begin,
        end,
        mode,
      },
      () => {
        this.handleSerach();
      }
    );
  };

  handleSerach = () => {
    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    let { begin, end, mode } = this.state;

    let limitDate = moment(begin).add(6, "month");

    if (limitDate.isBefore(moment(end))) {
      Swal.fire({
        icon: "warning",
        title: "입력 확인",
        html: `예약 조회 가능 기간은 6개월 이내 입니다.`,
        showCloseButton: true,
        confirmButtonText: "확인",
      }).then((result) => {
        if (result.value) {
          this.setState(
            {
              begin,
              end: limitDate,
            },
            () => {
              this.handleSerach();
            }
          );
        }
      });
    } else {
      begin = moment(begin).format("YYYY-MM-DD 00:00:00");
      end = moment(end).format("YYYY-MM-DD 23:59:59");

      console.log("- handleSerach", { place_id, begin, end, mode });

      // 객실 예약 정보 조회
      this.props.searchAllRoomReservs(place_id, begin, end);
    }
  };

  onStateSelected = (state) => {
    let { search, sort, reverse, selRoomTypeId, selRoomId, searchText } = this.state;
    let { roomReserv } = this.props;

    console.log("-- onStateSelected", { state, selRoomTypeId, selRoomId });

    let list = _.filter(search, function (o) {
      return (selRoomTypeId ? o.room_type_id === selRoomTypeId : true) && (selRoomId ? o.room_id === selRoomId : true) && (state ? o.state === state : true);
    });

    list = format.textFilter(list, searchText);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selState: state,
    });
  };

  onRoomTypeSelected = (room_type_id) => {
    let { search, sort, reverse, selState, searchText } = this.state;
    let { roomReserv } = this.props;

    room_type_id = Number(room_type_id);

    console.log("-- onRoomTypeSelected", { room_type_id, selState });

    let list = _.filter(search, function (o) {
      return (selState ? o.state === selState : true) && (room_type_id ? o.room_type_id === room_type_id : true);
    });

    list = format.textFilter(list, searchText);

    console.log("-- list", list);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    let roomList = [];

    _.map(list, (item) => {
      if (room_type_id === item.room_type_id) {
        const { room_id, room_name } = item;
        if (room_id && !_.find(roomList, { room_id })) roomList.push({ room_id, room_name });
      }
    });

    this.setState({
      list,
      roomList,
      selRoomTypeId: room_type_id,
      selRoomId: 0,
    });
  };

  onRoomSelected = (room_id) => {
    let { search, sort, reverse, selState, selRoomTypeId, searchText } = this.state;
    let { roomReserv } = this.props;

    room_id = Number(room_id);

    console.log("-- onRoomSelected", { room_id, selState, selRoomTypeId });

    let list = _.filter(search, function (o) {
      return (selState ? o.state === selState : true) && (selRoomTypeId ? o.room_type_id === selRoomTypeId : true) && (room_id ? o.room_id === room_id : true);
    });

    list = format.textFilter(list, searchText);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selRoomId: room_id,
    });
  };

  onReservSearch = (search_text) => {
    let { search, sort, reverse, selState, selRoomTypeId, selRoomId } = this.state;
    let { roomReserv } = this.props;

    search_text = String(search_text).replace(/\s/g, "");

    console.log("-- onReservSearch", search_text, { selState, selRoomTypeId, selRoomId });

    let list = _.filter(search, function (o) {
      return (selState ? o.state === selState : true) && (selRoomTypeId ? o.room_type_id === selRoomTypeId : true) && (selRoomId ? o.room_id === selRoomId : true);
    });

    if (this.timer) clearTimeout(this.timer);

    // 타이핑 딜레이..
    if (search_text) {
      this.timer = setTimeout(() => {
        list = format.textFilter(list, search_text);

        if (sort) list = _.sortBy(list, [sort]);
        if (reverse) list = list.reverse();

        this.setState({
          list,
          searchText: search_text,
        });
      }, 100);
    } else {
      this.setState({
        list,
        searchText: search_text,
      });
    }
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  onDataChange = (data) => {
    console.log("- onDataChange", JSON.stringify(data));

    let { begin, end } = this.state;

    if (data.begin && end.isBefore(data.begin)) data.end = data.begin;
    if (data.end && begin.isAfter(data.end)) data.begin = data.end;

    this.setState(
      {
        ...this.state,
        ...data,
        mode: 0,
      },
      () => {
        this.handleSerach();
      }
    );
  };

  onSortClick = (sorted) => {
    let { list, sort, reverse } = this.state;

    console.log("-- onSortClick", sorted, sort, reverse);

    reverse = sort === sorted ? !reverse : false;

    // 재 정렬.
    list = _.orderBy(list, sorted, !reverse ? "asc" : "desc");

    this.setState({
      sort: sorted,
      reverse,
      list,
    });
  };

  onItemClick = (item) => {
    console.log("-- onItemClick", item);

    // 상세 탭으로 전환.
    this.props.changeTab(2, item);
  };

  componentDidMount = () => {
    this.handleTerm(2, "day", 0); // 오늘 일자로 설정.
  };

  componentWillUnmount = () => {};

  shouldComponentUpdate = (nextProps, nextState) => {
    // console.log("-- shouldComponentUpdate");
    return true;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.roomReserv && nextProps.roomReserv.search !== this.props.roomReserv.search) {
      let { sort, reverse, selRoomTypeId, selState, selRoomId, searchText } = this.state;

      let { search } = nextProps.roomReserv;

      // console.log("-- componentWillReceiveProps", { selRoomTypeId, selState, selRoomId });

      // roomReserv search 목록에 저장됨(list 저장시 Room roomReserv 동작으로 속도 저하!)
      let list = _.filter(search, function (o) {
        return (selState ? o.state === selState : true) && (selRoomTypeId ? o.room_type_id === selRoomTypeId : true) && (selRoomId ? o.room_id === selRoomId : true);
      });

      list = format.textFilter(list, searchText);

      // console.log("-- list", list);

      let roomList = [];
      let roomTypeList = [];

      _.map(list, (item) => {
        const { room_type_id, room_type_name, room_id, room_name } = item;

        if (room_type_id) roomTypeList.push({ room_type_id, room_type_name });
        if (room_id) roomList.push({ room_id, room_name });
      });

      roomTypeList = _.uniqBy(roomTypeList, (item) => {
        return item.room_type_id;
      });

      roomList = _.uniqBy(roomList, (item) => {
        return item.room_id;
      });

      if (sort) list = _.sortBy(list, [sort]);
      if (reverse) list = list.reverse();

      // console.log("-- roomTypeList", roomTypeList);

      this.setState({
        search,
        list,
        roomTypeList,
        roomList,
      });
    }
  }

  render() {
    return (
      <RoomTab02
        {...this.props}
        {...this.state}
        handleTerm={this.handleTerm}
        onStateSelected={this.onStateSelected}
        onRoomTypeSelected={this.onRoomTypeSelected}
        onRoomSelected={this.onRoomSelected}
        onReservSearch={this.onReservSearch}
        onItemClick={this.onItemClick}
        onSortClick={this.onSortClick}
        onFocusChange={this.onFocusChange}
        onDataChange={this.onDataChange}
      />
    );
  }
}

export default withSnackbar(Container);
