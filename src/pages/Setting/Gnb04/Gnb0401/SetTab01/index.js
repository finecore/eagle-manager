// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as roomReservAction } from "actions/roomReserv";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, roomReserv, preferences } = state;

  return {
    auth,
    roomReserv,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    searchAllRoomReservs: (palceId, begin, end) => dispatch(roomReservAction.getSearchRoomReservList(palceId, begin, end)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
