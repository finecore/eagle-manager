// imports.
import React from "react";

import { FormGroup, Checkbox, FormControl, ControlLabel, ButtonToolbar, Button } from "react-bootstrap";

import ReactTooltip from "react-tooltip";
import cx from "classnames";

import moment from "moment";
import format from "utils/format-util";
import _ from "lodash";

// import cx from "classnames";
import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

import swal from "sweetalert";
import Swal from "sweetalert2";

// styles.
import "./styles.scss";

const SetTab02 = (props) => {
  let isDisable = props.modify.state === "C" || props.modify.check_out;

  return (
    <div>
      <ReactTooltip
        id={"roomReserv"}
        place="bottom"
        type="info"
        effect="solid"
        delayShow={1000}
        delayHide={0}
        html={true}
        getContent={(dataTip) => `${dataTip}`}
        className={cx("tooltop")}
        disable={props.preferences.tooltip_show !== 1 || props.edit} // 기타 정보
      />

      <div className="setting-form" id="gnb040102">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>{props.reserv.id ? "예약 정보 수정" : "예약 정보 입력"}</h2>
              <div className={cx("form-content")}>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-00">숙박 형태</ControlLabel>
                    <FormControl
                      id="reserve02-00"
                      componentClass="select"
                      placeholder="예약상태"
                      value={props.modify.stay_type}
                      onChange={(evt) => props.handleInputChange("stay_type", Number(evt.target.value))}
                      style={{
                        color: props.preferences.stay_type_color[props.modify.stay_type],
                      }}
                    >
                      <option value={1}>숙박</option>
                      <option value={2}>대실</option>
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-01">예약 상태</ControlLabel>
                    <FormControl
                      id="reserve02-01"
                      componentClass="select"
                      placeholder="예약상태"
                      value={props.modify.state}
                      onChange={(evt) => props.handleInputChange("state", evt.target.value)}
                      style={{
                        color: props.modify.state === "C" ? "#a000ff" : props.modify.state === "B" ? "#ff0000" : "#0000ff",
                      }}
                      disabled={!props.checkAuth()}
                    >
                      <option value={"A"}>정상예약</option>
                      {props.modify.id && <option value={"B"}>취소예약</option>}
                      {props.modify.id && <option value={"C"}>사용완료</option>}
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-02">예약 방법</ControlLabel>
                    <FormControl
                      id="reserve02-02"
                      componentClass="select"
                      placeholder="예약 방법"
                      value={props.modify.ota_code}
                      onChange={(evt) => props.handleInputChange("ota_code", Number(evt.target.value))}
                      style={{
                        color: props.modify.ota_code === 9 ? "#f60ea2" : "",
                      }}
                      disabled={isDisable}
                    >
                      <option value={1}>야놀자</option>
                      <option value={2}>여기어때</option>
                      <option value={3}>네이버</option>
                      <option value={4}>에어비앤비</option>
                      <option value={5}>호텔나우</option>
                      <option value={8}>기타</option>
                      <option value={9}>직접예약</option>
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-03">예약일</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-03"
                      placeholder="예약일"
                      value={moment(props.modify.reserv_date || new Date()).format("YYYY-MM-DD HH:mm")}
                      onChange={(evt) => props.handleInputChange("reserv_date", evt.target.value)}
                      readOnly={true}
                      disabled={isDisable}
                    />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-04">객실 타입</ControlLabel>
                    <FormControl
                      componentClass="select"
                      id="reserve02-04"
                      placeholder="객실 타입"
                      value={props.modify.room_type_id || ""}
                      onChange={(evt) => props.handleInputChange("room_type_id", evt.target.value)}
                      disabled={isDisable}
                    >
                      <option value="">객실타입 선택</option>
                      {props.roomType.list.map((item, i) => (
                        <option key={i} value={item.id}>
                          {item.name}
                        </option>
                      ))}
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-05">객실 배정</ControlLabel>
                    <div>
                      <FormControl
                        id="reserve02-05"
                        componentClass="select"
                        placeholder="배정 가능 객실"
                        value={props.modify.room_id || ""}
                        onChange={(evt) => props.handleInputChange("room_id", evt.target.value ? evt.target.value : null)}
                        style={{
                          color: props.modify.room_id === props.reserv.room_id ? "#2d43e8" : "#ff0000",
                        }}
                        disabled={isDisable || !props.modify.room_type_id}
                      >
                        <option value="">{!props.modify.room_type_id ? "객실타입을 선택해주세요" : props.roomList.length ? "객실 미배정" : "배정가능 객실 없음"}</option>
                        {props.roomList.map((item, i) => (
                          <option key={i} value={item.id}>
                            {item.name}
                            {item.id === props.modify.room_id && props.modify.room_id !== props.reserv.room_id && !props.assign_room ? " [자동 배정]" : null}
                          </option>
                        ))}
                      </FormControl>
                    </div>
                    <Checkbox
                      inline
                      className={cx("checkbox04", props.preferences.reserv_auto_assign_room === 1 && "checked-on")}
                      defaultChecked={props.preferences.reserv_auto_assign_room === 1}
                      onChange={(evt) => props.handlePreferencesSave("reserv_auto_assign_room", evt.target.checked ? 1 : 0)}
                    >
                      <span />
                      자동 배정
                    </Checkbox>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup className="reserve-time-form">
                    <ControlLabel htmlFor="reserve02-06">입실 예정일</ControlLabel>
                    <div>
                      <SingleDatePicker
                        id="reserve02-06"
                        placeholder="입실일 선택"
                        displayFormat="YYYY-MM-DD"
                        focused={props.focused1}
                        onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                        date={moment(props.modify.check_in_exp)}
                        onDateChange={(date) => {
                          props.handleInputChange("check_in_exp", moment(date).format("YYYY-MM-DD HH:mm"));
                        }}
                        isOutsideRange={(day) => moment().add(-7, "day").isSameOrAfter(day)}
                        disabled={isDisable}
                      />
                      <div>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={Number(moment(props.modify.check_in_exp).format("HH"))}
                            step={1}
                            min={0}
                            max={23}
                            formatter={(value) => `${value}시`}
                            onChange={(value) => {
                              props.handleInputChange("check_in_exp", moment(props.modify.check_in_exp).hour(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={isDisable}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={Number(moment(props.modify.check_in_exp).format("mm"))}
                            step={10}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(value) => {
                              props.handleInputChange("check_in_exp", moment(props.modify.check_in_exp).minute(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={isDisable}
                          />
                        </span>
                      </div>
                    </div>
                  </FormGroup>
                  <FormGroup className="reserve-time-form">
                    <ControlLabel htmlFor="reserve02-07">퇴실 예정일</ControlLabel>
                    <div>
                      <SingleDatePicker
                        id="reserve02-07"
                        placeholder="퇴실일 선택"
                        displayFormat="YYYY-MM-DD HH:mm"
                        focused={props.focused2}
                        onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                        date={props.modify.check_out_exp ? moment(props.modify.check_out_exp) : moment()}
                        onDateChange={(date) => {
                          props.handleInputChange("check_out_exp", moment(date).format("YYYY-MM-DD HH:mm"));
                        }}
                        isOutsideRange={(day) => moment(props.modify.check_in_exp).isAfter(day)}
                        disabled={isDisable || props.modify.stay_type === 2}
                      />
                      <div>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={Number(moment(props.modify.check_out_exp).format("HH"))}
                            step={1}
                            min={0}
                            max={23}
                            formatter={(value) => `${value}시`}
                            onChange={(value) => {
                              props.handleInputChange("check_out_exp", moment(props.modify.check_out_exp).hour(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={isDisable}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={Number(moment(props.modify.check_out_exp).format("mm"))}
                            step={10}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(value) => {
                              props.handleInputChange("check_out_exp", moment(props.modify.check_out_exp).minute(value).format("YYYY-MM-DD HH:mm"));
                            }}
                            disabled={isDisable}
                          />
                        </span>
                      </div>
                    </div>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-08">휴대폰 번호</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-08"
                      placeholder="휴대전화번호"
                      value={format.toPhone(props.modify.hp || "010")}
                      onChange={(evt) => props.handleInputChange("hp", evt.target.value.replace(/[^\d]/g, ""))}
                      maxLength={15}
                      disabled={isDisable}
                    />
                    {/* {!props.modify.id && (
                      <Button onClick={evt => props.setReservNum()} className="green-btn" disabled={isDisable}>
                        예약번호 입력
                      </Button>
                    )} */}
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-09">예약 번호</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-09"
                      placeholder="예약번호 입력"
                      value={String(props.modify.reserv_num || "").replace(/\B(?=(\d{4})+(?!\d))/g, "-")}
                      onChange={(evt) => props.handleInputChange("reserv_num", evt.target.value.replace(/[^\d]/g, ""))}
                      maxLength={20}
                      disabled={isDisable || props.modify.id}
                    />
                    {!props.modify.id && (
                      <Button onClick={(evt) => props.getReservNum()} className="blue-btn" disabled={isDisable}>
                        번호 자동생성
                      </Button>
                    )}
                    {format.isPhone(props.modify.hp || "") && props.modify.room_type_id && props.modify.reserv_num ? (
                      <FormGroup>
                        <Button onClick={(evt) => props.sendReservNum()} className="puple-btn">
                          SMS 발송
                        </Button>
                        <Button onClick={(evt) => props.sendQRReservNum()} className="puple-btn">
                          QR코드 발송
                        </Button>
                      </FormGroup>
                    ) : null}
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-10">고객명</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-10"
                      placeholder="고객명"
                      value={props.modify.name || ""}
                      onChange={(evt) => props.handleInputChange("name", evt.target.value)}
                      disabled={isDisable}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-11">방문 형태</ControlLabel>
                    <FormControl
                      id="reserve02-11"
                      componentClass="select"
                      placeholder="방문형태"
                      value={props.modify.visit_type || ""}
                      onChange={(evt) => props.handleInputChange("visit_type", evt.target.value)}
                      disabled={isDisable}
                    >
                      <option value={1}>도보방문</option>
                      <option value={2}>차량방문</option>
                      <option value={3}>대중교통</option>
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-12">판매가</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-12"
                      placeholder="정상 판매요금"
                      value={format.toMoney(props.modify.room_fee)}
                      onChange={(evt) => props.handleInputChange("room_fee", format.toNumber(evt.target.value))}
                      disabled={isDisable}
                    />
                    {props.modify.add_fee > 0 && (
                      <span>
                        <span id="reserve02-19">기본: {format.toMoney(props.modify.default_fee)}</span> <span id="reserve02-191">추가: {format.toMoney(props.modify.add_fee)}</span>
                      </span>
                    )}
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-13">예약가</ControlLabel>
                    <span className="numeric-wrap">
                      <InputNumber
                        className="form-control"
                        id="reserve02-13"
                        value={props.modify.reserv_fee}
                        step={props.preferences.stay_fee_unit}
                        min={0}
                        formatter={(value) => format.toMoney(value)}
                        onChange={(value) => props.handleInputChange("reserv_fee", format.toNumber(value))}
                        disabled={isDisable}
                      />
                    </span>
                    {props.modify.room_fee - props.modify.reserv_fee > 0 && <span id="reserve02-20">할인: {format.toMoney(props.modify.room_fee - props.modify.reserv_fee)}</span>}
                  </FormGroup>
                </div>
                {props.modify.ota_code < 9 && (
                  <div>
                    <FormGroup>
                      <ControlLabel htmlFor="reserve02-141">OTA 결제</ControlLabel>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="reserve02-141"
                          value={props.modify.prepay_ota_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_ota_amt", value)}
                          disabled={isDisable || !props.modify.prepay_ota_amt}
                        />
                      </span>
                      <Button
                        onClick={(evt) => props.handlePayCheck("prepay_ota_amt")}
                        data-for="roomReserv"
                        data-type="info"
                        data-effect="solid"
                        data-tip="클릭 시 카드 결제 잔액이 자동 입력 됩니다."
                      >
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </FormGroup>
                    <FormGroup></FormGroup>
                  </div>
                )}
                {props.modify.ota_code === 9 && (
                  <div>
                    <FormGroup>
                      <ControlLabel htmlFor="reserve02-14">현금 결제</ControlLabel>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="reserve02-14"
                          value={props.modify.prepay_cash_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          max={props.modify.reserv_fee - props.modify.prepay_card_amt}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_cash_amt", value)}
                          disabled={isDisable || !props.modify.prepay_cash_amt}
                        />
                      </span>
                      <Button
                        onClick={(evt) => props.handlePayCheck("prepay_cash_amt")}
                        data-for="roomReserv"
                        data-type="info"
                        data-effect="solid"
                        data-tip="클릭 시 카드 결제 잔액이 자동 입력 됩니다."
                      >
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel htmlFor="reserve02-15">카드 결제</ControlLabel>
                      <span className="numeric-wrap">
                        <InputNumber
                          className="form-control"
                          id="reserve02-15"
                          value={props.modify.prepay_card_amt || 0}
                          step={props.preferences.stay_fee_unit}
                          min={0}
                          max={props.modify.reserv_fee - props.modify.prepay_cash_amt}
                          formatter={(value) => format.toMoney(value)}
                          onChange={(value) => props.handleInputChange("prepay_card_amt", value)}
                          disabled={isDisable || !props.modify.prepay_card_amt}
                        />
                      </span>
                      <Button
                        onClick={(evt) => props.handlePayCheck("prepay_card_amt")}
                        data-for="roomReserv"
                        data-type="info"
                        data-effect="solid"
                        data-tip="클릭 시 현금 결제 잔액이 자동 입력 됩니다."
                      >
                        잔액 <span className="mdi mdi-plus-circle-outline" />
                      </Button>
                    </FormGroup>
                  </div>
                )}
                <div>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-16">결제 잔금</ControlLabel>
                    <FormControl
                      type="text"
                      id="reserve02-16"
                      placeholder="결제 잔금"
                      value={`${props.modify.reserv_fee - (props.modify.ota_code < 9 ? props.modify.prepay_ota_amt : props.modify.prepay_cash_amt + props.modify.prepay_card_amt)}원`.replace(
                        /\B(?=(\d{3})+(?!\d))/g,
                        ","
                      )}
                      readOnly={true}
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel htmlFor="reserve02-17">고객 메모</ControlLabel>
                    <FormControl
                      componentClass="textarea"
                      rows="2"
                      id="reserve02-17"
                      placeholder="메모"
                      value={props.modify.memo || ""}
                      onChange={(evt) => props.handleInputChange("memo", evt.target.value)}
                      disabled={isDisable}
                    />
                  </FormGroup>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          {!props.modify.check_out && <Button onClick={(evt) => props.onSave()}>{props.reserv.id ? "예약 수정" : "예약 등록"}</Button>}
          {props.checkAuth() && props.reserv.id && props.reserv.state === "A" && (
            <Button onClick={(evt) => props.onCancel()} className="btn-set-warning">
              예약 취소
            </Button>
          )}
          <Button onClick={(evt) => props.onList()} className="btn-set-cancel">
            예약 목록
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab02;
