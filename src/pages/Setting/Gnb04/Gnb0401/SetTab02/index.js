// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as roomReservAction } from "actions/roomReserv";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomFeeAction } from "actions/roomFee";
import { actionCreators as smsAction } from "actions/sms";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, room, roomType, roomReserv, preferences, subscribe, placeSubscribe, roomFee } = state;

  return {
    place,
    auth,
    room,
    roomType,
    roomFee,
    roomReserv,
    preferences: preferences.item,
    reserv: ownProps.item,
    subscribe,
    placeSubscribe,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomEnableReservs: (placeId) => dispatch(roomAction.getRoomEnableReservs(placeId)),
    initAllRoomReservs: (palceId) => dispatch(roomReservAction.getNowRoomReservs(palceId)),
    handleNewRoomReserv: (room_reserv) => dispatch(roomReservAction.newRoomReserv(room_reserv)),
    handleSaveRoomReserv: (id, room_reserv) => dispatch(roomReservAction.putRoomReserv(id, room_reserv)),
    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),
    handleSmsSendMsg: (sms) => dispatch(smsAction.newSms(sms)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
