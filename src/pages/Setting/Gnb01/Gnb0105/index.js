// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { room } = state;
  return {
    room,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
