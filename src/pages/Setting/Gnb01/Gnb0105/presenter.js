// imports.
import React from "react";
import Modal from "react-awesome-modal";

//room-popup
import { Form, ControlLabel, FormControl, Table, ButtonToolbar, Button } from "react-bootstrap";

import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

// functional component
const Gnb0105 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp">
        <article className="setting-wrapper">
          <h1>운영정보 조회</h1>
          <div className="setting-form-wrapper" id="gnb0105">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <h2>검색조건</h2>
                    <div className="form-content">
                      <div className="date-set">
                        <SingleDatePicker
                          id="startsOnDateInput-1"
                          placeholder="검색 시작일 선택"
                          displayFormat="YYYY/MM/DD/"
                          date={props.date}
                          onDateChange={(date) => props.onDateChange({ date })}
                          focused={props.focused}
                          onFocusChange={({ focused }) => props.onFocusChange({ focused })}
                        />
                        <span> ~ </span>
                        <SingleDatePicker
                          id="endsOnDateInput-2"
                          placeholder="검색 마침일 선택"
                          displayFormat="YYYY/MM/DD/"
                          date={props.date2}
                          onDateChange={(date2) => props.onDateChange2({ date2 })}
                          focused={props.focused2}
                          onFocusChange={({ focused: focused2 }) => props.onFocusChange2({ focused2 })}
                        />
                      </div>
                      <ControlLabel htmlFor="gnb0105-01">객실명</ControlLabel>
                      <FormControl componentClass="select" placeholder="전체" id="gnb0105-01">
                        <option value="전체">전체</option>
                        <option value="other">...</option>
                      </FormControl>
                    </div>
                    <div className="search-result">
                      <ButtonToolbar className="btn-room">
                        <Button>추가</Button>
                        <Button>삭제</Button>
                        <Button>조회</Button>
                      </ButtonToolbar>
                      <div className="table-wrapper">
                        <Table>
                          <thead>
                            <tr>
                              <th>객실명</th>
                              <th>일시</th>
                              <th>내용</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>203호</td>
                              <td>1시간</td>
                              <td>00:00:00</td>
                            </tr>
                            <tr>
                              <td>203호</td>
                              <td>1시간</td>
                              <td>00:00:00</td>
                            </tr>
                            <tr>
                              <td>203호</td>
                              <td>1시간</td>
                              <td>00:00:00</td>
                            </tr>
                            <tr>
                              <td>203호</td>
                              <td>1시간</td>
                              <td>00:00:00</td>
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </li>
                  <li>
                    <FormControl componentClass="select" placeholder="전체">
                      <option value="전체">전체</option>
                      <option value="other">...</option>
                    </FormControl>
                    <ButtonToolbar className="btn-room">
                      <Button>메모저장</Button>
                    </ButtonToolbar>
                    <FormControl componentClass="textarea" placeholder="textarea" />
                  </li>
                </ul>
              </div>
              <ButtonToolbar className="btn-room">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0105;
