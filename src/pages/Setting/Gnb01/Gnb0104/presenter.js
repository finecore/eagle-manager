// imports.
import React from "react";
import Modal from "react-awesome-modal";

//room-popup
import { Tabs, Tab, Form, ControlLabel, FormControl, Table, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

// functional component
const Gnb0104 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper">
          <h1>RCU이력조회</h1>
          <Tabs defaultActiveKey={1} id="rcu-layer-tabs">
            <Tab eventKey={1} title="RCU오류 이력">
              <div className="setting-form-wrapper" id="gnb010401">
                <Form className="setting-form">
                  <div className="search-wrapper">
                    <ul className="list-bul">
                      <li>
                        <h2>검색조건</h2>
                        <div className="form-content">
                          <div className="date-set">
                            <SingleDatePicker
                              id="startsOnDateInput-1"
                              placeholder="검색 시작일 선택"
                              displayFormat="YYYY/MM/DD/"
                              date={props.date}
                              onDateChange={(date) => props.onDateChange({ date })}
                              focused={props.focused}
                              onFocusChange={({ focused }) => props.onFocusChange({ focused })}
                            />
                            <span className="numeric-wrap">
                              <InputNumber className="form-control" defaultValue={0} step={1} min={0} max={24} formatter={(value) => `${value}시`} />
                            </span>
                            <span> ~ </span>
                            <SingleDatePicker
                              id="endsOnDateInput-2"
                              placeholder="검색 마침일 선택"
                              displayFormat="YYYY/MM/DD/"
                              date={props.date2}
                              onDateChange={(date2) => props.onDateChange2({ date2 })}
                              focused={props.focused2}
                              onFocusChange={({ focused: focused2 }) => props.onFocusChange2({ focused2 })}
                            />
                            <span className="numeric-wrap">
                              <InputNumber className="form-control" defaultValue={0} step={1} min={0} max={24} formatter={(value) => `${value}시`} />
                            </span>
                          </div>
                          <ControlLabel htmlFor="gnb0103-01">객실명</ControlLabel>
                          <FormControl componentClass="select" placeholder="전체" id="gnb0103-01">
                            <option value="전체">전체</option>
                            <option value="other">...</option>
                          </FormControl>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div className="search-result">
                    <div className="table-wrapper">
                      <Table>
                        <thead>
                          <tr>
                            <th>객실명</th>
                            <th>청소시간</th>
                            <th>시작</th>
                            <th>종료</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                            <td>00:00:00</td>
                            <td>00:00:00</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                            <td>00:00:00</td>
                            <td>00:00:00</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                            <td>00:00:00</td>
                            <td>00:00:00</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                            <td>00:00:00</td>
                            <td>00:00:00</td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </div>
                  <ButtonToolbar className="btn-room">
                    <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                      닫기
                    </Button>
                  </ButtonToolbar>
                </Form>
              </div>
            </Tab>
            <Tab eventKey={2} title="객실별 오류시간">
              <div className="setting-form-wrapper" id="gnb010402">
                <Form className="setting-form">
                  <div className="search-result">
                    <ButtonToolbar className="btn-room">
                      <Button>RCU오류시간 초기화</Button>
                    </ButtonToolbar>
                    <div className="table-wrapper">
                      <Table>
                        <thead>
                          <tr>
                            <th>객실명</th>
                            <th>RCU 오류시간</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                          </tr>
                          <tr>
                            <td>203호</td>
                            <td>1시간</td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </div>
                  <ButtonToolbar className="btn-room">
                    <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                      닫기
                    </Button>
                  </ButtonToolbar>
                </Form>
              </div>
            </Tab>
          </Tabs>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0104;
