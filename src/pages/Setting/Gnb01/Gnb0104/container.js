// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Gnb0104 from "./presenter";

class Container extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
  };
  state = {};

  closeModal = (evt) => {
    this.props.handleDisplayLayer(false);
  };

  onDateChange = ({ date }) => {
    this.setState({ date });
  };

  onFocusChange = ({ focused }) => {
    this.setState({ focused });
  };

  onDateChange2 = ({ date2 }) => {
    this.setState({ date2 });
  };

  onFocusChange2 = ({ focused2 }) => {
    this.setState({ focused2 });
  };

  render() {
    return (
      <Gnb0104
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        date={this.state.date}
        onDateChange={this.onDateChange}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        date2={this.state.date2}
        onDateChange2={this.onDateChange2}
        focused2={this.state.focused2}
        onFocusChange2={this.onFocusChange2}
      />
    );
  }
}

export default withSnackbar(Container);
