// imports.
import React from "react";
import Modal from "react-awesome-modal";
import moment from "moment";
import cx from "classnames";
import _ from "lodash";
import { keyToValue } from "constants/key-map";
import format from "utils/format-util";

//room-popup
import { Form, ControlLabel, FormGroup, Radio, Table, FormControl, Checkbox, ButtonToolbar, Button } from "react-bootstrap";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

// functional component
const Gnb0101 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper">
          <h1>기간별 매출집계</h1>
          <div className="setting-form-wrapper" id="gnb0101">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <h2>
                      검색기간
                      <div className="time-mode">
                        <Checkbox inline checked={props.timeMode} onChange={(evt) => props.handleTimeMode()}>
                          <span />
                          정산시간 기준
                        </Checkbox>
                      </div>
                    </h2>
                    <div className="form-content">
                      <Radio inline name="search_term" value={props.mode === 1} onChange={(evt) => props.handleTerm(-1, "day")}>
                        <span />
                        어제
                      </Radio>
                      <Radio inline name="search_term" value={props.mode === 2} defaultChecked={true} onChange={(evt) => props.handleTerm(0, "day")}>
                        <span />
                        오늘
                      </Radio>
                      <Radio inline name="search_term" value={props.mode === 3} onChange={(evt) => props.handleTerm(-1, "week")}>
                        <span />
                        이번주
                      </Radio>
                      <Radio inline name="search_term" value={props.mode === 4} onChange={(evt) => props.handleTerm(-1, "month")}>
                        <span />
                        이번달
                      </Radio>
                      <span className="notice"> ( 매출 데이터 보관 일수는 {props.preferences.data_keep_day_sale}일 입니다 )</span>
                      <div className="date-set">
                        <SingleDatePicker
                          id="startsOnDateInput-1"
                          placeholder="검색 시작일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.begin.date}
                          isOutsideRange={(day) => moment().diff(day) < 365}
                          onDateChange={(date) => props.onDataChange({ begin: { date } })}
                          focused={props.focused1}
                          onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                          disabled={false}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.hour}
                            step={1}
                            min={0}
                            max={23}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.onDataChange({ begin: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.min}
                            step={1}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.onDataChange({ begin: { min } })}
                          />
                        </span>
                        <span> ~ </span>
                        <SingleDatePicker
                          id="endsOnDateInput-2"
                          placeholder="검색 종료일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.end.date}
                          isOutsideRange={(day) => moment(day).diff(props.begin.date) < 0 || moment().diff(day) < 365}
                          onDateChange={(date) => props.onDataChange({ end: { date } })}
                          focused={props.focused2}
                          onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.hour}
                            step={1}
                            min={0}
                            max={24}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.onDataChange({ end: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.min}
                            step={1}
                            min={0}
                            max={props.end.hour < 24 ? 59 : 0}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.onDataChange({ end: { min } })}
                          />
                        </span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <h2>보기옵션</h2>
                    <div className="form-content search-select">
                      <Radio inline name="search_type" value={2} checked={props.searchType === 2} onChange={(evt) => props.handleSearchType(2)}>
                        <span />
                        결제 기준 (결제별 정산)
                      </Radio>
                      <Radio inline name="search_type" value={1} checked={props.searchType === 1} onChange={(evt) => props.handleSearchType(1)}>
                        <span />
                        숙박 기준 (입실별 정산)
                      </Radio>
                    </div>
                  </li>
                  <li>
                    <h2>필터옵션</h2>
                    <div className="form-content search-select">
                      <ControlLabel>객실 타입</ControlLabel>
                      <FormControl
                        componentClass="select"
                        placeholder="객실타입"
                        onChange={(evt) => props.handleOption({ option: { room_type_id: evt.target.value } })}
                        value={props.option.room_type_id}
                      >
                        <option value="">전체타입</option>
                        {_.map(props.roomType.list, (room_type, k) => (
                          <option key={k} value={room_type.id}>
                            {room_type.name}
                          </option>
                        ))}
                      </FormControl>
                      <span className="filter-dim">|</span>
                      <ControlLabel>객실명</ControlLabel>
                      <FormControl componentClass="select" placeholder="전체" onChange={(evt) => props.handleOption({ option: { room_id: evt.target.value } })} value={props.option.room_id}>
                        <option value="">전체객실</option>
                        {_.filter(props.room.list, (room) => !props.option.room_type_id || Number(props.option.room_type_id) === Number(room.room_type_id)).map((room, k) => (
                          <option key={k} value={room.id}>
                            {room.name}
                          </option>
                        ))}
                      </FormControl>
                      {props.device.isc.length ? (
                        <span>
                          <span className="filter-dim">|</span>
                          <ControlLabel>무인 판매기</ControlLabel>
                          <FormControl componentClass="select" placeholder="전체" onChange={(evt) => props.handleOption({ option: { serialno: evt.target.value } })} value={props.option.serialno}>
                            <option value="">전체무인</option>
                            {props.device.isc.map((item, k) => (
                              <option key={k} value={item.serialno}>
                                {item.name}
                              </option>
                            ))}
                          </FormControl>
                        </span>
                      ) : null}
                    </div>
                    <div className="form-content search-select">
                      <Checkbox inline checked={props.option.all || false} onChange={(evt) => props.handleOption({ option: { all: evt.target.checked } })}>
                        <span />
                        전체
                      </Checkbox>
                      <div className="separator" />
                      <Checkbox inline checked={props.option.rent} onChange={(evt) => props.handleOption({ option: { rent: evt.target.checked } })}>
                        <span />
                        대실
                      </Checkbox>
                      <Checkbox inline checked={props.option.stay} onChange={(evt) => props.handleOption({ option: { stay: evt.target.checked } })}>
                        <span />
                        숙박
                      </Checkbox>
                      <Checkbox inline checked={props.option.long} onChange={(evt) => props.handleOption({ option: { long: evt.target.checked } })}>
                        <span />
                        장기
                      </Checkbox>
                      <Checkbox inline checked={props.option.cash} onChange={(evt) => props.handleOption({ option: { cash: evt.target.checked } })}>
                        <span />
                        현금결제
                      </Checkbox>
                      <Checkbox inline checked={props.option.card} onChange={(evt) => props.handleOption({ option: { card: evt.target.checked } })}>
                        <span />
                        카드결제
                      </Checkbox>{" "}
                      <Checkbox inline checked={props.option.ota} onChange={(evt) => props.handleOption({ option: { ota: evt.target.checked } })}>
                        <span />
                        OTA결제
                      </Checkbox>
                      <Checkbox inline checked={props.option.point} onChange={(evt) => props.handleOption({ option: { point: evt.target.checked } })}>
                        <span />
                        포인트결제
                      </Checkbox>
                      <Checkbox inline checked={props.option.reserv} onChange={(evt) => props.handleOption({ option: { reserv: evt.target.checked } })}>
                        <span />
                        예약
                      </Checkbox>
                      <Checkbox inline checked={props.option.web} onChange={(evt) => props.handleOption({ option: { web: evt.target.checked } })}>
                        <span />
                        유인판매
                      </Checkbox>
                      <Checkbox inline checked={props.option.isc} onChange={(evt) => props.handleOption({ option: { isc: evt.target.checked } })}>
                        <span />
                        무인판매
                      </Checkbox>
                      <Checkbox inline checked={props.option.api} onChange={(evt) => props.handleOption({ option: { api: evt.target.checked } })}>
                        <span />
                        자동입실
                      </Checkbox>
                      <Checkbox inline checked={props.option.move} onChange={(evt) => props.handleOption({ option: { move: evt.target.checked } })}>
                        <span />
                        객실이동
                      </Checkbox>
                    </div>
                  </li>
                  <li>
                    <ButtonToolbar className="btn-room">
                      <Button onClick={(evt) => props.handleSerach(evt)}>검색조회</Button>
                    </ButtonToolbar>
                  </li>
                </ul>
                <ul className="list-bul">
                  <li>
                    <h2>{props.searchType === 1 ? "숙박 현황" : "결제 현황"}</h2>
                    <div className="form-content count-table">
                      <Table id="search-sales-sum">
                        <thead>
                          <tr>
                            <th />
                            <th>{props.searchType === 1 ? "숙박" : "매출"}건수</th>
                            <th>현금</th>
                            <th>카드</th>
                            <th>OTA</th>
                            <th>포인트</th>
                            <th>미수금</th>
                            <th>반환금</th>
                            <th>합계금액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th
                              style={{
                                color: props.preferences.stay_type_color[2],
                              }}
                            >
                              대실
                            </th>
                            <td>
                              {String(props.searchType === 1 ? props.sum.rent.count : props.sum.rent.pay_count).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              {props.searchType === 2 && " (" + String(props.sum.rent.count).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")"}
                            </td>
                            <td>{String(props.sum.rent.pay_cash_amt + props.sum.rent.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.rent.pay_card_amt + props.sum.rent.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.rent.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.rent.pay_point_amt + props.sum.rent.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.rent.receptAmt && "receptAmt")}>{String(props.sum.rent.receptAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.rent.returnAmt && "returnAmt")}>{String(props.sum.rent.returnAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.rent.default_fee + props.sum.rent.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                          </tr>
                          <tr>
                            <th
                              style={{
                                color: props.preferences.stay_type_color[1],
                              }}
                            >
                              숙박
                            </th>
                            <td>
                              {String(props.searchType === 1 ? props.sum.stay.count : props.sum.stay.pay_count).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              {props.searchType === 2 && " (" + String(props.sum.stay.count).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")"}
                            </td>
                            <td>{String(props.sum.stay.pay_cash_amt + props.sum.stay.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.stay.pay_card_amt + props.sum.stay.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.stay.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.stay.pay_point_amt + props.sum.stay.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.stay.receptAmt && "receptAmt")}>{String(props.sum.stay.receptAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.stay.returnAmt && "returnAmt")}>{String(props.sum.stay.returnAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.stay.default_fee + props.sum.stay.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                          </tr>
                          <tr>
                            <th
                              style={{
                                color: props.preferences.stay_type_color[3],
                              }}
                            >
                              장기
                            </th>
                            <td>
                              {String(props.searchType === 1 ? props.sum.long.count : props.sum.long.pay_count).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              {props.searchType === 2 && " (" + String(props.sum.long.count).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")"}
                            </td>
                            <td>{String(props.sum.long.pay_cash_amt + props.sum.long.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.long.pay_card_amt + props.sum.long.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.long.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.long.pay_point_amt + props.sum.long.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.long.receptAmt && "receptAmt")}>{String(props.sum.long.receptAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.sum.long.returnAmt && "returnAmt")}>{String(props.sum.long.returnAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>{String(props.sum.long.default_fee + props.sum.long.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>합계</th>
                            <td>
                              {String(
                                props.searchType === 1
                                  ? props.sum.rent.count + props.sum.stay.count + props.sum.long.count
                                  : props.sum.rent.pay_count + props.sum.stay.pay_count + props.sum.long.pay_count
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              {props.searchType === 2 && " (" + String(props.sum.rent.count + props.sum.stay.count + props.sum.long.count).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")"}
                            </td>
                            <td>
                              {String(
                                props.sum.rent.pay_cash_amt +
                                  props.sum.rent.prepay_cash_amt +
                                  props.sum.stay.pay_cash_amt +
                                  props.sum.stay.prepay_cash_amt +
                                  props.sum.long.pay_cash_amt +
                                  props.sum.long.prepay_cash_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </td>
                            <td>
                              {String(
                                props.sum.rent.pay_card_amt +
                                  props.sum.rent.prepay_card_amt +
                                  props.sum.stay.pay_card_amt +
                                  props.sum.stay.prepay_card_amt +
                                  props.sum.long.pay_card_amt +
                                  props.sum.long.prepay_card_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </td>
                            <td>{String(props.sum.rent.prepay_ota_amt + props.sum.stay.prepay_ota_amt + props.sum.long.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>
                              {String(
                                props.sum.rent.pay_point_amt +
                                  props.sum.rent.prepay_point_amt +
                                  props.sum.stay.pay_point_amt +
                                  props.sum.stay.prepay_point_amt +
                                  props.sum.long.pay_point_amt +
                                  props.sum.long.prepay_point_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </td>
                            <td className={cx(props.receptAmtTot && "receptAmt")}>{String(props.receptAmtTot).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td className={cx(props.returnAmtTot && "returnAmt")}>{String(props.returnAmtTot).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>
                              {String(
                                props.sum.rent.default_fee + props.sum.rent.add_fee + props.sum.stay.default_fee + props.sum.stay.add_fee + props.sum.long.default_fee + props.sum.long.add_fee
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </td>
                          </tr>
                        </tfoot>
                      </Table>
                      <div>
                        <FormGroup controlId="formInlineName">
                          <ControlLabel>총매출 </ControlLabel> <span> {String(props.total).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                        </FormGroup>
                        <FormGroup controlId="formInlineName">
                          <ControlLabel>미수금 </ControlLabel> <span>{String(props.receptAmtTot).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                        </FormGroup>
                        <FormGroup controlId="formInlineName">
                          <ControlLabel>반환금 </ControlLabel> <span>{String(props.returnAmtTot).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                        </FormGroup>
                      </div>
                    </div>
                    <span className="sum-notice">
                      {props.searchType === 1 ? "※ 대실/숙박 변경 시 결제 금액은 변경된 투숙형태에 합산 됩니다." : "※ 결제 금액은 결제 당시의 투숙형태에 합산 됩니다."}
                    </span>
                  </li>
                </ul>
              </div>
              <div className="search-result">
                <span className="search-count">Tot. {String(props.list.length).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                <ButtonToolbar className="btn-room">
                  <Button onClick={(evt) => props.excelDown()}>엑셀저장</Button>
                </ButtonToolbar>
                <div className="table-wrapper">
                  <Table id="search-sales-list">
                    <thead>
                      <tr>
                        <th>객실명</th>
                        <th>투숙형태</th>
                        <th>상태</th>
                        <th>{props.searchType === 1 ? "입실/퇴실(예정)일시" : "등록일시"}</th>
                        <th>{props.searchType === 1 ? "사용시간" : "경과시간"}</th>
                        <th>{props.searchType === 1 ? "예약" : "타입"}</th>
                        <th>판매구분</th>
                        <th>기본요금</th>
                        <th>추가/할인</th>
                        <th>판매요금</th>
                        <th>
                          현금결제
                          <br />
                          예약결제
                        </th>
                        <th>
                          카드결제
                          <br />
                          예약결제
                        </th>
                        <th>
                          포인트결제
                          <br />
                          예약결제
                        </th>
                        <th>OTA결제</th>
                        <th>
                          전화번호
                          <br />
                          카드승인번호
                        </th>
                        <th>
                          {props.searchType === 1 ? (
                            <span>
                              미수금
                              <br />
                              반환금
                            </span>
                          ) : (
                            <span>결제 금액</span>
                          )}
                        </th>
                        <th>
                          메모
                          <br />
                          차량번호
                        </th>
                        <th>근무자</th>
                        <th>비고/사유</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <td colSpan="15" />
                      </tr>
                    </tfoot>
                    <tbody>
                      {props.list.map((sale, k) => (
                        <tr key={k} className={cx(props.searchType === 1 || sale.first_pay ? "" : "first_pay")}>
                          <td>{sale.room_name}</td>
                          <td style={{ color: props.preferences.stay_type_color[sale.stay_type] }}>
                            {sale.prev_stay_type && sale.stay_type !== sale.prev_stay_type ? (
                              <div>
                                <span
                                  style={{
                                    color: props.preferences.stay_type_color[sale.prev_stay_type],
                                  }}
                                >
                                  {keyToValue("room_sale", "stay_type", sale.prev_stay_type)}
                                </span>
                                {" -> "}
                                <span
                                  style={{
                                    color: props.preferences.stay_type_color[sale.stay_type],
                                  }}
                                >
                                  {keyToValue("room_sale", "stay_type", sale.stay_type)}
                                </span>
                              </div>
                            ) : (
                              keyToValue("room_sale", "stay_type", sale.stay_type)
                            )}
                          </td>
                          <td
                            style={{
                              color:
                                sale.rollback === 1
                                  ? "rgb(255, 87, 112)"
                                  : sale.rollback === 2
                                  ? "rgb(12, 129, 55)"
                                  : sale.state === "B"
                                  ? "rgb(255, 87, 112)"
                                  : sale.state === "C"
                                  ? "rgb(43, 94, 214)"
                                  : "#000",
                            }}
                          >
                            {props.searchType === 1
                              ? sale.state === "A"
                                ? "사용중"
                                : sale.state === "B"
                                ? "입실취소"
                                : "퇴실"
                              : sale.rollback === 1
                              ? "입실취소"
                              : sale.rollback === 2
                              ? "퇴실취소"
                              : sale.state === "A"
                              ? sale.first_pay
                                ? "입실"
                                : "-"
                              : sale.state === "B"
                              ? "입실취소"
                              : "퇴실"}
                          </td>
                          {props.searchType === 1 ? (
                            <td>
                              {moment(sale.check_in).format("YYYY-MM-DD HH:mm:ss")}
                              <br />
                              <span style={{ color: sale.state === "B" ? "rgb(255, 87, 112)" : sale.check_out ? "rgb(27, 39, 255)" : "#999" }}>
                                {moment(sale.state === "B" ? sale.mod_date : sale.check_out || sale.check_out_exp).format("YYYY-MM-DD HH:mm:ss")}
                              </span>
                            </td>
                          ) : (
                            <td>
                              <span
                                style={{
                                  color: sale.rollback === 1 ? "rgb(255, 87, 112)" : sale.rollback === 2 ? "rgb(12, 129, 55)" : "",
                                }}
                              >
                                {moment(sale.reg_date).format("YYYY-MM-DD HH:mm:ss")}
                              </span>
                            </td>
                          )}
                          <td
                            style={{
                              color: (props.searchType === 1 && sale.state === "A") || (props.searchType === 2 && sale.s_state === "A" && sale.first_pay) ? "rgb(255, 87, 112)" : "",
                            }}
                          >
                            {Number(sale.useTime.hh) + " : " + sale.useTime.mm + " : " + sale.useTime.ss}
                          </td>
                          <td
                            style={{
                              color: sale.move_from ? "#ff0000" : sale.prev_stay_type ? "#1100ff" : "",
                            }}
                          >
                            {props.searchType === 2 ? (
                              sale.first_pay ? (
                                sale.room_reserv_id ? (
                                  "예약 체크인"
                                ) : (
                                  "체크인"
                                )
                              ) : sale.move_from ? (
                                "객실 이동"
                              ) : sale.prev_stay_type ? (
                                <div>
                                  <span
                                    style={{
                                      color: props.preferences.stay_type_color[sale.prev_stay_type],
                                    }}
                                  >
                                    {keyToValue("room_sale", "stay_type", sale.prev_stay_type)}
                                  </span>
                                  {" -> "}
                                  <span
                                    style={{
                                      color: props.preferences.stay_type_color[sale.stay_type],
                                    }}
                                  >
                                    {keyToValue("room_sale", "stay_type", sale.stay_type)}
                                  </span>
                                </div>
                              ) : sale.pay_cash_amt + sale.pay_card_amt + sale.pay_point_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.prepay_card_amt + sale.prepay_point_amt > 0 ? (
                                "추가 결제"
                              ) : sale.default_fee + sale.add_fee > 0 ? (
                                "요금 변경"
                              ) : (
                                "-"
                              )
                            ) : sale.room_reserv_id ? (
                              "예약"
                            ) : (
                              ""
                            )}
                            {sale.move_from ? (
                              <div>
                                <span style={{ color: "#000" }}>From {sale.move_room.name}</span>
                              </div>
                            ) : null}
                          </td>
                          <td>
                            {keyToValue("room_sale", "channel", sale.channel)}
                            <br />
                            {sale.isg_name ? sale.isg_name : null}
                          </td>
                          <td className="money">{String(sale.default_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                          <td className="money">
                            {sale.add_fee > 0 ? "+" : ""}
                            {String(props.searchType === 2 && sale.first_pay ? sale.s_add_fee : sale.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </td>
                          <td className="money">
                            {String(props.searchType === 2 && sale.first_pay ? sale.s_default_fee + sale.s_add_fee : sale.default_fee + sale.add_fee).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </td>
                          <td className="money">
                            {String(props.searchType === 2 && sale.first_pay ? sale.s_pay_cash_amt : sale.pay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <br />
                            {props.searchType === 1 || sale.first_pay
                              ? String(props.searchType === 2 && sale.first_pay ? sale.s_prepay_cash_amt : sale.prepay_cash_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                              : null}
                          </td>
                          <td className="money">
                            {String(props.searchType === 2 && sale.first_pay ? sale.s_pay_card_amt : sale.pay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <br />
                            {props.searchType === 1 || sale.first_pay
                              ? String(props.searchType === 2 && sale.first_pay ? sale.s_prepay_card_amt : sale.prepay_card_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                              : null}
                          </td>
                          <td className="money">
                            {String(props.searchType === 2 && sale.first_pay ? sale.s_pay_point_amt : sale.pay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            <br />
                            {props.searchType === 1 || sale.first_pay
                              ? String(props.searchType === 2 && sale.first_pay ? sale.s_prepay_point_amt : sale.prepay_point_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                              : null}
                          </td>
                          <td className="money">{String(props.searchType === 2 && sale.first_pay ? sale.s_prepay_ota_amt : sale.prepay_ota_amt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                          <td className="money">
                            {sale.phone ? <span className="phone-no">{format.toPhone(sale.phone)}</span> : null}
                            <br />
                            {sale.card_approval_num ? <span className="card-no">{String(sale.card_approval_num).replace(/\B(?=(\d{4})+(?!\d))/g, "-")}</span> : null}
                          </td>
                          <td className="money">
                            {props.searchType === 1 ? (
                              <span>
                                <span className={cx(sale.receptAmt && "receptAmt")}>{String(sale.receptAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                                <br />
                                <span className={cx(sale.returnAmt && "returnAmt")}>{String(sale.returnAmt).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
                              </span>
                            ) : (
                              String(
                                sale.first_pay
                                  ? sale.s_pay_cash_amt + sale.s_pay_card_amt + sale.s_pay_point_amt + sale.s_prepay_ota_amt + sale.s_prepay_cash_amt + sale.s_prepay_card_amt + sale.s_prepay_point_amt
                                  : sale.pay_cash_amt + sale.pay_card_amt + sale.pay_point_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.prepay_card_amt + sale.prepay_point_amt
                              ).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            )}
                          </td>
                          <td>
                            {props.searchType === 2 ? (sale.first_pay ? sale.s_memo : null) : sale.memo}
                            {props.searchType === 2 ? sale.first_pay ? sale.s_memo && sale.s_car_no ? <br /> : null : null : sale.memo && sale.car_no ? <br /> : null}
                            {props.searchType === 2 ? (sale.first_pay ? sale.s_car_no : null) : sale.car_no}
                          </td>
                          <td>{sale.user_name || "자동입실"}</td>
                          <td>{props.searchType === 1 ? sale.comment : sale.rollback === 1 ? sale.s_comment : ""}</td>
                        </tr>
                      ))}
                      {!props.list.length && (
                        <tr>
                          <td colSpan="14">검색 데이터가 없습니다.</td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0101;
