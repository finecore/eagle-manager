import React, { Component } from "react";
import PropTypes from "prop-types";

import moment from "moment";
import _ from "lodash";
import date from "utils/date-util";

import ExcelUtil from "utils/excel-util";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Gnb0101 from "./presenter";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomState: PropTypes.object.isRequired,
    roomSale: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      focused1: null,
      focused2: null,
      mode: 2,
      type: "day",
      timeMode: true, // 정산 시간 기준 여부.
      searchType: 2, // 조회 타입(1: 고객 기준, 2:매출 기준)
      begin: { date: moment().add(-1, "day"), hour: 0, min: 0 },
      end: {
        date: moment().add(-1, "day"),
        hour: 0,
        min: 0,
      },
      list: [],
      option: {
        all: true,
        rent: true,
        stay: true,
        long: true,
        web: true,
        isc: true,
        api: true,
        room_type_id: undefined,
        room_id: undefined,
        serialno: undefined,
        cash: true,
        card: true,
        ota: true,
        point: true,
        reserv: true,
        move: true,
      }, // 필터 적용,
      sum: {
        stay: {
          count: 0,
          default_fee: 0,
          add_fee: 0,
          pay_cash_amt: 0,
          prepay_ota_amt: 0,
          prepay_cash_amt: 0,
          pay_card_amt: 0,
          prepay_card_amt: 0,
          pay_point_amt: 0,
          prepay_point_amt: 0,
        },
        rent: {
          count: 0,
          default_fee: 0,
          add_fee: 0,
          pay_cash_amt: 0,
          prepay_ota_amt: 0,
          prepay_cash_amt: 0,
          pay_card_amt: 0,
          prepay_card_amt: 0,
          pay_point_amt: 0,
          prepay_point_amt: 0,
        },
        long: {
          count: 0,
          default_fee: 0,
          add_fee: 0,
          pay_cash_amt: 0,
          prepay_ota_amt: 0,
          prepay_cash_amt: 0,
          pay_card_amt: 0,
          prepay_card_amt: 0,
          pay_point_amt: 0,
          prepay_point_amt: 0,
        },
      },
      total: 0,
      receptAmt: 0, // 미수금
      returnAmt: 0, // 반환금
    };
  }

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  onDataChange = (data) => {
    console.log("- onDataChange", JSON.stringify(data));
    const state = _.merge({}, this.state, data);

    this.setState(state, () => {
      console.log("- state", JSON.stringify(this.state));
    });
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  handleTerm = (value, type) => {
    let { begin, end, timeMode, mode } = this.state;
    const { preferences } = this.props;

    // 시간 초기화.
    begin.hour = timeMode ? preferences.day_sale_end_time : 0;
    begin.min = 0;

    end.hour = timeMode ? preferences.day_sale_end_time : 24;
    end.min = 0;

    let start = moment({ hour: preferences.day_sale_end_time, minute: 0 });

    // 오늘 정산 시간 지났는지 여부.
    let isTodaySaleEnd = false;

    if (timeMode && start > moment()) {
      // 매출 마감 시간이 남았다면.
      isTodaySaleEnd = true;
    }

    if (type === "day" && value === -1) {
      begin.date = moment().add(isTodaySaleEnd ? -2 : -1, type);
      end.date = moment().add(isTodaySaleEnd ? -1 : 0, type);
      mode = 1;
    } else if (type === "week" && value === -1) {
      begin.date = moment().startOf("week");
      end.date = moment().endOf("week");
      mode = 2;
    } else if (type === "month" && value === -1) {
      begin.date = moment().startOf("month");
      end.date = moment().endOf("month");
      mode = 3;
    } else if (type === "year" && value === -1) {
      begin.date = moment().startOf("year");
      end.date = moment().endOf("year");
      mode = 4;
    } else {
      // 오늘 (정산 시간 지났으면 오늘 ~ 내일 정산 일자로 설정)
      begin.date = moment().add(isTodaySaleEnd ? -1 : 0, type);
      end.date = moment().add(isTodaySaleEnd ? 0 : 1, type);
      mode = 0;
    }

    console.log("- handleTerm", value, type, begin.date.format("YYYY-MM-DD HH:mm"), end.date.format("YYYY-MM-DD HH:mm:ss"));

    this.setState(
      {
        begin,
        end,
        mode,
        type,
      },
      () => {
        this.handleSerach();
      }
    );
  };

  handleOption = ({ option }) => {
    const { roomSale, roomSalePay } = this.props;

    console.log("- handleOption option ", option);

    let opt = _.merge({}, this.state.option, option);

    // 전체 선택/해제.
    if (option.all !== undefined) {
      _.map(this.state.option, (v, k) => {
        if (k !== "room_type_id" && k !== "room_id" && k !== "serialno") opt[k] = option.all;
      });
    } else {
      // 전체 옵션 선택 시 전체 체크.
      if (!option.all) {
        let isAll = true;
        _.map(opt, (v, k) => {
          // console.log("- k,v", k, v);
          if (k !== "room_type_id" && k !== "room_id" && k !== "serialno" && k !== "all" && !v) {
            isAll = false;
            return false;
          }
        });

        opt.all = isAll;
      }
    }

    console.log("- option", opt);

    this.setState({ option: opt }, () => {
      this.filter(this.state.searchType === 1 ? roomSale.search : roomSalePay.search);
    });
  };

  // 목록 필터링.
  filter = (search) => {
    const {
      searchType,
      option: { rent, stay, long, web, isc, api, room_type_id, room_id, serialno, cash, card, ota, point, reserv, move },
    } = this.state;

    const { device, room } = this.props;

    let list = [];

    console.log("- filter search", search);

    _.map(_.cloneDeep(search), (sale) => {
      let {
        room_type_id: typeid,
        room_id: rid,
        first_pay,
        state,
        s_state,
        check_in,
        check_out,
        check_out_exp,
        rollback,
        stay_type,
        channel,
        serialno: sno,
        pay_card_amt,
        pay_cash_amt,
        pay_point_amt,
        prepay_ota_amt,
        prepay_cash_amt,
        prepay_card_amt,
        prepay_point_amt,
        room_reserv_id,
        reg_date,
        mod_date,
        move_from,
      } = sale;

      // 필터 옵션.
      if (room_type_id && Number(typeid) !== Number(room_type_id)) return;
      if (room_id && Number(rid) !== Number(room_id)) return;
      if (serialno && String(sno) !== String(serialno)) return;

      let from = check_in;
      let to =
        searchType === 1 ? (state === "A" ? new Date() : state === "B" ? mod_date : check_out) : s_state === "C" ? check_out : s_state === "A" && state === "A" && first_pay ? new Date() : reg_date;

      // console.log("- sale reg_date", s_state, moment(check_out).format("YYYY-MM-DD HH:mm:ss"), moment(reg_date).format("YYYY-MM-DD HH:mm:ss"));
      // console.log("- sale from to", moment(from).format("YYYY-MM-DD HH:mm:ss"), moment(to).format("YYYY-MM-DD HH:mm:ss"));

      let useTime = date.diff(from, to);
      sale.useTime = useTime;

      console.log("- sale.useTime", sale.useTime, from, to);

      sale.isg_name = "";

      if (sno && device.isc) {
        let isg = _.find(device.isc, { serialno: sno });
        if (isg) sale.isg_name = isg.name;
      }

      if (move_from)
        // 이동 전 객실명.
        sale.move_room = _.find(room.list, { id: move_from });

      let fee = sale.default_fee + sale.add_fee;

      let recept = fee - (sale.pay_cash_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.pay_card_amt + sale.prepay_card_amt + sale.pay_point_amt + sale.prepay_point_amt);

      sale.receptAmt = recept > 0 ? Math.abs(recept) : 0;
      sale.returnAmt = recept < 0 ? Math.abs(recept) : 0;

      if ((rent && Number(stay_type) === 2) || (stay && Number(stay_type) === 1) || (long && Number(stay_type) === 3)) {
        list.push(sale);
      } else if (web && channel === "web") {
        list.push(sale);
      } else if (isc && (channel === "isc" || channel === "device")) {
        list.push(sale);
      } else if (api && channel === "api") {
        list.push(sale);
      } else if (ota && prepay_ota_amt) {
        list.push(sale);
      } else if (cash && (pay_cash_amt || prepay_cash_amt)) {
        list.push(sale);
      } else if (card && (pay_card_amt || prepay_card_amt)) {
        list.push(sale);
      } else if (point && (pay_point_amt || prepay_point_amt)) {
        list.push(sale);
      } else if (reserv && room_reserv_id) {
        list.push(sale);
      } else if (move && move_from) {
        list.push(sale);
      }

      // console.log("- filter add", sale);
    });

    console.log("- filter list", list);

    let { sum, total, receptAmtTot, returnAmtTot } = this.state;

    // 초기화.
    let set = {
      count: 0,
      pay_count: 0,
      default_fee: 0,
      add_fee: 0,
      prepay_ota_amt: 0,
      pay_cash_amt: 0,
      prepay_cash_amt: 0,
      pay_card_amt: 0,
      prepay_card_amt: 0,
      pay_point_amt: 0,
      prepay_point_amt: 0,
      receptAmt: 0,
      returnAmt: 0,
    };

    sum.stay = set;
    sum.rent = set;
    sum.long = set;

    total = receptAmtTot = returnAmtTot = 0;

    let sales = [];

    _.map(list, (sale, k) => {
      let type = sale.stay_type === 1 ? "stay" : sale.stay_type === 2 ? "rent" : "long";

      // 매출 취소는 합계에서 제외.
      if (sale.state !== "B") {
        let {
          count = 0,
          pay_count = 0,
          default_fee = 0,
          add_fee = 0,
          pay_cash_amt = 0,
          prepay_ota_amt = 0,
          prepay_cash_amt = 0,
          pay_card_amt = 0,
          prepay_card_amt = 0,
          pay_point_amt = 0,
          prepay_point_amt = 0,
          receptAmt = 0,
          returnAmt = 0,
        } = sum[type];

        let pay =
          sale.default_fee + sale.add_fee + sale.pay_cash_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.pay_card_amt + sale.prepay_card_amt + sale.pay_point_amt + sale.prepay_point_amt;

        if (searchType === 1) {
          count++;
        } else {
          if (!sale.move_from || sale.first_pay) {
            // console.log("- sale pay", pay);

            // 추가 매출 없으면 카운트 제외.
            if (pay) pay_count++; // 매출 보기 시 결제 건수.

            let isSaleId = _.find(sales, {
              sale_id: sale.sale_id,
            });

            // 매출 기준일때 숙박 카운팅
            if (!isSaleId) {
              count++; // 매출 보기 시 숙박 건수

              sales.push(sale);
            }
          }
        }

        let fee = sale.default_fee + sale.add_fee;

        let recept = fee - (sale.pay_cash_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.pay_card_amt + sale.prepay_card_amt + sale.pay_point_amt + sale.prepay_point_amt);

        sum[type] = {
          count,
          pay_count,
          default_fee: (default_fee += sale.default_fee),
          add_fee: (add_fee += sale.add_fee),
          pay_cash_amt: (pay_cash_amt += sale.pay_cash_amt),
          prepay_ota_amt: (prepay_ota_amt += sale.prepay_ota_amt),
          prepay_cash_amt: (prepay_cash_amt += sale.prepay_cash_amt),
          pay_card_amt: (pay_card_amt += sale.pay_card_amt),
          prepay_card_amt: (prepay_card_amt += sale.prepay_card_amt),
          pay_point_amt: (pay_point_amt += sale.pay_point_amt),
          prepay_point_amt: (prepay_point_amt += sale.prepay_point_amt),
          receptAmt: (receptAmt += recept > 0 ? Math.abs(recept) : 0),
          returnAmt: (returnAmt += recept < 0 ? Math.abs(recept) : 0),
        };

        total += sale.default_fee + sale.add_fee;
      }

      // console.log("- ", type, sum[type]);
    });

    // 미수/반환금 정리.
    _.map(sum, (v) => {
      if (v.receptAmt && v.returnAmt) {
        if (v.receptAmt > v.returnAmt) {
          v.receptAmt = v.receptAmt - v.returnAmt;
          v.returnAmt = 0;
        } else {
          v.receptAmt = 0;
          v.returnAmt = v.returnAmt - v.receptAmt;
        }
      }

      receptAmtTot += v.receptAmt;
      returnAmtTot += v.returnAmt;

      return v;
    });

    console.log("- sum ", { sum, total });

    this.setState({
      list,
      sum,
      total,
      receptAmtTot,
      returnAmtTot,
    });
  };

  handleSerach = () => {
    const {
      user: { place_id },
      preferences: { data_keep_day_sale },
    } = this.props;

    const {
      searchType,
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let minDate = moment().add(-data_keep_day_sale, "day");

    let begin = moment(bd).hour(bh).minute(bm);
    let end = moment(ed).hour(eh).minute(em);

    // 데이터 보관 기간 설정일 이내만 조회(실제 데이터는 존재)
    if (begin.isBefore(minDate)) begin = minDate.hour(bh).minute(bm);

    minDate = minDate.format("YYYY-MM-DD HH:mm");
    begin = begin.format("YYYY-MM-DD HH:mm:00");
    end = end.add(-1, "minute").format("YYYY-MM-DD HH:mm:59"); // 1초전 으로 설정.

    console.log("- handleSerach", { place_id, searchType, minDate, begin, end });

    if (searchType === 1) this.props.handleSerachRoomSales(place_id, begin, end);
    else this.props.handleSerachRoomSalePays(place_id, begin, end);
  };

  handleTimeMode = () => {
    const { timeMode, type } = this.state;

    this.setState({ timeMode: !timeMode }, () => this.handleTerm(-1, type));
  };

  handleSearchType = (searchType) => {
    console.log("- handleSearchType", { searchType });
    this.setState({ searchType }, () => this.handleSerach());
  };

  excelDown = () => {
    const { searchType } = this.state;

    // 매출 합계.
    new ExcelUtil("search-sales-sum", searchType === 1 ? "매출 집계표" : "결제 집계표").tableToExcel();

    // 매출 목록.
    setTimeout(() => {
      new ExcelUtil("search-sales-list", searchType === 1 ? "매출 목록" : "결제 목록").tableToExcel();
    }, 1000);
  };

  // 환경 설정.
  handlePreferencesSave = (name, value) => {
    console.log("- handlePreferencesSave", name, value);

    const { preferences } = this.props;

    let modifyClone = _.cloneDeep(preferences);

    modifyClone[name] = value;

    // Object To Json.
    if (modifyClone.voice_opt) modifyClone.voice_opt = JSON.stringify(modifyClone.voice_opt);
    if (modifyClone.stay_type_color) modifyClone.stay_type_color = JSON.stringify(modifyClone.stay_type_color);
    if (modifyClone.key_type_color) modifyClone.key_type_color = JSON.stringify(modifyClone.key_type_color);
    if (modifyClone.room_name_color) modifyClone.room_name_color = JSON.stringify(modifyClone.room_name_colors);
    if (modifyClone.time_bg_color) modifyClone.time_bg_color = JSON.stringify(modifyClone.time_bg_color);

    this.props.handleSavePreferences(preferences.id, {
      preferences: modifyClone,
    });
  };

  componentDidMount() {
    const { room_id, preferences } = this.props;
    let { option } = this.state;

    console.log("- componentDidMount room_id", room_id, preferences.sale_view_type);

    if (room_id) option.room_id = room_id;

    this.setState(
      {
        option,
        searchType: preferences.sale_view_type || 2,
        loading: false,
      },
      () => {
        this.handleTerm(0, "day"); // 오늘 일자로 설정.
      }
    );
  }

  componentWillUnmount() {
    // 환경설정에 저장.
    this.handlePreferencesSave("sale_view_type", this.state.searchType);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 객실 매출 정보.
    if (nextProps.roomSale && nextProps.roomSale.search !== this.props.roomSale.search) {
      const { search } = nextProps.roomSale;

      this.filter(search);
    }

    // 객실 결제 정보.
    if (nextProps.roomSalePay && nextProps.roomSalePay.search !== this.props.roomSalePay.search) {
      const { search } = nextProps.roomSalePay;

      this.filter(search);
    }
  }

  render() {
    return (
      <Gnb0101
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        onDataChange={this.onDataChange}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        handleSerach={this.handleSerach}
        handleTerm={this.handleTerm}
        excelDown={this.excelDown}
        handleOption={this.handleOption}
        handleTimeMode={this.handleTimeMode}
        handleSearchType={this.handleSearchType}
      />
    );
  }
}

export default withSnackbar(Container);
