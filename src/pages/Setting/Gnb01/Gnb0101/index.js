// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomSaleAPayction } from "actions/roomSalePay";
import { actionCreators as preferencesAction } from "actions/preferences";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, room, roomType, roomSale, roomSalePay, roomState, device } = state;

  return {
    user: auth.user,
    preferences: preferences.item,
    room,
    roomType,
    roomSale,
    roomSalePay,
    roomState,
    device,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleSerachRoomSales: (palceId, begin, end) => dispatch(roomSaleAction.getSerachRoomSales(palceId, begin, end)),

    handleSerachRoomSalePays: (palceId, begin, end) => dispatch(roomSaleAPayction.getSerachRoomSalePays(palceId, begin, end)),

    handleSavePreferences: (id, preferences) => dispatch(preferencesAction.putPreferences(id, preferences)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
