// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

import cx from "classnames";

import moment from "moment";
import _ from "lodash";
// import { keyToValue } from "constants/key-map";

// Components.
import Gnb0102 from "./presenter";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    roomStateLog: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      list: [],
      sorted: { id: false },
      sort: "id",
      begin: { date: moment(moment(new Date(), "YYYY-MM-DD 00:00")), hour: 0, min: 0 },
      end: { date: moment(), hour: 24, min: 0 },
      option: {
        all: true,
        key_1: true,
        key_3: true,
        door_0: true,
        rent: true,
        stay: true,
        long: true,
        cancel_in: true,
        cancel_out: true,
        clean_1: true,
        clean_0: true,
        outing_1: true,
        outing_0: true,
        check_out: true,
        main_relay_0: true,
        main_relay_1: true,
        car_call_1: true,
        car_call_2: true,
        car_call_3: true,
        change_room_id: true,
        room_id: undefined,
      }, // 필터 적용,
      intval: null,
      stop: false,
      repeat: 0,
      count: 0,
    };
  }

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  handleSerach = () => {
    const {
      user: { place_id },
      preferences: { data_keep_day_state },
    } = this.props;

    const {
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let minDate = moment().add(-data_keep_day_state, "day");

    let begin = moment(bd).hour(bh).minute(bm);
    let end = moment(ed).hour(eh).minute(em);

    // 데이터 보관 기간 설정일 이내만 조회(실제 데이터는 존재)
    if (begin.isBefore(minDate)) begin = minDate.hour(bh).minute(bm);

    minDate = minDate.format("YYYY-MM-DD HH:mm");
    begin = begin.format("YYYY-MM-DD HH:mm");
    end = end.format("YYYY-MM-DD HH:mm");

    console.log("- handleSerach", { place_id, minDate, begin, end });

    this.stopFilter(true);

    this.props.initRoomStateSearchLogs(place_id, begin, end, "");

    this.setState({
      loading: true,
    });
  };

  handleDateTime = (date) => {
    console.log("- handleDateTime ", JSON.stringify(date));

    const state = _.merge({}, this.state, date);

    this.setState(state);
  };

  handleTerm = (value, mode) => {
    const { begin, end } = this.state;
    const { preferences } = this.props;

    // 시간 초기화.
    begin.hour = preferences.day_sale_end_time;
    begin.min = 0;

    end.hour = preferences.day_sale_end_time;
    end.min = 0;

    let start = moment({ hour: preferences.day_sale_end_time, minute: 0 });

    // 오늘 정산 시간 지났는지 여부.
    let isTodaySaleEnd = false;

    if (start > moment()) {
      // 매출 마감 시간이 남았다면.
      isTodaySaleEnd = true;
    }

    if (mode === "day" && value === -1) {
      begin.date = moment().add(value, mode);
      end.date = moment().add(value + 1, mode);
    } else if (mode === "week" && value === -1) {
      begin.date = moment().startOf("week");
      end.date = moment().endOf("week");
    } else if (mode === "month" && value === -1) {
      begin.date = moment().startOf("month");
      end.date = moment().endOf("month");
    } else if (mode === "year" && value === -1) {
      begin.date = moment().startOf("year");
      end.date = moment().endOf("year");
    } else {
      // 오늘 (정산 시간 지났으면 오늘 ~ 내일 정산 일자로 설정)
      begin.date = moment().add(isTodaySaleEnd ? -1 : 0, mode);
      end.date = moment().add(isTodaySaleEnd ? 0 : 1, mode);
    }

    console.log("- handleTerm", value, mode, begin);

    this.setState({
      begin,
      end,
    });
  };

  handleOption = ({ option }) => {
    const {
      roomStateLog: { search },
    } = this.props;

    console.log("- handleOption option ", JSON.stringify(option));

    let filter = _.merge({}, this.state.option, option);

    // 전체 선택/해제.
    if (option.all !== undefined) {
      _.map(this.state.option, (v, k) => {
        if (k !== "room_id") filter[k] = option.all;
      });
    } else {
      // 전체 옵션 선택 시 전체 체크.
      if (!option.all) {
        let isAll = true;
        _.map(filter, (v, k) => {
          console.log("- k,v", k, v);
          if (k !== "room_id" && k !== "all" && !v) {
            isAll = false;
            return false;
          }
        });

        filter.all = isAll;
      }
    }

    // console.log("- filter", filter);

    this.stopFilter(true);

    this.setState({ option: filter }, () => {
      this.filter(search);
    });
  };

  startFilter = (search) => {
    console.log("- startFilter", search.length);

    let list = [];
    let count = 0;

    let slicearray = [];
    let size = 100;

    // 일정 갯수로 자른다.
    for (var i = 0; i < search.length; i += size) {
      slicearray.push(search.slice(i, i + size));
      size = i < 1000 ? 100 : 300;
    }

    console.log("- slicearray slicearray", slicearray.length);

    this.stopFilter(true);

    // 화면 랜더링 시간 문제로 나누어서 리스팅 한다.
    let intval = setInterval(() => {
      setTimeout(() => {
        let { intval, stop } = this.state;

        if (intval && !stop) {
          list = this.filter(slicearray[count], list);

          console.log("- list length", count, list.length, moment().format("HH:mm:ss:SSS"));

          this.setState({
            list,
          });

          count++;

          if (count > slicearray.length) {
            stop = true;
            this.stopFilter();
          } else {
            this.setState({
              count,
            });
          }
        }
      }, 50);
    }, 200);

    this.setState({
      intval,
      stop: false,
      repeat: slicearray.length,
      count: 0,
    });
  };

  stopFilter = (stop) => {
    let { intval } = this.state;

    console.log("- stopFilter", intval, stop);

    if (intval) clearInterval(intval);

    this.setState({
      intval: null,
      stop,
    });
  };

  // 목록 필터링.
  filter = (search, list = []) => {
    const { stay_type_color } = this.props.preferences;

    const {
      option: {
        key_1,
        key_3,
        door_0,
        rent,
        stay,
        long,
        cancel_in,
        cancel_out,
        clean_1,
        clean_0,
        outing_1,
        outing_0,
        check_out: _check_out,
        main_relay_0,
        main_relay_1,
        car_call_1,
        car_call_2,
        car_call_3,
        change_room_id: _change_room_id,
        room_id,
      },
      sort,
      sorted,
    } = this.state;

    // 랜더링 시간동안 로딩바 보인다.
    this.props.handleShowLoader();

    _.each(_.cloneDeep(search), (state) => {
      const { room_id: id, reg_date, name, data } = state;

      const { key, door, sale, clean, outing, channel, check_in, check_out, change_room_id, rollback, user_id, stay_type, main_relay, car_call } = data;

      // console.log("- filter", { room_id, data });

      // 필터 옵션.
      if (room_id && Number(id) !== Number(room_id)) return;

      if ((key_1 && (Number(key) === 1 || Number(key) === 4)) || (key_3 && (Number(key) === 3 || Number(key) === 6))) state.key = key;
      if (door_0) state.door = door;

      state.check_in = check_in;

      if (_check_out && check_out) state.check_out = check_out;
      if (_change_room_id && change_room_id) state.change_room_id = change_room_id;

      if ((rent && Number(sale) === 2) || (stay && Number(sale) === 1) || (long && Number(sale) === 3)) state.sale = sale;
      if ((clean_1 && Number(clean) === 1) || (clean_0 && Number(clean) === 0)) state.clean = clean;
      if ((outing_1 && Number(outing) === 1) || (outing_0 && Number(outing) === 0)) state.outing = outing;
      if ((main_relay_1 && Number(main_relay) === 1) || (main_relay_0 && Number(main_relay) === 0)) state.main_relay = main_relay;
      if ((car_call_1 && Number(car_call) === 1) || (car_call_2 && Number(car_call) === 2) || (car_call_3 && Number(car_call) === 3)) state.car_call = car_call;
      if ((cancel_in && Number(rollback) === 1) || (cancel_out && Number(rollback) === 2)) state.rollback = rollback;

      // console.log("- state", data, state);

      if (Object.keys(state).length > 0) {
        let item = {
          reg_date,
          name,
          channel,
          sale,
          stay_type,
          data: "",
          user_id,
        };

        if (state.rollback && stay_type) {
          item.data += state.rollback === 1 ? " 입실취소" : " 퇴실취소";
          item.color = state.rollback === 1 ? "red" : "green";
          list.push(_.clone(item));
        }

        if (!state.check_in && state.sale) {
          item.data += "입실";
          item.color = stay_type_color[state.sale] || "";
          list.push(_.clone(item));
        }
        if (state.key !== undefined) {
          item.data =
            Number(state.key) === 1
              ? "고객키 ▼"
              : Number(state.key) === 2
              ? "마스터키 ▼"
              : Number(state.key) === 3
              ? "청소키 ▼"
              : Number(state.key) === 4
              ? "고객키 △"
              : Number(state.key) === 5
              ? "마스터키 △"
              : Number(state.key) === 6
              ? "청소키 △"
              : "NO KEY";

          item.color = "";
          list.push(_.clone(item));
        }
        if (state.door !== undefined) {
          item.data = Number(state.door) === 1 ? "문 【열림】" : "문 닫】【힘";
          item.color = "";
          list.push(_.clone(item));
        }
        if (state.clean !== undefined) {
          item.data = Number(state.clean) === 1 ? "청소요청" : "청소완료";
          item.color = "";
          list.push(_.clone(item));
        }
        if (state.outing !== undefined) {
          item.data = Number(state.outing) === 0 ? "외출복귀" : "외출";
          item.color = "";
          list.push(_.clone(item));
        }
        if (state.main_relay !== undefined) {
          item.data = Number(state.main_relay) === 0 ? "전원차단" : "전원공급";
          item.color = "";
          list.push(_.clone(item));
        }
        if (state.car_call !== undefined) {
          item.data = Number(state.car_call) === 1 ? "배차요청" : "배차완료";
          item.color = "";
          list.push(_.clone(item));
        }
        // if (state.check_in !== undefined) {
        //   item.data = keyToValue("room_sale", "stay_type", state.stay_type) + " 입실";
        //   list.push(_.clone(item));
        // }
        if (state.check_out !== undefined) {
          item.data = "퇴실";
          item.color = stay_type_color[state.sale] || "";
          list.push(_.clone(item));
        }
        if (!state.check_in && !state.check_out && state.change_room_id !== undefined) {
          item.data = "객실 이동";
          item.color = "";
          list.push(_.clone(item));
        }
      }
    });

    // 재 정렬
    list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

    this.setState({ list, loading: false });

    this.props.handleHideLoader();

    return list;
  };

  onSortClick = (sort) => {
    this.stopFilter();

    let { list, sorted } = this.state;

    console.log("-- onSortClick", sort, sorted);

    sorted[sort] = sorted[sort] ? !sorted[sort] : true;

    list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

    this.setState({
      list,
      sort,
      sorted,
      selected: [],
    });
  };

  componentDidMount() {
    const { room_id } = this.props;

    let { option } = this.state;

    if (room_id) option.room_id = room_id;

    console.log("- componentDidMount2 room_id", room_id);

    this.handleSerach();

    this.setState({
      option,
      loading: true,
    });
  }

  componentWillUnmount = () => {
    this.stopFilter();
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    // this.setState() 를 해도 추가적으로 렌더링하지 않습니다.

    // 객실 로그.
    if (nextProps.roomStateLog && nextProps.roomStateLog !== this.props.roomStateLog && nextProps.roomStateLog.search !== this.props.roomStateLog.search) {
      let { search } = nextProps.roomStateLog;

      // Test 데이터.
      // search = search.concat(search);
      // search = search.concat(search);
      // search = search.concat(search);
      // search = search.concat(search);
      // search = search.concat(search);

      this.startFilter(search);
    }
  }

  render() {
    return (
      <Gnb0102
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleDateTime={this.handleDateTime}
        handleOption={this.handleOption}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        handleSerach={this.handleSerach}
        handleTerm={this.handleTerm}
        onSortClick={this.onSortClick}
        stopFilter={this.stopFilter}
      />
    );
  }
}

export default withSnackbar(Container);
