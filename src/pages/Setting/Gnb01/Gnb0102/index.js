// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomStateLogAction } from "actions/roomStateLog";
import { actionCreators as commonAction } from "actions/common";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, room, roomState, roomStateLog, preferences } = state;

  return {
    user: auth.user,
    preferences: preferences.item,
    room,
    roomState,
    roomStateLog,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initRoomStateSearchLogs: (placeId, begin, end, filter) => dispatch(roomStateLogAction.getRoomStateSearchLogs(placeId, begin, end, filter)),
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
    handleShowLoader: () => dispatch(commonAction.showLoader()),
    handleHideLoader: () => dispatch(commonAction.hideLoader()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
