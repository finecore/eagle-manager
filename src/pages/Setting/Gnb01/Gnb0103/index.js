// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as mileageAction } from "actions/mileage";
import { actionCreators as mileageLogAction } from "actions/mileageLog";
import { actionCreators as memberAction } from "actions/member";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, member, mileageLog, preferences } = state;

  return {
    user: auth.user,
    preferences: preferences.item,
    member,
    mileageLog,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initMileageSearchLogs: (placeId, begin, end, filter) => dispatch(mileageLogAction.getMileageSearchLogs(placeId, begin, end, filter)),

    initAllMileages: (palceId) => dispatch(mileageAction.getMileageList(palceId)),

    initAllMembers: (palceId) => dispatch(memberAction.getMemberList(palceId)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
