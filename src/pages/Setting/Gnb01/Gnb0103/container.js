// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

import moment from "moment";
import _ from "lodash";
import format from "utils/format-util";

// Components.
import Gnb0103 from "./presenter";

class Container extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    mileageLog: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      list: [],
      sorted: { id: false },
      sort: "id",
      begin: { date: moment(moment(new Date(), "YYYY-MM-DD 00:00")), hour: 0, min: 0 },
      end: { date: moment(), hour: 24, min: 0 },
      option: {
        all: true,
        member: true,
        not_member: true,
        plus_point: true,
        minus_point: true,
      }, // 필터 적용,
    };
  }

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  handleSerach = () => {
    const {
      user: { place_id },
    } = this.props;

    const {
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let begin = moment(bd).hour(bh).minute(bm).format("YYYY-MM-DD HH:mm");

    let end = moment(ed).hour(eh).minute(em).format("YYYY-MM-DD HH:mm");

    console.log("- handleSerach", place_id, begin, end);

    this.props.initMileageSearchLogs(place_id, begin, end, "");
  };

  handleDateTime = (date) => {
    console.log("- handleDateTime ", JSON.stringify(date));

    const state = _.merge({}, this.state, date);

    this.setState(state);
  };

  handleTerm = (value, mode) => {
    const { begin, end } = this.state;
    const { preferences } = this.props;

    // 시간 초기화.
    begin.hour = preferences.day_sale_end_time;
    begin.min = 0;

    end.hour = preferences.day_sale_end_time;
    end.min = 0;

    if (mode === "day" && value === -1) {
      // 어제
      begin.date = moment().add(-2, mode);
      end.date = moment().add(-1, mode);
    } else if (mode === "week" && value === -1) {
      begin.date = moment().startOf("week");
      end.date = moment().endOf("week");
    } else if (mode === "month" && value === -1) {
      begin.date = moment().startOf("month");
      end.date = moment().endOf("month");
    } else if (mode === "year" && value === -1) {
      begin.date = moment().startOf("year");
      end.date = moment().endOf("year");
    } else {
      // 오늘
      begin.date = moment().add(0, mode);
      end.date = moment().add(1, mode);
    }

    console.log("- handleTerm", value, mode, begin);

    this.setState({
      begin,
      end,
    });
  };

  handleOption = ({ option }) => {
    const {
      mileageLog: { search },
    } = this.props;

    console.log("- handleOption option ", JSON.stringify(option));

    let filter = _.merge({}, this.state.option, option);

    // 전체 선택/해제.
    if (option.all !== undefined) {
      _.map(this.state.option, (v, k) => {
        if (k !== "user_id") filter[k] = option.all;
      });
    } else {
      // 전체 옵션 선택 시 전체 체크.
      if (!option.all) {
        let isAll = true;
        _.map(filter, (v, k) => {
          console.log("- k,v", k, v);
          if (k !== "user_id" && k !== "all" && !v) {
            isAll = false;
            return false;
          }
        });

        filter.all = isAll;
      }
    }

    // console.log("- filter", filter);

    this.setState({ option: filter }, () => {
      this.filter(search);
    });
  };

  // 목록 필터링.
  filter = (search) => {
    const {
      option: { member, not_member, plus_point, minus_point },
      sort,
      sorted,
    } = this.state;

    let list = search.slice();

    if (!member) list = _.filter(list, (log) => !log.user_id);
    if (!not_member) list = _.filter(list, (log) => log.user_id);
    if (!plus_point) list = _.filter(list, (log) => log.change_point <= 0);
    if (!minus_point) list = _.filter(list, (log) => log.change_point >= 0);

    // 재 정렬.
    list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

    console.log("- filter", list);

    this.setState({ list });
  };

  handleFilter = (value) => {
    const {
      mileageLog: { search },
    } = this.props;

    let list = format.textFilter(search.slice(), value);

    // console.log("-- handleFilter", value);

    this.setState({ list });
  };

  onSortClick = (sort) => {
    let { list, sorted } = this.state;

    console.log("-- onSortClick", sort, sorted);

    sorted[sort] = sorted[sort] ? !sorted[sort] : true;

    list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

    this.setState({
      list,
      sort,
      sorted,
      selected: [],
    });
  };

  componentDidMount() {
    const {
      user: { place_id },
      user_id,
    } = this.props;

    let { option } = this.state;

    if (user_id) option.user_id = user_id;

    console.log("- componentDidMount2 user_id", user_id);

    const {
      begin: { date: bd, hour: bh, min: bm },
      end: { date: ed, hour: eh, min: em },
    } = this.state;

    let begin = moment(bd).hour(bh).minute(bm).format("YYYY-MM-DD HH:mm");

    let end = moment(ed).hour(eh).minute(em).format("YYYY-MM-DD HH:mm");

    this.props.initMileageSearchLogs(place_id, begin, end, ""); // 마일리지 정보 조회.
    this.props.initAllMileages(place_id);

    this.setState({
      option,
      loading: false,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // 마일리지 로그.
    if (nextProps.mileageLog && nextProps.mileageLog !== this.props.mileageLog) {
      const { search } = nextProps.mileageLog;

      this.filter(search);
    }
  }

  render() {
    return (
      <Gnb0103
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleDateTime={this.handleDateTime}
        handleOption={this.handleOption}
        focused={this.state.focused}
        onFocusChange={this.onFocusChange}
        handleSerach={this.handleSerach}
        handleTerm={this.handleTerm}
        onSortClick={this.onSortClick}
        handleFilter={this.handleFilter}
      />
    );
  }
}

export default withSnackbar(Container);
