// imports.
import React from "react";
import Modal from "react-awesome-modal";

import moment from "moment";

//room-popup
import { Form, FormControl, Checkbox, Table, ButtonToolbar, Button, Radio } from "react-bootstrap";

import cx from "classnames";
import _ from "lodash";
import format from "utils/format-util";

import { keyToValue } from "constants/key-map";

import InputNumber from "rc-input-number";
import { SingleDatePicker } from "react-dates";

// styles.
import "./styles.scss";

// functional component
const Gnb0103 = (props) => {
  // let column = ["id", "phone", "member_id", "member_name", "place_id", "user_id", "change_point", "point"];
  // let title = ["일시", "전화번호", "고객아이디", "고객명", "구분", "근무자", "변경 포인트", "잔여 포인트"];
  let column = ["id", "phone", "type", "user_id", "change_point", "point"];
  let title = ["일시", "전화번호", "구분", "근무자", "변경 포인트", "잔여 포인트"];

  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper">
          <h1>포인트 이력 조회</h1>
          <div className="setting-form-wrapper" id="gnb0103">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <h2>조회일자</h2>
                    <div className="form-content">
                      <Radio inline name="search_term" value="1" onChange={(evt) => props.handleTerm(-1, "day")}>
                        <span />
                        어제
                      </Radio>
                      <Radio inline name="search_term" value="2" defaultChecked={true} onChange={(evt) => props.handleTerm(0, "day")}>
                        <span />
                        오늘
                      </Radio>
                      <Radio inline name="search_term" value="3" onChange={(evt) => props.handleTerm(-1, "week")}>
                        <span />
                        이번주
                      </Radio>
                      <Radio inline name="search_term" value="4" onChange={(evt) => props.handleTerm(-1, "month")}>
                        <span />
                        이번달
                      </Radio>
                      <div className="date-set">
                        <SingleDatePicker
                          id="startsOnDateInput-1"
                          placeholder="검색 시작일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.begin.date}
                          isOutsideRange={(day) => moment(day) > moment().add(1, "day")}
                          onDateChange={(date) => props.handleDateTime({ begin: { date } })}
                          focused={props.focused1}
                          onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.hour}
                            step={1}
                            min={0}
                            max={24}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.handleDateTime({ begin: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.begin.min}
                            step={1}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.handleDateTime({ begin: { min } })}
                          />
                        </span>
                        <span> ~ </span>
                        <SingleDatePicker
                          id="endsOnDateInput-2"
                          placeholder="검색 종료일 선택"
                          displayFormat="YYYY-MM-DD"
                          date={props.end.date}
                          isOutsideRange={(day) => moment(day).diff(props.begin.date) < 0 || moment(day) > moment().add(1, "day")}
                          onDateChange={(date) => props.handleDateTime({ end: { date } })}
                          focused={props.focused2}
                          onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                        />
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.hour}
                            step={1}
                            min={0}
                            max={24}
                            formatter={(value) => `${value}시`}
                            onChange={(hour) => props.handleDateTime({ end: { hour } })}
                          />
                        </span>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control mid"
                            value={props.end.min}
                            step={1}
                            min={0}
                            max={59}
                            formatter={(value) => `${value}분`}
                            onChange={(min) => props.handleDateTime({ end: { min } })}
                          />
                        </span>

                        <span>
                          <label className="checkbox-inline" title="">
                            <Button onClick={(evt) => props.handleSerach(evt)}>검색조회</Button>
                          </label>
                        </span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <h2>필터옵션</h2>
                    <div className="form-content search-select">
                      <label>검색어 </label>
                      <FormControl type="text" placeholder="검색어를 입력 하세요" value={props.option.room_id} onChange={(evt) => props.handleFilter(evt.target.value)}></FormControl>
                    </div>
                    <Checkbox inline checked={props.option.all} onChange={(evt) => props.handleOption({ option: { all: evt.target.checked } })}>
                      <span />
                      전체
                    </Checkbox>

                    <div className="separator" />
                    <div className="form-content search-select">
                      {/* <Checkbox inline checked={props.option.member} onChange={evt => props.handleOption({ option: { member: evt.target.checked } })}>
                        <span />
                        회원
                      </Checkbox>
                      <Checkbox inline checked={props.option.not_member} onChange={evt => props.handleOption({ option: { not_member: evt.target.checked } })}>
                        <span />
                        비회원
                      </Checkbox> */}
                      <Checkbox inline checked={props.option.plus_point} onChange={(evt) => props.handleOption({ option: { plus_point: evt.target.checked } })}>
                        <span />
                        포인트 추가
                      </Checkbox>
                      <Checkbox inline checked={props.option.minus_point} onChange={(evt) => props.handleOption({ option: { minus_point: evt.target.checked } })}>
                        <span />
                        포인트 차감
                      </Checkbox>
                    </div>
                  </li>
                  <li className="search-count">Tot. {String(props.list.length).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</li>
                </ul>
              </div>
              <div className="search-result">
                <div className="table-wrapper">
                  <Table>
                    <thead>
                      <tr>
                        {column.map((col, i) => (
                          <th key={i} className={cx(props.sort === col ? "on" : "off")} onClick={(evt) => props.onSortClick(col)}>
                            {title[i]}
                            <b className={cx("caret", props.sorted[col] === false ? "reverse" : "")} />
                          </th>
                        ))}
                      </tr>
                    </thead>
                    <tbody>
                      {_.map(props.list, (log, k) => (
                        <tr key={k} className={cx("slide")}>
                          <td>{moment(log.reg_date).format("YYYY-MM-DD HH:mm:ss")}</td>
                          <td>{format.toPhone(log.phone)}</td>
                          {/* <td style={{ color: log.member_id ? "" : "#ddd" }}>{log.member_id || "비회원"}</td>
                          <td style={{ color: log.member_id ? "" : "#ddd" }}>{log.member_name || ""}</td> */}
                          <td style={{ color: log.type === 1 ? "#09130b" : log.type === 2 ? "#389438" : "#e83646" }}>{keyToValue("mileage", "type", log.type)}</td>
                          <td>{log.user_id}</td>
                          <td className={cx(log.change_point < 0 ? "minus" : "plus")}>
                            {/* {log.change_point > 0 ? "+" : ""} */}
                            {String(log.change_point).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </td>
                          <td>
                            <b>{String(log.point).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</b>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </div>
              <ButtonToolbar className="btn-room">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0103;
