// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import _ from "lodash";
// import moment from "moment";

import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";
import { imageHandler, imageRemove } from "utils/file-util";
import { fileDownload } from "utils/file-util";

// Components.
import SetTab02 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    noticePlace: PropTypes.object.isRequired,
    changeTab: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.editorRef = null;

    this.state = {
      modify: {
        id: 0,
        type: 1,
        important: 1,
        title: "",
        content: "",
      },
      fileList: [],
      editor: {
        modules: {
          imageResize: {
            displayStyles: {
              backgroundColor: "black",
              border: "none",
              color: "white",
            },
            modules: ["Resize", "DisplaySize", "Toolbar"],
          },
          toolbar: {
            container: [
              [{ header: "1" }, { header: "2" }, { font: [] }],
              [{ size: [] }],
              ["bold", "italic", "underline", "strike", "blockquote"],
              [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }],
              ["link", "image"],
              ["clean"],
            ],
            handlers: {
              image: (image, callback) => {
                const {
                  auth: { token },
                } = this.props;

                // console.log("-imageHandler", image, this.editorRef.editor, token);
                imageHandler(this.editorRef.editor, "notice_place", token);
              },
            },
          },
        },
      },
    };
  }

  setEditorRef = (ref) => {
    this.editorRef = ref;
  };

  // 권한 체크.
  checkAuth = (type) => {
    let { modify } = this.state;

    const {
      auth: {
        user: { id, level },
      },
    } = this.props;

    // 본인 또는 매니저 권한 이상만 수정 가능.
    if (modify.user_id !== id && level > 2) {
      Swal.fire({
        icon: "error",
        title: "권한 알림",
        html: type + " 권한이 없습니다.",
        showCloseButton: true,
        confirmButtonText: "확인",
      });
      return false;
    }

    return true;
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value, save) => {
    let { modify } = this.state;

    // 아이템 정보 변경.
    modify[name] = !value || isNaN(value) || name.lastIndexOf("_id") === -1 ? value : Number(value);

    console.log("- handleInputChange name", name, "value", modify[name]);

    this.setState({
      modify,
    });
  };

  changeEditor = (e) => {
    let { modify } = this.state;

    console.log("- changeEditor ", e);

    modify.content = e;

    this.setState({
      modify,
    });
  };

  onSave = () => {
    let { modify } = this.state;
    const {
      auth: { token },
      preferences: { speed_checkin },
      item,
    } = this.props;

    // 수정/삭제 는 등록자 보다 상위 권한만 가능
    if (modify.id && !this.checkAuth("수정")) return;

    let validate = "";
    if (!modify.title) validate = "제목을 입력해 주세요.";
    if (!modify.content && !validate) validate = "내용을 입력해 주세요.";

    if (validate) {
      Swal.fire({
        icon: "error",
        title: "입력 확인",
        html: validate,
        showCloseButton: true,
        confirmButtonText: "확인",
      });
      return false;
    }

    Swal.fire({
      icon: "info",
      title: "알림 " + (!modify.id ? "등록" : "수정") + " 확인",
      html: "알림을 " + (!modify.id ? "등록" : "수정") + " 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        if (!modify.id) {
          this.props.handleNewNoticePlace({ notice_place: modify }).then((res) => {
            this.props.changeTab(1, {}); // 목록 탭으로 전환.
          });
        } else {
          this.props.handleSaveNoticePlace(modify.id, { notice_place: modify }).then((res) => {
            if (res) {
              // 삭제된 이미지는 서버에서도 삭제 한다.
              let images = String(item.content || "").match(/<img.*?src="(.*?)"(.*?)>/gi);

              // console.log("- images", item.content, images);

              _.map(images, (img) => {
                let src = img.replace(/(.*)src="(.*?)"(.*)/, "$2").split("/");
                let name = src.pop();
                let path = src.pop();

                let isImage = modify.content.indexOf(name);

                console.log("- isImage", isImage, path, name);

                // 이미지 삭제
                if (isImage === -1 && name) {
                  imageRemove(path, name, token);
                }
              });

              this.props.changeTab(1, {}); // 목록 탭으로 전환.
            }
          });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onDelete = () => {
    let { modify } = this.state;
    const {
      auth: { token },
      preferences: { speed_checkin },
      item,
    } = this.props;

    // 수정/삭제 는 등록자 보다 상위 권한만 가능
    if (!this.checkAuth("삭제")) return;

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: "알림을 삭제 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        this.props.handleDeleteNoticePlace(modify.id).then((res) => {
          if (res) {
            // 삭제된 이미지는 서버에서도 삭제 한다.
            let images = String(item.content || "").match(/<img.*?src="(.*?)"(.*?)>/gi);

            // console.log("- images", item.content, images);

            _.map(images, (img) => {
              let src = img.replace(/(.*)src="(.*?)"(.*)/, "$2").split("/");
              let name = src.pop();
              let path = src.pop();

              console.log("- del img", path, name);

              // 이미지 삭제
              if (path && name) {
                imageRemove(path, name, token);
              }
            });

            if (!speed_checkin) {
              Swal.fire("삭제 완료", "알림 삭제가 완료 되었습니다", "success").then((result) => {
                this.props.changeTab(1, {});
              });
            } else {
              this.props.changeTab(1, {});
            }
          }
        });
      }
    });
  };

  onList = () => {
    // 목록 탭으로 전환.
    this.props.changeTab(1, {});
  };

  clickDownload = (file) => {
    const {
      auth: { token },
    } = this.props;

    let { id, name } = file;

    console.log("- clickDownload file", file);

    fileDownload(id, name, token);
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    let { modify } = this.state;

    const {
      auth: {
        user: { id, place_id },
      },
      item,
    } = this.props;

    console.log("- componentDidMount item", item);

    if (item && item.id) modify = _.cloneDeep(item);

    modify.place_id = place_id;
    modify.user_id = id;

    console.log("- componentDidMount modify", modify);

    this.setState({
      modify,
    });
  };

  componentWillUnmount = () => {
    this.setState({
      dialog: false,
    });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.file && prevState.file !== nextProps.file) {
      let { list } = nextProps.file;
      console.log("- nextProps file", list);

      return { fileList: list };
    }

    return null;
  }

  render() {
    return (
      <SetTab02
        {...this.props}
        {...this.state}
        setEditorRef={this.setEditorRef}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        changeEditor={this.changeEditor}
        onSave={this.onSave}
        onDelete={this.onDelete}
        onList={this.onList}
        clickDownload={this.clickDownload}
        checkAuth={this.checkAuth}
      />
    );
  }
}

export default withSnackbar(Container);
