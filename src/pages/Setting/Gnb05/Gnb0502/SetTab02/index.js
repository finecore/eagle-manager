// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as noticePlaceAction } from "actions/noticePlace";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, preferences, noticePlace } = state;

  return {
    place,
    auth,
    noticePlace,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleNewNoticePlace: (notice_place) => dispatch(noticePlaceAction.newNoticePlace(notice_place)),
    handleSaveNoticePlace: (id, notice_place) => dispatch(noticePlaceAction.putNoticePlace(id, notice_place)),
    handleDeleteNoticePlace: (id) => dispatch(noticePlaceAction.delNoticePlace(id)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
