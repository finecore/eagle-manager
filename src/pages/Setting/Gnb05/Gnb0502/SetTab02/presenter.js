// imports.
import React from "react";

import { FormGroup, FormControl, ControlLabel, ButtonToolbar, Button } from "react-bootstrap";

import cx from "classnames";

// styles.
import "./styles.scss";

import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css"; // 커스텀 라이브러리를 등록해 준다.

const SetTab02 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb050202">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>{props.modify.id ? "알림 정보 수정" : "알림 정보 입력"}</h2>
              <div className={cx("form-content")}>
                <div>
                  <FormGroup>
                    <ControlLabel>알림 타입</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="알림타입"
                      value={props.modify.type}
                      onChange={(evt) => props.handleInputChange("type", Number(evt.target.value))}
                      style={{
                        color: props.modify.type === 1 ? "" : "#0022ff",
                      }}
                    >
                      <option value={1}>알림</option>
                      <option value={2}>메모</option>
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>진행 상태</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="진행 상태"
                      value={props.modify.state}
                      onChange={(evt) => props.handleInputChange("state", Number(evt.target.value))}
                      style={{
                        color: props.modify.state === 4 ? "#ff2200" : props.modify.state === 9 ? "#00ff00" : "",
                      }}
                    >
                      <option value="1">등록</option>
                      <option value="2">확인</option>
                      <option value="3">처리중</option>
                      <option value="4">취소</option>
                      <option value="9">처리완료</option>
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>제목</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="제목을 입력해 주세요"
                      value={props.modify.title || ""}
                      onChange={(evt) => props.handleInputChange("title", evt.target.value)}
                      maxLength={100}
                    />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>내용</ControlLabel>
                    <ReactQuill
                      theme={"snow"}
                      className="editor"
                      ref={(el) => props.setEditorRef(el)}
                      onChange={props.changeEditor}
                      value={props.modify.content}
                      modules={props.editor.modules}
                      // formats={props.editor.formats} // 포멧 사용 시 리사이즈 한 이미지가 로딩 하면 초기화 되는 현상이 있어서 사용 안함
                      placeholder={"내용을 입력해 주세요"}
                    />
                  </FormGroup>
                </div>
                <div className="file-list">
                  <FormGroup>
                    <ControlLabel>첨부파일</ControlLabel>
                    {props.fileList.map((file, i) => (
                      <p key={i}>
                        <span>{file.name}</span>
                        <Button onClick={(evt) => props.clickDownload(file)} className="btn-set-confirm">
                          다운로드
                        </Button>
                      </p>
                    ))}
                  </FormGroup>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>{props.modify.id ? "알림 수정" : "알림 등록"}</Button>
          {props.modify.id ? (
            <Button onClick={(evt) => props.onDelete()} className="btn-set-warning">
              알림 삭제
            </Button>
          ) : null}
          <Button onClick={(evt) => props.onList()} className="btn-set-cancel">
            알림 목록
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab02;
