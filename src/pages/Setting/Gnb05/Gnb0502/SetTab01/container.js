// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import moment from "moment";
import _ from "lodash";
import format from "utils/format-util";

// Components.
import SetTab01 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    noticePlace: PropTypes.object.isRequired, // 현재 객실 예약 정보.
    changeTab: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      list: [],
      sort: "reg_date",
      reverse: true,
      selType: 0,
      selState: 0,
      searchText: "",
      begin: { date: moment().add(-7, "day"), hour: 0, min: 0 },
      end: { date: moment(), hour: 23, min: 59 },
      focused1: null,
      focused2: null,
    };
  }

  onTypeSelected = (type) => {
    let { sort, reverse, selState, searchText } = this.state;
    let { noticePlace } = this.props;

    console.log("-- onTypeSelected", { type });

    let list = _.filter(noticePlace.list, function (o) {
      return (type ? o.type === type : true) && (selState ? o.state === selState : true);
    });

    list = format.textFilter(list, searchText);

    console.log("-- list", list);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selType: type,
    });
  };

  onStateSelected = (state) => {
    let { sort, reverse, selType, searchText } = this.state;
    let { noticePlace } = this.props;

    console.log("-- onStateSelected", { state });

    let list = _.filter(noticePlace.list, function (o) {
      return (state ? o.state === state : true) && (selType ? o.type === selType : true);
    });

    list = format.textFilter(list, searchText);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selState: state,
    });
  };

  onTextSearch = (search_text) => {
    let { sort, reverse, selType, selState } = this.state;
    let { noticePlace } = this.props;

    console.log("-- onTextSearch", search_text);

    let list = _.filter(noticePlace.list, function (o) {
      return (selType ? o.type === selType : true) && (selState ? o.state === selState : true);
    });

    list = format.textFilter(list, search_text);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      searchText: search_text,
    });
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  onDataChange = (data) => {
    console.log("- onDataChange", JSON.stringify(data));

    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    const state = _.merge({}, this.state, data);

    this.setState(state, () => {
      const {
        begin: { date: bd },
        end: { date: ed },
      } = this.state;

      let begin = moment(bd).format("YYYY-MM-DD 00:00");
      let end = moment(ed).format("YYYY-MM-DD 23:59");

      console.log("- ", { begin, end });

      let filter = `a.place_id=${place_id}`;

      this.props.initNoticePlaces(filter, begin, end);
    });
  };

  onSortClick = (sorted) => {
    let { list, sort, reverse } = this.state;

    console.log("-- onSortClick", sorted, sort, reverse);

    reverse = sort === sorted ? !reverse : false;

    // 재 정렬.
    list = _.orderBy(list, sorted, !reverse ? "asc" : "desc");

    this.setState({
      sort: sorted,
      reverse,
      list,
    });
  };

  onItemClick = (item) => {
    console.log("-- onItemClick", item);

    // 상세 탭으로 전환.
    this.props.changeTab(2, item);
  };

  componentDidMount = () => {
    console.log("-- componentDidMount");

    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    const {
      begin: { date: bd },
      end: { date: ed },
    } = this.state;

    let begin = moment(bd).format("YYYY-MM-DD 00:00");
    let end = moment(ed).format("YYYY-MM-DD 23:59");

    let filter = `a.place_id=${place_id}`;

    this.props.initNoticePlaces(filter, begin, end);
  };

  componentWillUnmount = () => {};

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.noticePlace && nextProps.noticePlace !== this.props.noticePlace && nextProps.noticePlace.list !== this.props.noticePlace.list) {
      let { sort, reverse, selType, selState } = this.state;

      let list = _.filter(nextProps.noticePlace.list, function (o) {
        return (selState ? o.state === selState : true) && (selType ? o.type === selType : true);
      });

      if (sort) list = _.sortBy(list, [sort]);
      if (reverse) list = list.reverse();

      this.setState({
        list,
      });
    }
    return null;
  }

  render() {
    return (
      <SetTab01
        {...this.props}
        {...this.state}
        onTypeSelected={this.onTypeSelected}
        onStateSelected={this.onStateSelected}
        onTextSearch={this.onTextSearch}
        onItemClick={this.onItemClick}
        onSortClick={this.onSortClick}
        onFocusChange={this.onFocusChange}
        onDataChange={this.onDataChange}
      />
    );
  }
}

export default withSnackbar(Container);
