// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as noticePlaceAction } from "actions/noticePlace";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, noticePlace } = state;

  return {
    auth,
    noticePlace,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initNoticePlaces: (place_id, begin, end) => dispatch(noticePlaceAction.getNoticePlaceList(place_id, begin, end)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
