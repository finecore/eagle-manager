// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as asAction } from "actions/as";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { place, auth, preferences, as } = state;

  return {
    place,
    auth,
    as,
    preferences: preferences.item,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleNewAs: (notice_place) => dispatch(asAction.newAs(notice_place)),
    handleSaveAs: (id, notice_place) => dispatch(asAction.putAs(id, notice_place)),
    handleDeleteAs: (id) => dispatch(asAction.delAs(id)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
