// imports.
import React from "react";

import { FormGroup, FormControl, ControlLabel, ButtonToolbar, Button } from "react-bootstrap";

import cx from "classnames";

// styles.
import "./styles.scss";

import ImageResize from "quill-image-resize-module";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css"; // 커스텀 라이브러리를 등록해 준다.

Quill.register("modules/ImageResize", ImageResize);

const SetTab02 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb050302">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>{(props.modify.type === 1 ? "AS 요청" : "건의 사항") + (props.modify.id ? " 수정" : " 입력")}</h2>
              <div className={cx("form-content")}>
                <div>
                  <FormGroup>
                    <ControlLabel>타입 선택</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="타입 선택"
                      value={props.modify.type}
                      onChange={(evt) => props.handleInputChange("type", Number(evt.target.value))}
                      style={{
                        color: props.modify.type === 1 ? "" : "#0022ff",
                      }}
                    >
                      <option value={1}>AS 요청</option>
                      <option value={2}>건의 사항</option>
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>진행 상태</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="진행 상태"
                      value={props.modify.state}
                      onChange={(evt) => props.handleInputChange("state", Number(evt.target.value))}
                      style={{
                        color: props.modify.state === 4 ? "#ff2200" : props.modify.state === 9 ? "#00ff00" : "",
                      }}
                    >
                      <option value="1">등록</option>
                      <option value="2">확인</option>
                      <option value="3">처리중</option>
                      <option value="4">취소</option>
                      <option value="9">처리완료</option>
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>제목</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="제목을 입력해 주세요"
                      value={props.modify.title || ""}
                      onChange={(evt) => props.handleInputChange("title", evt.target.value)}
                      maxLength={100}
                    />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>내용</ControlLabel>
                    <ReactQuill
                      theme={"snow"}
                      className="editor"
                      ref={(el) => props.setEditorRef(el)}
                      onChange={props.changeEditor}
                      value={props.modify.content}
                      modules={props.editor.modules}
                      // formats={props.editor.formats} // 포멧 사용 시 리사이즈 한 이미지가 로딩 하면 초기화 되는 현상이 있어서 사용 안함
                      bounds={".app"}
                      placeholder={"내용을 입력해 주세요"}
                    />
                  </FormGroup>
                </div>
                {props.modify.action ? (
                  <div>
                    <FormGroup>
                      <ControlLabel>조치사항</ControlLabel>
                      <ReactQuill theme={"snow"} className="editor" defaultValue={props.modify.action} modules={{ toolbar: false }} placeholder={"조치중 입니다.."} readOnly={true} />
                    </FormGroup>
                  </div>
                ) : null}
                {props.fileList.length ? (
                  <div className="file-list">
                    <FormGroup>
                      <ControlLabel>첨부파일</ControlLabel>
                      {props.fileList.map((file, i) => (
                        <p key={i}>
                          <span>{file.name}</span>
                          <Button onClick={(evt) => props.clickDownload(file)} className="btn-set-confirm">
                            다운로드
                          </Button>
                        </p>
                      ))}
                    </FormGroup>
                  </div>
                ) : null}
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onSave()}>{(props.modify.type === 1 ? "AS 요청" : "건의 사항") + (props.modify.id ? " 수정" : " 등록")}</Button>
          {props.modify.id ? (
            <Button onClick={(evt) => props.onDelete()} className="btn-set-warning">
              {props.modify.type === 1 ? "AS 삭제" : "건의 삭제"}
            </Button>
          ) : null}
          <Button onClick={(evt) => props.onList()} className="btn-set-cancel">
            AS 목록
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab02;
