// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Gnb0503 from "./presenter";

class Container extends Component {
  static propTypes = {
    preferences: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      tabKey: 1,
      item: {},
    };
  }

  setActiveTab = (tabKey, item) => {
    // console.log("- setActiveTab", tabKey, item);

    this.setState({ tabKey, item });
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount() {
    let { item } = this.props;

    if (item) {
      this.setActiveTab(2, item);
    }
  }

  render() {
    return <Gnb0503 {...this.props} {...this.state} closeModal={this.closeModal} changeTab={this.setActiveTab} />;
  }
}

export default withSnackbar(Container);
