// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as asAction } from "actions/as";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, as } = state;

  return {
    auth,
    as,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAs: (place_id, between, begin, end) => dispatch(asAction.getAsList(place_id, between, begin, end)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
