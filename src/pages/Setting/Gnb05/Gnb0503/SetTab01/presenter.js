// imports.
import React from "react";

import { FormGroup, FormControl, Table, ButtonToolbar, Button } from "react-bootstrap";
import { SingleDatePicker } from "react-dates";
import SearchInput from "components/FormInputs/SearchInput";

import cx from "classnames";
import moment from "moment";
import { keyToValue } from "constants/key-map";

// styles.
import "./styles.scss";

const SetTab01 = (props) => {
  let column = ["type", "state", "title", "user_id", "reg_date", "action_date", "mod_date"];
  let title = ["타입", "상태", "제목", "등록자", "등록일자", "조치일자", "수정일자"];

  return (
    <div>
      <div className="setting-form" id="gnb050301">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>AS/건의 목록</h2>
              <div className="form-content">
                <FormGroup>
                  <div className="date-set">
                    <SingleDatePicker
                      id="startsOnDateInput-1"
                      placeholder="검색 시작일 선택"
                      displayFormat="YYYY-MM-DD"
                      date={props.begin.date}
                      onDateChange={(date) => props.onDataChange({ begin: { date } })}
                      focused={props.focused1}
                      onFocusChange={({ focused: focused1 }) => props.onFocusChange({ focused1 })}
                      isOutsideRange={(day) => moment().add(-1, "year").isSameOrAfter(day)}
                      disabled={false}
                    />
                    ~
                    <SingleDatePicker
                      id="endsOnDateInput-2"
                      placeholder="검색 종료일 선택"
                      displayFormat="YYYY-MM-DD"
                      date={props.end.date}
                      onDateChange={(date) => props.onDataChange({ end: { date } })}
                      focused={props.focused2}
                      onFocusChange={({ focused: focused2 }) => props.onFocusChange({ focused2 })}
                      isOutsideRange={(day) => moment(props.begin.date).isAfter(day)}
                    />
                  </div>
                  <FormControl componentClass="select" className="wd100" value={props.selType} onChange={(evt) => props.onTypeSelected(Number(evt.target.value))}>
                    <option value="">타입 선택</option>
                    <option value="1">AS 요청</option>
                    <option value="2">건의 사항</option>
                  </FormControl>
                  <FormControl componentClass="select" className="wd100" value={props.selState} onChange={(evt) => props.onStateSelected(Number(evt.target.value))}>
                    <option value="">진행 상태</option>
                    <option value="1">등록</option>
                    <option value="2">확인</option>
                    <option value="3">처리중</option>
                    <option value="4">취소</option>
                    <option value="9">처리완료</option>
                  </FormControl>
                  {/* <FormControl
                    className="search-text"
                    type="text"
                    placeholder="제목, 내용 검색"
                    value={String(props.searchText || "").replace(/\B(?=(\d{4})+(?!\d))/g, "-")}
                    onChange={evt => props.onTextSearch(evt.target.value)}
                    maxLength={20}
                  /> */}
                  <SearchInput colWidth="col-md-3" type="text" placeholder="검색어 입력" value={props.searchText} onSearch={(value) => props.onTextSearch(value)} maxLength={20} />
                </FormGroup>
                <div className="search-result">
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          {column.map((col, i) => (
                            <th key={i} className={cx(props.sort === col ? "on" : "off")} onClick={(evt) => props.onSortClick(col)}>
                              {title[i]}
                              <b className={cx("caret", props.reverse ? "desc" : "asc")} />
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {props.list.map((item, i) => (
                          <tr key={i} onClick={(evt) => props.onItemClick(item)}>
                            <td className={cx(item.type === 1 && "as")}>{keyToValue("as", "type", item.type)}</td>
                            <td className={cx(item.type === 9 && "done")}>{keyToValue("as", "state", item.state)}</td>
                            <td>{item.title}</td>
                            <td>{item.user_id}</td>
                            <td>{moment(item.reg_date).format("YYYY-MM-DD HH:mm")}</td>
                            <td>{item.action_date ? moment(item.action_date).format("YYYY-MM-DD HH:mm:ss") : null}</td>
                            <td>{item.mod_date ? moment(item.mod_date).format("YYYY-MM-DD HH:mm:ss") : null}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
            닫기
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab01;
