// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

import _ from "lodash";

import Swal from "sweetalert2";

// UI for notification
import { withSnackbar } from "notistack";
import { fileDownload } from "utils/file-util";

// Components.
import SetTab02 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    notice: PropTypes.object.isRequired,
    changeTab: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      modify: {
        id: 0,
        type: 1,
        important: 1,
        title: "",
        content: "",
      },
      fileList: [],
    };
  }

  // 권한 체크.
  checkAuth = (type) => {
    let { modify } = this.state;

    const {
      auth: {
        user: { id, level },
      },
    } = this.props;

    // 본인 또는 매니저 권한 이상만 수정 가능.
    if (modify.user_id !== id && level > 2) {
      Swal.fire({
        icon: "error",
        title: "권한 공지",
        html: type + " 권한이 없습니다.",
        showCloseButton: true,
        confirmButtonText: "확인",
      });
      return false;
    }

    return true;
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value, save) => {
    let { modify } = this.state;

    // 아이템 정보 변경.
    modify[name] = !value || isNaN(value) || name.lastIndexOf("_id") === -1 ? value : Number(value);

    // console.log("- handleInputChange name", name, "value", modify[name]);

    this.setState({
      modify,
    });
  };

  onSave = () => {
    let { modify } = this.state;
    const {
      preferences: { speed_checkin },
    } = this.props;

    // 수정/삭제 는 등록자 보다 상위 권한만 가능
    if (modify.id && !this.checkAuth("수정")) return;

    let validate = "";
    if (!modify.title) validate = "제목을 입력해 주세요.";
    if (!modify.content && !validate) validate = "내용을 입력해 주세요.";

    if (validate) {
      Swal.fire({
        icon: "error",
        title: "입력 확인",
        html: validate,
        showCloseButton: true,
        confirmButtonText: "확인",
      });
      return false;
    }

    Swal.fire({
      icon: "info",
      title: "공지 " + (!modify.id ? "등록" : "수정") + " 확인",
      html: "공지를 " + (!modify.id ? "등록" : "수정") + " 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        if (!modify.id) {
          this.props.handleNewNoticePlace({ notice: modify }).then((res) => {
            this.props.changeTab(1, {}); // 목록 탭으로 전환.
          });
        } else {
          this.props.handleSaveNoticePlace(modify.id, { notice: modify }).then((res) => {
            this.props.changeTab(1, {}); // 목록 탭으로 전환.
          });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onDelete = () => {
    let { modify } = this.state;
    const {
      preferences: { speed_checkin },
    } = this.props;

    // 수정/삭제 는 등록자 보다 상위 권한만 가능
    if (!this.checkAuth("삭제")) return;

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: "공지를 삭제 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "취소",
    }).then((result) => {
      if (result.value) {
        this.props.handleDeleteNoticePlace(modify.id).then((res) => {
          if (res) {
            if (!speed_checkin) {
              Swal.fire("삭제 완료", "공지 삭제가 완료 되었습니다", "success").then((result) => {
                this.props.changeTab(1, {});
              });
            } else {
              this.props.changeTab(1, {});
            }
          }
        });
      }
    });
  };

  onList = () => {
    // 목록 탭으로 전환.
    this.props.changeTab(1, {});
  };

  clickDownload = (file) => {
    const {
      auth: { token },
    } = this.props;

    let { id, name } = file;

    console.log("- clickDownload file", file);

    fileDownload(id, name, token);
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    let { modify } = this.state;

    const {
      auth: {
        user: { id, place_id },
      },
      item,
    } = this.props;

    console.log("- componentDidMount item", item);

    if (item && item.id) {
      modify = _.cloneDeep(item);

      // 첨부 파일 조회. (구분 (1: 공지게시판, 2:업소게시판, 3:AS 게시판))
      let filter = `a.type=1 and a.type_id=${modify.id}`;
      this.props.initFiles(filter);
    }

    modify.place_id = place_id;
    modify.user_id = id;

    this.setState({
      modify,
    });
  };

  componentWillUnmount = () => {
    this.setState({
      dialog: false,
    });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.file && prevState.file !== nextProps.file) {
      let { list } = nextProps.file;
      console.log("- nextProps file", list);

      return { fileList: list };
    }

    return null;
  }

  render() {
    return (
      <SetTab02
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSave={this.onSave}
        onDelete={this.onDelete}
        onList={this.onList}
        clickDownload={this.clickDownload}
        checkAuth={this.checkAuth}
      />
    );
  }
}

export default withSnackbar(Container);
