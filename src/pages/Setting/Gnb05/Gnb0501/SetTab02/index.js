// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as smsAction } from "actions/sms";
import { actionCreators as fileAction } from "actions/file";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, notice, file } = state;

  return {
    auth,
    preferences: preferences.item,
    notice,
    file,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initFiles: (filter) => dispatch(fileAction.getFileList(filter)),

    handleSmsSendMsg: (sms) => dispatch(smsAction.newSms(sms)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
