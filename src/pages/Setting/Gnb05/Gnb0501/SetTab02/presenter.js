// imports.
import React from "react";

import { FormGroup, FormControl, ControlLabel, ButtonToolbar, Button } from "react-bootstrap";

import cx from "classnames";

import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css"; // 커스텀 라이브러리를 등록해 준다.

// styles.
import "./styles.scss";

const SetTab02 = (props) => {
  return (
    <div>
      <div className="setting-form" id="gnb050102">
        <div className="search-wrapper">
          <ul className="list-bul">
            <li>
              <h2>{props.modify.type === 1 ? "공지 상세" : "업데이트 상세"}</h2>
              <div className={cx("form-content")}>
                <div>
                  <FormGroup>
                    <ControlLabel>공지 타입</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="공지타입"
                      value={props.modify.type}
                      onChange={(evt) => props.handleInputChange("type", Number(evt.target.value))}
                      style={{
                        color: props.modify.type === 1 ? "" : "#0022ff",
                      }}
                      disabled={true}
                    >
                      <option value="1">공지 사항</option>
                      <option value="2">업데이트 알림</option>
                    </FormControl>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>공지 구분</ControlLabel>
                    <FormControl
                      componentClass="select"
                      placeholder="공지구분"
                      value={props.modify.important}
                      onChange={(evt) => props.handleInputChange("important", evt.target.value)}
                      style={{
                        color: props.modify.important === 1 ? "" : "#ff0000",
                      }}
                      disabled={true}
                    >
                      <option value={1}>일반</option>
                      <option value={2}>중요</option>
                    </FormControl>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>제목</ControlLabel>
                    <FormControl
                      type="text"
                      placeholder="제목을 입력해 주세요"
                      value={props.modify.title || ""}
                      onChange={(evt) => props.handleInputChange("title", evt.target.value)}
                      maxLength={100}
                      disabled={true}
                    />
                  </FormGroup>
                </div>
                <div>
                  <FormGroup>
                    <ControlLabel>내용</ControlLabel>
                    {/* <div draggable={false} className="editor" dangerouslySetInnerHTML={{ __html: props.modify.content }} /> */}
                    <ReactQuill theme={"snow"} className="editor" value={props.modify.content || ""} modules={{ toolbar: false }} readOnly={true} />
                  </FormGroup>
                </div>
                <div className="file-list">
                  <FormGroup>
                    <ControlLabel>첨부파일</ControlLabel>
                    {props.fileList.map((file, i) => (
                      <p key={i}>
                        <span>{file.name}</span>
                        <Button onClick={(evt) => props.clickDownload(file)} className="btn-set-confirm">
                          다운로드
                        </Button>
                      </p>
                    ))}
                  </FormGroup>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <ButtonToolbar className="btn-room btn-set-close">
          <Button onClick={(evt) => props.onList()} className="btn-set-cancel">
            공지 목록
          </Button>
        </ButtonToolbar>
      </div>
    </div>
  );
};

export default SetTab02;
