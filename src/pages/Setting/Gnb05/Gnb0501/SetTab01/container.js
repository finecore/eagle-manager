// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";

// UI for notification
import { withSnackbar } from "notistack";
import moment from "moment";
import _ from "lodash";
import format from "utils/format-util";

// Components.
import SetTab01 from "./presenter";

class Container extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    notice: PropTypes.object.isRequired, // 현재 객실 예약 정보.
    changeTab: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      list: [],
      sort: "reg_date",
      reverse: true,
      selType: 0,
      selImportant: 0,
      searchText: "",
      begin: { date: moment().add(-1, "month"), hour: 0, min: 0 },
      end: { date: moment(), hour: 23, min: 59 },
      focused1: null,
      focused2: null,
    };
  }

  onTypeSelected = (type) => {
    let { sort, reverse, selImportant, searchText } = this.state;
    let { notice } = this.props;

    console.log("-- onTypeSelected", { type, selImportant });

    let list = _.filter(notice.list, function (o) {
      return (selImportant ? o.important === selImportant : true) && (type ? o.type === type : true);
    });

    list = format.textFilter(list, searchText);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selType: type,
    });
  };

  onEmergencySelected = (important) => {
    let { sort, reverse, selType, searchText } = this.state;
    let { notice } = this.props;

    console.log("-- onEmergencySelected", { important, selType });

    let list = _.filter(notice.list, function (o) {
      return (important ? o.important === important : true) && (selType ? o.type === selType : true);
    });

    list = format.textFilter(list, searchText);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      selImportant: important,
    });
  };

  onTextSearch = (search_text) => {
    let { sort, reverse, selImportant, selType } = this.state;
    let { notice } = this.props;

    console.log("-- onTextSearch", search_text, notice.list);

    let list = _.filter(notice.list, function (o) {
      return (selImportant ? o.important === selImportant : true) && (selType ? o.type === selType : true);
    });

    list = format.textFilter(list, search_text);

    console.log("-- onTextSearch", list);

    if (sort) list = _.sortBy(list, [sort]);
    if (reverse) list = list.reverse();

    this.setState({
      list,
      searchText: search_text,
    });
  };

  onFocusChange = (focused) => {
    this.setState(focused);
  };

  onDataChange = (data) => {
    console.log("- onDataChange", JSON.stringify(data));

    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    const state = _.merge({}, this.state, data);

    this.setState(state, () => {
      const {
        begin: { date: bd },
        end: { date: ed },
      } = this.state;

      let between = "a.reg_date";
      let begin = moment(bd).format("YYYY-MM-DD HH:mm");
      let end = moment(ed).format("YYYY-MM-DD HH:mm");

      let filter = `(a.place_id=0 or a.place_id=${place_id}) and open=1 `;

      this.props.initNotices(filter, between, begin, end);
    });
  };

  onSortClick = (sorted) => {
    let { list, sort, reverse } = this.state;

    console.log("-- onSortClick", sorted, sort, reverse);

    reverse = sort === sorted ? !reverse : false;

    // 재 정렬.
    list = _.orderBy(list, sorted, !reverse ? "asc" : "desc");

    this.setState({
      sort: sorted,
      reverse,
      list,
    });
  };

  onItemClick = (item) => {
    console.log("-- onItemClick", item);

    // 상세 탭으로 전환.
    this.props.changeTab(2, item);
  };

  componentDidMount = () => {
    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    const {
      begin: { date: bd },
      end: { date: ed },
    } = this.state;

    let between = "a.reg_date";
    let begin = moment(bd).format("YYYY-MM-DD HH:mm");
    let end = moment(ed).format("YYYY-MM-DD HH:mm");

    let filter = `(a.place_id=0 or a.place_id=${place_id}) and open=1 `;

    this.props.initNotices(filter, between, begin, end);
  };

  componentWillUnmount = () => {};

  shouldComponentUpdate = (nextProps, nextState) => {
    // console.log("-- shouldComponentUpdate");
    return true;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.notice && nextProps.notice !== this.props.notice && nextProps.notice.list !== this.props.notice.list) {
      let { sort, reverse, selType, selImportant } = this.state;

      let list = _.filter(nextProps.notice.list, function (o) {
        return (selImportant ? o.important === selImportant : true) && (selType ? o.type === selType : true);
      });

      _.map(list, (item) => {
        const { id, mod_date, reg_date } = item;
        const name = "notice";

        // 알림 확인 저장.
        let lastDate = moment(mod_date || reg_date).format("YYYYMMDDHHmmss"); // 최종 업데이트 일자로 저장
        localStorage.setItem(`${name}_display_${id}`, lastDate);
        localStorage.setItem(`${name}_display_${id}_today`, moment().format("YYYYMMDD"));
      });

      if (sort) list = _.sortBy(list, [sort]);
      if (reverse) list = list.reverse();

      this.setState({
        list,
      });
    }
    return null;
  }

  render() {
    return (
      <SetTab01
        {...this.props}
        {...this.state}
        onTypeSelected={this.onTypeSelected}
        onEmergencySelected={this.onEmergencySelected}
        onTextSearch={this.onTextSearch}
        onItemClick={this.onItemClick}
        onSortClick={this.onSortClick}
        onFocusChange={this.onFocusChange}
        onDataChange={this.onDataChange}
      />
    );
  }
}

export default withSnackbar(Container);
