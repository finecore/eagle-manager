// imports.
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

// Actions.
import { actionCreators as noticeAction } from "actions/notice";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, notice } = state;

  return {
    auth,
    notice,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initNotices: (place_id, between, begin, end) => dispatch(noticeAction.getNoticeList(place_id, between, begin, end)),
  };
};

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Container));
