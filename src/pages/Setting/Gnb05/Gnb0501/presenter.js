// imports.
import React from "react";
import Modal from "react-awesome-modal";

// Components.
import SetTab01 from "./SetTab01";
import SetTab02 from "./SetTab02";

import { Tab, Nav, NavItem, Form } from "react-bootstrap";

// icons
import ListAlt from "@material-ui/icons/ListAlt";
// import Publish from "@material-ui/icons/Publish";
// import Edit from "@material-ui/icons/Edit";

// styles.
import "./styles.scss";

// functional component
const Gnb0501 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb0501">
          <h1>공지 게시판</h1>
          <Form className="setting-tab-scroll">
            <Tab.Container className="setting-form-tab" id="settingTab" defaultActiveKey={1} activeKey={props.tabKey} onSelect={(key) => props.changeTab(key, {})}>
              <div className="clearfix">
                <div className="setting-tabs">
                  <Nav bsStyle="pills" stacked>
                    <NavItem key={1} eventKey={1}>
                      <ListAlt />
                      공지 목록
                    </NavItem>
                    {/* <NavItem key={2} eventKey={2}>
                      {props.item.id ? <Edit /> : <Publish />}
                      {"공지 상세"}
                    </NavItem> */}
                  </Nav>
                </div>
                <div className="setting-form-wrapper" id="gnb0501">
                  <Tab.Content animation>
                    <Tab.Pane key={1} title="공지 목록" eventKey={1} mountOnEnter unmountOnExit>
                      <SetTab01 {...props} />
                    </Tab.Pane>
                    <Tab.Pane key={2} title="공지 상세" eventKey={2} mountOnEnter unmountOnExit>
                      <SetTab02 {...props} />
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </Form>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0501;
