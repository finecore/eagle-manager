// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";
import _ from "lodash";
import moment from "moment";

//point-popup
import { Form, Checkbox, FormControl, ControlLabel, Table, ButtonToolbar, Button } from "react-bootstrap";
import InputNumber from "rc-input-number";
import SearchInput from "components/FormInputs/SearchInput";

import format from "utils/format-util";

// styles.
import "./styles.scss";

// functional component
const Gnb0204 = (props) => {
  // let column = ["phone", "member_id", "point", "reg_date", "mod_date"];
  // let title = ["전화번호", "고객아이디", "포인트", "등록일시", "수정일시"];

  let column = ["phone", "point", "reg_date", "mod_date"];
  let title = ["휴대폰번호", "포인트", "등록일시", "수정일시"];

  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb02">
          <h1>포인트 관리</h1>
          <div className="setting-form-wrapper" id="gnb0204">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <div className="form-content clearfix">
                      <Checkbox
                        inline
                        className="checkbox04"
                        checked={props.selectedAll || false}
                        onChange={(evt) => props.onChecked(-1, evt.target.checked)}
                        disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                      >
                        <span />
                        전체선택
                      </Checkbox>
                    </div>
                    <div className="form-content clearfix point-search">
                      <SearchInput colWidth="col-mid-12" type="text" placeholder="휴대폰번호 입력" value={props.searchText} onSearch={(value) => props.onSearch(value)} maxLength={20} />
                    </div>
                    <div className="form-content clearfix point-add">
                      <Button onClick={(evt) => props.onAdd(evt)}>포인트 추가</Button>
                    </div>
                    <div className="form-content">
                      <div className="search-result">
                        <div className="table-wrapper">
                          <Table>
                            <thead>
                              <tr>
                                <th>선택</th>
                                {column.map((col, i) => (
                                  <th key={i} className={cx(props.sort === col ? "on" : "off")} onClick={(evt) => props.onSortClick(col)}>
                                    {title[i]}
                                    <b className={cx("caret", props.sorted[col] === false ? "reverse" : "")} />
                                  </th>
                                ))}
                                <th>삭제</th>
                              </tr>
                            </thead>
                            <tbody>
                              {_.map(props.list, (item, k) => (
                                <tr key={k} className={cx(props.selected[k] ? "on" : "off")}>
                                  <td>
                                    <Checkbox
                                      inline
                                      checked={props.selected[k] || false}
                                      onChange={(evt) => props.onChecked(k, evt.target.checked)}
                                      disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                                    >
                                      <span />
                                    </Checkbox>
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl type="text" placeholder="전화번호" value={format.toPhone(item.phone)} onChange={(evt) => props.handleInputChange(k, "phone", evt.target.value)} />
                                  </td>
                                  {/* <td onClick={evt => !props.selected[k] && props.onChecked(k, true)}>
                                    {props.member.list.length ? (
                                      <FormControl
                                        className={item.member_id ? "" : "off"}
                                        componentClass="select"
                                        placeholder="비회원"
                                        value={item.member_id || ""}
                                        onChange={evt => props.handleInputChange(k, "member_id", evt.target.value)}
                                      >
                                        <option value="">비회원</option>
                                        {item.member_id ? <option value={item.member_id}>{item.member_id}</option> : null}
                                        {_.map(props.members, (v, k) => (
                                          <option key={k} value={v.id}>
                                            {v.id}
                                          </option>
                                        ))}
                                      </FormControl>
                                    ) : (
                                      <FormControl className={"off"} type="text" placeholder="비회원" disabled={true} />
                                    )}
                                  </td> */}
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <span className="numeric-wrap">
                                      <InputNumber
                                        className="form-control"
                                        step={10}
                                        min={0}
                                        value={item.point}
                                        onChange={(value) => props.handleInputChange(k, "point", value)}
                                        formatter={(value) => format.toMoney(value)}
                                      />
                                    </span>
                                  </td>
                                  <td>
                                    <ControlLabel>{moment(item.reg_date).format("YYYY-MM-DD HH:mm:ss")}</ControlLabel>
                                  </td>
                                  <td>
                                    <ControlLabel>{item.mod_date ? moment(item.mod_date).format("YYYY-MM-DD HH:mm") : ""}</ControlLabel>
                                  </td>
                                  <td>
                                    <Button className="del" onClick={(evt) => props.onDelete(k)} disabled={!props.selected[k]}>
                                      <span className="mdi mdi-delete" />
                                    </Button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        </div>
                      </div>

                      <div className="point-control">
                        <span>선택한 고객의 포인트를</span>
                        <FormControl type="text" placeholder="포인트 입력" value={props.point} onChange={(evt) => props.onSelPointChange(Number(evt.target.value || 0))} />
                        <Button
                          className="increase"
                          onClick={(evt) => props.onSelDo("increase")}
                          disabled={
                            _.filter(props.selected, (sel) => sel === true).length === 0 || props.point === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                          }
                        >
                          + Point
                        </Button>
                        <Button
                          className="decrease"
                          onClick={(evt) => props.onSelDo("decrease")}
                          disabled={
                            _.filter(props.selected, (sel) => sel === true).length === 0 || props.point === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                          }
                        >
                          - Point
                        </Button>
                        <Button
                          onClick={(evt) => props.onSelDo("set")}
                          disabled={
                            _.filter(props.selected, (sel) => sel === true).length === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                          }
                        >
                          Point로 변경
                        </Button>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <ButtonToolbar className="btn-point">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
                <Button
                  onClick={(evt) => props.onSave(evt)}
                  disabled={
                    _.filter(props.selected, (sel) => sel === true).length === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                  }
                >
                  저장
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0204;
