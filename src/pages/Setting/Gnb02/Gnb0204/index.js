// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as mileageAction } from "actions/mileage";
import { actionCreators as memberAction } from "actions/member";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, mileage, member, preferences, error } = state;
  return {
    auth,
    user: auth.user,
    mileage,
    member,
    preferences: preferences.item,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllMileages: (palceId) => dispatch(mileageAction.getMileageList(palceId)),

    initAllMembers: (palceId) => dispatch(memberAction.getMemberList(palceId)),

    handleNewMileage: (mileage) => dispatch(mileageAction.newMileage(mileage)),

    handleSaveMileage: (palceId, phone, mileage) => dispatch(mileageAction.putMileage(palceId, phone, mileage)),

    handleDeleteMileage: (id, userId) => dispatch(mileageAction.delMileage(id, userId)),

    handleIncreaseMileage: (place_id, phone, user_id, point) => dispatch(mileageAction.increaseMileage(place_id, phone, user_id, point)),

    handleDecreaseMileage: (place_id, phone, user_id, point) => dispatch(mileageAction.decreaseMileage(place_id, phone, user_id, point)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
