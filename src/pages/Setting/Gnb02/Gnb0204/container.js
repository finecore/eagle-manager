// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import format from "utils/format-util";

// UI for notification
import { withSnackbar } from "notistack";

import Swal from "sweetalert2";

// Components.
import Gnb0204 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      listOrg: [],
      members: [],
      selectedAll: false,
      sorted: { id: false },
      sort: "id",
      selected: [],
      searchText: "",
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      point: 0,
      selDo: 0, // 선택 작업.
    };
  }

  static props = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    mileage: PropTypes.object.isRequired,
    member: PropTypes.object.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (idx, name, value) => {
    let { list } = this.state;

    if (!value) value = null;

    // 뷰 아이템 정보 변경.
    list[idx][name] = !value || isNaN(value) ? value : Number(value);

    console.log("- handleInputChange name", name, "value", value);

    // 남은 고객
    if (name === "member_id") {
      let members = _.filter(this.props.member.list, (item) => !_.find(list, { member_id: item.id }));

      this.setState({
        members,
      });
    }

    this.setState({
      list,
    });
  };

  onSearch = (search_text) => {
    let { listOrg } = this.state;

    search_text = String(search_text).replace(/[^\d]/g, "");

    console.log("-- onSearch", search_text);

    let list = search_text
      ? _.filter(listOrg, function (o) {
          return (o.phone || "").replace(/[^\d]/g, "").indexOf(search_text) > -1;
        })
      : listOrg;

    this.setState({
      list,
      searchText: search_text,
    });
  };

  onSortClick = (sort) => {
    let { list, sorted } = this.state;

    sorted[sort] = sorted[sort] ? !sorted[sort] : true;

    console.log("-- onSortClick", sort, sorted[sort]);

    list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

    this.setState({
      list,
      listOrg: list,
      sort,
      sorted,
      selected: [],
    });
  };

  onChecked = (idx, checked) => {
    console.log("-- onChecked", idx, checked);

    let { selected, selectedAll } = this.state;

    if (idx > -1) {
      selected[idx] = checked;
    } else {
      _.map(selected, (v, k) => {
        selected[k] = checked;
      });
    }

    selectedAll = _.filter(selected, (sel) => !sel).length ? false : true;

    this.setState({
      selected,
      selectedAll,
    });
  };

  onDelete = (idx) => {
    console.log("-- onDelete", idx);

    const { list } = this.state;
    let mileage = list[idx];

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: `${mileage.phone} [${mileage.member_id}] 고객님의 포인트를 <br/>정말 <font color='red'>삭제</font> 하시겠습니까?`,
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "삭제",
      cancelButtonText: "취소",
      focusConfirm: false,
      footer: "삭제 시 마일리지 포인트도 모두 삭제 됩니다.",
    }).then((result) => {
      if (result.value) {
        const { user } = this.props.auth;

        this.props.handleDeleteMileage(mileage.id, user.id);

        this.setState({
          dialog: true,
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onAdd = (phone, confirm) => {
    console.log("-- onAdd", phone, confirm);

    if (!confirm || !phone) {
      Swal.fire({
        icon: "info",
        title: "추가 입력",
        html: `추가할 고객의 전화번호를 입력해주세요`,
        input: "text",
        inputPlaceholder: "휴대전화 번호를 입력해주세요",
        inputAttributes: {
          maxlength: 13,
        },
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: "추가",
        cancelButtonText: "취소",
      }).then((result) => {
        console.log("- result", result);

        if (result.dismiss === Swal.DismissReason.cancel) {
        } else {
          if (!result.value) {
            Swal.fire({
              icon: "error",
              title: "입력 오류",
              html: `휴대전화 번호를 입력해 주세요`,
            }).then((result) => {
              this.onAdd(result.value, false);
            });
          } else if (!format.isPhone(result.value)) {
            Swal.fire({
              icon: "error",
              title: "입력 오류",
              html: `올바른 휴대전화 번호를 입력해 주세요`,
            }).then((result) => {
              this.onAdd(result.value, false);
            });
          } else {
            this.onAdd(result.value, true);
          }
        }
      });
    } else {
      const {
        user: { place_id, id },
      } = this.props.auth;

      let mileage = {
        place_id,
        phone: format.toNumber(phone),
        user_id: id,
        type: 1, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
      };

      this.props.handleNewMileage({ mileage });

      this.setState({
        dialog: true,
      });
    }
  };

  onSelDo = (mode) => {
    const {
      user,
      mileage: { list: _orgList },
    } = this.props;
    const { selected, list, point } = this.state;

    console.log("-- onSelDo", mode);

    let doTxt = mode === "increase" ? "증가" : mode === "decrease" ? "차감" : "로 변경";

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: `선택한 고객의 포인트를 모두 ${point} Point ${doTxt} 하시겠습니까?`,
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "예",
      cancelButtonText: "아니오",
      focusConfirm: false,
    }).then((result) => {
      if (result.value) {
        _.map(selected, (sel, k) => {
          if (sel) {
            let { id, place_id, phone, member_id, point } = list[k];

            let nPoint = this.state.point;

            point = mode === "increase" ? point + nPoint : mode === "decrease" ? point - nPoint : nPoint;
            point = point < 0 ? 0 : point;

            const org = _.find(_orgList, { id }) || {};

            if (org.point !== point || org.phone !== phone) {
              const mileage = {
                id,
                place_id,
                phone: format.toNumber(phone),
                member_id,
                user_id: user.id,
                point,
                type: 1, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
              };

              this.props.handleSaveMileage(place_id, phone, { mileage });
            }
          }
        });

        this.setState({
          selDo: 0,
          point: 0,
          selectedAll: false,
          dialog: true,
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onSave = () => {
    const {
      user,
      mileage: { list: _orgList },
    } = this.props;
    const { list, selected } = this.state;

    console.log("-- onSave");

    Swal.fire({
      icon: "info",
      title: "저장 확인",
      html: `포인트 설정을 저장 하시겠습니까?`,
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        _.map(selected, (sel, k) => {
          if (sel) {
            const { id, place_id, phone, member_id, point } = list[k];

            const org = _.find(_orgList, { id }) || {};

            if (org.point !== point || org.phone !== phone) {
              const mileage = {
                id,
                place_id,
                phone: format.toNumber(phone),
                member_id,
                user_id: user.id,
                point,
                type: 1, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
              };

              this.props.handleSaveMileage(place_id, phone, { mileage });
            }
          }
        });

        this.setState({
          selDo: 0,
          point: 0,
          selectedAll: false,
          dialog: true,
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  };

  onCancel = () => {
    this.closeModal();
  };

  onSelDoChange = (selDo) => {
    console.log("- onSelDoChange", selDo);

    this.setState({
      selDo,
    });
  };

  onSelPointChange = (point) => {
    console.log("- onSelPointChange", point);

    this.setState({
      point,
    });
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      user: { place_id },
    } = this.props.auth;

    this.props.initAllMembers(place_id);
    this.props.initAllMileages(place_id);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        dialog: false,
      });
    }

    if (nextProps.mileage && nextProps.mileage !== this.props.mileage) {
      let { list } = nextProps.mileage;
      let { sort, sorted } = this.state;

      let selected = [];

      _.map(list, (v, k) => {
        selected.push(false);
      });

      // 재 정렬.
      list = _.orderBy(list, sort, sorted[sort] ? "asc" : "desc");

      this.setState({
        list: _.cloneDeep(list),
        listOrg: _.cloneDeep(list),
        selected,
        dialog: false,
        confirm: false,
      });

      // 회원 목록 재설정.
      setTimeout(() => {
        let members = _.filter(this.props.member.list, (item) => !_.find(list, { member_id: item.id }));

        this.setState({
          members,
        });
      }, 200);
    }
  }

  render() {
    return (
      <Gnb0204
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSortClick={this.onSortClick}
        onChecked={this.onChecked}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSelDo={this.onSelDo}
        onSave={this.onSave}
        onSearch={this.onSearch}
        onCancel={this.onCancel}
        onSelPointChange={this.onSelPointChange}
        onSelDoChange={this.onSelDoChange}
      />
    );
  }
}

export default withSnackbar(Container);
