// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";
import date from "utils/date-util";
import { keyToValue, displayState } from "constants/key-map";

// UI for notification
import { withSnackbar } from "notistack";

import swal from "sweetalert";
import Swal from "sweetalert2";

// Components.
import Gnb0203 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: {},
      disabled: [false, false, false, false, false, false, false],
      list: [],
      sort: "name",
      reverse: false,
      notice: null, // 객실 표시.
      room_type_id: 0,
    };
  }

  static props = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    roomType: PropTypes.object.isRequired,
    roomState: PropTypes.array.isRequired,
    roomSale: PropTypes.array.isRequired,
    seasonPremiums: PropTypes.array.isRequired,
  };

  // 객실 타입 변경.
  onRoomTypeSelect = (room_type_id) => {
    console.log("-- onSelect", room_type_id);

    let { roomState } = this.props;
    let { sort, reverse } = this.state;

    let list = roomState.list;

    if (room_type_id) {
      room_type_id = Number(room_type_id);

      list = _.filter(roomState.list, function (o) {
        return o.room_type_id === room_type_id;
      });
    }

    if (sort) list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

    this.setState({
      room_type_id,
      list,
    });
  };

  onAllClick = (mode, checked) => {
    console.log("-- onAllClick", mode, checked);

    let { selected, list } = this.state;

    let _selected = {};

    // 전체 선택/해제.
    if (mode === 1) {
      _.map(list, (v, k) => {
        if (checked) _selected[v.id] = true;
      });
    }
    // 선택 반전.
    else {
      _.map(list, (v, k) => {
        if (!selected[v.id]) _selected[v.id] = true;
      });
    }

    // console.log("-- selected", _selected);

    this.setState({
      selected: _selected,
    });
  };

  onChecked = (id) => {
    let { selected, list } = this.state;

    console.log("-- onChecked", id, selected[id]);

    if (!selected[id]) selected[id] = true;
    else delete selected[id];

    this.setState({
      selected,
    });
  };

  onSortClick = (sorted) => {
    let { list, sort, reverse } = this.state;

    reverse = sort === sorted ? !reverse : false;

    // 재 정렬 (객실명은 한글 제외 하고 정렬)
    list = _.orderBy(list, (v) => (sorted === "name" ? Number(v[sorted].replace(/[^\d]/g, "")) : v[sorted]), !reverse ? "asc" : "desc");

    this.setState({
      sort: sorted,
      reverse,
      list,
    });
  };

  onChangeState = (changeState, value, id, room_state, room_sale) => {
    console.log("- onChangeSale", { changeState, value, id, room_state, room_sale });

    const {
      preferences: {
        day_sale_end_time,
        stay_time_type,
        stay_time,
        stay_time_weekend,
        rent_time_am,
        rent_time_pm,
        rent_time_am_weekend,
        rent_time_pm_weekend,
        temp_key_1, // 사용중
        temp_key_2, // 외출중
        temp_key_3, // 공실
        temp_key_4, // 청소중
        temp_key_5, // 청소대기
      },
      seasonPremiums,
      user,
    } = this.props;

    let roomState = _.find(this.state.list, { id: Number(id) });

    console.log("- roomState", roomState);

    if (!roomState) return;

    let roomSale = _.find(this.props.roomSale.list, { id: roomState.room_sale_id }) || {}; // 현재 목록에서 조회.

    if (changeState === 0) {
      // 공실 처리 => state.sale = 0 , state.room_sale_id = 0
      if (!roomSale.check_out) room_sale["check_out"] = moment().format("YYYY-MM-DD HH:mm:ss"); // 체크아웃
      if (roomSale.state === "A") room_sale["state"] = "C"; // 매출 마감.

      // 무조건 공실 처리
      room_state["sale"] = 0; // 판매 상태 (0:없음 1:숙박 2:대실 3:장기)

      if (roomState.room_sale_id) room_state["room_sale_id"] = null; // 매출 키.
      if (roomState.outing) room_state["outing"] = 0; // 외출 없음.
      if (roomState.key !== 3 && roomState.clean) room_state["clean"] = 0; // 청소 완료.
      if (roomState.key > 3) room_state["key"] = 0; // 키 초기화
      if (roomState.car_call) room_state["car_call"] = 0; // 차량 호출 초기화.
      if (roomState.emerg) room_state["emerg"] = 0; // 비상 호출 초기화.

      // 차량 번호 표시 제거.
      if (roomState.notice && roomSale.car_no && roomState.notice.indexOf(roomSale.car_no) > -1) {
        const regex = new RegExp(roomSale.car_no, "g");
        room_state["notice"] = roomState.notice ? roomState.notice.replace(regex, "").replace(/^\s*/, "") : "";
      }

      // 공실시 온도
      room_state["air_set_temp"] = temp_key_3;
      if (roomState.key > 3) room_state["main_relay"] = 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
      if (!roomState.use_auto_power_off) room_state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.
    }
    // 입실 처리 => state.sale > 0 , state.room_sale_id > 0
    else if (changeState === 1) {
      room_state["sale"] = value; // 숙박형태

      if (roomState.clean) room_state["clean"] = 0; // 청소 요청 없음.
      if (roomState.key > 3) room_state["key"] = 0; // 키 초기화
      if (!roomState.use_auto_power_off) room_state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      // 사용중 온도
      room_state["air_set_temp"] = temp_key_1;
    }
    // 외출 처리
    else if (changeState === 2) {
      if (roomState.outing !== value) room_state["outing"] = value; // 외출/외출복귀

      // 외출시 / 외출복귀
      room_state["air_set_temp"] = value === 1 ? temp_key_2 : temp_key_1;
    }
    // 청소요청 처리
    else if (changeState === 3) {
      if (!roomState.clean) room_state["clean"] = 1; // 청소 지시.
      if (roomState.key > 3) room_state["key"] = 0; // 키 초기화

      // 청소대기 온도
      room_state["air_set_temp"] = temp_key_5;
    }
    // 청소중 처리
    else if (changeState === 4) {
      // 청소중 온도
      room_state["air_set_temp"] = temp_key_4;
    }
    // 청소완료 처리
    else if (changeState === 5) {
      if (roomState.clean) room_state["clean"] = 0; // 청소 요청 없음.

      // 사용중 / 공실시 온도
      room_state["air_set_temp"] = roomState.sale > 0 ? temp_key_1 : temp_key_3;
    }
    // 퇴실 처리
    else if (changeState === 6) {
      if (!roomSale.check_out) room_sale["check_out"] = moment().format("YYYY-MM-DD HH:mm:ss"); // 체크아웃
      if (roomSale.state === "A") room_sale["state"] = "C"; // 매출 마감.

      if (roomState.outing) room_state["outing"] = 0; // 외출 없음.
      if (!roomState.clean) room_state["clean"] = 1; // 청소 지시.

      if (roomState.key > 3) room_state["main_relay"] = 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
      if (!roomState.use_auto_power_off) room_state["use_auto_power_off"] = 1; // 자동 전원 차단 사용.

      // 청소대기 온도
      room_state["air_set_temp"] = temp_key_5;
    }
    // 입실취소 => state.sale = 0 , state.room_sale_id = 0
    else if (changeState === 7) {
      if (roomSale.state === "A") room_sale["state"] = "B"; // 매출 취소.
      if (roomSale.room_reserv_id) room_sale["room_reserv_id"] = 0; // 예약 키.

      if (roomState.sale) room_state["sale"] = 0; // 공실
      if (roomState.room_sale_id) room_state["room_sale_id"] = null; // 매출 키.
      if (roomState.outing) room_state["outing"] = 0; // 외출 없음.
      if (!roomState.clean) room_state["clean"] = 1; // 청소 지시.
      if (roomState.car_call) room_state["car_call"] = 0; // 차량 호출 초기화.
      if (roomState.emerg) room_state["emerg"] = 0; // 비상 호출 초기화.

      // 차량 번호 표시 제거.
      if (roomState.notice && roomSale.car_no && roomState.notice.indexOf(roomSale.car_no) > -1) {
        const regex = new RegExp(roomSale.car_no, "g");
        room_state["notice"] = roomState.notice ? roomState.notice.replace(regex, "").replace(/^\s*/, "") : "";
      }

      // 청소대기 온도
      room_state["air_set_temp"] = temp_key_5;
      room_sale["rollback"] = 1; // 입실취소.
    }
    // 퇴실 취소
    else if (changeState === 8) {
      if (!roomState.check_out) room_sale["check_out"] = null; // 체크아웃
      if (roomState.state !== "A") room_sale["state"] = "A"; // 매출 상태.
      if (roomState.key !== 3 && roomState.clean) room_state["clean"] = 0; // 청소 없음.
      room_sale["rollback"] = 2; // 퇴실 취소.
    }
    // 객실 표시 내용.
    else if (changeState === 9) {
      room_state["notice"] = this.state.notice || null;
      room_state["notice_display"] = value; // 객실 표시 여부 (0:표시 안함, 1:표시)
    }
    // 무인 숙박 판매 여부.
    else if (changeState === 10) {
      room_state["isc_sale_1"] = value;
    }
    // 무인 대실 판매 여부.
    else if (changeState === 11) {
      room_state["isc_sale_2"] = value;
    }
    // 무인 예약 판매 여부.
    else if (changeState === 12) {
      room_state["isc_sale_3"] = value;
    }
    // 사용중 온도 설정
    else if (changeState === 13) {
      room_state["temp_key_1"] = value;
    }
    // 외출중 온도 설정
    else if (changeState === 14) {
      room_state["temp_key_2"] = value;
    }
    // 청소대기 온도 설정
    else if (changeState === 15) {
      room_state["temp_key_5"] = value;
    }
    // 청소중 온도 설정
    else if (changeState === 16) {
      room_state["temp_key_4"] = value;
    }
    // 공실 온도 설정
    else if (changeState === 17) {
      room_state["temp_key_3"] = value;
    }

    console.log("- save room_state", room_state);
    console.log("- save room_sale", room_sale);

    const isNew = room_state.sale > 0 && !roomState.sale; // 매출 신규 (매출 변경 시 체크)

    console.log("- isNew", isNew);

    // 매출 신규.
    if (isNew) {
      // 매출 정보.
      room_sale.channel = "web"; // 매출 채널 (web, isc, api)
      room_sale.room_id = roomState.room_id;
      room_sale.user_id = user.id; // 현재 직원 id
      room_sale.state = "A"; // 정상
      room_sale.stay_type = room_state.sale || roomState.sale; // 숙박 형태.
      room_sale.check_in = moment().format("YYYY-MM-DD HH:mm"); // 입실 시 체크인.
      room_sale.check_in_exp = moment().format("YYYY-MM-DD HH:mm"); // 입실 시 체크인.

      // 요금 설정.
      let { default_fee, add_fee, option_fee, fee, season, usePeriod, stayTime, rentTime } = this.getFee(roomState, room_sale);

      room_sale.default_fee = default_fee; // 요금 설정.
      room_sale.add_fee = add_fee;
      room_sale.option_fee = option_fee;

      // 입실 시 현금/카드 선택 금액 설정 (추가요금 적용)
      if (room_sale.pay_cash_amt) room_sale.pay_cash_amt = room_sale.default_fee + room_sale.add_fee;
      else if (room_sale.pay_card_amt) room_sale.pay_card_amt = room_sale.default_fee + room_sale.add_fee;
      else room_sale.pay_cash_amt = room_sale.default_fee + room_sale.add_fee;

      if (!room_sale.check_out_exp) {
        let hour = Number(moment().format("HH"));

        console.log("- hour ", { stayTime, rentTime, hour }, stayTime > hour);

        // 퇴실 시간.
        room_sale.check_out_exp =
          room_sale.stay_type === 2
            ? moment().add(rentTime, "hour").format("YYYY-MM-DD HH:mm:ss")
            : stay_time_type === 0 // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
            ? moment().add(stayTime, "hour").format("YYYY-MM-DD HH:mm:ss")
            : moment()
                .hour(stayTime)
                .minute(0)
                .add(stayTime > hour ? 0 : 1, "day") // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
                .format("YYYY-MM-DD HH:mm:ss");
      }

      room_sale.check_out_exp = moment(room_sale.check_out_exp).format("YYYY-MM-DD HH:mm");

      console.log("- newRoomSale", room_sale);

      // 새로운 매출 발생.(신규 매출 id -> room_state room_sale_id 에 매핑)
      this.props.handleNewRoomSale({ room_sale }, ({ info, error }) => {
        if (error) {
          Swal.fire({
            icon: "warning",
            title: "체크인 오류",
            html: error.message,
            confirmButtonText: "확인",
          });
          return;
        } else if (!info.insertId) {
          Swal.fire({
            icon: "warning",
            title: "체크인 오류",
            html: "체크인 중 매출 등록이 정상적으로 되지 않았습니다.",
            confirmButtonText: "확인",
          });
          return;
        }

        // 매출 id
        room_state.room_sale_id = info.insertId;

        console.log("- handleSaveRoomState", roomState.room_id, room_state);

        // 매출 정보를 객실에 연결.
        this.props.handleSaveRoomState(roomState.room_id, { room_state }, (info) => {});
      });
    } else {
      // 매출 수정.
      if (Object.keys(room_sale).length) {
        const {
          preferences: { place_id },
          user,
        } = this.props;

        if (!roomSale.id) return false;

        room_sale.id = roomSale.id;
        room_sale.room_id = roomSale.room_id;
        room_sale.user_id = user.id; // 현재 직원 id

        console.log("- handleSaveRoomSale", room_sale.id, room_sale);
        if (room_sale.id)
          this.props.handleSaveRoomSale(room_sale.id, { room_sale }, ({ info, error }) => {
            if (!error) {
              const { rollback } = room_sale;

              // 입실 취소 시 예약 상태 롤백.
              if (roomSale.room_reserv_id && rollback === 1) {
                let room_reserv = { state: "A", room_id: null }; // 예약 상태 (A: 정상, B: 취소, C: 입실)

                this.props.handleSaveRoomReserv(roomSale.room_reserv_id, {
                  room_reserv,
                });
              }

              // 입실 취소 시
              if (roomSale.phone && rollback === 1) {
                if (roomSale.pay_point_amt) this.props.handleIncreaseMileage(place_id, roomSale.phone, user.id, roomSale.pay_point_amt, rollback);
                setTimeout(() => {
                  if (roomSale.save_point) this.props.handleDecreaseMileage(place_id, roomSale.phone, user.id, roomSale.save_point, rollback);
                }, 100);
              }
            }
          });
      }

      // 상태 수정.
      if (Object.keys(room_state).length) {
        room_state.room_id = roomState.room_id;
        room_state.user_id = user.id; // 현재 직원 id

        console.log("- handleSaveRoomState", roomState.room_id, room_state);
        this.props.handleSaveRoomState(roomState.room_id, { room_state }, () => {
          // 객실 표시 내용 삭제 시 입력 값도 삭제.
          if (changeState === 9 && value === 0) this.setState({ notice: null });
        });
      }
    }
  };

  onSelDo = (changeState, type) => {
    const {
      preferences: {
        temp_key_1, // 사용중
        temp_key_2, // 외출중
        temp_key_3, // 공실
        temp_key_4, // 청소중
        temp_key_5, // 청소대기
        use_sale_cancel_comment, // 입실 취소 사유 입력
        cancel_time,
      },
      roomSale,
    } = this.props;

    let { selected, list } = this.state;

    console.log("- onSelDo", changeState, type);

    if (_.isEmpty(selected)) {
      Swal.fire({
        icon: "warning",
        title: "선택 확인",
        html: "객실을 먼저 선택해 주세요.",
        confirmButtonText: "확인",
      });
      return;
    }

    let checklist = {};

    _.map(selected, (sel, id) => {
      if (sel) {
        let roomState = _.find(list, { id: Number(id) });

        // console.log("-- roomState", roomState);

        if (changeState === 0) {
          // 공실 안됨.
          if (roomState.sale === 0 || (roomState.sale > 0 && !roomState.check_out)) sel = false;
        } else if (changeState === 1) {
          // 입실 안됨.
          if (roomState.sale > 0 || roomState.check_in) sel = false;
          if (roomState.key > 0 && roomState.key < 4) sel = false; // 키 있으면 안됨.
          if (roomState.clean === 1) sel = false; // 청소 요청 시 안됨.
        } else if (changeState === 2) {
          // 외출/복귀 안됨.
          if (roomState.outing === 1 && type === 1) sel = false;
          else if (roomState.outing === 0 && type === 0) sel = false;
        } else if (changeState === 3) {
          // 청소 지시 안됨.
          if (roomState.clean === 1 && type === 1) sel = false;
        } else if (changeState === 5) {
          // 청소 완료 안됨.
          if (roomState.clean === 0 && type === 0) sel = false;
        } else if (changeState === 6) {
          // 퇴실 안됨.
          if (roomState.sale === 0 || roomState.check_out) sel = false;
        } else if (changeState === 7) {
          // 입실 취소 안됨.
          if (roomState.sale > 0 && !roomState.check_out) {
            // 경과 시간.
            const diff = date.diff(moment(roomState.check_in), new Date());
            const minutes = Number(diff.mm);

            // 입실 취소 가능 시간 체크.
            if (minutes > Number(cancel_time)) {
              sel = false;
            }
          } else {
            sel = false;
          }
        }
      }

      if (sel) checklist[id] = sel;
    });

    console.log("-- checklist", checklist);

    this.setState(
      {
        selected: checklist,
      },
      () => {
        if (_.isEmpty(checklist)) {
          Swal.fire({
            icon: "warning",
            title: "선택 확인",
            html: "적용 가능한 객실이 없습니다.",
            confirmButtonText: "확인",
          });
          return;
        }

        if (changeState === 1) {
          var elem = document.createElement("div");
          elem.innerHTML = " <font color='red'>[" + keyToValue("room_state", "sale", type) + "]</font> 체크인을 하시겠습니까? <br/><br/>";

          let input = document.createElement("input");
          input.value = "";
          input.type = "number";
          input.placeholder = "추가/할인 금액";
          input.className = "swal-content__input";

          elem.appendChild(input);

          swal({
            title: "처리 확인",
            icon: "warning",
            content: elem,
            type: "input",
            buttons: {
              cash: {
                text: "현금결제",
                value: "cash",
                className: "green-btn",
              },
              card: {
                text: "카드결제",
                value: "card",
                className: "blue-btn",
              },
              cancle: {
                text: "취소",
                value: "cancle",
                className: "btn-set-cancel",
              },
            },
            closeOnClickOutside: true,
          }).then((value) => {
            let add_fee = input.value || 0;

            console.log("- result", value, add_fee);
            switch (value) {
              case "cancle":
                break;
              case "cash":
                _.map(checklist, (sel, id) => {
                  if (sel) {
                    // 입실 시 현금으로 결제
                    this.onChangeState(changeState, type, id, {}, { pay_cash_amt: 1, add_fee });
                  }
                });
                break;
              case "card":
                _.map(checklist, (sel, id) => {
                  if (sel) {
                    // 입실 시 카드로 결제
                    this.onChangeState(changeState, type, id, {}, { pay_card_amt: 1, add_fee });
                  }
                });
                break;
              default:
            }
          });
        } else if (changeState === 7 && use_sale_cancel_comment) {
          Swal.fire({
            input: "text",
            title: "취소 사유 입력",
            html: `<font color=red><b>입실 취소 사유</b></font>를 입력해 주세요`,
            inputPlaceholder: "내용 입력",
            inputAttributes: {
              maxlength: 100,
            },
            inputValue: "",
            inputValidator: (value) => {
              if (!value) {
                return "내용을 입력해 주세요!";
              }
            },
            showCancelButton: true,
            confirmButtonText: "확인",
            cancelButtonText: "취소",
          }).then((result) => {
            console.log("- result", result);
            if (!result.dismiss) {
              if (result.value) {
                _.map(checklist, (sel, id) => {
                  if (sel) {
                    this.onChangeState(changeState, type, id, {}, { comment: result.value });
                  }
                });
              }
            }
          });
        } else if (changeState < 13) {
          const value = keyToValue("room", "state", changeState);
          let msg = "";

          if (changeState === 1) {
            msg = " <font color='red'>[" + keyToValue("room_state", "sale", type) + "]</font> 체크인을 하시겠습니까?";
          } else if (changeState === 2) {
            msg = " <font color='red'>[" + (type === 1 ? "외출" : "외출복귀") + "]</font> 처리를 하시겠습니까?";
          } else if (changeState === 3) {
            msg = " <font color='red'>[" + value + "]</font> 을 하시겠습니까?";
          } else if (changeState === 5) {
            msg = " <font color='red'>[" + value + "]</font> 를 하시겠습니까?";
          } else if (changeState === 7) {
            msg = "입실취소 시 <font color='red'>매출도 취소</font> 됩니다. <br/><br/>입실 취소를 하시겠습니까?";
          } else if (changeState === 9) {
            msg = "객실 표시 내용을 <font color='red'>[" + (type === 0 ? "삭제" : "표시") + "]</font> 하시겠습니까?";
          } else if (changeState === 10) {
            msg = "무인 숙박 판매를 <font color='red'>[" + (type === 0 ? "허용" : "중지") + "]</font> 하시겠습니까?";
          } else if (changeState === 11) {
            msg = "무인 대실 판매를 <font color='red'>[" + (type === 0 ? "허용" : "중지") + "]</font> 하시겠습니까?";
          } else if (changeState === 12) {
            msg = "무인 예약 판매를 <font color='red'>[" + (type === 0 ? "허용" : "중지") + "]</font> 하시겠습니까?";
          } else {
            msg = " <font color='red'>[" + value + "]</font> 처리를 하시겠습니까?";
          }

          Swal.fire({
            icon: "info",
            title: "처리 확인",
            html: msg,
            showCancelButton: true,
            confirmButtonText: "확인",
            cancelButtonText: "취소",
          }).then((result) => {
            if (result.value) {
              _.map(checklist, (sel, id) => {
                if (sel) {
                  this.onChangeState(changeState, type, id, {}, {});
                }
              });
            }
          });
        } else {
          let msg = "";
          let value = 0;

          if (changeState === 13) {
            msg = "사용중 온도";
            value = temp_key_1;
          } else if (changeState === 14) {
            msg = "외출중 온도";
            value = temp_key_2;
          } else if (changeState === 15) {
            msg = "청소대기 온도";
            value = temp_key_5;
          } else if (changeState === 16) {
            msg = "청소중 온도";
            value = temp_key_4;
          } else if (changeState === 17) {
            msg = "공실 온도";
            value = temp_key_3;
          }

          Swal.fire({
            input: "number",
            title: "설정 입력",
            html: `<font color=red><b>${msg}</b></font>를 입력해 주세요`,
            inputPlaceholder: "온도 입력",
            inputAttributes: {
              min: 0,
              max: 30,
            },
            inputValue: value,
            inputValidator: (value) => {
              if (!value) return "온도를 입력해 주세요";
              else if (isNaN(value)) return "숫자를 입력해 주세요";
            },
            showCancelButton: true,
            confirmButtonText: "적용",
            cancelButtonText: "취소",
          }).then((result) => {
            console.log("- result", result);
            if (!result.dismiss) {
              if (result.value) {
                _.map(checklist, (sel, id) => {
                  if (sel) {
                    this.onChangeState(changeState, Number(result.value), id, {}, {});
                  }
                });
              }
            }
          });
        }
      },
      100
    );
  };

  setNotice = (notice) => {
    this.setState({
      notice,
    });
  };

  onCancel = () => {
    this.closeModal();
  };

  // 요금 계산.
  getFee = (state, sale) => {
    const { roomType, roomFee = [], preferences } = this.props;

    // 객실 타입 정보.
    const roomTypeItem = _.find(roomType.list, {
      id: state.room_type_id,
    });

    // console.log("--> getFee ", sale, { first, changSale });

    // 객실 기본 요금
    let default_fee = state.sale === 2 ? roomTypeItem.default_fee_rent : roomTypeItem.default_fee_stay;
    let reserv_fee = sale.reserv_fee || 0;

    console.log("--> getFee ", { default_fee, sale });

    const card = sale.pay_card_amt > 0 || sale.prepay_card_amt > 0; // 카드 결제 시.
    if (card) default_fee += roomTypeItem.card_add_fee;

    const day = moment().isoWeekday();
    const hour = moment().format("HH");

    // 기본 숙박 시간
    let stayTime = day > 5 ? preferences.stay_time_weekend : preferences.stay_time;

    // 기본 대실 시간
    let rentTime =
      Number(hour) < 12 // 오전
        ? day > 5 // 주말
          ? preferences.rent_time_am_weekend
          : preferences.rent_time_am
        : day > 5
        ? preferences.rent_time_pm_weekend
        : preferences.rent_time_pm;

    rentTime = rentTime < 1 ? 1 : rentTime;

    // 요일 / 시간 요금.
    let fee = undefined;

    let hhmm = moment().format("HHmm");

    // 해당 룸타입의 요일 요금 목록
    const roomFeeTypeList = _.filter(roomFee.list, (v) => v.room_type_id === state.room_type_id && v.day === day);

    // 해당 요일의 해당 시간대의 추가 요금 조회.
    roomFeeTypeList.forEach(function (v) {
      // 적용 채널 (0: 카운터, 1:자판기)
      if (v.channel === 0 && v.stay_type === state.sale) {
        let st = Number(v.begin);
        let et = Number(v.end);
        let nt = Number(hhmm);

        // console.log("- fee ", v, st, "<=", nt, "~", nt, "<", et);

        // 이전일 ~ 다음일 시간 이라면.
        if (st > et) {
          if (st <= nt || et > nt) {
            fee = v;
          }
        } else {
          if (st <= nt && et > nt) {
            fee = v;
          }
        }
      }
    });

    // console.log("- fee ", fee);

    // 이용 기간(숙박/장기만 해당)
    let usePeriod = moment(moment(sale.check_out_exp).format("YYYYMMDD")).diff(moment(moment(sale.check_in).format("YYYYMMDD")), "day");

    // 0박은 1박으로 한다
    usePeriod = usePeriod || 1;

    // 옵션 요금.
    let option_fee = 0;

    // 현재 요일 시간 추가 요금 적용.
    if (fee) option_fee += fee.add_fee;

    // 입실 시간 옵션 추가 요금 적용.
    if (sale.time_option_fee) option_fee += sale.time_option_fee;

    // 인원 추가 요금.
    if (sale.person > roomTypeItem.person) {
      option_fee += roomTypeItem.person_add_fee * (sale.person - roomTypeItem.person);
    }

    // 시즌 프리미엄 요금 적용 여부.
    let season = this.getSeason(sale);

    // 시즌
    if (!sale.room_reserv_id && season && season.premium) {
      // 입실 시간 기준 일때만 성수기/비성수기 이용 시간 추가.
      if (preferences.stay_time_type === 0 && season.add_stay_time !== 0) stayTime += season.add_stay_time;
      if (season.add_rent_time !== 0) rentTime += season.add_rent_time;
      // 시즌 추가 요금
      option_fee += season.premium;
    }

    // 숙박 시 사용 일자에 곱한다.
    if (state.sale !== 2 && usePeriod > 1) {
      default_fee *= usePeriod;
    }

    // 추가요금에서 옵션 요금 제외 한다.
    let add_fee = Number(sale.add_fee || 0);

    default_fee = sale.change_default_fee ? sale.default_fee : default_fee; // 기본요금 직접 변경 여부(변경 시 현재 값 유지!)
    add_fee += option_fee;

    console.log("- getFee ok", {
      default_fee,
      add_fee,
      option_fee,
      stayTime,
      rentTime,
    });

    return {
      default_fee,
      add_fee,
      option_fee,
      stayTime,
      rentTime,
    };
  };

  getSeason = (sale) => {
    const { seasonPremiums } = this.props;

    // 이용 기간(숙박/장기만 해당)
    let usePeriod = moment(moment(sale.check_out_exp).format("YYYYMMDD")).diff(moment(moment(sale.check_in).format("YYYYMMDD")), "day");

    // 0박은 1박으로 한다
    usePeriod = usePeriod || 1;

    // 시즌 프리미엄 요금 적용 여부.
    let season = _.filter(seasonPremiums, (v, k) => {
      // console.log("- season", v);
      let isSeason = false;
      for (let i = 0; i <= usePeriod; i++) {
        const mmdd = moment(sale.check_in).add(i, "day").format("MMDD");

        // console.log("- day", mmdd, v.begin, v.end);

        // 기간이 다음 년도 포함 시.
        if (Number(v.begin) > Number(v.end)) {
          isSeason = (Number(mmdd) <= Number("1231") && Number(mmdd) >= Number(v.begin)) || (Number(mmdd) >= Number("0101") && Number(mmdd) <= Number(v.end)); // 시즌에 속하는지 여부.
        } else {
          isSeason = Number(v.begin) <= Number(mmdd) && Number(v.end) >= Number(mmdd); // 시즌에 속하는지 여부.
        }

        if (isSeason) break;
      }
      //  console.log("- isSeason", isSeason);
      return isSeason;
    })[0];

    console.log("- getSeason", season);

    return season;
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      auth: {
        user: { place_id },
      },
      roomState,
      room,
    } = this.props;

    let { sort, reverse } = this.state;

    let list = _.cloneDeep(roomState.list);

    // 재 정렬 (객실명은 한글 제외 하고 정렬)
    if (sort) list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

    this.setState({
      data: room,
      list,
    });
  };

  componentWillUnmount = () => {
    const {
      preferences: { day_sale_end_time, place_id },
    } = this.props;

    let start = moment({ hour: day_sale_end_time, minute: 0 });

    if (start > moment()) {
      // 현재 시간 이전이라면 어제 일자로.
      start = start.add(-1, "day");
    }

    // 일일 업소 매출.
    this.props.initPlaceRoomSale(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

    // 일일 객실별 매출
    this.props.initPlaceRoomSaleRoom(place_id, start.format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));

    this.props.initAllRoomStates(place_id);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { sort, reverse, room_type_id } = this.state;

    if (nextProps.roomState && nextProps.roomState !== this.props.roomState) {
      let list = _.cloneDeep(nextProps.roomState.list);

      _.map(list, (item) => {
        item.state_name = keyToValue("room", "state", displayState(item, item, this.props.preferences));
      });

      if (room_type_id) {
        room_type_id = Number(room_type_id);

        list = _.filter(list, { room_type_id });
      }

      // 재 정렬 (객실명은 한글 제외 하고 정렬)
      if (sort) list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

      this.setState({
        list,
      });
    }
  }

  render() {
    return (
      <Gnb0203
        {...this.props}
        {...this.state}
        onRoomTypeSelect={this.onRoomTypeSelect}
        onAllClick={this.onAllClick}
        closeModal={this.closeModal}
        onChecked={this.onChecked}
        onSortClick={this.onSortClick}
        onSelDo={this.onSelDo}
        onCancel={this.onCancel}
        setNotice={this.setNotice}
      />
    );
  }
}

export default withSnackbar(Container);
