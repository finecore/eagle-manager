// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as mileageAction } from "actions/mileage";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, roomState, roomSale, roomType, roomFee, seasonPremium, error } = state;
  return {
    auth,
    user: auth.user,
    preferences: preferences.item,
    roomState,
    roomSale,
    roomType,
    roomFee,
    seasonPremiums: seasonPremium.list,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRoomStates: (palceId) => dispatch(roomStateAction.getRoomStateList(palceId)),
    initAllRoomSales: (palceId) => dispatch(roomSaleAction.getNowRoomSales(palceId)),
    initPlaceRoomSale: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSum(placeId, begin, end)),
    initPlaceRoomSaleRoom: (placeId, begin, end) => dispatch(roomSaleAction.getPlaceRoomSaleSumRooms(placeId, begin, end)),
    handleNewRoomSale: (roomSale, callback) => dispatch(roomSaleAction.newRoomSale(roomSale, callback)),
    handleSaveRoomSale: (id, roomSale, callback) => dispatch(roomSaleAction.putRoomSale(id, roomSale, callback)),
    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),
    handleSaveMileage: (placeId, phone, mileage) => dispatch(mileageAction.putMileage(placeId, phone, mileage)),
    handleIncreaseMileage: (place_id, phone, user_id, point, rollback) => dispatch(mileageAction.increaseMileage(place_id, phone, user_id, point, rollback)),
    handleDecreaseMileage: (place_id, phone, user_id, point, rollback) => dispatch(mileageAction.decreaseMileage(place_id, phone, user_id, point, rollback)),
    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
