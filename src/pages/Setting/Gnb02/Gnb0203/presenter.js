// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";
import _ from "lodash";

//room-popup
import { Form, FormControl, ControlLabel, FormGroup, Checkbox, Table, ButtonToolbar, Button } from "react-bootstrap";

import swal from "sweetalert";
import Swal from "sweetalert2";

import { keyToValue, displayState } from "constants/key-map";

// styles.
import "./styles.scss";

// functional component
const Gnb0203 = (props) => {
  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb02">
          <h1>일괄 처리</h1>
          <div className="setting-form-wrapper" id="gnb0203">
            <Form className="setting-form">
              <div className="search-result">
                <ButtonToolbar className="btn-select">
                  <FormControl componentClass="select" placeholder="객실 타입" value={props.room_type_id || ""} onChange={(evt) => props.onRoomTypeSelect(evt.target.value)}>
                    <option value="">객실타입 선택</option>
                    {props.roomType.list.map((v, i) => (
                      <option key={i} value={v.id}>
                        {v.name}
                      </option>
                    ))}
                  </FormControl>
                  <Button onClick={(evt) => props.onAllClick(1, true)}>전체 선택</Button>
                  <Button onClick={(evt) => props.onAllClick(1, false)}>선택 해제</Button>
                  <Button onClick={(evt) => props.onAllClick(2, null)}>선택 반전</Button>
                </ButtonToolbar>
                <div className="select-layout">
                  <div className="table-wrapper">
                    <Table>
                      <thead>
                        <tr>
                          <th>선택</th>
                          <th onClick={(evt) => props.onSortClick("type_name")} className={cx(props.sort === "type_name" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            객실 타입
                          </th>
                          <th onClick={(evt) => props.onSortClick("name")} className={cx(props.sort === "name" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            객실명
                          </th>
                          <th onClick={(evt) => props.onSortClick("sale")} className={cx(props.sort === "sale" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            숙박현황
                          </th>
                          <th onClick={(evt) => props.onSortClick("state_name")} className={cx(props.sort === "state_name" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            객실상태
                          </th>
                          <th onClick={(evt) => props.onSortClick("key")} className={cx(props.sort === "key" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            키상태
                          </th>
                          <th onClick={(evt) => props.onSortClick("door")} className={cx(props.sort === "door" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            출입문상태
                          </th>
                          <th onClick={(evt) => props.onSortClick("temp_key_1")} className={cx(props.sort === "temp_key_1" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            사용중 °C
                          </th>
                          <th onClick={(evt) => props.onSortClick("temp_key_2")} className={cx(props.sort === "temp_key_2" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            외출 °C
                          </th>
                          <th onClick={(evt) => props.onSortClick("temp_key_5")} className={cx(props.sort === "temp_key_5" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            청소대기 °C
                          </th>
                          <th onClick={(evt) => props.onSortClick("temp_key_4")} className={cx(props.sort === "temp_key_4" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            청소중 °C
                          </th>
                          <th onClick={(evt) => props.onSortClick("door")} className={cx(props.sort === "temp_key_3" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            공실 °C
                          </th>
                          {/* 
                          preferences.isc_sale : 자판기 판매 여부 (0:판매, 1:중지)
                          preferences.isc_sale_1 ~ 3 : 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                          state.isc_sale_1 ~ 3 : 객실별 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                          */}
                          <th onClick={(evt) => props.onSortClick("isc_sale_1")} className={cx(props.sort === "isc_sale_1" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            무인 숙박 판매
                          </th>
                          <th onClick={(evt) => props.onSortClick("isc_sale_2")} className={cx(props.sort === "isc_sale_2" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            무인 대실 판매
                          </th>
                          <th onClick={(evt) => props.onSortClick("isc_sale_3")} className={cx(props.sort === "isc_sale_3" ? "on" : "off", props.reverse ? "reverse" : "")}>
                            무인 예약 판매
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.map(props.list, (item, k) => (
                          <tr key={k} className={cx(props.selected[item.id] ? "on" : "off", props.reverse ? "reverse" : "")}>
                            <td>
                              <Checkbox inline checked={props.selected[item.id] || false} onChange={(evt) => props.onChecked(item.id)}>
                                <span />
                                {props.selected[item.id]}
                              </Checkbox>
                            </td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.type_name}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.name}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{keyToValue("room_state", "sale", item.sale)}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{keyToValue("room", "state", displayState(item, item, props.preferences))}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{keyToValue("room_state", "key", item.key)}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{keyToValue("room_state", "door", item.door)}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.temp_key_1}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.temp_key_2}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.temp_key_5}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.temp_key_4}</td>
                            <td onClick={(evt) => props.onChecked(item.id)}>{item.temp_key_3}</td>
                            <td onClick={(evt) => props.onChecked(item.id)} className={cx("isc_sale", props.preferences.isc_sale || props.preferences.isc_sale_1 || item.isc_sale_1 ? "no" : "")}>
                              {props.preferences.isc_sale || props.preferences.isc_sale_1 ? "판매중지" : item.isc_sale_1 ? "중지" : "허용"}
                            </td>
                            <td onClick={(evt) => props.onChecked(item.id)} className={cx("isc_sale", props.preferences.isc_sale || props.preferences.isc_sale_2 || item.isc_sale_2 ? "no" : "")}>
                              {props.preferences.isc_sale || props.preferences.isc_sale_2 ? "판매중지" : item.isc_sale_2 ? "중지" : "허용"}
                            </td>
                            <td onClick={(evt) => props.onChecked(item.id)} className={cx("isc_sale", props.preferences.isc_sale || props.preferences.isc_sale_3 || item.isc_sale_3 ? "no" : "")}>
                              {props.preferences.isc_sale || props.preferences.isc_sale_3 ? "판매중지" : item.isc_sale_3 ? "중지" : "허용"}
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                  <ButtonToolbar>
                    <div className="button">
                      <Button onClick={(evt) => props.onSelDo(1, 2)}>대실입실</Button>
                      <Button onClick={(evt) => props.onSelDo(3, 0)}>청소지시</Button>
                      <Button onClick={(evt) => props.onSelDo(13, 0)}>사용중 온도</Button>
                    </div>
                    <div className="button">
                      <Button onClick={(evt) => props.onSelDo(1, 1)}>숙박입실</Button>
                      <Button onClick={(evt) => props.onSelDo(5, 0)}>청소완료</Button>
                      <Button onClick={(evt) => props.onSelDo(14, 0)}>외출중 온도</Button>
                    </div>
                    <div className="button">
                      <Button onClick={(evt) => props.onSelDo(7, 0)}>입실취소</Button>
                      <Button onClick={(evt) => props.onSelDo(2, 1)}>외출</Button>
                      <Button onClick={(evt) => props.onSelDo(15, 0)}>청소대기 온도</Button>
                    </div>
                    <div className="button">
                      <Button onClick={(evt) => props.onSelDo(6, 0)}>퇴 &nbsp;&nbsp;&nbsp; 실</Button>
                      <Button onClick={(evt) => props.onSelDo(2, 0)}>외출복귀</Button>
                      <Button onClick={(evt) => props.onSelDo(16, 0)}>청소중 온도</Button>
                    </div>
                    <div className="button">
                      <Button onClick={(evt) => props.onSelDo(0, 0)}>공 &nbsp;&nbsp;&nbsp; 실</Button>
                      <Button className="hide">&nbsp;</Button>
                      <Button onClick={(evt) => props.onSelDo(17, 0)}>공실 온도</Button>
                    </div>
                    <div className="notice">
                      <span>객실 표시 내용 입력</span>
                      <FormControl componentClass="textarea" placeholder="객실 표시 내용" value={props.notice || ""} onChange={(evt) => props.setNotice(evt.target.value)} />
                      <div>
                        <Button onClick={(evt) => props.onSelDo(9, 1)}>적용</Button>
                        <Button onClick={(evt) => props.onSelDo(9, 0)}>삭제</Button>
                      </div>
                    </div>

                    {/* 
                      preferences.isc_sale : 자판기 판매 여부 (0:판매, 1:중지)
                      preferences.isc_sale_1 ~ 3 : 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                      state.isc_sale_1 ~ 3 : 객실별 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                    */}
                    <div className="isc_sale form-content">
                      <span style={{ color: props.preferences.isc_sale || props.preferences.isc_sale_1 ? "#ff0000" : "" }}>
                        {props.preferences.isc_sale ? "전체 판매 중지" : props.preferences.isc_sale_1 ? "숙박 판매 중지" : "무인 숙박 판매"}
                      </span>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_1)} onClick={(evt) => props.onSelDo(10, 0)}>
                        허용
                      </Button>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_1)} onClick={(evt) => props.onSelDo(10, 1)} className="stop">
                        중지
                      </Button>
                      <br />

                      <span style={{ color: props.preferences.isc_sale || props.preferences.isc_sale_2 ? "#ff0000" : "" }}>
                        {props.preferences.isc_sale ? "전체 판매 중지" : props.preferences.isc_sale_2 ? "대실 판매 중지" : "무인 대실 판매"}
                      </span>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_2)} onClick={(evt) => props.onSelDo(11, 0)}>
                        허용
                      </Button>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_2)} onClick={(evt) => props.onSelDo(11, 1)} className="stop">
                        중지
                      </Button>
                      <br />
                      <span style={{ color: props.preferences.isc_sale || props.preferences.isc_sale_3 ? "#ff0000" : "" }}>
                        {props.preferences.isc_sale ? "전체 판매 중지" : props.preferences.isc_sale_3 ? "예약 판매 중지" : "무인 예약 판매 "}
                      </span>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_3)} onClick={(evt) => props.onSelDo(12, 0)}>
                        허용
                      </Button>
                      <Button disabled={!!(props.preferences.isc_sale || props.preferences.isc_sale_3)} onClick={(evt) => props.onSelDo(12, 1)} className="stop">
                        중지
                      </Button>
                    </div>

                    <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                      닫기
                    </Button>
                  </ButtonToolbar>
                </div>
              </div>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0203;
