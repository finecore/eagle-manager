// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

// UI for notification
import { withSnackbar } from "notistack";

import Swal from "sweetalert2";

// import { CubeGrid } from "better-react-spinkit";
import cx from "classnames";

// Components.
import Gnb0202 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          place_id: 0,
          name: "",
          person: 2,
          disc: "",
          default_fee_stay: 0,
          default_fee_rent: 0,
          card_add_fee: 0,
          person_add_fee: 0,
          reserv_discount_fee_stay: 0,
          reserv_discount_fee_rent: 0,
          ota_name_1: "",
          ota_name_2: "",
          ota_name_3: "",
          ota_name_4: "",
        },
      ],
      idx: 0,
    };
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    preferences: PropTypes.object.isRequired,
    roomType: PropTypes.array.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (name, value) => {
    let { data, idx = 0 } = this.state;

    // console.log("- handleInputChange name", name, "value", value, idx);

    // 뷰 아이템 정보 변경.
    data[idx][name] = !value || isNaN(value) ? value : Number(value);

    this.setState({
      data,
    });
  };

  onSelect = (idx) => {
    console.log("-- onSelect", idx);

    this.setState({
      idx,
    });
  };

  onDelete = (idx) => {
    console.log("-- onDelete", idx);

    const { data } = this.state;
    let room_type = data[idx];

    Swal.fire({
      icon: "warning",
      title: "삭제 확인",
      html: "[" + room_type.name + "] 객실 타입을 정말 삭제 하시겠습니까?",
      showCancelButton: true,
      showCloseButton: false,
      confirmButtonText: "예",
      cancelButtonText: "아니오",
    }).then((result) => {
      if (result.value) {
        this.props.handleDeleteRoomType(room_type.id);
      }
    });
  };

  onAdd = () => {
    console.log("-- onAdd");
    const { data, idx } = this.state;

    let room_type = data[idx];

    Swal.fire({
      input: "text",
      title: "추가 확인",
      html: "[" + room_type.name + "] 객실타입 정보로 새로운 타입을 추가 합니다. <br/>추가할 객실 타입명을 입력해주세요.",
      inputPlaceholder: "객실 타입명을 입력해주세요",
      inputAttributes: {
        maxlength: 40,
      },
      showCancelButton: true,
      confirmButtonText: "추가",
      cancelButtonText: "취소",
    }).then((result) => {
      console.log("- result", result);
      if (!result.dismiss) {
        if (!result.value) {
          Swal.fire({
            icon: "warning",
            title: "추가 확인",
            html: "객실 타입명을 입력해 주세요",
            confirmButtonText: "확인",
          }).then((res) => {
            this.onAdd();
          });
        } else {
          let new_room_type = JSON.parse(JSON.stringify(room_type)); // 객실 타입 복제.

          delete new_room_type.id;
          delete new_room_type.reg_date;
          delete new_room_type.mod_date;

          new_room_type.name = result.value;

          console.log("-- new_room_type", new_room_type);

          this.props.handleNewRoomType({ room_type: new_room_type });
        }
      }
    });
  };

  onSave = () => {
    const { data, idx } = this.state;

    let room_type = data[idx];

    console.log("-- onSave", room_type);

    if (!room_type.name) {
      Swal.fire({
        icon: "warning",
        title: "추가 확인",
        html: "객실 타입명을 입력해 주세요",
        confirmButtonText: "확인",
      }).then((res) => {});
    } else {
      Swal.fire({
        icon: "info",
        title: "저장 확인",
        text: "[" + room_type.name + "]  설정을 저장 하시겠습니까?",
        showCancelButton: true,
        showCloseButton: false,
        confirmButtonText: "예",
        cancelButtonText: "아니오",
      }).then((result) => {
        if (result.value) {
          if (!room_type.id) this.props.handleNewRoomType({ room_type });
          else this.props.handleSaveRoomType(room_type.id, { room_type });

          setTimeout(() => {
            this.props.initAllRoomTypes(room_type.place_id);
          }, 1000);
        }
      });
    }
  };

  onCancel = () => {
    this.closeModal();
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      auth: {
        user: { place_id },
      },
    } = this.props;

    let { data } = this.state;

    data[0].place_id = place_id;

    this.setState({
      data,
    });

    this.props.initAllRoomTypes(place_id);
  };

  componentWillUnmount() {
    const {
      user: { place_id },
    } = this.props.auth;

    this.props.initAllRoomTypes(place_id);
    this.props.initAllRooms(place_id); // 객실정보 재조회.
    setTimeout(() => {
      this.props.initAllRoomViewItems(place_id);
    }, 100);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.roomType && nextProps.roomType !== this.props.roomType) {
      if (nextProps.roomType.length) {
        this.setState({
          data: nextProps.roomType,
        });
      }
    }
  }

  render() {
    return this.state.data ? (
      <Gnb0202
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSelect={this.onSelect}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
      />
    ) : (
      <div className={cx("loader")}>{/* <CubeGrid color="white" size={60} /> */}</div>
    );
  }
}

export default withSnackbar(Container);
