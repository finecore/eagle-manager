// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomTypeAction } from "actions/roomType";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, preferences, roomType, device, error } = state;
  return {
    auth,
    user: auth.user,
    preferences: preferences.item,
    roomType: roomType.list,
    device,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRoomTypes: (palceId) => dispatch(roomTypeAction.getRoomTypeList(palceId)),

    initAllRooms: (palceId) => dispatch(roomAction.getRoomList(palceId)),

    initAllRoomViewItems: (palceId) => dispatch(roomViewItemAction.getPlaceRoomViewItemList(palceId)),

    handleNewRoomType: (room_type) => dispatch(roomTypeAction.newRoomType(room_type)),

    handleSaveRoomType: (typeId, room_type) => dispatch(roomTypeAction.putRoomType(typeId, room_type)),

    handleDeleteRoomType: (typeId) => dispatch(roomTypeAction.delRoomType(typeId)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
