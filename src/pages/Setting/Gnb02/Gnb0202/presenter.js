// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";

import { FormControl, ButtonToolbar, Button } from "react-bootstrap";

import format from "utils/format-util";

import InputNumber from "rc-input-number";
import swal from "sweetalert";
import Swal from "sweetalert2";

// styles.
import "./styles.scss";
import { FormLabel } from "@material-ui/core";

// functional component
const Gnb0202 = (props) => {
  const item = props.data[props.idx];

  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb02">
          <h1>객실타입 관리</h1>
          <div className="setting-form-wrapper" id="gnb0202">
            <div className="search-result">
              <div className="select-layout">
                <ButtonToolbar className="btn-room">
                  <div className="type-list">
                    {props.roomType.map((v, k) => (
                      <div key={k}>
                        <Button className={cx(props.idx === k ? "selected" : "")} onClick={(evt) => props.onSelect(k)}>
                          {v.name}
                        </Button>
                        <Button onClick={(evt) => props.onDelete(k)}>
                          <span className="mdi mdi-delete" />
                        </Button>
                      </div>
                    ))}
                    {props.roomType.length ? (
                      <div>
                        <Button className="add" onClick={(evt) => props.onAdd()}>
                          <span className="mdi mdi-plus-circle-outline" />
                        </Button>
                      </div>
                    ) : (
                      <div>신규 객실타입 추가</div>
                    )}
                  </div>
                </ButtonToolbar>
                <div className="search-wrapper">
                  <ul className="list-bul">
                    <li>
                      <h2>기본정보</h2>
                      <div className="form-content">
                        <FormLabel>타입명</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.name || ""} onChange={(evt) => props.handleInputChange("name", evt.target.value)} />
                        <FormLabel>투숙인원</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber className="form-control small" step={1} min={0} max={1000} value={item.person} onChange={(value) => props.handleInputChange("person", value)} />
                        </span>
                      </div>
                      <div className="form-content">
                        <FormLabel>설명</FormLabel>
                        <FormControl type="text" className="long-txt" placeholder="설명 입력" value={item.disc || ""} onChange={(evt) => props.handleInputChange("disc", evt.target.value)} />
                      </div>
                    </li>
                    <li>
                      <h2>
                        기본요금 <span>(변경 단위 수정은 환경설정에서 가능합니다)</span>
                      </h2>
                      <div className="form-content">
                        <FormLabel>숙박</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={props.preferences.stay_fee_unit}
                            min={0}
                            max={1000000}
                            value={item.default_fee_stay}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("default_fee_stay", value)}
                          />
                        </span>
                        <FormLabel>대실</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={props.preferences.rent_fee_unit}
                            min={0}
                            max={1000000}
                            value={item.default_fee_rent}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("default_fee_rent", value)}
                          />
                        </span>
                      </div>
                    </li>
                    <li>
                      <h2>추가요금</h2>
                      <div className="form-content">
                        <FormLabel>카드결제추가</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={1000}
                            min={0}
                            max={1000000}
                            value={item.card_add_fee}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("card_add_fee", value)}
                          />
                        </span>
                        <FormLabel>인원추가(1인당)</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={1000}
                            min={0}
                            max={1000000}
                            value={item.person_add_fee}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("person_add_fee", value)}
                          />
                        </span>
                      </div>
                    </li>
                    <li>
                      <h2>예약할인</h2>
                      <div className="form-content">
                        <FormLabel>숙박</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={1000}
                            min={0}
                            max={item.default_fee_stay}
                            value={item.reserv_discount_fee_stay}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("reserv_discount_fee_stay", value)}
                          />
                        </span>
                        <FormLabel>대실</FormLabel>
                        <span className="numeric-wrap">
                          <InputNumber
                            className="form-control"
                            step={1000}
                            min={0}
                            max={item.default_fee_rent}
                            value={item.reserv_discount_fee_rent}
                            formatter={(value) => format.toMoney(value)}
                            onChange={(value) => props.handleInputChange("reserv_discount_fee_rent", value)}
                          />
                        </span>
                      </div>
                    </li>
                    <li>
                      <h2>OTA 등록 타입명</h2>
                      <div className="form-content">
                        <FormLabel className="ota-name">OTA 1</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.ota_name_1 || ""} onChange={(evt) => props.handleInputChange("ota_name_1", evt.target.value)} />
                      </div>
                      <div className="form-content">
                        <FormLabel className="ota-name">OTA 2</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.ota_name_2 || ""} onChange={(evt) => props.handleInputChange("ota_name_2", evt.target.value)} />
                      </div>
                      <div className="form-content">
                        <FormLabel className="ota-name">OTA 3</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.ota_name_3 || ""} onChange={(evt) => props.handleInputChange("ota_name_3", evt.target.value)} />
                      </div>
                      <div className="form-content">
                        <FormLabel className="ota-name">OTA 4</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.ota_name_4 || ""} onChange={(evt) => props.handleInputChange("ota_name_4", evt.target.value)} />
                      </div>
                      <div className="form-content">
                        <FormLabel className="ota-name">OTA 5</FormLabel>
                        <FormControl type="text" placeholder="타입명 입력" value={item.ota_name_5 || ""} onChange={(evt) => props.handleInputChange("ota_name_5", evt.target.value)} />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <ButtonToolbar className="btn-room btn-set-close">
              <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                닫기
              </Button>
              {/* 매니저 권한 이상만 수정 가능.  */}
              <Button onClick={(evt) => props.onSave(evt)} disabled={props.user.level > 2}>
                저장
              </Button>
            </ButtonToolbar>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0202;
