// imports.
import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import format from "utils/format-util";

// UI for notification
import { withSnackbar } from "notistack";

// Components.
import Gnb0201 from "./presenter";

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomTypeList: [],
      selected: [],
      list: [],
      sort: "name",
      reverse: false,
      selectedAll: false,
      searchText: "",
      selRoomTypeId: "all",
      alert: {
        title: "확인",
        text: "",
        type: "info", // info , input, warning...
        inputTtype: "text", // text, passwrod ..
        showCancelBtn: true,
        callback: undefined,
        close: true,
      },
      dialog: false,
      confirm: false,
      selGid: 0, // 선택 GID
      selDo: 0, // 선택 객실 작업.
    };
  }

  static props = {
    preferences: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    room: PropTypes.array.isRequired,
    roomType: PropTypes.array.isRequired,
  };

  // 정보 변경 이벤트.
  handleInputChange = (idx, name, value) => {
    let { list } = this.state;

    // 뷰 아이템 정보 변경.
    list[idx][name] = !value || isNaN(value) || name === "name" || name === "doorlock_id" ? value : Number(value);

    console.log("- handleInputChange name", name, "value", value);

    this.setState({
      list,
    });
  };

  onSortClick = (sorted) => {
    let { list, sort, reverse } = this.state;

    console.log("-- onSortClick", sorted, sort, reverse);

    reverse = sort === sorted ? !reverse : false;

    // 재 정렬.
    list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

    this.setState({
      sort: sorted,
      reverse,
      list,
      selected: [],
    });
  };

  onChecked = (idx, checked) => {
    console.log("-- onChecked", idx, checked);

    let { selected } = this.state;

    if (idx > -1) {
      selected[idx] = checked;
    } else {
      _.map(selected, (v, k) => {
        selected[k] = checked;
      });
    }

    this.setState({
      selected,
      selectedAll: checked,
    });
  };

  onRoomTypeSelected = (room_type_id) => {
    let { sort, reverse, searchText } = this.state;
    let { room } = this.props;

    room_type_id = Number(room_type_id);

    console.log("-- onRoomTypeSelected", { room_type_id });

    let list = _.filter(room, function (o) {
      return room_type_id !== "all" ? (room_type_id ? o.room_type_id === room_type_id : !o.room_type_id) : true;
    });

    list = format.textFilter(list, searchText);

    console.log("-- list", list);

    if (sort) list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

    this.setState({
      list,
      selRoomTypeId: room_type_id,
    });
  };

  onTextSearch = (search_text) => {
    let { sort, reverse, selRoomTypeId } = this.state;
    let { room } = this.props;

    search_text = String(search_text).replace(/[^\d]/g, "");

    console.log("-- onReservSearch", search_text);

    let list = _.filter(room, function (o) {
      return selRoomTypeId ? o.room_type_id === selRoomTypeId : true;
    });

    list = format.textFilter(list, search_text);

    if (sort) list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

    this.setState({
      list,
      searchText: search_text,
    });
  };

  onDelete = (idx) => {
    console.log("-- onDelete", idx);

    const { list, confirm } = this.state;

    let room = list[idx];

    console.log("-- onDelete", room, confirm);

    if (!confirm) {
      this.setState({
        alert: {
          type: "warning",
          title: "삭제 확인",
          text:
            "객실 삭제 시 매출을 포함한 모든 객실관련 정보도 <br/><font color='red'>영구적으로 삭제</font> 됩니다. <br/><br/>[<b>" +
            room.name +
            "</b>] 객실을 정말 <font color='red'>삭제</font> 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: (confirm) => {
            if (confirm) this.onDelete(idx);
          },
        },
        dialog: true,
      });
    } else {
      this.props.handleDeleteRoom(room.id);
    }
  };

  onAdd = (name) => {
    const { confirm } = this.state;
    const { roomType } = this.props;

    console.log("-- onAdd", name, confirm);

    if (!roomType.length) {
      this.setState({
        alert: {
          type: "warning",
          title: "객실 타입 오류",
          text: "객실 타입을 먼저 등록해 주세요!",
          confirmButtonText: "확인",
          showCancelBtn: false,
          close: true,
        },
        dialog: true,
      });
      return;
    }

    if (!confirm) {
      this.setState({
        alert: {
          type: "input",
          title: "추가 확인",
          text: "객실을 추가 합니다. <br/>추가할 객실명을 입력해주세요.",
          valid: "객실명을 입력해주세요.",
          confirmButtonText: "추가",
          cancelButtonText: "취소",
          showCancelBtn: true,
          close: false,
          callback: (confirm, value) => {
            if (confirm) this.onAdd(value);
          },
        },
        dialog: true,
      });
    } else {
      console.log("- roomType", roomType[0]);

      const { place_id, id: room_type_id } = roomType[0];

      let new_room = {
        place_id,
        room_type_id,
        name,
      };

      console.log("-- new_room", new_room);

      this.props.handleNewRoom({ room: new_room });
    }
  };

  onSelGid = (evt) => {
    const { selected, list, selGid } = this.state;

    console.log("-- onSelGid", selGid);

    let inx = 0;

    _.map(selected, (sel, k) => {
      if (sel) {
        let room = list[k];

        room.gid = selGid;
        room.lid = ++inx;
      }
    });

    this.setState({
      list,
      selGid: 0,
    });
  };

  onSelDo = (evt) => {
    const { selected, list, confirm, selDo } = this.state;

    console.log("-- onSelDo", selDo);

    if (!selDo) {
      this.setState({
        alert: {
          type: "warning",
          title: "객실 동작 확인",
          text: "선택 객실의 동작(복제/삭제)을 선택해 주세요.",
          confirmButtonText: "확인",
          showCancelBtn: false,
          close: true,
        },
        dialog: true,
      });
      return;
    }

    let doTxt = selDo === 1 ? "복제" : "삭제";

    if (!confirm) {
      this.setState({
        alert: {
          type: "info",
          title: doTxt + " 확인",
          text: "선택한 객실이 " + doTxt + " 됩니다. <br/>" + doTxt + "를 하시겠습니까?",
          confirmButtonText: "확인",
          cancelButtonText: "취소",
          showCancelBtn: true,
          close: false,
          callback: (confirm) => {
            if (confirm) this.onSelDo(evt);
          },
        },
        dialog: true,
      });
    } else {
      _.map(selected, (sel, k) => {
        if (sel) {
          let room = list[k];

          let { id, place_id, room_type_id, name, count, gid, lid, card_barcode, doorlock_id } = room; // 객실.

          if (selDo === 1) {
            let new_room = {
              place_id,
              room_type_id,
              name,
              count,
              gid,
              lid,
              card_barcode,
              doorlock_id,
            };

            new_room.name += "_복제";

            console.log("-- new_room", new_room);

            this.props.handleNewRoom({ room: new_room });
          } else {
            console.log("-- del_room", name);

            this.props.handleDeleteRoom(id);
          }
        }
      });

      this.setState({
        selDo: 0,
      });
    }
  };

  onSave = () => {
    const { list, selected, confirm } = this.state;
    const { room: org_list } = this.props;

    console.log("-- onSave");

    if (!confirm) {
      this.setState({
        alert: {
          title: "저장 확인",
          text: "객실 설정을 저장 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: (confirm) => {
            if (confirm) this.onSave();
          },
        },
        dialog: true,
      });
    } else {
      _.map(selected, (sel, k) => {
        if (sel) {
          const org = org_list[k];

          const { id, place_id, room_type_id, name, floor, count, gid, lid, card_barcode, doorlock_id, reserv_yn, isc_sale_1, isc_sale_2, isc_sale_3, main_power_type } = list[k];

          const room = {
            id,
            place_id,
            room_type_id,
            name,
            floor,
            count,
            gid,
            lid,
            card_barcode,
            doorlock_id,
            reserv_yn,
          };

          console.log("-- save room", room);

          // 객실명 음성 파일 새로 만든다.(reset 를 주면 무조건 새로 생성)
          if (!org || name !== org.name) {
            this.props.makeVoiceFile("room_" + id, name, true, (name, url) => {
              if (name) {
                console.log("- new room name voice created!", name);
              }
            });
          }

          this.props.handleSaveRoom(room.id, { room });

          const room_state = {};

          if (isc_sale_1 !== org.isc_sale_1) room_state.isc_sale_1 = isc_sale_1;
          if (isc_sale_2 !== org.isc_sale_2) room_state.isc_sale_2 = isc_sale_2;
          if (isc_sale_3 !== org.isc_sale_3) room_state.isc_sale_3 = isc_sale_3;
          if (main_power_type !== org.main_power_type) room_state.main_power_type = main_power_type;

          console.log("-- room_state", room_state);

          if (!_.isEmpty(room_state)) {
            this.props.handleSaveRoomState(id, { room_state });
          }
        }
      });
    }
  };

  onCancel = () => {
    this.closeModal();
  };

  onRoomTypeChange = (idx) => {
    console.log("- onRoomTypeChange", idx);

    const { confirm, selected, list } = this.state;
    const { roomType } = this.props;

    console.log("- roomType", roomType[idx]);

    if (!confirm) {
      this.setState({
        alert: {
          type: "info",
          title: "객실타입변경 확인",
          text: "선택한 객실을 [<b>" + roomType[idx].name + "</b>] 으로 <font color='red'>변경</font> 하시겠습니까?",
          confirmButtonText: "예",
          cancelButtonText: "아니오",
          showCancelBtn: true,
          close: false,
          callback: (confirm) => {
            if (confirm) this.onRoomTypeChange(idx);
          },
        },
        dialog: true,
      });
    } else {
      _.map(selected, (sel, k) => {
        if (sel) {
          const { id } = list[k];

          const room = {
            id,
            room_type_id: roomType[idx].id,
          };

          console.log("-- room", room);

          this.props.handleSaveRoom(room.id, { room });
        }
      });
    }
  };

  onSelDoChange = (selDo) => {
    console.log("- onSelDoChange", selDo);

    this.setState({
      selDo,
    });
  };

  onSelGidChange = (selGid) => {
    console.log("- onSelGidChange", selGid);

    this.setState({
      selGid,
    });
  };

  onConfirm = (confirm, value) => {
    console.log("- onConfirm", confirm);

    let { alert } = this.state;

    this.setState(
      {
        confirm,
        dialog: false, // 취소 시 닫힘.
      },
      () => {
        if (confirm) {
          if (alert.callback) alert.callback(_.cloneDeep(confirm), value);

          // 확인 상태 초기화.
          this.setState({
            confirm: false,
          });
        }
      }
    );
  };

  closeModal = (evt) => {
    this.props.handleDisplayLayer(undefined);
  };

  componentDidMount = () => {
    const {
      auth: {
        user: { place_id },
      },
      roomType,
    } = this.props;

    // console.log("-- Setting > Gnb0201 > componentDidMount ", this.props.room);

    let roomTypeList = _.cloneDeep(roomType);

    roomTypeList.push({
      id: "",
      name: "없음",
    });

    this.props.initAllRooms(place_id);

    this.setState({
      roomTypeList,
      list: _.cloneDeep(this.props.room),
    });
  };

  componentWillUnmount() {
    const {
      user: { place_id },
    } = this.props.auth;

    this.props.initAllRooms(place_id); // 객실정보 재조회.
    this.props.initAllRoomViewItems(place_id);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.error && nextProps.error !== this.props.error) {
      this.setState({
        confirm: false,
        dialog: false,
      });
    }

    if (nextProps.room && nextProps.room !== this.props.room) {
      let selected = [];

      _.map(nextProps.room, (v, k) => {
        selected.push(false);
      });

      // 재 정렬.
      let { sort, reverse, selRoomTypeId } = this.state;
      let list = _.cloneDeep(nextProps.room);

      if (selRoomTypeId !== "all") {
        this.onRoomTypeSelected(selRoomTypeId);
      } else {
        list = _.orderBy(list, (v) => (sort === "name" ? Number(v[sort].replace(/[^\d]/g, "")) : v[sort]), !reverse ? "asc" : "desc");

        this.setState({
          list,
        });
      }

      console.log("- room list", list);

      this.setState({
        selected,
        confirm: false,
        dialog: false,
      });
    }
  }

  render() {
    return (
      <Gnb0201
        {...this.props}
        {...this.state}
        closeModal={this.closeModal}
        handleInputChange={this.handleInputChange}
        onSortClick={this.onSortClick}
        onChecked={this.onChecked}
        onDelete={this.onDelete}
        onAdd={this.onAdd}
        onSelGid={this.onSelGid}
        onSelDo={this.onSelDo}
        onSave={this.onSave}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm}
        onRoomTypeChange={this.onRoomTypeChange}
        onSelGidChange={this.onSelGidChange}
        onSelDoChange={this.onSelDoChange}
        onIscSaleChange={this.onIscSaleChange}
        onRoomTypeSelected={this.onRoomTypeSelected}
        onTextSearch={this.onTextSearch}
      />
    );
  }
}

export default withSnackbar(Container);
