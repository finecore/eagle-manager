// imports.
import React from "react";
import Modal from "react-awesome-modal";
import cx from "classnames";
import _ from "lodash";

//room-popup
import { Form, Checkbox, FormControl, Table, ButtonToolbar, Button } from "react-bootstrap";
// import InputNumber from "rc-input-number";
import swal from "sweetalert";
import Swal from "sweetalert2";

// Components.
import SearchInput from "components/FormInputs/SearchInput";

// styles.
import "./styles.scss";

// functional component
const Gnb0201 = (props) => {
  let column = ["name", "gid", "lid", "type_name", "floor", "count", "card_barcode", "reserv_yn", "isc_sale_1", "isc_sale_2", "isc_sale_3"];
  let title = ["객실이름", "GID", "LID", "객실타입", "층수", "방수", "바코드", "예약가능", "무인 숙박판매", "무인 대실판매", "무인 예약판매", "예약가능"];

  // QR 도어락 사용 시
  if (props.preferences.doorlock_send_qr_code === 1) {
    column = ["name", "gid", "lid", "type_name", "floor", "count", "card_barcode", "doorlock_id", "reserv_yn", "isc_sale_1", "isc_sale_2", "isc_sale_3"];
    title = ["객실이름", "GID", "LID", "객실타입", "층수", "방수", "바코드", "도어락ID", "예약가능", "무인 숙박판매", "무인 대실판매", "무인 예약판매"];
  }

  // 객실 전원 차단 사용 시
  if (props.preferences.main_power_type === 1) {
    column.push("main_power_type");
    title.push("전원설정");
  }

  if (props.dialog) {
    Swal.fire({
      title: props.alert.title,
      icon: props.alert.type !== "input" ? props.alert.type : "info",
      html: props.alert.text,
      input: props.alert.type === "input" ? props.alert.inputTtype || "text" : null,
      inputPlaceholder: props.alert.placeholder || "입력해 주세요",
      inputAttributes: {
        maxlength: 100,
      },
      showCancelButton: props.alert.showCancelBtn,
      confirmButtonText: props.alert.confirmButtonText || "확인",
      cancelButtonText: props.alert.cancelButtonText || "취소",
    }).then((result) => {
      console.log("- result", result);
      if (result.dismiss === Swal.DismissReason.cancel) {
        props.onConfirm(false);
      } else {
        if (props.alert.type === "input" && !result.value) {
          swal.showInputError(props.alert.valid);
        } else {
          props.onConfirm(true, result.value);
        }
      }
    });
  }

  return (
    <div className="setting-popup">
      <Modal visible={true} effect="fadeInUp" onClickAway={(evt) => props.closeModal(evt)}>
        <article className="setting-wrapper gnb02">
          <h1>객실정보 관리</h1>
          <div className="setting-form-wrapper" id="gnb0201">
            <Form className="setting-form">
              <div className="search-wrapper">
                <ul className="list-bul">
                  <li>
                    <h2>객실 정보변경</h2>
                    <div className="header">
                      <div className="form-content clearfix">
                        <Checkbox
                          inline
                          className="checkbox04"
                          checked={props.selectedAll || false}
                          onChange={(evt) => props.onChecked(-1, evt.target.checked)}
                          disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                        >
                          <span />
                          전체선택
                        </Checkbox>
                      </div>
                      <div className="form-content room-type">
                        <FormControl componentClass="select" placeholder="객실 타입" value={props.selRoomTypeId || ""} onChange={(evt) => props.onRoomTypeSelected(evt.target.value)}>
                          <option value="all">객실타입 선택</option>
                          {props.roomTypeList.map((item, i) => (
                            <option key={i} value={item.id}>
                              {item.name}
                            </option>
                          ))}
                        </FormControl>
                      </div>
                      <SearchInput colWidth="col-md-2" type="text" placeholder="객실명 검색" value={props.searchText} onSearch={(value) => props.onTextSearch(value)} maxLength={20} />
                      <div className="form-content clearfix room-add">
                        <Button onClick={(evt) => props.onAdd(evt)}>객실 추가</Button>
                      </div>
                    </div>
                    <div className="form-content">
                      <div className="search-result">
                        <div className="table-wrapper">
                          <Table>
                            <thead>
                              <tr>
                                <th>선택</th>
                                {column.map((col, i) => (
                                  <th key={i} className={cx(props.sort === col ? "on" : "off")} onClick={(evt) => props.onSortClick(col)}>
                                    {title[i]}
                                    <b className={cx("caret", props.reverse ? "reverse" : "")} />
                                  </th>
                                ))}
                                <th>삭제</th>
                              </tr>
                            </thead>
                            <tbody>
                              {props.list.map((item, k) => (
                                <tr key={k} className={cx(props.selected[k] ? "on" : "off")}>
                                  <td>
                                    <Checkbox
                                      inline
                                      checked={props.selected[k] || false}
                                      onChange={(evt) => props.onChecked(k, evt.target.checked)}
                                      disabled={props.user.level > 2} // 매니저 권한 이상만 수정 가능.
                                    >
                                      <span />
                                    </Checkbox>
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl type="text" placeholder="GID" value={item.name} onChange={(evt) => props.handleInputChange(k, "name", evt.target.value)} />
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl type="text" placeholder="GID" value={item.gid || ""} onChange={(evt) => props.handleInputChange(k, "gid", evt.target.value)} />
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl type="text" placeholder="LID" value={item.lid || ""} onChange={(evt) => props.handleInputChange(k, "lid", evt.target.value)} />
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl
                                      componentClass="select"
                                      placeholder="객실타입 입력"
                                      value={item.room_type_id || ""}
                                      onChange={(evt) => props.handleInputChange(k, "room_type_id", evt.target.value)}
                                    >
                                      {props.roomTypeList.map((v, k) => (
                                        <option key={k} value={v.id}>
                                          {v.name}
                                        </option>
                                      ))}
                                    </FormControl>
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    {/* <span className="numeric-wrap">
                                      <InputNumber className="form-control small" step={1} min={1} max={100} value={item.floor} onChange={value => props.handleInputChange(k, "floor", value)} />
                                    </span> */}
                                    <FormControl type="text" placeholder="층수" value={item.floor || 1} onChange={(evt) => props.handleInputChange(k, "floor", evt.target.value)} />
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    {/* <span className="numeric-wrap">
                                      <InputNumber className="form-control small" step={1} min={1} max={100} value={item.count} onChange={value => props.handleInputChange(k, "count", value)} />
                                    </span> */}
                                    <FormControl type="text" placeholder="방수" value={item.count || 1} onChange={(evt) => props.handleInputChange(k, "count", evt.target.value)} />
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <FormControl
                                      type="text"
                                      placeholder="바코드 입력"
                                      value={item.card_barcode || ""}
                                      onChange={(evt) => props.handleInputChange(k, "card_barcode", evt.target.value)}
                                    />
                                  </td>
                                  {props.preferences.doorlock_send_qr_code === 1 ? (
                                    <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                      <FormControl
                                        type="text"
                                        placeholder="도어락ID 입력"
                                        value={item.doorlock_id || ""}
                                        onChange={(evt) => props.handleInputChange(k, "doorlock_id", evt.target.value)}
                                      />
                                    </td>
                                  ) : null}
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <Checkbox
                                      inline
                                      checked={item.reserv_yn === 1}
                                      onChange={(evt) => props.handleInputChange(k, "reserv_yn", evt.target.checked ? 1 : 0)}
                                      className={item.reserv_yn === 1 ? "on" : "off"}
                                    >
                                      <span />
                                      {item.reserv_yn === 1 ? "가능" : "불가"}
                                    </Checkbox>
                                  </td>
                                  {/* 
                                  preferences.isc_sale : 자판기 판매 여부 (0:판매, 1:중지)
                                  preferences.isc_sale_1 ~ 3 : 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                                  state.isc_sale_1 ~ 3 : 객실별 숙박,대실,예약 무인 판매 여부(0:허용, 1:중지)
                                  */}
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    {props.preferences.isc_sale || props.preferences.isc_sale_1 === 1 ? (
                                      "전체중지"
                                    ) : (
                                      <Checkbox
                                        inline
                                        checked={props.preferences.isc_sale === 0 && props.preferences.isc_sale_1 === 0 && item.isc_sale_1 === 0}
                                        onChange={(evt) => props.handleInputChange(k, "isc_sale_1", evt.target.checked ? 0 : 1)}
                                        className={props.preferences.isc_sale === 0 && props.preferences.isc_sale_1 === 0 && item.isc_sale_1 === 0 ? "on" : "off"}
                                        disabled={props.preferences.isc_sale === 1 || props.preferences.isc_sale_1 === 1}
                                      >
                                        <span />
                                        {item.isc_sale_1 ? "중지" : "허용"}
                                      </Checkbox>
                                    )}
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    {props.preferences.isc_sale || props.preferences.isc_sale_2 === 1 ? (
                                      "전체중지"
                                    ) : (
                                      <Checkbox
                                        inline
                                        checked={props.preferences.isc_sale === 0 && props.preferences.isc_sale_2 === 0 && item.isc_sale_2 === 0}
                                        onChange={(evt) => props.handleInputChange(k, "isc_sale_2", evt.target.checked ? 0 : 1)}
                                        className={props.preferences.isc_sale === 0 && props.preferences.isc_sale_2 === 0 && item.isc_sale_2 === 0 ? "on" : "off"}
                                        disabled={props.preferences.isc_sale === 1 || props.preferences.isc_sale_2 === 1}
                                      >
                                        <span />
                                        {item.isc_sale_2 ? "중지" : "허용"}
                                      </Checkbox>
                                    )}
                                  </td>
                                  <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                    <Checkbox
                                      inline
                                      checked={props.preferences.isc_sale === 0 && props.preferences.isc_sale_3 === 0 && item.isc_sale_3 === 0}
                                      onChange={(evt) => props.handleInputChange(k, "isc_sale_3", evt.target.checked ? 0 : 1)}
                                      className={props.preferences.isc_sale === 0 && props.preferences.isc_sale_3 === 0 && item.isc_sale_3 === 0 ? "on" : "off"}
                                      disabled={props.preferences.isc_sale === 1 || props.preferences.isc_sale_3 === 1}
                                    >
                                      <span />
                                      {props.preferences.isc_sale || props.preferences.isc_sale_3 === 1 ? "전체중지" : item.isc_sale_3 ? "중지" : "허용"}
                                    </Checkbox>
                                  </td>
                                  {props.preferences.main_power_type > 0 && (
                                    <td onClick={(evt) => !props.selected[k] && props.onChecked(k, true)}>
                                      <FormControl
                                        componentClass="select"
                                        placeholder="예약 목록"
                                        value={item.main_power_type}
                                        onChange={(evt) => props.handleInputChange(k, "main_power_type", Number(evt.target.value))}
                                        className={cx(item.main_power_type ? "on" : "off")}
                                      >
                                        <option value="0">키에연동</option>
                                        <option value="1">차단</option>
                                        <option value="2">투입</option>
                                      </FormControl>
                                    </td>
                                  )}
                                  <td>
                                    <Button className="del" onClick={(evt) => props.onDelete(k)} disabled={!props.selected[k]}>
                                      <span className="mdi mdi-delete" />
                                    </Button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        </div>
                      </div>

                      <div className="room-control">
                        <div className="room-select">
                          선택한 객실을
                          <FormControl
                            componentClass="select"
                            placeholder="객실타입"
                            value={props.selected && _.filter(props.selected, (sel) => sel === true).length === 0 ? "" : ""}
                            onChange={(evt) => props.onRoomTypeChange(evt.target.value)}
                            disabled={
                              (props.selected && _.filter(props.selected, (sel) => sel === true).length === 0) || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                            }
                          >
                            <option value="">선택</option>
                            {props.roomTypeList.map((v, k) => (
                              <option key={k} value={k}>
                                {v.name}
                              </option>
                            ))}
                          </FormControl>
                          타입으로 변경
                        </div>

                        <div className="room-gid">
                          선택한 객실 GID를 <FormControl type="text" placeholder="GID를 입력" value={props.selGid} onChange={(evt) => props.onSelGidChange(Number(evt.target.value))} />로{" "}
                          <Button
                            onClick={(evt) => props.onSelGid(evt)}
                            disabled={
                              _.filter(props.selected, (sel) => sel === true).length === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                            }
                          >
                            변경
                          </Button>
                        </div>

                        <div className="room-clone">
                          선택한 객실을
                          <FormControl componentClass="select" placeholder="동작 선택" onChange={(evt) => props.onSelDoChange(Number(evt.target.value))} value={props.selDo}>
                            <option value={0}>선택</option>
                            <option value={1}>복제</option>
                            <option value={2}>삭제</option>
                          </FormControl>
                          <Button
                            onClick={(evt) => props.onSelDo(evt)}
                            disabled={
                              _.filter(props.selected, (sel) => sel === true).length === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                            }
                          >
                            적용
                          </Button>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <ButtonToolbar className="btn-room">
                <Button className="btn-set-cancel" onClick={(evt) => props.closeModal(evt)}>
                  닫기
                </Button>
                <Button
                  onClick={(evt) => props.onSave(evt)}
                  disabled={
                    _.filter(props.selected, (sel) => sel === true).length === 0 || props.user.level > 2 // 매니저 권한 이상만 수정 가능.
                  }
                >
                  저장
                </Button>
              </ButtonToolbar>
            </Form>
          </div>
          <a onClick={(evt) => props.closeModal(evt)} className="setting-close">
            Close
          </a>
        </article>
      </Modal>
    </div>
  );
};

export default Gnb0201;
