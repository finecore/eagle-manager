// imports.
import { connect } from "react-redux";

// Actions.
import { actionCreators as layoutAction } from "actions/layout";
import { actionCreators as roomAction } from "actions/room";
import { actionCreators as roomStateAction } from "actions/roomState";
import { actionCreators as voiceAction } from "actions/voice";
import { actionCreators as roomViewItemAction } from "actions/roomViewItem";

// Components.
import Container from "./container";

const mapStateToProps = (state, ownProps) => {
  const { auth, room, preferences, roomType, device, error } = state;
  return {
    auth,
    user: auth.user,
    room: room.list,
    preferences: preferences.item,
    roomType: roomType.list,
    device,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initAllRooms: (palceId) => dispatch(roomAction.getRoomList(palceId)),

    initAllRoomViewItems: (palceId) => dispatch(roomViewItemAction.getPlaceRoomViewItemList(palceId)),

    handleNewRoom: (room) => dispatch(roomAction.newRoom(room)),

    handleSaveRoom: (id, room) => dispatch(roomAction.putRoom(id, room)),

    handleDeleteRoom: (id) => dispatch(roomAction.delRoom(id)),

    handleDisplayLayer: (displayLayer) => dispatch(layoutAction.setDisplayLayer(displayLayer)),

    handleSaveRoomState: (roomId, roomState, callback) => dispatch(roomStateAction.putRoomState(roomId, roomState, callback)),

    makeVoiceFile: (name, text, reset, callback) => dispatch(voiceAction.makeVoiceFile(name, text, reset, callback)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
