// imports.
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import "./styles.scss";

class NotFound extends Component {
  static defaultProps = {};

  render() {
    const link = !this.props.isLogined ? "Login" : "Main";
    console.log("-- isLogined ", this.props.isLogined);

    return (
      <div className="FourOhFour">
        <div className="bg" />
        <div className="code">
          404
          <br />
          <span className="title">Not Found Page</span>
          <br />
          <span className="title">Check url please...{/* <Link to={"/"}>{link}</Link> */}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { auth } = state;
  return {
    isLogined: auth.isLogined,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NotFound);
