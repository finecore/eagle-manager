// imports.
import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./styles.scss";

class NotLogin extends Component {
  static defaultProps = {};

  render() {
    return (
      <div className="FourOhFour">
        <div className="bg" />
        <div className="code">
          901
          <br />
          <span className="title">로그인을 해주세요.</span>
          <br />
          <span className="title">
            <Link to="/">Login</Link>
          </span>
        </div>
      </div>
    );
  }
}

export default NotLogin;
