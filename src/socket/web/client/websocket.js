import moment from "moment";
import _ from "lodash";
import { actionCreators as errActions } from "actions/error";

let _store = null;
let serverIp = "localhost";

let webSocket = null;

export const setSocketServer = (ip) => {
  if (serverIp !== ip) {
    console.log("-> setSocketServer ", { serverIp, ip });
    serverIp = ip;
    if (webSocket) webSocket.close(); // 웹소켓 닫으면 자동 재연결..
  }
};

let update = new Date();

// websocket client.
// 분산 서버 환경에서는 로드 발란서 IP 로 설정 하고 세션 연관성을 client ip 로 설정하여 동일 업소의 직원를 한서버에 모아둔다.
export const setupWebsocket = ({ host, port, reConnect }) => {
  console.log("-> setupWebsocket ", host, port);

  let isQrCode = location.href.indexOf("/QR/") > -1;

  if (isQrCode) {
    console.log("- WebSocketUtil no connect ", { isQrCode });

    return new Promise((resolve) => {
      const send = (type, payload) => {};
      const receive = (store) => {};

      return resolve({ send, receive, webSocket });
    });
  } else {
    serverIp = host;

    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // 웹소켓 서버 접속.
    webSocket = new WebSocket(`ws://${host}:${port}`);

    console.log("- WebSocketUtil connect ", webSocket);

    update = new Date();

    return new Promise((resolve) => {
      const receive = (store) => {
        if (_store) store = _store;
        else _store = store;

        // store dispatch 객체를 전달 받아서 메세지가 도착 할때마다 reducer 에 전달 하고 해당 type 이 있으면 수행 후 state 갱신.
        webSocket.onmessage = (event) => {
          const {
            auth: { user, isLogined },
          } = _store.getState();

          const message = event.data ? JSON.parse(event.data) : {};
          const { websocket = true, type, payload, metadata } = message;

          console.log("%c -> websocket receive message ", "background: #222; color: #bada55", { type, payload, metadata, user });

          if (isLogined && type) {
            let action = { websocket, type, ...payload, metadata }; // ...payload : reducer action 구조분해 할당.

            const { auth, room } = store.getState();

            if (user && metadata && user.uuid === metadata.uuid) action.websocket = false; // 동일 브라우저는 음설 재생 안함.

            action.auth = auth; // auth store 주입
            action.room = room; // room state 주입

            try {
              store.dispatch(action);
            } catch (e) {
              console.log("-> webSocket receive dispatch error", e);

              // let message = type + " " + JSON.stringify(payload);
              // let error = { code: 400, message, detail: e.stack, logout: false };

              // store.dispatch(errActions.setErr(error));
            }
          }

          update = new Date();
        };
      };

      const send = (type, payload) => {
        console.log("-> webSocket send readyState");
        if (webSocket && webSocket.readyState === webSocket.OPEN) webSocket.send(JSON.stringify({ type, payload }));
        else console.log("-> webSocket not open ");
      };

      webSocket.onopen = () => {
        console.log("-> webSocket is onopen!");

        if (_store) {
          const {
            auth: { user },
          } = _store.getState();

          // user 값을 web socket server session 에 설정 한다.(CCU, FSC 와 통신시 웹매니저와 연동을 위한 키는 place_id 이다.)
          if (user) {
            console.log("- join user", user);
            send("JOIN_REQUESTED", { user });
          }

          setTimeout(() => {
            let action = {
              websocket: true,
              type: "SET_NETWORK_INFO",
              network: { mode: "SOCKET", status: true },
            };

            _store.dispatch(action);
          }, 1000);
        }

        console.log("- update", update);

        return resolve({ send, receive, webSocket });
      };

      webSocket.onclose = (e) => {
        console.log("-> webSocket is closed. Reconnect will be attempted in a second.", e.reason, e.wasClean);

        if (_store) {
          let action = {
            websocket: true,
            type: "SET_NETWORK_INFO",
            network: { mode: "SOCKET", status: false },
          };

          _store.dispatch(action);
        }

        try {
          webSocket.end();
          webSocket.destroy();
        } catch (e) {}

        // 웹소켓 재연결을 위해 콜백 함수 호출 한다.
        if (reConnect) reConnect(serverIp);
      };

      webSocket.onerror = (err) => {
        console.error("-> Socket encountered error: ", err.message, "Closing socket");
        webSocket.close();
      };
    });
  }
};
