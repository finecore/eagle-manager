import { createStore, combineReducers, applyMiddleware } from "redux";
import createLogger from "redux-logger";
import thunk from "redux-thunk";
import reducers from "reducers";
import { createBrowserHistory } from "history";
import { routerReducer, routerMiddleware } from "react-router-redux";
import { setupWebsocket } from "socket/web/client/websocket";
import ip from "ip";

const env = process.env.NODE_ENV;

// Application Node Server.
const envProps = {
  HOST_PROTOCOL: window.location.protocol,
  HOST_URL: process.env.REACT_APP_HOST.replace("localhost", location.hostname) + ":" + process.env.REACT_APP_PORT, // 도메인 + 포트
  API_SERVER_URL: process.env.REACT_APP_API_SERVER_HOST.replace("localhost", location.hostname) + ":" + process.env.REACT_APP_API_SERVER_PORT,
  SERVICE_MODE: process.env.REACT_APP_MODE,
  IS_LOCAL: window.location.href.indexOf("localhost") > -1,
};

const ipaddr = ip.address(); // my ip address
const ipequeal = ip.isEqual("::1", "::0:1"); // true

console.log("-- IP ", ipaddr, ipequeal);
console.log("-- NODE_ENV ", JSON.stringify({ env, envProps }));

// Create a history of your choosing (we're using a browser history in this case)
const history = createBrowserHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = [];

middleware.push(routerMiddleware(history));
middleware.push(createLogger);

function configureStore() {
  return websocket();
}

// websocket client.
// 분산 서버 환경에서는 로드 발란서 IP 로 설정 하고 세션 연관성을 client ip 로 설정하여 동일 업소의 직원를 한서버에 모아둔다.
const websocket = (serverIp) => {
  return setupWebsocket({
    host: serverIp || process.env.REACT_APP_WEB_SOCKET_HOST.replace("localhost", location.hostname),
    port: process.env.REACT_APP_WEB_SOCKET_PORT,
    reConnect: (serverIp) => {
      setTimeout(function () {
        console.log("---> websocket reConnect reconnecting... ");
        websocket(serverIp);
      }, 3000);
    },
  }).then(({ send, receive, webSocket }) => {
    console.log("---> setupWebsocket ok ");
    // reduct middleware thunk set custom argment.
    // action thunk 사용 시 async 요청 시 (dispatch, getState, {send}) 처럼 send 객체를 파라메터로 받을 수 있다.
    middleware.push(thunk.withExtraArgument({ send, webSocket }));

    const store = createStore(
      combineReducers({
        ...reducers,
        router: routerReducer, // redux router : history 관리를 redux 를 통해서 한다.
      }),
      applyMiddleware(...middleware)
    );

    // websocket client onmessage 시 store.dispatch 에서 해당 type 의 reducer 실행.
    // 서버에서 전송한 변경 데이터를 화면에 반영 하는 방법으로 reducer 사용.
    // Manager -> Server -> Other Mangers (동일 IP 의 동일 업소 계정으로 전파)
    // 서버 로드 밸런서 에서 세션 연관성을 IP 로 설정 하여 동일 IP 는 같은 서버에 몰아 넣는다.(다른 IP 사용이 있을경우 수정 해야함)
    receive(store);

    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept("../reducers", () => {
        const nextRootReducer = require("../reducers/index");
        store.replaceReducer(nextRootReducer);
      });
    }

    return store;
  });
};

export { configureStore, envProps, history, websocket };
