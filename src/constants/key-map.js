import _ from "lodash";

export const codes = {
  com: {
    useYn: [
      { text: "사용", value: "Y" },
      { text: "사용안함", value: "N" },
    ],
    acptYn: [
      { text: "허용", value: "Y" },
      { text: "허용안함", value: "N" },
    ],
    agreeYn: [
      { text: "동의", value: "Y" },
      { text: "동의안함", value: "N" },
    ],
    mailYn: [
      { text: "수신", value: "Y" },
      { text: "수신안함", value: "N" },
    ],
    otaMailYn: [
      { text: "수신", value: "1" },
      { text: "실패만 수신", value: "2" },
      { text: "수신안함", value: "0" },
    ],
    payYn: [
      { text: "유료", value: "Y" },
      { text: "무료", value: "N" },
    ],
    svcModes: [
      { text: "Air", value: 0 },
      { text: "Pro", value: 1 },
    ],
  },
  room: {
    state: [
      { value: 0, text: "공실" },
      { value: 1, text: "입실" },
      { value: 2, text: "외출" },
      { value: 3, text: "청소요청" },
      { value: 4, text: "청소중" },
      { value: 5, text: "청소완료" },
      { value: 6, text: "퇴실" },
      { value: 7, text: "입실취소" },
      { value: 8, text: "퇴실취소" },
    ],
  },
  room_state: {
    sale: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
    key: [
      { value: 0, text: "NO KEY" },
      { value: 1, text: "고객키 ▼" },
      { value: 2, text: "마스터 ▼" },
      { value: 3, text: "청소키 ▼" },
      { value: 4, text: "고객키 △" },
      { value: 5, text: "마스터 △" },
      { value: 6, text: "청소키 △" },
    ],
    door: [
      { value: 0, text: "문 닫】【힘" },
      { value: 1, text: "문 【열림】" },
    ],
    isc_sale: [
      { value: 0, text: "숙박/대실" },
      { value: 1, text: "X" },
      { value: 2, text: "숙박" },
      { value: 3, text: "대실" },
    ],
    clean: [
      { value: 0, text: "청소 완료" },
      { value: 1, text: "청소 요청" },
    ],
    outing: [
      { value: 0, text: "외출 복귀" },
      { value: 1, text: "외출" },
    ],
    signal: [
      { value: 0, text: "통신 정상" },
      { value: 1, text: "통신 이상" },
    ],
    main_relay: [
      { value: 0, text: "전원 차단" },
      { value: 1, text: "전원 공급" },
    ],
    car_call: [
      { value: 0, text: "배차 없음" },
      { value: 1, text: "배차 요청" },
      { value: 2, text: "배차 완료" },
      { value: 3, text: "배차 취소" },
    ],
    inspect: [
      { value: 1, text: "점검대기" },
      { value: 2, text: "점검 중" },
      { value: 3, text: "점검 완료" },
    ],
  },
  room_sale: {
    state: [
      { value: "A", text: "매출 등록" },
      { value: "B", text: "입실 취소" },
      { value: "C", text: "정산 완료" },
    ],
    channel: [
      { value: "web", text: "프론트" },
      { value: "mobile", text: "모바일" },
      { value: "isc", text: "무인" },
      { value: "device", text: "IDM" },
      { value: "pms", text: "PMS" },
      { value: "api", text: "자동" },
    ],
    stay_type: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
  },
  room_interrupt: {
    channel: [
      { value: 1, text: "WEB" },
      { value: 2, text: "ISG" },
      { value: 3, text: "MOBILE" },
      { value: 4, text: "PAD" },
    ],
  },
  room_reserv: {
    ota_code: [
      { value: 1, text: "야놀자" },
      { value: 2, text: "여기어때" },
      { value: 3, text: "네이버" },
      { value: 4, text: "에어비앤비" },
      { value: 5, text: "호텔나우" },
      { value: 8, text: "기타" },
      { value: 9, text: "직접예약" },
    ],
    state: [
      { value: "A", text: "정상예약" },
      { value: "B", text: "취소예약" },
      { value: "C", text: "사용완료" },
    ],
    type: [
      { value: "0", text: "예약신규" },
      { value: "1", text: "예약변경" },
      { value: "2", text: "예약취소" },
    ],
    visit_type: [
      { value: 1, text: "도보방문" },
      { value: 2, text: "차량방문" },
      { value: 3, text: "대중교통" },
    ],
  },
  user: {
    type: [
      { value: 1, text: "아이크루" },
      { value: 2, text: "대리점" },
      { value: 3, text: "업소" },
    ],
    level: [
      { value: 0, text: "슈퍼관리자" },
      { value: 1, text: "책임자(업주)" },
      { value: 2, text: "관리자(매니저)" },
      { value: 3, text: "근무자(카운터)" },
      { value: 4, text: "메이드(청소원)" },
      { value: 5, text: "PMS" },
      { value: 9, text: "뷰어" },
    ],
  },
  device: {
    type: [
      { value: "01", text: "IDM" },
      { value: "02", text: "ISG" },
      { value: "10", text: "RPT" },
    ],
  },
  notice: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "공지 알림" },
      { value: 2, text: "업데이트 알림" },
    ],
    important: [
      { value: "", text: "구분 선택" },
      { value: 1, text: "일반" },
      { value: 2, text: "중요" },
    ],
  },
  notice_place: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "공지" },
      { value: 2, text: "메모" },
    ],
    state: [
      { value: "", text: "진행 상태" },
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  as: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "AS 요청" },
      { value: 2, text: "건의 사항" },
    ],
    state: [
      { value: "", text: "진행 상태" },
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  model: {
    "01": [
      { value: "01", text: "A01" },
      { value: "02", text: "I01" },
    ],
    "02": [{ value: "01", text: "XA" }],
    10: [{ value: "01", text: "R01" }],
  },
  subscribe: {
    pay_type: [
      { value: "D", text: "일간 결제" },
      { value: "M", text: "월간 결제" },
      { value: "Y", text: "년간 결제" },
    ],
  },
  place_subscribe: {
    applyState: [
      { value: 0, text: "신규 신청" },
      { value: 1, text: "변경 신청" },
      { value: 2, text: "구독 완료" },
    ],
  },
  mileage: {
    type: [
      { value: 1, text: "관리자 변경" },
      { value: 2, text: "고객 적립" },
      { value: 3, text: "고객 사용" },
      { value: 4, text: "적립 취소" },
      { value: 5, text: "사용 취소" },
    ],
  },
};

export function keyCodes(talble, type) {
  // console.log("--- keyCodes", { talble, type }, codes[talble][type]);

  return codes[talble][type] || [];
}

export function keyToValue(talble, type, value) {
  // if (value !== null && value !== undefined) value = isNaN(value) || (value.length > 1 && value.charAt(0) === "0") ? value : Number(value);

  let code = _.find(codes[talble][type] || [], (code) => String(code.value) === String(value));
  // console.log("--- keyToValue", { talble, type, value }, code, codes[talble][type]);

  if (!code) {
    // console.log("- keyToValue error", { talble, type, value });
    return value;
  }

  return code.text;
}

// 조건에 따른 객실 상태값 (단지 room_state 의 clean, sale,  key 만 보고 판단)
// 화면에 보이는 상태이다.
export function displayState(roomState, roomSale, preferences) {
  /*
      clean = 0:없음, 1:있음 (청소요청)
      sale  = 0:업음 1:숙박 2:대실 3:장기
      key   = 0:없음, 1:고객키,2:마스터키 3:청소키

      state = 0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료, 6:퇴실 (상태 조건이 변경 될 때마다 체크 하여 반영 한다)
    */
  const { inspect_use } = preferences;
  const { clean, sale, key, outing, inspect } = roomState;
  const { check_out = undefined } = roomSale || {};

  let code = 0;

  // 판매 상태 일때.
  if (sale > 0) code = 1; // 입실.
  if (sale > 0 && (key < 1 || key > 3) && outing === 1) code = 2; // 외출(key out 시)
  if (sale > 0 && check_out) code = 6; // 퇴실

  // 청소 요청 상태 일때.
  if (clean === 1) code = 3; // 청소요청
  if (key === 2) code = 8; // 인스펙트 중 (마스터 키 삽입 시 무조건 점검 중)
  if (key === 3) code = 4; // 청소중
  if (sale > 0 && key === 6 && code !== 2) code = 5; // 청소완료

  // 객실 인스펙트 사용 여부 (0: 사용 안함, 1: 퇴실 상태만 사용, 2: 항상 사용)
  if (key !== 3) {
    if ((inspect_use === 1 && check_out) || inspect_use === 2) {
      if (key === 6) {
        code = 7; // 인스펙트대기
      } else if (key === 2) {
        code = 8; // 인스펙트 중
      } else if (key === 5) {
        code = 9; // 인스펙트 완료
      } else {
        // 점검 상태 이고 고객키 없을때 점검 표시.
        if (inspect && key !== 1) {
          if (inspect == 1) {
            code = 7; // 인스펙트대기
          } else if (inspect === 2) {
            code = 8; // 인스펙트 중
          } else if (inspect === 3) {
            code = 9; // 인스펙트 완료
          }

          console.log("----> displayState ", { inspect_use, inspect, sale, key, code });
        }
      }
    }
  }

  // console.log("----> displayState ", { inspect_use, inspect, check_out, clean, sale, key, outing, code });

  return code;
}

export function displayStateName(roomState, roomSale, preferences) {
  const code = displayState(roomState, roomSale, preferences);
  return keyToValue("room", "state", code);
}

// 자판기 관제 상태 화면
export function iscDisplayState(iscState) {
  /* 스텝
  1 : 판매대기 단계
  2 : 성인인증 모드
  3 : 예약 확인 단계
  4 : 객실 선택 단계 (예약 시 스킵)
  5 : 객실 선택 확인 단계 (예약 시 스킵)
  6 : 결제 단계 (예약 시 스킵)
  7 : 결제 확인 단계 및 카드키 토출
  8 : 영수증 출력 단계 (고객 선택 시) */

  let text = "판매대기";

  if (iscState.step === 1) {
    if (iscState.motion) text = "고객접근";
    else text = "고객이동";
  } else if (iscState.step === 2) {
    text = "성인인증<i>모드</i>";
    if (!iscState.minor_mode) text += "성인인증<i>완료</i>";
  } else if (iscState.step === 3) {
    text = "예약번호 입력";
    if (iscState.reserv_num) text = "예약번호<i>" + iscState.reserv_num + "</i>";
    if (iscState.reserv_error_msg) text += "예약오류<i>" + iscState.reserv_error_msg + "</i>";
  } else if (iscState.step === 4) {
    text = "객실선택";
    if (iscState.stay_type) text += "객실타입<i>" + iscState.stay_type + "</i>";
    if (iscState.room_id) text += "객실명<i>" + iscState.room_id + "</i>";
  } else if (iscState.step === 5) {
    text = "객실선택 확인";
    if (iscState.stay_type) text += "객실타입<i>" + iscState.stay_type + "</i>";
    if (iscState.room_id) text += "객실명<i>" + iscState.room_id + "</i>";
  } else if (iscState.step === 6) {
    text = "결제금액 " + iscState.pay_amt;

    // 현금 결제.
    if (iscState.act_input_unit) text += "현금결제<i>" + iscState.act_input_unit + "</i>";
    if (iscState.dsp_state) text += "거스름돈<i>" + iscState.dsp_change_unit + "</i>";
    if (iscState.dsp_not_change) text += "미방출<i>" + iscState.dsp_not_change + "</i>";

    // 카드 결제.
    if (iscState.crd_state) {
      text += "카드결제<i>" + iscState.crd_state + "</i>"; //  { value: 0, text: 대기상태 , // 1 : 결제중 // 2 : 결제 완료 // 3 : 결제 실패
      if (iscState.crd_card_no) text += "카드번호<i>" + iscState.crd_card_no + "</i>";
      if (iscState.crd_approval_no) text += "승인번호<i>" + iscState.crd_approval_no + "</i>";
    }
  } else if (iscState.step === 7) {
    text = "결제확인";
    text += "결제금액 " + iscState.pay_amt;

    // 현금 결제.
    if (iscState.act_input_unit) text += "현금결제<i>" + iscState.act_input_unit + "</i>";
    if (iscState.dsp_state) text += "거스름돈<i>" + iscState.dsp_change_unit + "</i>";
    if (iscState.dsp_not_change) text += "미방출<i>" + iscState.dsp_not_change + "</i>";

    // 카드 결제.
    if (iscState.crd_state) {
      text += "카드결제<i>" + iscState.crd_state + "</i>"; //  { value: 0, text: 대기상태 , // 1 : 결제중 // 2 : 결제 완료 // 3 : 결제 실패
      if (iscState.crd_card_no) text += "카드번호<i>" + iscState.crd_card_no + "</i>";
      if (iscState.crd_approval_no) text += "승인번호<i>" + iscState.crd_approval_no + "</i>";
    }

    // 카드키 배출.
    text += "카드키배출<i>" + iscState.kbx_dsp_error + "</i>"; // 0 : 정상 , 1 : 방출실패오류 , 2 : 확인완료
  }

  return { text };
}
