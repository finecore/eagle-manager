import moment from "moment";
import chalk from "chalk";

console.logTime = function () {
  if (arguments.length) {
    var timestamp = "[" + chalk.grey(moment().format("YYYY.MM.DD HH:mm:ss")) + "] ";
    let args = "";

    for (let key in arguments) {
      args += arguments[key];
    }
    this.log(timestamp, args);
  }
};

export default console;
