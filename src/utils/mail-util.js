const sgMail = require("@sendgrid/mail");
const _ = require("lodash");
const moment = require("moment");

let lastMail = [];

const send = ({ to, from, subject, content }) => {
  console.log("-> Mail send", { to, from, subject }, content.length);

  sgMail.setApiKey(process.env.REACT_APP_MAIL_SENDGRID_API_KEY);

  if (to && subject && content) {
    // 동일 메일 발송 검사
    let sameMail = 0;

    lastMail = _.filter(lastMail, (m) => {
      let near = moment(m.reg_date).add(1, "minute").isAfter(moment()); // 1 분이내 발송건만
      if (near && _.trim(m.to) === _.trim(to) && _.trim(m.content) === _.trim(content)) sameMail++;
      return near;
    });

    console.log("- send sameMail", sameMail);

    // 최근 1분이내 발송 1건이상 동일 하면 발송 취소.
    if (sameMail < 1) {
      lastMail.push({ to, content });

      const msg = {
        to,
        from: from || process.env.REACT_APP_MAIL_FROM_ADDR,
        subject,
        text: content,
        html: content,
      };

      sgMail.send(msg);

      console.log("- send ok", msg);
    }
  }
};

module.exports = { send };
