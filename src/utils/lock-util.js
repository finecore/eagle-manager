import { actionCreators as placeLockAction } from "actions/placeLock";
import { actionCreators as roomSaleAction } from "actions/roomSale";
import { actionCreators as roomStateAction } from "actions/roomState";
//import { actionCreators as wsActions } from "actions/ws";
//import _ from "lodash";
//import moment from "moment";
//import date from "utils/date-util";

class Lock {
  constructor(store) {
    this.store = store;

    this.isSysLock = false;
    this.user = null;
    this.checkTimer = null;

    this.MODE_API = "api"; // API 서버 연동 오류(WEB 서버 -> API 서버 통신 기준).
    this.MODE_NET = "net"; // 네크웤 오류.

    this.mode = null; // 네크웤 or API (net/api)
    this.host = null; // 서버 IP (분산 서버 환경에서 한쪽 서버 다운 시 세션 연결이 끊어져서 세션 재연결(화면 갱신) 해야 함)
    this.change = false; // 서버 변경 시.

    this.crear();

    this.user = sessionStorage.getItem("user") ? JSON.parse(sessionStorage.getItem("user")) : {};

    console.log("-> lock init ", this.user);
  }

  crear() {
    console.log("-> lock crear ");

    this.mode = null;
    this.host = null;
    this.isSysLock = false;

    if (this.checkTimer) clearInterval(this.checkTimer);
    return this;
  }

  on(mode, host) {
    if (this.user) {
      console.log("-> lock on ", mode);

      this.mode = mode;
      this.host = host;

      // 잠금 설정(시스템 잠금)
      const { place_id, id: user_id } = this.user;

      const place_lock = { id: 9999, on: true, place_id, user_id, type: 9, mode, host };

      // 네트웤 오류 이므로 서버에 저장 못 함. 로컬에서만 변경.
      this.store.dispatch(placeLockAction.setPlaceLock(place_lock));

      this.isSysLock = true;
    }

    return this;
  }

  off(reload, host) {
    if (this.user) {
      // 잠금 설정(시스템 잠금)
      const { place_id, id: user_id } = this.user;

      console.log("-> lock off ", this.mode, this.host, host, this.host !== host);

      const change = this.host !== host;
      this.host = host;

      const place_lock = {
        id: 9999,
        on: false,
        place_id,
        user_id,
        type: 9,
        mode: this.mode,
        host,
        change,
      };

      // 재연결 시  잠금 해제
      this.store.dispatch(placeLockAction.setPlaceLock(place_lock));

      if (reload) {
        // 누락 정보 조회.
        this.store.dispatch(roomStateAction.getRoomStateList(place_id));
        this.store.dispatch(roomSaleAction.getNowRoomSales(place_id));
      }

      // this.mode = null;
      this.isSysLock = false;
    }

    return this;
  }

  stop() {
    console.log("-> lock stop ");

    if (this.checkTimer) clearInterval(this.checkTimer);
    return this;
  }

  watch(interval, callback) {
    console.log("-> lock watch ");

    if (this.checkTimer) clearInterval(this.checkTimer);

    // 연결 체크
    this.checkTimer = setInterval(() => {
      callback.call(this);
    }, interval || 1000);

    return this;
  }
}

export default Lock;
