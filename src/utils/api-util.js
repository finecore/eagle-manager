import axios from "axios";

import { actionCreators as errActions } from "actions/error";
import { actionCreators as networkAction } from "actions/network";
import { actionCreators as commonAction } from "actions/common";

import _ from "lodash";
import moment from "moment";

let isAilive = true;

const api = {
  // error request.
  errorRequest: (err, dispatch) => {
    console.log("- errorRequest", err, "\n err.response", err.response);

    let errors = {
      code: "Request Error",
      message: "HttpRequest ERR_CONNECTION_REFUSED",
    };

    if (err.response) {
      const comerr = err.response.data.common ? err.response.data.common.error : {};

      // 호출 정보
      let { method, url, data } = err.response.config;

      errors = {
        code: err.response.status,
        message: _.trim(err.response.statusText),
        detail: comerr.message,
        config: { method, url, data },
      };
    } else if (err) {
      const errs = String(err).split(":");
      errors = {
        code: errs[0] === "TypeError" ? 401 : 400,
        message: _.trim(errs[1] ? errs[1] : err),
      };
    }

    // API 연동 오류 시 재 연결 체크!
    if (isAilive && errors.code === 400) {
      isAilive = false;

      setTimeout(() => {
        dispatch(networkAction.check());
        isAilive = true;
      }, 3000);
    }

    dispatch(commonAction.hideLoader());

    isCalling = 0;

    return errActions.setHostErr(errors);
  },

  // check response.
  checkResponse: (res, dispatch) => {
    // console.log("- checkResponse", res);

    isAilive = true;

    // 호출 정보
    let { method, url, data } = res.config;

    if (res.status !== 200) {
      const error = {
        code: res.statue,
        message: res.data.message,
        detail: res.statusText,
        config: { method, url, data },
      };

      dispatch(errActions.setHostErr(error));
    } else {
      const { common, body } = res.data;
      let { success, error } = common;

      if (!success) {
        console.log("- error.detail", error.detail);
        // to array.
        if (error.detail && !(error.detail instanceof Array)) {
          error.detail = [error.detail];
          console.log("- error.detail", error.detail);
        }

        error.config = { method, url, data };

        if (res.status !== 200 && !error.detail) error.detail.push(res.statusText);

        dispatch(errActions.setHostErr(error));
      }
      return { common, body }; // return object.
    }
  },
};

// Ajax As Axios
const ajax = axios.create({
  baseURL: "",
});

ajax.CancelToken = axios.CancelToken;
ajax.isCancel = axios.isCancel;
ajax.dispatch = null;

let lodingTimer = null;
let isCalling = 0;

// Add a request interceptor
ajax.interceptors.request.use(
  (config) => {
    // console.log("-> ajax.interceptors.request ", config.headers);

    if (!config.headers) config.headers = {};

    if (!config.headers["channel"]) config.headers["channel"] = "web"; // 채널 설정.

    let token = config.headers["token"] || sessionStorage.getItem("token");
    if (token) config.headers["x-access-token"] = token;

    let user = sessionStorage.getItem("user");
    if (user) config.headers["uuid"] = JSON.parse(user).uuid;

    config.time = new Date();

    console.log(
      ">>>>>> axios request >>>>>>\n %c" + config.method.toUpperCase(),
      "background: #222; color: #bada55",
      moment(config.time).format("mm:ss:SSS"),
      config.url,
      config.headers["channel"],
      "\ndata:",
      config.data
    );

    if (lodingTimer) clearTimeout(lodingTimer);

    if (isCalling === 0) {
      lodingTimer = setTimeout(() => {
        if (isCalling) {
          if (ajax.dispatch) ajax.dispatch(commonAction.showLoader());
        }
      }, 10 * 1000);
    } else {
      lodingTimer = setTimeout(() => {
        if (isCalling) {
          if (ajax.dispatch) ajax.dispatch(commonAction.hideLoader());
        }
      }, 5000);
    }

    isCalling++;

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
ajax.interceptors.response.use(
  function (response) {
    let diff = moment.utc(moment(new Date(), "DD/MM/YYYY HH:mm:ss:SSS").diff(moment(response.config.time, "DD/MM/YYYY HH:mm:ss:SSS"))).format("ss:SSS");

    console.log(
      "<<<<<< axios response <<<<<<\n %c" + response.config.method.toUpperCase() + "%c  %c " + diff + " ",
      "background: #f66; color: #fff",
      "background: #fff; color: #fff",
      "background: #4b49d7; color: #fff",
      response.config.url,
      "(" + moment(response.config.time).format("mm:ss:SSS") + " ~ " + moment(new Date()).format("mm:ss:SSS") + ")",
      "\ndata:",
      response.data
    );

    if (ajax.dispatch) ajax.dispatch(commonAction.hideLoader());

    isCalling--;

    return response;
  },
  function (error) {
    // Do something with response error
    if (ajax.dispatch) ajax.dispatch(commonAction.hideLoader());

    isCalling = 0;

    return Promise.reject(error);
  }
);

export { api, ajax };
