import { envProps } from "config/configureStore";
import moment from "moment";
//import _ from "lodash";

class ExcelUtil {
  constructor(id, name) {
    this.id = id;
    this.name = name || id;

    console.log("- ExcelUtil", id, name);
  }

  tableToExcel() {
    console.log("- ExcelUtil tableToExcel", this.id, this.name);

    var ele = document.getElementById(this.id);
    if (ele) {
      var data_type = "data:application/vnd.ms-excel;charset=utf-8";
      var table_html = encodeURIComponent(ele.outerHTML);

      var aId = "excel_a_" + this.id;

      var a = document.getElementById(aId);
      console.log("- a", a);
      if (!a) {
        a = document.createElement("a");
        a.id = "excel_a_" + this.id;
        a.href = data_type + ",%EF%BB%BF" + table_html;
        document.body.appendChild(a);
      }

      a.download = this.name + "_" + moment().format("YYY-MM-DD") + ".xls";
      a.click();
    }
  }

  downLoadExcel(url) {
    window.location.href = `${envProps.API_SERVER_URL}/${url}`;
  }
}

export default ExcelUtil;
