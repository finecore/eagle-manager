import crypto from "crypto";

const ENCRYPTION_KEY = process.env.REACT_APP_SECRET;

const encKey = crypto.createHash("md5").update(String(ENCRYPTION_KEY)).digest();
const iv16 = Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);

// console.log("crypto ", { ENCRYPTION_KEY, encKey, iv16 });

function getAlgorithm(encKey) {
  var key = Buffer.from(encKey, "base64");
  switch (key.length) {
    case 16:
      return "aes-128-cbc";
    case 32:
      return "aes-256-cbc";
  }

  throw new Error("Invalid key length: " + key.length);
}

export const encrypt = (text) => {
  text = String(text);

  console.log("encrypt ", { ENCRYPTION_KEY, encKey, iv16, text });

  try {
    const cipher = crypto.createCipheriv(getAlgorithm(encKey), encKey, iv16);
    let encrypted = cipher.update(String(text), "utf8", "base64");
    encrypted += cipher.final("base64");
    return encrypted;
  } catch (e) {
    console.error("- encrypt error", e);
  }
  return null;
};

export const decrypt = (messagebase64) => {
  messagebase64 = String(messagebase64);

  console.log("decrypt ", { ENCRYPTION_KEY, encKey, iv16, messagebase64 });

  try {
    const decipher = crypto.createDecipheriv(getAlgorithm(encKey), encKey, iv16);
    let decrypted = decipher.update(messagebase64, "base64", "utf8");
    decrypted += decipher.final();
    return decrypted;
  } catch (e) {
    console.error("- decrypt error", e);
  }
  return null;
};
