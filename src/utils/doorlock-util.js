import { keyToValue } from "constants/key-map";
import { CheckSubscribe } from "utils/subscribe-util";
import { encrypt, decrypt } from "utils/aes256-util";
import date from "utils/date-util";

import md5 from "md5";

import Swal from "sweetalert2";
import _ from "lodash";
import moment from "moment";
import axios from "axios";
import $ from "jquery";

class YYDoorLock {
  constructor(props) {
    console.log("-> YYDoorLock constructor", props);

    this.props = props;

    let { preferences } = this.props;

    let { doorlock_appid, doorlock_username, doorlock_userid, doorlock_password } = preferences;

    this.URL = "https://yylock.eeun.cn/dms/app";

    this.APPID = doorlock_appid;
    this.APPKEY = "0A615734763B4D1D8F8C82E7B5861CDF"; // 앱 개발자 키(DMS 홈페이지에 로그인 해서 체크)
    this.USERNAME = doorlock_username; // 고객 이름
    this.PASSWORD = doorlock_password; // 고객 비밀번호
    this.USERID = doorlock_userid; // user china Phone number (중국 김문표 전화번호: 18669800621)
    this.KEYLOCKID = ""; // 객실 도어락의 장비ID
    this.TOKEN = ""; // 대화token
    this.AT = String(new Date().getTime()); // Time Stam（미리초 단위）
    this.STARTDATE = ""; // Start Time(Format：년월일 시간 분 초)
    this.ENDDATE = ""; // End Time(Format：yyyymmddhhmmss)
    this.VALIDMINUTE = 60; // QR코드 유효시간（분,minute 단위）
    this.NONCESTR = String(new Date().getTime()); // 램덤 코드
    this.TIMEZONE = "-9"; // TimeZone（;// TimeZone,eg. Chinese timezone is -8, and South Korea's timezone is -9）
    this.SAVEFLAGE = "0"; // Save flag， 1：저장 ,0：저장안함

    this.SIGN = ""; // 전송 정보 암호화값 (MD5 hex)

    this.timer = null;
    this.timestamp = moment(); // 클래스 로드 시간.

    console.log("-> YYDoorLock create", this.URL, { doorlock_appid, doorlock_username, doorlock_password });
  }

  orderedQuery = (params) => {
    // 파라메터 명 정렬.
    const ordered = Object.keys(params)
      .sort()
      .reduce((obj, key) => {
        obj[key] = String(params[key]);
        return obj;
      }, {});

    // console.log("- YYDoorLock call ordered", ordered);

    return $.param(ordered).toUpperCase();
  };

  async call(path, params, isSign) {
    console.log("-> YYDoorLock call", { path, params, isSign });

    if (!this.APPID) {
      Swal.fire({
        icon: "error",
        title: "도어락 오류",
        text: "QR 도어락의 APPID 가 없습니다.",
        confirmButtonText: "확인",
      });

      return;
    }

    params.AT = String(new Date().getTime());
    params.NONCESTR = String(new Date().getTime());

    var query = this.orderedQuery(params);

    // 서명 암호화.
    if (isSign) {
      // 앱 개발자 키는 서명에는 참여 하지만 파라메터 전송은 하지 않는다.
      let signStr = `${query}&APPKEY=${this.APPKEY}`;

      console.log("- YYDoorLock call signStr", signStr);

      let sign = md5(signStr).toUpperCase();

      query += `&SIGN=${sign}`;
    }

    // console.log("-> YYDoorLock call query", query);

    let url = `${this.URL}/${path}?${query}`;

    console.log("-> YYDoorLock call url", url);

    // POST 전송 한다.
    let promise = await axios
      .post(url)
      .then((res) => res.data || [])
      .then((data) => {
        console.log("-> YYDoorLock call res", data);
        return data;
      })
      .catch((error) => {
        console.error("- YYDoorLock call error", error);
      });

    return promise;
  }

  async login() {
    console.log("- YYDoorLock login ");

    let { APPID, AT, NONCESTR, USERNAME, PASSWORD } = this;

    let params = { APPID, AT, NONCESTR, USERNAME, PASSWORD };

    return await this.call("dmsLogin", params, false).then((data) => {
      if (data) {
        let {
          result,
          token,
          dhUser: { USERNAME, USER_ID },
          msg,
        } = data;

        if (result === 0) {
          this.TOKEN = token;
        } else {
          Swal.fire({
            icon: "error",
            title: "도어락 오류",
            html: `QR 도어락 로그인 중 오류가 발생 했습니다. <br/>${msg}`,
            confirmButtonText: "확인",
          });
        }
      }

      return data || {};
    });
  }

  // QR 코드 전송 파라메터 정보 만들기.
  async makeQRCodeQuerys(startDate, endDate, deviceId, isSign) {
    console.log("- YYDoorLock makeQRCodeParams ", { startDate, endDate, deviceId });

    let { APPID, KEYLOCKID, SAVEFLAGE, TOKEN, USERID, VALIDMINUTE, TIMEZONE } = this;

    if (!TOKEN) {
      let {
        roomDoorLock: {
          login: { token },
        },
      } = this.props;

      this.TOKEN = TOKEN = token;
    }

    if (!TOKEN) {
      Swal.fire({
        icon: "error",
        title: "도어락 오류",
        html: "QR 도어락의 TOKEN 이 없습니다.<br/>화면을 갱신해 주세요.",
        confirmButtonText: "확인",
      });

      return;
    }

    var ms = moment(endDate, "DD/MM/YYYY HH:mm:ss").diff(moment(startDate, "DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var hh = Math.floor(d.asHours());
    var mm = Math.floor(d.asMinutes());

    VALIDMINUTE = mm;

    startDate = startDate.format("YYYYMMDDHHmmss");
    endDate = endDate.format("YYYYMMDDHHmmss");

    let AT = String(new Date().getTime());
    let NONCESTR = String(new Date().getTime());

    let params = { APPID, AT, TOKEN, USERID, NONCESTR, STARTDATE: startDate, ENDDATE: endDate, KEYLOCKID: deviceId, VALIDMINUTE, TIMEZONE, SAVEFLAGE };

    // 파라메터 명 정렬.
    const ordered = Object.keys(params)
      .sort()
      .reduce((obj, key) => {
        obj[key] = String(params[key]);
        return obj;
      }, {});

    // console.log("- YYDoorLock call ordered", ordered);

    var query = $.param(ordered).toUpperCase();

    // 서명 암호화.
    if (isSign) {
      // 앱 개발자 키는 서명에는 참여 하지만 파라메터 전송은 하지 않는다.
      let signStr = `${query}&APPKEY=${this.APPKEY}`;

      console.log("- YYDoorLock call signStr", signStr);

      let sign = md5(signStr).toUpperCase();

      query += `&SIGN=${sign}`;
    }

    console.log("- YYDoorLock query ", query);

    return query;
  }

  async getLockQRCode(startDate, endDate, deviceId) {
    console.log("- YYDoorLock getLockQRCode ", { startDate, endDate, deviceId });

    let { APPID, AT, KEYLOCKID, NONCESTR, SAVEFLAGE, TOKEN, USERID, VALIDMINUTE, TIMEZONE } = this;

    var ms = moment(endDate, "DD/MM/YYYY HH:mm:ss").diff(moment(startDate, "DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var hh = Math.floor(d.asHours());
    var mm = Math.floor(d.asMinutes());

    VALIDMINUTE = mm;

    startDate = startDate.format("YYYYMMDDHHmmss");
    endDate = endDate.format("YYYYMMDDHHmmss");

    let params = { APPID, AT, TOKEN, USERID, NONCESTR, STARTDATE: startDate, ENDDATE: endDate, KEYLOCKID: deviceId, VALIDMINUTE, TIMEZONE, SAVEFLAGE };

    return await this.call("getLockQRCode", params, true).then((data) => {
      if (data) {
        let { result, msg } = data;

        if (result !== 0) {
          Swal.fire({
            icon: "error",
            title: "도어락 오류",
            html: `QR 코드 생성 중 오류가 발생 했습니다. <br/>${msg}`,
            confirmButtonText: "확인",
          });
        }
      }

      return data || {};
    });
  }

  // checkout.(사용 못함!)
  async sysLockTime(deviceId) {
    console.log("- YYDoorLock sysLockTime ", { deviceId });

    let { APPID, AT, KEYLOCKID, NONCESTR, SAVEFLAGE, TOKEN } = this;

    let params = { APPID, AT, TOKEN, NONCESTR, DEVICE_ID: deviceId, CHECKOUTFLAG: 1 };

    return await this.call("sysLockTime", params, true).then((data) => {
      if (data) {
        let { result, msg } = data;

        if (result !== 0) {
          Swal.fire({
            icon: "error",
            title: "도어락 오류",
            html: `QR 코드 취소 중 오류가 발생 했습니다. <br/>${msg}`,
            confirmButtonText: "확인",
          });
        }
      }

      return data || {};
    });
  }

  timer() {
    console.log("- YYDoorLock timer ");

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      let hour = this.timestamp.add("hour", 1).format("HHmmss");
      let now = moment().format("HHmmss");

      // 1시간 경과 시 동작
      if (hour === now) {
        this.login();
        this.timestamp = moment();
      }
    }, 1000);
  }
}

export { YYDoorLock };
