/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ROOM_VIEW = "SET_ROOM_VIEW";
export const SET_ALL_ROOM_VIEW_LIST = "SET_ALL_ROOM_VIEW_LIST";
export const SET_ROOM_VIEW_SELECTED = "SET_ROOM_VIEW_SELECTED";
export const SET_ROOM_VIEW_DISPLAY_LAYER = "SET_ROOM_VIEW_DISPLAY_LAYER";
export const SET_ROOM_VIEW_FILTER = "SET_ROOM_VIEW_FILTER";
export const SET_ROOM_VIEW_MODE = "SET_ROOM_VIEW_MODE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomViewList = (all_room_views) => ({
  type: SET_ALL_ROOM_VIEW_LIST,
  all_room_views,
});

const setRoomView = (room_view) => ({
  type: SET_ROOM_VIEW,
  room_view,
});

const setRoomViewSelected = (selected) => ({
  type: SET_ROOM_VIEW_SELECTED,
  selected,
});

const setDisplayRoomLayer = (displayLayer) => ({
  type: SET_ROOM_VIEW_DISPLAY_LAYER,
  displayLayer,
});

const setViewMode = (mode) => ({
  type: SET_ROOM_VIEW_MODE,
  mode,
});

const setViewFilter = (filter) => ({
  type: SET_ROOM_VIEW_FILTER,
  filter,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomView list */
function getRoomViewList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/view/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_views } }) => {
        if (success) {
          dispatch(setAllRoomViewList(all_room_views));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomView By Id */
function getRoomView(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/view/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_view } }) => {
        if (success) {
          dispatch(setRoomView(room_view));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomView  */
function newRoomView(place_id, room_view) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/view/${place_id}`, room_view)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomViewList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomView  */
function putRoomView(id, room_view) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/view/${id}`, room_view)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomView(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Delete RoomView  */
function delRoomView(place_id, id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/view/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomViewList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  setRoomViewSelected,
  getRoomViewList,
  setRoomView,
  getRoomView,
  newRoomView,
  putRoomView,
  delRoomView,
  setViewMode,
  setViewFilter,
  setDisplayRoomLayer,
};

export { actionCreators };
