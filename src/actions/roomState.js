/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import moment from "moment";
// import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_STATE_LIST = "SET_ALL_ROOM_STATE_LIST";
export const SET_ROOM_STATE = "SET_ROOM_STATE";
export const ADD_ROOM_STATE_LOG_LIST = "ADD_ROOM_STATE_LOG_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomStateList = (all_room_states) => ({
  type: SET_ALL_ROOM_STATE_LIST,
  all_room_states,
});

const setRoomState = (room_state) => ({
  type: SET_ROOM_STATE,
  room_state,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomState all list */
function getRoomStateList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/all/sale/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_states } }) => {
        if (success) {
          dispatch(setAllRoomStateList(all_room_states));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomState  */
function getRoomState(room_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/sale/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_state } }) => {
        if (success) {
          dispatch(setRoomState(room_state[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomState  */
function newRoomState(room_state) {
  // console.log("-> new room_state ", room_state);

  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/state`, room_state)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_state: item } = room_state; // room_state.room_state -> room_state 로 구조 분해 할당.

          dispatch(getRoomState(item.room_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomState  */
function putRoomState(room_id, room_state, callback) {
  const {
    room_state: { headers = {} },
  } = room_state;

  let config = {
    headers,
  };

  delete room_state.room_state.headers;

  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/state/${room_id}`, room_state, config)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_state: item } = room_state; // room_state.room_state -> room_state 로 구조 분해 할당.
          item.room_id = room_id; // room_id 추가.

          dispatch(setRoomState(item));

          if (callback) callback(info);

          delete room_state.room_id;
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomState all  */
function putRoomStateAll(place_id, room_state, callback) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/state/all/${place_id}`, room_state)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomStateList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomStateList,
  getRoomState,
  newRoomState,
  putRoomState,
  putRoomStateAll,
};

export { actionCreators };
