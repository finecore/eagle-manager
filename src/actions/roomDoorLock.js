/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import moment from "moment";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_DOORLOCK_LIST = "SET_ALL_ROOM_DOORLOCK_LIST";
export const SET_SEARCH_ROOM_DOORLOCK_LIST = "SET_SEARCH_ROOM_DOORLOCK_LIST";
export const SET_ROOM_DOORLOCK_LIST = "SET_ROOM_DOORLOCK_LIST";
export const SET_ROOM_DOORLOCK = "SET_ROOM_DOORLOCK";
export const SET_ROOM_DOORLOCK_LOGIN = "SET_ROOM_DOORLOCK_LOGIN";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomDoorLockList = (all_room_doorlocks) => ({
  type: SET_ALL_ROOM_DOORLOCK_LIST,
  all_room_doorlocks,
});

const setSearchRoomDoorLockList = (all_room_doorlocks) => ({
  type: SET_SEARCH_ROOM_DOORLOCK_LIST,
  all_room_doorlocks,
});

const setRoomDoorLocks = (room_doorlocks) => ({
  type: SET_ROOM_DOORLOCK_LIST,
  room_doorlocks,
});

const setRoomDoorLock = (room_doorlock) => ({
  type: SET_ROOM_DOORLOCK,
  room_doorlock,
});

const setRoomDoorLockLogin = (login) => ({
  type: SET_ROOM_DOORLOCK_LOGIN,
  login,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  plaec doorlock list */
function getRoomDoorLockList(placeId, begin, end, params = {}) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/doorlock/all/${placeId}/${begin}/${end}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_doorlocks } }) => {
        if (success) {
          dispatch(setAllRoomDoorLockList(all_room_doorlocks));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomDoorLock by room_id */
function getRoomDoorLockByRoomId(room_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/doorlock/roomid/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_doorlock } }) => {
        if (success) {
          dispatch(setRoomDoorLock(room_doorlock[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getRoomDoorLock(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/doorlock/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_doorlock } }) => {
        if (success) {
          dispatch(setRoomDoorLock(room_doorlock[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getRoomDoorLockQR(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/doorlock/qr/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_doorlock } }) => {
        if (success) {
          dispatch(setRoomDoorLock(room_doorlock[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomDoorLock  */
function newRoomDoorLock(room_doorlock) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/doorlock/`, room_doorlock)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomDoorLock(info.insertId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomDoorLock  */
function putRoomDoorLock(id, room_doorlock) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/doorlock/${id}`, room_doorlock)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomDoorLock(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function delRoomDoorLock(id) {
  return (dispatch, getType) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/doorlock/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function clearRoomDoorLock(id) {
  return (dispatch, getType) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/doorlock/clear/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomDoorLock(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function loginQrCode(data) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/doorlock/login`, data)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { data } }) => {
        if (success) {
          dispatch(setRoomDoorLockLogin(data));
        }
        return { success, data };
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function sendQrCode(id, data) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/doorlock/send/${id}`, data)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, qr_link } }) => {
        return { success, error, info, qr_link };
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomDoorLockList,
  getRoomDoorLockByRoomId,
  getRoomDoorLock,
  getRoomDoorLockQR,
  newRoomDoorLock,
  putRoomDoorLock,
  delRoomDoorLock,
  clearRoomDoorLock,
  loginQrCode,
  sendQrCode,
};

export { actionCreators };
