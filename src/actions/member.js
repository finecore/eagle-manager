/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_MEMBER_LIST = "SET_MEMBER_LIST";
export const SET_MEMBER = "SET_MEMBER";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setMemberList = (members) => ({
  type: SET_MEMBER_LIST,
  members,
});

const setMember = (member) => ({
  type: SET_MEMBER,
  member,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Member list */
function getMemberList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/member/place/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { members } }) => {
        if (success) {
          dispatch(setMemberList(members));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get member */
function getMember(placeId, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/member/${placeId}/phone/${phone}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { member } }) => {
        if (success) {
          dispatch(setMember(member[0]));
        }
        return member[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Member  */
function putMember(placeId, phone, member) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/member/${placeId}/phone/${phone}`, member)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { member: item } = member; // 구조 분해 할당.
          item.phone = phone;
          dispatch(setMember(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Member  */
function newMember(member) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/member/`, member)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { member: item } = member; // 구조 분해 할당.
          dispatch(setMember(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getMemberList,
  getMember,
  putMember,
  newMember,
};

export { actionCreators };
