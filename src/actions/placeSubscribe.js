/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_PLACE_SUBSCRIBE = "SET_PLACE_SUBSCRIBE";
export const SET_PLACE_SUBSCRIBE_LIST = "SET_PLACE_SUBSCRIBE_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setPlaceSubscribe = (placeSubscribe) => ({
  type: SET_PLACE_SUBSCRIBE,
  placeSubscribe,
});

const setPlaceSubscribeList = (placeSubscribes) => ({
  type: SET_PLACE_SUBSCRIBE_LIST,
  placeSubscribes,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  PlaceSubscribe list */
function getPlaceSubscribeList(placeId) {
  return (dispatch, getState) => {
    let filter = `a.place_id=${placeId} and b.use_yn='Y'`;
    ajax
      .get(`${envProps.API_SERVER_URL}/place/subscribe/list/${filter}`) // api 업데이트 후 삭제.
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { place_subscribes } }) => {
        if (success) {
          dispatch(setPlaceSubscribeList(place_subscribes));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* PlaceSubscribe By Id */
function getPlaceSubscribe(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/place/subscribe/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { place_subscribe } }) => {
        if (success) {
          dispatch(setPlaceSubscribe(place_subscribe[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* PlaceSubscribe add */
function newPlaceSubscribe(placeSubscribe) {
  return (dispatch, getState) => {
    return (
      ajax
        .post(`${envProps.API_SERVER_URL}/place/subscribe`, placeSubscribe)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            const {
              place_subscribe: { place_id },
            } = placeSubscribe;
            dispatch(getPlaceSubscribeList(place_id));
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* PlaceSubscribe update */
function putPlaceSubscribe(id, placeSubscribe) {
  return (dispatch, getState) => {
    return (
      ajax
        .put(`${envProps.API_SERVER_URL}/place/subscribe/${id}`, placeSubscribe)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            const {
              place_subscribe: { place_id },
            } = placeSubscribe;
            dispatch(getPlaceSubscribeList(place_id));
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* PlaceSubscribe delete */
function delPlaceSubscribe(place_id, id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/place/subscribe/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getPlaceSubscribeList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getPlaceSubscribeList,
  getPlaceSubscribe,
  newPlaceSubscribe,
  putPlaceSubscribe,
  delPlaceSubscribe,
};

export { actionCreators };
