/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_PLACE_LIST = "SET_PLACE_LIST";
export const SET_PLACE = "SET_PLACE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setPlaceList = (places) => ({
  type: SET_PLACE_LIST,
  places,
});

const setPlace = (place, id) => ({
  type: SET_PLACE,
  place,
  id,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Place list */
function getPlaceList(filter, order = "a.id", desc = "asc", limit = 300) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/place/list/${filter}/${order}/${desc}/${limit}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { places } }) => {
        if (success) {
          dispatch(setPlaceList(places));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get place */
function getPlace(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/place/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { place } }) => {
        if (success) {
          dispatch(setPlace(place[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Place  */
function putPlace(id, place) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/place/${id}`, place)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { place } }) => {
        if (success) {
          dispatch(getPlace(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getPlaceList,
  getPlace,
  putPlace,
  setPlace,
};

export { actionCreators };
