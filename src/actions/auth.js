/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import { actionCreators as wsAction } from "./ws";
import Swal from "sweetalert2";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SUCCESS_LOGIN = "SUCCESS_LOGIN";
export const SUCCESS_TEMP_LOGIN = "SUCCESS_TEMP_LOGIN";
export const FAILURE_LOGIN = "FAILURE_LOGIN";
export const SUCCESS_LOGOUT = "SUCCESS_LOGOUT";
export const FAILURE_LOGOUT = "FAILURE_LOGOUT";
export const DUP_ACCESS_FAILUE = "DUP_ACCESS_FAILUE";
export const DUP_ACCESS_LOGOUT = "DUP_ACCESS_LOGOUT";
export const SET_UUID = "SET_UUID";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const succssLogin = (user, token, isViewerLogin = false) => ({
  type: SUCCESS_LOGIN,
  user,
  token,
  isViewerLogin,
});

const succssTempLogin = (user, token) => ({
  type: SUCCESS_TEMP_LOGIN,
  user,
  token,
});

const succLogout = () => ({
  type: SUCCESS_LOGOUT,
});

const setUuid = (uuid) => ({
  type: SET_UUID,
  uuid,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/* Login */
function doLogin(body) {
  return (dispatch, getState) => {
    // Ajax as Axios.
    ajax
      .post(`${envProps.API_SERVER_URL}/login`, body)
      .then((res) => api.checkResponse(res, dispatch))
      // 구조 분해 할당
      .then(({ common: { success, message, error }, body: { user, token } }) => {
        if (success) {
          let { licence_yn, type } = user;

          if (licence_yn === "Y" || type === 1) {
            dispatch(succssLogin(user, token));
            // user 값을 web socket server session 에 설정 한다.(CCU, FSC 와 통신시 웹매니저와 연동을 위한 키는 place_id 이다.)
            dispatch(wsAction.join(user, true));
          } else {
            Swal.fire({
              icon: "error",
              title: "라이선스 오류",
              html: `접속 라이선스가 없습니다. <br/><br/>업소 관리자에게 문의 하여 주세요.`,
              showCloseButton: false,
              confirmButtonText: "확인",
            }).then((result) => {});

            // 웹소켓 종료(재접속됨)
            wsAction.close();
          }
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Logout */
function doLogout(body) {
  return (dispatch, getState) => {
    const {
      auth: { user },
    } = getState();

    dispatch(wsAction.logout(user, true));

    setTimeout(() => {
      dispatch(succLogout());

      // 웹소켓 종료(재접속됨)
      wsAction.close();
    }, 100);
  };
}

/* Temp Login */
function tempLogin(user, token) {
  console.log("- tempLogin", { user, token });
  return (dispatch, getState) => {
    dispatch(succssTempLogin(user, token));
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  doLogin,
  doLogout,
  tempLogin,
  setUuid,
  succssLogin,
};

export { actionCreators };
