/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import moment from "moment";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_RESERV_LIST = "SET_ALL_ROOM_RESERV_LIST";
export const SET_SEARCH_ROOM_RESERV_LIST = "SET_SEARCH_ROOM_RESERV_LIST";
export const SET_ROOM_RESERV_LIST = "SET_ROOM_RESERV_LIST";
export const SET_ROOM_RESERV = "SET_ROOM_RESERV";
export const SET_PLACE_ENABLE_RESERV_LIST = "SET_PLACE_ENABLE_RESERV_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomReservList = (all_room_reservs) => ({
  type: SET_ALL_ROOM_RESERV_LIST,
  all_room_reservs,
});

const setSearchRoomReservList = (all_room_reservs) => ({
  type: SET_SEARCH_ROOM_RESERV_LIST,
  all_room_reservs,
});

const setRoomReservs = (room_reservs) => ({
  type: SET_ROOM_RESERV_LIST,
  room_reservs,
});

const setPlaceEnableReservs = (room_reservs) => ({
  type: SET_PLACE_ENABLE_RESERV_LIST,
  room_reservs,
});

const setRoomReserv = (room_reserv) => ({
  type: SET_ROOM_RESERV,
  room_reserv,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  plaec reserv list */
function getRoomReservList(placeId, begin, end, params = {}) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/reserv/all/${placeId}/${begin}/${end}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_reservs } }) => {
        if (success) {
          dispatch(setAllRoomReservList(all_room_reservs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getSearchRoomReservList(placeId, begin, end, params = {}) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/reserv/all/${placeId}/${begin}/${end}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_reservs } }) => {
        if (success) {
          // Room componentWillReceiveProps roomReserv 화면 갱신 방지! (속도 저하됨)
          dispatch(setSearchRoomReservList(all_room_reservs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getNowRoomReservs(placeId, params = {}) {
  // 퇴실 시간이 현재 이후 부터 1 개월 이내 까지 예약만 조회.
  let begin = moment().format("YYYY-MM-DD 00:00");
  let end = moment().add(1, "year").format("YYYY-MM-DD HH:mm");

  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/reserv/all/${placeId}/${begin}/${end}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_reservs } }) => {
        if (success) {
          dispatch(setAllRoomReservList(all_room_reservs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get all room reserv list    */
function getRoomReservs(filter, begin, end) {
  let url = `room/reserv/list/${filter}/${begin}/${end}`;

  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/${url}`, {})
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_reservs } }) => {
        if (success) {
          dispatch(setRoomReservs(room_reservs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get enable room reserv list    */
function getPlaceEnableReservs(place_id, params = {}) {
  let url = "room/reserv/enable/" + place_id;

  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/${url}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_reservs } }) => {
        if (success) {
          dispatch(setPlaceEnableReservs(room_reservs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomReserv by id */
function getRoomReserv(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/reserv/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_reserv } }) => {
        if (success) {
          dispatch(setRoomReserv(room_reserv[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getRoomReservQR(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/reserv/qr/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_reserv } }) => {
        if (success) {
          dispatch(setRoomReserv(room_reserv[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomReserv  */
function newRoomReserv(room_reserv) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/reserv/`, room_reserv)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const {
            room_reserv: { place_id },
          } = room_reserv;

          dispatch(place_id ? getNowRoomReservs(place_id) : getRoomReserv(info.insertId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomReserv  */
function putRoomReserv(id, room_reserv) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/reserv/${id}`, room_reserv)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const {
            room_reserv: { place_id },
          } = room_reserv;

          dispatch(place_id ? getNowRoomReservs(place_id) : getRoomReserv(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomReservList,
  getSearchRoomReservList,
  getNowRoomReservs,
  getPlaceEnableReservs,
  getRoomReservs,
  getRoomReserv,
  getRoomReservQR,
  newRoomReserv,
  putRoomReserv,
};

export { actionCreators };
