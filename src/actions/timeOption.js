/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_TIME_OPTION = "SET_TIME_OPTION";
export const SET_TIME_OPTION_LIST = "SET_TIME_OPTION_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setTimeOption = (time_option) => ({
  type: SET_TIME_OPTION,
  time_option,
});

const setTimeOptionList = (time_options) => ({
  type: SET_TIME_OPTION_LIST,
  time_options,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  TimeOption list */
function getTimeOptions(place_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/time/option/place/${place_id}`) // api 업데이트 후 삭제.
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { time_options } }) => {
        if (success) {
          dispatch(setTimeOptionList(time_options));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* TimeOption By Id */
function getTimeOption(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/time/option/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { time_option } }) => {
        if (success) {
          dispatch(setTimeOption(time_option[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* TimeOption add */
function newTimeOption(time_option) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/time/option`, time_option)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let {
            time_option: { place_id },
          } = time_option;
          dispatch(getTimeOptions(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* TimeOption update */
function putTimeOption(id, time_option) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/time/option/${id}`, time_option)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let {
            time_option: { place_id },
          } = time_option;
          dispatch(getTimeOptions(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* TimeOption delete */
function delTimeOption(place_id, id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/time/option/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getTimeOptions(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getTimeOptions,
  getTimeOption,
  newTimeOption,
  putTimeOption,
  delTimeOption,
};

export { actionCreators };
