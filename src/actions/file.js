/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_FILE_LIST = "SET_FILE_LIST";
export const SET_FILE = "SET_FILE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setFileList = (files) => ({
  type: SET_FILE_LIST,
  files,
});

const setFile = (file) => ({
  type: SET_FILE,
  file,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  File list */
function getFileList(filter) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/file/list/${filter}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { files } }) => {
        if (success) {
          dispatch(setFileList(files));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get file */
function getFile(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/file/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { file } }) => {
        if (success) {
          dispatch(setFile(file[0]));
        }
        return file[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set File  */
function putFile(id, file) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/file/${id}`, file)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { file: item } = file; // 구조 분해 할당.
          dispatch(setFile(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New File  */
function newFile(file) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/file/`, file)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { file: item } = file; // 구조 분해 할당.
          dispatch(setFile(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del File   */
function delFile(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/file/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setFile(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getFileList,
  getFile,
  putFile,
  newFile,
  delFile,
};

export { actionCreators };
