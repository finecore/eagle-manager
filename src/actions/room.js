/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ROOM_LIST = "SET_ROOM_LIST";
export const SET_ROOM = "SET_ROOM";
export const SET_ROOM_SELECTED = "SET_ROOM_SELECTED";
export const SET_ROOM_ENABLE_RESERV = "SET_ROOM_ENABLE_RESERV";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setRoomList = (rooms) => ({
  type: SET_ROOM_LIST,
  rooms,
});

const setRoom = (room) => ({
  type: SET_ROOM,
  room,
});

const setRoomEnableReserv = (rooms) => ({
  type: SET_ROOM_ENABLE_RESERV,
  rooms,
});

const setRoomSelected = (selected) => ({
  type: SET_ROOM_SELECTED,
  selected,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Room list */
function getRoomList(placeId, callback) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { rooms } }) => {
        if (success) {
          dispatch(setRoomList(rooms));
          if (callback) callback(rooms);
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get Room  */
function getRoom(id, callback) {
  return (dispatch, getType) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room } }) => {
        if (success) {
          dispatch(setRoom(room[0]));

          if (callback) callback(room[0]);
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get all room enable reserv list    */
function getRoomEnableReservs(place_id, room_id, params = {}) {
  let url = "room/enable/reserv/" + place_id;
  if (room_id) url += "/" + room_id;

  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/${url}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { rooms } }) => {
        if (success) {
          dispatch(setRoomEnableReserv(rooms));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Post Room  */
function newRoom(item, callback) {
  return (dispatch, getType) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room`, item)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          const { room } = item;
          dispatch(getRoomList(room.place_id));
        }

        if (callback) callback({ info, error });

        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Put Room  */
function putRoom(id, item, callback) {
  return (dispatch, getType) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/${id}`, item)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          const { room } = item;
          dispatch(room.place_id ? getRoomList(room.place_id) : getRoom(id));
        }

        if (callback) callback({ info, error });

        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del Room  */
function delRoom(id) {
  return (dispatch, getType) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const room = {
            id,
          };
          dispatch(setRoom(room));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  newRoom,
  getRoom,
  putRoom,
  delRoom,
  getRoomList,
  getRoomEnableReservs,
  setRoomSelected,
};

export { actionCreators };
