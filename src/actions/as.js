/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_AS_LIST = "SET_AS_LIST";
export const SET_AS = "SET_AS";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAsList = (ases, isNotifi) => ({
  type: SET_AS_LIST,
  ases,
  isNotifi,
});

const setAs = (ase) => ({
  type: SET_AS,
  ase,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  As list */
function getAsList(filter, between, begin, end, isNotifi) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/as/list/${filter}/${between}/${begin}/${end}/1000`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { ases } }) => {
        if (success) {
          dispatch(setAsList(ases, isNotifi));
        }
        return ases;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get ase */
function getAs(id, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/as/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { as } }) => {
        if (success) {
          dispatch(setAs(as[0]));
        }
        return as[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set As  */
function putAs(id, as) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/as/${id}`, as)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { as: item } = as; // 구조 분해 할당.
          dispatch(setAs(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New As  */
function newAs(as) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/as/`, as)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { as: item } = as; // 구조 분해 할당.
          dispatch(setAs(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del As  */
function delAs(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/as/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setAs(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getAsList,
  getAs,
  putAs,
  newAs,
  delAs,
};

export { actionCreators };
