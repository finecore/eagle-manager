/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_PLACE_LOCK_LIST = "SET_ALL_PLACE_LOCK_LIST";
export const SET_PLACE_LOCK = "SET_PLACE_LOCK";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllPlaceLockList = (all_place_locks) => ({
  type: SET_ALL_PLACE_LOCK_LIST,
  all_place_locks,
});

const setPlaceLock = (place_lock) => ({
  type: SET_PLACE_LOCK,
  place_lock,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  PlaceLock all list */
function getPlaceLocList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/place/lock/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_place_locks } }) => {
        if (success) {
          dispatch(setAllPlaceLockList(all_place_locks));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get PlaceLock by lock */
function getPlaceLock(id, lock) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/place/lock/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { place_lock } }) => {
        if (success) {
          dispatch(setPlaceLock(place_lock[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New PlaceLock  */
function newPlaceLock(place_lock) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/place/lock/`, place_lock)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { place_lock: item } = place_lock;
          item.id = info.insertId; // id 추가.
          dispatch(setPlaceLock(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set PlaceLock  */
function putPlaceLock(id, place_lock) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/place/lock/${id}`, place_lock)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { place_lock: item } = place_lock;
          item.id = id; // id 추가.
          dispatch(setPlaceLock(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del PlaceLock  */
function delPlaceLock(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/place/lock/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const ietm = { id }; // id 만 넘겨서 삭제 한다.
          dispatch(setPlaceLock(ietm));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  setPlaceLock,
  getPlaceLocList,
  getPlaceLock,
  newPlaceLock,
  putPlaceLock,
  delPlaceLock,
};

export { actionCreators };
