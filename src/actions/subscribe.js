/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_SUBSCRIBE = "SET_SUBSCRIBE";
export const SET_SUBSCRIBE_LIST = "SET_SUBSCRIBE_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setSubscribe = (subscribe) => ({
  type: SET_SUBSCRIBE,
  subscribe,
});

const setSubscribeList = (subscribes) => ({
  type: SET_SUBSCRIBE_LIST,
  subscribes,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Subscribe list */
function getSubscribeList() {
  return (dispatch, getState) => {
    let filter = "a.use_yn='Y'";
    ajax
      .get(`${envProps.API_SERVER_URL}/subscribe/list/${filter}`) // api 업데이트 후 삭제.
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { subscribes } }) => {
        if (success) {
          dispatch(setSubscribeList(subscribes));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Subscribe By Id */
function getSubscribe(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/subscribe/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { subscribe } }) => {
        if (success) {
          dispatch(setSubscribe(subscribe[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Subscribe add */
function newSubscribe(subscribe) {
  return (dispatch, getState) => {
    return (
      ajax
        .post(`${envProps.API_SERVER_URL}/subscribe`, subscribe)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            dispatch(getSubscribeList());
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* Subscribe update */
function putSubscribe(id, subscribe) {
  return (dispatch, getState) => {
    return (
      ajax
        .put(`${envProps.API_SERVER_URL}/subscribe/${id}`, subscribe)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            dispatch(getSubscribeList());
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* Subscribe delete */
function delSubscribe(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/subscribe/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { subscribe } }) => {
        if (success) {
          dispatch(getSubscribeList());
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getSubscribeList,
  getSubscribe,
  newSubscribe,
  putSubscribe,
  delSubscribe,
};

export { actionCreators };
