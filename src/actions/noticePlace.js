/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_NOTICE_PLACE_LIST = "SET_NOTICE_PLACE_LIST";
export const SET_NOTICE_PLACE = "SET_NOTICE_PLACE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setNoticePlaceList = (notice_places) => ({
  type: SET_NOTICE_PLACE_LIST,
  notice_places,
});

const setNoticePlace = (notice_place) => ({
  type: SET_NOTICE_PLACE,
  notice_place,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  NoticePlace list */
function getNoticePlaceList(filter, begin, end) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/notice/place/list/${filter}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { notice_places } }) => {
        if (success) {
          dispatch(setNoticePlaceList(notice_places));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get notice_place */
function getNoticePlace(id, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/notice/place/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { notice_place } }) => {
        if (success) {
          dispatch(setNoticePlace(notice_place[0]));
        }
        return notice_place[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set NoticePlace  */
function putNoticePlace(id, notice_place) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/notice/place/${id}`, notice_place)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { notice_place: item } = notice_place; // 구조 분해 할당.
          dispatch(setNoticePlace(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New NoticePlace  */
function newNoticePlace(notice_place) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/notice/place/`, notice_place)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { notice_place: item } = notice_place; // 구조 분해 할당.
          dispatch(setNoticePlace(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del NoticePlace  */
function delNoticePlace(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/notice/place/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setNoticePlace(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getNoticePlaceList,
  getNoticePlace,
  putNoticePlace,
  newNoticePlace,
  delNoticePlace,
};

export { actionCreators };
