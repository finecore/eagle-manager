/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_SALE_LIST = "SET_ALL_ROOM_SALE_LIST";
export const SET_ROOM_SALE_LIST = "SET_ROOM_SALE_LIST";
export const SET_ROOM_SALE = "SET_ROOM_SALE";
export const SET_ROOM_SALE_TODAY = "SET_ROOM_SALE_TODAY";
export const SET_PLACE_ROOM_SALE_SUM = "SET_PLACE_ROOM_SALE_SUM";
export const SET_ROOM_SALE_SUM_ROOMS = "SET_ROOM_SALE_SUM_ROOMS";
export const SET_ROOM_SALE_SUM = "SET_ROOM_SALE_SUM";
export const SET_ROOM_SALE_SEARCH_LIST = "SET_ROOM_SALE_SEARCH_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomSaleList = (all_room_sales) => ({
  type: SET_ALL_ROOM_SALE_LIST,
  all_room_sales,
});

const setRoomSales = (room_sales, room_id) => ({
  type: SET_ROOM_SALE_LIST,
  room_sales,
  room_id,
});

const setRoomSearchSales = (room_sales) => ({
  type: SET_ROOM_SALE_SEARCH_LIST,
  room_sales,
});

const setRoomSale = (room_sale) => ({
  type: SET_ROOM_SALE,
  room_sale,
});

const setRoomSaleToday = (room_sales) => ({
  type: SET_ROOM_SALE_TODAY,
  room_sales,
});

const setPlaceRoomSaleSum = (room_sales) => ({
  type: SET_PLACE_ROOM_SALE_SUM,
  room_sales,
});

const setPlaceRoomSaleSumRooms = (room_sales) => ({
  type: SET_ROOM_SALE_SUM_ROOMS,
  room_sales,
});

const setRoomSaleSum = (room_id, room_sales) => ({
  type: SET_ROOM_SALE_SUM,
  room_id,
  room_sales,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  plaec sale list */
function getRoomSaleList(placeId) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_sales } }) => {
        if (success) {
          dispatch(setAllRoomSaleList(all_room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getNowRoomSales(placeId) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/now/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_sales } }) => {
        if (success) {
          dispatch(setAllRoomSaleList(all_room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getSerachRoomSales(placeId, begin, end) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/all/${placeId}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_sales } }) => {
        if (success) {
          dispatch(setRoomSearchSales(all_room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get all room sale list    */
function getRoomSales(room_id, params = {}) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/list/${room_id}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sales } }) => {
        if (success) {
          dispatch(setRoomSales(room_sales, room_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSale by id */
function getRoomSale(id) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale } }) => {
        if (success) {
          dispatch(setRoomSale(room_sale[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSale Sum Place */
function getPlaceRoomSaleSum(place_id, begin, end) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/sum/place/${place_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sales } }) => {
        if (success) {
          dispatch(setPlaceRoomSaleSum(room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSale Sum Place Rooms*/
function getPlaceRoomSaleSumRooms(place_id, begin, end) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/sum/room/${place_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sales } }) => {
        if (success) {
          dispatch(setPlaceRoomSaleSumRooms(room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSale Today */
function getRoomSaleToday(room_id) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/today/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sales } }) => {
        if (success) {
          dispatch(setRoomSaleToday(room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSale Sum */
function getRoomSaleSum(room_id, begin, end) {
  return (dispatch, getSale) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/sale/sum/${room_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sales } }) => {
        if (success) {
          dispatch(setRoomSaleSum(room_id, room_sales));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomSale  */
function newRoomSale(room_sale, callback) {
  return (dispatch, getSale) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/sale`, room_sale)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          if (callback) callback({ info });
          if (info && info.insertId) dispatch(getRoomSale(info.insertId));
        } else {
          if (callback) callback({ error });
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomSale  */
function putRoomSale(id, room_sale, callback) {
  return (dispatch, getSale) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/sale/${id}`, room_sale)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          const { room_sale: item } = room_sale; // room_sale.room_sale -> room_sale 로 구조 분해 할당.
          item.id = id; // id 추가.
          dispatch(setRoomSale(item));
        }

        if (callback) callback({ info, error });
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomSaleList,
  getNowRoomSales,
  getSerachRoomSales,
  getRoomSales,
  getRoomSale,
  getPlaceRoomSaleSum,
  getPlaceRoomSaleSumRooms,
  getRoomSaleToday,
  getRoomSaleSum,
  newRoomSale,
  putRoomSale,
};

export { actionCreators };
