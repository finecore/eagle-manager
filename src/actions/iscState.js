/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ISC_STATE = "SET_ISC_STATE";
export const SET_ISC_STATE_LIST = "SET_ISC_STATE_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setIscStateList = (isc_states) => ({
  type: SET_ISC_STATE_LIST,
  isc_states,
});

const setIscState = (isc_state) => ({
  type: SET_ISC_STATE,
  isc_state,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/* Get isc state   */
function getIscStateList(place_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/state/place/${place_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_states } }) => {
        if (success) {
          dispatch(setIscStateList(isc_states));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get isc state   */
function getIscState(serialno) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/state/${serialno}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_state } }) => {
        if (success) {
          dispatch(setIscState(isc_state));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New IscState  */
function newIscState(isc_state) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/isc/set`, isc_state)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { isc_state: item } = isc_state; // 구조 분해 할당.
          item.id = info.insertId; // id 추가.
          dispatch(setIscState(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* State IscState  */
function putIscState(serialno, isc_state) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/isc/state/${serialno}`, isc_state)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { isc_state: item } = isc_state; // 구조 분해 할당.
          item.serialno = serialno; // id 추가.
          dispatch(setIscState(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del IscState  */
function delIscState(serialno, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/isc/state/${serialno}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { serialno, deleted: true };
          dispatch(setIscState(item));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getIscState,
  getIscStateList,
  newIscState,
  putIscState,
  delIscState,
};

export { actionCreators };
