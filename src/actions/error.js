/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ERR = "SET_ERR";
export const SET_AUTH_ERR = "SET_AUTH_ERR";
export const SET_METHOD_ERR = "SET_METHOD_ERR";
export const SET_API_STATUS = "SET_API_STATUS";
export const SET_CLEAR_ERR = "SET_CLEAR_ERR";

export const ERR_CODE_DEFAULT = 999;
export const ERR_CODE_AUTH = 401;
export const ERR_CODE_HOST = 400;

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function setErr(error) {
  const { code = ERR_CODE_DEFAULT, message = "요청 처리 중 오류가 발생 했습니다.", detail, config, logout = false } = error;

  return {
    type: SET_ERR,
    code,
    message,
    detail,
    config,
    logout,
  };
}

function setAuthErr(error) {
  const { code = ERR_CODE_AUTH, message = "권한이 없습니다.", detail = "관리자에게 문의해 주세요.", config, logout = false } = error;

  return {
    type: SET_AUTH_ERR,
    code,
    message,
    detail,
    config,
    logout,
  };
}

function setHostErr(error = {}) {
  const { code = ERR_CODE_HOST, message = "서버 처리 중 오류가 발생 했습니다.", detail, config, logout = false } = error;

  return {
    type: SET_METHOD_ERR,
    code,
    message,
    detail,
    config,
    logout,
  };
}

function clearError() {
  return {
    type: SET_CLEAR_ERR,
    code: null,
    message: null,
    detail: null,
    logout: false,
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////

const actionCreators = {
  setErr,
  setAuthErr,
  setHostErr,
  clearError,
};

export { actionCreators };
