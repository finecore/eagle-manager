/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_SALE_PAY_LIST = "SET_ALL_ROOM_SALE_PAY_LIST";
export const SET_ROOM_SALE_PAY_LIST = "SET_ROOM_SALE_PAY_LIST";
export const SET_ROOM_SALE_PAY = "SET_ROOM_SALE_PAY";
export const SET_ROOM_SALE_PAY_TODAY = "SET_ROOM_SALE_PAY_TODAY";
export const SET_PLACE_ROOM_SALE_PAY_SUM = "SET_PLACE_ROOM_SALE_PAY_SUM";
export const SET_ROOM_SALE_PAY_SUM_ALL = "SET_ROOM_SALE_PAY_SUM_ALL";
export const SET_ROOM_SALE_PAY_SUM = "SET_ROOM_SALE_PAY_SUM";
export const SET_ROOM_SALE_PAY_SEARCH_LIST = "SET_ROOM_SALE_PAY_SEARCH_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomSalePayList = (all_room_sale_pays) => ({
  type: SET_ALL_ROOM_SALE_PAY_LIST,
  all_room_sale_pays,
});

const setRoomSalePays = (room_sale_pays, room_id) => ({
  type: SET_ROOM_SALE_PAY_LIST,
  room_sale_pays,
  room_id,
});

const setRoomSearchSalePays = (room_sale_pays) => ({
  type: SET_ROOM_SALE_PAY_SEARCH_LIST,
  room_sale_pays,
});

const setRoomSalePay = (room_sale_pay) => ({
  type: SET_ROOM_SALE_PAY,
  room_sale_pay,
});

const setRoomSalePayToday = (room_sale_pays) => ({
  type: SET_ROOM_SALE_PAY_TODAY,
  room_sale_pays,
});

const setPlaceRoomSalePaySum = (room_sale_pays) => ({
  type: SET_PLACE_ROOM_SALE_PAY_SUM,
  room_sale_pays,
});

const setAllRoomSalePaySum = (room_sale_pays) => ({
  type: SET_ROOM_SALE_PAY_SUM_ALL,
  room_sale_pays,
});

const setRoomSalePaySum = (room_id, room_sale_pays) => ({
  type: SET_ROOM_SALE_PAY_SUM,
  room_id,
  room_sale_pays,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  plaec sale list */
function getRoomSalePayList(placeId) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_sale_pays } }) => {
        if (success) {
          dispatch(setAllRoomSalePayList(all_room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

function getSerachRoomSalePays(placeId, begin, end) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/all/${placeId}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_sale_pays } }) => {
        if (success) {
          dispatch(setRoomSearchSalePays(all_room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get all room sale list    */
function getRoomSalePays(room_id, params = {}) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/list/${room_id}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pays } }) => {
        if (success) {
          dispatch(setRoomSalePays(room_sale_pays, room_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSalePay by id */
function getRoomSalePay(id) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pay } }) => {
        if (success) {
          dispatch(setRoomSalePay(room_sale_pay[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSalePay Sum Place */
function getPlaceRoomSalePaySum(place_id, begin, end) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/sum/place/${place_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pays } }) => {
        if (success) {
          dispatch(setPlaceRoomSalePaySum(room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSalePay Today */
function getRoomSalePayToday(room_id) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/today/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pays } }) => {
        if (success) {
          dispatch(setRoomSalePayToday(room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSalePay Sum All rooms */
function getAllRoomSalePaySum(place_id, begin, end) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/sum/all/${place_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pays } }) => {
        if (success) {
          dispatch(setAllRoomSalePaySum(room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomSalePay Sum */
function getRoomSalePaySum(room_id, begin, end) {
  return (dispatch, getSalePay) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/salepay/sum/${room_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_sale_pays } }) => {
        if (success) {
          dispatch(setRoomSalePaySum(room_id, room_sale_pays));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomSalePay  */
function newRoomSalePay(room_sale_pay, callback) {
  return (dispatch, getSalePay) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/sale`, room_sale_pay)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          if (callback) callback({ info });
          if (info && info.insertId) dispatch(getRoomSalePay(info.insertId));
        } else {
          if (callback) callback({ error });
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomSalePay  */
function putRoomSalePay(id, room_sale_pay, callback) {
  console.log("- putRoomSalePay ", id, room_sale_pay);

  return (dispatch, getSalePay) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/salepay/${id}`, room_sale_pay)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          const { room_sale_pay: item } = room_sale_pay; // room_sale_pay.room_sale_pay -> room_sale_pay 로 구조 분해 할당.
          item.id = id; // id 추가.
          dispatch(setRoomSalePay(item));
        }

        if (callback) callback({ info, error });
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomSalePayList,
  getSerachRoomSalePays,
  getRoomSalePays,
  getRoomSalePay,
  getPlaceRoomSalePaySum,
  getRoomSalePayToday,
  getAllRoomSalePaySum,
  getRoomSalePaySum,
  newRoomSalePay,
  putRoomSalePay,
};

export { actionCreators };
