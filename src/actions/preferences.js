/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_PREFERENCE = "SET_PREFERENCE";
export const SET_PREFERENCE_MODIFY = "SET_PREFERENCE_MODIFY";
export const SET_STOP_RELOAD = "SET_STOP_RELOAD";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setPreferences = (preferences, id) => ({
  type: SET_PREFERENCE,
  preferences,
  id,
});

const setModify = (modify) => ({
  type: SET_PREFERENCE_MODIFY,
  modify,
});

const setStopReload = (stop) => ({
  type: SET_STOP_RELOAD,
  stop,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/* Get preferences */
function getPreferenceList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/preferences/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { preferences } }) => {
        if (success) {
          dispatch(setPreferences(preferences[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Preference  */
function newPreferences(preferences) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/preferences/`, preferences)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const {
            preferences: { place_id },
          } = preferences;
          dispatch(getPreferenceList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Preference  */
function putPreferences(id, modify) {
  let { preferences } = modify; // 비구조화 할당.

  _.map(preferences, (v, k) => {
    // 객체는 저장 시 string 형식으로 변환.
    if (typeof v === "object") preferences[k] = JSON.stringify(v);
    else if (v === "null") preferences[k] = ""; // null 값 삭제
  });

  console.log("- putPreferences", preferences);

  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/preferences/${id}`, { preferences }) // 비구조화
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { place_id } = preferences;
          dispatch(getPreferenceList(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getPreferenceList,
  newPreferences,
  putPreferences,
  setModify,
  setStopReload,
};

export { actionCreators };
