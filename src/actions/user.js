/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import { actionCreators as errActions } from "actions/error";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_USER = "SET_USER";
export const SET_USER_LIST = "SET_USER_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setUser = (user) => ({
  type: SET_USER,
  user,
});

const setUserList = (users) => ({
  type: SET_USER_LIST,
  users,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  User list */
function getPlaceUser(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/user/list/place/${placeId}`) // api 업데이트 후 삭제.
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { users } }) => {
        if (success) {
          if (!users.length) {
            dispatch(
              errActions.setErr({
                message: "직원 목록이 존재 하지 않습니다.",
              })
            );
          } else dispatch(setUserList(users));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* User By Id */
function getUser(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/user/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { user } }) => {
        if (success) {
          if (!user[0]) {
            dispatch(
              errActions.setErr({
                message: id + " 직원이 존재 하지 않습니다.",
              })
            );
          } else dispatch(setUser(user[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* User add */
function newUser(user) {
  return (dispatch, getState) => {
    return (
      ajax
        .post(`${envProps.API_SERVER_URL}/user`, user)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            const {
              user: { place_id },
            } = user;
            dispatch(getPlaceUser(place_id));
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* User update */
function putUser(id, user) {
  return (dispatch, getState) => {
    return (
      ajax
        .put(`${envProps.API_SERVER_URL}/user/${id}`, user)
        .then((res) => api.checkResponse(res, dispatch))
        // 구조 분해 할당
        .then(({ common: { success, message, error }, body: { info } }) => {
          if (success) {
            const {
              user: { place_id },
            } = user;
            dispatch(getPlaceUser(place_id));
          }
          return success;
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        })
    );
  };
}

/* User delete */
function delUser(place_id, id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/user/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { user } }) => {
        if (success) {
          dispatch(getPlaceUser(place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getPlaceUser,
  getUser,
  newUser,
  putUser,
  delUser,
};

export { actionCreators };
