/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ISC_SET = "SET_ISC_SET";
export const SET_ISC_SET_LIST = "SET_ISC_SET_LIST";
export const SET_KEY_BOX_DISPLAY_LAYER = "SET_KEY_BOX_DISPLAY_LAYER";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setIscSetList = (isc_sets) => ({
  type: SET_ISC_SET_LIST,
  isc_sets,
});

const setIscSet = (isc_set) => ({
  type: SET_ISC_SET,
  isc_set,
});

const setIscKeyBoxLayer = (displayKeyBox) => ({
  type: SET_KEY_BOX_DISPLAY_LAYER,
  displayKeyBox,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/* Get isc set   */
function getIscSetList(place_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/set/place/${place_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_sets } }) => {
        if (success) {
          dispatch(setIscSetList(isc_sets));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get isc set   */
function getIscSets(place_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/set/place/${place_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_sets } }) => {
        if (success) {
          dispatch(setIscSetList(isc_sets));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get isc set   */
function getIscSet(serialno) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/set/serialno/${serialno}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_set } }) => {
        if (success) {
          dispatch(setIscSet(isc_set));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New IscSet  */
function newIscSet(isc_set) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/isc/set`, isc_set)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { isc_set: item } = isc_set; // 구조 분해 할당.
          item.id = info.insertId; // id 추가.
          dispatch(setIscSet(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set IscSet  */
function putIscSet(id, isc_set) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/isc/set/${id}`, isc_set)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { isc_set: item } = isc_set; // 구조 분해 할당.
          item.id = id; // id 추가.
          dispatch(setIscSet(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del IscSet  */
function delIscSet(id, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/isc/set/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const ietm = { id, deleted: true }; // id 만 넘겨서 삭제 한다.
          dispatch(setIscSet(ietm));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getIscSetList,
  getIscSets,
  getIscSet,
  newIscSet,
  putIscSet,
  delIscSet,
  setIscKeyBoxLayer,
};

export { actionCreators };
