/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_TYPE_LIST = "SET_ALL_ROOM_TYPE_LIST";
export const SET_ROOM_TYPE_LIST = "SET_ROOM_TYPE_LIST";
export const SET_ROOM_TYPE = "SET_ROOM_TYPE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomTypeList = (all_room_types) => ({
  type: SET_ALL_ROOM_TYPE_LIST,
  all_room_types,
});

const setRoomType = (room_type) => ({
  type: SET_ROOM_TYPE,
  room_type,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomType all list */
function getRoomTypeList(placeId) {
  return (dispatch, getType) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/type/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_types } }) => {
        if (success) {
          dispatch(setAllRoomTypeList(all_room_types));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomType by type */
function getRoomType(id, type) {
  return (dispatch, getType) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/type/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_type } }) => {
        if (success) {
          dispatch(setRoomType(room_type[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomType  */
function newRoomType(room_type) {
  return (dispatch, getType) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/type/`, room_type)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomType(info.insertId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomType  */
function putRoomType(id, room_type) {
  return (dispatch, getType) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/type/${id}`, room_type)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomType(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del RoomType  */
function delRoomType(id) {
  return (dispatch, getType) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/type/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const room_type = {
            id,
          };
          dispatch(setRoomType(room_type));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomTypeList,
  getRoomType,
  newRoomType,
  putRoomType,
  delRoomType,
};

export { actionCreators };
