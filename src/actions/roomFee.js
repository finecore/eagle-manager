/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_FEE_LIST = "SET_ALL_ROOM_FEE_LIST";
export const SET_ROOM_FEE_LIST = "SET_ROOM_FEE_LIST";
export const SET_ROOM_FEE_TYPE_LIST = "SET_ROOM_FEE_TYPE_LIST";
export const SET_ROOM_FEE = "SET_ROOM_FEE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomFeeList = (all_room_fees, channel) => ({
  type: SET_ALL_ROOM_FEE_LIST,
  all_room_fees,
  channel,
});

const setRoomFeeType = (room_fees, room_type_id, channel) => ({
  type: SET_ROOM_FEE_TYPE_LIST,
  room_fees,
  room_type_id,
  channel,
});

const setRoomFees = (room_fees, del) => ({
  type: SET_ROOM_FEE_LIST,
  room_fees,
  del,
});

const setRoomFee = (room_fee) => ({
  type: SET_ROOM_FEE,
  room_fee,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  room fee list */
function getRoomFeeList(placeId, channel) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/fee/all/${placeId}/${channel}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_fees } }) => {
        if (success) {
          dispatch(setAllRoomFeeList(all_room_fees, channel));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get room type fee list    */
function getRoomTypeFees(room_type_id, channel = "web") {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/fee/type/${room_type_id}/${channel}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_fees } }) => {
        if (success) {
          dispatch(setRoomFeeType(room_fees, room_type_id, channel));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get room fee   */
function getRoomFee(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/fee/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_fees } }) => {
        if (success) {
          dispatch(setRoomFee(room_fees[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomFee  */
function newRoomFee(room_fee) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/fee`, room_fee)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_fee: item } = room_fee; // room_sale.room_sale -> room_sale 로 구조 분해 할당.
          item.id = info.insertId; // id 추가.
          dispatch(setRoomFee(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomFee  */
function putRoomFee(id, room_fee) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/fee/${id}`, room_fee)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_fee: item } = room_fee; // room_sale.room_sale -> room_sale 로 구조 분해 할당.
          item.id = id; // id 추가.
          dispatch(setRoomFee(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomFees  */
function newRoomFees(room_fees) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/fees/list`, room_fees)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, new_room_fees } }) => {
        if (success) {
          //dispatch(setRoomFees(new_room_fees, false));
          dispatch(getRoomTypeFees(new_room_fees[0].room_type_id, new_room_fees[0].channel));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomFees  */
function putRoomFees(room_fees) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/fees/list`, room_fees)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, mod_room_fees } }) => {
        if (success) {
          //dispatch(setRoomFees(mod_room_fees, false));
          dispatch(getRoomTypeFees(mod_room_fees[0].room_type_id, mod_room_fees[0].channel));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del RoomFees  */
function delRoomFees(room_fees, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/fees/list`, { data: room_fees }) // delete 는 data 에 넣어야 한다.
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, del_room_fees } }) => {
        if (success) {
          //dispatch(setRoomFees(del_room_fees, true));
          dispatch(getRoomTypeFees(del_room_fees[0].room_type_id, del_room_fees[0].channel));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del RoomFee  */
function delRoomFee(id, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/fee/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const ietm = { id }; // id 만 넘겨서 삭제 한다.
          dispatch(setRoomFee(ietm));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del RoomTypeFee  */
function delRoomTypeFee(roomTypeId, channel, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/fee/type/${roomTypeId}/${channel}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomTypeFees(roomTypeId, channel));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomFeeList,
  getRoomTypeFees,
  getRoomFee,
  newRoomFee,
  putRoomFee,
  setRoomFees,
  newRoomFees,
  putRoomFees,
  delRoomFees,
  delRoomFee,
  delRoomTypeFee,
};

export { actionCreators };
