/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";
import _ from "lodash";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_VOICE_LIST = "SET_VOICE_LIST";
export const SET_VOICE = "SET_VOICE";
export const PUT_VOICE_QUEUE = "PUT_VOICE_QUEUE";
export const SET_SPEECH = "SET_SPEECH";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

const setVoiceList = (voices) => ({
  type: SET_VOICE_LIST,
  voices,
});

const setVoice = (name, url) => ({
  type: SET_VOICE,
  name,
  url,
});

const putVoiceQueue = (tracks) => ({
  type: PUT_VOICE_QUEUE,
  tracks,
});

const setSpeech = (speech) => ({
  type: SET_SPEECH,
  speech,
});
/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/* Get voice list and make */
function makeVoiceFileList(voices, reset, callback) {
  return (dispatch, getState) => {
    const {
      item: { place_id, voice_name, voice_rate },
    } = getState().preferences;

    if (!place_id) {
      console.log("- makeVoiceFileList require place_id ", { place_id });
      return;
    }

    // GOOGLE_APPLICATION_CREDENTIALS 인증이 로컬에서는 되지 않으므로 테스트 서버를 사용한다.
    ajax
      .post((envProps.IS_LOCAL ? "http://35.234.57.35" : "") + `/tts/list`, {
        place_id,
        voices,
        voice_name,
        voice_rate,
        reset,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { voices } }) => {
        if (success) {
          dispatch(setVoiceList(voices));
          if (callback) callback(voices);
        }
        if (callback) callback(null, null);
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get voice and make */
function makeVoiceFile(name, text, reset, callback) {
  if (!name) {
    console.error("- makeVoiceFile require name ");
    return;
  }

  return (dispatch, getState) => {
    const {
      item: { place_id, voice_name, voice_rate },
    } = getState().preferences;

    const { list } = getState().voice;
    const voice = _.find(list, { name: name });

    // 이미 같은 이름이 캐시에 있거나 재생속도가 같으면 반환.
    if (voice && !reset) {
      const { name, url } = voice;
      if (callback) callback(name, url);
    } else {
      // GOOGLE_APPLICATION_CREDENTIALS 인증이 로컬에서는 되지 않으므로 테스트 서버를 사용한다.
      ajax
        .post((envProps.IS_LOCAL ? "http://35.234.57.35" : "") + `/tts`, {
          place_id,
          voice: { name, text },
          voice_name,
          voice_rate,
          reset,
        })
        .then((res) => api.checkResponse(res, dispatch))
        .then(({ common: { success, message, error }, body: { name, url } }) => {
          if (success) {
            dispatch(setVoice(name, url));
            if (callback) callback(name, url);
          }
          if (callback) callback(null, null);
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        });
    }
  };
}

// 실시간 TTS
function getVoice(text, callback) {
  if (!text) {
    console.error("- getVoice require text ");
    return;
  }

  return (dispatch, getState) => {
    const {
      item: { voice_name, voice_rate },
    } = getState().preferences;

    const { speechList } = getState().voice;
    const speech = _.find(speechList, { text: text });
    if (speech) {
      dispatch(setSpeech(speech));
      if (callback) callback(speech);
      dispatch(setSpeech(null)); // 재생 후 삭제.
    } else {
      // GOOGLE_APPLICATION_CREDENTIALS 인증이 로컬에서는 되지 않으므로 테스트 서버를 사용한다.
      ajax
        .get((envProps.IS_LOCAL ? "http://35.234.57.35" : "") + `/tts?text=${text}&voice_name=${voice_name}&voice_rate=${voice_rate}`)
        .then((res) => api.checkResponse(res, dispatch))
        .then(({ common: { success, message, error }, body: { contents } }) => {
          if (success) {
            dispatch(setSpeech({ text, contents }));
            if (callback) callback({ text, contents });
            dispatch(setSpeech(null)); // 재생 후 삭제.
          }
          if (callback) callback(null);
        })
        .catch((error) => {
          dispatch(api.errorRequest(error, dispatch));
        });
    }
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getVoice,
  makeVoiceFileList,
  makeVoiceFile,
  putVoiceQueue,
};

export { actionCreators };
