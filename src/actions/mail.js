/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_MAIL_LIST = "SET_MAIL_LIST";
export const SET_MAIL = "SET_MAIL";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setMailList = (mails) => ({
  type: SET_MAIL_LIST,
  mails,
});

const setMail = (maile) => ({
  type: SET_MAIL,
  maile,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Mail list */
function getMailList(filter, between, begin, end) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mail/list/${filter}/${between}/${begin}/${end}/1000`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mails } }) => {
        if (success) {
          dispatch(setMailList(mails));
        }
        return mails;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get maile */
function getMail(id, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mail/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mail } }) => {
        if (success) {
          dispatch(setMail(mail[0]));
        }
        return mail[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Mail  */
function putMail(id, mail) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/mail/${id}`, mail)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { mail: item } = mail; // 구조 분해 할당.
          dispatch(setMail(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Mail  */
function newMail(mail) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/mail/`, mail)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { mail: item } = mail; // 구조 분해 할당.
          dispatch(setMail(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del Mail  */
function delMail(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/mail/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setMail(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getMailList,
  getMail,
  putMail,
  newMail,
  delMail,
};

export { actionCreators };
