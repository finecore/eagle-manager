/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_SEASON_PREMIUM_LIST = "SET_ALL_SEASON_PREMIUM_LIST";
export const SET_SEASON_PREMIUM_LIST = "SET_SEASON_PREMIUM_LIST";
export const SET_SEASON_PREMIUM = "SET_SEASON_PREMIUM";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllSeasonPremiumList = (all_season_premiums) => ({
  type: SET_ALL_SEASON_PREMIUM_LIST,
  all_season_premiums,
});

const setSeasonPremiums = (season_premiums, place_id) => ({
  type: SET_SEASON_PREMIUM_LIST,
  season_premiums,
  place_id,
});

const setSeasonPremium = (season_premium) => ({
  type: SET_SEASON_PREMIUM,
  season_premium,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  plaec premium list */
function getSeasonPremiumList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/season/premium/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_season_premiums } }) => {
        if (success) {
          dispatch(setAllSeasonPremiumList(all_season_premiums));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get between season premium list    */
function getBetweenSeasonPremium(place_id, begin, end) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/season/premium/${place_id}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { season_premiums } }) => {
        if (success) {
          dispatch(setSeasonPremiums(season_premiums, place_id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get SeasonPremium by id */
function getSeasonPremium(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/season/premium/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { season_premium } }) => {
        if (success) {
          dispatch(setSeasonPremium(season_premium[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New SeasonPremium  */
function newSeasonPremium(season_premium) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/fee`, season_premium)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getSeasonPremium(info.insertId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set SeasonPremium  */
function putSeasonPremium(id, season_premium) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/season/premium/${id}`, season_premium)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getSeasonPremium(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getSeasonPremiumList,
  getBetweenSeasonPremium,
  getSeasonPremium,
  newSeasonPremium,
  putSeasonPremium,
};

export { actionCreators };
