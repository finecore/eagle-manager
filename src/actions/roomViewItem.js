/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ROOM_VIEW_ITEM = "SET_ROOM_VIEW_ITEM";
export const SET_ALL_ROOM_VIEW_ITEM_LIST = "SET_ALL_ROOM_VIEW_ITEM_LIST";
export const SET_ROOM_VIEW_ITEM_LIST = "SET_ROOM_VIEW_ITEM_LIST";
export const SET_ROOM_VIEW_ITEM_KEY = "SET_ROOM_VIEW_ITEM_KEY";
export const REMOVE_ROOM_VIEW_ITEM = "REMOVE_ROOM_VIEW_ITEM";
export const SET_ROOM_VIEW_ITEM_ELEMENT = "SET_ROOM_VIEW_ITEM_ELEMENT";
/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

const setAllRoomViewItemList = (all_room_view_items) => ({
  type: SET_ALL_ROOM_VIEW_ITEM_LIST,
  all_room_view_items,
});

const setRoomViewItemList = (room_view_items, view_id) => ({
  type: SET_ROOM_VIEW_ITEM_LIST,
  room_view_items,
  view_id,
});

const setRoomViewItemElement = (view_id, elements) => ({
  type: SET_ROOM_VIEW_ITEM_ELEMENT,
  elements,
  view_id,
});

const setRoomViewItem = (room_view_item) => ({
  type: SET_ROOM_VIEW_ITEM,
  room_view_item,
});

const removeRoomViewItem = (id) => ({
  type: REMOVE_ROOM_VIEW_ITEM,
  id,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomViewItem By placeId */
function getPlaceRoomViewItemList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/view/item/all/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_view_items } }) => {
        if (success) {
          dispatch(setAllRoomViewItemList(all_room_view_items));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomViewItem By viewId */
function getRoomViewItemList(viewId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/view/item/view/${viewId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_view_items } }) => {
        if (success) {
          dispatch(setRoomViewItemList(room_view_items, viewId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* newRoomViewItem */
function newRoomViewItem(room_view_item) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/view/item`, { room_view_item })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(setRoomViewItem(room_view_item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomViewItem update */
function putRoomViewItem(id, room_view_item) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/view/item/${id}`, {
        room_view_item,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(setRoomViewItem(room_view_item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomViewItem update */
function putRoomViewItems(viewId, room_view_items, callcack) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/view/item/view/${viewId}`, {
        room_view_items,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          if (callcack) callcack(info);
          dispatch(getRoomViewItemList(viewId)); // 화면 갱신
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomViewItem delete */
function deleteRoomViewItem(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/view/item/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(removeRoomViewItem(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getPlaceRoomViewItemList,
  getRoomViewItemList,
  newRoomViewItem,
  putRoomViewItem,
  putRoomViewItems,
  setRoomViewItemElement,
  deleteRoomViewItem,
};

export { actionCreators };
