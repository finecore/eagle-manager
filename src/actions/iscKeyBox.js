/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ISC_KEY_BOX_LIST = "SET_ALL_ISC_KEY_BOX_LIST";
export const SET_ISC_KEY_BOX = "SET_ISC_KEY_BOX";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setIscKeyBoxList = (all_isc_key_boxs) => ({
  type: SET_ALL_ISC_KEY_BOX_LIST,
  all_isc_key_boxs,
});

const setIscKeyBox = (isc_key_box) => ({
  type: SET_ISC_KEY_BOX,
  isc_key_box,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  isc key box list */
function getKeyBoxList(serialno) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/key/box/all/${serialno}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_isc_key_boxs } }) => {
        if (success) {
          dispatch(setIscKeyBoxList(all_isc_key_boxs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get isc key box   */
function getIscKeyBox(id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/isc/key/box/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { isc_key_boxs } }) => {
        if (success) {
          dispatch(setIscKeyBox(isc_key_boxs[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New IscKeyBox  */
function newIscKeyBox(isc_key_box) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/isc/key/box`, isc_key_box)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { isc_key_box: item } = isc_key_box; // room_sale.room_sale -> room_sale 로 구조 분해 할당.
          item.id = info.insertId; // id 추가.
          dispatch(setIscKeyBox(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set IscKeyBox  */
function putIscKeyBox(id, isc_key_box) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/isc/key/box/${id}`, isc_key_box)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { isc_key_box: item } = isc_key_box; // room_sale.room_sale -> room_sale 로 구조 분해 할당.
          item.id = id; // id 추가.
          dispatch(setIscKeyBox(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del IscKeyBox  */
function delIscKeyBox(id, callback) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/isc/key/box/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const ietm = { id }; // id 만 넘겨서 삭제 한다.
          dispatch(setIscKeyBox(ietm));
          if (callback) callback();
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getKeyBoxList,
  getIscKeyBox,
  newIscKeyBox,
  putIscKeyBox,
  delIscKeyBox,
};

export { actionCreators };
