/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_STATE_LOG_LIST = "SET_ALL_ROOM_STATE_LOG_LIST";
export const SET_ROOM_STATE_LOG_LIST = "SET_ROOM_STATE_LOG_LIST";
export const SET_ROOM_STATE_LOG = "SET_ROOM_STATE_LOG";
export const SET_ROOM_STATE_DAY_LOG_LIST = "SET_ROOM_STATE_DAY_LOG_LIST";
export const ADD_ROOM_STATE_LOG_LIST = "ADD_ROOM_STATE_LOG_LIST";
export const SET_ROOM_STATE_LOG_SEARCH_LIST = "SET_ROOM_STATE_LOG_SEARCH_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomStateLogList = (all_room_state_logs) => ({
  type: SET_ALL_ROOM_STATE_LOG_LIST,
  all_room_state_logs,
});

const setRoomStateLogSearchList = (room_state_logs) => ({
  type: SET_ROOM_STATE_LOG_SEARCH_LIST,
  room_state_logs,
});

const setRoomStateLogList = (room_state_logs) => ({
  type: SET_ROOM_STATE_LOG_LIST,
  room_state_logs,
});

const setRoomStateDayLogs = (room_state_logs) => ({
  type: SET_ROOM_STATE_DAY_LOG_LIST,
  room_state_logs,
});

const setRoomStateLog = (room_state_log, room) => ({
  type: SET_ROOM_STATE_LOG,
  room_state_log,
  room,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomStateLog all list */
function getRoomStateLogList(placeId, begin, end) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/all/${placeId}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_state_logs } }) => {
        if (success) {
          dispatch(setAllRoomStateLogList(all_room_state_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  RoomStateLog search list */
function getRoomStateSearchLogs(placeId, begin, end, filter) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/all/${placeId}/${begin}/${end}/${filter}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_state_logs } }) => {
        if (success) {
          dispatch(setRoomStateLogSearchList(all_room_state_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  RoomStateLog list */
function getRoomStateLogs(placeId, limit, params = {}) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/list/${placeId}/${limit}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_state_logs } }) => {
        if (success) {
          dispatch(setRoomStateLogList(room_state_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  RoomStateLog day list */
function getRoomStateDayLogs(place_id, day) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/day/${place_id}/${day}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_state_logs } }) => {
        if (success) {
          dispatch(setRoomStateDayLogs(room_state_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  RoomStateLog last list */
function getRoomStateLastLogs(place_id, last_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/last/${place_id}/${last_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_state_logs } }) => {
        if (success) {
          dispatch(setRoomStateDayLogs(room_state_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomStateLog by type */
function getRoomStateLog(room_id, type) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/state/log/${room_id}/${type}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_state_log } }) => {
        if (success) {
          const { room } = getState();
          dispatch(setRoomStateLog(room_state_log, room));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomStateLog  */
function newRoomStateLog(room_state_log) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/state/log`, room_state_log)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_state_log: item } = room_state_log; //  구조 분해 할당.
          item.id = info.insertId; // id 추가.

          const { room } = getState();
          dispatch(setRoomStateLog(item, room));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomStateLogList,
  getRoomStateSearchLogs,
  getRoomStateDayLogs,
  getRoomStateLastLogs,
  getRoomStateLogs,
  getRoomStateLog,
  newRoomStateLog,
  setRoomStateLog,
};

export { actionCreators };
