/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_NETWORK_INFO = "SET_NETWORK_INFO";
export const DEL_NETWORK_INFO = "DEL_NETWORK_INFO";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setNetWorkInfo = (network) => ({
  type: SET_NETWORK_INFO,
  network,
});

const delNetWorkInfo = (network) => ({
  type: DEL_NETWORK_INFO,
  network,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/* check */
function check() {
  return (dispatch, getState) => {
    // Ajax as Axios.
    ajax
      .get(`${envProps.API_SERVER_URL}/alive`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { alive } }) => {
        if (success) {
          let network = {
            mode: "API",
            status: true,
          };
          dispatch(setNetWorkInfo(network));

          const { room } = getState();

          // 첫 화면에 네트워크 오류면 화면 갱신.
          if (room.list.length === 0) {
            setTimeout(() => {
              window.location.href = "/";
            }, 1000);
          }
        }
        return success;
      })
      .catch((error) => {
        let network = {
          mode: "API",
          status: false,
        };
        dispatch(setNetWorkInfo(network));
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  setNetWorkInfo,
  delNetWorkInfo,
  check,
};

export { actionCreators };
