/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ROOM_INTERRUPT = "SET_ROOM_INTERRUPT";
export const SET_ALL_ROOM_INTERRUPT_LIST = "SET_ALL_ROOM_INTERRUPT_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomInterruptList = (all_room_interrupts, auth) => ({
  type: SET_ALL_ROOM_INTERRUPT_LIST,
  all_room_interrupts,
  auth,
});

const setRoomInterrupt = (room_interrupt, auth) => ({
  type: SET_ROOM_INTERRUPT,
  room_interrupt,
  auth,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomInterrupt list */
function getRoomInterruptList(place_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/interrupt/all/${place_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_interrupts } }) => {
        if (success) {
          dispatch(setAllRoomInterruptList(all_room_interrupts, getState().auth));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* RoomInterrupt By Id */
function getRoomInterrupt(room_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/interrupt/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_interrupt } }) => {
        if (success) {
          if (room_interrupt[0]) dispatch(setRoomInterrupt(room_interrupt[0], getState().auth));
          else dispatch(setRoomInterrupt({ room_id }, getState().auth)); // 삭제.
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomInterrupt  */
function newRoomInterrupt(room_interrupt) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/interrupt`, room_interrupt)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_interrupt: item } = room_interrupt; // 구조 분해 할당.
          item.id = info.insertId;
          dispatch(setRoomInterrupt(item, getState().auth));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomInterrupt  */
function putRoomInterrupt(room_id, room_interrupt) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/interrupt/${room_id}`, room_interrupt)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { room_interrupt: item } = room_interrupt; // 구조 분해 할당.
          dispatch(setRoomInterrupt(item, getState().auth));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Delete RoomInterrupt  */
function delRoomInterrupt(room_id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/room/interrupt/${room_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(setRoomInterrupt({ room_id }, getState().auth)); // 삭제.
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomInterruptList,
  getRoomInterrupt,
  newRoomInterrupt,
  putRoomInterrupt,
  delRoomInterrupt,
};

export { actionCreators };
