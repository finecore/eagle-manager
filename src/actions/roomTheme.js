/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_ROOM_THEME_LIST = "SET_ALL_ROOM_THEME_LIST";
export const SET_ROOM_THEME = "SET_ROOM_THEME";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllRoomThemeList = (all_room_themes) => ({
  type: SET_ALL_ROOM_THEME_LIST,
  all_room_themes,
});

const setRoomTheme = (room_theme) => ({
  type: SET_ROOM_THEME,
  room_theme,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  RoomTheme all list */
function getRoomThemeList() {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/theme/all`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_room_themes } }) => {
        if (success) {
          dispatch(setAllRoomThemeList(all_room_themes));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get RoomTheme by theme */
function getRoomTheme(id, theme) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/room/theme/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { room_theme } }) => {
        if (success) {
          dispatch(setRoomTheme(room_theme[0]));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New RoomTheme  */
function newRoomTheme(room_theme) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/room/theme/`, room_theme)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomTheme(info.insertId));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set RoomTheme  */
function putRoomTheme(id, room_theme) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/room/theme/${id}`, room_theme)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          dispatch(getRoomTheme(id));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getRoomThemeList,
  getRoomTheme,
  newRoomTheme,
  putRoomTheme,
};

export { actionCreators };
