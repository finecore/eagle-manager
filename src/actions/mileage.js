/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_MILEAGE_LIST = "SET_MILEAGE_LIST";
export const SET_MILEAGE = "SET_MILEAGE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setMileageList = (mileages) => ({
  type: SET_MILEAGE_LIST,
  mileages,
});

const setMileage = (mileage) => ({
  type: SET_MILEAGE,
  mileage,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Mileage list */
function getMileageList(placeId) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/place/${placeId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileages } }) => {
        if (success) {
          dispatch(setMileageList(mileages));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get mileage */
function getMileage(placeId, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/${placeId}/phone/${phone}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileage } }) => {
        if (success) {
          dispatch(setMileage(mileage[0]));
        }
        return mileage[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Mileage  */
function putMileage(placeId, phone, mileage) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/mileage/${placeId}/phone/${phone}`, mileage)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { mileage: item } = mileage; // 구조 분해 할당.
          item.phone = phone;
          item.mod_date = new Date();
          dispatch(setMileage(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Mileage  */
function increaseMileage(place_id, phone, user_id, point, rollback) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/mileage/increase/${place_id}/${phone}/${user_id}/${point}/${rollback}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, mileage } }) => {
        if (success) {
          dispatch(setMileage(mileage));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Mileage  */
function decreaseMileage(place_id, phone, user_id, point, rollback) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/mileage/decrease/${place_id}/${phone}/${user_id}/${point}/${rollback}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, mileage } }) => {
        if (success) {
          dispatch(setMileage(mileage));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Mileage  */
function newMileage(mileage) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/mileage/`, mileage)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { mileage: item } = mileage; // 구조 분해 할당.
          item.id = info.insertId;
          item.point = 0;
          item.reg_date = new Date();
          dispatch(setMileage(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del Mileage  */
function delMileage(id, userId) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/mileage/${id}/${userId}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setMileage(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getMileageList,
  getMileage,
  putMileage,
  increaseMileage,
  decreaseMileage,
  newMileage,
  delMileage,
};

export { actionCreators };
