/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_NOTICE_LIST = "SET_NOTICE_LIST";
export const SET_NOTICE = "SET_NOTICE";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setNoticeList = (notices, isNotifi) => ({
  type: SET_NOTICE_LIST,
  notices,
  isNotifi,
});

const setNotice = (notice) => ({
  type: SET_NOTICE,
  notice,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Notice list */
function getNoticeList(filter, between, begin, end, isNotifi) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/notice/list/${filter}/${between}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { notices } }) => {
        if (success) {
          dispatch(setNoticeList(notices, isNotifi));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get notice */
function getNotice(id, phone) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/notice/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { notice } }) => {
        if (success) {
          dispatch(setNotice(notice[0]));
        }
        return notice[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Notice  */
function putNotice(id, notice) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/notice/${id}`, notice)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { notice: item } = notice; // 구조 분해 할당.
          dispatch(setNotice(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Notice  */
function newNotice(notice) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/notice/`, notice)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { notice: item } = notice; // 구조 분해 할당.
          dispatch(setNotice(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Del Notice   */
function delNotice(id) {
  return (dispatch, getState) => {
    return ajax
      .delete(`${envProps.API_SERVER_URL}/notice/${id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let item = { id, deleted: true };
          dispatch(setNotice(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getNoticeList,
  getNotice,
  putNotice,
  newNotice,
  delNotice,
};

export { actionCreators };
