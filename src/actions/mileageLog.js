/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_ALL_MILEAGE_LOG_LIST = "SET_ALL_MILEAGE_LOG_LIST";
export const SET_MILEAGE_LOG_LIST = "SET_MILEAGE_LOG_LIST";
export const SET_MILEAGE_LOG = "SET_MILEAGE_LOG";
export const SET_MILEAGE_DAY_LOG_LIST = "SET_MILEAGE_DAY_LOG_LIST";
export const SET_MILEAGE_LOG_SEARCH_LIST = "SET_MILEAGE_LOG_SEARCH_LIST";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setAllMileageLogList = (all_mileage_logs) => ({
  type: SET_ALL_MILEAGE_LOG_LIST,
  all_mileage_logs,
});

const setMileageLogSearchList = (mileage_logs) => ({
  type: SET_MILEAGE_LOG_SEARCH_LIST,
  mileage_logs,
});

const setMileageLogList = (mileage_logs) => ({
  type: SET_MILEAGE_LOG_LIST,
  mileage_logs,
});

const setMileageDayLogs = (mileage_logs) => ({
  type: SET_MILEAGE_DAY_LOG_LIST,
  mileage_logs,
});

const setMileageLog = (mileage_log, room) => ({
  type: SET_MILEAGE_LOG,
  mileage_log,
  room,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  MileageLog all list */
function getMileageLogList(placeId, begin, end) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/all/${placeId}/${begin}/${end}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_mileage_logs } }) => {
        if (success) {
          dispatch(setAllMileageLogList(all_mileage_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  MileageLog search list */
function getMileageSearchLogs(placeId, begin, end, filter) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/all/${placeId}/${begin}/${end}/${filter}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { all_mileage_logs } }) => {
        if (success) {
          dispatch(setMileageLogSearchList(all_mileage_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  MileageLog list */
function getMileageLogs(placeId, limit, params = {}) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/list/${placeId}/${limit}`, {
        params: params,
      })
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileage_logs } }) => {
        if (success) {
          dispatch(setMileageLogList(mileage_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  MileageLog day list */
function getMileageDayLogs(place_id, day) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/day/${place_id}/${day}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileage_logs } }) => {
        if (success) {
          dispatch(setMileageDayLogs(mileage_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/*  MileageLog last list */
function getMileageLastLogs(place_id, last_id) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/last/${place_id}/${last_id}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileage_logs } }) => {
        if (success) {
          dispatch(setMileageDayLogs(mileage_logs));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get MileageLog by type */
function getMileageLog(room_id, type) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/mileage/log/${room_id}/${type}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { mileage_log } }) => {
        if (success) {
          const { room } = getState();
          dispatch(setMileageLog(mileage_log, room));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set MileageLog  */
function newMileageLog(mileage_log) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/mileage/log`, mileage_log)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          const { mileage_log: item } = mileage_log; //  구조 분해 할당.
          item.id = info.insertId; // id 추가.

          const { room } = getState();
          dispatch(setMileageLog(item, room));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getMileageLogList,
  getMileageSearchLogs,
  getMileageDayLogs,
  getMileageLastLogs,
  getMileageLogs,
  getMileageLog,
  newMileageLog,
  setMileageLog,
};

export { actionCreators };
