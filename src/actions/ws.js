/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { JOIN_REQUESTED, LOGOUT_REQUESTED, MESSAGE_ADDED, USER_REQUESTED, USER_STARTED_TYPING, USER_STOPPED_TYPING } from "constants/message-types";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const TYPING_STARTED = "TYPING_STARTED";
export const TYPING_STOPPED = "TYPING_STOPPED";
export const MESSAGE_SEND_REQUESTED = "MESSAGE_SEND_REQUESTED";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const requestUsers = (send) => send(USER_REQUESTED);

const join =
  (user, nobroad = false) =>
  (dispatch, getState, { send }) => {
    console.log("- join user", user);
    send(JOIN_REQUESTED, { user, nobroad });
  };

const logout =
  (user, nobroad = false) =>
  (dispatch, getState, { send }) => {
    console.log("- logout user", user);
    send(LOGOUT_REQUESTED, { user, nobroad });
  };

const close =
  () =>
  (dispatch, getState, { send, webSocket }) => {
    console.log("- close socket");
    // 웹소켓 접속 닫음.
    if (webSocket) webSocket.close();
  };

const sendMessage =
  (message) =>
  (dispatch, getState, { send }) => {
    const { currentUserIsTyping } = getState();

    // if we're sending a message we're probably not also typing :)
    if (currentUserIsTyping) {
      dispatch({ type: TYPING_STOPPED });
      send(USER_STOPPED_TYPING);
    }

    dispatch({ type: MESSAGE_SEND_REQUESTED });
    send(MESSAGE_ADDED, { message });
  };

const typingTimerLength = 400;

const typing =
  () =>
  (dispatch, getState, { send }) => {
    const { currentUserIsTyping } = getState();
    // don't spam "typing" events and websocket messages
    if (!currentUserIsTyping) {
      dispatch({ type: TYPING_STARTED });
      send(USER_STARTED_TYPING);
    }

    const lastTypingTime = Date.now();

    setTimeout(() => {
      const { currentUserIsTyping } = getState();
      const timeDiff = Date.now() - lastTypingTime;

      if (timeDiff >= typingTimerLength && currentUserIsTyping) {
        dispatch({ type: TYPING_STOPPED });
        send(USER_STOPPED_TYPING);
      }
    }, typingTimerLength);
  };

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  requestUsers,
  join,
  logout,
  close,
  sendMessage,
  typing,
};

export { actionCreators };
