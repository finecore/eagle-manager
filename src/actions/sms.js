/////////////////////////////////////////////////////////////////////////////
////////////////////////////// imports //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
import { envProps } from "config/configureStore";
import { api, ajax } from "utils/api-util";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// actions //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
export const SET_SMS_LIST = "SET_SMS_LIST";
export const SET_SMS = "SET_SMS";

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// action functions  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const setSmsList = (smses) => ({
  type: SET_SMS_LIST,
  smses,
});

const setSms = (sms) => ({
  type: SET_SMS,
  sms,
});

/////////////////////////////////////////////////////////////////////////////
////////////////////////////// api actions //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*  Sms list */
function getSmsList() {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/sms/all/`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { smses } }) => {
        if (success) {
          dispatch(setSmsList(smses));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Get sms */
function getSms(tr_num) {
  return (dispatch, getState) => {
    return ajax
      .get(`${envProps.API_SERVER_URL}/sms/${tr_num}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { sms } }) => {
        if (success) {
          dispatch(setSms(sms[0]));
        }
        return sms[0];
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Set Sms  */
function putSms(tr_num, sms) {
  return (dispatch, getState) => {
    return ajax
      .put(`${envProps.API_SERVER_URL}/sms/${tr_num}`, sms)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { sms: item } = sms; // 구조 분해 할당.
          dispatch(setSms(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* New Sms  */
function newSms(sms) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/sms/`, sms)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info } }) => {
        if (success) {
          let { sms: item } = sms; // 구조 분해 할당.
          dispatch(setSms(item));
        }
        return success;
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/* Send AuthNo  */
function sendAuthNo(phone, callback) {
  return (dispatch, getState) => {
    return ajax
      .post(`${envProps.API_SERVER_URL}/sms/authno/${phone}`)
      .then((res) => api.checkResponse(res, dispatch))
      .then(({ common: { success, message, error }, body: { info, authNo } }) => {
        console.log("- sendAuthNo authNo", authNo);
        callback(authNo);
      })
      .catch((error) => {
        dispatch(api.errorRequest(error, dispatch));
      });
  };
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////// export actionCreators ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////
const actionCreators = {
  getSmsList,
  getSms,
  putSms,
  newSms,
  sendAuthNo,
};

export { actionCreators };
