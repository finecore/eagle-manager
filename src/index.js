import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { ConnectedRouter } from "react-router-redux";
import { BrowserRouter as Router } from "react-router-dom"; // Router Link 사용시

import { Provider } from "react-redux";
import { configureStore, history } from "./config/configureStore";
import { SnackbarProvider } from "notistack"; // Snackbar stack.

import ErrorBoundary from "./components/Error/ErrorBoundary";

// pages.
import App from "./pages/App";

// styles.
import "./assets/styles/base.scss";
import "@mdi/font/css/materialdesignicons.css";

const rootElement = document.getElementById("root");

const renderApp = (Component) => {
  // create store and web socket connect promess function.
  configureStore().then((store) => {
    ReactDOM.render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <SnackbarProvider maxSnack={5} transitionDuration={{ exit: 180, enter: 100 }}>
            <ErrorBoundary history={history}>
              <Router>
                <Component />
              </Router>
            </ErrorBoundary>
          </SnackbarProvider>
        </ConnectedRouter>
      </Provider>,
      rootElement
    );

    if (module.hot) {
      // 핫 디플로이 설정.
      module.hot.accept("./pages/App", () => {
        const NextApp = require("./pages/App").default;
        store.replaceReducer(NextApp);
      });
    }
  });
};

renderApp(App);

registerServiceWorker();
