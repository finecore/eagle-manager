// Imports the Google Cloud client library
const textToSpeech = require("@google-cloud/text-to-speech");

// Import other required libraries
const fs = require("fs");
const util = require("util");

async function main() {
  // Creates a client
  const client = new textToSpeech.TextToSpeechClient();

  console.log("- client", client);

  // The text to synthesize
  const text = "101호 입실 입니다.";

  console.log("- text", text);

  // Construct the request
  const request = {
    audioConfig: {
      audioEncoding: "MP3",
      //audioEncoding: "LINEAR16",
      effectsProfileId: ["large-home-entertainment-class-device"],
      pitch: 0,
      speakingRate: 1.3,
    },
    input: {
      text,
    },
    voice: {
      languageCode: "ko-KR",
      name: "ko-KR-Wavenet-B",
    },
  };

  // Performs the Text-to-Speech request
  const [response] = await client.synthesizeSpeech(request);
  // Write the binary audio content to a local file
  const writeFile = util.promisify(fs.writeFile);

  await writeFile("./test.mp3", response.audioContent, "binary"); // dev server 경로.
  // await writeFile("../../build/voice/output.mp3", response.audioContent, "binary"); // build 경로.

  console.log("Audio content written to file");
}

main();
