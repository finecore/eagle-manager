/*
1. Google Cloud SDK(=gcloud) 설치하기를 참고하여 gcloud 설치
2. gcloud auth login을 터미널에 입력 -> gcloud auth application-default login 입력(Could not load the default credentials 오류 시)
3. gcloud config set project eagle-node (API 사용 설정된 Key 를 가진 프로젝트로 설정)
4. Google Cloud Console에서 메뉴 - API 및 서비스 - 서비스 계정 생성 및 다운로드
5. export GOOGLE_APPLICATION_CREDENTIALS="/home/friendkj/frontend/eagle-service-key.json" 환경설정
6. yarn add @google-cloud/text-to-speech -D 설치

— Error Request had insufficient authentication scopes 발생 시
—> 인스턴스의 서비스 계정 및 액세스 범위 변경을 하면 된다. (API 사용 범위를 한정 해서 나오는 오류)

1. Compute Engine의 VM 인스턴스 페이지로 이동합니다.VM 인스턴스 페이지로 이동
2. 서비스 계정을 변경할 VM 인스턴스를 클릭합니다.
3. 인스턴스가 중지되지 않은 경우 중지 버튼을 클릭합니다. 인스턴스가 중지될 때까지 기다립니다.
4. 그런 다음 수정 버튼을 클릭합니다.
5. 서비스 계정 섹션까지 아래로 스크롤합니다.
6. 드롭다운 메뉴에서 원하는 서비스 계정을 선택합니다.
7. 범위를 변경하려면 액세스 범위 섹션에서 필요사항에 적합한 범위를 설정합니다. VM 인스턴스에 필요한 액세스 범위만 지정하는 것이 좋습니다. 설정할 적절한 액세스 범위를 모르는 경우 모든 Cloud API에 대한 전체 액세스 허용을 선택한 다음 IAM 역할을 설정하여 액세스를 제한합니다.
8. 저 버튼을 클릭하여 변경사항을 저장합니다.
*/

const { Storage } = require("@google-cloud/storage");

// Imports the Google Cloud client library
const textToSpeech = require("@google-cloud/text-to-speech");

// Import other required libraries
const fs = require("fs");
const util = require("util");

const storage = new Storage();

// Makes an authenticated API request.
storage
  .getBuckets()
  .then((results) => {
    const buckets = results[0];

    console.log("Google Cloud API Test ...");
    console.log("GCP Storage Buckets:");
    buckets.forEach((bucket) => {
      console.log(bucket.name);
    });
    console.log("Google Cloud API Test Done.");
  })
  .catch((err) => {
    console.error("ERROR:", err);
  });

// 비동기 음성(인스턴스 생성)
async function voice(text, name, rate) {
  // Creates a client
  const client = new textToSpeech.TextToSpeechClient();

  if (!text) {
    console.error("Audio text nothing...");
    return false;
  }

  // Construct the request
  const request = {
    audioConfig: {
      audioEncoding: "MP3",
      effectsProfileId: ["large-home-entertainment-class-device"],
      pitch: 0,
      speakingRate: rate || 1.0,
    },
    input: {
      text,
    },
    voice: {
      languageCode: "ko-KR",
      name: name || "ko-KR-Wavenet-B",
    },
  };

  // Performs the Text-to-Speech request
  const [response] = await client.synthesizeSpeech(request);
  // Write the binary audio content to a local file

  // console.log("Audio content response  " + response.audioContent.length);

  return response;
}

let makingMp3 = {};

// 비동기 음성 파일 생성.
async function mp3(url, text, name, rate) {
  if (makingMp3[url]) {
    console.info("Audio file making wait...", url);
    return false;
  }

  // 생성중인 파일 목록 추가.
  makingMp3[url] = true;

  // 1 분 후 제거.
  setTimeout(() => {
    if (makingMp3[url]) delete makingMp3[url];
  }, 60 * 1000);

  // Creates a client
  const client = new textToSpeech.TextToSpeechClient();

  if (!url) {
    console.error("Audio file url nothing...");
    delete makingMp3[url];
    return false;
  }

  if (!text) {
    console.error("Audio text nothing...");
    delete makingMp3[url];
    return false;
  }

  // Construct the request
  const request = {
    audioConfig: {
      audioEncoding: "MP3",
      //audioEncoding: "LINEAR16",
      effectsProfileId: ["large-home-entertainment-class-device"],
      pitch: 0,
      speakingRate: rate || 1.3,
    },
    input: {
      text,
    },
    voice: {
      languageCode: "ko-KR",
      name: name || "ko-KR-Wavenet-B",
    },
  };

  // Performs the Text-to-Speech request
  const [response] = await client.synthesizeSpeech(request);

  console.info("Audio file making success..", text);

  // Write the binary audio content to a local file
  const writeFile = util.promisify(fs.writeFile);

  await writeFile(url, response.audioContent, "binary");

  console.log("Audio content written to file: " + url);

  delete makingMp3[url];

  return true;
}

function isExist(url, callback) {
  // console.log("Audio content isExist to file: " + url);

  try {
    callback(fs.existsSync(url));
  } catch (err) {
    console.error(err);
    callback(false);
  }
}

module.exports = { isExist, voice, mp3 };
