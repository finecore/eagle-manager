# Eagel Manager

icrew.co.kr

숙박업소 객실 관리 프로그램.

## Data Sync

- 설정 : 서버 로드 밸런서(LB) 에서 세션 연관성을 IP 로 설정 하여 동일 IP (업소 단위) 는 같은 서버에 넣는다.
- 만약 같은 업소에서 2 개 이상의 IP를 사용 하면 아래 로직에서 수정 되어야함.

1. Web -> Web : Manager -> Web Server -> Other Mangers (동일 IP 의 동일 업소 계정으로 전파)
2. Web -> Dev : Manager -> Web Server -> Api Server -> Devices (동일 업소 계정으로 전파)
3. Dev -> Dev : Device -> Api Server -> Other Devices (동일 IP 의 동일 업소 계정으로 전파)
4. Dev -> Web : Device -> Api Server -> Web Servers[1,2] -> Managers (동일 업소 계정으로 전파)
